# Underlord, Alauda 平台访问第三方平台的唯一入口

![underlord](underlord.jpg)

从 release-2.12 开始，Underlord 使用 [ait-shared](https://gitlab-ce.alauda.cn/frontend/ait-shared) [子模块仓库](http://confluence.alauda.cn/x/La0GB)与 Icarus 复用代码。clone 仓库之后，需初始化子模块仓库：

```shell
$ git submodule update --init --recursive
$ git config submodule.recurse true
```

- [Getting Started](#getting-started)
  - [依赖准备（以 MacOS 为例）](#依赖准备以-macos-为例)
  - [配置本地开发环境](#配置本地开发环境)
- [开始开发](#开始开发)
  - [提交代码规范](#提交代码规范)
    - [类型](#类型)
    - [主题](#主题)
  - [提交合并请求](#提交合并请求)
- [根目录简介](#根目录简介)
- [构建](#构建)
  - [自动构建](#自动构建)
  - [手动构建](#手动构建)
- [部署](#部署)

- Angular 作为前端框架的应用。
- 前端应减少逻辑复杂度，并且考虑到页面的高度相似性，应尽量提升代码的复用性。

## Getting Started

### 依赖准备（以 MacOS 为例）

- Node.js 10+
- Yarn 1.13+

### 配置本地开发环境

请在开发前，首先安装`yarn`。
安装之后，执行 `yarn dev`

> 注：yarn 命令实际上会执行列在 `package.json` 文件的 `scripts` 里面的脚本命令。以上命令执行了本地前端开发模式。

### alauda-console 代理配置

参考[文档](http://confluence.alauda.cn/pages/viewpage.action?pageId=56992615)

## 开始开发

### 产品运营中心及相关产品前端代码开发指南

参考[文档](http://confluence.alauda.cn/pages/viewpage.action?pageId=61910798)

### 提交代码规范

代码提交的 commit message 应遵循一定的规范。message 的基本信息格式为：

    <类型>: <主题>

例如: `feat: add node page routings`

#### 类型

必须为以下的一种

- **feat**: 新特性
- **fix**: bug 修复
- **docs**: 只包含文档的修改
- **style**: 不影响代码含义的更改 (例如: 空格, 代码格式, 补充缺失的分号等)
- **refactor**: 重构性代码
- **perf**: 提升性能的代码修改
- **test**: 增加新的或是修复测试用例
- **build**: 影响构建系统、持续集成配置或是更新外部依赖的更新
- **chore**: 不影响源码或是测试的维护性更新

#### 主题

主题包含对变更的简要描述：

- 使用命令式、现在时：使用 "change"，不要使用 "changed" 或是 "changes"
- 不要大写第一个字母
- 不要以点 `.` 作为结尾

### 提交合并请求

由于创建新的 Pull Request 或是更新 PR 都会会启动新的流水线任务，所以你需要保证代码质量足够之后再提交。原则上，代码提交后应保证获得一个非机器人的 Approval 后，并且 e2e 通过后才能合并 PR。

> 注：Jenkins 流水线地址 <http://jenkins-alaudak8s.myalauda.cn/>

## 根目录简介

- e2e: E2E 相关文件夹
- src: Angular 前端代码
- dockerfiles: 存放构建所需的 dockerfile 文件

## 构建

- /dockerfiles 文件夹中 /base 中的 dockerfile 用于构建基于 index.alauda.cn/alaudaorg/alpine:3.9.3 包含 node 和 yarn 的基础镜像， /prod 中用于执行 “ng build --prod” 构建 underlord 镜像

### 自动构建

1. 代码提交 pr 后会触发 underlord 的流水线，流水线完成会自动生成镜像并存放到镜像仓库（镜像仓库地址：<https://enterprise.alauda.cn/console/image/repository/detail?registryName=alauda_public_registry&repositoryName=underlord&projectName=>

### 手动构建

1.  在 underlord 构建项目输入 commitId 触发构建（构建项目地址：<https://enterprise.alauda.cn/console/build/config/detail?name=78790844-25b9-459c-8816-29ac5b0c2a85）>
2.  若构建正常会生成带有 commitId 后缀的 underlord 镜像，存放于 underlord 的镜像仓库 （镜像仓库地址：<https://enterprise.alauda.cn/console/image/repository/detail?registryName=alauda_public_registry&repositoryName=underlord&projectName=）>

## 部署

1. 镜像仓库中已产出对应镜像，修改本地 /deploy/underlord.yaml，将 yaml 中 “initContainers” 下的 “index.alauda.cn/alaudaorg/underlord:latest” 修改为 “index.alauda.cn/alaudaorg/underlord:” + “{{目标镜像tag}}”
2. 登入目标 k8s 集群，执行 “kubectl apply -f deploy/underlord.yaml -n alauda-system”，正常应返回

```sh
deployment "underlord" configured
service "underlord" configured
ingress "underlord" configured
configmap "underlord" configured
```

3. 访问环境验证，例如： “<https://129.28.182.197/console-platform/”>
