FROM alpine:3.11

WORKDIR /underlord

RUN mkdir -p /underlord/dist/static

COPY . /underlord/dist

ARG commit_id=dev
ARG app_version=dev
ENV COMMIT_ID=${commit_id}
ENV APP_VERSION=${app_version}
