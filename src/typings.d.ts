/// <reference path="../node_modules/monaco-editor/monaco.d.ts" />

declare module '*.svg' {
  const content: string;
  export = content;
}
