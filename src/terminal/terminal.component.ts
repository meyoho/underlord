import { ifExist } from '@alauda/common-snippet';
import { animate, style, transition, trigger } from '@angular/animations';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostListener,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ShellComponent, Theme } from 'terminal/shell/shell.component';
import { TerminalPageParams } from 'terminal/type';
import { SearchAddon } from 'xterm-addon-search';

const SHELL_THEME_KEY = 'alu-shell-theme';
function getPodShortName(name: string) {
  const names = name.split('-');
  return names[names.length - 1];
}

@Component({
  templateUrl: './terminal.component.html',
  styleUrls: ['./terminal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('zoomInOut', [
      transition(':enter', [
        style({ transform: 'scale(0.95)', opacity: '0' }),
        animate(200, style({ transform: 'scale(1)', opacity: '1' })),
      ]),
      transition(':leave', [
        style({ transform: 'scale(1)', opacity: '1' }),
        animate(200, style({ transform: 'scale(0.95)', opacity: '0' })),
      ]),
    ]),
  ],
})
export class TerminalComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChildren(ShellComponent)
  execShells: QueryList<ShellComponent>;

  @ViewChild('searchInput', { read: ElementRef, static: false })
  searchInput: ElementRef;

  params: TerminalPageParams;
  shell: ShellComponent;
  podsBackup: string[] = [];
  isFindBarActive = false;
  searchQuery = '';
  keys = Object.keys;
  activeIndex: number;
  pods: string[] = [];
  url: string;
  target: string;
  theme: Theme;
  isDarkTheme = false;

  params$ = this.activatedRoute.queryParams.pipe(
    tap((params: TerminalPageParams) => {
      this.params = params;
      try {
        const pods = params.pods.split(';');
        this.activeIndex = 0;
        this.pods = params.selectedPod ? [this.params.selectedPod] : [...pods];
        this.podsBackup = [...pods];
        this.cdr.markForCheck();
      } catch (err) {}
    }),
  );

  getPodShortName = getPodShortName;

  private keySub: Subscription;
  private searchAddon: SearchAddon;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.setTheme(
      (localStorage.getItem(SHELL_THEME_KEY) as 'dark' | 'light') || 'dark',
    );
  }

  ngAfterViewInit() {
    this.shell = this.execShells.find(item => {
      return item.podName === this.pods[this.activeIndex];
    });
    this.shell.term.loadAddon((this.searchAddon = new SearchAddon()));
    this.subscribeKeyEvent();
  }

  ngOnDestroy() {
    if (this.keySub) {
      this.keySub.unsubscribe();
    }
  }

  suffixText(text: string) {
    return ifExist(text, `${text} /`);
  }

  subscribeKeyEvent() {
    if (this.keySub) {
      this.keySub.unsubscribe();
    }
    if (this.shell) {
      this.keySub = this.shell.keyEvent$.subscribe(event =>
        this.onKeyEvent(event),
      );
    }
  }

  setTheme(theme: Theme) {
    this.isDarkTheme = theme === 'dark';
    localStorage.setItem(SHELL_THEME_KEY, theme);
    this.theme = theme;
    this.cdr.markForCheck();
  }

  toggleTheme() {
    this.setTheme(this.isDarkTheme ? 'light' : 'dark');
  }

  // Capturing Ctrl keyboard events in Javascript
  @HostListener('window:keydown', ['$event'])
  onKeyEvent(event: KeyboardEvent) {
    if ((event.metaKey || event.ctrlKey) && event.code === 'KeyF') {
      event.preventDefault();
      event.stopPropagation();
      this.toggleFindPanel();
    }
  }

  toggleFindPanel() {
    this.isFindBarActive = !this.isFindBarActive;
    this.searchQuery = '';
    this.cdr.markForCheck();
  }

  findPrevious(query: string) {
    this.searchAddon.findPrevious(query);
  }

  findNext(query: string) {
    this.searchAddon.findNext(query);
  }

  focusSearchInput() {
    if (this.isFindBarActive) {
      this.searchInput.nativeElement.focus();
    } else {
      this.shell.term.focus();
    }
  }

  isActiveMenuItem(pod: string) {
    return pod === this.pods[this.activeIndex];
  }

  close(index: number) {
    this.pods.splice(index, 1);
  }

  selectIndex(pod: string) {
    const selectIndex = this.pods.findIndex(_pod => _pod === pod);
    if (selectIndex !== this.activeIndex) {
      if (selectIndex < 0) {
        this.pods.push(pod);
        this.activeIndex = this.pods.length - 1;
      } else {
        this.activeIndex = selectIndex;
      }
    }
  }

  onTabIndexChange(index: number) {
    this.shell = this.execShells.find(
      item => item.podName === this.pods[index],
    );
    this.subscribeKeyEvent();
    this.activeIndex = index;
  }

  trackByFn(_: number, pod: string) {
    return pod;
  }
}
