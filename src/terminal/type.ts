declare global {
  interface Window {
    unescape(str: string): string;
    escape(str: string): string;
  }
}

export interface TerminalPageParams {
  app?: string;
  pods: string; // 多个pod 使用 ';' 分割
  selectedPod?: string;
  container: string;
  namespace: string;
  cluster: string;
  command?: string;
  user?: string;
  ctl?: string;
  url?: string;
  target?: string;
}
