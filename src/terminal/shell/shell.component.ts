import {
  ApiGatewayService,
  AuthorizationStateService,
  TranslateService,
  ValueHook,
  publishRef,
} from '@alauda/common-snippet';
import { NotificationService } from '@alauda/ui';
import { HttpParams } from '@angular/common/http';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { debounce, isEqual } from 'lodash-es';
import {
  EMPTY,
  ReplaySubject,
  Subscription,
  combineLatest,
  interval,
} from 'rxjs';
import { ITheme, Terminal } from 'xterm';
import { FitAddon } from 'xterm-addon-fit';

export enum ConnectionStatus {
  Connecting = 'connecting',
  GoodConnection = 'goodConnection',
  BadConnection = 'badConnection',
  Disconnected = 'disconnected',
}

const DARK_THEME: ITheme = {
  background: '#283238',
  foreground: '#d4d4d4',
};

const LIGHT_THEME: ITheme = {
  background: '#fff',
  foreground: '#000',
  cursor: '#00a',
  selection: '#00000033',
};

const SHELL_THEMES = {
  dark: DARK_THEME,
  light: LIGHT_THEME,
};

export type Theme = keyof typeof SHELL_THEMES;

const PING_INTERVAL = 5000;

function utf8ToB64(str: string) {
  return window.btoa(window.unescape(encodeURIComponent(str)));
}

function b64ToUtf8(str: string) {
  return decodeURIComponent(window.escape(window.atob(str)));
}

@Component({
  selector: 'alu-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ShellComponent implements AfterViewInit, OnDestroy {
  @Input() podName: string;
  @Input() container: string;
  @Input() namespace: string;
  @Input() cluster: string;
  @Input() user: string;
  @Input() command: string;

  @ValueHook(function (this: ShellComponent, theme: Theme) {
    this.setTheme(theme);
  })
  @Input()
  theme: Theme;

  @ViewChild('anchor', { static: true }) anchorRef: ElementRef;

  connecting = false;
  connectionClosed = false;

  term: Terminal;

  keyEvent$ = new ReplaySubject<KeyboardEvent>(2);

  private previousConfig: {
    apiGatewayAddress?: string;
    accessToken?: string;
    cluster?: string;
    container?: string;
    namespace?: string;
    podName?: string;
  };

  private conn: WebSocket;
  private readonly connSubject = new ReplaySubject<string>(100);
  private connected = false;
  private debouncedFit: EventListener;
  private readonly subscriptions: Subscription[] = [];
  private pingSub: Subscription;

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly notification: NotificationService,
    private readonly apiGateway: ApiGatewayService,
    private readonly auth: AuthorizationStateService,
    private readonly translate: TranslateService,
  ) {}

  setTheme(theme: Theme = this.theme) {
    if (!this.term) {
      return;
    }

    this.term.setOption('theme', SHELL_THEMES[theme]);
  }

  ngAfterViewInit() {
    this.term = new Terminal({
      fontSize: 14,
      fontFamily: 'Consolas, "Courier New", monospace',
      bellStyle: 'sound',
      cursorBlink: true,
    });
    const fitAddon = new FitAddon();
    this.term.loadAddon(fitAddon);
    this.setTheme();
    this.term.open(this.anchorRef.nativeElement);

    this.debouncedFit = debounce(() => {
      fitAddon.fit();
      this.cdr.markForCheck();
    }, 100);
    this.debouncedFit(null);
    window.addEventListener('resize', this.debouncedFit);

    this.term.onData(this.onTerminalSendString.bind(this));
    this.term.onSelectionChange(this.onTerminalSelection.bind(this));
    this.term.onResize(this.onTerminalResize.bind(this));
    this.term.onKey(({ domEvent }) => this.keyEvent$.next(domEvent));

    this.subscriptions.push(
      this.connSubject.subscribe(data => this.handleConnectionMessage(data)),
      combineLatest([
        this.apiGateway.getApiAddress(),
        this.auth.getToken(),
      ]).subscribe(args => this.setupConnection(...args)),
    );

    this.cdr.markForCheck();
  }

  ngOnDestroy() {
    if (this.conn && this.connected) {
      this.resetConnection();
    }

    if (this.connSubject) {
      this.connSubject.complete();
    }

    if (this.term) {
      this.term.dispose();
    }

    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  get connectionStatus(): ConnectionStatus {
    if (this.connecting) {
      return ConnectionStatus.Connecting;
    }

    if (!this.connecting && this.connectionClosed) {
      return ConnectionStatus.Disconnected;
    }

    return ConnectionStatus.GoodConnection;
  }

  get shouldShowProgressBar() {
    return this.connecting || this.statusColor !== 'primary';
  }

  get statusColor() {
    if (!this.connecting && this.connectionClosed) {
      return 'warn';
    }

    return 'primary';
  }

  setupConnection(apiGatewayAddress: string, accessToken: string) {
    this.connecting = true;
    this.connectionClosed = false;

    const newConfig = {
      apiGatewayAddress,
      accessToken,
      cluster: this.cluster,
      container: this.container,
      namespace: this.namespace,
      podName: this.podName,
    };

    if (this.conn && isEqual(this.previousConfig, newConfig)) {
      // Do nothing
      return;
    }

    // Should only have one connection per component:
    if (this.conn) {
      this.conn.close();
    }

    this.previousConfig = newConfig;

    const suCmd = `su -p ${this.user} -c ${this.command}`;

    this.conn = new WebSocket(
      apiGatewayAddress.replace(/^http/, 'ws') +
        `/kubernetes/${this.cluster}/api/v1/namespaces/${this.namespace}/pods/${
          this.podName
        }/exec?${new HttpParams({
          fromObject: {
            'access-token': accessToken,
            container: this.container,
            stdin: 'true',
            stdout: 'true',
            stderr: 'true',
            tty: 'true',
            command: ['sh', '-c', suCmd],
          },
        })}`,
      ['base64.channel.k8s.io'],
    );
    this.conn.addEventListener('open', this.onConnectionOpen.bind(this));
    this.conn.addEventListener('message', this.onConnectionMessage.bind(this));
    this.conn.addEventListener('close', this.onConnectionClose.bind(this));
    this.cdr.markForCheck();
  }

  reload() {
    location.reload();
  }

  private onConnectionOpen() {
    this.connected = true;
    this.connecting = false;
    this.connectionClosed = false;

    // Make sure the terminal is with correct display size.
    this.onTerminalResize();

    // Focus on connection
    this.term.focus();

    if (this.pingSub) {
      this.pingSub.unsubscribe();
    }

    this.pingSub = this.getPing$().subscribe(() => this.conn.send('0'));

    this.cdr.markForCheck();
  }

  private handleConnectionMessage(data: string) {
    this.term.write(data);
    this.cdr.markForCheck();
  }

  private onConnectionMessage(evt: MessageEvent) {
    const data = evt.data.slice(1);
    switch (evt.data[0]) {
      case '1':
      case '2':
      case '3':
        this.connSubject.next(b64ToUtf8(data));
        break;
    }
  }

  private onConnectionClose(_evt?: CloseEvent) {
    if (!this.connected) {
      return;
    }
    this.notification.error(this.translate.get('exec_disconnected'));
    this.resetConnection();
    this.cdr.markForCheck();
  }

  private resetConnection() {
    this.pingSub.unsubscribe();
    this.conn.close();
    this.connected = false;
    this.connecting = false;
    this.connectionClosed = true;
  }

  private onTerminalSendString(str: string) {
    if (this.connected) {
      this.conn.send('0' + utf8ToB64(str));
    }
  }

  private onTerminalSelection() {
    // TODO: do we still need this?
    // const selection = this.term.getSelection();
    // clipboard.writeText(selection);
    // this.toast.messageInfo({ content: '已复制Shell选择内容到剪切板' });
  }

  private onTerminalResize() {
    // TODO: do we still need this?
  }

  private getPing$() {
    if (!this.connected) {
      return EMPTY;
    }

    return interval(PING_INTERVAL).pipe(publishRef());
  }
}
