import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { ShellComponent } from './shell/shell.component';
import { TerminalComponent } from './terminal.component';
import { TerminalRoutingModule } from './terminal.routing.module';

@NgModule({
  imports: [SharedModule, TerminalRoutingModule],
  declarations: [TerminalComponent, ShellComponent],
})
export class TerminalModule {}
