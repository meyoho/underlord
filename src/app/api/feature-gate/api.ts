import {
  API_GATEWAY,
  KubernetesResource,
  KubernetesResourceList,
} from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

// http://confluence.alauda.cn/pages/viewpage.action?pageId=54853992
@Injectable({
  providedIn: 'root',
})
export class FeatureGateApi {
  constructor(private readonly http: HttpClient) {}

  getGlobalGateList() {
    return this.http.get<KubernetesResourceList<FeatureGate>>(
      `${API_GATEWAY}/fg/v1/featuregates`,
    );
  }

  getGlobalGate(name: string) {
    return this.http.get<FeatureGate>(
      `${API_GATEWAY}/fg/v1/featuregates/${name}`,
    );
  }

  getClusterGateList(cluster: string) {
    return this.http.get<KubernetesResourceList<FeatureGate>>(
      `${API_GATEWAY}/fg/v1/${cluster}/featuregates`,
    );
  }

  getClusterGate(cluster: string, name: string) {
    return this.http.get<FeatureGate>(
      `${API_GATEWAY}/fg/v1/${cluster}/featuregates/${name}`,
    );
  }
}

export function toMap(items: FeatureGate[]): Record<string, boolean> {
  return items.reduce(
    (acc, curr) => ({
      ...acc,
      [curr.metadata.name]: curr.status.enabled,
    }),
    {},
  );
}

export interface FeatureGate extends KubernetesResource {
  spec: {
    dependency: {
      type: DependencyType;
      featureGates: string[];
    };
    description: string;
    enabled: boolean;
    stage: FeatureStage;
  };
  status: {
    enabled: boolean;
  };
}

export enum DependencyType {
  Any = 'any',
  All = 'all',
}

export enum FeatureStage {
  Alpha = 'Alpha',
  Beta = 'Beta',
  GA = 'GA',
  EOF = 'EOF',
}
