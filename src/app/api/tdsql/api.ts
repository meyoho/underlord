import { API_GATEWAY } from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { get } from 'lodash-es';

import { ProductCRD } from 'app/typings';

import {
  TdsqlDeployCR,
  TdsqlLogType,
  TdsqlLogs,
} from './../../manage-platform/features/product-center/middleware/tdsql/types';

const tdsqlapi = `${API_GATEWAY}/apis/product.alauda.io/v1alpha1/tdsqldeploys`;

@Injectable({ providedIn: 'root' })
export class TdsqlApiService {
  constructor(private readonly http: HttpClient) {}

  getTdsqlDeploy() {
    return this.http.get<{
      crd: ProductCRD;
      crs: TdsqlDeployCR[];
      error: string;
    }>(`${API_GATEWAY}/auth/v1/products/tdsqldeploys.product.alauda.io`);
  }

  createTdsqlDeploy(td: TdsqlDeployCR) {
    return this.http.post(`${tdsqlapi}`, td);
  }

  updateTdsqlDeploy(td: TdsqlDeployCR) {
    const name = get(td, 'metadata.name');
    return this.http.put(`${tdsqlapi}/${name}`, td);
  }

  deleteTdsqlDeploy() {
    return this.http.delete(tdsqlapi);
  }

  getTdsqlLogs(type: TdsqlLogType) {
    return this.http.get<TdsqlLogs>(`${API_GATEWAY}/tdsql/v1alpha1/logs`, {
      params: { type },
    });
  }

  getLog(name: string, namespace: string) {
    return this.http.get(
      `{{API_GATEWAY}}/api/v1/namespaces/${namespace}/pods/${name}/log`,
      {
        responseType: 'text',
      },
    );
  }
}
