import {
  API_GATEWAY,
  FALSE,
  K8sApiService,
  NOTIFY_ON_ERROR_HEADER,
  publishRef,
} from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map, startWith } from 'rxjs/operators';

import { ClusterFeature } from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';
export interface Metric {
  metric?: {
    container_name?: string;
    pod_name?: string;
    __query_id__?: string;
    metric_name?: string;
    unit?: string;
    instance?: string;
    device?: string;
  };
  values?: Array<Array<string | number>>;
  value?: number[];
}

export interface MetricQuery {
  start?: number;
  end?: number;
  step?: number;
  time?: number;
  queries?: MetricQueries[];
}

export interface MetricQueries {
  aggregator: string;
  level?: string;
  range?: number;
  labels?: MetricLabels[];
  group_by?: string;
  id?: string;
}

export interface MetricLabels {
  type?: string;
  name: string;
  value: string;
}

export interface IndicatorType {
  kind: string;
  name: string;
  unit: string;
  string?: string;
  value_type?: string;
  type?: string;
  query?: string;
  alert_enabled?: boolean;
  aggregation_enabled?: boolean;
  annotations?: {
    cn: string;
    en: string;
  };
}

// 监控API说明 http://confluence.alauda.cn/x/4rQHAw
// 监控指标 http://confluence.alauda.cn/x/SIeeAQ

@Injectable({ providedIn: 'root' })
export class MetricService {
  METRICS_URL: string;

  constructor(
    private readonly http: HttpClient,
    private readonly k8sApi: K8sApiService,
  ) {
    this.METRICS_URL = `${API_GATEWAY}/v1/metrics/`;
  }

  getIndicators(cluster = 'global'): Observable<IndicatorType[]> {
    const endpoint = `${this.METRICS_URL}${cluster}/indicators`;
    return this.http
      .get(endpoint)
      .pipe(
        map((result: IndicatorType[]) =>
          [...result].sort((a, b) => ('' + a.name).localeCompare(b.name)),
        ),
      );
  }

  queryMetric(clusterName: string, payload: MetricQuery) {
    return this.http
      .post(`${this.METRICS_URL}${clusterName}/query`, payload, {
        headers: {
          [NOTIFY_ON_ERROR_HEADER]: FALSE,
        },
      })
      .toPromise();
  }

  queryMetrics(clusterName: string, payload: MetricQuery) {
    return this.http
      .post(`${this.METRICS_URL}${clusterName}/query_range`, payload, {
        headers: {
          [NOTIFY_ON_ERROR_HEADER]: FALSE,
        },
      })
      .toPromise();
  }

  getPrometheusMetrics(clusterName = 'global'): Observable<any> {
    return this.http
      .get<any>(
        `${this.METRICS_URL}${clusterName}/prometheus/label/__name__/values`,
      )
      .pipe(map(({ data }) => data || []));
  }

  getPrometheusMetricLabels(
    clusterName = 'global',
    query: string,
  ): Observable<any[]> {
    return this.http
      .get<any>(`${this.METRICS_URL}${clusterName}/prometheus/query`, {
        params: {
          query,
          time: '' + Math.floor(Date.now() / 1000),
        },
      })
      .pipe(
        map(({ data }) => (Array.isArray(data) ? data : data.result) || []),
      );
  }

  canUseMetric(cluster = 'global') {
    return this.k8sApi
      .getResourceList<ClusterFeature>({
        type: RESOURCE_TYPES.FEATURE,
        cluster,
        queryParams: {
          labelSelector: 'instanceType=prometheus',
        },
      })
      .pipe(
        map(features => {
          return !!features.items.length;
        }),
        startWith(true),
        catchError(() => of(false)),
        publishRef(),
      );
  }
}
