import { API_GATEWAY } from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { MetricQueries } from 'app/api/alarm/metric.service';
export interface Alarm {
  compare?: string;
  description?: string;
  display_name?: string;
  group_name?: string;
  label?: {
    label_name: string;
  };
  namespace?: string;
  metric_name?: string;
  metric?: {
    step?: number;
    queries?: MetricQueries[];
  };
  name?: string;
  notifications?: Array<{ name: string }>;
  queries?: MetricQueries[];
  resource_name?: string;
  scale_up?: {
    name: string;
    namespace: string;
  };
  scale_down?: {
    name: string;
    namespace: string;
  };
  state?: string;
  summary?: string;
  threshold?: number;
  type?: string;
  wait?: number;
  labels?: {
    severity?: string;
    [key: string]: string;
  };
  severity?: string;
  annotations?: {};
  level?: string;
  unit?: string;
  expr?: string;
  query?: string;
  kind?: string;
  metric_tooltip?: string;
  created_at?: string;
  updated_at?: string;
}

@Injectable({ providedIn: 'root' })
export class AlarmService {
  ALERT_CONFIG_URL: string;

  constructor(private readonly http: HttpClient) {
    this.ALERT_CONFIG_URL = `${API_GATEWAY}/v1/config/alerts`;
  }

  getPrometheusFunctions() {
    return this.http
      .get<{ prometheus_functions: string[] }>(this.ALERT_CONFIG_URL)
      .pipe(
        map(_ => _.prometheus_functions || []),
        catchError(() => of(['sum', 'rate', 'irate'])),
      );
  }
}
