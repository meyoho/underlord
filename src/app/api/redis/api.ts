import { API_GATEWAY, K8sUtilService } from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { REDIS_CRD_NAME } from 'app/manage-platform/features/product-center/middleware/redis/constants';
import {
  HostResponse,
  RedisCR,
} from 'app/manage-platform/features/product-center/middleware/redis/types';
import { createEvent } from 'app/manage-platform/features/product-center/middleware/utils';
import { ProductCRD } from 'app/typings';

@Injectable()
export class RedisApiService {
  constructor(
    private readonly http: HttpClient,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  getProduct(name: string) {
    return this.http.get<{ crd: ProductCRD; crs: RedisCR[]; error: string }>(
      `${API_GATEWAY}/auth/v1/products/${name}`,
    );
  }

  createRedisDeploy(cr: RedisCR) {
    return this.http.post(
      `${API_GATEWAY}/apis/product.alauda.io/v1alpha1/${REDIS_CRD_NAME}`,
      cr,
    );
  }

  getLog(name: string, namespace: string) {
    return this.http.get(
      `${API_GATEWAY}/api/v1/namespaces/${namespace}/pods/${name}/log`,
      {
        responseType: 'text',
      },
    );
  }

  getHost(params: {
    host: string;
    port: string;
    user: string;
    password?: string;
    key?: string;
    keypass?: string;
  }) {
    return this.http.get<HostResponse>(`${API_GATEWAY}/redis/hostinfo`, {
      params,
    });
  }

  reprovision(cr: RedisCR) {
    cr.spec.event = createEvent('ReProvision');

    return this.http.put(
      `${API_GATEWAY}/apis/product.alauda.io/v1alpha1/${REDIS_CRD_NAME}/${this.k8sUtil.getName(
        cr,
      )}`,
      cr,
    );
  }

  terminate(cr: RedisCR) {
    cr.spec.event = createEvent('Terminate');

    return this.http.put(
      `${API_GATEWAY}/apis/product.alauda.io/v1alpha1/${REDIS_CRD_NAME}/${this.k8sUtil.getName(
        cr,
      )}`,
      cr,
    );
  }
}
