import { Injectable } from '@angular/core';

export abstract class StorageService {
  abstract storage: Storage;

  getItem<T>(key: string): T {
    const item = this.storage.getItem(key);
    try {
      return item ? JSON.parse(item) : null;
    } catch {
      return null;
    }
  }

  setItem<T>(key: string, value: T) {
    if (value == null) {
      this.storage.removeItem(key);
    } else {
      this.storage.setItem(key, JSON.stringify(value));
    }
  }
}

@Injectable({ providedIn: 'root' })
export class LocalStorageService extends StorageService {
  storage = localStorage;
}

@Injectable({ providedIn: 'root' })
export class SessionStorageService extends StorageService {
  storage = sessionStorage;
}
