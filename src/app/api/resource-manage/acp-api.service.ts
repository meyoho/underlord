import {
  API_GATEWAY,
  K8sApiService,
  KubernetesResource,
  StringMap,
} from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, forkJoin, of } from 'rxjs';
import { catchError, map, pluck } from 'rxjs/operators';

import {
  APIResourceList,
  Application,
  ApplicationAddress,
  ApplicationFormModel,
  ContainerLog,
  ContainerLogParams,
  DaemonSet,
  Deployment,
  FederatedAppTemplatesResource,
  HpaConfigResource,
  Job,
  StatefulSet,
  TApp,
  TopologyResponse,
  Workloads,
  WorkspaceDetailParams,
} from 'app/typings';
import { ApplicationHistoryMeta, RESOURCE_TYPES } from 'app/utils';
export interface AppSubmitOption {
  forceUpdate?: boolean;
}

/**
 * 对应后端组件 archon 的高级 API 实现
 * 对应 API Gateway 前缀为 /acp
 * 参考：http://confluence.alaudatech.com/pages/viewpage.action?pageId=48728931
 *      http://confluence.alaudatech.com/pages/viewpage.action?pageId=44663323
 */

const BASE_URL = `${API_GATEWAY}/acp/v1/kubernetes`;
const BASE_RESOURCE_URL = `${API_GATEWAY}/acp/v1/resources`;

export interface Label {
  key: string;
  value: string;
}

export interface NodeLabels {
  [key: string]: Label[];
}

@Injectable({ providedIn: 'root' })
export class AcpApiService {
  constructor(
    private readonly httpClient: HttpClient,
    private readonly k8sApi: K8sApiService,
  ) {}

  getApplicationYaml(params: {
    cluster: string;
    namespace: string;
    name: string;
  }) {
    return this.httpClient.get<KubernetesResource[]>(
      `${BASE_URL}/${params.cluster}/namespaces/${params.namespace}/applications/${params.name}`,
    );
  }

  createApplication(params: {
    payload: ApplicationFormModel;
    cluster: string;
  }) {
    const url = `${BASE_URL}/${params.cluster}/namespaces/${params.payload.metadata.namespace}/applications`;
    return this.httpClient.post(url, params.payload);
  }

  updateApplication(params: {
    payload: ApplicationFormModel;
    cluster: string;
    submitOption?: AppSubmitOption;
  }) {
    const url = `${BASE_URL}/${params.cluster}/namespaces/${params.payload.metadata.namespace}/applications/${params.payload.metadata.name}`;
    return this.httpClient.put(url, params.payload, {
      params: {
        forceUpdatePod:
          params.submitOption && params.submitOption.forceUpdate
            ? 'true'
            : 'false',
      },
    });
  }

  startApplication(params: { resource: Application; cluster: string }) {
    const url = `${BASE_URL}/${params.cluster}/namespaces/${params.resource.metadata.namespace}/applications/${params.resource.metadata.name}/start`;
    return this.httpClient.post(url, null);
  }

  stopApplication(params: { resource: Application; cluster: string }) {
    const url = `${BASE_URL}/${params.cluster}/namespaces/${params.resource.metadata.namespace}/applications/${params.resource.metadata.name}/stop`;
    return this.httpClient.post(url, null);
  }

  getApplicationAddress(params: {
    cluster: string;
    namespace: string;
    app: string;
  }): Observable<ApplicationAddress> {
    return this.httpClient.get(
      `${BASE_URL}/${params.cluster}/namespaces/${params.namespace}/applications/${params.app}/address`,
    );
  }

  rollbackApplication(params: {
    cluster: string;
    namespace: string;
    revision: number;
    app: string;
    user: string;
  }) {
    return this.httpClient.post(
      `${BASE_URL}/${params.cluster}/namespaces/${params.namespace}/applications/${params.app}/rollback`,
      {
        kind: ApplicationHistoryMeta.kind,
        apiVersion: ApplicationHistoryMeta.apiVersion,
        metadata: {
          name: params.app,
          namespace: params.namespace,
        },
        spec: {
          revision: params.revision,
          user: params.user,
        },
      },
    );
  }

  federalizeApplication({
    cluster,
    namespace,
    name,
  }: {
    cluster: string;
    namespace: string;
    name: string;
  }) {
    return this.httpClient.post<FederatedAppTemplatesResource>(
      `${BASE_URL}/${cluster}/namespaces/${namespace}/applications/${name}/federate`,
      null,
    );
  }

  manageResources(
    params: {
      cluster: string;
      namespace: string;
      app: string;
      components: KubernetesResource[];
    },
    type: 'import' | 'export',
  ) {
    return this.httpClient.post(
      `${BASE_URL}/${params.cluster}/namespaces/${params.namespace}/applications/${params.app}/${type}`,
      {
        kind: ApplicationHistoryMeta.kind,
        apiVersion: ApplicationHistoryMeta.apiVersion,
        metadata: {
          name: params.app,
          namespace: params.namespace,
        },
        spec: {
          components: params.components,
        },
      },
    );
  }

  createSnapshot(params: {
    cluster: string;
    namespace: string;
    app: string;
    cause: string;
  }) {
    return this.httpClient.post(
      `${BASE_URL}/${params.cluster}/namespaces/${params.namespace}/applications/${params.app}/snapshot`,
      {
        kind: ApplicationHistoryMeta.kind,
        apiVersion: ApplicationHistoryMeta.apiVersion,
        metadata: {
          name: params.app,
          namespace: params.namespace,
        },
        spec: {
          changeCause: params.cause,
        },
      },
    );
  }

  getContainerLog(params: {
    cluster: string;
    namespace: string;
    pod: string;
    container: string;
    queryParams: ContainerLogParams;
  }) {
    return this.httpClient.get<ContainerLog>(
      `${BASE_URL}/${params.cluster}/namespaces/${params.namespace}/pods/${params.pod}/${params.container}/log`,
      {
        params: params.queryParams as StringMap,
      },
    );
  }

  getTopologyByResource(params: {
    cluster: string;
    namespace: string;
    kind: string;
    name: string;
    depth?: number;
  }) {
    return this.httpClient.get<TopologyResponse>(
      `${BASE_URL}/${params.cluster}/topology/${
        params.namespace
      }/${params.kind.toLowerCase()}/${params.name}`,
      {
        params: params.depth ? { depth: params.depth.toString() } : null,
      },
    );
  }

  manualExecJob(params: WorkspaceDetailParams) {
    return this.httpClient.post<Job>(
      `${BASE_URL}/${params.cluster}/cronjobs/${params.namespace}/${params.name}/exec`,
      {},
    );
  }

  getResourceTypes(cluster: string) {
    return this.httpClient.get<APIResourceList[]>(
      `${API_GATEWAY}/acp/v1/resources/${cluster}/resourcetypes`,
    );
  }

  createResources(cluster: string, resources: KubernetesResource[]) {
    return this.httpClient.post<KubernetesResource[]>(
      `${API_GATEWAY}/acp/v1/resources/${cluster}/resources`,
      resources,
    );
  }

  getHpaResource(params: {
    cluster: string;
    namespace: string;
    workloadType: string;
    workloadName: string;
  }) {
    return this.httpClient.get<HpaConfigResource>(
      `${BASE_URL}/${params.cluster}/hpa/${params.namespace}/${params.workloadType}/${params.workloadName}`,
    );
  }

  updateHpaResource(params: {
    cluster: string;
    namespace: string;
    workloadType: string;
    workloadName: string;
    payload: HpaConfigResource;
  }) {
    return this.httpClient.put<HpaConfigResource>(
      `${BASE_URL}/${params.cluster}/hpa/${params.namespace}/${params.workloadType}/${params.workloadName}`,
      params.payload,
    );
  }

  // ******** util **********
  getWorkloads(params: {
    cluster: string;
    namespace: string;
  }): Observable<Workloads> {
    const cluster = params.cluster;
    const namespace = params.namespace;
    return forkJoin([
      this.k8sApi
        .getResourceList<Deployment>({
          type: RESOURCE_TYPES.DEPLOYMENT,
          cluster,
          namespace,
        })
        .pipe(
          pluck('items'),
          catchError(() => of([])),
        ),
      this.k8sApi
        .getResourceList<DaemonSet>({
          type: RESOURCE_TYPES.DAEMONSET,
          cluster,
          namespace,
        })
        .pipe(
          pluck('items'),
          catchError(() => of([])),
        ),
      this.k8sApi
        .getResourceList<StatefulSet>({
          type: RESOURCE_TYPES.STATEFULSET,
          cluster,
          namespace,
        })
        .pipe(
          pluck('items'),
          catchError(() => of([])),
        ),
      this.k8sApi
        .getResourceList<TApp>({
          type: RESOURCE_TYPES.TAPP,
          cluster,
          namespace,
        })
        .pipe(
          pluck('items'),
          catchError(() => of([])),
        ),
    ]).pipe(
      // tslint:disable-next-line: no-shadowed-variable
      map(([Deployment, DaemonSet, StatefulSet, TApp]) => ({
        Deployment,
        DaemonSet,
        StatefulSet,
        TApp,
      })),
    );
  }

  getClusterNodesLabels(clusterName: string) {
    return this.httpClient.get<NodeLabels>(
      `${BASE_RESOURCE_URL}/${clusterName}/nodes/labels`,
    );
  }

  /**
   *
   * @param nodeLabels
   * @param labelStrings
   * labelStrings 之间为 and 关系
   */
  checkNodeByLabels(nodeLabels: NodeLabels, labelStrings: Label[]) {
    if (!labelStrings || labelStrings.length === 0 || !nodeLabels) {
      return [];
    }
    const nodeLabelsArray = Object.entries(nodeLabels);
    if (nodeLabelsArray.length === 0) {
      return [];
    }
    const nodes = new Set<string>();
    nodeLabelsArray.forEach(([node, labels]) => {
      const match = labelStrings.every(
        label =>
          labels.findIndex(
            t => t.key === label.key && t.value === label.value,
          ) >= 0,
      );
      if (match) {
        nodes.add(node);
      }
    });
    return [...nodes];
  }
}
