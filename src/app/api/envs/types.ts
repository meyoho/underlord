export interface Environments {
  ALAUDA_AUDIT_TTL?: string;
  LOGO_URL?: string;
  LABEL_BASE_DOMAIN?: string;
  GLOBAL_NAMESPACE?: string;
  TDSQL_DEPLOYMENT_MODE?: string;
  LOG_PROXY_PREFIX?: string;
}
