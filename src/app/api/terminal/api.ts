import {
  K8sPermissionService,
  K8sResourceAction,
  StringMap,
} from '@alauda/common-snippet';
import { APP_BASE_HREF } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';

import { RESOURCE_TYPES } from 'app/utils';

@Injectable({ providedIn: 'root' })
export class TerminalService {
  constructor(
    private readonly k8sPermission: K8sPermissionService,
    @Inject(APP_BASE_HREF) private readonly appBaseHref: string,
  ) {}

  getPermissions({
    cluster,
    namespace,
  }: {
    cluster: string;
    namespace: string;
  }) {
    return this.k8sPermission.isAllowed({
      type: RESOURCE_TYPES.PODS_EXEC,
      cluster,
      namespace,
      action: [K8sResourceAction.CREATE],
    });
  }

  openTerminal(queryParams: StringMap) {
    // Following is to center the new window.
    const w = 800;
    const h = 600;

    const dualScreenLeft = window.screenLeft || window.screenX;
    const dualScreenTop = window.screenTop || window.screenY;

    const width =
      window.innerWidth || document.documentElement.clientWidth || screen.width;
    const height =
      window.innerHeight ||
      document.documentElement.clientHeight ||
      screen.height;

    const left = width / 2 - w / 2 + dualScreenLeft;
    const top = height / 2 - h / 2 + dualScreenTop;

    window.open(
      `${this.appBaseHref}terminal?${new HttpParams({
        fromObject: queryParams,
      })}`,
      '_blank',
      `width=${w},height=${h},resizable=yes,left=${left},top=${top}`,
    );
  }
}
