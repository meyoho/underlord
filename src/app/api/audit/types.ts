export interface AuditDetail {
  param: string;
}

export interface AuditParam {
  page: number;
  page_size: number;
  start_time: number;
  end_time: number;
  project_name?: string;
  user_name?: string;
  operation_type?: string;
  resource_name?: string;
  resource_type?: string;
  operation_result?: string;
}

export interface Audit {
  Cluster: string;
  StageTimestamp: string;
  ReferObject: string;
  AuditID: string;
  RequestURI: string;
  Verb: string;
  User: {
    Username: string;
    Uid: string;
    Groups: string[];
  };
  SourceIPs: string[];
  ObjectRef: {
    Resource: string;
    Namespace: string;
  };
  ResponseStatus: {
    code: number;
  };
}

export interface Audits {
  Items: Audit[];
  total_pages: number;
  total_items: number;
}

export interface AuditTypes {
  code: number;
  Items: string[];
}
