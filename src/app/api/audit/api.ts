import { API_GATEWAY } from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { AuditTypes, Audits } from './types';

const AUDIT_ENDPOINT = `${API_GATEWAY}/v1/kubernetes-audits`;

@Injectable()
export class AuditApi {
  constructor(private readonly http: HttpClient) {}

  getAudits(params: any): Observable<Audits> {
    return this.http.get<Audits>(AUDIT_ENDPOINT, {
      params,
    });
  }

  getAuditTypes(params: any): Observable<AuditTypes> {
    return this.http.get<AuditTypes>(`${AUDIT_ENDPOINT}/types`, {
      params,
    });
  }
}
