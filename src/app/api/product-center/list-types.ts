// tslint:disable: max-union-size
export interface ProductStatus {
  type:
    | 'NotDeployed'
    | 'InDeployment'
    | 'Running'
    | 'DeploymentFailure'
    | 'RunningAbnormal'
    | 'Deleting'
    | 'DeleteFailure'
    | 'DeploymentSucceed'
    | 'SystemError';
  icon: string;
  text: string;
}

export interface Metrics {
  status: string;
  data: {
    resultType: string;
    result: Metric[];
  };
}

export interface Metric {
  metric: {
    __name__?: string;
    instance?: string;
    job?: string;
    metricname?: string;
    product?: string;
    version?: string;
  };
  value: [number, string];
}
