import {
  API_GATEWAY,
  K8sUtilService,
  TranslateService,
} from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import {
  PRODUCT_API_VERSION,
  ProductStatusMap,
} from 'app/manage-platform/features/product-center/constants';
import {
  mapDeployStatus,
  mapOperatingStatus,
  mapStatus,
} from 'app/manage-platform/features/product-center/utils';
import {
  ProductCR,
  ProductCRD,
  ProductCenter,
  ProductCenterList,
} from 'app/typings';

import { Metrics, ProductStatus } from './list-types';

export const PRODUCT_URL = `${API_GATEWAY}/auth/v1`;
export const PRODUCT_APIS = `${API_GATEWAY}/apis`;
const PRODUCT_METRIC = `${API_GATEWAY}/v1/metrics/global/prometheus/query?query=product_metric`;

@Injectable({ providedIn: 'root' })
export class ProductCenterListApi {
  constructor(
    private readonly http: HttpClient,
    private readonly k8sUtil: K8sUtilService,
    private readonly translate: TranslateService,
  ) {}

  getProducts(): Observable<ProductCenterList> {
    return this.http.get<ProductCenterList>(`${PRODUCT_URL}/products`);
  }

  getProduct(crdName: string): Observable<ProductCenter> {
    return this.http.get<ProductCenter>(`${PRODUCT_URL}/products/${crdName}`);
  }

  postProduct(crd: ProductCRD) {
    return this.http.post(
      `${PRODUCT_APIS}/${this.handleProductType(crd)}`,
      this.createProductCr(crd),
    );
  }

  upgradeProduct(crd: ProductCRD, payload: ProductCR) {
    return this.http.put(
      `${PRODUCT_APIS}/${this.handleProductType(crd)}/${this.k8sUtil.getName(
        payload,
      )}`,
      payload,
    );
  }

  deleteProduct(crd: ProductCRD, payload: ProductCR) {
    return this.http.delete(
      `${PRODUCT_APIS}/${this.handleProductType(crd)}/${this.k8sUtil.getName(
        payload,
      )}`,
    );
  }

  getProductMetric(): Observable<Metrics> {
    return this.http.get<Metrics>(PRODUCT_METRIC);
  }

  private handleProductType(crd: ProductCRD) {
    return `${crd.spec.group}/${crd.spec.version}/${crd.spec.names.plural}`;
  }

  getResourcesTranslate(res: ProductCRD, type: string) {
    const data = this.getTranslateData(res, type);
    try {
      return this.translate.get(JSON.parse(data), {}, true);
    } catch (error) {
      return this.translate.get(data, {}, true);
    }
  }

  private getTranslateData(res: ProductCRD, type: string) {
    switch (type) {
      case 'displayName':
        return this.k8sUtil.getDisplayName(res);
      case 'description':
        return this.k8sUtil.getDescription(res);
      case 'brief':
        return this.k8sUtil.getAnnotation(res, 'brief');
      default:
        return '';
    }
  }

  private createProductCr(crd: ProductCRD) {
    return {
      apiVersion: PRODUCT_API_VERSION,
      kind: crd.spec.names.kind,
      metadata: {
        name: crd.spec.names.plural,
      },
      spec: {
        version: this.k8sUtil.getLabel(crd, 'available-version'),
      },
    };
  }
}

export const handleStatus = (item: ProductCenter): ProductStatus => {
  const crs = item.crs || [];
  const crNum = crs.length;
  if (crNum !== 1) {
    return crNum > 1
      ? ProductStatusMap.SystemError
      : ProductStatusMap.NotDeployed;
  }
  return mapStatus(crs[0]);
};

export const handleOperatingStatus = (item: ProductCenter): ProductStatus => {
  const crs = item.crs || [];
  return mapOperatingStatus(crs[0]);
};

export const handleDeployStatus = (item: ProductCenter): ProductStatus => {
  const crs = item.crs || [];
  const crNum = crs.length;
  if (crNum !== 1) {
    return crNum > 1
      ? ProductStatusMap.SystemError
      : ProductStatusMap.NotDeployed;
  }
  return mapDeployStatus(crs[0]);
};
