import {
  API_GATEWAY,
  FALSE,
  KubernetesResourceList,
  NOTIFY_ON_ERROR_HEADER,
  ResourceListParams,
} from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { CfcMeta, License } from './types';

const LICENSE_PUBLIC_URL = `${API_GATEWAY}/lic/v1/licenses`;

@Injectable({ providedIn: 'root' })
export class LicenseApi {
  constructor(private readonly http: HttpClient) {}

  getCfcInfo() {
    return this.http.get<CfcMeta>(`${API_GATEWAY}/lic/v1/metadata`);
  }

  getLicenseList(params: ResourceListParams) {
    return this.http.get<KubernetesResourceList<License>>(LICENSE_PUBLIC_URL, {
      params,
    });
  }

  importLicense(license: string) {
    return this.http.post<License>(LICENSE_PUBLIC_URL, license, {
      headers: {
        'Content-Type': 'application/octet-stream',
        [NOTIFY_ON_ERROR_HEADER]: FALSE,
      },
    });
  }

  deleteLicense(license: License) {
    return this.http.delete<License>(
      `${LICENSE_PUBLIC_URL}/${license.metadata.name}`,
    );
  }
}
