import { KubernetesResource } from '@alauda/common-snippet';

export interface License extends KubernetesResource {
  spec: LicenseSpec;
}
export interface LicenseSpec {
  products: Array<Record<'name', string>>;
  validity: LicenseValidity;
  enabled: boolean;
}

interface LicenseValidity {
  notBefore?: string;
  notAfter?: string;
}

export enum LicenseStatus {
  Unauthorized = 'unauthorized',
  Expired = 'expired',
  Existed = 'existed',
  Activated = '',
}

export interface CfcMeta {
  name: string;
  cfc: string;
}
