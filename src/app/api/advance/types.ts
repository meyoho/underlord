export interface Diagnose {
  details: Array<{
    name: string;
    status: string;
    message: string;
  }>;
}
