import {
  API_GATEWAY,
  DISPLAY_NAME,
  K8sUtilService,
  KubernetesResourceList,
  NAME,
  PROJECT,
  ResourceListParams,
  StringMap,
  ifExist,
  skipError,
} from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { cloneDeep, get } from 'lodash-es';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  NamespaceActionFormData,
  NamespaceActionSubmitData,
} from 'app/manage-project/features/namespace/action/namespace-action.component';
import { HOSTNAME, OVERRIDE, PREFIXES } from 'app/services/k8s-util.service';
import {
  AvailableResource,
  FederatedNamespace,
  Group,
  MeterSummary,
  MeterTop,
  Namespace,
  Report,
  RoleRuleGroup,
  Status,
  User,
  UserBinding,
  UserRole,
} from 'app/typings';
import {
  ArchiveLogMeta,
  DEFAULT,
  MeterReportMeta,
  RESOURCE_TYPES,
  getYamlApiVersion,
} from 'app/utils';

import { Diagnose } from './types';

export const ACP_URL = `${API_GATEWAY}/acp/v1/kubernetes/`;
export const AUTH_URL = `${API_GATEWAY}/auth/v1`;
export const METER_URL = `${API_GATEWAY}/v1/meter`;
export const DIAGNOSE_URL = `${API_GATEWAY}/morgans/_diagnose`;
export const METER_REPORT_URL = `${API_GATEWAY}/apis/${MeterReportMeta.apiVersion}/reports/`;
export const LOG_REPORT_URL = `${API_GATEWAY}/apis/${ArchiveLogMeta.apiVersion}/archivelogs/`;

@Injectable({ providedIn: 'root' })
export class AdvanceApi {
  constructor(
    private readonly http: HttpClient,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  getNamespaces({ project, cluster, ...params }: ResourceListParams) {
    return this.http.get<KubernetesResourceList<Namespace>>(
      `${AUTH_URL}/projects/${project}/clusters/${cluster}/namespaces`,
      {
        params,
      },
    );
  }

  getFederatedNamespaces({ project, cluster, ...params }: ResourceListParams) {
    return this.http.get<KubernetesResourceList<FederatedNamespace>>(
      `${AUTH_URL}/projects/${project}/clusters/${cluster}/federatednamespaces`,
      {
        params,
      },
    );
  }

  editNamespace(formData: NamespaceActionFormData, isCreate: boolean) {
    const {
      basicInfo: { clusterName, _name, name = _name },
    } = formData;
    return this.http[isCreate ? 'post' : 'put']<Namespace>(
      `${ACP_URL}${clusterName}/general-namespaces${ifExist(
        !isCreate,
        '/' + name,
      )}`,
      this.formDataToSubmitData(formData),
    );
  }

  editFederatedNamespace(formData: NamespaceActionFormData, isCreate: boolean) {
    const { basicInfo } = formData;
    return this.http[isCreate ? 'post' : 'put']<FederatedNamespace>(
      `${ACP_URL}${basicInfo.hostClusterName}/federatednamespaces${ifExist(
        !isCreate,
        '/' + basicInfo.name,
      )}`,
      this.federatedFormDataToSubmitData(formData),
      {
        params: {
          project_name: basicInfo.projectName,
        },
      },
    );
  }

  filterUsers(params: StringMap) {
    return this.http.get<KubernetesResourceList<User>>(`${AUTH_URL}/users`, {
      params,
    });
  }

  getRulesByRoleName(name: string) {
    return this.http.get<KubernetesResourceList<RoleRuleGroup>>(
      `${AUTH_URL}/roles/${name}/rules`,
    );
  }

  getUserInfo(name: string) {
    return this.http.get<User>(`${AUTH_URL}/users/${name}`);
  }

  createUser(user: User) {
    return this.http.post<User>(`${AUTH_URL}/users`, user);
  }

  patchUser(username: string, part: User) {
    return this.http.patch(`${AUTH_URL}/users/${username}`, part);
  }

  deleteUser(username: string) {
    return this.http.delete(`${AUTH_URL}/users/${username}`);
  }

  createUserbinding(payload: UserBinding) {
    return this.http.post(`${AUTH_URL}/userbindings`, payload);
  }

  deleteUserbinding(name: string) {
    return this.http.delete(`${AUTH_URL}/userbindings/${name}`);
  }

  deleteUsers(names: string[], range: string) {
    return this.http.request('delete', `${AUTH_URL}/users/`, {
      body: names,
      params: {
        range,
      },
    });
  }

  getUsersRoles(users: string) {
    return this.http.get<KubernetesResourceList<UserRole>>(
      `${AUTH_URL}/usersroles/`,
      {
        params: {
          users,
        },
      },
    );
  }

  getGroups() {
    return this.http.get<KubernetesResourceList<Group>>(`${AUTH_URL}/groups`);
  }

  syncUser(type: string) {
    return this.http.put<Status>(
      `${AUTH_URL}/users/sync`,
      {},
      {
        params: {
          type,
        },
      },
    );
  }

  getAvailableResource() {
    return this.http.get<KubernetesResourceList<AvailableResource>>(
      `${AUTH_URL}/projects/available-resources`,
    );
  }

  private formDataToSubmitData(
    formData: NamespaceActionFormData,
  ): NamespaceActionSubmitData {
    const {
      basicInfo: { _name, name = _name },
      annotations,
      labels,
      resourcequota,
      limitrange,
    }: NamespaceActionFormData = cloneDeep(formData);
    return {
      namespace: {
        apiVersion: getYamlApiVersion(RESOURCE_TYPES.NAMESPACE),
        kind: 'Namespace',
        metadata: {
          name,
          annotations,
          labels,
        },
      },
      resourcequota,
      limitrange,
    };
  }

  federatedFormDataToSubmitData({
    basicInfo,
    diffConfig,
    resourcequota,
    limitrange,
    resourceConfigs,
  }: NamespaceActionFormData): FederatedNamespace {
    const metadata = {
      name: basicInfo.name,
      annotations: {
        [this.k8sUtil.normalizeType(DISPLAY_NAME)]: basicInfo.displayName,
      },
      labels: {
        [this.k8sUtil.normalizeType(PROJECT)]: basicInfo.projectName,
        [this.k8sUtil.normalizeType(
          NAME,
          PREFIXES.FEDERATION,
        )]: basicInfo.clusterFedName,
        [this.k8sUtil.normalizeType(
          HOSTNAME,
          PREFIXES.FEDERATION,
        )]: basicInfo.hostClusterName,
        [this.k8sUtil.normalizeType(OVERRIDE, PREFIXES.FEDERATION)]: String(
          diffConfig,
        ),
      },
    };
    return {
      kind: 'FederatedNamespace',
      apiVersion: getYamlApiVersion(RESOURCE_TYPES.FEDERATED_NAMESPACE),
      metadata,
      spec: {
        namespace: {
          resource: {
            kind: 'Namespace',
            apiVersion: getYamlApiVersion(RESOURCE_TYPES.NAMESPACE),
            metadata,
          },
        },
        resourcequota: {
          resource: {
            kind: 'ResourceQuota',
            apiVersion: getYamlApiVersion(RESOURCE_TYPES.RESOURCE_QUOTA),
            metadata: {
              name: DEFAULT,
              namespace: basicInfo.name,
            },
            spec: {
              hard: get(resourcequota, ['spec', 'hard'], {}),
            },
          },
          overrides: diffConfig
            ? resourceConfigs.map(({ clusterName, resourcequota }) => ({
                clusterName,
                clusterOverrides: [
                  {
                    path: '/spec/hard',
                    value: get(resourcequota, ['spec', 'hard']),
                  },
                ],
              }))
            : undefined,
        },
        limitrange: {
          resource: {
            kind: 'LimitRange',
            apiVersion: getYamlApiVersion(RESOURCE_TYPES.LIMIT_RANGE),
            metadata: {
              name: DEFAULT,
              namespace: basicInfo.name,
            },
            spec: {
              limits: get(limitrange, ['spec', 'limits'], [{}]),
            },
          },
          overrides: diffConfig
            ? resourceConfigs.map(({ clusterName, limitrange }) => ({
                clusterName,
                clusterOverrides: (get(limitrange, ['spec', 'limits']) || [])
                  .flatMap((limit, index) => ({
                    path: `/spec/limits/${index}`,
                    value: limit,
                  }))
                  .filter(Boolean),
              }))
            : undefined,
        },
      },
    };
  }

  deleteRoleTemplate(name: string) {
    return this.http.delete(`${AUTH_URL}/roletemplates/${name}`);
  }

  getMeterTopN(params: StringMap): Observable<MeterTop> {
    return this.http.get<MeterTop>(`${METER_URL}/topn`, {
      params,
    });
  }

  getMeterTotal({
    type = 'podUsage',
    groupBy = 'project',
    startTime = '',
    endTime = '',
    project = '',
    cluster = '',
    namespace = '',
    classifyBy = 'month',
  }): Observable<MeterSummary> {
    return this.http.get<MeterSummary>(`${METER_URL}/total`, {
      params: {
        type,
        groupBy,
        startTime,
        endTime,
        project,
        cluster,
        namespace,
        classifyBy,
      },
    });
  }

  getMeterSummary({
    type = 'podUsage',
    groupBy = 'project',
    startTime = '',
    endTime = '',
    project = '',
    page = '1',
    pageSize = '20',
    orderBy = 'name',
  }): Observable<MeterSummary> {
    return this.http.get<MeterSummary>(`${METER_URL}/summary`, {
      params: {
        type,
        groupBy,
        startTime,
        endTime,
        project,
        page,
        pageSize,
        orderBy,
      },
    });
  }

  getMeterDetail({
    type = 'podUsage',
    groupBy = 'namespace',
    startTime = '',
    endTime = '',
    project = '',
    cluster = '',
    namespace = '',
    page = '1',
    pageSize = '20',
    orderBy = 'name',
  }): Observable<MeterSummary> {
    return this.http.get<MeterSummary>(`${METER_URL}/query`, {
      params: {
        type,
        groupBy,
        startTime,
        endTime,
        project,
        cluster,
        namespace,
        page,
        pageSize,
        orderBy,
      },
    });
  }

  getDiagnoseInfo() {
    return this.http.get(DIAGNOSE_URL).pipe(
      map((res: Diagnose) => {
        const esStatus = res.details.find(
          detail => detail.name === 'elasticsearch',
        );
        return (
          esStatus?.status === 'ok' &&
          ['green', 'yellow'].includes(esStatus?.message)
        );
      }),
      skipError(),
    );
  }

  getMeterReportInfo() {
    return this.http.get(DIAGNOSE_URL).pipe(
      map((res: Diagnose) => {
        const esStatus = res.details.find(
          detail => detail.name === 'meterReport',
        );
        return esStatus?.status === 'ok';
      }),
      skipError(),
    );
  }

  clearReportStatus(report: Report, type = 'meter') {
    return this.http.put<Report>(
      `${
        type === 'meter' ? METER_REPORT_URL : LOG_REPORT_URL
      }${this.k8sUtil.getName(report)}/status`,
      {
        ...report,
        status: {},
      },
    );
  }
}
