import {
  K8sApiService,
  K8sUtilService,
  KubernetesResourceList,
  PROJECT,
  ResourceListParams,
  matchExpressionsToString,
  skipError,
} from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { get } from 'lodash-es';
import { map, pluck } from 'rxjs/operators';

import { PREFIXES } from 'app/services/k8s-util.service';
import { ClusterFed, K8sResourceStatus, Project } from 'app/typings';
import { PERMISSION_PROJECT_URL, RESOURCE_TYPES } from 'app/utils';

@Injectable({ providedIn: 'root' })
export class ProjectApiService {
  private readonly projectLabelEnabled = false;

  constructor(
    private readonly http: HttpClient,
    private readonly k8sApi: K8sApiService,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  getProjects(params: ResourceListParams) {
    return this.http.get<KubernetesResourceList<Project>>(
      PERMISSION_PROJECT_URL,
      {
        params,
      },
    );
  }

  getClusterFeds(project: Project) {
    const projectName = this.k8sUtil.getName(project);
    const projectLabel = this.k8sUtil.normalizeType(
      projectName,
      [PROJECT, PREFIXES.AUTH].join('.'),
    );
    const projectClusters = get(project, ['spec', 'clusters']) || [];
    /**
     * try to get all cluster federations related to a project,
     * but they can also be not related this project at all,
     * so a further filter action is required
     */
    return this.k8sApi
      .getGlobalResourceList<ClusterFed>({
        type: RESOURCE_TYPES.CLUSTER_FED,
        queryParams: this.projectLabelEnabled
          ? {
              labelSelector: matchExpressionsToString([
                {
                  key: projectLabel,
                  operator: '=',
                  values: [projectName],
                },
                {
                  key: projectLabel,
                  operator: 'doesnotexist',
                },
              ]),
            }
          : null,
      })
      .pipe(
        skipError(),
        pluck('items'),
        map((clusterFeds: ClusterFed[]) =>
          clusterFeds.filter(
            clusterFed =>
              clusterFed.status.phase === K8sResourceStatus.ACTIVE &&
              clusterFed.spec.clusters.find(({ name: clusterName }) =>
                projectClusters.find(
                  ({ name: projectCluster, type }) =>
                    type === 'FedMember' && clusterName === projectCluster,
                ),
              ),
          ),
        ),
      );
  }
}
