import { K8sQuotaItem, ProjectQuota } from 'app/typings';

export interface Cluster {
  name?: string;
  fedName?: string;
  displayName?: string;
  quota?: K8sQuotaItem;
  limit?: K8sQuotaItem;
  hard?: K8sQuotaItem;
  capacity?: K8sQuotaItem;
  used?: K8sQuotaItem;
  type?: string;
  isNormal?: boolean;
}

export interface FedCluster extends Cluster {
  clusters?: Array<{
    name: string;
    type?: string;
    isNormal?: boolean;
  }>;
  check?: boolean;
  disabledCheck?: boolean;
}

export interface ClusterQuotaMap {
  [key: string]: {
    quota?: K8sQuotaItem;
    limit?: K8sQuotaItem;
    capacity?: K8sQuotaItem;
    used?: K8sQuotaItem;
    hard?: K8sQuotaItem;
    // ! hack: make sure resourceQuotas item will not change on form input changes
    _quota?: K8sQuotaItem;
    fedHostName?: string;
  };
}

export interface ProjectFormData {
  name: string;
  displayName?: string;
  description?: string;
  uniteQuotaFedClusters?: string;
  clusters?: Cluster[];
}

export const FED_MEMBER = 'FedMember';

export interface QuotaDisplay {
  capacity?: K8sQuotaItem;
  name?: string;
  displayName?: string;
  fedClusterName?: string;
  isHost?: boolean;
  isNormal?: boolean;
  projectQuota?: ProjectQuota;
}

export interface FedQuotaDisplay extends QuotaDisplay {
  name?: string;
  clusters?: QuotaDisplay[];
}
