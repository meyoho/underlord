import {
  KubernetesResourceList,
  TOKEN_GLOBAL_NAMESPACE,
} from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import {
  ClusterNodeResource,
  NodeAddressOption,
  TIMatrixCR,
} from 'app/manage-platform/features/product-center/ti/types';

export const TI_RESOURCE_NAME = 'timatrix';

export function mapNodeAddresses(
  res: KubernetesResourceList<ClusterNodeResource> | ClusterNodeResource[],
): NodeAddressOption[] {
  return ('items' in res ? res.items || [] : res).map(item => {
    return item.status.addresses.reduce((accm, cur) => {
      return {
        ...accm,
        [cur.type.toLowerCase()]: cur.address,
      };
    }, {} as NodeAddressOption);
  });
}

const apiBase = '{{API_GATEWAY}}/apis/product.alauda.io/v1alpha1/timatrixes';

@Injectable()
export class TIApiService {
  constructor(
    @Inject(TOKEN_GLOBAL_NAMESPACE) private readonly globalNamespace: string,
    private readonly http: HttpClient,
  ) {}

  deploy(payload: string | TIMatrixCR) {
    return this.http.post(`${apiBase}/${TI_RESOURCE_NAME}`, payload, {
      headers: {
        'Content-Type':
          typeof payload === 'string' ? 'application/yaml' : 'application/json',
      },
    });
  }

  update(payload: TIMatrixCR) {
    return this.http.put(`${apiBase}/${TI_RESOURCE_NAME}`, payload, {
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }

  getSystemDefaultAddresses() {
    return this.http
      .get(`{{API_GATEWAY}}/api/v1/nodes`, {
        params: {
          labelSelector: 'log=true',
        },
      })
      .pipe(map(mapNodeAddresses));
  }

  getClusterNodes(cluster: string) {
    return this.http.get<KubernetesResourceList<ClusterNodeResource>>(
      `{{API_GATEWAY}}/kubernetes/${cluster}/api/v1/nodes`,
    );
  }

  getEsSecret() {
    return this.http.get(
      `{{API_GATEWAY}}/api/v1/namespaces/${this.globalNamespace}/secrets/cpaas-es`,
    );
  }

  getLog(name: string, namespace: string) {
    return this.http.get(
      `{{API_GATEWAY}}/api/v1/namespaces/${namespace}/pods/${name}/log`,
      {
        responseType: 'text',
      },
    );
  }
}
