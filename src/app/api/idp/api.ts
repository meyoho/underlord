import {
  API_GATEWAY,
  KubernetesResourceList,
  ResourceListParams,
  TOKEN_GLOBAL_NAMESPACE,
} from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';

import { Idp } from 'app/typings';

@Injectable({ providedIn: 'root' })
export class IdpApi {
  PREFIX_URL = `${API_GATEWAY}/auth/v1/connectors`;

  COMMON_PREFIX_URL = `${API_GATEWAY}/apis/dex.coreos.com/v1/namespaces/${this.globalNamespace}/connectors`;

  constructor(
    private readonly http: HttpClient,
    @Inject(TOKEN_GLOBAL_NAMESPACE) private readonly globalNamespace: string,
  ) {}

  createIdp(payload: Idp) {
    return this.http.post(`${this.PREFIX_URL}`, payload);
  }

  updateIdp(payload: Idp) {
    return this.http.put(`${this.PREFIX_URL}/${payload.id}`, payload);
  }

  deleteIdp(id: string, deleteUsers = false) {
    return this.http.delete(
      `${this.PREFIX_URL}/${id}?delete-users=${deleteUsers}`,
    );
  }

  getIdps(params: ResourceListParams) {
    return this.http.get<KubernetesResourceList<Idp>>(
      `${this.COMMON_PREFIX_URL}`,
      {
        params,
      },
    );
  }

  getIdp(name: string) {
    return this.http.get<Idp>(`${this.COMMON_PREFIX_URL}/${name}`);
  }

  getIdpFilter(originFilter: string) {
    let objectType = '';
    let filter = '';
    /*
      ladp filter 的规则(&(objectClass=${class})(${filter}))
      其中class是页面上输入的'对象类型', 必填
      filter是页面上输入的'过滤条件', 可选
      例如1：(&(objectClass=person)(mail=*))
      需要取出'对象类型'：person，'过滤条件': (mail=*)
      1. 首先截取字符串，去除开头'(&' 和 结尾的 ')'
      2. 正则匹配 '(objectClass=' 开头和第一个')' 结尾，取出中间字符串即为 '对象类型' 的值
      3. 使用第二步获取到的对象类型的值拼接正则 '(objectClass=person)' 开头。取余下的字符串即为过滤条件的值
      例如2：(objectClass=person)
      需要取出'对象类型'：person
    */
    if (originFilter && this.isValidFilter(originFilter)) {
      if (originFilter.includes('&')) {
        originFilter = originFilter.slice(2, -1);
        const match = new RegExp(/\(objectClass=(\S*?)\)/).exec(originFilter);
        objectType = match ? match[1] : '';
        const reg = `\\(objectClass=${objectType}\\)(\\S*)`;
        const filterMatch = new RegExp(reg).exec(originFilter);
        filter = filterMatch ? filterMatch[1] : '';
      } else {
        const match = new RegExp(/\(objectClass=(\S*)\)/).exec(originFilter);
        objectType = match ? match[1] : '';
      }
    }
    return { objectType, filter };
  }

  private isValidFilter(filter: string) {
    if (filter.includes('&')) {
      return (
        filter.startsWith('(') &&
        filter.charAt(1) === '&' &&
        filter.endsWith(')')
      );
    }
    return filter.startsWith('(') && filter.endsWith(')');
  }
}
