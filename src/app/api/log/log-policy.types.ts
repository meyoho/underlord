import { KubernetesResource } from '@alauda/common-snippet';
export const POLICY_ENABLED = 'project.esPolicyEnabled';
export const INDICES_KEEP_DAYS = 'project.esIndicesKeepDays';
export interface ClusterPolicy extends KubernetesResource {
  spec: {
    indicesKeepDays: number;
    disabled: boolean;
  };
}

export enum PolicyType {
  SYSTEM = 'system',
  PLATFORM = 'platform',
  KUBERNETES = 'kubernetes',
  WORKLOAD = 'workload',
}
