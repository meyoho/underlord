import {
  K8sApiService,
  KubernetesResourceList,
  ObjectMeta,
  ResourceListParams,
  TOKEN_BASE_DOMAIN,
} from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';

import { Project } from 'app/typings';
import { PERMISSION_PROJECT_URL, RESOURCE_TYPES } from 'app/utils';

import { ClusterPolicy, POLICY_ENABLED } from './log-policy.types';
@Injectable({
  providedIn: 'root',
})
export class LogPolicyService {
  constructor(
    private readonly k8sApi: K8sApiService,
    private readonly http: HttpClient,
    @Inject(TOKEN_BASE_DOMAIN)
    private readonly baseDomain: string,
  ) {}

  getGlobalPolicyList() {
    return this.k8sApi.getGlobalResourceList<ClusterPolicy>({
      type: RESOURCE_TYPES.LOGPOLICISE,
    });
  }

  updateGlobalPoicy(payload: ClusterPolicy) {
    return this.k8sApi.putGlobalResource({
      type: RESOURCE_TYPES.LOGPOLICISE,
      resource: payload,
    });
  }

  getProjectPolicy(params: ResourceListParams) {
    return this.http.get<KubernetesResourceList<Project>>(
      PERMISSION_PROJECT_URL,
      {
        params: {
          ...params,
          labelSelector: `${this.baseDomain}/${POLICY_ENABLED}`,
        },
      },
    );
  }

  updateProjectPolicy(payload: Project, data: { metadata: ObjectMeta }) {
    return this.k8sApi.patchGlobalResource({
      type: RESOURCE_TYPES.PROJECT,
      resource: payload,
      part: data,
    });
  }
}
