import {
  API_GATEWAY,
  K8sUtilService,
  KubernetesResourceList,
  LabelSelectorRequirement,
  matchExpressionsToString,
} from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { isEmpty } from 'lodash-es';
import { of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { PREFIXES, ROLE_NAME, USER_EMAIL } from 'app/services/k8s-util.service';
import { User, UserBinding } from 'app/typings';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';

export const RESOURCE_ADMIN_VALUES: Partial<Record<ResourceType, string[]>> = {
  [RESOURCE_TYPES.NAMESPACE]: ['acp-namespace-admin'],
  [RESOURCE_TYPES.PROJECT]: ['acp-project-admin'],
};

@Injectable({ providedIn: 'root' })
export class AdminApiService {
  USER_URL = `${API_GATEWAY}/apis/auth.alauda.io/v1/users`;
  USERBINDING_URL = `${API_GATEWAY}/apis/auth.alauda.io/v1/userbindings`;

  userEmail = this.k8sUtil.normalizeType(USER_EMAIL, PREFIXES.AUTH);

  constructor(
    private readonly http: HttpClient,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  getAdmins(
    resourceType: ResourceType,
    resources: string | string[],
    ...extraExpressions: LabelSelectorRequirement[]
  ) {
    return this.http
      .get<KubernetesResourceList<UserBinding>>(this.USERBINDING_URL, {
        params: {
          labelSelector: matchExpressionsToString([
            {
              key: this.k8sUtil.normalizeType(resourceType.toLowerCase()),
              operator: 'in',
              values: Array.isArray(resources) ? resources : [resources],
            },
            {
              key: this.k8sUtil.normalizeType(ROLE_NAME, PREFIXES.AUTH),
              operator: '=',
              values: RESOURCE_ADMIN_VALUES[resourceType],
            },
            ...extraExpressions,
          ]),
        },
      })
      .pipe(
        map(({ items }) =>
          items.map(item => item.metadata.labels[this.userEmail]),
        ),
        switchMap(emails => {
          if (!isEmpty(emails)) {
            return this.http.get<KubernetesResourceList<User>>(this.USER_URL, {
              params: {
                labelSelector: `${this.userEmail}+in+(${emails.join(',')})`,
              },
            });
          } else {
            return of({ items: [] });
          }
        }),
        map(res => res.items.map(item => item.spec.email)),
      );
  }
}
