export function b64EncodeUnicode(str: string) {
  // first we use encodeURIComponent to get percent-encoded UTF-8,
  // then we convert the percent encodings into raw bytes which
  // can be fed into btoa.
  return btoa(
    encodeURIComponent(str).replace(/%([\dA-F]{2})/g, (_match, p1) => {
      return String.fromCharCode(Number('0x' + p1));
    }),
  );
}

export function b64DecodeUnicode(str: string) {
  return (
    str &&
    decodeURIComponent(
      atob(str)
        .split('')
        .map(c => {
          return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        })
        .join(''),
    )
  );
}
