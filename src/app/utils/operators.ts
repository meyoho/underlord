import { get } from 'lodash-es';
import { pipe } from 'rxjs';
import { map } from 'rxjs/operators';

export const safePluck = (...properties: Array<string | number>) =>
  pipe(map(source => get(source, properties)));
