import { API_GATEWAY } from '@alauda/common-snippet';

import { isBlank, numToStr } from './commons';

export const PERMISSION_PROJECT_URL = `${API_GATEWAY}/auth/v1/projects`;

const COMMON_UNITS = ['M', 'G', 'T', 'P', 'E'];

export const CORE_UNITS = ['m', '', 'k', ...COMMON_UNITS];

export const CORE_UNIT_REG = new RegExp(`[${CORE_UNITS.join('')}]$`);

const toUnitNumBase = (coreStr?: string | number, isUnitM = false) => {
  if (typeof coreStr === 'number') {
    return numToStr(coreStr);
  }

  if (isBlank(coreStr)) {
    return '';
  }
  coreStr = coreStr.trim();
  const num = +coreStr.replace(CORE_UNIT_REG, '');
  if (Number.isNaN(num)) {
    return '';
  }
  if (num <= 0) {
    return '0';
  }
  const lastChar = coreStr[coreStr.length - 1];
  let unitIndex = CORE_UNITS.indexOf(lastChar);
  unitIndex = (unitIndex === -1 ? 0 : unitIndex - 1) + +isUnitM;
  return numToStr(num * Math.pow(1000, unitIndex));
};

export const toUnitNumM = (coreStr?: string | number) =>
  toUnitNumBase(coreStr, true);

export const toUnitNum = (coreStr?: string | number) => toUnitNumBase(coreStr);

export const UNITS = ['', 'K', ...COMMON_UNITS];

export const UNIT_REG = new RegExp(`[${UNITS.join('')}]i?$`);

export const toUnitI = (str?: string | number, negative = false) => {
  if (typeof str === 'number') {
    if (str <= 0 && !negative) {
      return '0';
    }
    return numToStr(str);
  }

  if (isBlank(str)) {
    return '';
  }
  str = str.trim();
  const num = +str.replace(UNIT_REG, '');
  if (Number.isNaN(num)) {
    return '';
  }
  if (num <= 0 && !negative) {
    return '0';
  }

  let base = 1000;

  if (str.endsWith('i')) {
    base = 1024;
    str = str.replace(/i$/, '');
  }

  const lastChar = str[str.length - 1];
  const unitIndex = UNITS.indexOf(lastChar);
  return numToStr(num * Math.pow(base, unitIndex === -1 ? 0 : unitIndex));
};

// tslint:disable-next-line: bool-param-default
export const toUnitMi = (str?: string | number, negative?: boolean) => {
  const unitI = toUnitI(str, negative);
  if (!unitI) {
    return '';
  }
  return numToStr(+unitI / 1024 / 1024);
};

// tslint:disable-next-line: bool-param-default
export const toUnitGi = (str?: string | number, negative?: boolean) => {
  const unitI = toUnitI(str, negative);
  if (!unitI) {
    return '';
  }
  return numToStr(+unitI / 1024 / 1024 / 1024);
};

export const addUnitCoreM = (coreStr?: string | number) => {
  if (isBlank(coreStr)) {
    return null;
  }
  return numToStr(+coreStr) + 'm';
};

export const addUnitMi = (miStr?: string | number) => {
  if (isBlank(miStr)) {
    return null;
  }
  return numToStr(+miStr) + 'Mi';
};

// tslint:disable-next-line: no-identical-functions
export const addUnitGi = (giStr?: string | number) => {
  if (isBlank(giStr)) {
    return null;
  }
  return numToStr(+giStr) + 'Gi';
};
