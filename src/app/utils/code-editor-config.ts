import { CodeEditorActionsConfig } from '@alauda/code-editor';

export type IEditorConstructionOptions = import('monaco-editor').editor.IEditorConstructionOptions & {
  language?: string;
};

const commonActions: CodeEditorActionsConfig = {
  copy: true,
  find: true,
  export: true,
};

export const createActions: CodeEditorActionsConfig = {
  diffMode: false,
  clear: true,
  recover: false,
  import: true,
  ...commonActions,
};

export const viewActions: CodeEditorActionsConfig = {
  diffMode: false,
  clear: false,
  recover: false,
  import: false,
  ...commonActions,
};

export const updateActions: CodeEditorActionsConfig = {
  diffMode: true, // if true, you should also set 'originalValue' attribute, eg: <aui-code-editor [originalValue]="originalFile"></aui-code-editor>
  clear: true,
  recover: true,
  import: true,
  ...commonActions,
};

export const commonOptions: IEditorConstructionOptions = {
  folding: true,
  minimap: { enabled: false },
  wordWrap: 'on',
};

export const readonlyOptions: IEditorConstructionOptions = {
  ...commonOptions,
  readOnly: true,
};

export const yamlReadOptions: IEditorConstructionOptions = {
  language: 'yaml',
  ...readonlyOptions,
};

export const yamlWriteOptions: IEditorConstructionOptions = {
  language: 'yaml',
  ...commonOptions,
};
