export const isBlank = (str?: unknown) =>
  str == null ||
  Number.isNaN(str as number) ||
  !(str as object).toString().trim();

export const isNotBlank = (str?: unknown) => !isBlank(str);

export const isValidNonNegative = (num: unknown) =>
  isNotBlank(num) && (num as number) >= 0;

const NOT_USE_GROUPING = { useGrouping: false };

export const numToStr = (num: number) =>
  !num && num !== 0 ? '' : num.toLocaleString('fullwide', NOT_USE_GROUPING);

export const randomPassword = (length: number) => {
  const passwordArray = [
    'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
    'abcdefghijklmnopqrstuvwxyz',
    '1234567890',
    '!@#$%&*()',
  ];
  const password = [];
  for (let i = 0; i < length; i++) {
    // Get random passwordArray index
    const arrayRandom = Math.floor(Math.random() * 4);
    // Get password array value
    const passwordItem = passwordArray[arrayRandom];
    // Get password array value random index
    // Get random real value
    const item = passwordItem[Math.floor(Math.random() * passwordItem.length)];
    password.push(item);
  }
  return password.join('');
};
