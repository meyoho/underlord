import {
  CLUSTER,
  DISPLAY_NAME,
  K8sUtilService,
  Locale,
  NAMESPACE,
  PROJECT,
  StringMap,
  TRUE,
  wrapText,
} from '@alauda/common-snippet';
import dayjs from 'dayjs';
import { merge } from 'lodash-es';
import { base58 } from 'simple-base';
import { Md5 } from 'ts-md5';

import { TIME_STAMP_OPTIONS_CUSTOM } from 'app/maintenance-center/features-shared/utils';
import {
  PREFIXES,
  ROLE_DISPLAY_NAME,
  ROLE_LEVEL,
  ROLE_NAME,
  ROLE_TEMPLATE_LEVEL,
  ROLE_TEMPLATE_OFFICIAL,
  USER_EMAIL,
} from 'app/services/k8s-util.service';
import { RoleTemplate, UserBinding, UserbindingFormModel } from 'app/typings';

export const unionDisplayName = ({
  name,
  displayName,
}: {
  name: string;
  displayName?: string;
}) => name + (displayName ? wrapText(displayName) : '');

function initUserbinding(this: { k8sUtil: K8sUtilService }): UserBinding {
  return {
    apiVersion: 'auth.alauda.io/v1',
    kind: 'UserBinding',
    metadata: {
      name: '',
      annotations: {},
      labels: {
        [this.k8sUtil.normalizeType(CLUSTER)]: '',
        [this.k8sUtil.normalizeType(NAMESPACE)]: '',
        [this.k8sUtil.normalizeType(PROJECT)]: '',
      },
    },
  };
}

export function genUserbinding(
  this: { k8sUtil: K8sUtilService },
  username: string,
  role: RoleTemplate,
  inputLabels: StringMap,
  local: string,
) {
  const userbinding = initUserbinding.call(this);
  const { labels } = userbinding.metadata;

  const email = Md5.hashStr(username).toString().toLowerCase();

  let displayName = '';

  if (
    this.k8sUtil.getLabel(role, ROLE_TEMPLATE_OFFICIAL, PREFIXES.AUTH) ===
      TRUE &&
    local === Locale.EN
  ) {
    displayName = this.k8sUtil.getAnnotation(role, `${DISPLAY_NAME}.en`);
  } else {
    displayName = this.k8sUtil.getDisplayName(role);
  }

  Object.assign(labels, inputLabels, {
    [this.k8sUtil.normalizeType(USER_EMAIL, PREFIXES.AUTH)]: email,
    [this.k8sUtil.normalizeType(
      ROLE_LEVEL,
      PREFIXES.AUTH,
    )]: this.k8sUtil.getLabel(role, ROLE_TEMPLATE_LEVEL, PREFIXES.AUTH),
    [this.k8sUtil.normalizeType(
      ROLE_NAME,
      PREFIXES.AUTH,
    )]: this.k8sUtil.getName(role),
    [this.k8sUtil.normalizeType(ROLE_DISPLAY_NAME, PREFIXES.AUTH)]: displayName
      ? base58.encode(displayName)
      : '',
  });

  let name = `${username}-${role.metadata.name}`;

  switch (
    role.metadata.labels[
      this.k8sUtil.normalizeType(ROLE_TEMPLATE_LEVEL, PREFIXES.AUTH)
    ]
  ) {
    case NAMESPACE:
      name = `${name}-namespace-${
        labels[this.k8sUtil.normalizeType(PROJECT)]
      }-${labels[this.k8sUtil.normalizeType(CLUSTER)]}-${
        labels[this.k8sUtil.normalizeType(NAMESPACE)]
      }`;
      break;
    case PROJECT:
      name = `${name}-project-${labels[this.k8sUtil.normalizeType(PROJECT)]}`;
      break;
    case 'platform':
      name = `${name}-platform`;
      break;
  }

  merge(userbinding.metadata, {
    annotations: {
      [this.k8sUtil.normalizeType(
        ROLE_DISPLAY_NAME,
        PREFIXES.AUTH,
      )]: displayName,
      [this.k8sUtil.normalizeType(USER_EMAIL, PREFIXES.AUTH)]: username,
    },
    name: Md5.hashStr(name).toString(),
  });

  return userbinding;
}

export const trackByKey = (_index: number, key: keyof any) => key;

export const parseJSONStream = (jsonStream: string) => {
  for (let i = 1; i <= jsonStream.length; i++) {
    try {
      return JSON.parse(jsonStream.slice(-i));
    } catch {}
  }
};

export function genUserbindingByFormModel(
  this: { k8sUtil: K8sUtilService },
  email: string,
  userbindingFormModel: UserbindingFormModel,
  local: string,
) {
  const labels: StringMap = {};
  const label = this.k8sUtil.getLabel(
    userbindingFormModel.role,
    ROLE_TEMPLATE_LEVEL,
    PREFIXES.AUTH,
  );
  switch (label) {
    case NAMESPACE:
      labels[this.k8sUtil.normalizeType(PROJECT)] = this.k8sUtil.getName(
        userbindingFormModel.project,
      );
      labels[this.k8sUtil.normalizeType(NAMESPACE)] = this.k8sUtil.getName(
        userbindingFormModel.namespace,
      );
      labels[this.k8sUtil.normalizeType(CLUSTER)] = this.k8sUtil.getLabel(
        userbindingFormModel.namespace,
        CLUSTER,
        PREFIXES.AUTH,
      );
      break;
    case PROJECT:
      labels[this.k8sUtil.normalizeType(PROJECT)] = this.k8sUtil.getName(
        userbindingFormModel.project,
      );
      break;
  }

  return genUserbinding.call(
    this,
    email,
    userbindingFormModel.role,
    labels,
    local,
  );
}

/**
 * 判断一个数组中的某个key的值是否重复
 * @param arr 一个对象数组
 * @param key 需要判断的key
 * @return 如重复，则返回重复的值，否则返回false
 */
export const hasDuplicateBy = <T>(arr: T[], key: keyof T) => {
  const values = new Set<T[keyof T]>();
  if (arr.some(item => values.size === values.add(item[key]).size)) {
    return values.values().next().value;
  }
  return false;
};

export const getTimeRangeByRangeType = (type: string) => {
  let startTime;
  const endTime = new Date().getTime();
  const offset =
    TIME_STAMP_OPTIONS_CUSTOM.find(option => option.type === type).offset ||
    TIME_STAMP_OPTIONS_CUSTOM[0].offset;
  if (offset > 7 * 24 * 3600 * 1000) {
    const endDate = new Date(endTime);
    startTime =
      new Date(
        endDate.getFullYear(),
        endDate.getMonth(),
        endDate.getDate(),
      ).getTime() -
      offset +
      24 * 3600 * 1000;
  } else {
    startTime = endTime - offset;
  }
  return {
    start_time: startTime,
    end_time: endTime,
  };
};

export const dateNumToStr = (num: number) => {
  return dayjs(num).format('YYYY-MM-DD HH:mm:ss');
};

export const dateStrToNum = (str: string) => {
  return dayjs(str, 'YYYY-MM-DD HH:mm:ss').valueOf();
};
