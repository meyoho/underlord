import { AbstractControl, ValidationErrors } from '@angular/forms';

import {
  K8S_RESOURCE_LABEL_KEY_NAME_PATTERN,
  K8S_RESOURCE_LABEL_KEY_PREFIX_PATTERN,
} from './patterns';

export function k8sResourceLabelKeyValidator(
  control: AbstractControl,
): ValidationErrors | null {
  if (!control.value) {
    return null;
  }

  const keyPartition = control.value.split('/');
  const [prefix, ...nameParts] = keyPartition;
  let checkPrefix = true;
  let checkName = true;
  if (keyPartition.length > 1) {
    checkPrefix = K8S_RESOURCE_LABEL_KEY_PREFIX_PATTERN.pattern.test(prefix);
    checkName = K8S_RESOURCE_LABEL_KEY_NAME_PATTERN.pattern.test(
      nameParts.join('/'),
    );
  } else {
    checkName = K8S_RESOURCE_LABEL_KEY_NAME_PATTERN.pattern.test(
      keyPartition[0],
    );
  }
  if (!checkPrefix) {
    return { prefixPattern: true };
  }
  if (!checkName) {
    return { namePattern: true };
  }

  return null;
}
