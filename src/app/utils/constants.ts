import { TypeMeta, createResourceDefinitions } from '@alauda/common-snippet';

import {
  K8sQuotaType,
  LimitRangeItem,
  LimitRangeItemType,
  QuotaItemType,
} from 'app/typings/raw-k8s';

export const K8S_QUOTA_TYPES: K8sQuotaType[] = [
  {
    key: QuotaItemType.CPU_LIMITS,
    fallbackKey: QuotaItemType.CPU_REQUESTS,
    name: 'cpu',
    unit: 'unit_core',
    static: true,
  },
  {
    key: QuotaItemType.MEMORY_LIMITS,
    fallbackKey: QuotaItemType.MEMORY_REQUESTS,
    name: 'memory',
    unit: 'unit_Gi',
    static: true,
  },
  {
    key: QuotaItemType.STORAGE_REQUESTS,
    name: 'storage',
    unit: 'unit_Gi',
    static: true,
  },
  {
    key: QuotaItemType.PODS,
    name: 'pods_num',
    unit: 'unit_ge',
    static: true,
  },
  {
    key: QuotaItemType.PVC,
    name: 'pvc_num',
    unit: 'unit_ge',
    static: true,
  },
  {
    key: QuotaItemType.NVIDIA_GPU,
    fallbackKey: 'requests.' + QuotaItemType.NVIDIA_GPU,
    name: 'physical_gpu',
    unit: 'unit_ge',
    description: 'physical_gpu_input_tip',
  },
  {
    key: QuotaItemType.AMD_GPU,
    fallbackKey: 'requests.' + QuotaItemType.AMD_GPU,
    name: 'physical_gpu',
    unit: 'unit_ge',
    description: 'physical_gpu_input_tip',
  },
  {
    key: QuotaItemType.VIRTUAL_GPU,
    fallbackKey: 'requests.' + QuotaItemType.VIRTUAL_GPU,
    name: 'virtual_gpu',
    unit: 'unit_ge',
    description: 'virtual_gpu_input_tip',
  },
  {
    key: QuotaItemType.VIRTUAL_VIDEO_MEMORY,
    fallbackKey: 'requests.' + QuotaItemType.VIRTUAL_VIDEO_MEMORY,
    name: 'video_memory',
    unit: 'unit_ge',
    description: 'video_memory_input_tip',
  },
];

export const DEFAULT_LIMIT_RANGE = Object.values(LimitRangeItemType).reduce<
  LimitRangeItem
>(
  (acc, key) =>
    Object.assign(acc, {
      [key]: {
        cpu: null,
        memory: null,
      },
    }),
  {
    type: 'Container',
  },
);

export const DEFAULT = 'default';
export const TRUE = 'true';
export const FALSE = 'false';

const AUTH_API_GROUP = 'auth.alauda.io';
const FEDERATED_API_GROUP = 'types.kubefed.io';
const PRODUCT_API_GROUP = 'product.alauda.io';
const AIOPS_GROUP = 'aiops.alauda.io';
const TKE_PLATFORM_GROUP = 'platform.tkestack.io';

const _ = createResourceDefinitions({
  PROJECT: {
    type: 'projects',
    apiGroup: AUTH_API_GROUP,
  },
  VIEW: {
    type: 'views',
    apiGroup: AUTH_API_GROUP,
  },
  CLUSTER_REGISTRY: {
    type: 'clusters',
    apiGroup: 'clusterregistry.k8s.io',
    apiVersion: 'v1alpha1',
  },
  USER_BINDING: {
    type: 'userbindings',
    apiGroup: AUTH_API_GROUP,
  },
  USER: {
    type: 'users',
    apiGroup: AUTH_API_GROUP,
  },
  GROUP: {
    type: 'groups',
    apiGroup: AUTH_API_GROUP,
  },
  NAMESPACE: {
    type: 'namespaces',
  },
  RESOURCE_QUOTA: {
    type: 'resourcequotas',
  },
  SECRET: {
    type: 'secrets',
  },
  NOTIFICATION: {
    apiGroup: AIOPS_GROUP,
    apiVersion: 'v1beta1',
    type: 'notifications',
  },
  NOTIFICATION_SERVER: {
    apiGroup: AIOPS_GROUP,
    apiVersion: 'v1beta1',
    type: 'notificationservers',
  },
  NOTIFICATION_SENDER: {
    apiGroup: AIOPS_GROUP,
    apiVersion: 'v1beta1',
    type: 'notificationsenders',
  },
  NOTIFICATION_RECEIVER: {
    apiGroup: AIOPS_GROUP,
    apiVersion: 'v1beta1',
    type: 'notificationreceivers',
  },
  NOTIFICATION_TEMPLATE: {
    apiGroup: AIOPS_GROUP,
    apiVersion: 'v1beta1',
    type: 'notificationtemplates',
  },
  FEDERATED_RESOURCE_QUOTA: {
    type: 'federatedresourcequotas',
    apiGroup: FEDERATED_API_GROUP,
    apiVersion: 'v1beta1',
  },
  LIMIT_RANGE: {
    type: 'limitranges',
  },
  FEDERATED_LIMIT_RANGE: {
    type: 'federatedlimitranges',
    apiGroup: FEDERATED_API_GROUP,
    apiVersion: 'v1beta1',
  },
  LOG: {
    apiGroup: AIOPS_GROUP,
    apiVersion: 'v1beta1',
    type: 'logs',
  },
  CUSTOM_RESOURCE_DEFINITION: {
    type: 'customresourcedefinitions',
    apiGroup: 'apiextensions.k8s.io',
    apiVersion: 'v1beta1',
  },
  FEATURE: {
    type: 'features',
    apiGroup: 'infrastructure.alauda.io',
    apiVersion: 'v1alpha1',
  },
  CONNECTOR: {
    type: 'connectors',
    apiGroup: 'dex.coreos.com',
  },
  ROLE_TEMPLATE: {
    type: 'roletemplates',
    apiGroup: AUTH_API_GROUP,
    apiVersion: 'v1beta1',
  },
  FUNCTION_RESOURCE: {
    type: 'functionresources',
    apiGroup: AUTH_API_GROUP,
    apiVersion: 'v1beta1',
  },
  FEDERATED_NAMESPACE: {
    type: 'federatednamespaces',
    apiGroup: FEDERATED_API_GROUP,
    apiVersion: 'v1beta1',
  },
  CLUSTER_FED: {
    type: 'clusterfeds',
    apiGroup: 'infrastructure.alauda.io',
    apiVersion: 'v1alpha1',
  },
  TKE_CLUSTER: {
    apiGroup: TKE_PLATFORM_GROUP,
    type: 'clusters',
  },
  ALAUDA_FEATURE_GATE: {
    apiGroup: 'alauda.io',
    apiVersion: 'v1',
    type: 'alaudafeaturegates',
  },
  CLUSTER_ALAUDA_FEATURE_GATE: {
    apiGroup: 'alauda.io',
    apiVersion: 'v1',
    type: 'clusteralaudafeaturegates',
  },
  PROJECT_QUOTA: {
    apiGroup: AUTH_API_GROUP,
    apiVersion: 'v1',
    type: 'projectquotas',
  },
  LICENSE: {
    type: 'secrets',
  },
  REPORT: {
    type: 'reports',
    apiGroup: 'metering.alauda.io',
    apiVersion: 'v1beta1',
  },
  CSP: {
    type: 'csps',
    apiGroup: PRODUCT_API_GROUP,
  },
  NODE: {
    type: 'nodes',
  },
  TSF: {
    type: 'tsfproducts',
    apiGroup: PRODUCT_API_GROUP,
    apiVersion: 'v1beta1',
  },
  TDSQL_DEPLOY: {
    type: 'tdsqldeploys',
    apiVersion: 'v1alpha1',
    apiGroup: 'tdsql.alauda.io',
  },
  PORTAL: {
    type: '*',
    apiGroup: PRODUCT_API_GROUP,
  },
  NAMESPACE_IMPORT: {
    type: 'namespace-import',
    apiGroup: 'namespace.alauda.io',
  },
  ALERT_TEMPLATE: {
    apiGroup: AIOPS_GROUP,
    apiVersion: 'v1beta1',
    type: 'alerttemplates',
  },
  PROMETHEUS_RULE: {
    apiGroup: 'monitoring.coreos.com',
    apiVersion: 'v1',
    type: 'prometheusrules',
  },
  DEPLOYMENT: {
    apiGroup: 'apps',
    type: 'deployments',
  },
  CLUSTERADDONTYPE: {
    apiGroup: TKE_PLATFORM_GROUP,
    type: 'clusteraddontypes',
  },
  STATEFULSET: {
    apiGroup: 'apps',
    type: 'statefulsets',
  },
  DAEMONSET: {
    apiGroup: 'apps',
    type: 'daemonsets',
  },
  APPLICATION_HISTORY: {
    apiGroup: 'app.k8s.io',
    apiVersion: 'v1beta1',
    type: 'applicationhistories',
  },
  TAPP: {
    apiGroup: 'apps.tkestack.io',
    type: 'tapps',
    apiVersion: 'v1',
  },
  CLUSTER: {
    apiGroup: 'clusterregistry.k8s.io',
    apiVersion: 'v1alpha1',
    type: 'clusters',
  },
  MACHINE: {
    apiGroup: TKE_PLATFORM_GROUP,
    type: 'machines',
  },
  REDIS: {
    type: 'redisinstallers',
    apiGroup: PRODUCT_API_GROUP,
    version: 'v1alpha1',
  },
  CLUSTER_CREDENTIAL: {
    apiGroup: TKE_PLATFORM_GROUP,
    type: 'clustercredentials',
  },
  POD: {
    type: 'pods',
  },
  PODS_EXEC: {
    type: 'pods/exec',
  },
  PODS_ROOT_EXEC: {
    type: 'pods/root-exec',
  },
  NODE_METRICS: {
    apiGroup: 'metrics.k8s.io',
    apiVersion: 'v1beta1',
    type: 'nodes',
  },
  CODING: {
    type: 'codings',
    apiGroup: PRODUCT_API_GROUP,
    apiVersion: 'v1',
  },
  CONFIG_MAP: {
    type: 'configmaps',
  },
  ARCHIVELOG: {
    type: 'archivelogs',
    apiGroup: AIOPS_GROUP,
    apiVersion: 'v1alpha1',
  },
  LOGPOLICISE: {
    apiGroup: AIOPS_GROUP,
    apiVersion: 'v1alpha1',
    type: 'logpolicies',
  },
  CRON_JOB: {
    apiGroup: 'batch',
    apiVersion: 'v1beta1',
    type: 'cronjobs',
  },
  TIMATRIX: {
    type: 'timatrixes',
    apiVersion: 'v1alpha1',
    apiGroup: PRODUCT_API_GROUP,
  },
});

export const RESOURCE_DEFINITIONS = _.RESOURCE_DEFINITIONS;
export const RESOURCE_TYPES = _.RESOURCE_TYPES;
export const getYamlApiVersion = _.getYamlApiVersion;
export const TRUTHY = 'true';
export const FALSY = 'false';
export const COMMA_ENTITY = '&#44%3B';
export const TYPE = 'type';
export const ACTION = 'action';
export const STATUS = 'status';
export type ResourceType = keyof typeof RESOURCE_TYPES;

export const SecretTypeMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.SECRET),
  kind: 'Secret',
};

export const NotificationMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.NOTIFICATION),
  kind: 'Notification',
};

export const NotificationServerMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.NOTIFICATION_SERVER),
  kind: 'NotificationServer',
};

export const NotificationSenderMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.NOTIFICATION_SENDER),
  kind: 'NotificationSender',
};

export const NotificationReceiverMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.NOTIFICATION_RECEIVER),
  kind: 'NotificationReceiver',
};

export const NotificationTemplateMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.NOTIFICATION_TEMPLATE),
  kind: 'NotificationTemplate',
};

export const AlertTemplateMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.ALERT_TEMPLATE),
  kind: 'AlertTemplate',
};

export const PrometheusRuleMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.PROMETHEUS_RULE),
  kind: 'PrometheusRule',
};

export const DeploymentTypeMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.DEPLOYMENT),
  kind: 'Deployment',
};

export const ClusterAddonTypeMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.CLUSTERADDONTYPE),
  kind: 'ClusterAddonType',
};

export const ApplicationHistoryMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.APPLICATION_HISTORY),
  kind: 'ApplicationRollback',
};

export const MachineMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.MACHINE),
  kind: 'Machine',
};

export const ClusterCredentialMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.CLUSTER_CREDENTIAL),
  kind: 'ClusterCredential',
};

export const TKEClusterMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.TKE_CLUSTER),
  kind: 'Cluster',
};

export const ClusterFedMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.CLUSTER_FED),
  kind: 'Clusterfed',
};

export const MeterReportMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.REPORT),
  kind: 'Report',
};

export const ArchiveLogMeta: TypeMeta = {
  apiVersion: getYamlApiVersion(RESOURCE_TYPES.ARCHIVELOG),
  kind: 'ArchiveLog',
};
