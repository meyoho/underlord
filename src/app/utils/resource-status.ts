import { TRUE } from '@alauda/common-snippet';
import { get } from 'lodash-es';

import { GenericStatus } from 'app/shared/components/status-icon/component';
import {
  AlarmStatusEnum,
  Cluster,
  ClusterAddOn,
  ClusterAddOnStatusEnum,
  ClusterStatusEnum,
  ContainerStatus,
  Node,
  NodeStatusEnum,
  Pod,
  PodStatusEnum,
  PrometheusRuleItem,
  TKECluster,
} from 'app/typings';

export function getAlarmStatus(rule: PrometheusRuleItem): AlarmStatusEnum {
  if (rule.labels.alarm_status === 'pending') {
    return AlarmStatusEnum.pending;
  } else if (rule.labels.alarm_status === 'firing') {
    return AlarmStatusEnum.firing;
  }
  return AlarmStatusEnum.normal;
}

export function getClusterAddonText(addon: ClusterAddOn): GenericStatus {
  switch (addon.status.phase) {
    case 'Running':
      return GenericStatus.Success;
    case 'Initializing':
    case 'Reinitializing':
    case 'Pending':
    case 'Checking':
    case 'Upgrading':
      return GenericStatus.Pending;
    case 'Terminating':
      return GenericStatus.Deleting;
    case 'Failed':
    case 'Unhealthy':
      return GenericStatus.Error;
    default:
      return GenericStatus.Unknown;
  }
}

export function getTKEClusterStatus(cluster: TKECluster) {
  if (cluster && cluster.status) {
    switch (cluster.status.phase) {
      case 'Running':
        return ClusterStatusEnum.normal;
      case 'Initializing':
        return cluster.spec.type === 'Baremetal'
          ? ClusterStatusEnum.deploying
          : ClusterStatusEnum.accessing;
      case 'Terminating':
        return ClusterStatusEnum.deleting;
    }
  }
  return ClusterStatusEnum.abnormal;
}

export function getClusterStatus(cluster: Cluster) {
  if (cluster && cluster.status) {
    const clusterStatus = cluster.status.conditions.find(
      condition => condition.type === 'ComponentNotHealthy',
    );
    if (clusterStatus) {
      return clusterStatus.status.toLowerCase() === TRUE
        ? ClusterStatusEnum.abnormal
        : ClusterStatusEnum.normal;
    }
  }
  return ClusterStatusEnum.abnormal;
}

export function getNodeStatus(node: Node) {
  if (node.status.phase === 'Initializing') {
    return NodeStatusEnum.adding;
  }

  let ready: boolean;

  node.status.conditions.forEach(condition => {
    const status = condition.status.toLowerCase();
    if (condition.type === 'Ready') {
      ready = status === TRUE;
    }
  });

  return ready ? NodeStatusEnum.normal : NodeStatusEnum.abnormal;
}

export function getClusterAddonStatus(
  addon: ClusterAddOn,
): ClusterAddOnStatusEnum {
  switch (addon.status.phase) {
    case 'Running':
      return ClusterAddOnStatusEnum.NORMAL;
    case 'Initializing':
    case 'Reinitializing':
    case 'Pending':
    case 'Checking':
    case 'Upgrading':
      return ClusterAddOnStatusEnum.DEPLOYING;
    case 'Terminating':
      return ClusterAddOnStatusEnum.DELETING;
    case 'Failed':
    case 'Unhealthy':
      return ClusterAddOnStatusEnum.ABNORMAL;
    default:
      return ClusterAddOnStatusEnum.UNKNOWN;
  }
}

// 将 getPodStatus 得到的多种状态映射到标签类型
// reference: https://bitbucket.org/mathildetech/link/src/c81161f02f981904b716462a305e09414e5617d3/src/backend/resource/common/podinfo.go#lines-68
export function getPodAggregatedStatus(status: string): PodStatusEnum {
  if (
    status.split(':')[0] === 'Initing' ||
    [
      'Initing',
      'Pending',
      'PodInitializing',
      'ContainerCreating',
      'Terminating',
    ].includes(status)
  ) {
    return PodStatusEnum.pending;
  }

  if (status === 'Completed') {
    return PodStatusEnum.completed;
  }
  if (status === 'Running') {
    return PodStatusEnum.running;
  }

  return PodStatusEnum.error;
}

// reference: https://bitbucket.org/mathildetech/link/src/c81161f02f981904b716462a305e09414e5617d3/src/backend/resource/common/pod.go#lines-179
export function getPodStatus(pod: Pod): string {
  let reason = pod.status.phase || pod.status.reason;
  const { initializing, reason: initReason } = getPodInitStatus(pod, reason);
  if (!initializing) {
    reason = getPodContainerStatus(pod, reason);
  } else {
    reason = initReason;
  }

  if (pod.metadata.deletionTimestamp && pod.status.reason === 'NodeLost') {
    reason = 'Unknown';
  } else if (pod.metadata.deletionTimestamp) {
    reason = 'Terminating';
  }
  return reason;
}

function getPodContainerStatus(pod: Pod, reason: string): string {
  const containerStatuses = get(pod, ['status', 'containerStatuses'], []);
  for (let i = 0, len = containerStatuses.length; i < len; i++) {
    const container = containerStatuses[i];
    let waitingReason, terminatedReason;
    if ((waitingReason = get(container, ['state', 'waiting', 'reason']))) {
      return waitingReason;
    } else if (
      (terminatedReason = get(container, ['state', 'terminated', 'reason']))
    ) {
      return terminatedReason;
    } else if (get(container, ['state', 'terminated'])) {
      const { signal, exitCode } = container.state.terminated;
      if (signal !== 0) {
        return `Signal:${signal}`;
      } else {
        return `ExitCode:${exitCode}`;
      }
    }
  }
  return reason;
}

function getPodInitStatus(pod: Pod, reason: string) {
  let initializing = false;
  const initContainerStatuses = get(
    pod,
    ['status', 'initContainerStatuses'],
    [] as ContainerStatus[],
  );
  for (let i = initContainerStatuses.length - 1; i >= 0; i--) {
    const status = initContainerStatuses[i];
    if (get(status, ['state', 'terminated', 'exitCode']) === 0) {
      continue;
    }
    let waitingReason;
    const terminated = get(status, ['state', 'terminated']);
    if (terminated) {
      reason = terminated.reason
        ? `Init:${terminated.reason}`
        : terminated.signal !== 0
        ? `Init:Signal:${terminated.signal}`
        : `Init:ExitCode:${terminated.exitCode}`;
    } else if (
      (waitingReason = get(status, ['state', 'waiting', reason])) &&
      waitingReason !== 'PodInitializing'
    ) {
      reason = `Initing:${waitingReason}`;
    } else {
      reason = `Initing:${i}/${pod.spec.initContainers.length}`;
    }
    initializing = true;
    break;
  }
  return { initializing, reason };
}
