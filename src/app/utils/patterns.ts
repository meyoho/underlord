export const K8S_RESOURCE_NAME_BASE = {
  pattern: /^[\da-z]([\da-z-]*[\da-z])?$/,
  tip: 'regexp_tip_k8s_resource_name_base',
};

export const K8S_RESOURCE_LABEL_KEY_PREFIX_PATTERN = {
  pattern: /^([\da-z]([\da-z-]*[\da-z])?(\.[\da-z]([\da-z-]*[\da-z])?)*)?$/,
  tip: 'regexp_tip_resource_label_key_prefix',
};

export const K8S_RESOURCE_LABEL_KEY_NAME_PATTERN = {
  pattern: /^(([\dA-Za-z][\w./-]*)?[\dA-Za-z])$/,
  tip: 'regexp_tip_resource_label_key_name',
};

export const K8S_RESOURCE_LABEL_VALUE_PATTERN = {
  pattern: /^(([\dA-Za-z][\w.-]*)?[\dA-Za-z])?$/,
  tip: 'regexp_tip_resource_label_value',
};

export const CLUSTER_PARAM_KEY_PATTERN = {
  pattern: /^(([\dA-Za-z][\w.-]*)?[\dA-Za-z])$/,
  tip: 'regexp_tip_extended_parameters_key',
};

export const CLUSTER_PARAM_VALUE_PATTERN = {
  pattern: /^([\w!#$%&'()*+-;<=>?@[\]^`{|}~-])*$/,
  tip: 'regexp_tip_extended_parameters_value',
};

export const NUMBER_PATTERN = {
  pattern: /^-?\d+(\.\d*)?$/,
  tip: 'invalid_number',
};

export const POSITIVE_INT_PATTERN = {
  pattern: /^[1-9]\d*$/,
  tip: 'positive_integer_pattern',
};

export const EMAIL_PATTERN = {
  pattern: /^[\w-]+@[\w-]+(\.[\w-]+)+$/,
  tip: 'invalid_email',
};

export const PHONE_PATTERN = {
  pattern: /^\+{0,1}\d{7,15}$/,
  tip: 'invalid_phone',
};

export const HTTP_ADDRESS_PATTERN = {
  pattern: /^(http|https):\/\/([\w.]+\/?)\S*$/,
  tip: 'invalid_http_address_format',
};

export const K8S_API_RESOURCE = {
  pattern: /^[\da-z]([\d.a-z-]*[\da-z])?(\.[\da-z]([\da-z-]*[\da-z])?)*$/,
  tip: 'regexp_tip_k8s_api_resource',
};

export const IP_PATTERN = {
  pattern: /^((2(5[0-5]|[0-4]\d))|[01]?\d{1,2})(\.((2(5[0-5]|[0-4]\d))|[01]?\d{1,2})){3}$/,
  tip: 'invalid_ip',
};

export const PORT_PATTERN = {
  pattern: /(^[1-9]\d{0,3}$)|(^[1-5]\d{4}$)|^(6[0-4]\d{3}$)|(^65[0-4]\d{2}$)|(^655[0-2]\d$)|(^6553[0-5]$)/,
  tip: 'invalid_port',
};

export const IP_ADDRESS_PATTERN = {
  pattern: /^(?:(?:25[0-5]|2[0-4]\d|[01]?\d{1,2})\.){3}(?:25[0-5]|2[0-4]\d|[01]?\d{1,2})$/,
  placeholder: 'ip_address_placeholder',
  tip: 'regexp_tip_ip_address',
};

export const IP_ADDRESS_HOSTNAME_PATTERN = {
  pattern: /^((\d|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])\.){3}(\d|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])$|^(([A-Za-z]|[A-Za-z][\dA-Za-z-]*[\dA-Za-z])\.)*([A-Za-z]|[A-Za-z][\dA-Za-z-]*[\dA-Za-z])$/,
  tip: 'regexp_tip_ip_address_or_hostname',
};

export const HOST_IP_PATTERN = {
  pattern: /^((?:(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d)))\.){3}(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d))))$/,
  tip: 'tdsql_host_ip_pattern_error',
};

export const COMPONENT_DIR_PATH_PATTERN = {
  pattern: /^\//,
  tip: 'tdsql_component_path_error',
};

export const PASSWORD_PATTERN = {
  pattern: /^(?!\d+$)(?![A-Za-z]+$)(?![!#$%&()*@]+$)[\d!#$%&()*@A-Za-z]{6,20}$/,
  tip: 'regexp_tip_password',
};

export const USERNAME_PATTERN = {
  pattern: /^[a-z]([\d.a-z-]*[\da-z])?(\.[\da-z]([\da-z-]*[\da-z])?)*$/,
  tip: 'regexp_tip_username',
};

export const POSITIVE_INTEGER_PATTERN = {
  pattern: /^[1-9]\d*$/,
  tip: 'regexp_tip_positive_integer',
};

export const LOOSE_K8S_RESOURCE_NAME_BASE = {
  pattern: /^[\da-z]([\da-z-]*[\da-z])?(\.[\da-z]([\da-z-]*[\da-z])?)*$/,
  tip: 'regexp_tip_k8s_resource_name_loose',
};

export const ALARM_LABEL_KEY_NAME_PATTERN = {
  pattern: /^[A-Z_a-z]\w*$/,
  tip: 'regexp_tip_alert_label_key_name',
};

export const ALARM_METRIC_PATTERN = {
  pattern: /^[\u0020-\u007E]*$/,
  tip: 'regexp_tip_alert_metric',
};

export const ALARM_THRESHOLD_PATTERN = {
  pattern: /^(?!0\d)\d{0,17}(\.\d{1,4})?$/,
  tip: 'regexp_tip_alert_threshold',
};

export const NATURAL_NUMBER_PATTERN = {
  pattern: /^\d+$/,
  tip: 'natural_number_pattern',
};

export const CERT_FILE_PATTERN = {
  pattern: /^(-{5}BEGIN CERTIFICATE-{5}[\d\n+/=A-Za-z]*-{5}END CERTIFICATE-{5}\n?)+$/,
  tip: 'cert_file_pattern_hint',
};

export const MYSQL_PASSWORD_PATTERN = {
  pattern: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!#$%&*@^~])[\d!#$%&*@A-Z^a-z~]{8,16}$/,
  tip: 'tsf_mysql_password_placeholder',
};

export const REDIS_PASSWORD_PATTERN = {
  pattern: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!#$%&*@^~])[\d!#$%&*@A-Z^a-z~]{8,16}$/,
  tip: 'tsf_redis_password_placeholder',
};
