import { StringMap } from '@alauda/common-snippet';

export const getParams = (search: string) => {
  if (!search) {
    return {};
  }
  const queryString = search.slice(1);
  if (!queryString) {
    return {};
  }

  return queryString.split('&').reduce<StringMap>((accum, segment) => {
    if (!segment) {
      return accum;
    }
    const [key, value] = segment.split('=');
    if (!key) {
      return accum;
    }
    return {
      ...accum,
      [key]: value,
    };
  }, {});
};
