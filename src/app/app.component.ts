import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  OnInit,
} from '@angular/core';

import { getParams } from './utils';

@Component({
  selector: 'alu-root',
  template: '<router-outlet></router-outlet>',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit, AfterViewInit {
  jumpFrom = '';

  private timer: number;

  ngOnInit() {
    const queryParams = getParams(location.search);
    if (queryParams.from) {
      this.jumpFrom = queryParams.from;
    }
  }

  ngAfterViewInit() {
    this.setup();
  }

  private setup() {
    if (this.timer === null) {
      return;
    }
    cancelAnimationFrame(this.timer);
    this.timer = requestAnimationFrame(() => {
      if (!document.querySelector('.aui-page__content')) {
        return this.setup();
      }
      this.timer = null;
      const loadingSpinner = document.querySelector('.index-loading-spinner');
      if (loadingSpinner) {
        loadingSpinner.remove();
      }
    });
  }
}
