import { NavItemConfig } from '@alauda/ui';
import { TemplatePortal } from '@angular/cdk/portal';
import { HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, ReplaySubject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

/**
 * Provides a holder for UI templates to be used globally.
 */
export class TemplateHolder {
  private readonly templatePortalSubject = new BehaviorSubject<TemplatePortal>(
    undefined,
  );

  get templatePortal$() {
    return this.templatePortalSubject.pipe(debounceTime(0));
  }

  setTemplatePortal(templatePortal: TemplatePortal) {
    this.templatePortalSubject.next(templatePortal);
  }
}

export enum TemplateHolderType {
  PageHeaderContent = 'PageHeaderContent',
}

/**
 * Acts as a general ui state store
 */
@Injectable({ providedIn: 'root' })
export class UiStateService {
  activeItemInfo$ = new ReplaySubject<NavItemConfig>(1);
  // Pending requests
  requests = new Set<HttpRequest<any>>();

  // Pending requests in observable
  requests$ = new BehaviorSubject<Set<HttpRequest<any>>>(this.requests);

  private readonly templateHolders = new Map<
    TemplateHolderType,
    TemplateHolder
  >();

  // Will init template holder if not initialized yet
  getTemplateHolder(id: TemplateHolderType) {
    if (!this.templateHolders.has(id)) {
      this.registerTemplateHolder(id);
    }

    return this.templateHolders.get(id);
  }

  setActiveItemInfo(activeNavItemInfo: NavItemConfig) {
    this.activeItemInfo$.next(activeNavItemInfo);
  }

  private registerTemplateHolder(id: TemplateHolderType) {
    if (this.templateHolders.has(id)) {
      throw new Error(`Template holder for ${id} has already registered!`);
    }
    this.templateHolders.set(id, new TemplateHolder());
  }
}
