import { K8sPermissionService } from '@alauda/common-snippet';
import { Injectable } from '@angular/core';
import { map, shareReplay, startWith } from 'rxjs/operators';

import { RESOURCE_TYPES } from 'app/utils';

@Injectable({
  providedIn: 'root',
})
export class ViewPermissionsService {
  constructor(private readonly k8sPermission: K8sPermissionService) {}

  viewPermissions$ = this.k8sPermission
    .isAllowed({
      type: RESOURCE_TYPES.VIEW,
      name: ['projectview', 'platformview', 'maintenanceview'],
    })
    .pipe(
      startWith([] as boolean[]),
      map(([projectView, platformView, maintenanceView]) => ({
        projectView,
        platformView,
        maintenanceView,
      })),
      shareReplay(1),
    );
}
