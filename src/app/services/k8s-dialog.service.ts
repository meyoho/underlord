import {
  ANNOTATIONS,
  K8sApiService,
  KubernetesResource,
  LABELS,
  METADATA,
  ResourceWriteParams,
  TranslateService,
} from '@alauda/common-snippet';
import { ConfirmType, DialogService, MessageService } from '@alauda/ui';
import { Injectable } from '@angular/core';
import { Validators } from '@angular/forms';
import { cloneDeep, get } from 'lodash-es';
import { EMPTY, Observable, Subject, from } from 'rxjs';
import { catchError, switchMap, tap } from 'rxjs/operators';

import {
  UpdateKeyValueDialogComponent,
  UpdateKeyValueDialogData,
} from 'app/shared/components/update-key-value-dialog/update-key-value-dialog.component';
import {
  K8S_RESOURCE_LABEL_KEY_NAME_PATTERN,
  K8S_RESOURCE_LABEL_VALUE_PATTERN,
  ResourceType,
} from 'app/utils';

import { K8sUtilService } from './k8s-util.service';

@Injectable()
export class K8sDialogService {
  constructor(
    private readonly dialog: DialogService,
    private readonly message: MessageService,
    private readonly k8sApi: K8sApiService,
    private readonly k8sUtil: K8sUtilService,
    private readonly translate: TranslateService,
  ) {}

  /**
   * 更新 Annotation 或 Label
   */
  updatePart<T extends KubernetesResource>(
    params: ResourceWriteParams<ResourceType, T> & {
      readonlyKeys?: Array<string | RegExp>;
      part: typeof ANNOTATIONS | typeof LABELS;
    },
  ) {
    const { part, resource, readonlyKeys } = params;
    const keyValues = get(resource, [METADATA, part], {});
    const updated$$ = new Subject<T>();
    this.dialog.open<
      UpdateKeyValueDialogComponent<T>,
      UpdateKeyValueDialogData<T>
    >(UpdateKeyValueDialogComponent, {
      data: {
        title: `update_${part}`,
        keyValues,
        readonlyKeys,
        updateSuccessMsg: `update_${part}_successed`,
        updateFailMsg: `update_${part}_failed`,
        validator: {
          key: [
            Validators.pattern(K8S_RESOURCE_LABEL_KEY_NAME_PATTERN.pattern),
          ],
          value:
            part === LABELS
              ? [Validators.pattern(K8S_RESOURCE_LABEL_VALUE_PATTERN.pattern)]
              : [],
        },
        errorMapper: {
          key: {
            pattern: this.translate.get(
              K8S_RESOURCE_LABEL_KEY_NAME_PATTERN.tip,
            ),
          },
          value: {
            pattern: this.translate.get(K8S_RESOURCE_LABEL_VALUE_PATTERN.tip),
          },
        },
        onUpdate: results => {
          const checkedResults = cloneDeep(results);
          Object.keys(keyValues).forEach(key => {
            if (!(key in checkedResults)) {
              checkedResults[key] = null;
            }
          });
          return ((this.k8sApi[
            'cluster' in params && params.cluster
              ? 'patchResource'
              : 'patchGlobalResource'
          ] as any)({
            ...params,
            part: {
              metadata: {
                [part]: checkedResults,
              },
            },
          }) as Observable<T>).pipe(tap(res => updated$$.next(res)));
        },
      },
    });
    return updated$$.asObservable();
  }

  /**
   * @param type 对应资源的翻译 key
   */
  deleteResource<T extends KubernetesResource>(
    params: ResourceWriteParams<ResourceType, T>,
    type: string,
  ) {
    return from(
      this.dialog.confirm({
        title: this.translate.get(`delete_${type}`),
        content:
          this.translate.get('delete_confirm_resource', {
            type: this.translate.get(type).toLowerCase(),
            name: this.k8sUtil.getName(params.resource),
          }) + this.translate.get('delete_not_recoverable'),
        confirmType: ConfirmType.Danger,
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
      }),
    ).pipe(
      catchError(() => EMPTY),
      switchMap(() =>
        ((this.k8sApi[
          'cluster' in params && params.cluster
            ? 'deleteResource'
            : 'deleteGlobalResource'
        ] as any)(params) as Observable<T>).pipe(
          tap(() => this.message.success(this.translate.get('delete_success'))),
          catchError(() => EMPTY),
        ),
      ),
    );
  }
}
