import { Injectable } from '@angular/core';

interface FsDocument extends HTMLDocument {
  mozFullScreenEnabled?: boolean;
  mozFullScreenElement?: Element;
  mozCancelFullScreen?: () => void;
}

interface FsElement extends Element {
  mozRequestFullScreen?: () => void;
}

@Injectable()
export class FullScreenService {
  prefixName = '';
  supportFullScreen = false;
  constructor() {
    const fsDoc: FsDocument = document;
    if (fsDoc.fullscreenEnabled) {
      this.supportFullScreen = fsDoc.fullscreenEnabled;
    } else if (fsDoc.mozFullScreenEnabled) {
      this.supportFullScreen = fsDoc.mozFullScreenEnabled;
      this.prefixName = 'moz';
    }
  }

  /**
   * @description: 将传进来的元素全屏
   * @param {String} domName 要全屏的dom名称
   */
  requestFullscreen(domName: string) {
    if (!this.supportFullScreen) {
      return;
    }
    const element: FsElement = document.querySelector(domName);
    this.prefixName === ''
      ? element.requestFullscreen()
      : element.mozRequestFullScreen();
  }

  /**
   * @description: 退出全屏，如果有多层全屏会一层一层的退出。如果按 ESC 键则会全部退出。
   */
  exitFullscreen() {
    const fsDoc: FsDocument = document;
    this.prefixName === ''
      ? fsDoc.exitFullscreen()
      : fsDoc.mozCancelFullScreen();
  }

  /**
   * @description: 检测有没有元素处于全屏状态
   * @return boolean
   */
  isElementFullScreen() {
    const fsDoc: FsDocument = document;
    const fullscreenElement =
      fsDoc.fullscreenElement || fsDoc.mozFullScreenElement;
    return !!fullscreenElement;
  }
}
