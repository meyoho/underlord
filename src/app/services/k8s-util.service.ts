import {
  K8sUtilService as _K8sUtilService,
  KubernetesResource,
} from '@alauda/common-snippet';
import { Injectable, Injector } from '@angular/core';
import { get } from 'lodash-es';

import { K8sResourceStatus } from 'app/typings';

// types
export const FEDERATED = 'federated';
export const HOSTNAME = 'hostname';
export const OVERRIDE = 'override';

// prefixes
export const PREFIXES = {
  AUTH: 'auth',
  FEDERATION: 'federation',
} as const;

// annotations
export const UNITE_QUOTA_FED_CLUSTERS = 'unite-quota-fed-clusters';

// labels
export const ROLE_NAME = 'role.name';
export const ROLE_DISPLAY_NAME = 'role.display-name';
export const ROLE_LEVEL = 'role.level';
export const ROLE_TEMPLATE_OFFICIAL = 'roletemplate.official';
export const ROLE_TEMPLATE_LEVEL = 'roletemplate.level';
export const USER_EMAIL = 'user.email';
export const USER_VALID = 'user.valid';
export const USER_CONNECTOR_ID = 'user.connector_id';
export const USER_CONNECTOR_TYPE = 'user.connector_type';

export interface StatusItem {
  status: string;
  desc: string;
}

export const STATUSES: Partial<Record<K8sResourceStatus, StatusItem>> = {
  Active: { status: 'success', desc: 'active' },
  Creating: { status: 'pending', desc: 'creating' },
  Terminating: { status: 'pending', desc: 'deleting' },
  Error: { status: 'error', desc: 'error' },
  Unknown: { status: 'unknown', desc: 'unknown' },
  Processing: { status: 'pending', desc: 'processing' },
  Completed: { status: 'success', desc: 'completed' },
  Failed: { status: 'error', desc: 'failed' },
  Pending: { status: 'exclamation', desc: 'pending' },
};

const STATUS_PROPERTIES = ['status', 'phase'] as const;

const SPEC_STATUS_PROPERTIES = ['spec', ...STATUS_PROPERTIES] as const;

@Injectable({ providedIn: 'root' })
export class K8sUtilService extends _K8sUtilService {
  constructor(protected injector: Injector) {
    super(injector);
  }

  // reference: http://confluence.alaudatech.com/pages/viewpage.action?pageId=27166946
  getStatus(
    resource: KubernetesResource,
    withSpec = true,
    property: keyof StatusItem = 'desc',
  ) {
    const status: K8sResourceStatus = get(
      resource,
      withSpec ? SPEC_STATUS_PROPERTIES : STATUS_PROPERTIES,
    );
    return (STATUSES[status] || STATUSES.Unknown)[property];
  }
}
