import {
  ApiGatewayInterceptor,
  AuthorizationInterceptorService,
  ResourceErrorInterceptor,
  TOKEN_BASE_DOMAIN,
  TOKEN_GLOBAL_NAMESPACE,
  TOKEN_RESOURCE_DEFINITIONS,
  TranslateService,
} from '@alauda/common-snippet';
import { PaginatorIntl, TooltipCopyIntl } from '@alauda/ui';
import { APP_BASE_HREF, PlatformLocation } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import {
  Inject,
  InjectionToken,
  NgModule,
  Optional,
  SkipSelf,
} from '@angular/core';

import { Environments } from 'app/api/envs/types';
import { AppPaginatorIntl } from 'app/services/app-paginator-intl.service';
import { RESOURCE_DEFINITIONS } from 'app/utils';

import { envs } from '../../env';

import { TooltipCopyIntlService } from './tooltip-copy-intl.service';

export function getAppBaseHref(platformLocation: PlatformLocation): string {
  return platformLocation.getBaseHrefFromDOM();
}
export const ENVIRONMENTS = new InjectionToken<Environments>('ENVIRONMENTS');

/**
 * Services in Angular App is static and singleton over the whole system,
 * so we will import them into the root module.
 *
 * This is to replace the Core module, which we believe is somewhat duplicates with the
 * purpose of global service module.
 */
@NgModule({
  providers: [
    {
      provide: ENVIRONMENTS,
      useValue: envs,
    },
    {
      provide: TOKEN_BASE_DOMAIN,
      useFactory: (envs: Environments) => envs.LABEL_BASE_DOMAIN,
      deps: [ENVIRONMENTS],
    },
    {
      provide: TOKEN_GLOBAL_NAMESPACE,
      useFactory: (envs: Environments) => envs.GLOBAL_NAMESPACE,
      deps: [ENVIRONMENTS],
    },
    {
      provide: TooltipCopyIntl,
      useClass: TooltipCopyIntlService,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiGatewayInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ResourceErrorInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthorizationInterceptorService,
      multi: true,
    },
    {
      provide: APP_BASE_HREF,
      useFactory: getAppBaseHref,
      deps: [PlatformLocation],
    },
    {
      provide: TOKEN_RESOURCE_DEFINITIONS,
      useValue: RESOURCE_DEFINITIONS,
    },
    {
      provide: PaginatorIntl,
      useClass: AppPaginatorIntl,
    },
  ],
})
export class ServicesModule {
  /* Make sure ServicesModule is imported only by one NgModule the RootModule */
  constructor(
    @Inject(ServicesModule)
    @Optional()
    @SkipSelf()
    parentModule: ServicesModule,
    // Inject the following to make sure they are loaded ahead of other components.
    _translate: TranslateService,
  ) {
    if (parentModule) {
      throw new Error(
        'ServicesModule is already loaded. Import only in AppModule.',
      );
    }
  }
}
