// tslint:disable: max-union-size
import {
  KubernetesResource,
  ObjectMeta,
  StringMap,
  TranslateKey,
  TypeMeta,
} from '@alauda/common-snippet';

import { WorkloadStatusEnum } from './status';

export interface UserSpec {
  connector_name?: string;
  connector_type?: string;
  email?: string;
  groups?: string[];
  is_admin?: boolean;
  password?: string;
  valid?: boolean;
  username?: string;
  old_password?: string;
  account?: string;
}

// http://confluence.alaudatech.com/pages/viewpage.action?pageId=39338366#1-%E8%8E%B7%E5%8F%96%E7%94%A8%E6%88%B7%E4%BF%A1%E6%81%AF
export interface User extends KubernetesResource {
  spec?: UserSpec;
}

export interface ProjectCluster {
  name: string;
  quota?: K8sQuotaItem;
  type?: '' | 'FedMember';
}

export interface Project extends KubernetesResource {
  spec?: {
    clusters?: ProjectCluster[];
    status?: {
      phase?: string;
    };
    clusterDeletePolicy?: 'Delete' | 'Retain';
  };
  status?: {
    phase?: K8sResourceStatus;
  };
}

export interface Namespace extends KubernetesResource {
  spec?: {
    finalizers?: string[];
  };
  status?: {
    phase?: string;
  };
}

export interface ScopedResourceSelectorRequirement {
  operator: 'In' | 'NotIn' | 'Exists' | 'DoesNotExist';
  scopeName: string;
  values: string;
}

export interface ScopeSelector {
  matchExpressions: ScopedResourceSelectorRequirement[];
}

export enum QuotaItemType {
  PODS = 'pods',
  CPU_LIMITS = 'limits.cpu',
  CPU_REQUESTS = 'requests.cpu',
  MEMORY_LIMITS = 'limits.memory',
  MEMORY_REQUESTS = 'requests.memory',
  STORAGE_REQUESTS = 'requests.storage',
  PVC = 'persistentvolumeclaims',
  NVIDIA_GPU = 'nvidia.com/gpu',
  AMD_GPU = 'amd.com/gpu',
  VIRTUAL_GPU = 'tencent.com/vcuda-core',
  VIRTUAL_VIDEO_MEMORY = 'tencent.com/vcuda-memory',
}

export interface K8sQuotaType {
  key: string;
  fallbackKey?: string;
  name: string;
  unit: string;
  static?: boolean; // 标识这个 key 是不是前端定死的，如果为 false，需要判断这个key 在后端的capacity中是否存在
  description?: TranslateKey;
}

export type K8sQuotaItem = Partial<Record<QuotaItemType, string>>;

export interface K8sQuotaSpec {
  hard?: K8sQuotaItem;
  scopeSelector?: ScopeSelector;
  scopes?: string[];
}

export interface K8sBaseQuota extends KubernetesResource {
  spec?: K8sQuotaSpec;
  status?: {
    hard?: K8sQuotaItem;
    used?: K8sQuotaItem;
  };
}

export interface ResourceQuota extends K8sBaseQuota {
  kind?: 'ResourceQuota';
}

export interface ProjectQuota extends K8sBaseQuota {
  kind?: 'ProjectQuota';
}

export enum LimitRangeItemType {
  DEFAULT = 'default',
  DEFAULT_REQUEST = 'defaultRequest',
  MAX = 'max',
  MIN = 'min',
}

export type LimitRangeItem = Partial<
  Record<
    LimitRangeItemType,
    {
      cpu?: string;
      memory?: string;
    }
  > & {
    type: 'Container';
  }
>;

export interface LimitRangeSpec {
  limits?: LimitRangeItem[];
}

export interface LimitRange extends KubernetesResource {
  kind?: 'LimitRange';
  spec?: LimitRangeSpec;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface Group extends KubernetesResource {}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface GroupBinding extends KubernetesResource {}

export interface UserBinding extends KubernetesResource {
  spec?: {
    username: string;
  };
}

export interface ProductSpec {
  homepage: string;
  hide: boolean;
}

export interface Idp extends KubernetesResource {
  id: string;
  name: string;
  type: string;
  config: string;
  idpValidation?: {
    username: string;
    password: string;
  };
}

export interface Product extends KubernetesResource {
  kind: 'AlaudaProduct';
  spec: ProductSpec;
}

export interface Status extends TypeMeta {
  kind: 'Status';
  metadata: {
    continue?: string;
  };
  status: string;
  message: string;
  reason: string;
  code: number;
  details: {
    causes: Array<{
      message: string;
    }>;
  };
}

export interface ResourceRatio {
  cpu: number;
  memory: number;
}

export interface FeatureSpec {
  accessInfo: {
    grafanaAdminPassword: string;
    grafanaAdminUser: string;
    grafanaUrl: string;
    name: string;
    namespace: string;
    prometheusTimeout: number;
    prometheusUrl: string;
  };
  instanceType: string;
  type: string;
  deployInfo?: ResourceRatio;
}

export interface Feature extends KubernetesResource {
  spec?: FeatureSpec;
}

export const CRD_FEATURE = 'features.infrastructure.alauda.io';

export interface CustomResourceDefinition extends KubernetesResource {
  kind: 'CustomResourceDefinition';
}

export interface Log extends KubernetesResource {
  kind: string;
  spec?: {
    AUDIT_TTL: number;
    EVENT_TTL: number;
    LOG_TTL: number;
  };
}

export interface RoleRule {
  module: string;
  functionResourceRef: string;
  verbs: string[];
}

export interface RoleCustomRule {
  apiGroup: string;
  resources: string[];
  verbs: string[];
}

export interface RoleTemplateSpec {
  rules?: RoleRule[];
  customRules?: RoleCustomRule[];
}

export interface RoleTemplate extends KubernetesResource {
  spec?: RoleTemplateSpec;
}

export interface FederatedOverrides {
  clusterName: string;
  clusterOverrides: Array<{
    path: string;
    value?: unknown;
  }>;
}

export interface FederatedNamespaceSpec {
  namespace: {
    resource: Namespace;
  };
  resourcequota?: {
    resource?: ResourceQuota;
    overrides?: FederatedOverrides[];
  };
  limitrange?: {
    resource?: LimitRange;
    overrides?: FederatedOverrides[];
  };
  template?: {
    metadata: {
      labels?: StringMap;
    };
  };
}

export interface FederatedNamespace extends KubernetesResource {
  spec?: FederatedNamespaceSpec;
  status?: {
    clusters: Array<{
      name: string;
    }>;
    conditions: Array<{
      status: 'True' | 'False';
      type: 'Propagation';
    }>;
  };
}

export type UnionNamespace = Namespace | FederatedNamespace;

export interface ClusterFedItem {
  name: string;
  type: 'Host' | 'Member';
}

export interface ClusterFedSpec {
  check?: boolean;
  unjoinpolicy?: string;
  clusters: ClusterFedItem[];
}

export interface ClusterFed extends KubernetesResource {
  spec?: ClusterFedSpec;
  status?: {
    phase: K8sResourceStatus;
  };
}

export interface TKEClusterSpecMachine {
  ip?: string;
  error?: string;
  displayName?: string;
  port?: number | string;
  username?: string;
  password?: string;
  privateKey?: string;
  passPhrase?: string;
  labels?: StringMap;
  taints?: NodeTaint[];
}

export interface TKEClusterSpec {
  displayName: string;
  clusterCIDR?: string;
  serviceCIDR?: string;
  networkDevice?: string;
  properties?: {
    maxClusterServiceNum?: number;
    maxNodePodNum?: number;
    oversoldRatio?: {
      cpu: string;
      memory: string;
    };
  };
  clusterCredentialRef?: {
    name: string;
  };
  features?: {
    ipvs?: boolean;
    enableMasterSchedule?: boolean;
    gpuType?: 'Physical' | 'Virtual';
    files?: Array<{
      src?: string;
      dst?: string;
    }>;
    ha?: {
      thirdParty?: {
        vip?: string;
        vport?: number;
      };
    };
    skipConditions?: string[];
    hooks?: {
      PostInstall?: string;
    };
  };
  type: 'Baremetal' | 'Imported';
  version?: string;
  machines?: TKEClusterSpecMachine[];
  dnsDomain?: string;
  apiServerExtraArgs?: StringMap;
  dockerExtraArgs?: StringMap;
  kubeletExtraArgs?: StringMap;
  controllerManagerExtraArgs?: StringMap;
  schedulerExtraArgs?: StringMap;
  publicAlternativeNames?: string[];
}

export interface TKEClusterStatusCondition {
  type: string;
  status: string;
  lastProbeTime: string;
  lastTransitionTime: string;
  reason: string;
}

export interface TKECluster extends KubernetesResource {
  spec?: TKEClusterSpec;
  status?: {
    version?: string;
    phase?: string;
    conditions?: TKEClusterStatusCondition[];
    addresses?: Array<{
      host: string;
      type: 'Advertise';
      port: number;
    }>;
    serviceCIDR?: string;
    nodeCIDRMaskSize?: number;
    dnsIP?: string;
  };
}

export enum K8sResourceStatus {
  EMPTY = '',
  ACTIVE = 'Active',
  CREATING = 'Creating',
  ERROR = 'Error',
  TERMINATING = 'Terminating',
  UNKNOWN = 'Unknown',
  PROCESSING = 'Processing',
  COMPLETED = 'Completed',
  FAILED = 'Failed',
  PENDING = 'Pending',
}

export interface Report extends KubernetesResource {
  spec?: {
    kind?: string;
    type?: string;
    groupBy?: string;
    projects?: string[];
    clusters?: string[];
    namespaces?: string[];
    orderBy?: string;
    startTime?: string;
    endTime?: string;
  };
  status?: {
    phase?: string;
    downloadLink?: string;
    conditions?: Array<{
      lastProbeTime: string;
      lastTransitionTime: string;
      reason: string;
      message: string;
      retryTimes: number;
      status: string;
      type: string;
    }>;
  };
}

export interface ProductCRD extends KubernetesResource {
  spec: {
    group: string;
    names: {
      kind: string;
      plural: string;
    };
    version: string;
  };
}

export interface ProductStatusCondition {
  status?: 'True' | 'False' | 'Unknown';
  reason?: string;
  message?: string;
  type: 'Ready' | 'Initialized' | 'Reclaimed' | 'Succeed';
  lastTransitionTime?: string;
  lastHeartbeatTime?: string;
}

export interface ProductCR extends KubernetesResource {
  spec: { version: string };
  status?: {
    version: string;
    phase?:
      | 'Pending'
      | 'Provisioning'
      | 'Provisioned'
      | 'Deleting'
      | 'Failed'
      | 'Running'
      | 'Installing' // tdsql
      | 'Upgrading';
    productEntrypoint: string;
    deletable: boolean;
    conditions: ProductStatusCondition[];
  };
}

export interface ProductCenter {
  crd: ProductCRD;
  crs: ProductCR[];
  error: string;
}

export interface CspSpec {
  productName?: 'csp';
  productType?: string;
  version?: string;
  installerNamespace?: string;
  highAvailable: boolean;
  machines: [
    {
      type: 'master';
      ip: string;
      port: number;
      password: string;
    },
    {
      type: 'slave';
      ip: string;
      port: number;
      password: string;
    }?,
  ];
  vip?: string;
}

export interface CspStatus {
  conditions: Array<{
    status: 'True';
    reason: string;
    message: string;
    type: 'Ready';
    lastProbeTime: string;
  }>;
  phase?:
    | 'Pending'
    | 'Provisioning'
    | 'Provisioned'
    | 'Deleting'
    | 'Failed'
    | 'Running';
  version: string;
  productEntrypoint: string;
  installerNamespaceName: string;
  lastProbeTime: string;
  deployTime: string;
}

export interface Csp extends KubernetesResource {
  spec: CspSpec;
  status?: CspStatus;
}

export interface WatchResponse<T> {
  type: string;
  object: T;
}

export interface ClusterFeatureSpec {
  type: string;
  accessInfo?: {
    grafanaAdminPassword: string;
    grafanaAdminUser: string;
    grafanaUrl: string;
    name: string;
    namespace: string;
    prometheusTimeout: number;
    prometheusUrl: string;
  };
  instanceType?: string;
  deployInfo?: ResourceRatio;
  version?: string;
}

export interface ClusterFeature extends KubernetesResource {
  spec?: ClusterFeatureSpec;
}

export interface ClusterSpec {
  authInfo?: {
    controller: {
      kind: string;
      namespace: string;
      name: string;
    };
  };
  kubernetesApiEndpoints?: {
    caBundle?: string;
    serverEndpoints: Array<{
      clientCIDR: string;
      serverAddress: string;
    }>;
  };
}

export type ClusterStatusReason =
  | 'Timeout'
  | 'Forbidden'
  | 'Unauthorized'
  | 'SomeComponentsUnhealthy'
  | 'SomeNodesNotReady';

export interface ClusterStatus {
  version: string;
  conditions: [
    {
      lastTransitionTime: string;
      type: 'NotReachable' | 'NotAccessible' | 'ComponentNotHealthy';
      status: 'True' | 'False';
      reason: ClusterStatusReason;
      message: string;
    },
  ];
}

export interface Cluster extends KubernetesResource {
  metadata?: ObjectMeta & {
    finalizers?: string[];
  };
  spec?: ClusterSpec;
  status?: ClusterStatus;
  details?: {
    group: string;
    kind: string;
    name: string;
    uid: string;
  };
}

// Notification
export interface NotificationSubscription {
  method: string;
  receivers: Array<{
    name: string;
    namespace: string;
  }>;
  sender: string;
  template: string;
}

export interface NotificationSpec {
  subscriptions: NotificationSubscription[];
}

export interface Notification extends KubernetesResource {
  kind: string;
  spec?: NotificationSpec;
}

export interface ValueFromSecret {
  namespace: string;
  name: string;
  key: string;
}

export interface NotificationEmailServer {
  host: string;
  port: number;
  username: string;
  password: string;
  valueFromSecret: ValueFromSecret;
  ssl: boolean;
  insecureSkipVerify: boolean;
}

export interface NotificationSmsServer {
  host: string;
  port: number;
  provider: string;
  softVersion: string;
  accountSid: {
    valueFromSecret: ValueFromSecret;
  };
  accountToken: {
    valueFromSecret: ValueFromSecret;
  };
  appId: string;
}

export interface NotificationServer extends KubernetesResource {
  spec?: {
    email?: NotificationEmailServer;
    sms?: NotificationSmsServer;
  };
}

export interface NotificationReceiver extends KubernetesResource {
  spec: {
    destination: string;
  };
}

export interface NotificationTemplate extends KubernetesResource {
  spec: {
    subject?: string;
    content: string;
  };
}

export interface Secret extends KubernetesResource {
  data?: StringMap;
  stringData?: StringMap;
  type?: SecretType;
}

export interface POOL extends KubernetesResource {
  size?: number;
}

export enum SecretType {
  Opaque = 'Opaque',
  TLS = 'kubernetes.io/tls',
  SSHAuth = 'kubernetes.io/ssh-auth',
  BasicAuth = 'kubernetes.io/basic-auth',
  DockerConfigJson = 'kubernetes.io/dockerconfigjson',
  NotificationSender = 'NotificationSender',
}

// http://confluence.alaudatech.com/x/Z4YHAw
// 后端对于 PrometheusRule 的资源定义
export interface PrometheusRuleItemAnnotation extends StringMap {
  // 告警规则触发时的当前值
  alert_current_value?: string;
  // 告警关联的通知的列表
  alert_notifications?: string;
}

export interface PrometheusRuleItem {
  // alert是告警规则名称：<规则名称>-<一个32位MD5值>
  // 规则名称：<指标名称>-<5位随机字符串(小写字母+数字的组合)>
  // MD5值：MD5<resourceName__groupName__ruleName>，是为规则生成一个唯一的名字
  alert: string;
  // Prometheus表达式
  expr: string;
  // 持续时间
  for: string;
  annotations: PrometheusRuleItemAnnotation;
  labels: PrometheusRuleItemLabel;
}

export interface PrometheusRuleItemLabel extends StringMap {
  // 等级
  severity: string;
  // 所属应用的名称
  application?: string;
  // 告警规则名称
  alert_name: string;
  // 所属对象的类型 Deployment/StatefulSet/DaemonSet/Node/Cluster
  alert_involved_object_kind: string;
  // 所属对象的名称
  alert_involved_object_name: string;
  // 所属对象的命名空间 可能为空字符串
  alert_involved_object_namespace: string;
  // 所属集群名称
  alert_cluster: string;
  // 所属对象的项目名称 可能为空字符串
  alert_project: string;
  // 创建人
  alert_creator?: string;
  // 指标名称 预设的告警指标/custom
  alert_indicator: string;
  // 聚合时间
  alert_indicator_aggregate_range?: string;
  // 聚合方式 max/min/avg
  alert_indicator_aggregate_function?: string;
  // 比较方式
  alert_indicator_comparison: string;
  // 阈值
  alert_indicator_threshold: string;
  // 日志告警的查询语句
  alert_indicator_query?: string;
  // 自定义告警必传 可能是空字符串
  alert_indicator_unit?: string;
  alarm_status?: string;
}

export interface PrometheusRuleSpec {
  groups: Array<{
    // 告警规则所属的group的名称，固定为general
    name: string;
    rules: PrometheusRuleItem[];
  }>;
}

export interface PrometheusRule extends KubernetesResource {
  kind: string;
  spec?: PrometheusRuleSpec;
}

export interface AlarmAction {
  name: string;
  namespace: string;
}

export interface AlertTemplateItem {
  name: string;
  compare: string;
  threshold: number;
  unit?: string;
  wait: number;
  notifications: AlarmAction[];
  annotations?: StringMap;
  labels?: StringMap;
  metric: {
    queries: Array<{
      aggregator: string;
      range: number;
      labels: Array<{
        name: string;
        value: string;
      }>;
    }>;
  };
  expr?: string;
  metric_name?: string;
}

export interface AlertTemplateSpec {
  templates: AlertTemplateItem[];
}

export interface AlertTemplate extends KubernetesResource {
  kind: string;
  spec?: AlertTemplateSpec;
}

export interface Node extends KubernetesResource {
  spec?: {
    providerID?: string;
    taints?: NodeTaint[];
    configSource?: string;
    unschedulable?: boolean;
    podCIDR?: string;
    externalID?: string;
    ip?: string;
  };
  status?: NodeStatus;
}

// Cluster
export type NodeStatusReason =
  | 'KubeletHasSufficientDisk'
  | 'KubeletHasSufficientMemory'
  | 'KubeletHasNoDiskPressure'
  | 'KubeletReady';

export type NodeStatusType =
  | 'OutOfDisk'
  | 'MemoryPressure'
  | 'DiskPressure'
  | 'Ready';

export interface NodeStatus {
  volumesAttached: boolean;
  daemonEndpoints: {
    kubeletEndpoint: {
      port: number;
    };
  };
  capacity: {
    'alpha.kubernetes.io/nvidia-gpu': string;
    [QuotaItemType.VIRTUAL_GPU]: string;
    [QuotaItemType.VIRTUAL_VIDEO_MEMORY]: string;
    pods: string;
    cpu: string;
    memory: string;
  };
  addresses: Array<{
    type: string;
    address: string;
  }>;
  allocatable: {
    'alpha.kubernetes.io/nvidia-gpu': string;
    [QuotaItemType.VIRTUAL_GPU]: string;
    [QuotaItemType.VIRTUAL_VIDEO_MEMORY]: string;
    pods: string;
    cpu: string;
    memory: string;
  };
  images: Array<{
    sizeBytes: number;
    names: string[];
  }>;
  nodeInfo: {
    kubeProxyVersion: string;
    operatingSystem: string;
    kernelVersion: string;
    systemUUID: string;
    containerRuntimeVersion: string;
    osImage: string;
    architecture: string;
    bootID: string;
    machineID: string;
    kubeletVersion: string;
  };
  volumesInUse: boolean;
  phase: string;
  conditions: Array<{
    lastHeartbeatTime: string;
    status: string;
    lastTransitionTime: string;
    reason: NodeStatusReason;
    message: string;
    type: NodeStatusType;
  }>;
}

export interface NodeTaint {
  key: string;
  timeAdded?: string;
  effect: string;
  value?: string;
  unschedulable?: boolean;
}

export const FEATURE_RESOURCE_RATIO = 'resourceratio';

export interface TKEClusterMetadata extends ObjectMeta {
  generateName?: string;
}

export interface ClusterCredential extends KubernetesResource {
  metadata?: TKEClusterMetadata;
  clusterName: string;
  caCert: string;
  token: string;
}

export interface TKEMachineSpec {
  clusterName: string;
  ip: string;
  port: number;
  password?: string;
  privateKey?: string;
  passPhrase?: string;
  username: string;
  labels?: {
    ['nvidia-device-enable']?: string;
  };
  type: string;
}

export interface TKEMachine extends KubernetesResource {
  spec?: TKEMachineSpec;
  status?: NodeStatus;
}

export interface ClusterAddOnType extends KubernetesResource {
  type: string;
  level: string;
  latestVersion: string;
  description?: string;
}

export interface ClusterAddOnSpec {
  type: string;
  level: string;
  version: string;
}

export interface ClusterAddOn extends KubernetesResource {
  spec?: ClusterAddOnSpec;
  status?: {
    phase?: string;
  };
}

export interface GroupKind {
  kind: string;
  group: string;
}

export enum ApplicationPhaseEnum {
  Succeeded = 'Succeeded',
  Failed = 'Failed',
  Pending = 'Pending',
}

export interface LabelSelectorRequirement {
  key: string;
  operator?: string;
  values?: string[];
}

export interface LabelSelector {
  matchLabels?: StringMap;
  matchExpressions?: LabelSelectorRequirement[];
}

export interface ApplicationSpec {
  componentKinds: GroupKind[];
  assemblyPhase?: ApplicationPhaseEnum;
  selector?: LabelSelector;
  descriptor?: any;
}

export interface Application extends KubernetesResource {
  spec: ApplicationSpec;
  status: ApplicationStatus;
}

export enum ApplicationStateEnum {
  Pending = 'Pending',
  PartialRunning = 'PartialRunning',
  Running = 'Running',
  Stopped = 'Stopped',
  Empty = 'Empty',
}

export interface ApplicationCondition {
  lastTransitionTime?: string;
  lastUpdateTime?: string;
  message?: string;
  reason?: string;
  status?: string;
  type?: string;
}

export interface GenericWorkloadStatus {
  status?: WorkloadStatusEnum;
  desired?: number;
  current?: number;
}

export interface WorkloadStatus extends GenericWorkloadStatus {
  name?: string;
  group?: string;
  kind?: string;
  messages?: Array<{
    message: string;
    reason?: string;
  }>;
}

export interface ApplicationStatus {
  state: ApplicationStateEnum;
  totalComponents: number;
  conditions?: ApplicationCondition[];
  workloadsStatus?: {
    pending?: number;
    ready?: number;
    stopped?: number;
    workloads?: WorkloadStatus[];
  };
}

export enum VolumeTypeEnum {
  persistentVolumeClaim = 'persistentVolumeClaim',
  configMap = 'configMap',
  secret = 'secret',
  hostPath = 'hostPath',
  emptyDir = 'emptyDir',
}

export interface EmptyDirVolumeSource {
  medium?: string;
  sizeLimit?: string; // eg: 1500m refers to: https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.15/#quantity-resource-core
}
export interface KeyToPath {
  key: string;
  mode?: number;
  path: string;
}

export interface ConfigMapVolumeSource {
  name: string;
  optional?: boolean;
  items?: KeyToPath[];
}

export interface SecretVolumeSource {
  secretName: string;
  optional?: boolean;
  items?: KeyToPath[];
}

export interface HostPathVolumeSource {
  type?: string;
  path: string;
}

export interface PersistentVolumeClaimVolumeSource {
  claimName: string;
  readOnly?: boolean;
}

export interface Volume {
  name: string;
  [VolumeTypeEnum.emptyDir]?: EmptyDirVolumeSource;
  [VolumeTypeEnum.configMap]?: ConfigMapVolumeSource;
  [VolumeTypeEnum.secret]?: SecretVolumeSource;
  [VolumeTypeEnum.persistentVolumeClaim]?: PersistentVolumeClaimVolumeSource;
  [VolumeTypeEnum.hostPath]?: HostPathVolumeSource;
  // other iaas storage definitions not included
}

export interface ConfigMapKeyRef extends LocalObjectReference {
  key: string;
  optional?: boolean;
}

export interface SecretKeyRef extends LocalObjectReference {
  key: string;
  optional?: boolean;
}

export interface ObjectFieldSelector {
  apiVersion?: string;
  fieldPath: string;
}

export interface ResourceFieldSelector {
  containerName?: string;
  divisor?: string;
  resource: string;
}

export interface EnvVarSource {
  configMapKeyRef?: ConfigMapKeyRef;
  secretKeyRef?: SecretKeyRef;
  fieldRef?: ObjectFieldSelector;
  resourceFieldRef?: ResourceFieldSelector;
}

export interface EnvVar {
  name: string;
  value?: string;
  valueFrom?: EnvVarSource;
}

export interface ConfigMapRef extends LocalObjectReference {
  optional?: boolean;
}

export interface SecretRef extends LocalObjectReference {
  optional?: boolean;
}

export interface EnvFromSource {
  prefix?: string;
  configMapRef?: ConfigMapRef;
  secretRef?: SecretRef;
}

export interface ContainerPort {
  name?: string;
  hostPort?: number;
  containerPort?: number;
  protocol?: string;
  hostIP?: string;
}

export interface ResourceQuotaItem {
  pods?: string;
  'limits.cpu'?: string;
  'requests.cpu'?: string;
  'limits.memory'?: string;
  'requests.memory'?: string;
  'requests.storage'?: string;
  persistentvolumeclaims?: string;
}

export interface NodeMetrics extends KubernetesResource {
  usage?: {
    cpu: string;
    memory: string;
  };
  window?: string;
}

export interface ResourceRequirements {
  limits?: StringMap;
  requests?: StringMap;
}

export interface ExecAction {
  command?: string[];
}

export interface HTTPHeader {
  name?: string;
  value?: string;
}

export interface HTTPGetAction {
  host?: string;
  httpHeaders?: HTTPHeader[];
  path: string;
  port?: number | string;
  scheme?: string;
}

export interface TCPSocketAction {
  host?: string;
  port?: number | string;
}

export interface Probe {
  exec?: ExecAction;
  failureThreshold?: number;
  httpGet?: HTTPGetAction;
  initialDelaySeconds?: number;
  periodSeconds?: number;
  successThreshold?: number;
  tcpSocket?: TCPSocketAction;
  timeoutSeconds?: number;
}

export interface Container {
  name?: string;
  image?: string;
  command?: string[];
  args?: string[];
  env?: EnvVar[];
  envFrom?: EnvFromSource[];
  workingDir?: string;
  ports?: ContainerPort[];
  resources?: ResourceRequirements;
  volumeMounts?: any;
  livenessProbe?: Probe;
  readinessProbe?: Probe;
}

export interface WeightedPodAffinityTerm {
  podAffinityTerm: PodAffinityTerm;
  weight: number;
}

export interface PodAffinityTerm {
  labelSelector: LabelSelector;
  topologyKey: string;
  namespaces?: string[];
}

export interface Affinity {
  requiredDuringSchedulingIgnoredDuringExecution?: PodAffinityTerm[];
  preferredDuringSchedulingIgnoredDuringExecution?: WeightedPodAffinityTerm[];
}

export interface PodSpecAffinity {
  podAffinity?: Affinity;
  podAntiAffinity?: Affinity;
  nodeAffinity?: Affinity;
}

export interface LocalObjectReference {
  name: string;
}

export interface PodSpec {
  containers?: Container[];
  initContainers?: Container[];
  volumes?: Volume[];
  nodeSelector?: StringMap;
  hostNetwork?: boolean;
  affinity?: PodSpecAffinity;
  restartPolicy?: string;
  imagePullSecrets?: LocalObjectReference[];
  serviceAccountName?: string;
  serviceAccount?: string;
  nodeName?: string;
}

export interface PodTemplateSpec {
  metadata?: ObjectMeta;
  spec?: PodSpec;
}
export type DaemonSetUpdateStrategyType = 'RollingUpdate' | 'OnDelete';
export interface RollingUpdateDaemonSet {
  maxUnavailable?: string | number;
}
export interface DaemonSetUpdateStrategy {
  type?: DaemonSetUpdateStrategyType;
  rollingUpdate?: RollingUpdateDaemonSet;
}

export interface DaemonSetSpec {
  selector?: LabelSelector;
  template?: PodTemplateSpec;
  updateStrategy?: DaemonSetUpdateStrategy;
  minReadySeconds?: number;
  revisionHistoryLimit?: number;
}

export type DaemonSetStatus = Partial<{
  currentNumberScheduled: number;
  numberMisscheduled: number;
  desiredNumberScheduled: number;
  numberReady: number;
  observedGeneration: number;
  updatedNumberScheduled: number;
  numberUnavailable: number;
  numberAvailable: number;
}>;

export interface DaemonSet extends KubernetesResource {
  spec?: DaemonSetSpec;
  status?: DaemonSetStatus;
}

/**
 * Workload API
 */
export interface Deployment extends KubernetesResource {
  spec?: DeploymentSpec;
  status?: DeploymentStatus;
  generation?: number;
}
export type DeploymentStrategyType = 'RollingUpdate' | 'Recreate';

export interface DeploymentStrategy {
  type?: DeploymentStrategyType;
  rollingUpdate?: RollingUpdateDeployment;
}

export interface RollingUpdateDeployment {
  maxUnavailable?: string | number;
  maxSurge?: string | number;
}

// DeploymentSpec is the specification of the desired behavior of the Deployment.
export interface DeploymentSpec {
  replicas?: number;
  selector?: LabelSelector;
  template?: PodTemplateSpec;
  strategy?: DeploymentStrategy;
  minReadySeconds?: number;
  revisionHistoryLimit?: number;
}

export interface DeploymentStatus {
  availableReplicas?: number;
  collisionCount?: number;
  observedGeneration?: number;
  readyReplicas?: number;
  replicas?: number;
  unavailableReplicas?: number;
  updatedReplicas?: number;
  conditions?: DeploymentCondition[];
}

export interface DeploymentCondition {
  workingDir?: string;
  ports?: ContainerPort[];
  volumeMounts?: any;
  resources?: ResourceRequirements;
}

export interface Pod extends KubernetesResource {
  spec?: PodSpec;
  status?: PodStatus;
}

export interface PodStatus {
  conditions?: PodCondition[];
  containerStatuses?: ContainerStatus[];
  hostIP?: string;
  initContainerStatuses?: ContainerStatus[];
  message?: string;
  nominatedNodeName?: string;
  phase?: string;
  podIP?: string;
  qosClass?: string;
  reason?: string;
  startTime?: string;
}

export interface PodCondition {
  lastProbeTime?: string;
  lastTransitionTime?: string;
  message?: string;
  reason?: string;
  status?: string;
  type?: string;
}

export interface Job extends KubernetesResource {
  spec?: JobSpec;
  status?: JobStatus;
}

// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.11/#jobspec-v1-batch
export interface JobSpec {
  activeDeadlineSeconds?: number | '';
  backoffLimit?: number | '';
  completions?: number | '';
  manualSelector?: boolean;
  parallelism?: number | '';
  selector?: LabelSelector;
  template?: PodTemplateSpec;
  startingDeadlineSeconds?: number | '';
}

// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.11/#jobtemplatespec-v1beta1-batch
export interface JobTemplateSpec {
  metadata?: ObjectMeta;
  spec?: JobSpec;
}

export interface JobCondition {
  lastProbeTime: string;
  lastTransitionTime: string;
  message: string;
  reason: string;
  status: string;
  type: string;
}

export interface JobStatus {
  active: number | '';
  completionTime: string;
  conditions: JobCondition[];
  startTime: string;
  succeeded: number;
  failed: number;
}

export interface CronJobSpec {
  concurrencyPolicy?: string;
  failedJobsHistoryLimit?: number | '';
  schedule?: string;
  startingDeadlineSeconds?: number | '';
  successfulJobsHistoryLimit?: number | '';
  suspend?: boolean;
  jobTemplate?: JobTemplateSpec;
}

export interface ObjectReference {
  apiVersion: string;
  fieldPath: string;
  kind: string;
  name: string;
  namespace: string;
  resourceVersion: string;
  uid: string;
}
export interface CronJobStatus {
  active?: ObjectReference[];
  lastScheduleTime: string;
}
export interface CronJob extends KubernetesResource {
  spec?: CronJobSpec;
  status?: CronJobStatus;
}

export interface StatefulSet extends KubernetesResource {
  spec?: StatefulSetSpec;
  status?: StatefulSetStatus;
}

export type StatefulSetUpdateStrategyType = 'RollingUpdate' | 'OnDelete';
// Partition indicates the ordinal at which the StatefulSet should be
// partitioned.
// Default value is 0.
export interface RollingUpdateStatefulSetStrategy {
  partition?: string | number;
}
export interface StatefulSetUpdateStrategy {
  type?: StatefulSetUpdateStrategyType;
  rollingUpdate?: RollingUpdateStatefulSetStrategy;
}

export interface StatefulSetSpec {
  replicas?: number;
  selector?: LabelSelector;
  template?: PodTemplateSpec;

  // serviceName is the name of the service that governs this StatefulSet.
  // This service must exist before the StatefulSet, and is responsible for
  // the network identity of the set. Pods get DNS/hostnames that follow the
  // pattern: pod-specific-string.serviceName.default.svc.cluster.local
  // where "pod-specific-string" is managed by the StatefulSet controller.
  serviceName?: string;
  updateStrategy?: StatefulSetUpdateStrategy;
  revisionHistoryLimit?: number;
}

export type StatefulSetStatus = Partial<{
  collisionCount: number;
  observedGeneration: number;
  replicas: number;
  readyReplicas: number;
  currentReplicas: number;
  currentRevision: string;
  updateRevision: string;
}>;

export interface TAppSpec {
  replicas?: number;
  forceDeletePod?: boolean;
  updateStrategy?: { maxUnavailable: number | string };
  template?: PodTemplateSpec;
  templatePool?: Record<string, PodTemplateSpec>;
  templates?: Record<string, string>;
  selector?: LabelSelector;
  statuses?: Record<string, string>;
}

export interface TAppStatus {
  appStatus: string;
  observedGeneration?: number;
  replicas: number;
  readyReplicas?: number;
  scaleLabelSelector?: string;
  statuses: { [key: number]: string };
}

export interface TApp extends KubernetesResource {
  spec?: TAppSpec;
  status?: TAppStatus;
}

export interface CrossVersionObjectReference {
  apiVersion: string;
  kind: string;
  name: string;
}

export interface ExternalMetricSource {
  metric?: MetricIdentifier;
  target?: MetricTarget;
}

export interface ObjectMetricSource {
  describedObject?: CrossVersionObjectReference;
  metric?: MetricIdentifier;
  target?: MetricTarget;
}

export interface PodsMetricSource {
  metric?: MetricIdentifier;
  target?: MetricTarget;
}

export interface ResourceMetricSource {
  name: string;
  target?: MetricTarget;
}

export interface MetricIdentifier {
  name: string;
  selector?: LabelSelector;
}

export interface MetricTarget {
  averageUtilization?: number;
  averageValue?: string;
  type: string;
  value?: string;
}

export interface MetricSpec {
  external?: ExternalMetricSource;
  object?: ObjectMetricSource;
  resource?: ResourceMetricSource;
  pods?: PodsMetricSource;
  type: string;
}

export interface HorizontalPodAutoscalerSpec {
  maxReplicas?: number;
  minReplicas?: number;
  targetCPUUtilizationPercentage?: string;
  scaleTargetRef?: CrossVersionObjectReference;
  metrics?: MetricSpec[]; // appears in v2beta2
}

export interface CronHorizontalPodAutoscalerRule {
  schedule: string;
  targetReplicas: number;
}

export interface CronHorizontalPodAutoscalerSpec {
  crons: CronHorizontalPodAutoscalerRule[];
  scaleTargetRef?: CrossVersionObjectReference;
}

export interface CronHorizontalPodAutoscaler extends KubernetesResource {
  spec: CronHorizontalPodAutoscalerSpec;
}

export interface HorizontalPodAutoscaler extends KubernetesResource {
  spec: HorizontalPodAutoscalerSpec;
  status?: any;
}

export interface HpaConfigResource {
  hpa: HorizontalPodAutoscaler;
  cronHpa: CronHorizontalPodAutoscaler;
}

export interface FederatedAppTemplatesResource extends KubernetesResource {
  spec?: FederatedAppTemplatesSpec;
}

export enum ClusterOverrideOp {
  Add = 'add',
  Remove = 'remove',
  Replace = 'replace',
}

export interface ClusterOverride {
  op?: ClusterOverrideOp;
  path: string;
  value?: any;
}

export interface FederatedAppOverride {
  clusterName: string;
  clusterOverrides: ClusterOverride[];
}

export interface FederatedAppComponent {
  resource: KubernetesResource;
  overrides: FederatedAppOverride[];
}

export interface FederatedAppTemplate extends KubernetesResource {
  spec: {
    componentTemplates: FederatedAppComponent[];
    selector?: LabelSelector;
    descriptor?: unknown;
  };
}

export interface FederatedAppTemplatesSpec {
  placement: {
    clusterSelector: LabelSelector;
  };
  overrides: FederatedAppOverride[];
  template: FederatedAppTemplate;
}

export interface ContainerStatus {
  containerID?: string;
  image?: string;
  imageID?: string;
  lastState?: ContainerState;
  name?: string;
  ready?: boolean;
  restartCount?: number;
  state?: ContainerState;
}

export interface ContainerState {
  running?: ContainerStateRunning;
  terminated?: ContainerStateTerminated;
  waiting?: ContainerStateWaiting;
}

export interface ContainerStateRunning {
  startedAt?: string;
}

export interface ContainerStateTerminated {
  containerID?: string;
  exitCode?: number;
  finishedAt?: string;
  message?: string;
  reason?: string;
  signal?: number;
  startedAt?: string;
}

export interface ContainerStateWaiting {
  message?: string;
  reason?: string;
}

export interface ConfigMap extends KubernetesResource {
  data?: StringMap;
}

export interface ArchiveLog extends KubernetesResource {
  spec?: {
    type: string;
    queryString: string;
    fileType: string;
    fields: string[];
  };
  status?: {
    phase?: string;
    downloadLink?: string;
    conditions?: Array<{
      lastProbeTime: string;
      lastTransitionTime: string;
      reason: string;
      message: string;
      retryTimes: number;
      status: string;
      type: string;
    }>;
  };
}
