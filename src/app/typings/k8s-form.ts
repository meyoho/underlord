import { KubernetesResource } from '@alauda/common-snippet';

import { Namespace, Project, RoleTemplate } from './raw-k8s';

export interface FunctionFrom {
  name: string;
  verbs: string[];
}

export interface RoleRuleForm {
  module: string;
  custom?: boolean;
  functions: FunctionFrom[];
}

export interface RoleTemplateSpecForm {
  rules?: RoleRuleForm[];
  customRules?: RoleRuleForm[];
}

export interface RoleGroup {
  group: string;
  items: RoleTemplate[];
}

export interface UserbindingFormModel {
  role: RoleTemplate;
  project: Project;
  namespace: Namespace;
}

export interface NotificationSubscriptionFormModel {
  method: string;
  receivers: string[];
  sender: string;
  template: string;
}

export interface NotificationReceiverFormModel {
  method: string;
  displayName: string;
  email: string;
  sms: string;
  dingtalk: string;
  wechat: string;
  webhook: string;
  apiVersion: string;
  kind: string;
  name: string;
  namespace: string;
  resourceVersion: string;
}

export interface ApplicationFormModel extends KubernetesResource {
  spec: {
    componentTemplates: KubernetesResource[];
  };
}
