import { K8sResourceAction, KubernetesResource } from '@alauda/common-snippet';

import {
  DaemonSet,
  Deployment,
  K8sQuotaItem,
  ProductCenter,
  StatefulSet,
  TApp,
} from './raw-k8s';

interface RoleRule {
  name: string;
  verbs: string[];
  resourceNames: string[];
}

interface DisplayItem {
  name: string;
  displayName: string;
}

interface MeterUsed {
  cpu: number;
  memory: number;
}

export interface RoleRuleGroup extends KubernetesResource {
  group: string;
  resources: RoleRule[];
}

// https://godoc.org/github.com/coreos/dex/connector/ldap
export interface LdapConfig {
  host: string;
  insecureNoSSL?: boolean;
  insecureSkipVerify?: boolean;
  startTLS?: boolean;
  rootCA?: string;
  rootCAData?: string;
  bindDN: string;
  bindPW: string;
  usernamePrompt?: string;
  userSearch: LdapUserSearch;
  groupSearch?: LdapGroupSearch;
}

export interface LdapUserSearch {
  baseDN: string;
  filter: string;
  scope?: string;
  username: string;
  idAttr: string;
  emailAttr: string;
  nameAttr: string;
}

export interface LdapGroupSearch {
  baseDN: string;
  filter: string;
  scope?: string;
  userAttr: string;
  groupAttr: string;
  nameAttr: string;
}

// https://godoc.org/github.com/coreos/dex/connector/oidc
export interface OidcConfig {
  issuer: string;
  clientID: string;
  clientSecret: string;
  redirectURI: string;
  scopes?: string[];
}

export interface AvailableResourceCapacity {
  cpu: string;
  'ephemeral-storage': string;
  'hugepages-1Gi': string;
  'hugepages-2Mi': string;
  memory: string;
  pods: string;
}

export interface AvailableResource extends KubernetesResource {
  status?: {
    capacity?: AvailableResourceCapacity;
    allocatable?: K8sQuotaItem;
    used?: K8sQuotaItem;
  };
}

export interface MeterQueryParams {
  type: string;
  groupBy?: string;
  startTime: string;
  endTime: string;
  top?: number;
  project?: string;
  cluster?: string;
  namespace?: string;
  orderBy?: string;
  classifyBy?: string;
  page?: string;
  pageSize?: string;
}

export interface MeterTop {
  topN: Array<{
    project: DisplayItem;
    cluster: DisplayItem;
    namespace: DisplayItem;
    meter: MeterUsed;
    startTime: string;
    endTime: string;
  }>;
  remain: {
    meter: MeterUsed;
    startTime: string;
    endTime: string;
  };
}

export interface MeterSummary {
  next: string;
  previous: string;
  page_size: number;
  count: number;
  num_pages: number;
  results: Array<{
    project?: DisplayItem;
    meter: MeterUsed;
    month: string;
    date: string;
    startTime: string;
    endTime: string;
  }>;
}

export interface ProductCenterList {
  kind: string;
  metadata: {
    resourceVersion: string;
  };
  items: ProductCenter[];
}

export interface UserRole extends KubernetesResource {
  name?: string;
  roles?: string[];
}

/**
 * API resource types
 * refers to: http://confluence.alaudatech.com/pages/viewpage.action?pageId=44663323#id-%E6%96%B0%E5%BA%94%E7%94%A8API-1.2.0GET/acp/v1/resources/{cluster}/resourcetypes
 */
export interface APIResource {
  kind: string;
  name: string;
  singularName?: string;
  shortNames?: string[];
  namespaced: boolean;
  verbs: K8sResourceAction[];
  // copied from APIResourceList
  groupVersion?: string;
}

export interface APIResourceList {
  kind: 'APIResourceList';
  apiVersion?: string;
  groupVersion: string;
  resources: APIResource[];
}

/**
 * application address
 * refers to: http://confluence.alaudatech.com/pages/viewpage.action?pageId=44663323#id-%E6%96%B0%E5%BA%94%E7%94%A8API-1.1.6GET/acp/v1/kubernetes/%3Ccluster_name%3E/namespaces/%3Cnamespace%3E/applications/%3Cname%3E/address
 */
export interface ApplicationAddress {
  workloads?: {
    [key: string]: {
      [key: string]: WorkloadAddress[];
    };
  };
}

export interface WorkloadAddress {
  name?: string;
  protocol?: string;
  host?: string;
  frontend?: number;
  port?: number;
  url?: string;
}

export interface LogInfo {
  podName: string;
  containerName: string;
  initContainerName: string;
  fromDate: string;
  toDate: string;
  truncated: boolean;
}
export interface LogLine {
  timestamp: string;
  content: string;
}

export interface LogSelection {
  logFilePosition: string;
  referencePoint: LogLineReference;
  offsetFrom: number;
  offsetTo: number;
}

export interface LogLineReference {
  timestamp: string;
  lineNum: number;
}

export interface ContainerLog {
  info: LogInfo;
  logs: LogLine[];
  selection: LogSelection;
}

export interface ContainerLogParams {
  previous?: string;
  logFilePosition?: string;
  referenceTimestamp?: string;
  referenceLineNum?: string;
  offsetFrom?: string;
  offsetTo?: string;
}

/**
 * Topology Resources
 * refers to: http://confluence.alaudatech.com/pages/viewpage.action?pageId=44663323#id-%E6%96%B0%E5%BA%94%E7%94%A8API-1.1.8GET/acp/v1/kubernetes/%3Ccluster%3E/topology/%3Cnamespace%3E/%3Ckind%3E/%3Cname%3E
 */
export interface TopologyResponse {
  nodes: {
    [key: string]: KubernetesResource;
  };
  edges: TopologyEdge[];
}

export interface TopologyEdge {
  type: string;
  from: string;
  to: string;
}

export interface Workloads {
  Deployment?: Deployment[];
  DaemonSet?: DaemonSet[];
  StatefulSet?: StatefulSet[];
  TApp?: TApp[];
}
