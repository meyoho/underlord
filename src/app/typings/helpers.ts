import { KubernetesResource, ResourceListParams } from '@alauda/common-snippet';
import { safeDump } from 'js-yaml';

export const getResourceYaml = <T extends KubernetesResource>(resource: T) =>
  safeDump(resource, { sortKeys: true });

export type NumberMap = Record<string | number, number>;

export enum ActionType {
  CREATE = 'create',
  UPDATE = 'update',
  IMPORT = 'import',
}

export interface WorkspaceBaseParams {
  project?: string;
  cluster?: string;
  namespace?: string;
}

/**
 * Console related status.
 *
 * All feature related status should be mapped into one of the categories
 */
export enum UnifiedStatus {
  SUCCESS = 'SUCCESS',
  ERROR = 'ERROR',
  PENDING = 'PENDING',
  IN_PROGRESS = 'IN_PROGRESS',
  INACTIVE = 'INACTIVE',
}

export interface WorkspaceDetailParams extends WorkspaceBaseParams {
  name?: string;
}

export interface WorkspaceListParams
  extends WorkspaceBaseParams,
    ResourceListParams {}
