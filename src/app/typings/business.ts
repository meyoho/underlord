import { ResourceListParams } from '@alauda/common-snippet';

export interface ResourceBaseParams {
  project?: string;
  cluster?: string;
  namespace?: string;
}

export interface ResourceDetailParams extends ResourceBaseParams {
  name?: string;
}

export interface ProjectListParams
  extends ResourceBaseParams,
    ResourceListParams {}
