export interface AccountInfo {
  iss?: string;
  sub?: string;
  aud?: string;
  exp?: number;
  iat?: number;
  azp?: string;
  at_hash?: string;
  email?: string;
  email_verified?: true;
  name?: string;
  token?: string;
  ext?: {
    is_admin?: boolean;
    conn_id?: boolean;
  };
}
