import {
  FeatureGateService,
  NavConfigLoaderService,
  ObservableInput,
  Reason,
  TranslateService,
  createRecursiveFilter,
  publishRef,
} from '@alauda/common-snippet';
import { NavItemConfig } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import { Injector, OnDestroy, OnInit, isDevMode } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Observable, Subject, combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  map,
  startWith,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { ViewPermissionsService } from 'app/services/view-permissions.service';

import {
  TemplateHolderType,
  UiStateService,
} from './services/ui-state.service';
export abstract class BaseLayoutComponent implements OnInit, OnDestroy {
  protected navConfig: NavItemConfig[];

  @ObservableInput(true, [])
  protected readonly navConfig$: Observable<NavItemConfig[]>;

  onDestroy$ = new Subject<void>();
  flattenNavConfig$: Observable<NavItemConfig[]>;
  activatedKey$: Observable<string>;
  navConfigAddress: string;
  isLocalNavConfig = true;

  http = this.injector.get(HttpClient);
  router = this.injector.get(Router);
  title = this.injector.get(Title);
  translate = this.injector.get(TranslateService);
  activatedRoute = this.injector.get(ActivatedRoute);
  uiState = this.injector.get(UiStateService);
  fgService = this.injector.get(FeatureGateService);
  navLoader = this.injector.get(NavConfigLoaderService);

  viewPermissions$ = this.injector.get(ViewPermissionsService).viewPermissions$;

  noPermission = Reason.NoPermission;
  noPermissionDesc = 'no_view_permissions';

  pageHeaderContentTemplate$ = this.uiState.getTemplateHolder(
    TemplateHolderType.PageHeaderContent,
  ).templatePortal$;

  constructor(protected injector: Injector) {}

  ngOnInit() {
    this.uiState.activeItemInfo$
      .pipe(
        map(activeItemInfo => activeItemInfo.label),
        takeUntil(this.onDestroy$),
      )
      .subscribe(title => this.title.setTitle(title));

    this.flattenNavConfig$ = this.isLocalNavConfig
      ? this.getLocalNavConfig()
      : this.navLoader
          .loadNavConfig(this.navConfigAddress)
          .pipe(
            this.navLoader.parseYaml(),
            this.navLoader.mapToAuiNav(),
            publishRef(),
          );

    this.activatedKey$ = combineLatest([
      this.flattenNavConfig$,
      this.router.events.pipe(
        filter(event => event instanceof NavigationEnd),
        startWith(this.router.url),
        distinctUntilChanged(),
      ),
    ]).pipe(
      map(([flattenNavConfig]) => this.getActivatedConfig(flattenNavConfig)),
      filter(config => !!config),
      tap(config => this.uiState.setActiveItemInfo(config)),
      map(config => config.key),
      publishRef(),
    );
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  flatNavItemConfigs(configs: NavItemConfig[]): NavItemConfig[] {
    return configs.reduce((prev, curr) => {
      return prev.concat(
        curr.children && !curr.notFlat
          ? this.flatNavItemConfigs(curr.children)
          : curr,
      );
    }, []);
  }

  onActivatedItemChange(config: NavItemConfig) {
    const href = this.getConfigUrl(config);
    if (href.startsWith('http://') || href.startsWith('https://')) {
      window.open(href, '_blank');
    } else {
      this.router.navigateByUrl(href);
    }
  }

  onPageAnimationStart() {
    // A dirty fix to make sure when router component changes, the page keeps at
    // the top.
    document.querySelector('.aui-page__content').scrollTo(0, 0);
  }

  filterFg = (items: NavItemConfig[]) => {
    return this.fgService.filterEnabled(
      items || [],
      item => item.gate,
      '',
      createRecursiveFilter(
        item => item.children,
        (item, children) => ({ ...item, children }),
      ),
    );
  };

  getLocalNavConfig() {
    return combineLatest([this.translate.locale$, this.navConfig$]).pipe(
      map(([_, navConfig]) =>
        this.flatNavItemConfigs(navConfig).map(config =>
          this.translateNavLabel(config),
        ),
      ),
      publishRef(),
    );
  }

  protected getConfigUrl(config: NavItemConfig) {
    return config.href;
  }

  private translateNavLabel(config: NavItemConfig): NavItemConfig {
    return {
      ...config,
      label: this.translate.get(config.label),
      children: config.children?.map(item => this.translateNavLabel(item)),
    };
  }

  // eslint-disable-next-line sonarjs/cognitive-complexity
  private getActivatedConfig(configs: NavItemConfig[]) {
    for (const config of configs) {
      if (isDevMode() && !config.href && !config.children) {
        throw new Error('Invalid config without href nor children');
      }

      if (config.href) {
        if (this.router.isActive(this.getConfigUrl(config), false)) {
          return config;
        }
      } else {
        const item = config.children.find(item => {
          if (isDevMode() && !item.href) {
            throw new Error('Invalid sub config without href');
          }
          return this.router.isActive(this.getConfigUrl(item), false);
        });

        if (item) {
          return item;
        }
      }
    }
  }
}
