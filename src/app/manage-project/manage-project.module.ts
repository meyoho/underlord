import { CommonLayoutModule } from '@alauda/common-snippet';
import { PageModule, PlatformNavModule } from '@alauda/ui';
import { PortalModule } from '@angular/cdk/portal';
import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { ManageProjectRoutingModule } from './manage-project-routing.module';
import { ManageProjectComponent } from './manage-project.component';

@NgModule({
  imports: [
    PageModule,
    PlatformNavModule,
    SharedModule,
    PortalModule,
    CommonLayoutModule,

    ManageProjectRoutingModule,
  ],
  declarations: [ManageProjectComponent],
})
export class ManageProjectModule {}
