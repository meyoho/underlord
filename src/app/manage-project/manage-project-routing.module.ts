import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ManageProjectComponent } from './manage-project.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'project',
        component: ManageProjectComponent,
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'detail',
          },
          {
            path: '**',
            redirectTo: 'manage-platform',
          },
          {
            path: 'detail',
            loadChildren: () =>
              import('./features/project/module').then(M => M.ProjectModule),
          },
          {
            path: 'namespace',
            loadChildren: () =>
              import('./features/namespace/namespace.module').then(
                M => M.NamespaceModule,
              ),
          },
          {
            path: 'federated_namespace',
            // eslint-disable-next-line sonarjs/no-identical-functions
            loadChildren: () =>
              import('./features/namespace/namespace.module').then(
                M => M.NamespaceModule,
              ),
          },
          {
            path: 'member',
            loadChildren: () =>
              import('./features/project-member/module').then(
                M => M.ProjectMemberModule,
              ),
          },
        ],
      },
    ]),
  ],
})
export class ManageProjectRoutingModule {}
