import { get } from 'lodash-es';

import {
  K8sBaseQuota,
  K8sQuotaItem,
  K8sQuotaType,
  QuotaItemType,
} from 'app/typings';
import { toUnitGi, toUnitI, toUnitNum } from 'app/utils';

export const NORMALIZE_MAPPER = {
  pods: toUnitNum,
  persistentvolumeclaims: toUnitNum,
  'limits.cpu': toUnitNum,
  'requests.cpu': toUnitNum,
  'requests.memory': toUnitGi,
  'limits.memory': toUnitGi,
  'requests.storage': toUnitGi,
  [QuotaItemType.AMD_GPU]: toUnitNum,
  [QuotaItemType.NVIDIA_GPU]: toUnitNum,
  [QuotaItemType.VIRTUAL_GPU]: toUnitNum,
  [QuotaItemType.VIRTUAL_VIDEO_MEMORY]: toUnitNum,
};

export const NORMALIZE_MAPPER_NUM = {
  ...NORMALIZE_MAPPER,
  'requests.memory': toUnitI,
  'limits.memory': toUnitI,
  'requests.storage': toUnitI,
};

export function getQuotaValue<T = string>(
  quotaType: K8sQuotaType,
  mapper: Record<string, T>,
) {
  return quotaType && (mapper[quotaType.key] || mapper[quotaType.fallbackKey]);
}

export function getNormalizedQuotaValue(
  quota: K8sQuotaItem,
  quotaType: K8sQuotaType,
  preferNum?: true,
): string;
export function getNormalizedQuotaValue(
  quota: K8sBaseQuota,
  quotaType: K8sQuotaType,
  key: string,
  fallback?: string,
): string;
export function getNormalizedQuotaValue(
  quota: K8sBaseQuota | K8sQuotaItem,
  quotaType: K8sQuotaType,
  preferNumOrKey?: true | string,
  fallback: string = null,
) {
  if (!quota) {
    return fallback;
  }
  const normalize =
    getQuotaValue(
      quotaType,
      preferNumOrKey === true ? NORMALIZE_MAPPER_NUM : NORMALIZE_MAPPER,
    ) || toUnitNum;
  const usedValue = getQuotaValue(
    quotaType,
    preferNumOrKey && preferNumOrKey !== true
      ? get(quota, preferNumOrKey, {})
      : quota,
  );
  return normalize(usedValue) || fallback;
}
