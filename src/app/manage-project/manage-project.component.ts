import { NavItemConfig } from '@alauda/ui';
import { animate, style, transition, trigger } from '@angular/animations';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import {
  ActivatedRoute,
  PRIMARY_OUTLET,
  UrlSegment,
  UrlTree,
} from '@angular/router';
import { map, tap } from 'rxjs/operators';

import { BaseLayoutComponent } from 'app/base-layout-component';

@Component({
  templateUrl: 'manage-project.component.html',
  styleUrls: ['manage-project.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('zoomInOut', [
      transition('* => *', [
        style({ transform: 'scale(0.99) translateY(5px)', opacity: '0.1' }),
        animate(
          '0.3s ease',
          style({ transform: 'scale(1) translateY(0)', opacity: '1' }),
        ),
      ]),
    ]),
  ],
})
export class ManageProjectComponent extends BaseLayoutComponent
  implements OnInit {
  navConfig = [
    {
      icon: 'basic:project_s',
      label: 'project_detail',
      key: 'detail',
      href: 'detail',
    },
    {
      icon: 'basic:member_s',
      label: 'project_member',
      key: 'member',
      href: 'member',
    },
    {
      icon: 'basic:namespace_s',
      label: 'namespace',
      key: 'namespace_group',
      notFlat: true,
      children: [
        {
          label: 'namespace',
          key: 'namespace',
          href: 'namespace',
        },
        {
          label: 'federated_namespace',
          key: 'federated_namespace',
          href: 'federated_namespace',
          gate: 'kubefed',
        },
      ],
    },
  ];

  projectName: string;
  projectName$ = this.route.paramMap.pipe(
    map(params => params.get('project')),
    tap(projectName => (this.projectName = projectName)),
  );

  constructor(injector: Injector, private readonly route: ActivatedRoute) {
    super(injector);
  }

  getConfigUrl(config: NavItemConfig) {
    return `/manage-project/project;project=${this.projectName}/${config.href}`;
  }

  projectSwitchHandle(name: string) {
    this.router.navigateByUrl(this.createUrlTree(name));
  }

  private createUrlTree(project: string): UrlTree {
    const tree = this.router.parseUrl(this.router.url);
    const segments = tree.root.children[PRIMARY_OUTLET].segments;
    segments.forEach((segment: UrlSegment) => {
      if (segment.path === 'project') {
        segment.parameters.project = project;
      }
    });
    return tree;
  }
}
