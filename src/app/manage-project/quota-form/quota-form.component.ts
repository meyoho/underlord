import { ValueHook } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { K8sBaseQuota, K8sQuotaItem } from 'app/typings';
import { DEFAULT } from 'app/utils';

import { CustomResourceData } from './quota-base-from.component';

@Component({
  selector: 'alu-quota-form',
  templateUrl: './quota-form.component.html',
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuotaFormComponent extends BaseResourceFormGroupComponent<
  K8sBaseQuota
> {
  @Input()
  maxLimits: K8sQuotaItem;

  @Input()
  customResourceLimits: CustomResourceData[];

  @Input()
  limitOrDisplay: boolean;

  @Input()
  updateOrCreate: boolean;

  @Input()
  isForProject = false;

  @ValueHook<QuotaFormComponent, 'namespace'>(function (namespace) {
    if (!this.form) {
      return;
    }
    if (this.updateMode || !namespace) {
      return false;
    }
    this.form.patchValue(
      {
        metadata: {
          ...this.form.value.metadata,
          namespace,
        },
      },
      {
        emitEvent: false,
      },
    );
  })
  @Input()
  namespace: string;

  constructor(public injector: Injector, public fb: FormBuilder) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      apiVersion: [''],
      kind: [''],
      metadata: {
        name: '',
        namespace: '',
      },
      spec: this.fb.group({
        hard: this.fb.control({}),
      }),
    });
  }

  getDefaultFormModel() {
    return {
      apiVersion: 'v1',
      kind: 'ResourceQuota',
      metadata: {
        name: DEFAULT,
        namespace: this.namespace,
      },
      spec: {
        hard: {},
      },
    };
  }
}
