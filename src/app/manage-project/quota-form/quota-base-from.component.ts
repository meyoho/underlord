import { StringMap, ValueHook } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { cloneDeep } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { K8sQuotaItem, K8sQuotaType, QuotaItemType } from 'app/typings';
import { K8S_QUOTA_TYPES, isNotBlank, toUnitGi, toUnitNum } from 'app/utils';

export const CUSTOM_RESOURCE_DEFINITIONS: StringMap = {
  'tencent.com/vcuda-core': 'virtual_gpu',
  'tencent.com/vcuda-memory': 'video_memory',
  'nvidia.com/gpu': 'physical_gpu',
  'amd.com/gpu': 'physical_gpu',
};

export interface CustomResourceData {
  key: string;
  descriptionEn: string;
  descriptionZh: string;
}

@Component({
  selector: 'alu-quota-base-form',
  templateUrl: './quota-base-from.component.html',
  styles: [
    `
      :host ::ng-deep .aui-form-item__label-wrapper::after {
        content: ':';
        align-self: flex-start;
        margin-left: 2px;
      }
    `,
  ],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuotaBaseFormComponent extends BaseResourceFormGroupComponent<
  K8sQuotaItem
> {
  k8sQuotaTypes = this.filter(K8S_QUOTA_TYPES);

  @Input()
  maxLimits: K8sQuotaItem = {};

  @ValueHook(function (
    this: QuotaBaseFormComponent,
    customResourceLimits: CustomResourceData[],
  ) {
    if (!customResourceLimits) {
      return false;
    }
    customResourceLimits.forEach(({ key }) => {
      this.form.setControl(`requests.${key}`, this.fb.control(''));
    });
    this.k8sQuotaTypes = [
      ...this.filter(K8S_QUOTA_TYPES),
      ...customResourceLimits.map(({ key, descriptionEn, descriptionZh }) => ({
        key: `requests.${key}`,
        name: CUSTOM_RESOURCE_DEFINITIONS[key],
        unit: 'unit_ge',
        description: {
          zh: descriptionZh,
          en: descriptionEn,
        },
      })),
    ];
  })
  @Input()
  customResourceLimits: CustomResourceData[] = [];

  @Input()
  limitOrDisplay: boolean;

  @Input()
  updateOrCreate: boolean;

  constructor(public injector: Injector) {
    super(injector);
    this.getQuotaLimit = this.getQuotaLimit.bind(this);
  }

  createForm() {
    return this.fb.group({
      [QuotaItemType.CPU_LIMITS]: [''],
      [QuotaItemType.CPU_REQUESTS]: [''],
      [QuotaItemType.MEMORY_LIMITS]: [''],
      [QuotaItemType.MEMORY_REQUESTS]: [''],
      [QuotaItemType.STORAGE_REQUESTS]: [''],
      [QuotaItemType.PVC]: [''],
      [QuotaItemType.PODS]: [''],
    });
  }

  getDefaultFormModel() {
    return {};
  }

  filter(types: K8sQuotaType[]) {
    return types.filter(type => type.static);
  }

  adaptResourceModel(quotaItem: K8sQuotaItem): K8sQuotaItem {
    if (!quotaItem || !this.form) {
      return;
    }
    quotaItem = cloneDeep(quotaItem);
    return Object.keys(quotaItem).reduce((acc, quotaType) => {
      const quota = this.k8sQuotaTypes.find(t => t.key === quotaType);
      return Object.assign(acc, {
        [quotaType]: (quota?.unit === 'unit_Gi' ? toUnitGi : toUnitNum)(
          quotaItem[quotaType as QuotaItemType] ||
            quotaItem[quota?.fallbackKey as QuotaItemType],
        ),
      });
    }, {});
  }

  adaptFormModel(quotaItem: K8sQuotaItem) {
    return Object.entries(quotaItem).reduce<K8sQuotaItem>(
      (acc, [key, value]: [QuotaItemType, string]) => {
        if (
          [QuotaItemType.CPU_REQUESTS, QuotaItemType.MEMORY_REQUESTS].includes(
            key,
          )
        ) {
          return acc;
        }
        const isNotBlankValue = isNotBlank(value);
        value = isNotBlankValue
          ? value
          : this.updateOrCreate
          ? null
          : undefined;
        if (key === QuotaItemType.CPU_LIMITS) {
          acc[QuotaItemType.CPU_REQUESTS] = value;
        } else if (key === QuotaItemType.MEMORY_LIMITS) {
          if (isNotBlankValue) {
            value += 'Gi';
          }
          acc[QuotaItemType.MEMORY_REQUESTS] = value;
        } else if (isNotBlankValue && key === QuotaItemType.STORAGE_REQUESTS) {
          value += 'Gi';
        }
        acc[key] = value;
        return acc;
      },
      {},
    );
  }

  getQuotaLimit(maxLimits: K8sQuotaItem, type: QuotaItemType) {
    const quota = this.k8sQuotaTypes.find(({ key }) => key === type);
    return (quota?.unit === 'unit_Gi' ? toUnitGi : toUnitNum)(
      maxLimits?.[type] || maxLimits?.[quota?.fallbackKey as QuotaItemType],
    );
  }

  trackByKey(quotaType: K8sQuotaType) {
    return quotaType.key;
  }
}
