import { Component, Input } from '@angular/core';

@Component({
  selector: 'alu-ratio-line',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class RatioLineComponent {
  /**
   * -1 represents unlimited
   * NaN represents unavailable
   */
  @Input()
  ratio: number;

  isNaN = Number.isNaN;

  get color() {
    if (this.ratio > 0.9) {
      return '#e54545';
    }

    if (this.ratio > 0.7) {
      return '#ffab00';
    }

    return '#1ba4b3';
  }

  get width() {
    if (this.ratio > 1) {
      return 1;
    }

    return this.ratio;
  }
}
