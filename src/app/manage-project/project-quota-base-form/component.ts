import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';

import { K8sQuotaItem, K8sQuotaType, QuotaItemType } from 'app/typings';
import { K8S_QUOTA_TYPES } from 'app/utils';

import { QuotaBaseFormComponent } from '../quota-form/quota-base-from.component';
import { getNormalizedQuotaValue } from '../utils';

@Component({
  selector: 'alu-project-quota-base-form',
  templateUrl: 'template.html',
  styles: [
    `
      :host ::ng-deep .aui-form-item__label-wrapper::after {
        content: ':';
        align-self: flex-start;
        margin-left: 2px;
      }
    `,
  ],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectQuotaBaseFormComponent extends QuotaBaseFormComponent
  implements OnInit {
  K8S_QUOTA_TYPES = K8S_QUOTA_TYPES;

  @Input()
  isUpdate = false;

  @Input()
  hardQuota: K8sQuotaItem = {};

  @Input()
  allCapacity: K8sQuotaItem;

  @Input()
  allUsed: K8sQuotaItem;

  noLimitKey = [QuotaItemType.PODS, QuotaItemType.PVC];

  constructor(public injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    K8S_QUOTA_TYPES.forEach(type => {
      if (
        !type.static &&
        !Object.prototype.hasOwnProperty.call(this.allCapacity, type.key)
      ) {
        this.form.controls[type.key].disable();
      }
    });
  }

  createForm() {
    const formstate = { value: '' };
    return this.fb.group({
      [QuotaItemType.CPU_LIMITS]: this.fb.control(formstate),
      [QuotaItemType.CPU_REQUESTS]: this.fb.control(formstate),
      [QuotaItemType.MEMORY_LIMITS]: this.fb.control(formstate),
      [QuotaItemType.MEMORY_REQUESTS]: this.fb.control(formstate),
      [QuotaItemType.STORAGE_REQUESTS]: this.fb.control(formstate),
      [QuotaItemType.PVC]: this.fb.control(formstate),
      [QuotaItemType.PODS]: this.fb.control(formstate),
      [QuotaItemType.NVIDIA_GPU]: this.fb.control(formstate),
      [QuotaItemType.AMD_GPU]: this.fb.control(formstate),
      [QuotaItemType.VIRTUAL_GPU]: this.fb.control(formstate),
      [QuotaItemType.VIRTUAL_VIDEO_MEMORY]: this.fb.control(formstate),
    });
  }

  hasLimit(type: QuotaItemType) {
    return !this.noLimitKey.includes(type);
  }

  getMin(type: QuotaItemType) {
    return (
      getNormalizedQuotaValue(
        this.hardQuota,
        K8S_QUOTA_TYPES.find(({ key }) => key === type),
      ) || 0
    );
  }

  filter = (types: K8sQuotaType[]) => {
    return types.filter(
      type =>
        type.static ||
        Object.prototype.hasOwnProperty.call(this.allCapacity, type.key),
    );
  };

  showIcon(name: string) {
    return ['physical_gpu', 'virtual_gpu', 'video_memory'].includes(name);
  }
}
