import {
  K8sApiService,
  TranslateService,
  skipError,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { EMPTY, Observable, Subject, of } from 'rxjs';
import {
  catchError,
  finalize,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { K8sUtilService } from 'app/services/k8s-util.service';
import { LimitRange, Namespace, ResourceBaseParams } from 'app/typings';
import { DEFAULT, RESOURCE_TYPES, ResourceType } from 'app/utils';

@Component({
  templateUrl: './update-limit-range.component.html',
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateLimitRangeComponent implements OnInit, OnDestroy {
  params: ResourceBaseParams;
  limitRange: LimitRange;

  destroy$$ = new Subject<void>();
  loading$$ = new Subject<boolean>();
  loadError$$ = new Subject<boolean>();
  fetch$$ = new Subject<void>();
  action$$ = new Subject<void>();

  data$: Observable<LimitRange> = this.fetch$$.pipe(
    takeUntil(this.destroy$$),
    tap(() => {
      this.loadError$$.next(false);
      this.loading$$.next(true);
    }),
    switchMap(() =>
      this.k8sApi
        .getResource({
          type: RESOURCE_TYPES.LIMIT_RANGE,
          cluster: this.params.cluster,
          namespace: this.params.namespace,
          name: DEFAULT,
        })
        .pipe(
          catchError(e => {
            if (e.code === 404) {
              return of(null);
            }
            this.loadError$$.next(true);
            this.message.error(e.message);
            return EMPTY;
          }),
          finalize(() => this.loading$$.next(false)),
        ),
    ),
    tap(limitRange => this.setLimitRange(limitRange)),
  );

  action$ = this.action$$.pipe(
    takeUntil(this.destroy$$),
    tap(() => this.loading$$.next(true)),
    switchMap(() => {
      const COMMON_PARAMS = {
        type: RESOURCE_TYPES.LIMIT_RANGE,
        cluster: this.params.cluster,
        namespace: this.params.namespace,
        resource: this.limitRange,
      };
      return (this._isCreateOrUpdate
        ? this.k8sApi.postResource(COMMON_PARAMS)
        : this.k8sApi.patchResource(
            Object.assign(COMMON_PARAMS, {
              part: {
                spec: {
                  limits: this.limitRange.spec.limits,
                },
              },
            }),
          )
      ).pipe(
        skipError(),
        finalize(() => this.loading$$.next(false)),
      );
    }),
    tap(limitRange => {
      this.message.success(
        this.translate.get('update_container_limit_range_successed'),
      );
      this.dialogRef.close(limitRange);
    }),
  );

  private _isCreateOrUpdate: boolean;

  constructor(
    public dialogRef: DialogRef<UpdateLimitRangeComponent, LimitRange>,
    private readonly message: MessageService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    private readonly translate: TranslateService,
    @Inject(DIALOG_DATA)
    {
      params,
      cluster,
      namespace,
      limitRange,
    }: {
      params?: ResourceBaseParams;
      cluster?: string;
      namespace?: Namespace;
      limitRange?: LimitRange;
    },
  ) {
    this.params = params || {
      project: this.k8sUtil.getProject(namespace),
      cluster,
      namespace: this.k8sUtil.getName(namespace),
    };
    if (limitRange) {
      this.setLimitRange(limitRange);
    }
  }

  ngOnInit() {
    this.action$.subscribe();

    if (!this.limitRange) {
      this.data$.subscribe();
      this.fetch$$.next();
    }
  }

  ngOnDestroy() {
    this.destroy$$.next();
    this.destroy$$.complete();
  }

  setLimitRange(limitRange: LimitRange) {
    this._isCreateOrUpdate = !limitRange;
    this.limitRange = limitRange;
  }

  updateLimitRange() {
    this.action$$.next();
  }
}
