import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { camelCase, first, get } from 'lodash-es';

import { LimitRange } from 'app/typings';
import { isBlank } from 'app/utils';

export interface LimitRangeItemDef {
  indicator: 'cpu' | 'memory';
  default?: string;
  max?: string;
  defaultUnit?: string;
}

@Component({
  selector: 'alu-limit-range',
  templateUrl: './limit-range.component.html',
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LimitRangeComponent {
  columns = ['indicator', 'default', 'max'];

  @Input()
  set limitRange(limitRange: LimitRange) {
    if (!limitRange) {
      this.limitRangeItemDefs = null;
      return;
    }

    const limits = first(limitRange.spec.limits);
    this.limitRangeItemDefs = [
      {
        indicator: 'cpu',
        default: get(limits, 'default.cpu'),
        max: get(limits, 'max.cpu'),
        defaultUnit: 'unit_core',
      },
      {
        indicator: 'memory',
        default: get(limits, 'default.memory'),
        max: get(limits, 'max.memory'),
      },
    ];
  }

  limitRangeItemDefs: [LimitRangeItemDef, LimitRangeItemDef];

  getLimitRangeItemText(limitRange: LimitRangeItemDef, type: string) {
    return limitRange[camelCase(type) as keyof LimitRangeItemDef] || null;
  }

  getFallbackUnit = (limitRange: LimitRangeItemDef, type: string) => {
    const value = this.getLimitRangeItemText(limitRange, type);
    if (typeof value === 'number' || isBlank(value) || /[A-Za-z]/.test(value)) {
      return;
    }
    return limitRange.defaultUnit;
  };
}
