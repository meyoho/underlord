import { K8sApiService, TranslateService } from '@alauda/common-snippet';
import {
  DIALOG_DATA,
  DialogRef,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import { HttpErrorResponse } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { EMPTY, Subject, of } from 'rxjs';
import {
  catchError,
  finalize,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { CustomResourceData } from 'app/manage-project/quota-form/quota-base-from.component';
import { K8sUtilService } from 'app/services/k8s-util.service';
import {
  K8sQuotaItem,
  Namespace,
  ProjectQuota,
  ResourceDetailParams,
  ResourceQuota,
} from 'app/typings';
import { DEFAULT, RESOURCE_TYPES, ResourceType } from 'app/utils';

import { diffQuotaItem } from '../util';

@Component({
  templateUrl: './update-resource-quota.component.html',
  styleUrls: ['./update-resource-quota.component.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateResourceQuotaComponent implements OnInit, OnDestroy {
  params: ResourceDetailParams;
  maxLimits: K8sQuotaItem;
  customResourceLimits: CustomResourceData[];
  resourceQuota: ResourceQuota;
  updateOrCreate: boolean;

  destroy$$ = new Subject<void>();
  loading$$ = new Subject<boolean>();
  loadError$$ = new Subject<boolean>();
  fetch$$ = new Subject<void>();
  action$$ = new Subject<void>();

  data$ = this.fetch$$.pipe(
    takeUntil(this.destroy$$),
    tap(() => {
      this.loadError$$.next(false);
      this.loading$$.next(true);
    }),
    switchMap(() =>
      this.k8sApi
        .getResource({
          type: RESOURCE_TYPES.RESOURCE_QUOTA,
          cluster: this.params.cluster,
          namespace: this.params.namespace,
          name: DEFAULT,
        })
        .pipe(
          catchError(e => {
            if (e instanceof HttpErrorResponse) {
              if (e.status === 404) {
                return of(null);
              }
              this.loadError$$.next(true);
              this.message.error(e.error.message || e.message);
            }
            return EMPTY;
          }),
          finalize(() => this.loading$$.next(false)),
        ),
    ),
    tap(resourceQuota => this.setResourceQuota(resourceQuota)),
  );

  action$ = this.action$$.pipe(
    takeUntil(this.destroy$$),
    tap(() => this.loading$$.next(true)),
    switchMap(() => {
      const COMMON_PARAMS = {
        type: RESOURCE_TYPES.RESOURCE_QUOTA,
        cluster: this.params.cluster,
        namespace: this.params.namespace,
        resource: this.resourceQuota,
      };
      return (this.updateOrCreate
        ? this.k8sApi.patchResource(
            Object.assign(COMMON_PARAMS, {
              part: {
                spec: {
                  hard: this.resourceQuota.spec.hard,
                },
              },
            }),
          )
        : this.k8sApi.postResource(COMMON_PARAMS)
      ).pipe(
        catchError(e => {
          this.notification.error({
            title: this.translate.get('update_resource_quota_failed'),
            content: e.error.message || e.message,
          });
          return EMPTY;
        }),
        finalize(() => this.loading$$.next(false)),
      );
    }),
    tap(resourceQuota => {
      this.message.success(
        this.translate.get('update_resource_quota_successed'),
      );
      this.dialogRef.close(resourceQuota);
    }),
  );

  constructor(
    public dialogRef: DialogRef<UpdateResourceQuotaComponent, ResourceQuota>,
    private readonly cdr: ChangeDetectorRef,
    private readonly message: MessageService,
    private readonly notification: NotificationService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    private readonly translate: TranslateService,
    @Inject(DIALOG_DATA)
    private readonly data: {
      params?: ResourceDetailParams;
      cluster?: string;
      namespace?: Namespace;
      projectQuota?: ProjectQuota;
      resourceQuota?: ResourceQuota;
      customResourceLimits?: CustomResourceData[];
    },
  ) {
    const {
      params,
      cluster,
      namespace,
      resourceQuota,
      customResourceLimits,
    } = this.data;
    this.params = params || {
      project: this.k8sUtil.getProject(namespace),
      cluster,
      namespace: this.k8sUtil.getName(namespace),
    };
    this.setResourceQuota(resourceQuota);
    this.customResourceLimits = customResourceLimits;
  }

  ngOnInit() {
    this.action$.subscribe();

    if (!this.resourceQuota) {
      this.data$.subscribe();
      this.fetch$$.next();
    }
  }

  ngOnDestroy() {
    this.destroy$$.next();
    this.destroy$$.complete();
  }

  setResourceQuota(resourceQuota: ResourceQuota) {
    this.updateOrCreate = !!resourceQuota;
    this.resourceQuota = resourceQuota;
    this.maxLimits = diffQuotaItem(
      this.data.projectQuota?.spec.hard,
      this.data.projectQuota?.status.hard,
      resourceQuota?.spec.hard,
    );
    this.cdr.markForCheck();
  }

  updateResourceQuota() {
    this.action$$.next();
  }
}
