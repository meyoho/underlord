import { NgModule } from '@angular/core';

import { ManageProjectSharedModule } from 'app/manage-project/shared.module';
import { FeaturesModule } from 'app/shared/features/module';
import { SharedModule } from 'app/shared/shared.module';

import { NamespaceActionComponent } from './action/namespace-action.component';
import { BasicInfoFormComponent } from './basic-info-form/basic-info-form.component';
import { NamespaceBasicInfoComponent } from './basic-info/namespace-basic-info.component';
import { NamespaceDetailInfoComponent } from './detail/namespace-detail-info.component';
import { NamespaceDetailComponent } from './detail/namespace-detail.component';
import { LimitRangeBaseFormComponent } from './limit-range-form/limit-range-base-form.component';
import { LimitRangeFormComponent } from './limit-range-form/limit-range-form.component';
import { LimitRangeComponent } from './limit-range/limit-range.component';
import { NamespaceListComponent } from './list/namespace-list.component';
import { NamespaceTableComponent } from './list/namespace-table.component';
import { NamespaceRoutingModule } from './namespace.routing.module';
import { UpdateLimitRangeComponent } from './update-limit-range/update-limit-range.component';
import { UpdateResourceQuotaComponent } from './update-resource-quota/update-resource-quota.component';

@NgModule({
  imports: [
    SharedModule,
    FeaturesModule,
    ManageProjectSharedModule,
    NamespaceRoutingModule,
  ],
  declarations: [
    NamespaceListComponent,
    NamespaceTableComponent,
    UpdateResourceQuotaComponent,
    UpdateLimitRangeComponent,
    NamespaceActionComponent,
    BasicInfoFormComponent,
    LimitRangeBaseFormComponent,
    LimitRangeFormComponent,
    NamespaceDetailComponent,
    NamespaceDetailInfoComponent,
    NamespaceBasicInfoComponent,
    LimitRangeComponent,
  ],
  entryComponents: [UpdateResourceQuotaComponent, UpdateLimitRangeComponent],
})
export class NamespaceModule {}
