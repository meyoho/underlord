import { ValueHook } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { DEFAULT } from 'app/utils';

@Component({
  selector: 'alu-limit-range-form',
  templateUrl: './limit-range-form.component.html',
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LimitRangeFormComponent extends BaseResourceFormGroupComponent {
  @ValueHook<LimitRangeFormComponent, 'namespace'>(function (namespace) {
    if (!this.form) {
      return;
    }
    if (this.updateMode || !namespace) {
      return false;
    }
    this.form.patchValue(
      {
        metadata: {
          ...this.form.value.metadata,
          namespace,
        },
      },
      {
        emitEvent: false,
      },
    );
  })
  @Input()
  namespace: string;

  constructor(public injector: Injector, public fb: FormBuilder) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      apiVersion: [''],
      kind: [''],
      metadata: {
        name: '',
        namespace: '',
      },
      spec: this.fb.group({
        limits: this.fb.control([]),
      }),
    });
  }

  getDefaultFormModel() {
    return {
      apiVersion: 'v1',
      kind: 'LimitRange',
      metadata: {
        name: DEFAULT,
        namespace: this.namespace,
      },
      spec: {
        limits: [
          {
            type: 'Container',
          },
        ],
      },
    };
  }
}
