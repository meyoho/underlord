import { DeepPartial } from '@alauda/common-snippet';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnDestroy,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { first, get } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { LimitRangeItem } from 'app/typings';
import { addUnitCoreM, addUnitMi, toUnitMi, toUnitNumM } from 'app/utils';

export const LIMIT_RANGE_DEFINITIONS: Record<
  'cpu' | 'memory',
  Array<{
    key: string;
    name?: string;
    unit: string;
  }>
> = {
  cpu: [
    {
      key: 'max',
      unit: 'm',
    },
    {
      key: 'default',
      unit: 'm',
    },
  ],
  memory: [
    {
      key: 'max',
      unit: 'Mi',
    },
    {
      key: 'default',
      unit: 'Mi',
    },
  ],
};

export type LimitRangeFormData = Record<
  'cpu' | 'memory',
  {
    default: string;
    max: string;
  }
>;

const MAX_VALUE = 999_999_999;

@Component({
  selector: 'alu-limit-range-base-form',
  templateUrl: './limit-range-base-form.component.html',
  styleUrls: ['./limit-range-base-form.component.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LimitRangeBaseFormComponent
  extends BaseResourceFormGroupComponent<
    [LimitRangeItem],
    DeepPartial<LimitRangeFormData>
  >
  implements AfterViewInit, OnDestroy {
  limitRangeDefinitions = LIMIT_RANGE_DEFINITIONS;

  MAX_VALUE = MAX_VALUE;

  constructor(public injector: Injector, public fb: FormBuilder) {
    super(injector);
  }

  getMaxValue(type: string, key: string) {
    if (key === 'max') {
      return MAX_VALUE;
    }
    const { controls } = this.form.controls[type] as FormGroup;
    return controls.max.value;
  }

  createForm() {
    return this.fb.group({
      cpu: this.fb.group({
        default: '',
        max: '',
      }),
      memory: this.fb.group({
        default: '',
        max: '',
      }),
    });
  }

  adaptResourceModel(resource: [LimitRangeItem]) {
    const limitRange = first(resource);
    return {
      cpu: {
        default: toUnitNumM(get(limitRange, 'default.cpu')),
        max: toUnitNumM(get(limitRange, 'max.cpu')),
      },
      memory: {
        default: toUnitMi(get(limitRange, 'default.memory')),
        max: toUnitMi(get(limitRange, 'max.memory')),
      },
    };
  }

  adaptFormModel(formModel: LimitRangeFormData): [LimitRangeItem] {
    return [
      {
        type: 'Container',
        default: {
          cpu: addUnitCoreM(formModel.cpu.default),
          memory: addUnitMi(formModel.memory.default),
        },
        max: {
          cpu: addUnitCoreM(formModel.cpu.max),
          memory: addUnitMi(formModel.memory.max),
        },
      },
    ];
  }

  getDefaultFormModel() {
    return {};
  }
}
