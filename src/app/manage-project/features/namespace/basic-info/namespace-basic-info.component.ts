import {
  ANNOTATIONS,
  CLUSTER,
  CREATOR,
  DISPLAY_NAME,
  K8sApiService,
  K8sUtilService,
  LABELS,
  PROJECT,
  UPDATED_AT,
  ValueHook,
} from '@alauda/common-snippet';
import { DialogRef, DialogService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  TemplateRef,
} from '@angular/core';

import { K8sDialogService } from 'app/services/k8s-dialog.service';
import { UnionNamespace } from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';

import { UnionNamespaceBaseParams } from '../detail/namespace-detail.component';

@Component({
  selector: 'alu-namespace-basic-info',
  templateUrl: 'namespace-basic-info.component.html',
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NamespaceBasicInfoComponent {
  @Input()
  params: UnionNamespaceBaseParams;

  @Input()
  canUpdate: boolean;

  @ValueHook<NamespaceBasicInfoComponent, 'namespace'>(function (namespace) {
    if (!namespace) {
      return false;
    }
    this.displayName = this.k8sUtil.getDisplayName(namespace);
  })
  @Input()
  namespace: UnionNamespace;

  @Input()
  admins: string[] = [];

  @Output()
  update = new EventEmitter<UnionNamespace>();

  get resourceType() {
    return this.params.isFederated
      ? RESOURCE_TYPES.FEDERATED_NAMESPACE
      : RESOURCE_TYPES.NAMESPACE;
  }

  dialogRef: DialogRef;

  displayName: string;

  constructor(
    private readonly k8sApi: K8sApiService,
    private readonly k8sDialog: K8sDialogService,
    private readonly k8sUtil: K8sUtilService,
    private readonly dialog: DialogService,
  ) {}

  onUpdate(part: typeof ANNOTATIONS | typeof LABELS) {
    this.k8sDialog
      .updatePart({
        type: this.resourceType,
        cluster: this.params.cluster,
        resource: this.namespace,
        part,
        readonlyKeys:
          part === LABELS
            ? [
                this.k8sUtil.normalizeType(PROJECT),
                this.k8sUtil.normalizeType(CLUSTER),
              ]
            : [
                this.k8sUtil.normalizeType(CREATOR),
                this.k8sUtil.normalizeType(DISPLAY_NAME),
                this.k8sUtil.normalizeType(UPDATED_AT),
              ],
      })
      .subscribe(namespace => this.update.emit(namespace));
  }

  onUpdateDisplayName(updateDisplayName: TemplateRef<any>) {
    this.dialogRef = this.dialog.open(updateDisplayName);
  }

  onSubmit() {
    this.k8sApi
      .patchResource({
        type: this.resourceType,
        cluster: this.params.cluster,
        resource: this.namespace,
        part: {
          metadata: {
            annotations: {
              [this.k8sUtil.normalizeType(DISPLAY_NAME)]: this.displayName,
            },
          },
        },
      })
      .subscribe(namespace => {
        this.dialogRef.close();
        this.update.emit(namespace);
      });
  }
}
