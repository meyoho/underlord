import { getNormalizedQuotaValue } from 'app/manage-project/utils';
import { K8sQuotaItem, QuotaItemType } from 'app/typings';
import { K8S_QUOTA_TYPES, numToStr, toUnitNum } from 'app/utils';

export const diffQuotaItem = (
  spec: K8sQuotaItem,
  status: K8sQuotaItem,
  exist?: K8sQuotaItem,
) => {
  if (!spec) {
    return;
  }

  return Object.keys(spec).reduce<K8sQuotaItem>(
    (remains, quotaType: QuotaItemType) => {
      const quota = K8S_QUOTA_TYPES.find(
        t => t.static && (t.key === quotaType || t.fallbackKey === quotaType),
      );
      if (!quota && !quotaType.startsWith('requests.')) {
        const customQuotaType = `requests.${quotaType}` as QuotaItemType;
        return Object.assign(remains, {
          [customQuotaType]: numToStr(
            parseFloat(toUnitNum(spec[quotaType])) -
              +toUnitNum(status?.[customQuotaType]) +
              +toUnitNum(exist?.[customQuotaType]),
          ),
        });
      }
      return Object.assign(remains, {
        [quotaType]: numToStr(
          // empty string will be converted to NaN which represents no limit
          parseFloat(getNormalizedQuotaValue(spec, quota, true)) -
            // empty string will be converted to 0 which represents no effect
            +getNormalizedQuotaValue(status, quota, true) +
            +getNormalizedQuotaValue(exist, quota, true),
        ),
      });
    },
    {},
  );
};
