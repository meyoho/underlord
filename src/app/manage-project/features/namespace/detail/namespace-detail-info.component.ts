import {
  API_GATEWAY,
  AsyncDataLoader,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  K8sUtilService,
  KubernetesResource,
  ObservableInput,
  PROJECT,
  TranslateService,
  catchPromise,
  matchLabelsToString,
  publishRef,
  skipError,
  wrapText,
} from '@alauda/common-snippet';
import { DialogRef, DialogService, DialogSize } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnDestroy,
  TemplateRef,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { get } from 'lodash-es';
import { EMPTY, Observable, Subject, combineLatest, forkJoin, of } from 'rxjs';
import {
  concatMap,
  map,
  shareReplay,
  startWith,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { AdminApiService } from 'app/api/admin/api';
import { CustomResourceData } from 'app/manage-project/quota-form/quota-base-from.component';
import { PREFIXES } from 'app/services/k8s-util.service';
import { ConfirmDeleteComponent } from 'app/shared/components/confirm-delete/confirm-delete.component';
import {
  ClusterFed,
  ConfigMap,
  FederatedNamespace,
  LimitRange,
  Project,
  ProjectQuota,
  ResourceQuota,
  UnionNamespace,
} from 'app/typings';
import {
  DEFAULT,
  RESOURCE_TYPES,
  ResourceType,
  TYPE,
  getYamlApiVersion,
  unionDisplayName,
} from 'app/utils';

import { UpdateLimitRangeComponent } from '../update-limit-range/update-limit-range.component';
import { UpdateResourceQuotaComponent } from '../update-resource-quota/update-resource-quota.component';

import { UnionNamespaceBaseParams } from './namespace-detail.component';

@Component({
  selector: 'alu-namespace-detail-info',
  templateUrl: 'namespace-detail-info.component.html',
  styleUrls: ['namespace-detail-info.component.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NamespaceDetailInfoComponent implements OnDestroy {
  @Input()
  params: UnionNamespaceBaseParams;

  @ObservableInput(true)
  params$: Observable<UnionNamespaceBaseParams>;

  @Input()
  projectQuota: ProjectQuota;

  destroy$$ = new Subject<void>();

  admins$ = this.params$.pipe(
    switchMap(({ cluster, namespace }) =>
      this.adminApi.getAdmins(RESOURCE_TYPES.NAMESPACE, namespace, {
        key: this.k8sUtil.normalizeType('cluster'),
        operator: '=',
        values: [cluster],
      }),
    ),
  );

  dataLoader = new AsyncDataLoader<UnionNamespace, UnionNamespaceBaseParams>({
    params$: this.params$,
    fetcher: this.getNamespace.bind(this),
  });

  project$ = this.params$.pipe(
    switchMap(({ project }) =>
      this.k8sApi.getGlobalResource<Project>({
        type: RESOURCE_TYPES.PROJECT,
        name: project,
      }),
    ),
  );

  cluster: KubernetesResource;
  cluster$ = this.params$.pipe(
    switchMap(({ isFederated, cluster }) =>
      isFederated
        ? EMPTY
        : this.k8sApi.getGlobalResource({
            type: RESOURCE_TYPES.CLUSTER_REGISTRY,
            name: cluster,
            namespaced: true,
          }),
    ),
    tap(cluster => (this.cluster = cluster)),
    takeUntil(this.destroy$$),
    shareReplay(1),
  );

  createNamespaceDisabled$ = combineLatest([this.project$, this.cluster$]).pipe(
    map(
      ([project, cluster]) =>
        !project.spec.clusters?.find(
          c =>
            c.type === 'FedMember' && c.name === this.k8sUtil.getName(cluster),
        ) || !this.k8sUtil.getLabel(cluster, 'name', 'federation'),
    ),
    takeUntil(this.destroy$$),
    shareReplay(1),
  );

  clusterFedInfo$ = this.cluster$.pipe(
    switchMap(cluster =>
      this.k8sApi.getGlobalResource<ClusterFed>({
        type: RESOURCE_TYPES.CLUSTER_FED,
        name: this.k8sUtil.getLabel(cluster, 'name', 'federation'),
      }),
    ),
    map(clusterFed =>
      [
        this.k8sUtil.getName(clusterFed),
        wrapText(clusterFed.spec.clusters.map(({ name }) => name).join(', ')),
      ].join(' '),
    ),
  );

  resourceQuota?: ResourceQuota;
  resourceQuota$ = this.params$.pipe(
    switchMap(params => this.getResourceQuota(params)),
    publishRef(),
  );

  resourceQuotas$ = combineLatest([this.params$, this.dataLoader.data$]).pipe(
    switchMap(([params, namespace]) =>
      combineLatest(
        (
          get(namespace as FederatedNamespace, ['status', 'clusters']) || []
        ).map(({ name }) =>
          this.getResourceQuota({
            ...params,
            cluster: name,
          }),
        ),
      ),
    ),
    startWith([] as ResourceQuota[]),
    publishRef(),
  );

  limitRange?: LimitRange;
  limitRange$ = this.params$.pipe(
    switchMap(this.getLimitRange.bind(this)),
    publishRef(),
  );

  limitRanges$ = combineLatest([this.params$, this.dataLoader.data$]).pipe(
    switchMap(([params, namespace]) =>
      combineLatest(
        (
          get(namespace as FederatedNamespace, ['status', 'clusters']) || []
        ).map(({ name }) =>
          this.getLimitRange({
            ...params,
            cluster: name,
          }),
        ),
      ),
    ),
    publishRef(),
  );

  deleteDialogRef: DialogRef<ConfirmDeleteComponent>;

  permissions$ = combineLatest([this.params$, this.dataLoader.data$]).pipe(
    switchMap(([{ project, cluster }, namespace]) =>
      this.k8sPermission.isAllowed({
        advanced: true,
        project,
        cluster,
        resource: namespace,
        action: [
          K8sResourceAction.DELETE,
          K8sResourceAction.PATCH,
          K8sResourceAction.UPDATE,
        ],
      }),
    ),
    shareReplay(1),
    takeUntil(this.destroy$$),
  );

  federatedNamespaceCreatePermission$ = this.params$.pipe(
    switchMap(({ project, cluster }) =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.FEDERATED_NAMESPACE,
        advanced: true,
        project,
        cluster,
        action: K8sResourceAction.CREATE,
      }),
    ),
    shareReplay(1),
    takeUntil(this.destroy$$),
  );

  resourceQuotaPermission$ = combineLatest([
    this.params$,
    this.resourceQuota$,
  ]).pipe(
    switchMap(([{ isFederated, project, cluster }, resourceQuota]) =>
      this.k8sPermission.isAllowed({
        advanced: true,
        project,
        cluster,
        type: isFederated
          ? RESOURCE_TYPES.FEDERATED_RESOURCE_QUOTA
          : RESOURCE_TYPES.RESOURCE_QUOTA,
        action: resourceQuota
          ? K8sResourceAction.UPDATE
          : K8sResourceAction.CREATE,
      }),
    ),
    takeUntil(this.destroy$$),
    shareReplay(1),
  );

  limitRangePermission$ = combineLatest([this.params$, this.limitRange$]).pipe(
    switchMap(([{ isFederated, project, cluster }, limitRange]) =>
      this.k8sPermission.isAllowed({
        advanced: true,
        project,
        cluster,
        type: isFederated
          ? RESOURCE_TYPES.FEDERATED_LIMIT_RANGE
          : RESOURCE_TYPES.LIMIT_RANGE,
        action: limitRange
          ? K8sResourceAction.UPDATE
          : K8sResourceAction.CREATE,
      }),
    ),
    takeUntil(this.destroy$$),
    shareReplay(1),
  );

  customResourceLimits: CustomResourceData[];
  customResourceLimits$ = this.params$.pipe(
    switchMap(({ cluster }) =>
      this.k8sApi.getResourceList<ConfigMap>({
        type: RESOURCE_TYPES.CONFIG_MAP,
        cluster,
        namespace: 'kube-public',
        queryParams: {
          labelSelector: matchLabelsToString({
            [this.k8sUtil.normalizeType(
              TYPE,
              'features',
            )]: 'CustomResourceLimitation',
          }),
        },
      }),
    ),
    map(({ items }) => items.map(item => item.data)),
    tap(
      customResourceLimits =>
        (this.customResourceLimits = (customResourceLimits as unknown) as CustomResourceData[]),
    ),
    publishRef(),
  );

  dialogRef: DialogRef;

  unionDisplayName = unionDisplayName;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly http: HttpClient,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sUtil: K8sUtilService,
    private readonly adminApi: AdminApiService,
  ) {
    this.backToList = this.backToList.bind(this);
    this.getClusterNames = this.getClusterNames.bind(this);
  }

  ngOnDestroy() {
    this.destroy$$.next();
    this.destroy$$.complete();
  }

  getNamespace({ namespace, cluster, isFederated }: UnionNamespaceBaseParams) {
    const resourceParams = {
      type: isFederated
        ? RESOURCE_TYPES.FEDERATED_NAMESPACE
        : RESOURCE_TYPES.NAMESPACE,
      cluster,
      name: namespace,
    };
    if (isFederated) {
      Object.assign(resourceParams, { namespace });
    }
    return this.k8sApi
      .watchResource<UnionNamespace>(resourceParams)
      .pipe(takeUntil(this.destroy$$));
  }

  getResourceQuota({ cluster, namespace }: UnionNamespaceBaseParams) {
    return this.k8sApi
      .watchResource<ResourceQuota>({
        type: RESOURCE_TYPES.RESOURCE_QUOTA,
        cluster,
        namespace,
        name: DEFAULT,
      })
      .pipe(
        skipError(null),
        tap(resourceQuota => {
          this.resourceQuota = resourceQuota;
        }),
      );
  }

  getLimitRange({ cluster, namespace }: UnionNamespaceBaseParams) {
    return this.k8sApi
      .watchResource<LimitRange>({
        type: RESOURCE_TYPES.LIMIT_RANGE,
        cluster,
        namespace,
        name: DEFAULT,
      })
      .pipe(
        skipError(null),
        tap((limitRange: LimitRange) => {
          this.limitRange = limitRange;
        }),
      );
  }

  updateResourceQuota() {
    this.dialog.open(UpdateResourceQuotaComponent, {
      data: {
        params: this.params,
        projectQuota: this.projectQuota,
        resourceQuota: this.resourceQuota,
        customResourceLimits: this.customResourceLimits,
      },
    });
  }

  updateLimitRange() {
    this.dialog.open<
      UpdateLimitRangeComponent,
      {
        params: UnionNamespaceBaseParams;
        limitRange?: LimitRange;
      },
      LimitRange
    >(UpdateLimitRangeComponent, {
      size: DialogSize.Big,
      data: {
        params: this.params,
        limitRange: this.limitRange,
      },
    });
  }

  confirmDeleteNamespace(templateRef: TemplateRef<ConfirmDeleteComponent>) {
    this.deleteDialogRef = this.dialog.open(templateRef);
  }

  deleteNamespace(_: string, namespace: UnionNamespace) {
    const { isFederated, namespace: name } = this.params;
    const cluster = isFederated
      ? this.k8sUtil.getLabel(namespace, 'hostname', PREFIXES.FEDERATION)
      : this.params.cluster;
    return this.k8sApi
      .deleteResource({
        type: isFederated
          ? RESOURCE_TYPES.FEDERATED_NAMESPACE
          : RESOURCE_TYPES.NAMESPACE,
        cluster,
        resource: namespace,
      })
      .pipe(
        concatMap(() =>
          isFederated
            ? // reference: http://confluence.alauda.cn/pages/viewpage.action?pageId=56993132#id-联邦命名空间API设计-1.4删除命名空间(标准API)
              this.k8sApi.deleteResource({
                type: RESOURCE_TYPES.NAMESPACE,
                cluster,
                resource: {
                  apiVersion: getYamlApiVersion(RESOURCE_TYPES.NAMESPACE),
                  metadata: {
                    name,
                  },
                },
              })
            : of(null),
        ),
      )
      .pipe(tap(this.backToList));
  }

  removeNamespace(namespace: UnionNamespace) {
    catchPromise(
      this.dialog.confirm({
        title: this.translate.get('remove_namespace_title', {
          namespace: this.k8sUtil.getName(namespace),
        }),
        content: this.translate.get('remove_namespace_tips'),
        confirmText: this.translate.get('remove'),
        cancelText: this.translate.get('cancel'),
      }),
    )
      .pipe(
        concatMap(() =>
          this.k8sApi.patchResource({
            type: this.params.isFederated
              ? RESOURCE_TYPES.FEDERATED_NAMESPACE
              : RESOURCE_TYPES.NAMESPACE,
            cluster: this.params.cluster,
            resource: namespace,
            part: {
              metadata: {
                labels: {
                  [this.k8sUtil.normalizeType(PROJECT)]: null,
                },
              },
            },
          }),
        ),
      )
      .subscribe(this.backToList);
  }

  confirmFederalizeNamespace(templateRef: TemplateRef<any>) {
    this.dialogRef = this.dialog.open(templateRef, {
      size: DialogSize.Big,
    });
  }

  federalizeNamespace() {
    this.http
      .post(
        `${API_GATEWAY}/acp/v1/kubernetes/${this.params.cluster}/namespaces/${this.params.namespace}/federate`,
        null,
      )
      .pipe(takeUntil(this.destroy$$))
      .subscribe(() => {
        this.dialogRef.close();
        this.router.navigate(
          [
            '../../../../federated_namespace/detail',
            this.k8sUtil.getLabel(this.cluster, 'hostname', 'federation'),
            this.params.namespace,
          ],
          {
            relativeTo: this.route,
            replaceUrl: true,
          },
        );
      });
  }

  getClusterNames(
    clusters: Array<{
      name: string;
    }>,
  ) {
    if (!clusters?.length) {
      return of([]);
    }
    return forkJoin(
      clusters.map(({ name }) =>
        this.k8sApi
          .getGlobalResource({
            type: RESOURCE_TYPES.CLUSTER_REGISTRY,
            name,
            namespaced: true,
          })
          .pipe(
            map(cluster => ({
              name: this.k8sUtil.getName(cluster),
              displayName: this.k8sUtil.getDisplayName(cluster),
            })),
          ),
      ),
    );
  }

  backToList() {
    this.router.navigate(['../../..'], {
      relativeTo: this.route,
      replaceUrl: true,
    });
  }
}
