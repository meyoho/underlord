import { K8sApiService, publishRef } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { get } from 'lodash-es';
import { Observable, combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { ProjectQuota, ResourceBaseParams } from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';

export interface UnionNamespaceBaseParams extends ResourceBaseParams {
  isFederated: boolean;
}

@Component({
  templateUrl: './namespace-detail.component.html',
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NamespaceDetailComponent {
  isFederated$ = this.route.parent.url.pipe(
    map(urls => get(urls, [0, 'path']) === 'federated_namespace'),
  );

  params$: Observable<UnionNamespaceBaseParams> = combineLatest([
    this.isFederated$,
    this.route.parent.parent.paramMap,
    this.route.paramMap,
  ]).pipe(
    map(([isFederated, paramMap, params]) => ({
      isFederated,
      project: paramMap.get('project'),
      cluster: params.get('cluster'),
      namespace: params.get('namespace'),
    })),
    publishRef(),
  );

  projectQuota$ = this.params$.pipe(
    switchMap(({ project, cluster }) =>
      this.k8sApi.getResource<ProjectQuota>({
        type: RESOURCE_TYPES.PROJECT_QUOTA,
        cluster,
        name: project,
      }),
    ),
    publishRef(),
  );

  constructor(
    private readonly route: ActivatedRoute,
    private readonly k8sApi: K8sApiService,
  ) {}
}
