import { ValueHook } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { ActionType, Namespace } from 'app/typings';
import { K8S_RESOURCE_NAME_BASE, unionDisplayName } from 'app/utils';

export interface NamespaceBasicInfo {
  projectName: string;
  name: string;
  // ! hack, make sure name will never change
  _name?: string;
  displayName?: string;
  clusterName?: string;
  clusterFedName?: string;
  hostClusterName?: string;
}

@Component({
  selector: 'alu-namespace-basic-info-form',
  templateUrl: './basic-info-form.component.html',
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BasicInfoFormComponent extends BaseResourceFormGroupComponent {
  @Input()
  isFederated: boolean;

  @Input()
  actionType: ActionType;

  @ValueHook<BasicInfoFormComponent, 'projectName'>(function (
    projectName: string,
  ) {
    this.form.patchValue(
      {
        projectName,
      },
      {
        emitEvent: false,
        onlySelf: true,
      },
    );
  })
  @Input()
  projectName: string;

  @Input()
  clusterFedNames: Array<{
    name: string;
    displayName?: string;
  }>;

  @Input()
  clusterNames: Array<{
    name: string;
    displayName?: string;
  }>;

  @Input()
  namespacesLoading: boolean;

  @Input()
  namespaces: Namespace[];

  namePattern = K8S_RESOURCE_NAME_BASE;

  unionDisplayName = unionDisplayName;

  constructor(public injector: Injector, public fb: FormBuilder) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      projectName: [''],
      name: [''],
      _name: [''],
      displayName: [''],
      clusterName: [''],
      clusterFedName: [''],
      hostClusterName: [''],
    });
  }

  adaptFormModel(basicInfo: NamespaceBasicInfo) {
    return this.actionType === ActionType.CREATE
      ? {
          ...basicInfo,
          name: this.projectName + '-' + basicInfo._name,
        }
      : basicInfo;
  }

  getDefaultFormModel() {
    return {
      projectName: '',
      _name: '',
      name: '',
      displayName: '',
      clusterName: '',
      clusterFedName: '',
      hostClusterName: '',
    };
  }

  namespacesTrackBy(_index: number, namespace: Namespace) {
    return namespace.metadata.name;
  }
}
