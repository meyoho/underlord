import {
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  K8sUtilService,
  NAME,
  PROJECT,
  isAllowed,
  matchExpressionsToString,
  matchLabelsToString,
  publishRef,
} from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { get, isEqual } from 'lodash-es';
import { EMPTY, Observable, combineLatest, throwError } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  shareReplay,
  switchMap,
} from 'rxjs/operators';

import { AdvanceApi } from 'app/api/advance/api';
import { ProjectApiService } from 'app/api/project/api';
import { Cluster } from 'app/api/project/types';
import { FEDERATED } from 'app/services/k8s-util.service';
import {
  ConfigMap,
  Project,
  ProjectListParams,
  ProjectQuota,
  UnionNamespace,
} from 'app/typings';
import { RESOURCE_TYPES, TRUE, TYPE } from 'app/utils';

@Component({
  templateUrl: './namespace-list.component.html',
  styleUrls: ['./namespace-list.component.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NamespaceListComponent {
  isFederated$ = this.route.parent.url.pipe(
    map(urls => get(urls, [0, 'path']) === 'federated_namespace'),
  );

  projectName$ = this.route.parent.parent.paramMap.pipe(
    map(params => params.get('project')),
    publishRef(),
  );

  private readonly project$ = this.projectName$.pipe(
    switchMap(projectName =>
      this.k8sApi.getGlobalResource<Project>({
        type: RESOURCE_TYPES.PROJECT,
        name: projectName,
      }),
    ),
    publishRef(),
  );

  hostClusters$: Observable<Cluster[]> = this.project$.pipe(
    switchMap(project => this.projectApi.getClusterFeds(project)),
    map(clusterFeds =>
      clusterFeds.reduce<Cluster[]>((acc, clusterFed) => {
        const clusters = get(clusterFed, ['spec', 'clusters']) || [];
        const hostCluster = clusters.find(({ type }) => type === 'Host');
        if (hostCluster) {
          acc.push({
            fedName: this.k8sUtil.getName(clusterFed),
            name: hostCluster.name,
          });
        }
        return acc;
      }, []),
    ),
    shareReplay(1),
  );

  clusters$: Observable<Cluster[]> = combineLatest([
    this.project$.pipe(
      map(project => get(project, ['spec', 'clusters']) || []),
    ),
    this.k8sApi.getGlobalResourceList({
      type: RESOURCE_TYPES.CLUSTER_REGISTRY,
      namespaced: true,
    }),
  ]).pipe(
    map(([clusters, { items }]) =>
      clusters.map((cluster: Cluster) => {
        const resource = items.find(
          item => this.k8sUtil.getName(item) === cluster.name,
        );
        if (resource) {
          return {
            ...cluster,
            displayName: this.k8sUtil.getDisplayName(resource),
          };
        }
        return cluster;
      }),
    ),
    shareReplay(1),
  );

  unionClusters$ = this.isFederated$.pipe(
    switchMap(isFederated =>
      isFederated ? this.hostClusters$ : this.clusters$,
    ),
    publishRef(),
  );

  cluster$: Observable<Cluster> = combineLatest([
    this.unionClusters$,
    this.route.queryParams,
  ]).pipe(
    map(
      ([clusters, queryParams]) =>
        clusters.find(
          ({ name, fedName }) => (fedName || name) === queryParams.cluster,
        ) || clusters[0],
    ),
    publishRef(),
  );

  createNamespaceDisabled$ = combineLatest([
    this.isFederated$,
    this.project$,
    this.cluster$,
  ]).pipe(
    map(
      ([isFederated, project, cluster]) =>
        isFederated &&
        !project.spec.clusters?.find(
          c => c.type === 'FedMember' && c.name === cluster?.name,
        ),
    ),
    publishRef(),
  );

  params$: Observable<ProjectListParams> = combineLatest([
    this.isFederated$,
    this.projectName$,
    this.cluster$,
    this.route.queryParams,
  ]).pipe(
    map(([isFederated, project, cluster, queryParams]) => ({
      ...queryParams,
      isFederated,
      project,
      cluster: cluster?.name,
    })),
    distinctUntilChanged(isEqual),
    publishRef(),
  );

  projectQuota$ = this.params$.pipe(
    switchMap(({ project, cluster }) =>
      cluster
        ? this.k8sApi.getResource<ProjectQuota>({
            type: RESOURCE_TYPES.PROJECT_QUOTA,
            cluster,
            name: project,
          })
        : EMPTY,
    ),
    publishRef(),
  );

  permissions$ = this.params$.pipe(
    switchMap(({ isFederated, project, cluster }) =>
      this.k8sPermission.getAccess({
        advanced: true,
        type: isFederated
          ? RESOURCE_TYPES.FEDERATED_NAMESPACE
          : RESOURCE_TYPES.NAMESPACE,
        project,
        cluster,
        action: [K8sResourceAction.CREATE, K8sResourceAction.UPDATE],
      }),
    ),
    isAllowed(),
  );

  namespaceImportPermission$ = this.params$.pipe(
    switchMap(({ project, cluster }) =>
      cluster
        ? this.k8sPermission.getAccess({
            advanced: true,
            type: RESOURCE_TYPES.NAMESPACE_IMPORT,
            project,
            cluster,
            action: K8sResourceAction.CREATE,
          })
        : EMPTY,
    ),
    isAllowed(false),
  );

  customResourceLimits$ = this.params$.pipe(
    switchMap(({ cluster }) =>
      cluster
        ? this.k8sApi.getResourceList<ConfigMap>({
            type: RESOURCE_TYPES.CONFIG_MAP,
            cluster,
            namespace: 'kube-public',
            queryParams: {
              labelSelector: matchLabelsToString({
                [this.k8sUtil.normalizeType(
                  TYPE,
                  'features',
                )]: 'CustomResourceLimitation',
              }),
            },
          })
        : EMPTY,
    ),
    map(({ items }) => items.map(item => item.data)),
    publishRef(),
  );

  list = new K8SResourceList<UnionNamespace>({
    fetchParams$: this.params$,
    fetcher: this.fetchList.bind(this),
  });

  featGuardAllowed$ = this.params$.pipe(
    map(({ cluster, isFederated }) => !!(cluster || !isFederated)),
  );

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sApi: K8sApiService,
    private readonly k8sUtil: K8sUtilService,
    private readonly advanceApi: AdvanceApi,
    private readonly projectApi: ProjectApiService,
  ) {}

  fetchList(params: ProjectListParams) {
    const {
      isFederated,
      cluster,
      project,
      keyword = '',
      ...queryParams
    } = params;
    const filterBy = [NAME, keyword].join();
    return cluster
      ? isFederated
        ? this.advanceApi.getFederatedNamespaces({
            ...queryParams,
            cluster,
            project,
            filterBy,
            labelSelector: matchLabelsToString({
              [this.k8sUtil.normalizeType(PROJECT)]: project,
            }),
          })
        : this.advanceApi.getNamespaces({
            ...queryParams,
            cluster,
            project,
            filterBy: [NAME, keyword].join(),
            labelSelector: matchExpressionsToString([
              {
                key: this.k8sUtil.normalizeType(FEDERATED),
                operator: '!=',
                values: [TRUE],
              },
            ]),
          })
      : throwError(new Error('No cluster'));
  }

  onSearch(queryParams: Params) {
    this.router.navigate([], {
      queryParams,
      queryParamsHandling: 'merge',
    });
  }

  clusterTrackFn(cluster: string | Cluster) {
    return typeof cluster === 'string' ? cluster : cluster.name;
  }
}
