import { DialogService, DialogSize } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { Cluster } from 'app/api/project/types';
import { CustomResourceData } from 'app/manage-project/quota-form/quota-base-from.component';
import {
  K8sResourceStatus,
  LimitRange,
  ProjectQuota,
  UnionNamespace,
} from 'app/typings';

import { UpdateLimitRangeComponent } from '../update-limit-range/update-limit-range.component';
import { UpdateResourceQuotaComponent } from '../update-resource-quota/update-resource-quota.component';

@Component({
  selector: 'alu-namespace-table',
  templateUrl: './namespace-table.component.html',
  styleUrls: ['./namespace-table.component.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NamespaceTableComponent {
  @Input()
  isFederated: boolean;

  @Input()
  projectQuota: Observable<ProjectQuota>;

  @Input()
  customResourceLimits: CustomResourceData[];

  @Input()
  cluster: Cluster;

  @Input()
  namespaces: UnionNamespace[];

  @Input()
  canUpdate: boolean;

  columns = ['name', 'clusterName', 'creationTime', 'action'];

  constructor(
    private readonly router: Router,
    private readonly dialog: DialogService,
  ) {}

  isTerminating(namespace: UnionNamespace) {
    return 'status' in namespace && 'phase' in namespace.status
      ? namespace.status.phase === K8sResourceStatus.TERMINATING
      : namespace.metadata.deletionTimestamp;
  }

  getFedClusterNames(clusters: Array<{ name: string }>) {
    return clusters.map(({ name }) => name).join(', ');
  }

  updateResourceQuota(namespace: UnionNamespace) {
    if (this.isFederated) {
      this.router.navigate([]);
    }

    this.dialog.open(UpdateResourceQuotaComponent, {
      data: {
        cluster: this.cluster.name,
        namespace,
        projectQuota: this.projectQuota,
        customResourceLimits: this.customResourceLimits,
      },
    });
  }

  updateLimitRange(namespace: UnionNamespace) {
    this.dialog.open<
      UpdateLimitRangeComponent,
      {
        cluster: string;
        namespace: UnionNamespace;
      },
      LimitRange
    >(UpdateLimitRangeComponent, {
      size: DialogSize.Big,
      data: {
        cluster: this.cluster.name,
        namespace,
      },
    });
  }
}
