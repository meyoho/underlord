import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { NamespaceActionComponent } from './action/namespace-action.component';
import { NamespaceDetailComponent } from './detail/namespace-detail.component';
import { NamespaceListComponent } from './list/namespace-list.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list',
      },
      {
        path: 'list',
        component: NamespaceListComponent,
      },
      {
        path: 'update/:cluster/:namespace',
        component: NamespaceActionComponent,
      },
      {
        path: ':action',
        component: NamespaceActionComponent,
      },
      {
        path: 'detail/:cluster',
        pathMatch: 'full',
        redirectTo: 'list',
      },
      {
        path: 'detail/:cluster/:namespace',
        component: NamespaceDetailComponent,
      },
    ]),
  ],
  exports: [RouterModule],
})
export class NamespaceRoutingModule {}
