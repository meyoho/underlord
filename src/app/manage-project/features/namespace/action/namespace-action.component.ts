import {
  ANNOTATIONS,
  AsyncDataLoader,
  CLUSTER,
  DISPLAY_NAME,
  FALSE,
  K8sApiService,
  KubernetesResource,
  LABELS,
  METADATA,
  NAME,
  PROJECT,
  StringMap,
  TranslateService,
  ifExist,
  matchLabelsToString,
  publishRef,
  skipError,
} from '@alauda/common-snippet';
import { MessageService, NotificationService } from '@alauda/ui';
import { HttpErrorResponse } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnDestroy,
  OnInit,
  isDevMode,
} from '@angular/core';
import { FormArray, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { get, isEqual, unset } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import {
  BehaviorSubject,
  EMPTY,
  Observable,
  Subject,
  combineLatest,
  forkJoin,
  of,
} from 'rxjs';
import {
  catchError,
  concatMap,
  distinctUntilChanged,
  filter,
  finalize,
  map,
  pluck,
  startWith,
  switchMap,
  takeUntil,
  tap,
  withLatestFrom,
} from 'rxjs/operators';

import { AdvanceApi } from 'app/api/advance/api';
import { ProjectApiService } from 'app/api/project/api';
import {
  K8sUtilService,
  OVERRIDE,
  PREFIXES,
} from 'app/services/k8s-util.service';
import {
  ActionType,
  ClusterFed,
  ClusterFedItem,
  ConfigMap,
  K8sQuotaItem,
  LimitRange,
  Namespace,
  Project,
  ProjectQuota,
  ResourceQuota,
  UnionNamespace,
} from 'app/typings';
import {
  DEFAULT,
  K8S_RESOURCE_LABEL_KEY_NAME_PATTERN,
  K8S_RESOURCE_LABEL_VALUE_PATTERN,
  RESOURCE_TYPES,
  ResourceType,
  TYPE,
  delay,
  trackByKey,
} from 'app/utils';

import { NamespaceBasicInfo } from '../basic-info-form/basic-info-form.component';
import { diffQuotaItem } from '../util';

export interface ResourceConfig {
  clusterName: string;
  resourcequota: ResourceQuota;
  limitrange: LimitRange;
}

export interface NamespaceActionFormData {
  basicInfo: NamespaceBasicInfo;
  labels?: StringMap;
  annotations?: StringMap;
  resourcequota?: ResourceQuota;
  limitrange?: LimitRange;
  resourceConfigs?: ResourceConfig[];
  diffConfig?: boolean;
}

export interface NamespaceActionSubmitData extends KubernetesResource {
  namespace: Namespace;
  resourcequota: ResourceQuota;
  limitrange: LimitRange;
}

// reference: http://confluence.alauda.cn/pages/viewpage.action?pageId=54856689
const PRESERVED_NAMESPACES = new Set([
  'alauda-system',
  'kube-ovn',
  'kube-system',
  'kube-public',
]);

@Component({
  templateUrl: './namespace-action.component.html',
  styleUrls: ['./namespace-action.component.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NamespaceActionComponent extends BaseResourceFormGroupComponent
  implements OnInit, OnDestroy {
  private isFederated: boolean;
  isFederated$ = this.route.parent.url.pipe(
    map(urls => get(urls, [0, 'path']) === 'federated_namespace'),
    tap(isFederated => (this.isFederated = isFederated)),
  );

  namespacePrefix$ = this.isFederated$.pipe(
    map(isFederated => (isFederated ? 'federated_' : '') + 'namespace'),
  );

  projectName$ = this.route.parent.parent.paramMap.pipe(
    map(params => params.get('project')),
    publishRef(),
  );

  params: {
    isFederated: boolean;
    cluster: string;
    namespace: string;
    action: ActionType;
  };

  params$ = combineLatest([this.isFederated$, this.route.paramMap]).pipe(
    map(([isFederated, params]) => ({
      isFederated,
      cluster: params.get('cluster'),
      namespace: params.get('namespace'),
      action: (params.get('action') as ActionType) || ActionType.UPDATE,
    })),
    tap(params => (this.params = params)),
    publishRef(),
  );

  actionPrefix$ = this.params$.pipe(map(params => params.action + '_'));

  isCreate: boolean;
  isCreate$ = this.params$.pipe(
    map(params => params.action === ActionType.CREATE),
    tap(isCreate => (this.isCreate = isCreate)),
    publishRef(),
  );

  onDestroy$$ = new Subject<void>();

  originalNamespace$ = this.params$.pipe(
    switchMap(({ isFederated, action, cluster, namespace }) =>
      action === ActionType.UPDATE
        ? this.k8sApi.getResource<UnionNamespace>({
            type: isFederated
              ? RESOURCE_TYPES.FEDERATED_NAMESPACE
              : RESOURCE_TYPES.NAMESPACE,
            cluster,
            namespace,
            name: namespace,
          })
        : EMPTY,
    ),
  );

  private readonly project$$ = new BehaviorSubject<Project>(null);
  private readonly project$ = this.project$$.pipe(
    filter(_ => !!_),
    publishRef(),
  );

  private readonly clusterName$$ = new Subject<string>();
  private readonly clusterName$ = this.clusterName$$.pipe(
    distinctUntilChanged(),
    publishRef(),
  );

  private clusterFedName: string;
  private readonly clusterFedName$$ = new Subject<string>();
  private readonly clusterFedName$ = this.clusterFedName$$.pipe(
    distinctUntilChanged(),
    publishRef(),
  );

  private readonly annotationName = this.k8sUtil.normalizeType(NAME);
  private readonly annotationDisplayName = this.k8sUtil.normalizeType(
    DISPLAY_NAME,
  );

  private readonly labelProject = this.k8sUtil.normalizeType(PROJECT);
  private readonly labelCluster = this.k8sUtil.normalizeType(CLUSTER);

  private readonly defaultReadonlyAnnotations = [this.annotationDisplayName];
  private readonly defaultReadonlyLabels = [
    this.labelProject,
    this.labelCluster,
  ];

  private clusterFeds: ClusterFed[];
  private readonly clusterFeds$ = combineLatest([
    this.isFederated$,
    this.project$,
  ]).pipe(
    switchMap(([isFederated, project]) =>
      isFederated ? this.projectApi.getClusterFeds(project) : EMPTY,
    ),
    publishRef(),
    tap(clusterFeds => (this.clusterFeds = clusterFeds)),
  );

  readonlyAnnotations = this.defaultReadonlyAnnotations;
  readonlyLabels = this.defaultReadonlyLabels;

  fedClusters$ = combineLatest([this.clusterFeds$, this.clusterFedName$]).pipe(
    map(([clusterFeds, clusterFedName]) =>
      this.getFedClusters(clusterFeds, clusterFedName),
    ),
  );

  fedClusterNames$ = this.fedClusters$.pipe(
    map(fedClusters => fedClusters.map(({ name }) => name)),
  );

  private hostClusterName: string;
  hostClusterName$ = this.fedClusters$.pipe(
    map(fedClusters => fedClusters.find(({ type }) => type === 'Host').name),
    tap(hostClusterName => (this.hostClusterName = hostClusterName)),
  );

  unionClusterName$ = this.isFederated$.pipe(
    switchMap(isFederated =>
      isFederated ? this.hostClusterName$ : this.clusterName$,
    ),
  );

  clusterFedNames$ = this.clusterFeds$.pipe(
    map(clusterFeds =>
      clusterFeds.map(clusterFed => ({
        name: this.k8sUtil.getName(clusterFed),
        displayName: this.k8sUtil.getDisplayName(clusterFed),
      })),
    ),
  );

  diffConfig = false;

  resourceConfigs$: Observable<ResourceConfig[]> = combineLatest([
    this.params$,
    this.fedClusterNames$,
  ]).pipe(
    switchMap(([{ isFederated, namespace }, fedClusters]) =>
      isFederated
        ? forkJoin(
            fedClusters.map(clusterName =>
              forkJoin([
                this.k8sApi
                  .getResource<ResourceQuota>({
                    type: RESOURCE_TYPES.RESOURCE_QUOTA,
                    cluster: clusterName,
                    namespace,
                    name: DEFAULT,
                  })
                  .pipe(skipError(null)),
                this.k8sApi
                  .getResource<LimitRange>({
                    type: RESOURCE_TYPES.LIMIT_RANGE,
                    cluster: clusterName,
                    namespace,
                    name: DEFAULT,
                  })
                  .pipe(skipError(null)),
              ]).pipe(
                map(([resourcequota, limitrange]) => ({
                  clusterName,
                  resourcequota,
                  limitrange,
                })),
              ),
            ),
          )
        : EMPTY,
    ),
    publishRef(),
  );

  quotaMaxLimitsList$ = combineLatest([
    this.projectName$,
    this.fedClusterNames$,
    this.resourceConfigs$,
  ]).pipe(
    switchMap(([projectName, fedClusterNames, resourceConfigs]) =>
      combineLatest(
        fedClusterNames.map((fedClusterName, index) =>
          this.getQuotaMaxLimits(
            projectName,
            fedClusterName,
            resourceConfigs[index].resourcequota,
          ),
        ),
      ),
    ),
    startWith([] as K8sQuotaItem[]),
    publishRef(),
  );

  namespacesLoading$$ = new Subject<boolean>();
  namespaces$ = combineLatest([this.isCreate$, this.clusterName$]).pipe(
    switchMap(([isCreate, cluster]) => {
      if (isCreate) {
        return of<Namespace[]>([]);
      }
      this.namespacesLoading$$.next(true);
      return this.k8sApi
        .getResourceList<Namespace>({
          type: RESOURCE_TYPES.NAMESPACE,
          cluster,
          queryParams: {
            labelSelector: '!' + this.k8sUtil.normalizeType(PROJECT),
          },
        })
        .pipe(
          pluck('items'),
          map(namespaces =>
            namespaces.filter(
              namespace =>
                !PRESERVED_NAMESPACES.has(this.k8sUtil.getName(namespace)),
            ),
          ),
          finalize(() => this.namespacesLoading$$.next(false)),
        );
    }),
    startWith<Namespace[]>([]),
    publishRef(),
  );

  private readonly namespace$$ = new Subject<string>();
  private readonly namespace$ = this.namespace$$.asObservable();

  private readonly baseParams$ = combineLatest([
    this.unionClusterName$,
    this.namespace$.pipe(
      startWith(this.route.snapshot.paramMap.get('namespace')),
      filter(_ => !!_),
    ),
  ]).pipe(distinctUntilChanged(isEqual), publishRef());

  resourceQuota$: Observable<ResourceQuota> = this.baseParams$.pipe(
    switchMap(([cluster, namespace]) =>
      this.k8sApi
        .getResource<ResourceQuota>({
          type: RESOURCE_TYPES.RESOURCE_QUOTA,
          cluster,
          namespace,
          name: DEFAULT,
        })
        .pipe(skipError(null)),
    ),
    publishRef(),
  );

  limitRange$: Observable<LimitRange> = this.baseParams$.pipe(
    switchMap(([cluster, namespace]) =>
      this.k8sApi
        .getResource<LimitRange>({
          type: RESOURCE_TYPES.LIMIT_RANGE,
          cluster,
          namespace,
          name: DEFAULT,
        })
        .pipe(skipError(null)),
    ),
    publishRef(),
  );

  quotaMaxLimits$ = combineLatest([
    this.projectName$,
    this.unionClusterName$,
    this.resourceQuota$.pipe(startWith(null as ResourceQuota)),
  ]).pipe(
    switchMap(([projectName, clusterName, resourceQuota]) =>
      this.getQuotaMaxLimits(projectName, clusterName, resourceQuota),
    ),
    publishRef(),
  );

  dataLoader = new AsyncDataLoader<Project, string>({
    params$: this.projectName$,
    fetcher: this.getProject.bind(this),
  });

  trackByKey = trackByKey;

  annotationKeyValueValidators = {
    key: [Validators.pattern(K8S_RESOURCE_LABEL_KEY_NAME_PATTERN.pattern)],
  };

  labelKeyValueValidators = {
    ...this.annotationKeyValueValidators,
    value: [Validators.pattern(K8S_RESOURCE_LABEL_VALUE_PATTERN.pattern)],
  };

  keyValueErrorMapper$ = this.translate.locale$.pipe(
    map(() => ({
      key: {
        pattern: this.translate.get(K8S_RESOURCE_LABEL_KEY_NAME_PATTERN.tip),
      },
      value: {
        pattern: this.translate.get(K8S_RESOURCE_LABEL_VALUE_PATTERN.tip),
      },
    })),
    publishRef(),
  );

  constructor(
    public injector: Injector,
    public fb: FormBuilder,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly message: MessageService,
    private readonly notification: NotificationService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    private readonly advanceApi: AdvanceApi,
    private readonly translate: TranslateService,
    private readonly projectApi: ProjectApiService,
  ) {
    super(injector);
    this.handleError = this.handleError.bind(this);
    this.getProjectClusterNames = this.getProjectClusterNames.bind(this);
  }

  getProject(projectName: string) {
    return this.k8sApi
      .getGlobalResource({
        type: RESOURCE_TYPES.PROJECT,
        name: projectName,
      })
      .pipe(tap(project => this.project$$.next(project)));
  }

  getProjectClusterNames(
    project: Project,
  ): Observable<
    Array<{
      name: string;
      displayName?: string;
    }>
  > {
    const clusters = get(project, ['spec', 'clusters']) || [];
    if (!clusters.length) {
      return of([]);
    }
    return forkJoin(
      clusters.map(({ name }) =>
        this.k8sApi
          .getGlobalResource({
            type: RESOURCE_TYPES.CLUSTER_REGISTRY,
            name,
            namespaced: true,
          })
          .pipe(
            map(cluster => ({
              name: this.k8sUtil.getName(cluster),
              displayName: this.k8sUtil.getDisplayName(cluster),
            })),
          ),
      ),
    );
  }

  getQuotaMaxLimits(
    projectName: string,
    clusterName: string,
    resourceQuota: ResourceQuota,
  ) {
    return this.k8sApi
      .getResource<ProjectQuota>({
        type: RESOURCE_TYPES.PROJECT_QUOTA,
        cluster: clusterName,
        name: projectName,
      })
      .pipe(
        map(projectQuota =>
          diffQuotaItem(
            projectQuota.spec.hard,
            projectQuota.status.hard,
            resourceQuota?.spec.hard,
          ),
        ),
      );
  }

  customResourceLimits$ = this.unionClusterName$.pipe(
    switchMap(cluster =>
      this.k8sApi.getResourceList<ConfigMap>({
        type: RESOURCE_TYPES.CONFIG_MAP,
        cluster,
        namespace: 'kube-public',
        queryParams: {
          labelSelector: matchLabelsToString({
            [this.k8sUtil.normalizeType(
              TYPE,
              'features',
            )]: 'CustomResourceLimitation',
          }),
        },
      }),
    ),
    map(({ items }) => items.map(item => item.data)),
    publishRef(),
  );

  ngOnInit() {
    super.ngOnInit();

    const basicInfoCtrl = this.form.get('basicInfo');
    const resourceConfigsArr = this.form.get('resourceConfigs') as FormArray;
    combineLatest([
      this.params$,
      this.projectName$,
      this.namespaces$,
      basicInfoCtrl.valueChanges.pipe(startWith({})),
    ])
      .pipe(takeUntil(this.onDestroy$$))
      .subscribe(
        ([{ action, isFederated }, projectName, namespaces, basicInfo]) => {
          const annotationsControl = this.form.get('annotations');
          const labelsControl = this.form.get('labels');

          const {
            clusterFedName,
            clusterName,
            displayName,
            name,
          }: NamespaceBasicInfo = basicInfo;

          if (isFederated && clusterFedName !== this.clusterFedName) {
            this.clusterFedName = clusterFedName;
            this.clusterFedName$$.next(clusterFedName);
            resourceConfigsArr.clear();
            this.getFedClusters(this.clusterFeds, clusterFedName).forEach(
              ({ name, type }, index) => {
                if (type === 'Host') {
                  basicInfoCtrl.patchValue(
                    {
                      ...basicInfoCtrl.value,
                      hostClusterName: name,
                    },
                    {
                      emitEvent: false,
                      onlySelf: true,
                    },
                  );
                }
                resourceConfigsArr.setControl(
                  index,
                  this.fb.group({
                    clusterName: name,
                    resourcequota: null,
                    limitrange: null,
                  }),
                );
                if (!this.diffConfig) {
                  resourceConfigsArr.disable();
                }
              },
            );
          }

          if (clusterName) {
            this.clusterName$$.next(clusterName);
          }

          let namespace: Namespace;
          if (name && action === ActionType.IMPORT) {
            namespace = namespaces.find(_ => name === this.k8sUtil.getName(_));
            this.namespace$$.next(this.k8sUtil.getName(namespace));
          }

          const namespaceAnnotations =
            get(namespace, [METADATA, ANNOTATIONS]) || {};
          const namespaceLabels = get(namespace, [METADATA, LABELS]) || {};

          if (namespace) {
            this.readonlyAnnotations = this.defaultReadonlyAnnotations.concat(
              Object.keys(namespaceAnnotations),
            );
            this.readonlyLabels = this.defaultReadonlyLabels.concat(
              Object.keys(namespaceLabels),
            );
          }

          const annotations = {
            ...annotationsControl.value,
            ...namespaceAnnotations,
            [this.annotationDisplayName]: displayName,
          };
          unset(annotations, this.annotationName);
          annotationsControl.setValue(annotations);
          labelsControl.setValue({
            ...labelsControl.value,
            ...namespaceLabels,
            [this.labelProject]: projectName,
            [this.labelCluster]: clusterName,
          });

          this.cdr.markForCheck();
        },
      );

    combineLatest([
      this.projectName$,
      this.originalNamespace$,
      this.clusterFeds$,
    ])
      .pipe(
        withLatestFrom(this.params$),
        tap(
          ([
            [projectName, namespace, clusterFeds],
            { isFederated, cluster },
          ]) => {
            if (isFederated) {
              this.diffConfig =
                this.k8sUtil.getLabel(
                  namespace,
                  OVERRIDE,
                  PREFIXES.FEDERATION,
                ) !== FALSE;
            }
            const basicInfo = {
              [isFederated ? 'clusterFedName' : 'clusterName']: isFederated
                ? this.k8sUtil.getName(
                    clusterFeds.find(clusterFed =>
                      clusterFed.spec.clusters.find(
                        ({ name, type }) => type === 'Host' && name === cluster,
                      ),
                    ),
                  )
                : cluster,
              name: this.k8sUtil.getName(namespace),
              projectName,
              displayName: this.k8sUtil.getDisplayName(namespace),
            };
            basicInfoCtrl.patchValue(basicInfo);
          },
        ),
        concatMap(() => this.resourceConfigs$),
        takeUntil(this.onDestroy$$),
      )
      .subscribe(resourceConfigs => {
        resourceConfigsArr.patchValue(resourceConfigs);
        // ! hack, `patchValue` seems to be asynchronous
        setTimeout(
          () => this.onDiffConfigChange(this.diffConfig, this.diffConfig),
          100,
        );
      });

    combineLatest([this.resourceQuota$, this.limitRange$])
      .pipe(takeUntil(this.onDestroy$$))
      .subscribe(([resourcequota, limitrange]) =>
        this.form.patchValue({ resourcequota, limitrange }),
      );
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.onDestroy$$.next();
    this.onDestroy$$.complete();
  }

  createForm() {
    return this.fb.group({
      basicInfo: null,
      labels: {
        [this.labelProject]: null,
        [this.labelCluster]: null,
      },
      annotations: {
        [this.annotationDisplayName]: null,
      },
      resourcequota: null,
      limitrange: null,
      resourceConfigs: this.fb.array([]),
    });
  }

  getDefaultFormModel() {
    return {};
  }

  onDiffConfigChange(diffConfig: boolean, skipOverride = false) {
    const resourceQuotaCtrl = this.form.get('resourcequota');
    const limitRangeCtrl = this.form.get('limitrange');
    const resourceConfigsCtrl = this.form.get('resourceConfigs');
    if (diffConfig) {
      resourceQuotaCtrl.disable();
      limitRangeCtrl.disable();
      resourceConfigsCtrl.enable();
      if (!skipOverride) {
        this.setResourceConfigs(resourceQuotaCtrl.value, limitRangeCtrl.value);
      }
    } else {
      resourceQuotaCtrl.enable();
      limitRangeCtrl.enable();
      resourceConfigsCtrl.disable();
      if (!skipOverride) {
        this.adaptResourceConfigs(resourceConfigsCtrl.value);
      }
    }
  }

  adaptResourceConfigs(resourceConfigs: ResourceConfig[]) {
    const { resourcequota, limitrange } = resourceConfigs.find(
      ({ clusterName }) => clusterName === this.hostClusterName,
    );
    this.form.get('resourcequota').patchValue(resourcequota);
    this.form.get('limitrange').patchValue(limitrange);
  }

  setResourceConfigs(resourcequota: ResourceQuota, limitrange: LimitRange) {
    const resourceConfigsCtrl = this.form.get('resourceConfigs');
    resourceConfigsCtrl.patchValue(
      (resourceConfigsCtrl.value as ResourceConfig[]).map(resourceConfig =>
        Object.assign(resourceConfig, {
          resourcequota,
          limitrange,
        }),
      ),
    );
  }

  async editNamespace() {
    await delay(); // FIXME: Why it is needed?

    if (this.form.invalid) {
      if (isDevMode()) {
        console.info(this.form);
      }
      return;
    }

    const formData: NamespaceActionFormData = this.form.value;
    formData.diffConfig = this.diffConfig;
    const tip = `${ifExist(this.isFederated, 'federated_')}namespace_${
      this.params.action
    }`;

    const unionNamespace$: Observable<UnionNamespace> = this.isFederated
      ? this.advanceApi.editFederatedNamespace(formData, this.isCreate)
      : this.advanceApi.editNamespace(formData, this.isCreate);

    unionNamespace$
      .pipe(
        tap(() => {
          this.message.success(this.translate.get(`${tip}_successed`));
          this.gotoDetail();
        }),
        catchError(e => {
          this.notification.error({
            title: this.translate.get(`${tip}_failed`),
            content: get(e, ['error', 'message'], e.message),
          });
          return EMPTY;
        }),
      )
      .subscribe();
  }

  gotoDetail() {
    const { basicInfo } = this.form.value;
    this.router.navigate(
      [
        this.params.action === ActionType.UPDATE ? '../../..' : '..',
        'detail',
        this.isFederated ? basicInfo.hostClusterName : basicInfo.clusterName,
        basicInfo.name,
      ],
      {
        relativeTo: this.route,
      },
    );
  }

  private getFedClusters(
    clusterFeds: ClusterFed[],
    clusterFedName: string,
  ): ClusterFedItem[] {
    return get(
      clusterFeds.find(
        clusterFed => this.k8sUtil.getName(clusterFed) === clusterFedName,
      ),
      ['spec', 'clusters'],
      [],
    );
  }

  private handleError<T>(e: HttpErrorResponse) {
    if (e instanceof HttpErrorResponse && e.status === 404) {
      return of(null as T);
    }
    this.notification.error((e.error && e.error.message) || e.message);
    return EMPTY;
  }
}
