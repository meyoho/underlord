import { K8sApiService, TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
} from '@angular/core';
import { finalize } from 'rxjs/operators';

import { FedQuotaDisplay } from 'app/api/project/types';
import { Project } from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteProjectDialogComponent {
  inputValue = '';
  submitting = false;
  clearResourceTip = '';

  constructor(
    @Inject(DIALOG_DATA)
    public readonly modalData: {
      project: Project;
      projectName: string;
      quotas: FedQuotaDisplay[];
    },
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sApi: K8sApiService,
    private readonly dialogRef: DialogRef,
    private readonly messageService: MessageService,
    private readonly translateService: TranslateService,
  ) {
    const abNormalClusters = modalData.quotas.filter(
      ({ isNormal }) => !isNormal,
    );
    if (abNormalClusters.length) {
      this.clearResourceTip = this.translateService.get(
        'clear_project_resource_tip',
        {
          name: abNormalClusters.map(({ name }) => name).join(', '),
        },
      );
    }
  }

  confirm() {
    this.submitting = true;
    this.k8sApi
      .deleteGlobalResource({
        type: RESOURCE_TYPES.PROJECT,
        resource: this.modalData.project,
      })
      .pipe(
        finalize(() => {
          this.projectDeleteFailed();
        }),
      )
      .subscribe(project => {
        if (project.status.phase === 'Error') {
          this.messageService.error(
            this.translateService.get('project_delete_failed'),
          );
          this.projectDeleteFailed();
          return;
        }
        this.messageService.success(
          this.translateService.get('project_delete_successed'),
        );
        this.dialogRef.close(project);
      });
  }

  projectDeleteFailed() {
    this.submitting = false;
    this.cdr.markForCheck();
  }

  cancel() {
    this.dialogRef.close(null);
  }

  get disabled() {
    return this.inputValue !== this.modalData.project.metadata.name;
  }

  get deleteConfirm() {
    return this.translateService.get('project_delete_confirm', {
      name: this.modalData.project.metadata.name,
    });
  }

  get inputTitle() {
    return this.translateService.get('confirm_delete_text', {
      name: this.modalData.project.metadata.name,
    });
  }
}
