import {
  DESCRIPTION,
  DISPLAY_NAME,
  K8sApiService,
  K8sUtilService,
  StringMap,
  TranslateService,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { get } from 'lodash-es';
import { Subject } from 'rxjs';

import { Project } from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';

@Component({
  templateUrl: './update-info-dialog.component.html',
  styleUrls: ['./update-info-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BasicInfoUpdateDialogComponent {
  displayName = '';
  description = '';
  annotations: StringMap;

  loading$$ = new Subject<boolean>();

  constructor(
    public dialogRef: DialogRef<BasicInfoUpdateDialogComponent, Project>,
    private readonly message: MessageService,
    private readonly k8sUtil: K8sUtilService,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService,
    @Inject(DIALOG_DATA)
    public dialogData: {
      project: Project;
      name: string;
    },
  ) {
    this.annotations = get(this.dialogData.project, 'metadata.annotations', {});
    this.displayName = this.k8sUtil.getDisplayName(this.dialogData.project);
    this.description = this.k8sUtil.getDescription(this.dialogData.project);
  }

  update() {
    this.loading$$.next(true);
    const part = {
      metadata: {
        annotations: {
          ...this.annotations,
          [this.k8sUtil.normalizeType(DISPLAY_NAME)]: this.displayName,
          [this.k8sUtil.normalizeType(DESCRIPTION)]: this.description,
        },
      },
    };
    this.k8sApi
      .patchGlobalResource({
        type: RESOURCE_TYPES.PROJECT,
        resource: this.dialogData.project,
        part,
      })
      .subscribe(
        project => {
          this.message.success({
            content: this.translate.get('project_update_successed'),
          });
          this.dialogRef.close(project);
        },
        () => {
          this.loading$$.next(false);
        },
      );
  }
}
