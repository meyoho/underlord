import { NgModule } from '@angular/core';

import { ManageProjectSharedModule } from 'app/manage-project/shared.module';
import { SharedModule } from 'app/shared/shared.module';

import { AddClusterDialogComponent } from './add-cluster/add-cluster-dialog.component';
import { ProjectCreateComponent } from './create/create.component';
import { DeleteProjectDialogComponent } from './delete/component';
import { ProjectDetailComponent } from './detail/detail.component';
import { ProjectListPageComponent } from './list/list.component';
import { ProjectListTableComponent } from './list/table.component';
import { ProjectQuotaDisplayListComponent } from './quota-display-list/component';
import { ProjectQuotaFormComponent } from './quota-form/component';
import { RemoveProjectDialogComponent } from './remove-cluster/remove-cluster-dialog.component';
import { ProjectRoutingModule } from './routing.module';
import { BasicInfoUpdateDialogComponent } from './update-info/update-info-dialog.component';
import { UpdateLogPolicyDialogComponent } from './update-log-policy-dialog/update-log-policy-dialog.component';
import { UpdateQuotaComponent } from './update-quota/update-quota.component';
@NgModule({
  imports: [SharedModule, ManageProjectSharedModule, ProjectRoutingModule],
  declarations: [
    ProjectListPageComponent,
    ProjectListTableComponent,
    ProjectCreateComponent,
    ProjectDetailComponent,
    ProjectQuotaDisplayListComponent,
    ProjectQuotaFormComponent,
    DeleteProjectDialogComponent,
    BasicInfoUpdateDialogComponent,
    UpdateQuotaComponent,
    RemoveProjectDialogComponent,
    AddClusterDialogComponent,
    UpdateLogPolicyDialogComponent,
  ],
  entryComponents: [
    DeleteProjectDialogComponent,
    BasicInfoUpdateDialogComponent,
    RemoveProjectDialogComponent,
    AddClusterDialogComponent,
    UpdateLogPolicyDialogComponent,
  ],
  exports: [ProjectListPageComponent, ProjectCreateComponent],
})
export class ProjectModule {}
