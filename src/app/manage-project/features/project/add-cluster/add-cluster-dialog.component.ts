import {
  K8sApiService,
  K8sUtilService,
  TranslateService,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { get } from 'lodash-es';
import { Subject, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

import { AdvanceApi } from 'app/api/advance/api';
import {
  Cluster,
  ClusterQuotaMap,
  FED_MEMBER,
  FedCluster,
} from 'app/api/project/types';
import { UNITE_QUOTA_FED_CLUSTERS } from 'app/services/k8s-util.service';
import {
  ClusterFed,
  K8sQuotaItem,
  Project,
  ProjectCluster,
  TKECluster,
} from 'app/typings';
import { RESOURCE_TYPES, toUnitGi, toUnitNum } from 'app/utils';

import { disableFedCheck, getClusterStatus } from '../util';

@Component({
  templateUrl: './add-cluster-dialog.component.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddClusterDialogComponent {
  selectCluster: FedCluster;
  quota: K8sQuotaItem;
  quotaMap: ClusterQuotaMap = {};
  uniteCheck: boolean;
  toUnitNum = toUnitNum;
  toUnitGi = toUnitGi;

  loading$$ = new Subject<boolean>();
  fedClusters: FedCluster[] = [];
  clusters$ = forkJoin([
    this.k8sApi.getGlobalResourceList<TKECluster>({
      type: RESOURCE_TYPES.TKE_CLUSTER,
    }),
    this.advanceApi.getAvailableResource(),
    this.k8sApi.getGlobalResourceList<ClusterFed>({
      type: RESOURCE_TYPES.CLUSTER_FED,
    }),
  ]).pipe(
    map(([clusters, limits, fedClusters]) => {
      const clusterList: Cluster[] = [];
      this.fedClusters = this.genFedClusters(clusters.items, fedClusters.items);
      (clusters.items || []).forEach(cluster => {
        const clusterName = this.k8sUtil.getName(cluster);
        const limit = limits.items
          ? limits.items.find(
              _limit => _limit.metadata.name === cluster.metadata.name,
            )
          : null;
        const quota = {
          quota: {},
          _quota: {},
          limit: limit ? limit.status.allocatable : {},
          capacity: limit ? limit.status.capacity : null,
          used: limit ? limit.status.used : null,
        };
        this.quotaMap[clusterName] = quota;
        if (!this.dialogData.clusters.find(c => c.name === clusterName)) {
          clusterList.push({
            name: clusterName,
            displayName: cluster.spec.displayName,
            isNormal: getClusterStatus(cluster) && !!limit,
            ...quota,
          });
        }
      });
      this.cdr.markForCheck();
      return clusterList;
    }),
  );

  constructor(
    public dialogRef: DialogRef<AddClusterDialogComponent, Project>,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService,
    @Inject(DIALOG_DATA)
    public dialogData: {
      project: Project;
      name: string;
      clusters: Cluster[];
      uniteFeds: string;
    },
    private readonly advanceApi: AdvanceApi,
    private readonly k8sUtil: K8sUtilService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  genFedClusters(clusters: TKECluster[], fedClusters: ClusterFed[]) {
    const clusterList: FedCluster[] = [];
    (fedClusters || []).forEach(fedCluster => {
      const disabledCheck = disableFedCheck(fedCluster.spec.clusters, clusters);
      const hasFedCluster = fedCluster.spec.clusters.some((c: Cluster) => {
        return this.dialogData.clusters.find(dc => dc.name === c.name);
      });
      if (!hasFedCluster) {
        clusterList.push({
          name: this.k8sUtil.getName(fedCluster),
          displayName: this.k8sUtil.getDisplayName(fedCluster),
          clusters: fedCluster.spec.clusters.map(cluster => ({
            ...cluster,
            isNormal: getClusterStatus(
              clusters.find(cl => cl.metadata.name === cluster.name),
            ),
          })),
          disabledCheck,
        });
      }
    });
    return clusterList;
  }

  isFedCluster(cluster: FedCluster) {
    return cluster && cluster.clusters;
  }

  isClusterFedDisabled(fedCluster: FedCluster) {
    return fedCluster.clusters.some(cluster => !cluster?.isNormal);
  }

  selectClusterChange() {
    if (this.isFedCluster(this.selectCluster)) {
      this.uniteCheck = !this.selectCluster.disabledCheck;
    }
    this.quota = {};
  }

  fedClusterCheckChange(cluster: FedCluster) {
    cluster.clusters.forEach(cluster => {
      this.quotaMap[cluster.name].quota = {};
    });
  }

  getQuotaLimit(cluster: Cluster, key: string) {
    return cluster ? get(this.quotaMap[cluster.name], key) : {};
  }

  getFedClusters(cluster: FedCluster) {
    if (this.uniteCheck) {
      return [cluster.clusters[0]];
    }
    return cluster.clusters;
  }

  add(quotaForm: NgForm) {
    quotaForm.onSubmit(null);
    if (!quotaForm.valid) {
      return;
    }
    const project: Project = {
      spec: {
        clusters: this.dialogData.clusters as ProjectCluster[],
      },
    };
    if (this.isFedCluster(this.selectCluster)) {
      const selectCluster: FedCluster = this.selectCluster;
      const defaultQuotaIndex = selectCluster.clusters[0].name;
      selectCluster.clusters.forEach(c => {
        project.spec.clusters.push({
          name: c.name,
          quota: this.uniteCheck
            ? this.quotaMap[defaultQuotaIndex]._quota
            : this.quotaMap[c.name]._quota,
          type: FED_MEMBER,
        });
      });
      if (this.uniteCheck) {
        project.metadata = {
          annotations: {
            [this.k8sUtil.normalizeType(UNITE_QUOTA_FED_CLUSTERS)]: this
              .dialogData.uniteFeds
              ? `${this.dialogData.uniteFeds},${this.selectCluster.name}`
              : this.selectCluster.name,
          },
        };
      }
    } else {
      project.spec.clusters.push({
        name: this.selectCluster.name,
        quota: this.quota,
      });
    }
    this.loading$$.next(true);
    this.k8sApi
      .patchGlobalResource({
        type: RESOURCE_TYPES.PROJECT,
        resource: this.dialogData.project,
        part: project,
      })
      .subscribe(
        project => {
          this.message.success({
            content: this.translate.get('add_cluster_successed'),
          });
          this.dialogRef.close(project);
        },
        () => {
          this.loading$$.next(false);
        },
      );
  }
}
