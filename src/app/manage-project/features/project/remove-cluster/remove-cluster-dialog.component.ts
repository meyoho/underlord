import {
  K8sApiService,
  K8sUtilService,
  TranslateService,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import { Component, Inject } from '@angular/core';
import { differenceBy, remove } from 'lodash-es';

import { Cluster, FedQuotaDisplay } from 'app/api/project/types';
import { UNITE_QUOTA_FED_CLUSTERS } from 'app/services/k8s-util.service';
import { ClusterFed, Project, ProjectCluster } from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';

@Component({
  templateUrl: './remove-cluster-dialog.component.html',
  styleUrls: ['style.scss'],
})
export class RemoveProjectDialogComponent {
  inputValue: string;
  clusterNames: string[] = [];
  deleting = false;
  clearResource = false;
  clearResourceTip = '';

  constructor(
    public dialogRef: DialogRef<RemoveProjectDialogComponent, Project>,
    private readonly k8sApiService: K8sApiService,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly k8sUtil: K8sUtilService,
    @Inject(DIALOG_DATA)
    public dialogData: {
      project: Project;
      clusters: Cluster[];
      projectName: string;
      fedClusters: ClusterFed[];
      uniteFeds: string;
      quotas: FedQuotaDisplay[];
    },
  ) {
    if (this.dialogData.fedClusters.length > 0) {
      this.dialogData.clusters.forEach(cluster => {
        const has = this.dialogData.fedClusters.some(fed =>
          fed.spec.clusters.find(
            specCluster => specCluster.name === cluster.name,
          ),
        );
        if (!has) {
          this.clusterNames.push(cluster.name);
        }
      });
      this.dialogData.fedClusters.forEach(fed => {
        this.clusterNames.push(fed.metadata.name);
      });
    } else {
      this.clusterNames = this.dialogData.clusters.map(cluster => cluster.name);
    }
    const abnormalClusters =
      dialogData.quotas.filter(({ isNormal }) => !isNormal) || [];
    const abNormalFeds = dialogData.fedClusters.filter(
      clusterFed => clusterFed.status.phase === 'Error',
    );
    if (abnormalClusters.length || abNormalFeds.length) {
      this.clearResourceTip = translate.get('clear_project_resource_tip', {
        name: abnormalClusters
          .map(({ name }) => name)
          .concat(abNormalFeds.map(fed => fed.metadata.name))
          .join(', '),
      });
    }
  }

  isFedCluster(name: string) {
    return this.dialogData.fedClusters.find(fdc => fdc.metadata.name === name);
  }

  removeCluster() {
    this.deleting = true;
    let uniteFedList = (this.dialogData.uniteFeds || '').split(',');
    let rmClusters: Cluster[] = [];
    const fedCluster = this.isFedCluster(this.inputValue);
    if (fedCluster) {
      uniteFedList = remove(uniteFedList, this.inputValue);
      rmClusters = fedCluster.spec.clusters;
    } else {
      rmClusters = [{ name: this.inputValue }];
    }
    const clusters = differenceBy(this.dialogData.clusters, rmClusters, 'name');
    const project: Project = {
      metadata: {
        annotations: {
          [this.k8sUtil.normalizeType(
            UNITE_QUOTA_FED_CLUSTERS,
          )]: uniteFedList.join(','),
        },
      },
      spec: {
        clusters: clusters as ProjectCluster[],
        clusterDeletePolicy: this.clearResource ? 'Delete' : 'Retain',
      },
    };
    this.k8sApiService
      .patchGlobalResource({
        type: RESOURCE_TYPES.PROJECT,
        resource: this.dialogData.project,
        part: project,
      })
      .subscribe(
        project => {
          this.message.success(this.translate.get('cluster_remove_successed'));
          this.dialogRef.close(project);
          this.deleting = false;
        },
        () => {
          this.deleting = false;
        },
      );
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
