import {
  AsyncDataLoader,
  K8sApiService,
  K8sUtilService,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import { MessageService, NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { cloneDeep, get } from 'lodash-es';
import { combineLatest, forkJoin, of } from 'rxjs';
import { catchError, map, pluck, switchMap } from 'rxjs/operators';

import { AdvanceApi } from 'app/api/advance/api';
import {
  Cluster,
  ClusterQuotaMap,
  FED_MEMBER,
  FedCluster,
} from 'app/api/project/types';
import { UNITE_QUOTA_FED_CLUSTERS } from 'app/services/k8s-util.service';
import {
  AvailableResource,
  ClusterFed,
  ClusterFedItem,
  K8sQuotaItem,
  Project,
  ProjectQuota,
  TKECluster,
} from 'app/typings';
import { RESOURCE_TYPES, addUnitMi, toUnitMi } from 'app/utils';

import { ProjectQuotaFormComponent } from '../quota-form/component';

interface Params {
  project: string;
  clusters?: string[];
}
@Component({
  templateUrl: './update-quota.component.html',
  styleUrls: ['./update-quota.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateQuotaComponent {
  @ViewChild('quotaForm')
  quotaForm: ProjectQuotaFormComponent;

  submitting = false;
  projectName: string;
  quotaMap: ClusterQuotaMap = {};
  uniteFedClusters: string[] = [];
  abnormalClusters: string[] = [];
  project: Project;

  identity$ = combineLatest([
    this.route.parent.parent.paramMap,
    this.route.parent.parent.queryParamMap,
  ]).pipe(
    map(([params, query]) => {
      this.projectName = params.get('project');
      const cluster = query.get('cluster');
      return {
        project: this.projectName,
        clusters: cluster ? cluster.split(',') : [],
      };
    }),
    publishRef(),
  );

  dataLoader = new AsyncDataLoader<Array<Cluster | FedCluster>, Params>({
    params$: this.identity$,
    fetcher: this.fetchProject.bind(this),
  });

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly message: MessageService,
    private readonly notification: NotificationService,
    private readonly translate: TranslateService,
    private readonly advanceApi: AdvanceApi,
    private readonly k8sUtil: K8sUtilService,
    private readonly k8sApi: K8sApiService,
  ) {}

  fetchProject(params: Params) {
    return forkJoin([
      this.k8sApi
        .getGlobalResource({
          type: RESOURCE_TYPES.PROJECT,
          name: params.project,
        })
        .pipe(
          switchMap((project: Project) => {
            this.project = project;
            this.uniteFedClusters = (
              this.k8sUtil.getAnnotation(project, UNITE_QUOTA_FED_CLUSTERS) ||
              ''
            ).split(',');
            return forkJoin(
              get(project, 'spec.clusters', [])
                .filter((cluster: Cluster) =>
                  params.clusters.includes(cluster.name),
                )
                .map((cluster: Cluster) =>
                  this.genCluster(cluster, params.project),
                ),
            );
          }),
        ),
      this.advanceApi.getAvailableResource(),
      this.k8sApi
        .getGlobalResourceList<TKECluster>({
          type: RESOURCE_TYPES.TKE_CLUSTER,
        })
        .pipe(pluck('items')),
      this.k8sApi
        .getGlobalResourceList<ClusterFed>({
          type: RESOURCE_TYPES.CLUSTER_FED,
        })
        .pipe(
          pluck('items'),
          catchError(() => of([])),
        ),
    ]).pipe(
      map(([clusters, resourceLimit, originClusters, fedClusters]) => {
        const selectCluster: Array<Cluster | FedCluster> = [];
        clusters.forEach((cluster: Cluster) => {
          const originCluster = (originClusters || []).find(
            _cluster => _cluster.metadata.name === cluster.name,
          );
          cluster.displayName = originCluster.spec.displayName;
          let hostName = '';
          let fedClusterName = '';
          const found = fedClusters.find(fed =>
            fed.spec.clusters.some(
              (clu: ClusterFedItem) => clu.name === originCluster.metadata.name,
            ),
          );
          if (found) {
            fedClusterName = found.metadata.name;
            const host = found.spec.clusters.find(
              (member: ClusterFedItem) => member.type === 'Host',
            );
            hostName = host.name;
          }
          const limit = (resourceLimit.items || []).find(
            _limit => _limit.metadata.name === cluster.name,
          );
          const quota = this.genQuota(limit, cluster, hostName);
          this.quotaMap[cluster.name] = quota;
          if (
            fedClusterName &&
            cluster.type === FED_MEMBER &&
            fedClusters.length > 0
          ) {
            const originFed = fedClusters.find(
              _fed => this.k8sUtil.getName(_fed) === fedClusterName,
            );
            const findFed: FedCluster = selectCluster.find(
              c => c.name === fedClusterName,
            );
            if (findFed) {
              findFed.clusters.push({ name: cluster.name });
            } else {
              selectCluster.push({
                name: fedClusterName,
                displayName: this.k8sUtil.getDisplayName(originFed) || '',
                clusters: [
                  {
                    name: cluster.name,
                  },
                ],
                check: this.uniteFedClusters.includes(fedClusterName),
              });
            }
          } else {
            selectCluster.push(cluster);
          }
        });
        return selectCluster;
      }),
    );
  }

  mergeQuotaLimit(quota: K8sQuotaItem, limit: K8sQuotaItem) {
    if (!quota || !limit) {
      return limit || quota || {};
    }
    const mergeLimit: K8sQuotaItem = {};
    Object.entries(limit).forEach(([key, value]) => {
      switch (key) {
        case 'pods':
        case 'persistentvolumeclaims':
        case 'limits.cpu':
        case 'requests.cpu':
          if (quota[key] && value) {
            const _value = +value > 0 ? +value : 0;
            mergeLimit[key] = (+quota[key] + _value).toString();
          } else {
            mergeLimit[key] = quota[key] || value || '0';
          }
          break;
        case 'requests.memory':
        case 'limits.memory':
        case 'requests.storage':
          mergeLimit[key] = addUnitMi(
            +toUnitMi(quota[key], true) + +toUnitMi(value, true),
          );
      }
    });
    return mergeLimit;
  }

  genQuota(limit: AvailableResource, cluster: Cluster, fedHostName: string) {
    return {
      // 更新时候的 limit 应该是当前 project 的配额值加limit
      limit: this.mergeQuotaLimit(
        cluster.quota,
        limit ? limit.status.allocatable : null,
      ),
      capacity: limit ? limit.status.capacity : null,
      used: limit ? limit.status.used : null,
      hard: cluster.hard,
      quota: cluster.quota,
      fedHostName,
      _quota: cloneDeep(cluster.quota),
    };
  }

  update() {
    if (this.quotaForm && this.quotaForm.quotaForm) {
      this.quotaForm.quotaForm.onSubmit(null);
      if (!this.quotaForm.quotaForm.valid) {
        return;
      }
    }
    this.submitting = true;
    const {
      clusters,
      uniteQuotaFedClusters,
    } = this.quotaForm.getQuotaFormData();
    const postClusters = cloneDeep(this.project.spec.clusters);
    postClusters.forEach(postCluster => {
      const clusterQuota = clusters.find(
        cluster => cluster.name === postCluster.name,
      );
      if (clusterQuota) {
        postCluster.quota = clusterQuota.quota;
      }
    });
    const part = {
      metadata: {
        annotations: {
          [this.k8sUtil.normalizeType(
            UNITE_QUOTA_FED_CLUSTERS,
          )]: uniteQuotaFedClusters,
        },
      },
      spec: {
        clusters: postClusters,
      },
    };
    this.k8sApi
      .patchGlobalResource({
        type: RESOURCE_TYPES.PROJECT,
        resource: this.project,
        part,
      })
      .subscribe(
        () => {
          this.message.success({
            content: this.translate.get('project_update_successed'),
          });
          this.router.navigate(['..'], {
            relativeTo: this.route,
          });
        },
        err => {
          this.notification.error({
            title: this.translate.get('project_update_failed'),
            content: err.message || err.error,
          });
          this.submitting = false;
          this.cdr.markForCheck();
        },
      );
  }

  genCluster(cluster: Cluster, projectName: string) {
    return this.k8sApi
      .getResource<ProjectQuota>({
        type: RESOURCE_TYPES.PROJECT_QUOTA,
        cluster: cluster.name,
        name: projectName,
      })
      .pipe(
        catchError(() => of({ status: {} } as ProjectQuota)),
        map(
          projectQuota =>
            ({
              ...cluster,
              hard: projectQuota.status.hard,
            } as Cluster),
        ),
      );
  }
}
