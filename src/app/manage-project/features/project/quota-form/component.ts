import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import {
  Cluster,
  ClusterQuotaMap,
  FED_MEMBER,
  FedCluster,
} from 'app/api/project/types';
import { ProjectCluster } from 'app/typings';

@Component({
  selector: 'alu-project-quota-form',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectQuotaFormComponent {
  @Input()
  clusters: FedCluster[];

  @Input()
  quotaMap: ClusterQuotaMap;

  @Input()
  isUpdate = false;

  @Input()
  isAddCluster = true;

  @ViewChild('form', { static: true })
  quotaForm: NgForm;

  isFedCluster = (cluster: FedCluster) => {
    return cluster.clusters && cluster.clusters.length;
  };

  getQuotaFormData() {
    const clusters: ProjectCluster[] = [];
    let uniteQuotaFedClusters = '';
    this.clusters.forEach((cluster: FedCluster) => {
      if (cluster.clusters) {
        const defaultQuotaIndex = cluster.clusters[0].name;
        if (cluster.check) {
          uniteQuotaFedClusters = uniteQuotaFedClusters
            ? `${uniteQuotaFedClusters},${cluster.name}`
            : cluster.name;
        }
        cluster.clusters.forEach(_cluster => {
          clusters.push({
            name: _cluster.name,
            quota: cluster.check
              ? this.quotaMap[defaultQuotaIndex]._quota
              : this.quotaMap[_cluster.name]._quota,
            type: FED_MEMBER,
          });
        });
      } else {
        clusters.push({
          name: cluster.name,
          quota: this.quotaMap[cluster.name]._quota,
        });
      }
    });
    return { clusters, uniteQuotaFedClusters };
  }

  getFedClusters(cluster: FedCluster) {
    if (cluster.check) {
      return [cluster.clusters[0]];
    }
    return cluster.clusters;
  }

  fedClusterCheckChange(cluster: FedCluster) {
    if (cluster.check) {
      this.quotaMap[cluster.clusters[0].name]._quota = {};
    }
    cluster.clusters.forEach(cls => {
      this.quotaMap[cls.name].quota = cluster.check
        ? {}
        : this.quotaMap[cluster.clusters[0].name]._quota;
    });
  }

  getDisplayName(cluster: Cluster) {
    return `${cluster.name}${
      cluster.displayName ? ' (' + cluster.displayName + ')' : ''
    }`;
  }
}
