import {
  K8sApiService,
  K8sUtilService,
  publishRef,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { get } from 'lodash-es';
import { map } from 'rxjs/operators';

import { Cluster } from 'app/api/project/types';
import { ClusterFed, Project, TKECluster } from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';

@Component({
  selector: 'alu-project-list-table',
  templateUrl: 'table.component.html',
  styleUrls: ['table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectListTableComponent implements OnInit {
  @Input() data: Project[];

  @Input() clusterFeds: ClusterFed[];

  columns = ['name', 'status', 'clusters', 'creationTimestamp'];
  clusters: Array<{
    name: string;
    displayName: string;
  }> = [];

  constructor(
    private readonly k8sApi: K8sApiService,
    private readonly k8sUtil: K8sUtilService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.k8sApi
      .getGlobalResourceList<TKECluster>({
        type: RESOURCE_TYPES.TKE_CLUSTER,
      })
      .pipe(
        map(list =>
          list.items.map(item => ({
            name: item.metadata.name,
            displayName: item.spec.displayName,
          })),
        ),
        publishRef(),
      )
      .subscribe(clusters => {
        this.clusters = clusters;
        this.cdr.markForCheck();
      });
  }

  getDisplayName = (name: string) => {
    const item = this.clusters.find(it => it.name === name);
    if (item && item.displayName) {
      return `${name} (${item.displayName})`;
    }
    return name;
  };

  getClusters(item: Project) {
    return get(item, 'spec.clusters', []);
  }

  getClusterFedDisplayName(cluster: Cluster) {
    const item = (this.clusterFeds || []).find(it => {
      return !!it.spec.clusters.find(_c => _c.name === cluster.name);
    });
    if (item) {
      return this.k8sUtil.getUnionDisplayName(item);
    }
    return '';
  }
}
