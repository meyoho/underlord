import {
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  ResourceListParams,
  StringMap,
  isAllowed,
  publishRef,
} from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { isEqual } from 'lodash-es';
import { Observable, of } from 'rxjs';
import { catchError, distinctUntilChanged, map, pluck } from 'rxjs/operators';

import { ProjectApiService } from 'app/api/project/api';
import { ClusterFed, Project } from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';

@Component({
  selector: 'alu-project-list',
  templateUrl: 'list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectListPageComponent {
  searchTypeList = ['name', 'display_name'];
  searchType = 'name';
  searchKeyMap: StringMap = {
    name: 'name',
    display_name: 'display-name',
  };

  params$ = this.route.queryParamMap.pipe(
    map(queryParams => ({
      keywords: queryParams.get('keywords') || '',
      search_type: queryParams.get('search_type') || '',
    })),
    distinctUntilChanged((x, y) => {
      if (x.search_type !== y.search_type && !y.keywords) {
        return true;
      }
      return isEqual(x, y);
    }),
    publishRef(),
  );

  keywords$ = this.params$.pipe(
    map(params => params.keywords),
    publishRef(),
  );

  fetchParams$: Observable<StringMap> = this.params$.pipe(
    map(params => {
      if (!params.keywords) {
        return null;
      }
      let searchKey = 'name';
      if (params.search_type) {
        this.searchType = params.search_type;
        searchKey = this.searchKeyMap[params.search_type];
      }
      return {
        filterBy: `${searchKey},${params.keywords}`,
      };
    }),
    publishRef(),
  );

  permissions$ = this.k8sPermission
    .getAccess({
      type: RESOURCE_TYPES.PROJECT,
      action: K8sResourceAction.CREATE,
    })
    .pipe(isAllowed());

  clusterFeds$ = this.k8sApi
    .getGlobalResourceList<ClusterFed>({
      type: RESOURCE_TYPES.CLUSTER_FED,
    })
    .pipe(
      pluck('items'),
      catchError(() => of([])),
    );

  list = new K8SResourceList<Project>({
    fetchParams$: this.fetchParams$,
    fetcher: this.fetchProjects.bind(this),
  });

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly projectApi: ProjectApiService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sApi: K8sApiService,
  ) {}

  fetchProjects(params: ResourceListParams) {
    return this.projectApi.getProjects(params);
  }

  search(keywords: string) {
    this.router.navigate([], {
      queryParams: { keywords },
      queryParamsHandling: 'merge',
    });
  }

  getPlaceHolder(type: string) {
    switch (type) {
      case 'name':
        return 'search_by_name_placeholder';
      case 'display_name':
        return 'search_by_display_name_placeholder';
    }
  }

  searchTypeChange(searchType: string) {
    this.router.navigate([], {
      queryParams: { search_type: searchType },
      queryParamsHandling: 'merge',
    });
  }
}
