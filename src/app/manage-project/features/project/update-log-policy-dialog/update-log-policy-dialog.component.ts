import {
  K8sUtilService,
  TOKEN_BASE_DOMAIN,
  TranslateService,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import {
  INDICES_KEEP_DAYS,
  LogPolicyService,
  POLICY_ENABLED,
} from 'app/api/log';
import { Project } from 'app/typings';
@Component({
  selector: 'alu-update-log-policy-dialog',
  templateUrl: './update-log-policy-dialog.component.html',
  styleUrls: ['./update-log-policy-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateLogPolicyDialogComponent implements OnInit {
  viewModel = {
    log: 7,
    enable: false,
  };

  @ViewChild('form', { static: true })
  form: NgForm;

  submitting: boolean;
  constructor(
    @Inject(DIALOG_DATA)
    public dialogData: {
      resource: Project;
    },
    private readonly dialogRef: DialogRef,
    private readonly messageService: MessageService,
    private readonly translate: TranslateService,
    @Inject(TOKEN_BASE_DOMAIN)
    private readonly baseDomain: string,
    private readonly api: LogPolicyService,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  ngOnInit() {
    this.viewModel = {
      log: +(
        this.k8sUtil.getLabel(this.dialogData.resource, INDICES_KEEP_DAYS) || 7
      ),
      enable: this.isTrue(
        this.k8sUtil.getLabel(this.dialogData.resource, POLICY_ENABLED),
      ),
    };
  }

  isTrue(value: string) {
    return '' + value === 'true';
  }

  confirm(form: NgForm) {
    form.onSubmit(null);
    if (form.invalid) {
      return;
    }
    this.submitting = true;
    const payload = this.handleProjectProxy();
    this.api.updateProjectPolicy(this.dialogData.resource, payload).subscribe(
      () => {
        this.submitting = false;
        this.dialogRef.close(true);
        this.messageService.success(
          this.translate.get('successful_log_policy_update'),
        );
      },
      () => {
        this.submitting = false;
      },
    );
  }

  valueMathCeil() {
    this.viewModel.log = Math.ceil(this.viewModel.log);
  }

  handleProjectProxy() {
    const project = this.dialogData.resource;
    const {
      metadata: { labels },
    } = project;
    const { enable, log } = this.viewModel;
    return {
      metadata: {
        labels: {
          ...labels,
          [`${this.baseDomain}/${INDICES_KEEP_DAYS}`]: enable ? '' + log : null,
          [`${this.baseDomain}/${POLICY_ENABLED}`]: '' + enable,
        },
      },
    };
  }
}
