import {
  AsyncDataLoader,
  FeatureGateService,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { get, set } from 'lodash-es';
import { Subject, combineLatest, forkJoin, merge, of } from 'rxjs';
import { catchError, map, pluck, switchMap, tap } from 'rxjs/operators';

import { AdminApiService } from 'app/api/admin/api';
import { AdvanceApi } from 'app/api/advance/api';
import {
  Cluster,
  FED_MEMBER,
  FedQuotaDisplay,
  QuotaDisplay,
} from 'app/api/project/types';
import {
  K8sUtilService,
  UNITE_QUOTA_FED_CLUSTERS,
} from 'app/services/k8s-util.service';
import {
  AvailableResource,
  ClusterFed,
  K8sResourceStatus,
  Project,
  ProjectCluster,
  ProjectQuota,
  TKECluster,
} from 'app/typings';
import { K8S_QUOTA_TYPES, RESOURCE_TYPES } from 'app/utils';

import { AddClusterDialogComponent } from '../add-cluster/add-cluster-dialog.component';
import { DeleteProjectDialogComponent } from '../delete/component';
import { RemoveProjectDialogComponent } from '../remove-cluster/remove-cluster-dialog.component';
import { BasicInfoUpdateDialogComponent } from '../update-info/update-info-dialog.component';
import { UpdateLogPolicyDialogComponent } from '../update-log-policy-dialog/update-log-policy-dialog.component';
import { getClusterStatus } from '../util';
@Component({
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectDetailComponent {
  K8S_QUOTA_TYPES = K8S_QUOTA_TYPES;
  params$ = this.route.parent.parent.paramMap.pipe(
    map(paramMap => ({
      name: paramMap.get('project'),
    })),
    publishRef(),
  );

  projectAdmins$ = this.params$.pipe(
    switchMap(params =>
      this.adminApi.getAdmins(RESOURCE_TYPES.PROJECT, params.name),
    ),
    publishRef(),
  );

  project$$ = new Subject<Project>();

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.PROJECT,
    action: [K8sResourceAction.UPDATE, K8sResourceAction.DELETE],
  });

  includeClusterFed: ClusterFed[] = [];

  dataLoader = new AsyncDataLoader<Project, { name: string }>({
    params$: this.params$,
    fetcher: this.fetchProject.bind(this),
  });

  quotas: FedQuotaDisplay[];
  quotas$ = combineLatest([
    merge(this.project$$, this.dataLoader.data$),
    this.k8sApi
      .getGlobalResourceList<TKECluster>({
        type: RESOURCE_TYPES.TKE_CLUSTER,
      })
      .pipe(pluck('items')),
    this.k8sApi
      .getGlobalResourceList<ClusterFed>({
        type: RESOURCE_TYPES.CLUSTER_FED,
      })
      .pipe(
        pluck('items'),
        catchError(() => of([])),
      ),
    this.advanceApi.getAvailableResource(),
    this.fg.isEnabled('kubefed'),
  ]).pipe(
    switchMap(([project, clusters, fedClusters, limits, kubefedEnabled]) =>
      this.hasCluster(project)
        ? this.fetchProjectQuota(
            project,
            clusters,
            fedClusters,
            limits.items,
            kubefedEnabled,
          )
        : of([]),
    ),
    tap(quotas => (this.quotas = quotas)),
    publishRef(),
  );

  intervalTimer: number;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly dialogService: DialogService,
    private readonly adminApi: AdminApiService,
    private readonly k8sApi: K8sApiService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sUtil: K8sUtilService,
    private readonly advanceApi: AdvanceApi,
    private readonly fg: FeatureGateService,
  ) {}

  fetchProject({ name }: { name: string }) {
    return this.k8sApi
      .watchGlobalResource<Project>({
        type: RESOURCE_TYPES.PROJECT,
        name,
      })
      .pipe(
        map(project => {
          if (
            !project.status ||
            project.status.phase === K8sResourceStatus.EMPTY
          ) {
            set(project, 'status.phase', K8sResourceStatus.CREATING);
          }
          return project;
        }),
      );
  }

  fetchProjectQuota(
    project: Project,
    clusters: TKECluster[],
    fedClusters: ClusterFed[],
    limits: AvailableResource[],
    kubefedEnabled: boolean,
  ) {
    return forkJoin(
      this.getClusters(project).map(cluster =>
        this.k8sApi
          .getResource<ProjectQuota>({
            type: RESOURCE_TYPES.PROJECT_QUOTA,
            cluster: cluster.name,
            name: project.metadata.name,
          })
          .pipe(
            catchError(() => of(null)),
            map(projectQuota => {
              return {
                name: cluster.name,
                displayName: this.getClusterDisplayName(cluster.name, clusters),
                isNormal:
                  getClusterStatus(
                    clusters.find(
                      it => this.k8sUtil.getName(it) === cluster.name,
                    ),
                  ) && !!projectQuota,
                ...this.getFedClusterName(cluster, fedClusters, kubefedEnabled),
                projectQuota: projectQuota || {},
                capacity: limits.find(
                  limit => limit.metadata.name === cluster.name,
                )?.status.capacity,
              };
            }),
          ),
      ),
    ).pipe(
      map(items => {
        const quotas: FedQuotaDisplay[] = [];
        items.forEach(item => {
          if (item.fedClusterName) {
            const findQuota = quotas.find(
              quota => quota.displayName === item.fedClusterName,
            );
            if (findQuota) {
              findQuota.clusters.push(item);
            } else {
              quotas.push({
                displayName: item.fedClusterName,
                clusters: [item],
              });
            }
          } else {
            quotas.push(item);
          }
        });
        return quotas;
      }),
    );
  }

  getClusters(item: Project): ProjectCluster[] {
    return get(item, ['spec', 'clusters'], []);
  }

  hasCluster = (project: Project) => {
    return this.getClusters(project).length;
  };

  getClusterDisplayName(name: string, items: TKECluster[]) {
    const item = items.find(it => this.k8sUtil.getName(it) === name);
    const displayName = item.spec.displayName;
    return item
      ? item.metadata.name + (displayName ? ` (${displayName})` : '')
      : name;
  }

  isClusterFedDisabled(clusters: QuotaDisplay[]) {
    return clusters.some(cluster => !cluster.isNormal);
  }

  isHostClusterFedDisabled(clusters: QuotaDisplay[]) {
    return !clusters.find(cluster => cluster.isHost)?.isNormal;
  }

  getFedClusterString(clusters: QuotaDisplay[]) {
    return clusters.map(cluster => cluster.name).join(',');
  }

  getFedClusterName = (
    cluster: Cluster,
    items: ClusterFed[],
    kubefedEnabled: boolean,
  ): {
    fedClusterName: string;
    isHost: boolean;
  } => {
    let isHost = false;
    if (kubefedEnabled && cluster.type === FED_MEMBER) {
      const item = items.find(({ spec: { clusters } }) => {
        const result = clusters.find(c => c.name === cluster.name);
        if (result) {
          isHost = result.type === 'Host';
        }
        return result;
      });
      if (item) {
        this.genIncludeClusterFed(item);
        return {
          fedClusterName: this.k8sUtil.getUnionDisplayName(item),
          isHost,
        };
      }
    }
    return {
      fedClusterName: '',
      isHost,
    };
  };

  genIncludeClusterFed(fed: ClusterFed) {
    const find = this.includeClusterFed.find(
      item => fed.metadata.name === item.metadata.name,
    );
    if (!find) {
      this.includeClusterFed.push(fed);
    }
  }

  showDeleteDialog(project: Project) {
    const dialogRef = this.dialogService.open(DeleteProjectDialogComponent, {
      data: {
        project,
        quotas: this.quotas,
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.router.navigate(['/home'], {
          relativeTo: this.route,
        });
      }
    });
  }

  showUpdateBasicInfoDialog(project: Project) {
    const dialogRef = this.dialogService.open(BasicInfoUpdateDialogComponent, {
      data: {
        project,
      },
    });
    dialogRef
      .afterClosed()
      .subscribe(result => result && this.dataLoader.reload());
  }

  showUpdateLogPolicyDialog(project: Project) {
    const dialogRef = this.dialogService.open(UpdateLogPolicyDialogComponent, {
      data: {
        resource: project,
      },
    });
    dialogRef
      .afterClosed()
      .subscribe(result => result && this.dataLoader.reload());
  }

  showAddClusterDialog(project: Project) {
    const dialogRef = this.dialogService.open(AddClusterDialogComponent, {
      data: {
        project,
        clusters: this.getClusters(project),
        name: this.k8sUtil.getName(project),
        uniteFeds: this.k8sUtil.getAnnotation(
          project,
          UNITE_QUOTA_FED_CLUSTERS,
        ),
      },
    });
    dialogRef
      .afterClosed()
      .subscribe(result => result && this.dataLoader.reload());
  }

  showRemoveClusterDialog(project: Project) {
    const dialogRef = this.dialogService.open(RemoveProjectDialogComponent, {
      data: {
        project,
        clusters: this.getClusters(project),
        projectName: this.k8sUtil.getName(project),
        uniteFeds: this.k8sUtil.getAnnotation(
          project,
          UNITE_QUOTA_FED_CLUSTERS,
        ),
        fedClusters: this.includeClusterFed,
        quotas: this.quotas,
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.includeClusterFed = [];
        this.project$$.next(result);
        this.dataLoader.reload();
      }
    });
  }
}
