import { uniq } from 'lodash-es';

import { ClusterFedItem, TKECluster } from 'app/typings';

export const getClusterStatus = (cluster: TKECluster) => {
  return cluster?.status?.phase === 'Running';
};

export const disableFedCheck = (
  fedClusters: ClusterFedItem[],
  clusters: TKECluster[],
) => {
  if (!fedClusters.length) {
    return false;
  }
  const gpuTypes: string[] = [];
  fedClusters.forEach(fedCluster => {
    const findCluster = clusters.find(
      cluster => cluster.metadata.name === fedCluster.name,
    );
    if (findCluster) {
      gpuTypes.push(findCluster.spec.features.gpuType);
    }
  });
  return uniq(gpuTypes).length !== 1;
};
