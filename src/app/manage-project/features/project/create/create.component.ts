import {
  DESCRIPTION,
  DISPLAY_NAME,
  K8sApiService,
  K8sUtilService,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { remove } from 'lodash-es';
import { forkJoin, of } from 'rxjs';
import { catchError, map, pluck } from 'rxjs/operators';

import { AdvanceApi } from 'app/api/advance/api';
import {
  Cluster,
  ClusterQuotaMap,
  FedCluster,
  ProjectFormData,
} from 'app/api/project/types';
import { UNITE_QUOTA_FED_CLUSTERS } from 'app/services/k8s-util.service';
import {
  ClusterFed,
  ClusterFedItem,
  Project,
  ProjectCluster,
  TKECluster,
} from 'app/typings';
import {
  K8S_RESOURCE_NAME_BASE,
  RESOURCE_TYPES,
  getYamlApiVersion,
  toUnitGi,
  toUnitNum,
} from 'app/utils';

import { ProjectQuotaFormComponent } from '../quota-form/component';
import { disableFedCheck, getClusterStatus } from '../util';

enum CREATE_STEPS {
  BASIC = 1,
  QUOTA = 2,
}

@Component({
  selector: 'alu-project-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectCreateComponent implements OnInit {
  @ViewChild('basicForm', { static: true })
  basicForm: NgForm;

  @ViewChild('quotaForm', { static: true })
  quotaForm: ProjectQuotaFormComponent;

  identity$ = this.route.parent.parent.paramMap.pipe(
    map(paramMap => ({
      name: paramMap.get('project'),
    })),
    publishRef(),
  );

  selectClusters: Array<FedCluster | Cluster> = [];
  quotaMap: ClusterQuotaMap = {};

  minQuota: number;
  submitting: boolean;

  data: ProjectFormData = {
    name: '',
    displayName: '',
    description: '',
    uniteQuotaFedClusters: '',
    clusters: [],
  };

  curStep = CREATE_STEPS.BASIC;
  stepsEnum = CREATE_STEPS;
  steps: string[];
  projectPatternObj = K8S_RESOURCE_NAME_BASE;
  clusterList: Cluster[] = [];
  toUnitNum = toUnitNum;
  toUnitGi = toUnitGi;
  fedClusters: FedCluster[] = [];

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly advanceApi: AdvanceApi,
    private readonly k8sUtil: K8sUtilService,
    private readonly k8sApi: K8sApiService,
  ) {}

  ngOnInit() {
    this.steps = ['basic_info', 'set_quotas'];
    forkJoin([
      this.k8sApi
        .getGlobalResourceList<TKECluster>({
          type: RESOURCE_TYPES.TKE_CLUSTER,
        })
        .pipe(pluck('items')),
      this.advanceApi.getAvailableResource(),
      this.k8sApi
        .getGlobalResourceList<ClusterFed>({
          type: RESOURCE_TYPES.CLUSTER_FED,
        })
        .pipe(
          pluck('items'),
          catchError(() => of([])),
        ),
    ]).subscribe(([clusters, limits, fedClusters]) => {
      this.fedClusters = (fedClusters || []).map((fedCluster: ClusterFed) => {
        const disabledCheck = disableFedCheck(
          fedCluster.spec.clusters,
          clusters,
        );
        return {
          name: this.k8sUtil.getName(fedCluster),
          displayName: this.k8sUtil.getDisplayName(fedCluster),
          clusters: fedCluster.spec.clusters.map(cluster => ({
            ...cluster,
            isNormal: getClusterStatus(
              clusters.find(cl => cl.metadata.name === cluster.name),
            ),
          })),
          check: !disabledCheck,
          disabledCheck,
        };
      });
      this.clusterList = clusters.map(cluster => {
        let hostName = '';
        const found = fedClusters.find(fed =>
          fed.spec.clusters.some(
            (clu: ClusterFedItem) => clu.name === cluster.metadata.name,
          ),
        );
        if (found) {
          const host = found.spec.clusters.find(
            (member: ClusterFedItem) => member.type === 'Host',
          );
          hostName = host.name;
        }
        const limit = limits.items
          ? limits.items.find(
              _limit => _limit.metadata.name === cluster.metadata.name,
            )
          : null;
        const quota = {
          quota: {},
          _quota: {},
          limit: limit ? limit.status.allocatable : {},
          capacity: limit ? limit.status.capacity : null,
          used: limit ? limit.status.used : null,
          fedHostName: hostName,
        };
        this.quotaMap[cluster.metadata.name] = quota;
        return {
          name: cluster.metadata.name,
          displayName: cluster.spec.displayName,
          isNormal: getClusterStatus(cluster) && !!limit,
          ...quota,
        };
      });
    });
  }

  stepQuota() {
    this.basicForm.onSubmit(null);
    if (this.basicForm.valid) {
      this.curStep = this.stepsEnum.QUOTA;
    }
  }

  isClusterDisabled(cluster: Cluster) {
    const selectClusterFedClusters: string[] = [];
    this.selectClusters.forEach((cluster: FedCluster) => {
      if (cluster.clusters) {
        cluster.clusters.forEach(_cluster => {
          selectClusterFedClusters.push(_cluster.name);
        });
      }
    });
    return selectClusterFedClusters.includes(cluster.name) || !cluster.isNormal;
  }

  isClusterFedDisabled(fedCluster: FedCluster) {
    return fedCluster.clusters.some(cluster => !cluster?.isNormal);
  }

  selectClustersChange(value: Array<FedCluster | Cluster>) {
    const selectClusterFedClusters: string[] = [];
    const selectCluster = value.filter(cluster => cluster.name);
    value
      .filter((cluster: FedCluster) => cluster.clusters)
      .forEach((cluster: FedCluster) => {
        cluster.clusters.forEach(_cluster => {
          if (selectCluster.find(sc => sc.name === _cluster.name)) {
            selectClusterFedClusters.push(_cluster.name);
          }
        });
      });
    if (selectClusterFedClusters.length > 0) {
      selectClusterFedClusters.forEach(name => {
        remove(value, cluster => cluster.name === name);
      });
    }
    this.selectClusters = value;
  }

  create() {
    if (this.basicForm) {
      this.basicForm.onSubmit(null);
      if (!this.basicForm.valid) {
        return;
      }
    }
    if (this.quotaForm && this.quotaForm.quotaForm) {
      this.quotaForm.quotaForm.onSubmit(null);
      if (!this.quotaForm.quotaForm.valid) {
        return;
      }
    }
    Object.assign(this.data, this.quotaForm.getQuotaFormData());
    this.submitting = true;
    const resource = this.toCreateYaml(this.data);
    this.k8sApi
      .postGlobalResource({
        type: RESOURCE_TYPES.PROJECT,
        resource,
      })
      .subscribe(
        res => {
          this.message.success({
            content: this.translate.get('project_create_successed'),
          });
          this.router.navigate([
            '/manage-project/',
            'project',
            { project: res.metadata.name },
            'detail',
          ]);
        },
        () => {
          this.submitting = false;
          this.cdr.markForCheck();
        },
      );
  }

  private toCreateYaml(postBody: ProjectFormData): Project {
    return {
      apiVersion: getYamlApiVersion(RESOURCE_TYPES.PROJECT),
      kind: 'Project',
      metadata: {
        annotations: {
          [this.k8sUtil.normalizeType(DISPLAY_NAME)]: postBody.displayName,
          [this.k8sUtil.normalizeType(DESCRIPTION)]: postBody.description,
          [this.k8sUtil.normalizeType(
            UNITE_QUOTA_FED_CLUSTERS,
          )]: postBody.uniteQuotaFedClusters,
        },
        labels: {
          [this.k8sUtil.normalizeType('project.level')]: '1',
          [this.k8sUtil.normalizeType('project.parent')]: '',
        },
        name: postBody.name,
        namespace: '',
      },
      spec: {
        clusters: postBody.clusters as ProjectCluster[],
      },
    };
  }
}
