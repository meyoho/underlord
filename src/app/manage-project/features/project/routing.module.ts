import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProjectDetailComponent } from './detail/detail.component';
import { UpdateQuotaComponent } from './update-quota/update-quota.component';

const routes: Routes = [
  {
    path: '',
    component: ProjectDetailComponent,
  },
  {
    path: 'detail',
    component: ProjectDetailComponent,
  },
  {
    path: 'update-quota',
    component: UpdateQuotaComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProjectRoutingModule {}
