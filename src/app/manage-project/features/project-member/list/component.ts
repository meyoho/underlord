import { publishRef } from '@alauda/common-snippet';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  templateUrl: 'template.html',
})
export class ProjectMemberListComponent {
  project$ = this.activatedRoute.parent.parent.paramMap.pipe(
    map(paramMap => paramMap.get('project')),
    publishRef(),
  );

  constructor(private readonly activatedRoute: ActivatedRoute) {}
}
