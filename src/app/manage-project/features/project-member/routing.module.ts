import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProjectMemberListComponent } from 'app/manage-project/features/project-member/list/component';

const routes: Routes = [
  {
    path: '',
    component: ProjectMemberListComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProjectMemberRoutingModule {}
