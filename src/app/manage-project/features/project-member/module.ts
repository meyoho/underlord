import { NgModule } from '@angular/core';

import { ProjectMemberListComponent } from 'app/manage-project/features/project-member/list/component';
import { ProjectMemberRoutingModule } from 'app/manage-project/features/project-member/routing.module';
import { FeaturesModule } from 'app/shared/features/module';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  imports: [SharedModule, FeaturesModule, ProjectMemberRoutingModule],
  declarations: [ProjectMemberListComponent],
})
export class ProjectMemberModule {}
