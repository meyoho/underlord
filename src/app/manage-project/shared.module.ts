import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { ProjectQuotaBaseFormComponent } from './project-quota-base-form/component';
import { QuotaBaseFormComponent } from './quota-form/quota-base-from.component';
import { QuotaFormComponent } from './quota-form/quota-form.component';
import { QuotaHorizontalComponent } from './quota/quota-horizontal/component';
import { QuotaVerticalComponent } from './quota/quota-vertical/component';
import { RatioLineComponent } from './ratio-line/component';

export const SHARED_COMPONENTS = [
  QuotaBaseFormComponent,
  QuotaFormComponent,
  ProjectQuotaBaseFormComponent,
  QuotaHorizontalComponent,
  QuotaVerticalComponent,
  RatioLineComponent,
];

@NgModule({
  imports: [SharedModule],
  declarations: SHARED_COMPONENTS,
  exports: SHARED_COMPONENTS,
})
export class ManageProjectSharedModule {}
