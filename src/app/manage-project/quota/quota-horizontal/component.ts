import { ObservableInput, StringMap } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  CUSTOM_RESOURCE_DEFINITIONS,
  CustomResourceData,
} from 'app/manage-project/quota-form/quota-base-from.component';
import { getNormalizedQuotaValue } from 'app/manage-project/utils';
import { ResourceQuota } from 'app/typings';
import { K8S_QUOTA_TYPES } from 'app/utils';

@Component({
  selector: 'alu-quota-horizontal',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
})
export class QuotaHorizontalComponent {
  @ObservableInput()
  @Input('quota')
  quota$!: Observable<ResourceQuota>;

  @ObservableInput(false, [])
  @Input('customResourceLimits')
  customResourceLimits$!: Observable<CustomResourceData[]>;

  @Input()
  capacity: StringMap = {};

  @Input()
  usedKey = 'status.used';

  @Input()
  hardKey = 'status.hard';

  columns = ['resourceType', 'allocated', 'totalQuota', 'distributionRatio'];

  quotaItems$ = combineLatest([this.quota$, this.customResourceLimits$]).pipe(
    map(([quota, customResourceLimits]) => {
      // @ts-ignore
      return K8S_QUOTA_TYPES.filter(
        type =>
          type.static ||
          Object.prototype.hasOwnProperty.call(this.capacity || {}, type.key),
      )
        .concat(
          (customResourceLimits || []).map(
            ({ key, descriptionEn, descriptionZh }) => ({
              key: `requests.${key}`,
              name: CUSTOM_RESOURCE_DEFINITIONS[key],
              unit: 'unit_ge',
              description: {
                en: descriptionEn,
                zh: descriptionZh,
              },
            }),
          ),
        )
        .map(quotaType => {
          const used = getNormalizedQuotaValue(quota, quotaType, this.usedKey);
          const total = getNormalizedQuotaValue(quota, quotaType, this.hardKey);
          return {
            ...quotaType,
            used,
            total,
            ratio: total ? (+total ? +used / +total : NaN) : -1,
          };
        });
    }),
  );
}
