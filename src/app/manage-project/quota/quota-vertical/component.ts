import { ObservableInput } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { getNormalizedQuotaValue } from 'app/manage-project/utils';
import { K8sBaseQuota, K8sQuotaType, ResourceQuota } from 'app/typings';
import { K8S_QUOTA_TYPES } from 'app/utils';

@Component({
  selector: 'alu-quota-vertical',
  templateUrl: 'template.html',
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuotaVerticalComponent {
  @ObservableInput()
  @Input('quota')
  quota$: Observable<ResourceQuota>;

  @Input()
  usedKey = 'status.used';

  @Input()
  hardKey = 'status.hard';

  quotaItems$ = this.quota$.pipe(map(quota => [quota]));

  K8S_QUOTA_TYPES = K8S_QUOTA_TYPES;

  columns = K8S_QUOTA_TYPES.map(({ name }) => name);

  constructor() {
    this.calc = this.calc.bind(this);
  }

  calc(quota: K8sBaseQuota, quotaType: K8sQuotaType) {
    const used = getNormalizedQuotaValue(quota, quotaType, this.usedKey);
    const total = getNormalizedQuotaValue(quota, quotaType, this.hardKey);
    return {
      used,
      total,
      ratio: total ? (+total ? +used / +total : NaN) : -1,
    };
  }
}
