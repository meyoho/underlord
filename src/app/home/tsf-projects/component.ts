import {
  KubernetesResource,
  KubernetesResourceList,
  StringMap,
  publishRef,
} from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';

import { ProjectApiService } from 'app/api/project/api';

import { Project } from '../../typings';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TSFProjectsComponent {
  private projects: KubernetesResource[] = [];
  productEntrypoint = decodeURIComponent(
    this.route.snapshot.queryParams.tsfUrl || '',
  );

  keyword: string;
  next: string;
  loading$ = new Subject();
  update$ = new BehaviorSubject<StringMap>({ limit: '20' });

  projects$ = this.update$.pipe(
    tap(() => {
      this.loading$.next(true);
    }),
    switchMap(params => {
      return this.projectApi.getProjects(params).pipe(
        map((res: KubernetesResourceList<Project>) => {
          if (this.next) {
            this.projects = this.projects.concat(res.items);
          } else {
            this.projects = res.items;
          }
          this.next = (res.metadata && res.metadata.continue) || '';
          return this.projects;
        }),
      );
    }),
    tap(() => {
      this.loading$.next(false);
    }),
    publishRef(),
  );

  constructor(
    private readonly projectApi: ProjectApiService,
    private readonly route: ActivatedRoute,
  ) {}

  search(keyword: string) {
    this.keyword = keyword;
    const params: { [key: string]: string } = {
      limit: '20',
    };
    if (this.keyword) {
      params.filterBy = `name,${this.keyword}`;
    }
    this.update$.next(params);
  }

  tsfRoute(uid: string) {
    const token = localStorage.getItem('id_token');
    if (!this.productEntrypoint) {
      return '';
    }
    return (
      (this.productEntrypoint.startsWith('http://') ||
      this.productEntrypoint.startsWith('https://')
        ? this.productEntrypoint
        : `${window.location.origin}${this.productEntrypoint}`) +
      `${
        this.productEntrypoint.includes('?') ? '&' : '?'
      }id_token=${token}&project_id=${uid}`
    );
  }

  loadMore() {
    const params: { [key: string]: string } = {
      limit: '20',
    };
    if (this.keyword) {
      params.filterBy = `name,${this.keyword}`;
    }
    if (this.next) {
      params.continue = this.next;
    }
    this.update$.next(params);
  }
}
