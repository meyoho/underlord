import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FeatureGateComponent } from './feature-gate/page/component';
import { HomeComponent } from './home.component';
import { PersonalInfoComponent } from './personal-info/component';
import { ProjectCreateComponent } from './project-create/component';
import { ProjectsComponent } from './project-list/component';
import { TSFProjectsComponent } from './tsf-projects/component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      { path: '', redirectTo: 'project', pathMatch: 'full' },
      {
        path: 'project',
        children: [
          { path: '', redirectTo: 'list', pathMatch: 'full' },
          {
            path: 'list',
            component: ProjectsComponent,
          },
          {
            path: 'create',
            component: ProjectCreateComponent,
          },
        ],
      },
      { path: 'personal-info', component: PersonalInfoComponent },
      { path: 'feature-gate', component: FeatureGateComponent },
      { path: 'tsf-projects', component: TSFProjectsComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
