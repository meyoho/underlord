import { NavItemConfig } from '@alauda/ui';
import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { NavigationStart } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { filter, map, takeUntil } from 'rxjs/operators';

import { BaseLayoutComponent } from 'app/base-layout-component';
@Component({
  templateUrl: 'home.component.html',
})
export class HomeComponent extends BaseLayoutComponent
  implements OnInit, OnDestroy {
  personalInfoAllowed = false;
  navConfig: NavItemConfig[] = [
    {
      label: 'project_management',
      key: 'project_management',
      href: '/home/project/list',
    },
    {
      label: 'project_management',
      key: 'project_management',
      href: '/home/project/create',
    },
    {
      label: 'my_profile',
      key: '/home/personal-info',
      href: '/home/personal-info',
    },
    {
      label: 'feature_gate',
      key: '/home/feature-gate',
      href: '/home/feature-gate',
    },
    {
      label: 'project_management',
      key: '/home/tsf-projects',
      href: '/home/tsf-projects',
    },
  ];

  logoTitle$: Observable<string>;
  onDestroy$ = new Subject<void>();
  constructor(protected injector: Injector) {
    super(injector);
    this.initRouterObserver();
  }

  ngOnInit() {
    super.ngOnInit();
    this.personalInfoAllowed = window.location.pathname.includes(
      'personal-info',
    );
    this.logoTitle$ = this.activatedKey$.pipe(
      map(key => {
        const nav = this.navConfig.find(_ => _.key === key);
        return nav.label;
      }),
    );
  }

  private initRouterObserver() {
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationStart),
        takeUntil(this.onDestroy$),
      )
      .subscribe(({ url }: NavigationStart) => {
        this.personalInfoAllowed = url.includes('personal-info');
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }
}
