import { K8sApiService } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { finalize } from 'rxjs/operators';

import { FeatureGate } from 'app/api/feature-gate/api';
import { RESOURCE_TYPES } from 'app/utils';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectGlobalFgComponent {
  cluster: string;
  selectableFg: FeatureGate[];
  selectedFg: FeatureGate;

  loading = false;

  @ViewChild('form', { static: true })
  form: NgForm;

  constructor(
    @Inject(DIALOG_DATA) data: { cluster: string; selectableFg: FeatureGate[] },
    private readonly dialogRef: DialogRef,
    private readonly k8sApi: K8sApiService,
    private readonly cdr: ChangeDetectorRef,
  ) {
    this.cluster = data.cluster;
    this.selectableFg = data.selectableFg;
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.loading = true;
    const resource = {
      apiVersion: this.selectedFg.apiVersion,
      kind: 'ClusterAlaudaFeatureGate',
      metadata: { name: this.selectedFg.metadata.name },
      spec: this.selectedFg.spec,
    };
    this.k8sApi
      .postResource({
        type: RESOURCE_TYPES.CLUSTER_ALAUDA_FEATURE_GATE,
        cluster: this.cluster,
        resource,
      })
      .pipe(
        finalize(() => {
          this.loading = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(() => this.dialogRef.close(this.cluster));
  }
}
