import {
  FeatureGateService,
  K8sApiService,
  K8sUtilService,
  TranslateService,
  catchPromise,
  publishRef,
  skipError,
} from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import {
  Observable,
  ReplaySubject,
  Subject,
  combineLatest,
  merge,
  of,
} from 'rxjs';
import {
  catchError,
  map,
  mergeMap,
  pluck,
  scan,
  shareReplay,
  startWith,
  switchMap,
  take,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { FeatureGate, FeatureGateApi } from 'app/api/feature-gate/api';
import { RESOURCE_TYPES } from 'app/utils';

import { SelectGlobalFgComponent } from '../select-global-fg/component';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeatureGateComponent implements OnInit, OnDestroy {
  private readonly reloadAllClusterFg$$ = new Subject<void>();
  private readonly updateClusterFg$$ = new Subject<string>();
  private readonly updateGlobalFg$$ = new Subject<void>();
  private readonly destroy$$ = new Subject<void>();

  clusters$ = this.k8sApi
    .getGlobalResourceList({
      type: RESOURCE_TYPES.CLUSTER_REGISTRY,
    })
    .pipe(pluck('items'), publishRef());

  globalFg$ = this.updateGlobalFg$$.pipe(
    startWith(null as void),
    switchMap(() =>
      this.fgApi.getGlobalGateList().pipe(skipError({ items: [] })),
    ),
    map((list: { items: FeatureGate[] }) => this.transformFgStatus(list.items)),
    publishRef(),
  );

  clusterFg$ = this.reloadAllClusterFg$$.pipe(
    startWith(null as void),
    switchMap(() => this.buildClusterFg()),
    takeUntil(this.destroy$$),
    shareReplay(1),
  );

  allClustersSym = Symbol('all clusters');
  platformSym = Symbol('platform setting');

  selectedCluster: symbol | string = this.allClustersSym;

  alphaFg: FeatureGate;
  betaFg: FeatureGate;

  constructor(
    private readonly fgApi: FeatureGateApi,
    private readonly k8sApi: K8sApiService,
    private readonly k8sUtil: K8sUtilService,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly cdr: ChangeDetectorRef,
    private readonly featureGateService: FeatureGateService,
  ) {}

  ngOnInit() {
    this.fgApi.getGlobalGate('alpha').subscribe(res => {
      this.alphaFg = res;
      this.cdr.markForCheck();
    });
    this.fgApi.getGlobalGate('beta').subscribe(res => {
      this.betaFg = res;
      this.cdr.markForCheck();
    });
  }

  ngOnDestroy() {
    this.destroy$$.next();
    this.destroy$$.complete();
    this.featureGateService.refetchCache();
  }

  selectCluster(cluster: symbol | string) {
    this.selectedCluster = cluster;
  }

  displayTable(cluster: symbol | string) {
    return (
      this.selectedCluster === this.allClustersSym ||
      this.selectedCluster === cluster
    );
  }

  refreshCluster(cluster: symbol | string) {
    if (cluster === this.platformSym) {
      this.updateGlobalFg$$.next();
    } else {
      this.updateClusterFg$$.next(cluster as string);
    }
  }

  switchStage(stage: string) {
    const resource = stage === 'Alpha' ? this.alphaFg : this.betaFg;
    const { enabled } = resource.spec;

    catchPromise(
      this.dialog.confirm({
        title: this.translate.get(
          enabled ? 'forbid_stage_confirm' : 'enable_stage_confirm',
          { stage },
        ),
        content: this.translate.get(
          enabled
            ? 'forbid_stage_confirm_content'
            : 'enable_stage_confirm_content',
          { stage },
        ),
        confirmText: this.translate.get(enabled ? 'forbid' : 'enable'),
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) => {
          this.k8sApi
            .patchGlobalResource({
              type: RESOURCE_TYPES.ALAUDA_FEATURE_GATE,
              resource,
              part: { spec: { enabled: !enabled } },
            })
            .subscribe(resolve, reject);
        },
      }),
    ).subscribe((res: FeatureGate) => {
      if (stage === 'Alpha') {
        this.alphaFg = res;
      } else {
        this.betaFg = res;
      }
      this.updateGlobalFg$$.next();
      this.reloadAllClusterFg$$.next();
      this.cdr.markForCheck();
    });
  }

  addFgToCluster(cluster: string) {
    combineLatest([
      this.globalFg$,
      this.clusterFg$.pipe(
        map(fg => fg[cluster].map(item => item.metadata.name)),
      ),
    ])
      .pipe(
        take(1),
        switchMap(([globalFg, clusterFg]) =>
          this.dialog
            .open(SelectGlobalFgComponent, {
              data: {
                cluster,
                selectableFg: globalFg.filter(
                  item => !clusterFg.includes(item.metadata.name),
                ),
              },
            })
            .afterClosed(),
        ),
      )
      .subscribe(result => {
        if (result) {
          this.updateClusterFg$$.next(cluster);
        }
      });
  }

  private buildClusterFg(): Observable<Record<string, FeatureGate[]>> {
    const loadClusterFg$$ = new ReplaySubject<string>();

    return this.clusters$.pipe(
      map(clusters => clusters.map(cluster => cluster.metadata.name)),
      switchMap(clusters => {
        for (let i = 0; i < 5 && clusters.length; ++i) {
          loadClusterFg$$.next(clusters.shift());
        }
        return merge(loadClusterFg$$, this.updateClusterFg$$).pipe(
          mergeMap(cluster =>
            this.fgApi.getClusterGateList(cluster).pipe(
              map(list => ({
                [cluster]: this.transformFgStatus(
                  list.items.filter(
                    item =>
                      this.k8sUtil.getLabel(
                        item,
                        'override',
                        'feature-gate',
                      ) === 'true',
                  ),
                ),
              })),
              tap(() => {
                if (clusters.length > 0) {
                  loadClusterFg$$.next(clusters.shift());
                }
              }),
              catchError(() => {
                if (clusters.length > 0) {
                  loadClusterFg$$.next(clusters.shift());
                }
                return of({ [cluster]: [] });
              }),
            ),
          ),
        );
      }),
      scan<Record<string, FeatureGate[]>>(
        (acc, curr) => ({
          ...acc,
          ...curr,
        }),
        {},
      ),
      startWith({}),
    );
  }

  private transformFgStatus(items: FeatureGate[]) {
    return items
      .filter(
        item => item.metadata.name !== 'alpha' && item.metadata.name !== 'beta',
      )
      .sort((a, b) => a.metadata.name.localeCompare(b.metadata.name));
  }
}
