import { K8sApiService } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

import { FeatureGate } from 'app/api/feature-gate/api';
import { RESOURCE_TYPES } from 'app/utils';

@Component({
  selector: 'alu-fg-table',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeatureGateTableComponent {
  @Input()
  isGlobal = false;

  @Input()
  clusterDisplayName: string;

  @Input()
  clusterName: string;

  @Input()
  items: FeatureGate[];

  @Input()
  alphaForbidden = false;

  @Input()
  betaForbidden = false;

  @Output()
  updated = new EventEmitter<void>();

  @Output()
  addFg = new EventEmitter<string>();

  switching: Record<string, boolean> = {};

  constructor(
    private readonly k8sApi: K8sApiService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  isRowDisabled(item: FeatureGate) {
    switch (item.spec.stage) {
      case 'Alpha':
        return this.alphaForbidden;
      case 'Beta':
        return this.betaForbidden;
      default:
        return false;
    }
  }

  switch(resource: FeatureGate) {
    this.switching[resource.metadata.name] = true;
    const part = { spec: { enabled: !resource.spec.enabled } };
    (this.isGlobal
      ? this.k8sApi.patchGlobalResource({
          type: RESOURCE_TYPES.ALAUDA_FEATURE_GATE,
          resource,
          part,
        })
      : this.k8sApi.patchResource({
          type: RESOURCE_TYPES.CLUSTER_ALAUDA_FEATURE_GATE,
          cluster: this.clusterName,
          resource,
          part,
        })
    ).subscribe(
      () => {
        this.switching[resource.metadata.name] = false;
        this.updated.emit();
        this.cdr.markForCheck();
      },
      () => {
        this.switching[resource.metadata.name] = false;
        this.cdr.markForCheck();
      },
    );
  }

  delete(resource: FeatureGate) {
    this.k8sApi
      .deleteResource({
        type: RESOURCE_TYPES.CLUSTER_ALAUDA_FEATURE_GATE,
        cluster: this.clusterName,
        resource,
      })
      .subscribe(() => {
        this.updated.emit();
      });
  }
}
