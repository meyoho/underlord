import { PageModule, PlatformNavModule } from '@alauda/ui';
import { PortalModule } from '@angular/cdk/portal';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ProjectModule } from 'app/manage-project/features/project/module';
import { FeaturesModule } from 'app/shared/features/module';
import { SharedModule } from 'app/shared/shared.module';

import { FeatureGateComponent } from './feature-gate/page/component';
import { SelectGlobalFgComponent } from './feature-gate/select-global-fg/component';
import { FeatureGateTableComponent } from './feature-gate/table/component';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { PersonalInfoComponent } from './personal-info/component';
import { UpdatePasswordComponent } from './personal-info/update-password/component';
import { ProjectCreateComponent } from './project-create/component';
import { ProjectsComponent } from './project-list/component';
import { TSFProjectsComponent } from './tsf-projects/component';

@NgModule({
  imports: [
    PageModule,
    PlatformNavModule,
    PortalModule,
    CommonModule,
    FormsModule,
    HomeRoutingModule,
    SharedModule,
    ProjectModule,
    FeaturesModule,
  ],
  declarations: [
    HomeComponent,
    ProjectsComponent,
    ProjectCreateComponent,
    PersonalInfoComponent,
    UpdatePasswordComponent,
    FeatureGateComponent,
    SelectGlobalFgComponent,
    FeatureGateTableComponent,
    TSFProjectsComponent,
  ],
  entryComponents: [UpdatePasswordComponent, SelectGlobalFgComponent],
})
export class HomeModule {}
