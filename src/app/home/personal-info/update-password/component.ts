import { K8sUtilService, TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, MessageService } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  Injector,
  Output,
} from '@angular/core';
import { FormControl, NgForm, Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { AdvanceApi } from 'app/api/advance/api';
import { User } from 'app/typings';
import { PASSWORD_PATTERN } from 'app/utils';

@Component({
  templateUrl: 'template.html',
})
export class UpdatePasswordComponent extends BaseResourceFormGroupComponent {
  submitting: boolean;
  passwordPattern = PASSWORD_PATTERN;
  keys = Object.keys;
  readonly = false;

  constructor(
    public injector: Injector,
    @Inject(DIALOG_DATA) readonly data: { user: User },
    private readonly k8sUtil: K8sUtilService,
    private readonly advanceApi: AdvanceApi,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
  ) {
    super(injector);
  }

  @Output()
  close = new EventEmitter<boolean>();

  cancel() {
    this.close.next(false);
  }

  getDefaultFormModel() {
    return {};
  }

  passwordConfirming = (c: FormControl) => {
    if (c.get('comfirmPassword').value !== c.get('password').value) {
      return { not_match: true };
    }
    return null;
  };

  createForm() {
    return this.fb.group(
      {
        oldPassword: this.fb.control('', Validators.required),
        password: this.fb.control('', [
          Validators.required,
          Validators.pattern(this.passwordPattern.pattern),
        ]),
        comfirmPassword: this.fb.control('', Validators.required),
      },
      { validators: this.passwordConfirming },
    );
  }

  update(form: NgForm) {
    form.onSubmit(null);
    if (form.invalid) {
      return;
    }
    const part: User = {
      spec: {
        old_password: btoa(this.form.get('oldPassword').value),
        password: btoa(this.form.get('password').value),
      },
    };
    this.advanceApi
      .patchUser(this.k8sUtil.getName(this.data.user), part)
      .subscribe(
        () => {
          this.message.success(this.translate.get('update_password_successed'));
          this.close.next(true);
        },
        () => this.close.next(false),
      );
  }
}
