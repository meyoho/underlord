import {
  AsyncDataLoader,
  AuthorizationStateService,
  K8SResourceList,
  K8sApiService,
  K8sUtilService,
  NAMESPACE,
  PROJECT,
  ResourceListParams,
} from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import { Component } from '@angular/core';
import { filter, first, map, tap } from 'rxjs/operators';
import { Md5 } from 'ts-md5';

import { AdvanceApi } from 'app/api/advance/api';
import {
  PREFIXES,
  ROLE_LEVEL,
  USER_EMAIL,
} from 'app/services/k8s-util.service';
import { UpdateDisplayNameDialogComponent } from 'app/shared/features/update-displayname/component';
import { AccountInfo, User, UserBinding } from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';

import { UpdatePasswordComponent } from './update-password/component';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class PersonalInfoComponent {
  columns = ['role_name', 'role_type', 'role_scope'];
  tokenCopied = false;
  refreshing = false;
  email: string;
  accountEmail$ = this.auth.getTokenPayload<AccountInfo>().pipe(
    filter(res => !!res.email),
    tap(res => (this.email = res.email)),
    map(res => Md5.hashStr(res.email).toString().toLowerCase()),
  );

  host = location.origin;

  accountToken$ = this.auth.getToken();

  params$ = this.accountEmail$.pipe(
    map(email => ({
      labelSelector: `${this.k8sUtil.normalizeType(
        USER_EMAIL,
        PREFIXES.AUTH,
      )}=${email}`,
    })),
  );

  list = new K8SResourceList({
    fetchParams$: this.params$,
    fetcher: this.fetchResources.bind(this),
  });

  dataLoader = new AsyncDataLoader<User, string>({
    params$: this.accountEmail$,
    fetcher: (username: string) => {
      return this.advanceApi.getUserInfo(username);
    },
  });

  constructor(
    private readonly k8sApiService: K8sApiService,
    private readonly dialogService: DialogService,
    private readonly auth: AuthorizationStateService,
    private readonly advanceApi: AdvanceApi,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  fetchResources(params: ResourceListParams) {
    return this.k8sApiService.getGlobalResourceList({
      type: RESOURCE_TYPES.USER_BINDING,
      queryParams: params,
    });
  }

  copyText(input: HTMLInputElement) {
    input.select();
    document.execCommand('Copy');
    this.tokenCopied = true;
  }

  refreshToken() {
    this.refreshing = true;
    this.auth.refreshToken().subscribe(
      () => {
        this.tokenCopied = false;
        this.refreshing = false;
      },
      () => {
        this.refreshing = false;
      },
    );
  }

  getRoleScopeName(item: UserBinding) {
    if (item.metadata.labels) {
      switch (
        item.metadata.labels[
          this.k8sUtil.normalizeType(ROLE_LEVEL, PREFIXES.AUTH)
        ]
      ) {
        case NAMESPACE:
          return item.metadata.labels[this.k8sUtil.normalizeType(NAMESPACE)];
        case PROJECT:
          return item.metadata.labels[this.k8sUtil.normalizeType(PROJECT)];
      }
    }
    return '';
  }

  updatePassword(user: User) {
    const dialogRef = this.dialogService.open(UpdatePasswordComponent, {
      data: {
        user,
      },
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((_res: boolean) => {
        dialogRef.close();
      });
  }

  updateDisplayName(user: User) {
    const ref = this.dialogService.open(UpdateDisplayNameDialogComponent, {
      data: {
        user,
      },
    });
    ref.componentInstance.close.subscribe((res: boolean) => {
      ref.close();
      if (res) {
        this.dataLoader.reload();
      }
    });
  }
}
