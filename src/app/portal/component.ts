/* eslint-disable no-fallthrough */
import {
  FeatureGateService,
  K8sApiService,
  K8sUtilService,
  Reason,
  TOKEN_GLOBAL_NAMESPACE,
  publishRef,
} from '@alauda/common-snippet';
import { NavItemConfig } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Injector,
  OnInit,
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { chunk, get } from 'lodash-es';
import { Observable, combineLatest, interval, merge, of } from 'rxjs';
import { catchError, map, switchMap, takeUntil } from 'rxjs/operators';

import {
  ProductCenterListApi,
  handleStatus,
} from 'app/api/product-center/list-api';
import { Metric } from 'app/api/product-center/list-types';
import { BaseLayoutComponent } from 'app/base-layout-component';
import { ProductCenter } from 'app/typings';
import { RESOURCE_DEFINITIONS } from 'app/utils';
interface MetricTranslate {
  [key: string]: { zh: string; en: string };
}
const RowNumber = 5;
const Interval = 60 * 1000 * 5;
@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductPortalComponent extends BaseLayoutComponent
  implements OnInit {
  logoTitle = 'Container Platform';

  logoTitle$: Observable<string>;
  reason = Reason;
  metricsAbnormal = false;
  navConfig: NavItemConfig[] = [
    {
      label: 'portal',
      key: 'portal',
      href: '/portal',
    },
  ];

  productTypes = [
    'compute',
    'application',
    'middleware',
    'storage',
    'public_service',
  ];

  products$ = this.productApi.getProducts().pipe(
    map(res =>
      (res.items || []).filter(
        item => this.checkCr(item) && handleStatus(item).type === 'Running',
      ),
    ),
    publishRef(),
  );

  metrics$ = merge(of(null), interval(Interval)).pipe(
    takeUntil(this.onDestroy$),
    switchMap(() =>
      this.productApi.getProductMetric().pipe(
        catchError(() => {
          this.metricsAbnormal = true;
          this.cdr.markForCheck();
          return of({});
        }),
      ),
    ),
    map(res => {
      const result = get(res, 'data.result', []);
      if (result.length > 0) {
        this.metricsAbnormal = false;
        this.cdr.markForCheck();
      }
      return result;
    }),
    publishRef(),
  );

  metricsTranslate$ = this.k8sApi
    .getResource({
      definition: RESOURCE_DEFINITIONS.CONFIG_MAP,
      cluster: 'global',
      namespace: this.globalNamespace,
      name: 'metrics-translate',
    })
    .pipe(
      map(item => {
        const data = get(item, 'data', {});
        return Object.keys(data).reduce((acc, key) => {
          return {
            ...acc,
            [key]: JSON.parse(data[key]),
          };
        }, {});
      }),
      catchError(() => of({})),
    );

  featGuardAllowed$ = this.fg.isEnabled('ace3');

  productCards$ = combineLatest([
    this.products$,
    this.metrics$,
    this.metricsTranslate$,
    this.translate.locale$,
  ]).pipe(
    map(([products, metrics, metricsTranslate]) =>
      this.getProductCards(products, metrics, metricsTranslate),
    ),
    publishRef(),
  );

  constructor(
    protected injector: Injector,
    private readonly productApi: ProductCenterListApi,
    private readonly k8sUtil: K8sUtilService,
    private readonly sanitizer: DomSanitizer,
    private readonly fg: FeatureGateService,
    private readonly k8sApi: K8sApiService,
    @Inject(TOKEN_GLOBAL_NAMESPACE) private readonly globalNamespace: string,
    private readonly cdr: ChangeDetectorRef,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.logoTitle$ = this.activatedKey$.pipe(
      map(() => this.translate.get('portal_title')),
    );
  }

  checkCr(product: ProductCenter) {
    return product.crs?.length === 1;
  }

  getProductCards(
    products: ProductCenter[],
    metrics: Metric[],
    metricsTranslate: MetricTranslate,
  ) {
    const res = (products || []).reduce((accum, product) => {
      const productMetrics = (metrics || []).filter(
        item => item.metric.product === get(product, 'crd.spec.names.plural'),
      );
      return productMetrics.length > 0
        ? [
            ...accum,
            {
              ...product,
              metrics: this.handleMetrics(productMetrics, metricsTranslate),
            },
          ]
        : accum;
    }, []);
    return chunk(res, RowNumber);
  }

  getLogo = (product: ProductCenter) => {
    const url = this.k8sUtil.getAnnotation(product.crd, 'product-logo');
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  };

  getVersion(product: ProductCenter) {
    return product.crs ? get(product.crs[0], 'status.version') : '';
  }

  getProductType = (product: ProductCenter) => {
    return (
      this.k8sUtil.getLabel(product.crd, 'product-type') || ''
    ).toLowerCase();
  };

  getProductHref = (href = '') => {
    const token = localStorage.getItem('id_token');
    return href.startsWith('http://') || href.startsWith('https://')
      ? href
      : `${window.location.origin}${href}${
          href.includes('?') ? '&' : '?'
        }id_token=${token}`;
  };

  getProductEntryPoint(product: ProductCenter) {
    return product.crs ? get(product.crs[0], 'status.productEntrypoint') : '';
  }

  filterType = (products: ProductCenter[], type: string) => {
    return (products || []).filter(product => {
      return (
        (
          this.k8sUtil.getLabel(product.crd, 'product-type', '') || ''
        ).toLowerCase() === type
      );
    });
  };

  private handleMetrics(metrics: Metric[], metricsTranslate: MetricTranslate) {
    const messages: string[] = [];
    for (let i = 0; i <= 2; i++) {
      if (metrics[i]) {
        messages[i] = this.getMetricMessage(metrics[i], metricsTranslate);
      }
    }
    return messages;
  }

  private getMetricMessage(data: Metric, metricsTranslate: MetricTranslate) {
    const value = data.value[1];
    const metricName = data.metric.metricname;
    return this.translate.get(
      metricsTranslate[metricName],
      {
        value: value,
      },
      true,
    );
  }
}
