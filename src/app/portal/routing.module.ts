import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProductPortalComponent } from './component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'products',
    pathMatch: 'full',
  },
  {
    path: 'products',
    component: ProductPortalComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductPortalRoutingModule {}
