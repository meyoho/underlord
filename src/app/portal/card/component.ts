import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'alu-product-card',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductCardComponent {
  @Input()
  logo = 'products/empty-tencent.svg';

  @Input()
  name = '';

  @Input()
  type = '';

  @Input()
  version = '';

  @Input()
  href = '';

  @Input()
  metricData: string[] = [];
}
