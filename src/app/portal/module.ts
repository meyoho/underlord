import { PageModule, PlatformNavModule } from '@alauda/ui';
import { PortalModule } from '@angular/cdk/portal';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';

import { ProductCardComponent } from './card/component';
import { ProductPortalComponent } from './component';
import { ProductPortalRoutingModule } from './routing.module';

@NgModule({
  imports: [
    PageModule,
    PlatformNavModule,
    SharedModule,
    PortalModule,

    ProductPortalRoutingModule,
  ],
  declarations: [ProductPortalComponent, ProductCardComponent],
  exports: [RouterModule],
})
export class ProductPortalModule {}
