import { ValueHook } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  DoCheck,
  ElementRef,
  Input,
  Renderer2,
} from '@angular/core';

@Component({
  selector: 'alu-user-groups-display',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserGroupDisplayComponent implements DoCheck {
  @ValueHook(Boolean)
  @Input()
  groups: string[] = [];

  @Input()
  parentPadding: number;

  showToggleLabel: boolean;

  UNGROUPED = 'ungrouped';

  constructor(
    private readonly renderer: Renderer2,
    private readonly el: ElementRef,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  ngDoCheck(): void {
    const hostWidth = this.el.nativeElement.offsetWidth;
    const parentContentWidth = this.renderer.parentNode(this.el.nativeElement)
      .clientWidth;

    const pcw = this.parentPadding
      ? parentContentWidth - +this.parentPadding
      : parentContentWidth;

    this.showToggleLabel = hostWidth >= pcw;
    this.cdr.markForCheck();
  }
}
