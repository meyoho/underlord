import { K8sUtilService, TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, MessageService } from '@alauda/ui';
import { Component, EventEmitter, Inject, Output } from '@angular/core';

import { AdvanceApi } from 'app/api/advance/api';
import { User } from 'app/typings';

@Component({
  templateUrl: 'template.html',
})
export class UpdateDisplayNameDialogComponent {
  submitting: boolean;
  displayName: string;

  @Output()
  close = new EventEmitter<boolean>();

  constructor(
    @Inject(DIALOG_DATA) readonly data: { user: User },
    private readonly k8sUtil: K8sUtilService,
    private readonly advanceApi: AdvanceApi,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
  ) {
    this.displayName = this.k8sUtil.getDisplayName(data.user);
  }

  cancel() {
    this.close.next(false);
  }

  update() {
    const part: User = {
      spec: {
        username: this.displayName,
      },
    };
    this.advanceApi
      .patchUser(this.k8sUtil.getName(this.data.user), part)
      .subscribe(
        () => {
          this.message.success(this.translate.get('update_successed'));
          this.close.next(true);
        },
        () => this.close.next(false),
      );
  }
}
