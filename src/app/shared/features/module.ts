import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { NotificationReceiverFormItemComponent } from './notification/receiver-form-item/component';
import { NotificationReceiverFormComponent } from './notification/receiver-form/component';
import { AddMemberDialogComponent } from './scope-member-list/add-member/component';
import { ScopeMemberListComponent } from './scope-member-list/component';
import { ImportMemberDialogComponent } from './scope-member-list/import-member/component';
import { UpdateDisplayNameDialogComponent } from './update-displayname/component';
import { UserGroupDisplayComponent } from './user-groups-display/component';
import { UserRoleAggListComponent } from './user-role-agg-list/component';

@NgModule({
  imports: [CommonModule, SharedModule],
  declarations: [
    ScopeMemberListComponent,
    ImportMemberDialogComponent,
    AddMemberDialogComponent,
    UserRoleAggListComponent,
    UserGroupDisplayComponent,
    UpdateDisplayNameDialogComponent,
    NotificationReceiverFormComponent,
    NotificationReceiverFormItemComponent,
  ],
  entryComponents: [
    ImportMemberDialogComponent,
    AddMemberDialogComponent,
    UpdateDisplayNameDialogComponent,
    NotificationReceiverFormComponent,
  ],
  exports: [
    ScopeMemberListComponent,
    UserRoleAggListComponent,
    UserGroupDisplayComponent,
    NotificationReceiverFormComponent,
    NotificationReceiverFormItemComponent,
  ],
})
export class FeaturesModule {}
