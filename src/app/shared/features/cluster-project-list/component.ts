import {
  K8sApiService,
  K8sUtilService,
  KubernetesResource,
  TranslateService,
  matchExpressionsToString,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { from, of } from 'rxjs';
import { map, mergeMap, toArray } from 'rxjs/operators';

import { Project } from 'app/typings';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectListDialogComponent {
  inputValue = '';
  columns = ['project_name', 'display_name', 'admin'];
  projects$ = of(this.modalData.projectItems).pipe(
    mergeMap(items =>
      from(items).pipe(
        mergeMap(item =>
          this.getUserBindingData(item).pipe(
            map(detail => ({ ...item, userbinding: detail })),
          ),
        ),
        toArray(),
      ),
    ),
  );

  constructor(
    @Inject(DIALOG_DATA)
    public readonly modalData: {
      projectItems: Project[];
      name: string;
      unionDisplayName: string;
    },
    private readonly dialogRef: DialogRef,
    private readonly translateService: TranslateService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  get disabled() {
    return this.inputValue !== this.modalData.name;
  }

  get inputTitle() {
    return this.translateService.get('input_name_force_delete', {
      name: this.modalData.name,
    });
  }

  get displayName() {
    return this.translateService.get('cluster_delete_with_project_tip', {
      name: this.modalData.unionDisplayName,
    });
  }

  getUserBindingData(project: Project) {
    return this.k8sApi
      .getGlobalResourceList<KubernetesResource>({
        type: RESOURCE_TYPES.USER_BINDING,
        queryParams: {
          labelSelector: matchExpressionsToString([
            {
              key: this.k8sUtil.normalizeType('project'),
              operator: 'in',
              values: [project.metadata.name],
            },
            {
              key: this.k8sUtil.normalizeType('role.name', 'auth'),
              operator: '=',
              values: ['acp-project-admin'],
            },
          ]),
        },
      })
      .pipe(
        map(userbinding =>
          userbinding.items
            .map(item => this.k8sUtil.getAnnotation(item, 'user.email', 'auth'))
            .join(','),
        ),
      );
  }

  delete() {
    this.dialogRef.close(true);
  }

  close() {
    this.dialogRef.close(false);
  }
}
