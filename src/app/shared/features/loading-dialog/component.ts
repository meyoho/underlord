import { StringMap, TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogService } from '@alauda/ui';
import { Component, EventEmitter, Inject, Output } from '@angular/core';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class LoadingDialogComponent {
  @Output()
  close = new EventEmitter<boolean>();

  iconColors: StringMap = {
    info: '#999',
    primary: '#006eff',
  };

  constructor(
    @Inject(DIALOG_DATA)
    public data: {
      title: string;
      iconColor: string;
      closeConfirmContent: string;
    },
    private readonly dialogService: DialogService,
    private readonly translateService: TranslateService,
  ) {}

  async closeConfirm() {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('loading_close_confirm_title'),
        content: this.data.closeConfirmContent,
        confirmText: this.translateService.get('close'),
        cancelText: this.translateService.get('cancel'),
      });
      this.close.next(true);
    } catch (e) {
      this.close.next(false);
    }
  }
}
