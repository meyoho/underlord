import {
  DISPLAY_NAME,
  K8sApiService,
  Locale,
  NAMESPACE,
  PROJECT,
  StringMap,
  TRUE,
  TranslateService,
} from '@alauda/common-snippet';
import { DIALOG_DATA } from '@alauda/ui';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { pluck } from 'rxjs/operators';

import { AdvanceApi } from 'app/api/advance/api';
import {
  K8sUtilService,
  PREFIXES,
  ROLE_TEMPLATE_LEVEL,
  ROLE_TEMPLATE_OFFICIAL,
} from 'app/services/k8s-util.service';
import { RoleTemplate } from 'app/typings';
import { RESOURCE_TYPES, genUserbinding } from 'app/utils';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class AddMemberDialogComponent implements OnInit {
  submitting: boolean;
  username: string;
  role: RoleTemplate;
  scope: string;
  roleList$: Observable<RoleTemplate[]>;

  @Output()
  close = new EventEmitter<boolean>();

  constructor(
    private readonly k8sApi: K8sApiService,
    public k8sUtil: K8sUtilService,
    private readonly advanceApi: AdvanceApi,
    @Inject(DIALOG_DATA)
    public data: StringMap,
    private readonly translate: TranslateService,
  ) {
    this.scope = this.data[this.k8sUtil.normalizeType(NAMESPACE)]
      ? NAMESPACE
      : PROJECT;
  }

  ngOnInit() {
    this.roleList$ = this.getRoleList();
  }

  cancel() {
    this.close.next(false);
  }

  getRoleList() {
    const params = {
      labelSelector: `${this.k8sUtil.normalizeType(
        ROLE_TEMPLATE_LEVEL,
        PREFIXES.AUTH,
      )}=${this.scope}`,
    };
    return this.k8sApi
      .getGlobalResourceList<RoleTemplate>({
        type: RESOURCE_TYPES.ROLE_TEMPLATE,
        queryParams: params,
      })
      .pipe(pluck('items'));
  }

  displayName = (role: RoleTemplate) => {
    let displayName = '';
    const name = this.k8sUtil.getName(role);
    if (
      this.translate.locale === Locale.EN &&
      this.k8sUtil.getLabel(role, ROLE_TEMPLATE_OFFICIAL, PREFIXES.AUTH) ===
        TRUE
    ) {
      displayName = this.k8sUtil.getAnnotation(role, `${DISPLAY_NAME}.en`);
    } else {
      displayName = this.k8sUtil.getDisplayName(role);
    }
    return displayName ? `${name} (${displayName})` : name;
  };

  addMember(formRef: NgForm) {
    formRef.onSubmit(null);
    if (formRef.invalid) {
      return;
    }
    this.addRoleBinding().subscribe(() => {
      this.close.next(true);
    });
  }

  addRoleBinding() {
    const userbinding = genUserbinding.call(
      this,
      this.username,
      this.role,
      this.data,
      this.translate.locale,
    );
    return this.advanceApi.createUserbinding(userbinding);
  }
}
