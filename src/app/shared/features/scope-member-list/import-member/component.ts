import {
  DISPLAY_NAME,
  K8SResourceList,
  K8sApiService,
  Locale,
  NAMESPACE,
  PROJECT,
  StringMap,
  TRUE,
  TranslateService,
} from '@alauda/common-snippet';
import { DIALOG_DATA, MessageService } from '@alauda/ui';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable, Subject, combineLatest } from 'rxjs';
import { map, pluck, startWith } from 'rxjs/operators';

import { AdvanceApi } from 'app/api/advance/api';
import {
  K8sUtilService,
  PREFIXES,
  ROLE_TEMPLATE_LEVEL,
  ROLE_TEMPLATE_OFFICIAL,
} from 'app/services/k8s-util.service';
import { Group, RoleTemplate, User } from 'app/typings';
import { COMMA_ENTITY, RESOURCE_TYPES, genUserbinding } from 'app/utils';

interface UserForm extends User {
  check?: boolean;
}

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class ImportMemberDialogComponent implements OnInit {
  submitting: boolean;
  columns = ['checkbox', 'username', 'user_group'];
  groupList$: Observable<Group[]>;
  roleList$: Observable<RoleTemplate[]>;
  allGroup = {
    apiVersion: 'auth.alauda.io/v1',
    kind: 'Group',
    metadata: {
      annotations: {
        [this.k8sUtil.normalizeType(DISPLAY_NAME)]: this.translate.get('all'),
      },
      name: '',
    },
  };

  notin = '';
  searchAction$$ = new Subject<string>();
  searchAction$ = this.searchAction$$.pipe(startWith(''));
  group$$ = new Subject<Group>();
  group$ = this.group$$.pipe(startWith(this.allGroup));
  userParams$: Observable<StringMap> = combineLatest([
    this.searchAction$,
    this.group$,
  ]).pipe(
    map(([keyword, group]) => {
      let filterBy = this.notin;
      if (group && group !== this.allGroup) {
        filterBy = `${filterBy},group,${this.getGroupName(group)}`;
      }
      if (keyword) {
        filterBy = `${filterBy},email,${keyword.replace(',', COMMA_ENTITY)}`;
      }
      return { filterBy };
    }),
  );

  list = new K8SResourceList<User>({
    fetchParams$: this.userParams$,
    fetcher: this.fetchResources.bind(this),
  });

  role: RoleTemplate;
  scope: string;

  searchType: string;

  @Output()
  close = new EventEmitter<boolean>();

  constructor(
    private readonly message: MessageService,
    private readonly k8sApi: K8sApiService,
    public k8sUtil: K8sUtilService,
    private readonly translate: TranslateService,
    private readonly advanceApi: AdvanceApi,
    @Inject(DIALOG_DATA)
    public data: StringMap,
  ) {
    this.notin = this.data.notin;
    delete this.data.notin;
    this.scope = this.data[this.k8sUtil.normalizeType(NAMESPACE)]
      ? NAMESPACE
      : PROJECT;
  }

  ngOnInit() {
    this.groupList$ = this.advanceApi.getGroups().pipe(
      map(res => {
        res.items.unshift(this.allGroup);
        return res.items;
      }),
    );

    this.roleList$ = this.getRoleList();
  }

  fetchResources(params: StringMap) {
    return this.advanceApi.filterUsers(params);
  }

  displayName = (role: RoleTemplate) => {
    let displayName = '';
    const name = this.k8sUtil.getName(role);
    if (
      this.translate.locale === Locale.EN &&
      this.k8sUtil.getLabel(role, ROLE_TEMPLATE_OFFICIAL, PREFIXES.AUTH) ===
        TRUE
    ) {
      displayName = this.k8sUtil.getAnnotation(role, `${DISPLAY_NAME}.en`);
    } else {
      displayName = this.k8sUtil.getDisplayName(role);
    }
    return displayName ? `${name} (${displayName})` : name;
  };

  getRoleList() {
    const params = {
      labelSelector: `${[
        this.k8sUtil.normalizeType(ROLE_TEMPLATE_LEVEL, PREFIXES.AUTH),
      ]}=${this.scope}`,
    };
    return this.k8sApi
      .getGlobalResourceList({
        type: RESOURCE_TYPES.ROLE_TEMPLATE,
        queryParams: params,
      })
      .pipe(pluck('items'));
  }

  getGroupName(group: Group) {
    return group.metadata.annotations &&
      group.metadata.annotations[this.k8sUtil.normalizeType(DISPLAY_NAME)]
      ? group.metadata.annotations[this.k8sUtil.normalizeType(DISPLAY_NAME)]
      : group.metadata.name;
  }

  getCheckList(userList: UserForm[]) {
    return userList?.length > 0 ? userList.filter(item => item.check) : [];
  }

  cancel() {
    this.close.next(false);
  }

  addRoleBinding(user: User) {
    const userbinding = genUserbinding.call(
      this,
      user.spec.email,
      this.role,
      this.data,
      this.translate.locale,
    );
    return this.advanceApi.createUserbinding(userbinding);
  }

  import(userList: UserForm[], form: NgForm) {
    form.onSubmit(null);
    if (!this.role) {
      return;
    }
    const list = this.getCheckList(userList);
    if (list.length === 0) {
      this.message.warning(this.translate.get('please_select_member'));
      return;
    }
    const promiseList: any = [];
    list.forEach(item => {
      promiseList.push(this.addRoleBinding(item).toPromise());
    });
    Promise.race(promiseList)
      .then(() => this.close.next(true))
      .catch(() => this.close.next(false));
  }
}
