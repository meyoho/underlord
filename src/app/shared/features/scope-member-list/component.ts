import {
  ANNOTATIONS,
  CLUSTER,
  K8SResourceList,
  K8sPermissionService,
  K8sResourceAction,
  K8sUtilService,
  METADATA,
  NAMESPACE,
  ObservableInput,
  PROJECT,
  StringMap,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService, DialogSize, MessageService } from '@alauda/ui';
import { Component, Input } from '@angular/core';
import { get } from 'lodash-es';
import { Observable, Subject, combineLatest } from 'rxjs';
import { first, map, pluck, startWith, switchMap } from 'rxjs/operators';

import { AdvanceApi } from 'app/api/advance/api';
import { IdpApi } from 'app/api/idp/api';
import { PREFIXES } from 'app/services/k8s-util.service';
import { User } from 'app/typings';
import { COMMA_ENTITY, RESOURCE_TYPES } from 'app/utils';

import { AddMemberDialogComponent } from './add-member/component';
import { ImportMemberDialogComponent } from './import-member/component';

@Component({
  selector: 'alu-scope-member-list',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class ScopeMemberListComponent {
  @Input()
  namespace: string;

  @ObservableInput(true)
  namespace$: Observable<string>;

  @Input()
  cluster: string;

  @ObservableInput(true)
  cluster$: Observable<string>;

  @Input()
  project: string;

  @ObservableInput(true)
  project$: Observable<string>;

  @Input()
  isFederated: boolean;

  searchKeyMap: StringMap = {
    username: 'email',
    display_name: 'username',
    user_group: 'group',
  };

  searchType = 'username';
  notin = '';

  searchTypeList = ['username', 'display_name', 'user_group'];
  keywords$$ = new Subject<string>();
  keywords$ = this.keywords$$.pipe(startWith(null as string), publishRef());
  showAddMember$ = this.idpApi.getIdps({}).pipe(
    pluck('items'),
    map(idps => {
      return !!idps.find(idp => idp.type === 'oidc');
    }),
  );

  userParams$: Observable<StringMap> = combineLatest([
    this.project$,
    this.keywords$,
  ]).pipe(
    map(([project, keywords]) => {
      const searchParams: StringMap = {};
      if (project) {
        searchParams.filterBy = `role_level,project,project,${project}`;
      }
      if (this.namespace) {
        searchParams.filterBy = `role_level,namespace,namespace,${this.namespace},cluster,${this.cluster}`;
      }
      this.notin = `${searchParams.filterBy},notin,true`;
      if (keywords) {
        searchParams.filterBy = `${searchParams.filterBy},${
          this.searchKeyMap[this.searchType] || 'email'
        },${keywords.replace(',', COMMA_ENTITY)}`;
      }
      return searchParams;
    }),
    publishRef(),
  );

  list = new K8SResourceList<User>({
    fetchParams$: this.userParams$,
    fetcher: this.fetchResources.bind(this),
  });

  permissions$ = combineLatest([
    this.project$,
    this.cluster$,
    this.namespace$,
  ]).pipe(
    switchMap(([project, cluster, namespace]) =>
      this.k8sPermissionService.isAllowed({
        advanced: true,
        project,
        cluster,
        namespace,
        type: RESOURCE_TYPES.USER_BINDING,
        action: [K8sResourceAction.CREATE, K8sResourceAction.DELETE],
      }),
    ),
    startWith({}),
    publishRef(),
  );

  columns = ['username', 'user_group', 'user_role', 'add_time', 'action'];

  constructor(
    private readonly dialogService: DialogService,
    private readonly advanceApi: AdvanceApi,
    private readonly translateService: TranslateService,
    private readonly k8sPermissionService: K8sPermissionService,
    private readonly message: MessageService,
    private readonly k8sUtil: K8sUtilService,
    private readonly idpApi: IdpApi,
  ) {}

  fetchResources(params: StringMap) {
    return this.advanceApi.filterUsers(params);
  }

  addMember() {
    const data = {
      [this.k8sUtil.normalizeType(CLUSTER)]: this.cluster ? this.cluster : '',
      [this.k8sUtil.normalizeType(NAMESPACE)]: this.namespace
        ? this.namespace
        : '',
      [this.k8sUtil.normalizeType(PROJECT)]: this.project ? this.project : '',
    };
    if (this.isFederated) {
      data[this.k8sUtil.normalizeType('federated', PREFIXES.AUTH)] = 'true';
    }
    const dialogRef = this.dialogService.open(AddMemberDialogComponent, {
      data,
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((res: boolean) => {
        dialogRef.close();
        if (res) {
          this.list.reload();
        }
      });
  }

  importMember() {
    const data = {
      [this.k8sUtil.normalizeType(CLUSTER)]: this.cluster ? this.cluster : '',
      [this.k8sUtil.normalizeType(NAMESPACE)]: this.namespace
        ? this.namespace
        : '',
      [this.k8sUtil.normalizeType(PROJECT)]: this.project ? this.project : '',
      notin: this.notin,
    };
    if (this.isFederated) {
      data[this.k8sUtil.normalizeType('federated', PREFIXES.AUTH)] = 'true';
    }
    const dialogRef = this.dialogService.open(ImportMemberDialogComponent, {
      size: DialogSize.Big,
      data,
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((res: boolean) => {
        dialogRef.close();
        if (res) {
          this.message.success(this.translateService.get('import_success'));
          this.list.reload();
        }
      });
  }

  async removeMember(user: User) {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('confirm_to_remove_member', {
          username: user.spec.email,
        }),
        confirmText: this.translateService.get('remove'),
        cancelText: this.translateService.get('cancel'),
      });
      const annotations = user.metadata.annotations;
      if (annotations && annotations['userbinding/name']) {
        this.advanceApi
          .deleteUserbinding(annotations['userbinding/name'])
          .subscribe(() => {
            this.message.success(this.translateService.get('remove_success'));
            this.list.reload();
          });
      }
    } catch (e) {}
  }

  getPlaceHolder(type: string) {
    switch (type) {
      case 'username':
        return 'search_by_username_placeholder';
      case 'display_name':
        return 'search_by_display_name_placeholder';
      case 'user_group':
        return 'search_by_group_placeholder';
    }
  }

  getAnnotation(resource: User, label: string) {
    return get(resource, [METADATA, ANNOTATIONS, label]);
  }
}
