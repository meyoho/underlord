import { Component, Input } from '@angular/core';
import Highcharts from 'highcharts';
import { cloneDeep } from 'lodash-es';

@Component({
  selector: 'alu-metric-chart',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class MetricChartComponent {
  @Input()
  metricOptions: Highcharts.Options;

  @Input()
  chartTitle: string;

  chartOptions: Highcharts.Options;
  Highcharts = Highcharts;

  @Input()
  set series(series: Highcharts.SeriesOptionsType[]) {
    if (!this.chartOptions) {
      this.chartOptions = cloneDeep(this.metricOptions);
    }
    if (series && series.length > 0) {
      this.chartOptions.series = series;
    }
  }
}
