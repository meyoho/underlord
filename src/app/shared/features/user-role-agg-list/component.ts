import {
  CLUSTER,
  K8SResourceList,
  K8sApiService,
  K8sUtilService,
  KubernetesResource,
  NAMESPACE,
  PROJECT,
  TranslateService,
  catchPromise,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable, forkJoin } from 'rxjs';
import { concatMap, finalize, map } from 'rxjs/operators';

import { AdvanceApi } from 'app/api/advance/api';
import { PREFIXES, ROLE_LEVEL, ROLE_NAME } from 'app/services/k8s-util.service';
import { UserBinding } from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';

import { LoadingDialogComponent } from '../loading-dialog/component';

interface DisplayItem {
  name: string;
  display_name: string;
}

interface UserBindingAggProject {
  userbindingName: string;
  name: string;
  displayName?: string;
}
interface UserBindingAggNamespace extends UserBindingAggProject {
  clusterName?: string;
}
interface UserBindingAgg extends KubernetesResource {
  roleName: string;
  roleLevel: string;
  userbindingName?: string;
  sliceIndex?: number;
  aggs?: Array<UserBindingAggProject | UserBindingAggNamespace>;
}

@Component({
  selector: 'alu-user-role-agg-list',
  styleUrls: ['style.scss'],
  templateUrl: 'template.html',
})
export class UserRoleAggListComponent implements OnInit {
  @Input()
  list: K8SResourceList;

  @Input()
  canDelete = true;

  @Input()
  showDelete = true;

  @Output()
  reload = new EventEmitter();

  columns = ['role_name', 'role_type', 'project_or_namespace', 'action'];
  clusters: DisplayItem[] = [];
  projects: DisplayItem[] = [];
  userBindingAggs$: Observable<UserBindingAgg[]>;

  constructor(
    private readonly k8sApi: K8sApiService,
    private readonly k8sUtil: K8sUtilService,
    private readonly advanceApi: AdvanceApi,
    private readonly dialogService: DialogService,
    private readonly translate: TranslateService,
  ) {}

  ngOnInit() {
    this.userBindingAggs$ = this.list.items$.pipe(
      map((items: UserBinding[]) => this.genUserBindingAggs(items)),
    );
    forkJoin([
      this.k8sApi.getGlobalResourceList({
        type: RESOURCE_TYPES.CLUSTER_REGISTRY,
        namespaced: true,
      }),
      this.k8sApi.getGlobalResourceList({
        type: RESOURCE_TYPES.PROJECT,
      }),
    ])
      .pipe(
        map(([clusterList, projectList]) => [
          clusterList.items.map(item => ({
            name: item.metadata.name,
            display_name: this.k8sUtil.getDisplayName(item),
          })),
          projectList.items.map(item => ({
            name: item.metadata.name,
            display_name: this.k8sUtil.getDisplayName(item),
          })),
        ]),
        publishRef(),
      )
      .subscribe(([clusters, projects]) => {
        this.clusters = clusters;
        this.projects = projects;
      });
  }

  deleteUserbinding(
    userBindingAgg: UserBindingAgg,
    agg: UserBindingAggProject,
  ) {
    catchPromise(
      this.dialogService.confirm({
        title: this.translate.get('remove_confirm_title'),
        content: this.translate.get('remove_user_role_content', {
          roleName: userBindingAgg.roleName,
          name: agg.name,
          level: this.translate.get(userBindingAgg.roleLevel),
        }),
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
      }),
    )
      .pipe(
        concatMap(_ => this.advanceApi.deleteUserbinding(agg.userbindingName)),
      )
      .subscribe(_ => {
        this.reload.next();
      });
  }

  deleteUserbindings(userBindingAgg: UserBindingAgg) {
    const content = {
      roleName: userBindingAgg.roleName,
    };
    catchPromise(
      this.dialogService.confirm({
        title: this.translate.get('delete_user_role_title', content),
        content:
          userBindingAgg.roleLevel === 'platform'
            ? ''
            : this.translate.get('delete_user_role_content', {
                level: userBindingAgg.roleLevel,
              }),
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
      }),
    )
      .pipe(
        concatMap(_ => {
          if (userBindingAgg.userbindingName) {
            return this.advanceApi.deleteUserbinding(
              userBindingAgg.userbindingName,
            );
          } else {
            const ref = this.dialogService.open(LoadingDialogComponent, {
              size: DialogSize.Small,
              data: {
                title: this.translate.get('deleting_please_wait'),
                closeConfirmContent: this.translate.get(
                  'delete_loading_close_content',
                ),
              },
            });
            ref.componentInstance.close.subscribe((res: boolean) => {
              if (res) {
                ref.close();
              }
            });
            return forkJoin(
              userBindingAgg.aggs.map(agg => {
                return this.advanceApi.deleteUserbinding(agg.userbindingName);
              }),
            ).pipe(finalize(() => ref.close()));
          }
        }),
      )
      .subscribe(_ => {
        this.reload.next();
      });
  }

  genUserBindingAggs = (userbindings: UserBinding[]) => {
    const userBindingAggs: UserBindingAgg[] = [];
    userbindings.forEach(item => {
      const roleLevel = this.k8sUtil.getLabel(item, ROLE_LEVEL, PREFIXES.AUTH);
      const roleName = this.k8sUtil.getLabel(item, ROLE_NAME, PREFIXES.AUTH);
      const find = userBindingAggs.find(uba => uba.roleName === roleName);
      if (find) {
        switch (roleLevel) {
          case PROJECT:
            find.aggs.push({
              userbindingName: this.k8sUtil.getName(item),
              name: this.k8sUtil.getLabel(item, PROJECT),
            });
            break;
          case NAMESPACE:
            find.aggs.push({
              userbindingName: this.k8sUtil.getName(item),
              name: this.k8sUtil.getLabel(item, NAMESPACE),
              clusterName: this.k8sUtil.getLabel(item, CLUSTER),
            });
            break;
        }
      } else {
        const userBindingAgg: UserBindingAgg = {
          roleName,
          roleLevel,
          sliceIndex: 5,
        };
        switch (roleLevel) {
          case 'platform':
            userBindingAgg.userbindingName = this.k8sUtil.getName(item);
            break;
          case PROJECT:
            userBindingAgg.aggs = [
              {
                userbindingName: this.k8sUtil.getName(item),
                name: this.k8sUtil.getLabel(item, PROJECT),
              },
            ];
            break;
          case NAMESPACE:
            userBindingAgg.aggs = [
              {
                userbindingName: this.k8sUtil.getName(item),
                name: this.k8sUtil.getLabel(item, NAMESPACE),
                clusterName: this.k8sUtil.getLabel(item, CLUSTER),
              },
            ];
            break;
        }
        userBindingAggs.push(userBindingAgg);
      }
    });
    return userBindingAggs;
  };

  getDisplayName = (name: string, items: DisplayItem[]) => {
    const item = items.find(it => it.name === name);
    if (item && item.display_name) {
      return `${name} (${item.display_name})`;
    }
    return name;
  };
}
