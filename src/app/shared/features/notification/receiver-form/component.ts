import {
  DISPLAY_NAME,
  K8sApiService,
  TOKEN_GLOBAL_NAMESPACE,
  TranslateService,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  Injector,
  OnInit,
  Output,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { EMPTY, from } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';

import {
  Method,
  generateName,
  types,
} from 'app/maintenance-center/features/notification/util';
import { K8sUtilService } from 'app/services/k8s-util.service';
import { NotificationReceiver } from 'app/typings';
import {
  EMAIL_PATTERN,
  HTTP_ADDRESS_PATTERN,
  NotificationReceiverMeta,
  PHONE_PATTERN,
  RESOURCE_TYPES,
  ResourceType,
  TYPE,
} from 'app/utils';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class NotificationReceiverFormComponent
  extends BaseResourceFormArrayComponent<NotificationReceiver>
  implements OnInit {
  submitting: boolean;
  update: boolean;
  displayName = this.k8sUtil.normalizeType(DISPLAY_NAME);
  type = this.k8sUtil.normalizeType(TYPE);
  types = types;
  @Output()
  close: EventEmitter<NotificationReceiver> = new EventEmitter();

  constructor(
    @Inject(DIALOG_DATA)
    public dialogData: {
      method: string;
      receiver: NotificationReceiver;
    },
    @Inject(TOKEN_GLOBAL_NAMESPACE) private readonly globalNamespace: string,
    injector: Injector,
    private readonly translate: TranslateService,
    private readonly k8sUtil: K8sUtilService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly messageService: MessageService,
    private readonly dialogRef: DialogRef,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.update = !!this.dialogData.receiver;
    if (this.update) {
      this.add(0);
      this.form.patchValue([this.dialogData.receiver]);
    }
  }

  getOnFormArrayResizeFn() {
    let type = 'email';
    if (this.dialogData.method) {
      type = this.dialogData.method;
    }
    return () =>
      this.fb.control({
        apiVersion: NotificationReceiverMeta.apiVersion,
        kind: NotificationReceiverMeta.kind,
        metadata: {
          labels: {
            [this.type]: type,
          },
          annotations: {
            [this.displayName]: '',
          },
          name: '',
          namespace: this.globalNamespace,
          resourceVersion: '',
        },
        spec: {
          destination: '',
        },
      });
  }

  cancel() {
    this.dialogRef.close();
  }

  onSubmit(form: NgForm) {
    form.onSubmit(null);
    if (this.submitting || this.validForm(this.form.value)) {
      return;
    }
    this.submitting = true;
    from(this.form.value)
      .pipe(
        mergeMap((resource: NotificationReceiver) => {
          if (!this.update) {
            resource.metadata.name = generateName(resource.spec.destination);
          }
          return this.k8sApi[
            this.update ? 'putGlobalResource' : 'postGlobalResource'
          ]<NotificationReceiver>({
            type: RESOURCE_TYPES.NOTIFICATION_RECEIVER,
            namespaced: true,
            resource,
          });
        }),
        catchError(() => {
          this.submitting = false;
          return EMPTY;
        }),
      )
      .subscribe(resource => {
        this.messageService.success(
          this.translate.get(this.update ? 'update_success' : 'add_success'),
        );
        this.close.next(resource);
      });
  }

  validForm(receivers: NotificationReceiver[]) {
    return (
      !receivers ||
      receivers.length === 0 ||
      receivers.some(receiver => {
        if (!receiver.metadata.annotations[this.displayName]) {
          return true;
        }
        switch (receiver.metadata.labels[this.type]) {
          case Method.EMAIL:
            return !EMAIL_PATTERN.pattern.exec(receiver.spec.destination);
          case Method.SMS:
            return !PHONE_PATTERN.pattern.exec(receiver.spec.destination);
          case Method.DINGTALK:
          case Method.WEBHOOK:
          case Method.WECHAT:
            return !HTTP_ADDRESS_PATTERN.pattern.exec(
              receiver.spec.destination,
            );
        }
      })
    );
  }
}
