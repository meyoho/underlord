import { DISPLAY_NAME } from '@alauda/common-snippet';
import { AfterViewInit, Component, Injector, Input } from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { pairwise, startWith } from 'rxjs/operators';

import {
  Method,
  types,
} from 'app/maintenance-center/features/notification/util';
import { K8sUtilService } from 'app/services/k8s-util.service';
import {
  NotificationReceiver,
  NotificationReceiverFormModel,
} from 'app/typings';
import {
  EMAIL_PATTERN,
  HTTP_ADDRESS_PATTERN,
  PHONE_PATTERN,
  TYPE,
} from 'app/utils';

@Component({
  selector: 'alu-notification-receiver-form-item',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class NotificationReceiverFormItemComponent
  extends BaseResourceFormGroupComponent<
    NotificationReceiver,
    NotificationReceiverFormModel
  >
  implements AfterViewInit {
  @Input()
  method: string;

  types = types;
  Method = Method;
  EMAIL_PATTERN = EMAIL_PATTERN;
  PHONE_PATTERN = PHONE_PATTERN;
  HTTP_ADDRESS_PATTERN = HTTP_ADDRESS_PATTERN;
  displayName = this.k8sUtil.normalizeType(DISPLAY_NAME);
  type = this.k8sUtil.normalizeType(TYPE);

  constructor(injector: Injector, private readonly k8sUtil: K8sUtilService) {
    super(injector);
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
    this.form
      .get('method')
      .valueChanges.pipe(startWith(null as string), pairwise())
      .subscribe(([prev, next]: [string, string]) => {
        if (prev) {
          this.form.get(prev).disable();
        }
        this.form.get(next).enable();
        this.form.patchValue({
          email: '',
          sms: '',
          dingtalk: '',
          wechat: '',
          webhook: '',
        });
      });
    setTimeout(
      () =>
        this.form.get('method').value &&
        this.form.get(this.form.get('method').value).enable(),
    );
  }

  createForm() {
    const httpAddressValidator = Validators.pattern(
      HTTP_ADDRESS_PATTERN.pattern,
    );
    return this.fb.group({
      method: ['', Validators.required],
      displayName: ['', Validators.required],
      email: [
        { value: '', disabled: true },
        [Validators.required, Validators.pattern(EMAIL_PATTERN.pattern)],
      ],
      sms: [
        { value: '', disabled: true },
        [Validators.required, Validators.pattern(PHONE_PATTERN.pattern)],
      ],
      dingtalk: [
        { value: '', disabled: true },
        [Validators.required, httpAddressValidator],
      ],
      wechat: [
        { value: '', disabled: true },
        [Validators.required, httpAddressValidator],
      ],
      webhook: [
        { value: '', disabled: true },
        [Validators.required, httpAddressValidator],
      ],
    });
  }

  adaptFormModel(formModel: NotificationReceiverFormModel) {
    if (formModel) {
      let destination = '';
      switch (formModel.method) {
        case Method.EMAIL:
          destination = formModel.email;
          break;
        case Method.SMS:
          destination = formModel.sms;
          break;
        case Method.DINGTALK:
          destination = formModel.dingtalk;
          break;
        case Method.WECHAT:
          destination = formModel.wechat;
          break;
        case Method.WEBHOOK:
          destination = formModel.webhook;
          break;
      }
      return {
        apiVersion: formModel.apiVersion,
        kind: formModel.kind,
        metadata: {
          labels: {
            [this.type]: formModel.method,
          },
          annotations: {
            [this.displayName]: formModel.displayName,
          },
          name: formModel.name,
          namespace: formModel.namespace,
          resourceVersion: formModel.resourceVersion,
        },
        spec: {
          destination,
        },
      };
    }
  }

  adaptResourceModel(resource: NotificationReceiver) {
    if (resource) {
      const method = resource.metadata.labels[this.type] || Method.EMAIL;
      const destination = resource.spec.destination;
      let email: string;
      let sms: string;
      let dingtalk: string;
      let wechat: string;
      let webhook: string;
      switch (method) {
        case Method.EMAIL:
          email = destination;
          break;
        case Method.SMS:
          sms = destination;
          break;
        case Method.DINGTALK:
          dingtalk = destination;
          break;
        case Method.WECHAT:
          wechat = destination;
          break;
        case Method.WEBHOOK:
          webhook = destination;
          break;
      }
      return {
        method,
        displayName: resource.metadata.annotations[this.displayName],
        email,
        sms,
        dingtalk,
        wechat,
        webhook,
        apiVersion: resource.apiVersion,
        kind: resource.kind,
        name: resource.metadata.name,
        namespace: resource.metadata.namespace,
        resourceVersion: resource.metadata.resourceVersion,
      };
    }
  }

  getDefaultFormModel() {
    return {} as NotificationReceiverFormModel;
  }
}
