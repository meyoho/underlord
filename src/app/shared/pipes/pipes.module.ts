import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FormatUtcStrPipe } from './format-utc-str.pipe';
import { GetPipe } from './get.pipe';
import { IsTerminatingPipe } from './is-terminating.pipe';
import { K8sStatusPipe } from './k8s-util.pipe';
import { ParseJsonPipe } from './parse-json.pipe';
import { PurePipe } from './pure.pipe';
import { TrustHtmlPipe } from './trust-html.pipe';
import { TrustUrlPipe } from './trust-url.pipe';
const PIPES = [
  PurePipe,
  GetPipe,
  FormatUtcStrPipe,
  TrustUrlPipe,
  TrustHtmlPipe,
  // k8s util
  K8sStatusPipe,
  IsTerminatingPipe,
  ParseJsonPipe,
];

@NgModule({
  imports: [CommonModule],
  declarations: PIPES,
  exports: PIPES,
})
export class PipesModule {}
