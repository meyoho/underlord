import { Arrayable } from '@alauda/common-snippet';
import { Pipe, PipeTransform } from '@angular/core';
import { get } from 'lodash-es';

@Pipe({
  name: 'aluGet',
})
export class GetPipe implements PipeTransform {
  transform(
    value: unknown,
    properties: Arrayable<string | number>,
    defaultValue?: unknown,
  ) {
    return get(value, properties, defaultValue);
  }
}
