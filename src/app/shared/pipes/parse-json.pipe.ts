import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'aluParseJson',
})
export class ParseJsonPipe implements PipeTransform {
  transform(value: string) {
    try {
      return JSON.parse(value);
    } catch (error) {
      return value;
    }
  }
}
