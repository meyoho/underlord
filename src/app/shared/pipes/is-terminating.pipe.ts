import { KubernetesResource } from '@alauda/common-snippet';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'aluIsDeleting' })
export class IsTerminatingPipe implements PipeTransform {
  transform(resource: KubernetesResource) {
    return !!resource?.metadata?.deletionTimestamp;
  }
}
