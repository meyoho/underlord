import { FIELD_NOT_AVAILABLE_PLACEHOLDER } from '@alauda/common-snippet';
import { Pipe, PipeTransform } from '@angular/core';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
dayjs.extend(utc);
/**
 * Format a UTC string.
 */
@Pipe({ name: 'aluFormatUtcStr' })
export class FormatUtcStrPipe implements PipeTransform {
  transform(utcStr: string): string {
    if (!utcStr) {
      return FIELD_NOT_AVAILABLE_PLACEHOLDER;
    }
    return dayjs.utc(utcStr).local().format('YYYY-MM-DD HH:mm:ss');
  }
}
