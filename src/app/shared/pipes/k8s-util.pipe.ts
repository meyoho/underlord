import { KubernetesResource } from '@alauda/common-snippet';
import { Pipe, PipeTransform } from '@angular/core';

import { K8sUtilService, StatusItem } from 'app/services/k8s-util.service';

@Pipe({
  name: 'aluStatus',
})
export class K8sStatusPipe implements PipeTransform {
  constructor(private readonly k8sUtil: K8sUtilService) {}

  transform(
    value: KubernetesResource,
    withSpec = true,
    property?: keyof StatusItem,
  ) {
    return this.k8sUtil.getStatus(value, withSpec, property);
  }
}
