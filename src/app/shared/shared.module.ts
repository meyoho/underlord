import { CodeEditorModule } from '@alauda/code-editor';
import {
  AccountMenuModule,
  AsyncDataModule,
  CommonLayoutModule,
  DisabledContainerModule,
  HelpMenuModule,
  K8SResourceListModule,
  NotificationUtilModule,
  PageGuardModule,
  PlatformCenterNavModule,
  UtilsModule,
} from '@alauda/common-snippet';
import {
  AccordionModule,
  AutocompleteModule,
  BackTopModule,
  BreadcrumbModule,
  ButtonModule,
  CardModule,
  CheckboxModule,
  DialogModule,
  DropdownModule,
  FormModule,
  IconModule,
  InlineAlertModule,
  InputModule,
  NotificationModule,
  PaginatorModule,
  RadioModule,
  SelectModule,
  SortModule,
  StatusBarModule,
  SwitchModule,
  TableModule,
  TableOfContentsModule,
  TabsModule,
  TagModule,
  TooltipModule,
} from '@alauda/ui';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HighchartsChartModule } from 'highcharts-angular';
import { AitSharedModule } from 'submodules/ait-shared/shared/shared.module';

import { K8sDialogService } from 'app/services/k8s-dialog.service';

import { DefaultBreadcrumbComponent } from './components/breadcrumb/default-breadcrumb.component';
import { CardSectionComponent } from './components/card-section/card-section.component';
import { CircleChartComponent } from './components/circle-chart/circle.component';
import { ClusterBadgeComponent } from './components/cluster-badge/component';
import { CodeDisplayDialogComponent } from './components/code-display-dialog/component';
import {
  ConfirmDeleteComponent,
  ConfirmDeleteContentDirective,
  ConfirmDeleteLabelDirective,
} from './components/confirm-delete/confirm-delete.component';
import { ErrorMapperComponent } from './components/error-mapper/error-mapper.component';
import {
  FieldSetGroupComponent,
  FieldSetItemActionDirective,
  FieldSetItemComponent,
} from './components/field-set/field-set.component';
import { FoldableBlockComponent } from './components/foldable-block/component';
import {
  FormInlineGroupComponent,
  FormInlineItemComponent,
} from './components/form-inline/component';
import {
  ArrayFormTableComponent,
  ArrayFormTableFooterDirective,
  ArrayFormTableHeaderDirective,
  ArrayFormTableRowControlDirective,
  ArrayFormTableRowDirective,
  ArrayFormTableZeroStateDirective,
} from './components/form-table/array-form-table/component';
import { KeyValueFormTableComponent } from './components/form-table/key-value-form-table/component';
import { StringArrayFormTableComponent } from './components/form-table/string-array-form-table/component';
import { HeaderComponent } from './components/header/component';
import { LoadingMaskComponent } from './components/loading-mask/loading-mask.component';
import { LogoComponent } from './components/logo/logo.component';
import { MenuTriggerComponent } from './components/menu-trigger/component';
import { NoDataComponent } from './components/no-data/no-data.component';
import { PasswordDisplayComponent } from './components/password-display/component';
import { PieChartComponent } from './components/pie-chart/component';
import { RelativeTimeComponent } from './components/relative-time/component';
import { ResourceYamlDisplayComponent } from './components/resource-yaml-display/component';
import { SearchGroupWrapperComponent } from './components/search-group-wrapper/component';
import { StatusBadgeComponent } from './components/status-badge/status-badge.component';
import { StatusIconComponent } from './components/status-icon/component';
import { StepsProcessComponent } from './components/steps-process/steps-process.component';
import { SwiperSliderDirective } from './components/swiper/swiper-slide.directive';
import { SwiperComponent } from './components/swiper/swiper.component';
import { TagsLabelComponent } from './components/tags-label/tags-label.component';
import { TerminatingTagComponent } from './components/terminating-tag/component';
import { UpdateKeyValueDialogComponent } from './components/update-key-value-dialog/update-key-value-dialog.component';
import { ZeroStateComponent } from './components/zero-state/zero-state.component';
import { DisabledControlDirective } from './directives/disabled-control.directive';
import { MaxValidatorDirective } from './directives/max.directive';
import { MinValidatorDirective } from './directives/min.directive';
import { PageHeaderContentDirective } from './directives/page-header-content.directive';
import { TagInputPatternDirective } from './directives/pattern.directive';
import { ProjectListDialogComponent } from './features/cluster-project-list/component';
import { LoadingDialogComponent } from './features/loading-dialog/component';
import { MetricChartComponent } from './features/metric-chart/component';
import { PipesModule } from './pipes/pipes.module';

const COMMON_MODULES = [
  CommonModule,
  FormsModule,
  K8SResourceListModule,
  ReactiveFormsModule,
  RouterModule,
  PipesModule,
  DisabledContainerModule,
  AsyncDataModule,
  UtilsModule,
  NotificationUtilModule,
  AccountMenuModule,
  PlatformCenterNavModule,
  PageGuardModule,
  HelpMenuModule,
];

const AUI_MODULES = [
  AccordionModule,
  AccordionModule,
  BackTopModule,
  BreadcrumbModule,
  ButtonModule,
  CardModule,
  CheckboxModule,
  CheckboxModule,
  CodeEditorModule,
  DialogModule,
  DropdownModule,
  FormModule,
  IconModule,
  InlineAlertModule,
  InputModule,
  NotificationModule,
  PaginatorModule,
  RadioModule,
  SelectModule,
  SortModule,
  StatusBarModule,
  SwitchModule,
  TableModule,
  TableOfContentsModule,
  TabsModule,
  TagModule,
  TooltipModule,
  AutocompleteModule,
];

const THIRD_PARTY_MODULES = [
  FlexLayoutModule,
  HighchartsChartModule,
  AitSharedModule,
];

const SHARED_COMPONENTS = [
  SwiperSliderDirective,
  SwiperComponent,
  SearchGroupWrapperComponent,
  NoDataComponent,
  MenuTriggerComponent,
  StepsProcessComponent,
  StatusIconComponent,
  CircleChartComponent,
  CardSectionComponent,
  ConfirmDeleteComponent,
  ConfirmDeleteContentDirective,
  ConfirmDeleteLabelDirective,
  DefaultBreadcrumbComponent,
  FieldSetItemComponent,
  FieldSetGroupComponent,
  FieldSetItemActionDirective,
  TagsLabelComponent,
  UpdateKeyValueDialogComponent,
  PageHeaderContentDirective,
  ErrorMapperComponent,
  MinValidatorDirective,
  MaxValidatorDirective,
  TagInputPatternDirective,
  LogoComponent,
  ClusterBadgeComponent,
  PieChartComponent,
  DisabledControlDirective,

  ArrayFormTableComponent,
  ArrayFormTableFooterDirective,
  ArrayFormTableHeaderDirective,
  ArrayFormTableRowControlDirective,
  ArrayFormTableRowDirective,
  ArrayFormTableZeroStateDirective,
  KeyValueFormTableComponent,
  StringArrayFormTableComponent,
  PasswordDisplayComponent,
  LoadingDialogComponent,
  FormInlineGroupComponent,
  FormInlineItemComponent,
  LoadingMaskComponent,
  RelativeTimeComponent,
  ZeroStateComponent,
  FoldableBlockComponent,
  StatusBadgeComponent,
  TerminatingTagComponent,
  ProjectListDialogComponent,
  MetricChartComponent,
  ResourceYamlDisplayComponent,
  CodeDisplayDialogComponent,
  HeaderComponent,
];

@NgModule({
  imports: [
    ...AUI_MODULES,
    ...COMMON_MODULES,
    ...THIRD_PARTY_MODULES,
    CommonLayoutModule,
  ],
  declarations: [...SHARED_COMPONENTS],
  exports: [
    ...AUI_MODULES,
    ...COMMON_MODULES,
    ...SHARED_COMPONENTS,
    ...THIRD_PARTY_MODULES,
    CommonLayoutModule,
  ],
  entryComponents: [
    ConfirmDeleteComponent,
    UpdateKeyValueDialogComponent,
    LoadingDialogComponent,
    ProjectListDialogComponent,
  ],
  providers: [K8sDialogService],
})
export class SharedModule {}
