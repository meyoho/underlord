import {
  Directive,
  Input,
  OnChanges,
  Provider,
  SimpleChanges,
  forwardRef,
} from '@angular/core';
import {
  AbstractControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
  ValidatorFn,
  Validators,
} from '@angular/forms';

export const MIN_VALIDATOR: Provider = {
  provide: NG_VALIDATORS,
  useExisting: forwardRef(() => MinValidatorDirective),
  multi: true,
};

// tslint:disable: directive-selector no-host-metadata-property no-identical-functions

/**
 * ! modified base on https://github.com/angular/angular/commit/81925fa66d2c930f68d3b6dd084c954ea1bab6b6
 */

/**
 * A directive which installs the {@link MinValidator} for any `formControlName`,
 * `formControl`, or control with `ngModel` that also has a `min` attribute.
 *
 * @experimental
 */
@Directive({
  selector:
    'aui-input[min],[min][formControlName],[min][formControl],[min][ngModel]',
  providers: [MIN_VALIDATOR],
  host: { '[attr.min]': 'min ? min : null' },
})
export class MinValidatorDirective implements Validator, OnChanges {
  @Input() min: string;

  private _validator: ValidatorFn;
  private _onChange: () => void;

  ngOnChanges(changes: SimpleChanges): void {
    if ('min' in changes) {
      this._createValidator();
      if (this._onChange) {
        this._onChange();
      }
    }
  }

  validate(c: AbstractControl): ValidationErrors | null {
    return this._validator(c);
  }

  registerOnValidatorChange(fn: () => void): void {
    this._onChange = fn;
  }

  private _createValidator(): void {
    this._validator = Validators.min(parseFloat(this.min));
  }
}
