import { Directive, Input, OnChanges, SimpleChanges } from '@angular/core';
import {
  AbstractControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
  ValidatorFn,
} from '@angular/forms';

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: 'aui-tags-input[tagInputPattern]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: TagInputPatternDirective,
      multi: true,
    },
  ],
})
export class TagInputPatternDirective implements Validator, OnChanges {
  @Input()
  tagInputPattern: string;

  private validator: ValidatorFn;
  private onChange: () => void;
  private pattern: RegExp;

  ngOnChanges(changes: SimpleChanges): void {
    if ('tagInputPattern' in changes) {
      this.pattern = new RegExp(this.tagInputPattern);
      this.createValidator();
      if (this.onChange) {
        this.onChange();
      }
    }
  }

  validate(c: AbstractControl): ValidationErrors | null {
    return this.validator(c);
  }

  registerOnValidatorChange(fn: () => void): void {
    this.onChange = fn;
  }

  private createValidator(): void {
    this.validator = (control: AbstractControl): ValidationErrors => {
      const value: string[] = control.value || [];
      const test = value.every(v => this.pattern.test(v));
      return test ? null : { pattern: true };
    };
  }
}
