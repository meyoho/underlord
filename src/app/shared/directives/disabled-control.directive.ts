import { Directive, Input } from '@angular/core';
import { NgControl } from '@angular/forms';

/**
 *
 * Usage:
 * If you want to disable a formcontrol.
 *
 * eg:
 * <input formControlName="name" [aluDisabledControl]="function()">
 *
 * todo: move to common
 */
@Directive({
  selector: '[aluDisabledControl]',
})
export class DisabledControlDirective {
  @Input()
  set aluDisabledControl(condition: boolean) {
    const action = condition ? 'disable' : 'enable';
    this.ngControl.control && this.ngControl.control[action]();
  }

  constructor(private readonly ngControl: NgControl) {}
}
