import { TemplatePortal } from '@angular/cdk/portal';
import {
  Directive,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';

import {
  TemplateHolder,
  TemplateHolderType,
  UiStateService,
} from 'app/services/ui-state.service';

/**
 * Dynamically change page header content based on active template.
 *
 * Usage:
 * If you want to customize a page header, wrap a template with *aluPageHeaderContent.
 *
 * eg:
 * <ng-container *aluPageHeaderContent>
 *   ... YOUR TEMPLATE CONTENT
 * </ng-container>
 */
@Directive({
  selector: '[aluPageHeaderContent]',
})
export class PageHeaderContentDirective implements OnInit, OnDestroy {
  templateHolder: TemplateHolder;

  constructor(
    private readonly templateRef: TemplateRef<any>,
    private readonly viewContainerRef: ViewContainerRef,
    private readonly uiState: UiStateService,
  ) {
    this.templateHolder = this.uiState.getTemplateHolder(
      TemplateHolderType.PageHeaderContent,
    );
  }

  ngOnInit() {
    const portal = new TemplatePortal(this.templateRef, this.viewContainerRef);
    this.templateHolder.setTemplatePortal(portal);
  }

  ngOnDestroy(): void {
    this.templateHolder.setTemplatePortal(null);
  }
}
