import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'alu-zero-state',
  templateUrl: './zero-state.component.html',
  styleUrls: ['./zero-state.component.scss'],
})
export class ZeroStateComponent {
  @Input()
  resourceName = 'data';

  @Input()
  zeroState = true;

  @Input()
  fetching = false;

  @HostBinding('hidden')
  get hiddenState() {
    return !this.zeroState && !this.fetching;
  }
}
