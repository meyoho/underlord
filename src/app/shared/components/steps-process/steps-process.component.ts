import { Component, EventEmitter, Input, Output } from '@angular/core';

export interface StepProcessItem {
  label: string;
  type?: 'info' | 'error' | 'success';
}

@Component({
  selector: 'alu-steps-process',
  templateUrl: './steps-process.component.html',
  styleUrls: ['./steps-process.component.scss'],
})
export class StepsProcessComponent {
  @Input()
  steps: Array<string | StepProcessItem>;

  @Input()
  currStep: number;

  @Input()
  layout: 'row' | 'column' = 'row';

  selectedStep: number;

  @Input()
  selectable = false;

  @Input()
  needTranslate = true;

  @Output()
  select = new EventEmitter<number>();

  stepClick(step: number) {
    if (!this.selectable || step > this.currStep) {
      return;
    }
    this.selectedStep = step;
    this.select.emit(step);
  }

  initType(steps: Array<string | StepProcessItem>) {
    return steps.map(step => {
      return typeof step === 'string'
        ? {
            label: step,
            type: 'info',
          }
        : step;
    });
  }
}
