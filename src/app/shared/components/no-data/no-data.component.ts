import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { get, head, isArray } from 'lodash-es';

// TODO: temp solution for corner case
export const DEFAULT_TRANSLATES = {
  loading: 'loading',
  noResource: 'no_resource',
  noData: 'no_data',
  retryOnError: 'retry_on_error',
  retry: 'retry',
  forbidden: 'forbidden',
  notFound: 'not_found',
  serviceUnavailable: 'service_unavailable',
};

@Component({
  selector: 'alu-no-data',
  templateUrl: 'no-data.component.html',
  styleUrls: ['no-data.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NoDataComponent {
  @Input() resourceName = '';

  @Input() error: any = null;

  @Input() loading = false;

  @Input() retryDisabled = false;

  @Input() mode: 'table' | 'list' | 'card' = 'list';

  @Input() translateOptions = DEFAULT_TRANSLATES;

  @Output() retry = new EventEmitter<void>();

  get errorType() {
    if (!this.error) {
      return null;
    }

    const status = isArray(this.error)
      ? this.getListErrorStatus(this.error)
      : this.error.status || 500;

    if (!status) {
      return null;
    }

    switch (status) {
      case 403:
        return this.translateOptions.forbidden;
      case 404:
        return this.translateOptions.notFound;
      default:
        return this.translateOptions.serviceUnavailable;
    }
  }

  getListErrorStatus(errors: any[]) {
    if (errors.length === 0) {
      return null;
    }

    return get(head(errors), 'ErrStatus.code') || 500;
  }

  onRetry() {
    this.retry.emit();
  }
}
