import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'alu-foldable-block',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class FoldableBlockComponent {
  @HostBinding('class.foldable-card')
  get foldableCard() {
    return this.context === 'card';
  }

  @Input() context = 'form'; // form 对应表单里的样式, card 对应详情页

  @Input()
  hint: string;

  @Input() folded = true;
}
