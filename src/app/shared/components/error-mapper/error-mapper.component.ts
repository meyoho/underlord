import {
  ObservableInput,
  StringMap,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';

export const KNOWN_ERRORS: StringMap = {
  required: 'field_required',
  pattern: 'pattern_error',
};

@Component({
  selector: 'alu-error-mapper',
  template: `
    <ng-container *ngIf="errors$ | async">
      {{ errorMessage$ | async }}
    </ng-container>
  `,
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ErrorMapperComponent {
  @ObservableInput()
  @Input('errors')
  readonly errors$: Observable<Record<string, boolean>>;

  @ObservableInput()
  @Input('errorsMapper')
  readonly errorsMapper$: Observable<StringMap>;

  readonly errorMessage$ = combineLatest([
    this.errors$,
    this.errorsMapper$,
  ]).pipe(
    map(([errors, errorsMapper]) => {
      const firstError = Object.keys(errors || {})[0];

      if (!firstError) {
        return;
      }

      return (
        errorsMapper?.[firstError] ||
        this.translate.get(KNOWN_ERRORS[firstError], null, true) ||
        errorsMapper?.unknown ||
        this.translate.get('unknown_error')
      );
    }),
    distinctUntilChanged(),
    publishRef(),
  );

  constructor(private readonly translate: TranslateService) {}
}
