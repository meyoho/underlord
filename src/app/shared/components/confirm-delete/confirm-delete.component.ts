import {
  DIALOG_DATA,
  DialogRef,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import { HttpErrorResponse } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  Directive,
  EventEmitter,
  Inject,
  Input,
  Optional,
  Output,
  TemplateRef,
} from '@angular/core';
import { get } from 'lodash-es';
import { BehaviorSubject, Observable, combineLatest, from } from 'rxjs';
import { finalize, map } from 'rxjs/operators';
export enum DeleteType {
  DELETE = 'delete',
  UNINSTALL = 'uninstall',
}
export type DeleteResourceApi<P, R> = (
  inputValue?: string,
  params?: P,
) => Observable<R> | Promise<R>;

export interface ConfirmDeleteData<P = unknown, R = unknown> {
  context?: unknown;
  title: string;
  resourceName: string | string[];
  deleteTips: string;
  confirmTips?: string;
  extraConfirmTips?: string;
  deleteResourceApi: DeleteResourceApi<P, R>;
  deleteResourceParams?: P;
  deleteSuccessMsg: string;
  deleteFailMsg: string;
  confirmText?: string;
}

@Directive({
  selector: '[aluConfirmDeleteContent]',
})
export class ConfirmDeleteContentDirective {
  constructor(public templateRef: TemplateRef<any>) {}
}

@Directive({
  selector: '[aluConfirmDeleteLabel]',
})
export class ConfirmDeleteLabelDirective {
  constructor(public templateRef: TemplateRef<any>) {}
}

@Component({
  selector: 'alu-confirm-delete',
  templateUrl: './confirm-delete.component.html',
  styleUrls: ['./confirm-delete.component.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfirmDeleteComponent<P = unknown, R = unknown>
  implements ConfirmDeleteData<P, R> {
  @Input()
  context?: unknown;

  @Input()
  type: DeleteType = DeleteType.DELETE;

  @Input()
  title: string;

  @Input()
  resourceName: string | string[];

  @Input()
  deleteTips: string;

  @Input()
  confirmTips?: string;

  @Input()
  extraConfirmTips?: string;

  @Input()
  confirmText?: string;

  @Input()
  deleteResourceApi: DeleteResourceApi<P, R>;

  @Input()
  deleteResourceParams?: P;

  @Input()
  deleteSuccessMsg: string;

  @Input()
  deleteFailMsg: string;

  @Output()
  close = new EventEmitter<boolean>();

  @ContentChild(ConfirmDeleteContentDirective, {
    read: TemplateRef,
  })
  contentTemplate: TemplateRef<any>;

  @ContentChild(ConfirmDeleteLabelDirective, {
    read: TemplateRef,
  })
  labelTemplate: TemplateRef<any>;

  inputValue = '';

  inputValue$$ = new BehaviorSubject(this.inputValue);

  deleting$$ = new BehaviorSubject(false);
  deleteType = DeleteType;
  disabled$ = combineLatest([this.inputValue$$, this.deleting$$]).pipe(
    map(([inputValue, deleting]) => {
      const { resourceName } = this;
      return (
        deleting ||
        !(Array.isArray(resourceName)
          ? resourceName.includes(inputValue)
          : inputValue === resourceName)
      );
    }),
  );

  constructor(
    private readonly message: MessageService,
    private readonly notification: NotificationService,
    @Optional()
    private readonly dialogRef?: DialogRef<
      ConfirmDeleteComponent<P, R>,
      boolean
    >,
    @Optional()
    @Inject(DIALOG_DATA)
    public data?: ConfirmDeleteData<P, R>,
  ) {
    this.resolveData();
  }

  onConfirm() {
    this.deleting$$.next(true);
    from(
      this.deleteResourceApi.call(
        this.context,
        this.inputValue,
        this.deleteResourceParams,
      ),
    )
      .pipe(finalize(() => this.deleting$$.next(false)))
      .subscribe(
        () => {
          this.message.success(this.deleteSuccessMsg);
          this.closeDialog(true);
        },
        (e: HttpErrorResponse) =>
          this.notification.error({
            title: this.deleteFailMsg,
            content: get(e.error, 'message', e.message),
          }),
      );
  }

  closeDialog(confirm = false) {
    if (this.dialogRef) {
      this.dialogRef.close(confirm);
    } else {
      this.close.emit(confirm);
    }
  }

  private resolveData() {
    if (!this.data) {
      return;
    }

    Object.entries(this.data).forEach(([key, value]) => {
      if (value != null) {
        this[key as keyof ConfirmDeleteData<P, R>] = value;
      }
    });
  }
}
