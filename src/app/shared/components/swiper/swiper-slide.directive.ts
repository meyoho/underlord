import { Directive, HostBinding, TemplateRef } from '@angular/core';

@Directive({
  selector: '[aluSwiperSlider]',
})
export class SwiperSliderDirective {
  @HostBinding('class.alu-swiper__slide') class = true;
  constructor(public template: TemplateRef<any>) {}
}
