import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  AfterContentInit,
  ChangeDetectionStrategy,
  Component,
  ContentChildren,
  ElementRef,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { ReplaySubject } from 'rxjs';

import { SwiperSliderDirective } from './swiper-slide.directive';

@Component({
  selector: 'alu-swiper',
  templateUrl: './swiper.component.html',
  styleUrls: ['./swiper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('swiper', [
      state(
        'stay',
        style({
          marginLeft: '0',
        }),
      ),
      state(
        'moveLeft',
        style({
          marginLeft: '0',
        }),
      ),
      transition(
        '* => moveLeft',
        animate(
          '500ms ease-in-out',
          style({
            marginLeft: '-100%',
          }),
        ),
      ),
      transition(
        '* => stay',
        animate(
          '500ms ease-in-out',
          style({
            marginLeft: '-100%',
          }),
        ),
      ),
    ]),
  ],
})
export class SwiperComponent implements AfterContentInit {
  sliders$ = new ReplaySubject(1);
  timer: any;
  state = 'stop';
  duration = 3000;
  currIndex = 0;
  cache: SwiperSliderDirective[];
  @ContentChildren(SwiperSliderDirective)
  slidersQueryList: QueryList<SwiperSliderDirective>;

  @ViewChild('aluSwiper')
  aluSwiper: ElementRef;

  @ViewChild('aluWrapper')
  aluWrapper: ElementRef;

  @ViewChildren('aluSwiperContainer')
  slideEls: ElementRef[];

  ngAfterContentInit() {
    const sliderArr = this.slidersQueryList.toArray();
    this.cache = sliderArr;
    this.sliders$.subscribe(() => {
      setTimeout(() => {
        this.resizeSlideStyle();
      }, 0);
    });
    this.sliders$.next(this.cache);
    this.swiperStart();
  }

  autoPlay(): void {
    if (this.timer) {
      clearInterval(this.timer);
    }
    this.timer = setInterval(() => {
      this.state = this.state === 'stay' ? 'moveLeft' : 'stay';
    }, this.duration);
  }

  swiperStop() {
    if (this.timer) {
      clearInterval(this.timer);
    }
  }

  swiperStart() {
    if (this.cache.length >= 2) {
      this.autoPlay();
    }
  }

  swiperTo(index: number) {
    if (index === this.currIndex) {
      return;
    }
    this.swiperStop();
    this.sliders$.next(this.slidersQueryList.toArray());
    this.aluWrapper.nativeElement.style.marginLeft =
      -this.currIndex * 100 + '%';
    this.currIndex = index;
    setTimeout(() => {
      this.aluWrapper.nativeElement.style.marginLeft = -index * 100 + '%';
      this.aluWrapper.nativeElement.style.transition =
        'margin-left 500ms ease-in-out';
      const transFun = () => {
        this.resetSlides(index);
        this.aluWrapper.nativeElement.style.marginLeft = 0;
        this.aluWrapper.nativeElement.style.transition = null;
        this.aluWrapper.nativeElement.removeEventListener(
          'transitionend',
          transFun,
        );
        this.swiperStart();
      };
      this.aluWrapper.nativeElement.addEventListener('transitionend', transFun);
    }, 0);
  }

  afterPlay(): void {
    if (this.state === 'stop') {
      return;
    }
    this.cache.push(this.cache[0]);
    this.cache.shift();
    this.currIndex =
      this.currIndex === this.cache.length - 1 ? 0 : this.currIndex + 1;
    this.sliders$.next(this.cache);
  }

  resetSlides(resetIndex: number) {
    const slidesLength = this.slidersQueryList.length;
    if (resetIndex >= slidesLength) {
      return;
    }
    const newSlides = new Array(slidesLength).fill(null).map((_s, index) => {
      return this.slidersQueryList.toArray()[
        (resetIndex + index) % slidesLength
      ];
    });
    this.cache = newSlides;
    this.sliders$.next(newSlides);
  }

  resizeSlideStyle() {
    if (!this.aluSwiper || !this.aluWrapper) {
      return;
    }
    const wrapperWidth = this.aluSwiper.nativeElement.clientWidth;
    this.aluWrapper.nativeElement.style.width =
      wrapperWidth * this.slidersQueryList.length + 'px';
    if (this.slideEls?.length > 0) {
      this.slideEls.forEach(ele => {
        ele.nativeElement.style.width = wrapperWidth + 'px';
      });
    }
  }
}
