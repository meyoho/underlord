import { ObservableInput } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { delay, map, startWith } from 'rxjs/operators';

@Component({
  selector: 'alu-circle-chart',
  templateUrl: './circle.component.html',
  styleUrls: ['./circle.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CircleChartComponent {
  @Input()
  noAvailText = 'unlimited';

  perimeter = Math.PI * 2 * 40;

  @Input()
  percent: number;

  @Input()
  color: string;

  @ObservableInput(true)
  readonly percent$: Observable<number>;

  animatedPercent$ = this.percent$.pipe(
    delay(0),
    startWith(0),
    map(percent =>
      percent < 0
        ? '0 1000'
        : `${(this.perimeter * percent) / 100} ${
            this.perimeter * (100 - percent)
          }`,
    ),
  );

  round(percent: number) {
    return Math.round(percent);
  }
}
