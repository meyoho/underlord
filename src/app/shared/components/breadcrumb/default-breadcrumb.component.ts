import { TranslateService } from '@alauda/common-snippet';
import { Component, OnInit } from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import { UiStateService } from 'app/services/ui-state.service';

@Component({
  selector: 'alu-default-breadcrumb',
  templateUrl: 'default-breadcrumb.component.html',
})
export class DefaultBreadcrumbComponent implements OnInit {
  partials$: Observable<string[]>;

  constructor(
    private readonly uiState: UiStateService,
    private readonly translate: TranslateService,
  ) {}

  ngOnInit(): void {
    this.partials$ = combineLatest([
      this.uiState.activeItemInfo$,
      this.translate.locale$,
    ]).pipe(
      map(([navItemPartials]) => {
        const partialsMatched = navItemPartials.href.match(/\w*\/(\w*)\/\w*/);
        if (partialsMatched && partialsMatched[1]) {
          return [
            this.translate.get(partialsMatched[1]),
            navItemPartials.label,
          ];
        }
      }),
    );
  }
}
