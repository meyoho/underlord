import {
  ChangeDetectionStrategy,
  Component,
  Directive,
  Input,
} from '@angular/core';

@Component({
  selector: 'alu-field-set-item',
  templateUrl: 'field-set-item.component.html',
  styleUrls: ['field-set-item.component.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FieldSetItemComponent {
  @Input() allowOverflow = true;
  @Input() allowWrap = false;
  @Input() allowFlex = false;
}

// tslint:disable-next-line: max-classes-per-file
@Component({
  selector: 'alu-field-set-group',
  template: '<ng-content></ng-content>',
  styleUrls: ['field-set-group.component.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FieldSetGroupComponent {}

@Directive({
  selector: '[aluFieldSetItemAction]',
})
export class FieldSetItemActionDirective {}
