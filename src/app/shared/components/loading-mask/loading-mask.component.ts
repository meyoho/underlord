import { Component, HostBinding, Input } from '@angular/core';

/**
 * A simple loading mask component which will show a spinner
 * when loading attribute is true.
 */
@Component({
  selector: 'alu-loading-mask',
  template: `
    <aui-icon icon="spinner" [size]="iconSize"></aui-icon>
  `,
  styleUrls: ['loading-mask.component.scss'],
})
export class LoadingMaskComponent {
  @Input() iconSize = '24px, 24px';
  @Input()
  loading: boolean;

  @HostBinding('hidden')
  get hidden() {
    return !this.loading;
  }
}
