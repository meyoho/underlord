import { Component, Inject, Input } from '@angular/core';

import { Environments } from 'app/api/envs/types';
import { ENVIRONMENTS } from 'app/services/services.module';

@Component({
  selector: 'alu-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss'],
})
export class LogoComponent {
  @Input() title: string;

  constructor(@Inject(ENVIRONMENTS) public env: Environments) {}
}
