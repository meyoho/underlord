import { K8sApiService, TranslateService } from '@alauda/common-snippet';
import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, Subject, combineLatest } from 'rxjs';
import { map, pluck, takeUntil } from 'rxjs/operators';

import { Cluster } from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';

const CLUSTER_KEY = 'CLUSTER';

@Component({
  selector: 'alu-cluster-badge',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class ClusterBadgeComponent implements OnInit, OnDestroy {
  selectedCluster: Cluster;

  clusters$: Observable<Cluster[]>;
  clusterLength$: Observable<number>;
  onDestroy$ = new Subject<void>();
  search$ = new BehaviorSubject('');
  filteredClusters$: Observable<Cluster[]>;

  @Output() clusterChange = new EventEmitter<Cluster>();

  constructor(
    private readonly k8sApi: K8sApiService,
    private readonly router: Router,
    private readonly translate: TranslateService,
  ) {}

  ngOnInit() {
    this.clusters$ = this.k8sApi
      .getGlobalResourceList<Cluster>({
        type: RESOURCE_TYPES.CLUSTER_REGISTRY,
      })
      .pipe(pluck('items'));
    this.clusterLength$ = this.clusters$.pipe(map(clusters => clusters.length));
    this.filteredClusters$ = combineLatest([this.clusters$, this.search$]).pipe(
      takeUntil(this.onDestroy$),
      map(([clusters, filter]) => {
        return clusters.filter(cluster =>
          cluster.metadata.name.startsWith(filter),
        );
      }),
    );
    this.clusters$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((clusters: Cluster[]) => {
        const name = window.sessionStorage.getItem(CLUSTER_KEY);
        const cluster = clusters.find(c => c.metadata.name === name);
        if (cluster) {
          this.onClusterSelected(cluster);
        } else if (clusters.length > 0) {
          this.onClusterSelected(clusters[0]);
        }
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  onClusterSelected(cluster: Cluster) {
    this.selectedCluster = cluster;
    window.sessionStorage.setItem(CLUSTER_KEY, cluster.metadata.name);
    this.clusterChange.emit(cluster);
  }

  goToDetail() {
    if (this.selectedCluster) {
      // todo: 更改成新的路由
      this.router.navigate([
        '/manage-platform/cluster/detail',
        this.selectedCluster.metadata.name,
      ]);
    }
  }

  trackByName(_: number, cluster: Cluster) {
    return cluster.metadata.name;
  }

  onSearch(name: string) {
    this.search$.next(name);
  }

  searchName(ev: Event) {
    // prevent closing dialog
    ev.stopPropagation();
  }

  getDisplayNameLabel = (name: string) => {
    return `${this.translate.get('cluster')}:${name}`;
  };
}
