import {
  Arrayable,
  KubernetesResource,
  ObservableInput,
} from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { safeDump } from 'js-yaml';
import { sortBy } from 'lodash-es';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { viewActions, yamlReadOptions } from 'app/utils';

@Component({
  selector: 'alu-resource-yaml-display',
  template: `
    <aui-code-editor
      name="yaml"
      [ngModel]="yaml$ | async"
      [options]="editorOptions"
      [actionsConfig]="viewActions"
    ></aui-code-editor>
  `,
  styleUrls: ['./style.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourceYamlDisplayComponent<T extends KubernetesResource> {
  @ObservableInput()
  @Input('resource')
  private readonly resource$: Observable<Arrayable<T>>;

  yaml$ = this.resource$.pipe(
    map(resource =>
      sortBy(Array.isArray(resource) ? resource : [resource], 'kind')
        .map(res =>
          safeDump(res, {
            sortKeys: true,
          }),
        )
        .join('---\r\n'),
    ),
  );

  editorOptions = yamlReadOptions;
  viewActions = viewActions;
}
