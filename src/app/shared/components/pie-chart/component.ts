import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
// eslint-disable-next-line node/no-extraneous-import
import { arc, pie } from 'd3-shape';

@Component({
  selector: 'alu-pie-chart',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PieChartComponent {
  width = 36;
  height = 36;

  @Input()
  percentage = 0;

  @Input()
  color: string;

  private readonly valueAccessor: (item: any) => number = (item: any) => item;

  get transform() {
    return 'translate(18, 18)';
  }

  get arcsValue() {
    return [this.percentage, 1 - this.percentage];
  }

  get arcs() {
    return pie()
      .startAngle(0)
      .endAngle(Math.PI * 2)
      .value(this.valueAccessor)
      .sort(undefined)(this.arcsValue);
  }

  get UsedFractionArcD() {
    const { startAngle, endAngle } = this.arcs[0];
    return arc()({
      startAngle,
      endAngle,
      innerRadius: 14,
      outerRadius: 18,
    });
  }

  get NormalFractionArcD() {
    const { startAngle, endAngle } = this.arcs[1];
    return arc()({
      startAngle,
      endAngle,
      innerRadius: 14,
      outerRadius: 18,
    });
  }
}
