import { Component, Input } from '@angular/core';

export enum GenericStatus {
  Success = 'success',
  Succeeded = 'success',
  Completed = 'completed',
  Running = 'running',
  Pending = 'pending',
  Failed = 'failed',
  Error = 'error',
  Unknown = 'unknown',
  Bound = 'bound',
  Warning = 'warning',
  Terminating = 'terminating',
  Stopped = 'stopped',
  Suspended = 'suspended',
  Invalid = 'invalid',
  Exclamation = 'exclamation',
  Normal = 'normal',
  Abnormal = 'abnormal',
  Creating = 'creating',
  Accessing = 'accessing',
  Adding = 'adding',
  Deleting = 'deleting',
  Deploying = 'deploying',
}

@Component({
  selector: 'alu-status-icon',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class StatusIconComponent {
  @Input()
  status: GenericStatus;

  getSvgIconName() {
    switch (this.status) {
      case GenericStatus.Success:
      case GenericStatus.Normal:
      case GenericStatus.Completed:
        return 'check_circle_s';
      case GenericStatus.Running:
        return 'basic:running_circle_s';
      case GenericStatus.Creating:
      case GenericStatus.Accessing:
      case GenericStatus.Adding:
      case GenericStatus.Pending:
      case GenericStatus.Deploying:
        return 'basic:sync_circle_s';
      case GenericStatus.Error:
      case GenericStatus.Failed:
        return 'basic:close_circle_s';
      case GenericStatus.Unknown:
        return 'basic:question_circle_s';
      case GenericStatus.Stopped:
        return 'basic:stop_circle_s';
      case GenericStatus.Suspended:
        return 'basic:paused_circle_s';
      case GenericStatus.Bound:
        return 'basic:link_circle_s';
      case GenericStatus.Warning:
        return 'exclamation_circle_s';
      case GenericStatus.Terminating:
        return 'basic:hourglass_half_circle_s';
      case GenericStatus.Invalid:
        return 'basic:minus_circle_s';
      case GenericStatus.Exclamation:
        return 'basic:hourglass_half_circle_s';
      case GenericStatus.Abnormal:
        return 'exclamation_triangle_s';
      case GenericStatus.Deleting:
        return 'basic:trash_s';
    }
  }
}
