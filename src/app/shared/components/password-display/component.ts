import { Component, Input } from '@angular/core';

@Component({
  selector: 'alu-password-display',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class PasswordDisplayComponent {
  @Input()
  value: string;

  hidden = true;
}
