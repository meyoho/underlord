import { ValueHook } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { sortBy } from 'lodash-es';

import { ErrorMapper } from 'app/typings';

import { BaseStringMapFormComponent } from '../base-string-map-form.component';

@Component({
  selector: 'alu-key-value-form-table',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class KeyValueFormTableComponent extends BaseStringMapFormComponent
  implements OnInit, OnChanges {
  @Input()
  resourceName = 'data';

  @Input()
  validator: {
    key: ValidatorFn[];
    value: ValidatorFn[];
  } = {
    key: [],
    value: [],
  };

  @Input()
  errorMapper: {
    key: ErrorMapper;
    value: ErrorMapper;
  } = {
    key: {},
    value: {},
  };

  @ValueHook<KeyValueFormTableComponent, 'readonlyKeys'>(Boolean)
  @Input()
  readonlyKeys: Array<string | RegExp> = [];

  constructor(public injector: Injector) {
    super(injector);
  }

  ngOnChanges({ validator }: SimpleChanges): void {
    if (validator && validator.currentValue && this.form) {
      this.form.controls.forEach((formArray: FormArray) => {
        const [key, value] = formArray.controls;
        key.setValidators(this.getKeyValidators());
        value.setValidators(this.getValueValidators());
      });
    }
  }

  isReadonly(key: string) {
    return this.readonlyKeys.some(toCheckKey => {
      if (!toCheckKey) {
        return false;
      }
      if (typeof toCheckKey === 'string') {
        return toCheckKey === key;
      }

      return toCheckKey.test(key);
    });
  }

  adaptResourceModel(resource: { [key: string]: string }) {
    let newFormModel = Object.entries(resource || {});
    // 排序，先按字母排序，再把 readonly 的放前面
    newFormModel = sortBy(newFormModel, (arr: [string, string]) => {
      return arr[0];
    });
    newFormModel.sort((arr1: [string, string], arr2: [string, string]) => {
      const flag1 = this.isReadonly(arr1[0]);
      const flag2 = this.isReadonly(arr2[0]);
      if ((flag1 && flag2) || (!flag1 && !flag2)) {
        return 0;
      } else {
        return flag1 ? -1 : 1;
      }
    });
    if (newFormModel.length === 0) {
      newFormModel = this.getDefaultFormModel();
    }

    return newFormModel;
  }

  rowBackgroundColorFn(row: FormControl) {
    if (row.invalid) {
      return '#f9f2f4';
    } else {
      return '';
    }
  }

  getKeyValidators() {
    return this.validator?.key || [];
  }

  getValueValidators() {
    return this.validator?.value || [];
  }

  isLabelReadonly(index: number) {
    const formArray = this.form.controls[index] as FormArray;
    return formArray.valid && this.isReadonly(formArray.controls[0].value);
  }
}
