import { StringMap } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { FormControl, ValidatorFn } from '@angular/forms';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';

@Component({
  selector: 'alu-string-array-form-table',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StringArrayFormTableComponent
  extends BaseResourceFormArrayComponent<string>
  implements OnInit {
  @Input()
  resourceName: string;

  @Input()
  validators: ValidatorFn[] = [];

  @Input()
  errorMapper: StringMap = {};

  constructor(public injector: Injector) {
    super(injector);
  }

  add(index = this.form.length) {
    this.form.insert(index, this.getOnFormArrayResizeFn()());
    this.cdr.markForCheck();
  }

  remove(index: number) {
    this.form.removeAt(index);
    this.cdr.markForCheck();
  }

  rowBackgroundColorFn(row: FormControl) {
    if (row.invalid) {
      return '#f9f2f4';
    } else {
      return '';
    }
  }

  getOnFormArrayResizeFn() {
    return () => this.fb.control('', this.validators);
  }
}
