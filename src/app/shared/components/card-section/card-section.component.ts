import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'alu-card-section',
  templateUrl: './card-section.component.html',
  styleUrls: ['./card-section.component.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardSectionComponent {
  @Input()
  title: string;

  @Input()
  bordered = true;
}
