import { Component, Input } from '@angular/core';

@Component({
  selector: 'alu-header',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class HeaderComponent {
  @Input()
  logoTitle: string;
}
