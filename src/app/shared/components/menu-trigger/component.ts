import { MenuComponent } from '@alauda/ui';
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'alu-menu-trigger',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class MenuTriggerComponent {
  @Input()
  size: string;

  @Input()
  menu: MenuComponent;

  @Input()
  context: { [key: string]: any };

  @Output()
  menuTriggerShow = new EventEmitter<void>();
}
