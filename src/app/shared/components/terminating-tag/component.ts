import { KubernetesResource } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'alu-deleting-tag',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TerminatingTagComponent {
  @Input()
  resource: KubernetesResource;
}
