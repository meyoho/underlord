import { Injectable } from '@angular/core';
import { PreloadingStrategy, Route } from '@angular/router';
import { EMPTY, Observable, of } from 'rxjs';
import { delay, switchMap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class CustomPreloadingStrategy implements PreloadingStrategy {
  private loadCounter = 2; // starting from 2 to unblock other xhr.

  preload(_route: Route, fn: () => Observable<any>) {
    if (process.env.NODE_ENV === 'development') {
      return EMPTY;
    }
    return of(null).pipe(delay(this.loadCounter++ * 1000), switchMap(fn));
  }
}
