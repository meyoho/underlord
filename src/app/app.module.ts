import { CodeEditorModule } from '@alauda/code-editor';
import {
  CommonLayoutModule,
  DEFAULT_CODE_EDITOR_OPTIONS,
  TranslateModule,
} from '@alauda/common-snippet';
import {
  DialogModule,
  MessageModule,
  NotificationModule,
  PageModule,
} from '@alauda/ui';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServicesModule } from './services/services.module';
import { SharedModule } from './shared/shared.module';
import { en } from './translate/i18n-en';
import { zh } from './translate/i18n-zh';
@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    // Translate
    TranslateModule.forRoot({
      loose: true,
      translations: { zh, en },
    }),
    // Shared
    SharedModule,
    ServicesModule,
    CommonLayoutModule,

    // AUI ToastModule need to be imported here since it provides a service
    MessageModule,
    NotificationModule,
    DialogModule,

    CodeEditorModule.forRoot({
      baseUrl: 'lib/v1',
      defaultOptions: DEFAULT_CODE_EDITOR_OPTIONS,
    }),

    // App routing module should stay at the bottom
    AppRoutingModule,
    PageModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
