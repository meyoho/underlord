import { AuthorizationGuardService } from '@alauda/common-snippet';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CustomPreloadingStrategy } from './preloading.strategy';
const MANAGE_PLATFORM = 'manage-platform';
const routes: Routes = [
  {
    path: '',
    redirectTo: MANAGE_PLATFORM,
    pathMatch: 'full',
  },
  {
    path: MANAGE_PLATFORM,
    loadChildren: () =>
      import('app/manage-platform/module').then(M => M.ManagePlatformModule),
  },
  {
    path: 'manage-project',
    loadChildren: () =>
      import('app/manage-project/manage-project.module').then(
        M => M.ManageProjectModule,
      ),
  },
  {
    path: 'home',
    loadChildren: () => import('app/home/home.module').then(M => M.HomeModule),
  },
  {
    path: 'portal',
    loadChildren: () =>
      import('app/portal/module').then(M => M.ProductPortalModule),
  },
  {
    path: 'maintenance-center',
    loadChildren: () =>
      import('app/maintenance-center/module').then(
        M => M.MaintenanceCenterModule,
      ),
  },
  {
    path: 'terminal',
    loadChildren: () =>
      import('../terminal/terminal.module').then(M => M.TerminalModule),
  },
  {
    path: '**',
    redirectTo: MANAGE_PLATFORM,
  },
];

routes.forEach(route => {
  if (route.redirectTo) {
    return;
  }
  if (!route.canActivate) {
    route.canActivate = [];
  }
  if (!route.canActivateChild) {
    route.canActivateChild = [];
  }
  route.canActivate.unshift(AuthorizationGuardService);
  route.canActivateChild.unshift(AuthorizationGuardService);
});

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: CustomPreloadingStrategy,
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
