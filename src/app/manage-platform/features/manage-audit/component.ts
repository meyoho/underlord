import {
  DATE_TIME_FORMAT,
  Reason,
  TranslateService,
  WRITABLE_ACTIONS,
  publishRef,
  skipError,
} from '@alauda/common-snippet';
import { DialogService, DialogSize, NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import dayjs from 'dayjs';
import { BehaviorSubject, EMPTY, combineLatest, timer } from 'rxjs';
import { filter, map, startWith, switchMap, tap } from 'rxjs/operators';

import { AdvanceApi } from 'app/api/advance/api';
import { AuditApi } from 'app/api/audit/api';
import { Audit } from 'app/api/audit/types';
import { TIME_STAMP_OPTIONS_CUSTOM } from 'app/maintenance-center/features-shared/utils';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ManageAuditListComponent {
  @ViewChild(NgForm, { static: true })
  advancedForm: NgForm;

  isDeployed$ = this.advanceApi.getDiagnoseInfo().pipe(publishRef());
  currentTime = dayjs();
  startTime = this.currentTime.subtract(1, 'hour').format(DATE_TIME_FORMAT);

  endTime = this.currentTime.format(DATE_TIME_FORMAT);

  dateTimeOptions = {
    maxDate: this.currentTime.clone().endOf('day').valueOf(),
    dateFormat: 'Y-m-d H:i:S',
    enableTime: true,
    enableSeconds: true,
    time_24hr: true,
  };

  timestampOption = 'last_30_minutes';
  timestampOptions = TIME_STAMP_OPTIONS_CUSTOM.map(option => ({
    ...option,
    name: this.translateService.get(option.type),
  }));

  operationTypes = WRITABLE_ACTIONS.map(el => el + '').concat('replace');

  userName = '_audit_all';
  operationType = '_audit_all';
  resourceName = '';
  resourceType = '_audit_all';
  actionsConfigView = {
    diffMode: false,
    clear: false,
    recover: false,
    import: false,
    copy: true,
    find: true,
    export: true,
  };

  auditDetail = {};
  expandedAudits: { [key: string]: boolean } = {};
  pagination = {
    count: 0,
    size: 20,
    current: 1,
    pages: 0,
  };

  loading: boolean;
  reason = Reason;

  updated$ = new BehaviorSubject(null);
  fetchAudits$ = new BehaviorSubject(null);
  fetchAll$ = combineLatest([
    this.updated$.pipe(startWith(null as string)),
    timer(0, 60000).pipe(
      filter(
        () =>
          this.timestampOption !== 'custom_time_range' &&
          !(this.advancedForm && this.advancedForm.dirty),
      ),
    ),
  ]).pipe(
    tap(() => {
      if (this.timestampOption !== 'custom_time_range') {
        this.resetTimeRange();
      }
    }),
    map(() => ({
      start_time: this.dateValue(this.startTime),
      end_time: this.dateValue(this.endTime),
    })),
  );

  usernames$ = this.fetchAll$.pipe(
    switchMap(time =>
      this.auditApi.getAuditTypes({
        field: 'user_name',
        ...time,
      }),
    ),
    map(res => res.Items && res.Items.filter(item => item !== '')),
    skipError(),
  );

  resourceTypes$ = this.fetchAll$.pipe(
    switchMap(time =>
      this.auditApi.getAuditTypes({
        field: 'resource_name',
        ...time,
      }),
    ),
    map(res => res.Items && res.Items.filter(item => item !== '')),
    skipError(),
  );

  audits$ = combineLatest([
    this.fetchAll$,
    this.fetchAudits$.pipe(startWith(null as string)),
  ]).pipe(
    switchMap(([time]) => {
      const startTime = time.start_time;
      const endTime = time.end_time;
      if (!startTime || !endTime) {
        this.auiNotificationService.warning(
          this.translateService.get('log_query_timerange_required'),
        );
        return EMPTY;
      }
      if (
        this.timestampOption === 'custom_time_range' &&
        startTime >= endTime
      ) {
        this.auiNotificationService.warning(
          this.translateService.get('log_query_timerange_warning'),
        );
        return EMPTY;
      }
      this.loading = true;
      return this.auditApi.getAudits({
        start_time: String(startTime),
        end_time: String(endTime),
        pageno: this.pagination.current,
        size: this.pagination.size,
        user_name: this.getParam(this.userName),
        operation_type: this.getParam(this.operationType),
        resource_name: this.resourceName,
        resource_type: this.getParam(this.resourceType),
      });
    }),
    map(audits => {
      this.pagination.count =
        audits.total_items > 10000 ? 10000 : audits.total_items;
      this.pagination.pages = Math.ceil(
        this.pagination.count / this.pagination.size,
      );
      return audits.Items;
    }),
    tap(() => {
      this.loading = false;
    }),
    skipError(),
  );

  constructor(
    public cdr: ChangeDetectorRef,
    private readonly auiNotificationService: NotificationService,
    private readonly translateService: TranslateService,
    private readonly dialogService: DialogService,
    private readonly auditApi: AuditApi,
    private readonly advanceApi: AdvanceApi,
  ) {}

  currentPageChange(_currentPage: number) {
    this.fetchAudits$.next(null);
  }

  pageSizeChange(size: number) {
    if (Math.ceil(this.pagination.count / size) < this.pagination.pages) {
      this.pagination.current = 1;
    }
    this.fetchAudits$.next(null);
  }

  onTimestampChanged(type: string) {
    this.timestampOption = type;
    this.pagination.current = 1;
    this.updated$.next(null);
  }

  showDetailTemplate(audit: Audit, template: TemplateRef<any>) {
    this.auditDetail = audit;
    this.dialogService.open(template, {
      size: DialogSize.Big,
    });
  }

  refetch() {
    this.updated$.next(null);
  }

  onStartTimeSelect(event: string) {
    this.pagination.current = 1;
    this.startTime = event;
    this.updated$.next(null);
  }

  onEndTimeSelect(event: string) {
    this.pagination.current = 1;
    this.endTime = event;
    this.updated$.next(null);
  }

  clickRow(audit: Audit) {
    this.expandedAudits[audit.AuditID] = !this.expandedAudits[audit.AuditID];
  }

  trackById(_index: number, audit: Audit) {
    return audit.AuditID;
  }

  private getParam(param: string) {
    return param === '_audit_all' ? '' : param;
  }

  private resetTimeRange() {
    const timeRange = TIME_STAMP_OPTIONS_CUSTOM.find(
      option => option.type === this.timestampOption,
    );
    this.endTime = dayjs().format(DATE_TIME_FORMAT);
    this.startTime = dayjs()
      .startOf('second')
      .subtract(timeRange.offset, 'ms')
      .format(DATE_TIME_FORMAT);
  }

  private dateValue(date: string) {
    return dayjs(date).valueOf() / 1000;
  }
}
