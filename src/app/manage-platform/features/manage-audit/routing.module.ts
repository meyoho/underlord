import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ManageAuditListComponent } from 'app/manage-platform/features/manage-audit/component';

const routes: Routes = [
  {
    path: '',
    component: ManageAuditListComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManageAuditRoutingModule {}
