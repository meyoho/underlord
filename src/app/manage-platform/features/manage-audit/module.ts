import { CodeEditorIntl } from '@alauda/code-editor';
import { NgModule } from '@angular/core';
import { MonacoProviderService } from 'ng-monaco-editor';

import { AuditApi } from 'app/api/audit/api';
import { ManageAuditListComponent } from 'app/manage-platform/features/manage-audit/component';
import { ManageAuditRoutingModule } from 'app/manage-platform/features/manage-audit/routing.module';
import { CustomCodeEditorIntlService } from 'app/services/code-editor-intl.service';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  imports: [SharedModule, ManageAuditRoutingModule],
  declarations: [ManageAuditListComponent],
  providers: [
    AuditApi,
    {
      provide: CodeEditorIntl,
      useClass: CustomCodeEditorIntlService,
    },
  ],
})
export class ManageAuditModule {
  constructor(monacoProvider: MonacoProviderService) {
    monacoProvider.initMonaco();
  }
}
