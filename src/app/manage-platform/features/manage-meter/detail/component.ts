import {
  API_GATEWAY,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  KubernetesResourceList,
  Reason,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import dayjs from 'dayjs';
import { BehaviorSubject, Subject, combineLatest, of } from 'rxjs';
import {
  catchError,
  debounceTime,
  filter,
  finalize,
  map,
  pluck,
  skipWhile,
  switchMap,
  tap,
} from 'rxjs/operators';

import { AdvanceApi } from 'app/api/advance/api';
import { MeterQueryParams, Namespace, Report, TKECluster } from 'app/typings';
import { MeterReportMeta, RESOURCE_TYPES } from 'app/utils';

import {
  TIME_FORMAT_DAY,
  TYPES,
  endOfDay,
  endOfMonth,
  formatReportNameTime,
  getEndDayTime,
  getStartDayTime,
  startOfMonth,
} from '../util';

const ALL_CLUSTER_SYMBOL = Symbol('All Clusters');
const ALL_NAMESPACE_SYMBOL = Symbol('All Namespaces');

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MeterDetailComponent {
  refetch$ = new BehaviorSubject(null);
  loading$ = new Subject();
  exporting$ = new Subject();
  project$ = new BehaviorSubject(null);
  isDeployed$ = this.advanceApi.getDiagnoseInfo().pipe(publishRef());
  isMeterReportDeployed$ = this.advanceApi
    .getMeterReportInfo()
    .pipe(publishRef());

  cluster$: BehaviorSubject<
    string | typeof ALL_CLUSTER_SYMBOL
  > = new BehaviorSubject(ALL_CLUSTER_SYMBOL);

  namespace$: BehaviorSubject<
    string | typeof ALL_NAMESPACE_SYMBOL
  > = new BehaviorSubject(ALL_NAMESPACE_SYMBOL);

  reason = Reason;
  project = '';
  cluster = '';
  namespace = '';
  type: string;
  types = TYPES.slice(0, 2);
  allValCpu: number;
  allValMemory: number;
  currentTime = dayjs();
  startTime = startOfMonth(this.currentTime).format(TIME_FORMAT_DAY);
  endTime = this.currentTime.clone().format(TIME_FORMAT_DAY);
  ALL_CLUSTER_SYMBOL = ALL_CLUSTER_SYMBOL;
  ALL_NAMESPACE_SYMBOL = ALL_NAMESPACE_SYMBOL;

  dateTimeOptions = {
    maxDate: endOfDay(this.currentTime).valueOf(),
    minDate: this.currentTime.clone().subtract(18, 'month').valueOf(),
  };

  projects$ = this.k8sApi
    .getGlobalResourceList({
      type: RESOURCE_TYPES.PROJECT,
    })
    .pipe(
      pluck('items'),
      tap(projects => {
        if (!this.project && projects.length) {
          this.project$.next(projects[0].metadata.name);
        }
      }),
    );

  clusters$ = this.k8sApi
    .getGlobalResourceList<TKECluster>({
      type: RESOURCE_TYPES.TKE_CLUSTER,
    })
    .pipe(pluck('items'));

  namespaces$ = combineLatest([this.project$, this.cluster$]).pipe(
    filter(
      ([project, cluster]) => project && cluster && typeof cluster !== 'symbol',
    ),
    switchMap(([project, cluster]) =>
      this.httpClient
        .get<KubernetesResourceList<Namespace>>(
          `${API_GATEWAY}/auth/v1/projects/${project}/clusters/${String(
            cluster,
          )}/namespaces`,
        )
        .pipe(
          catchError(() => {
            this.namespace$.next(ALL_NAMESPACE_SYMBOL);
            return of({
              items: [] as Namespace[],
            });
          }),
          pluck('items'),
        ),
    ),
  );

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.REPORT,
    action: [K8sResourceAction.CREATE],
  });

  list$ = combineLatest([
    this.project$,
    this.cluster$,
    this.namespace$,
    this.refetch$,
  ]).pipe(
    skipWhile(([project]) => !project),
    debounceTime(250),
    tap(([project, cluster, namespace]) => {
      this.loading$.next(true);
      this.project = project;
      this.cluster = typeof cluster === 'symbol' ? '' : cluster;
      this.namespace = typeof namespace === 'symbol' ? '' : namespace;
    }),
    switchMap(([project, cluster, namespace]) => {
      const params: MeterQueryParams = {
        type: this.type,
        groupBy: 'namespace',
        startTime: getStartDayTime(this.startTime),
        endTime: getEndDayTime(this.endTime),
        project,
        cluster: typeof cluster === 'symbol' ? '' : cluster,
        namespace: typeof namespace === 'symbol' ? '' : namespace,
        classifyBy: 'day',
      };
      return this.advanceApi.getMeterTotal(params).pipe(
        map(res => {
          if (res && res.results) {
            this.allValCpu = res.results
              .map(el => el.meter.cpu)
              .reduce((a, b) => a + b, 0);
            this.allValMemory = res.results
              .map(el => el.meter.memory)
              .reduce((a, b) => a + b, 0);
            res.results.sort((a, b) => b.date.localeCompare(a.date));
            return res.results;
          }
          this.allValCpu = 0;
          this.allValMemory = 0;
          return [];
        }),
        catchError(() => of([])),
      );
    }),
    finalize(() => {
      this.loading$.next(false);
    }),
    publishRef(),
  );

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly auiMessageService: MessageService,
    private readonly k8sApiService: K8sApiService,
    private readonly translateService: TranslateService,
    private readonly advanceApi: AdvanceApi,
    private readonly k8sApi: K8sApiService,
    private readonly httpClient: HttpClient,
    private readonly k8sPermission: K8sPermissionService,
  ) {
    this.initForm();
  }

  onProjectChange(project: string) {
    this.project$.next(project);
  }

  onClusterChange(cluster: string | typeof ALL_CLUSTER_SYMBOL) {
    this.namespace$.next(ALL_NAMESPACE_SYMBOL);
    this.cluster$.next(
      typeof cluster === 'symbol' ? ALL_CLUSTER_SYMBOL : cluster,
    );
  }

  onNamespaceChange(namespace: string | typeof ALL_NAMESPACE_SYMBOL) {
    this.namespace$.next(
      typeof namespace === 'symbol' ? ALL_NAMESPACE_SYMBOL : namespace,
    );
  }

  onStartTimeSelect(time: string) {
    this.startTime = time;
  }

  onEndTimeSelect(time: string) {
    this.endTime = time;
  }

  initForm() {
    const params = this.route.snapshot.queryParams;
    this.type = params.type || 'podUsage';
    if (params.project) {
      this.project = params.project;
      this.project$.next(this.project || null);
    }
    this.cluster = params.cluster || '';
    this.cluster$.next(this.cluster || ALL_CLUSTER_SYMBOL);
    this.namespace = params.namespace || '';
    this.namespace$.next(this.namespace || ALL_NAMESPACE_SYMBOL);
    if (params.startTime) {
      this.startTime = startOfMonth(dayjs(params.startTime)).format(
        TIME_FORMAT_DAY,
      );
      const endTime = endOfMonth(dayjs(params.startTime));
      if (dayjs().isBefore(endTime)) {
        this.endTime = endOfDay(dayjs()).format(TIME_FORMAT_DAY);
      } else {
        this.endTime = endTime.format(TIME_FORMAT_DAY);
      }
    } else {
      this.startTime = startOfMonth(this.currentTime).format(TIME_FORMAT_DAY);
      this.endTime = endOfDay(this.currentTime).format(TIME_FORMAT_DAY);
    }
  }

  refetch() {
    if (this.checkQueryDates()) {
      return;
    }
    this.refetch$.next(null);
  }

  reset() {
    this.initForm();
    this.refetch();
  }

  getTypeText() {
    return this.types.find(el => el.key === this.type).text;
  }

  getClusterText = (cluster: string | typeof ALL_CLUSTER_SYMBOL) => {
    return typeof cluster === 'symbol' || !cluster
      ? this.translateService.get('all_cluster')
      : cluster;
  };

  private checkQueryDates() {
    if (!this.startTime || !this.endTime) {
      this.auiMessageService.warning({
        content: this.translateService.get('log_query_timerange_required'),
      });
      return true;
    }
    if (this.startTime > this.endTime) {
      this.auiMessageService.warning(
        this.translateService.get('log_query_timerange_warning'),
      );
      return true;
    }
    if (
      dayjs(this.endTime).subtract(30, 'day').isAfter(dayjs(this.startTime))
    ) {
      this.auiMessageService.warning(
        this.translateService.get(
          'log_query_timerange_greater_than_31_warning',
        ),
      );
      return true;
    }
    return false;
  }

  export() {
    this.exporting$.next(true);
    this.k8sApiService
      .postGlobalResource<Report>({
        type: RESOURCE_TYPES.REPORT,
        resource: {
          kind: MeterReportMeta.kind,
          apiVersion: MeterReportMeta.apiVersion,
          metadata: {
            name: formatReportNameTime(dayjs()),
          },
          spec: {
            kind: 'detail',
            type: this.type,
            groupBy: 'namespace',
            projects: this.project ? [this.project] : [],
            namespaces: this.namespace ? [this.namespace] : [],
            clusters: this.cluster ? [this.cluster] : [],
            orderBy: '-cpu',
            startTime: getStartDayTime(this.startTime),
            endTime: getEndDayTime(this.endTime),
          },
        },
      })
      .pipe(
        finalize(() => {
          this.exporting$.next(false);
        }),
      )
      .subscribe(() => {
        this.router.navigate(['../export'], {
          relativeTo: this.route,
        });
      });
  }
}
