import { ObservableInput, publishRef } from '@alauda/common-snippet';
import { Sort } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import {
  BehaviorSubject,
  EMPTY,
  Observable,
  Subject,
  combineLatest,
} from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import { AdvanceApi } from 'app/api/advance/api';
import { MeterQueryParams } from 'app/typings';

import { getEndTime, getStartTime } from '../util';

interface MeterParams {
  startTime: string;
  endTime: string;
  type: string;
  groupBy: string;
  project: string;
}

const COMMON_COLUMNS = ['cpu', 'memory'] as const;

@Component({
  selector: 'alu-meter-report-detail',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MeterReportDetailComponent {
  @ObservableInput()
  @Input('queryParams')
  queryParams$: Observable<MeterParams>;

  sort$ = new BehaviorSubject<{
    active: string;
    direction: string;
  }>({
    active: 'cpu',
    direction: 'desc',
  });

  pageSize$ = new BehaviorSubject<string>('100');
  loading$ = new Subject();

  type: string;
  groupBy: string;
  next: string;
  pageSize: number;
  columns = ['name', 'cpu', 'memory'];
  list$ = combineLatest([this.queryParams$, this.pageSize$, this.sort$]).pipe(
    tap(() => {
      this.loading$.next(true);
    }),
    switchMap(
      ([{ type, groupBy, startTime, endTime, project }, pageSize, sort]) => {
        this.type = type;
        this.groupBy = groupBy;
        this.columns =
          groupBy === 'project'
            ? ['name', ...COMMON_COLUMNS]
            : ['name', 'cluster', 'project', ...COMMON_COLUMNS];
        if (type === 'podUsage' || type === 'podRequests') {
          this.columns.push('detail');
        }
        const params: MeterQueryParams = {
          type: type,
          groupBy: groupBy,
          startTime: getStartTime(startTime),
          endTime: getEndTime(endTime),
          page: '1',
          pageSize: pageSize,
          orderBy: (sort.direction === 'desc' ? '-' : '') + sort.active,
        };
        if (project) {
          params.project = project;
        }
        return this.advanceApi.getMeterSummary(params).pipe(
          map(res => {
            this.next = res.next;
            this.pageSize = res.page_size;
            return res.results;
          }),
          catchError(() => {
            return EMPTY;
          }),
        );
      },
    ),
    tap(() => {
      this.loading$.next(false);
    }),
    publishRef(),
  );

  constructor(private readonly advanceApi: AdvanceApi) {}

  sortData(sort: Sort) {
    this.sort$.next(sort);
  }

  loadMore() {
    this.pageSize$.next((this.pageSize + 100).toString());
  }
}
