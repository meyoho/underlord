import { Reason, publishRef } from '@alauda/common-snippet';
import { Component } from '@angular/core';
import dayjs from 'dayjs';

import { AdvanceApi } from 'app/api/advance/api';

import {
  TYPES,
  endOfDay,
  endOfLastMonth,
  formatLocalToUTC,
  startOfLastMonth,
  startOfLastThreeMonth,
  startOfMonth,
} from '../util';

@Component({
  templateUrl: 'template.html',
})
export class MeterSummaryComponent {
  currentTime = dayjs();
  rangeList = [
    {
      text: 'this_month',
      startTime: formatLocalToUTC(startOfMonth(this.currentTime)),
      endTime: formatLocalToUTC(endOfDay(this.currentTime)),
    },
    {
      text: 'last_month',
      startTime: formatLocalToUTC(startOfLastMonth(this.currentTime)),
      endTime: formatLocalToUTC(endOfLastMonth(this.currentTime)),
    },
    {
      text: 'last_three_month',
      startTime: formatLocalToUTC(startOfLastThreeMonth(this.currentTime)),
      endTime: formatLocalToUTC(endOfDay(this.currentTime)),
    },
  ];

  reason = Reason;
  types = TYPES;
  rangeSelected = this.rangeList[0];
  rangeSelectedText = 'this_month';
  isDeployed$ = this.advanceApi.getDiagnoseInfo().pipe(publishRef());

  constructor(private readonly advanceApi: AdvanceApi) {}

  queryTopN(range: { text: string; startTime: string; endTime: string }) {
    this.rangeSelected = range;
    this.rangeSelectedText = range.text;
  }
}
