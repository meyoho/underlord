import dayjs from 'dayjs';

enum Date {
  Month = 'month',
  Day = 'day',
}

export const TYPES = [
  { key: 'podUsage', text: 'pod_usage' },
  { key: 'podRequests', text: 'pod_requests' },
  { key: 'namespaceQuota', text: 'namespace_quota' },
  { key: 'projectQuota', text: 'project_quota' },
];

export function formatReportNameTime(time: dayjs.Dayjs) {
  return time.format('YYYYMMDDHHmmss');
}

export function formatLocalToUTC(time: dayjs.Dayjs) {
  return time.format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
}

export function getStartTime(time: string) {
  return formatLocalToUTC(dayjs(time).startOf(Date.Month));
}

export function getEndTime(time: string) {
  return formatLocalToUTC(dayjs(time).endOf(Date.Month));
}

export function getStartDayTime(time: string) {
  return formatLocalToUTC(dayjs(time).startOf(Date.Day));
}

export function getEndDayTime(time: string) {
  return formatLocalToUTC(dayjs(time).endOf(Date.Day));
}

export function startOfMonth(time: dayjs.Dayjs) {
  return time.clone().startOf(Date.Month);
}

export function endOfMonth(time: dayjs.Dayjs) {
  return time.clone().endOf(Date.Month);
}

export function endOfDay(time: dayjs.Dayjs) {
  return time.clone().endOf(Date.Day);
}

export function startOfLastMonth(time: dayjs.Dayjs) {
  return time.clone().subtract(1, Date.Month).startOf(Date.Month);
}

export function endOfLastMonth(time: dayjs.Dayjs) {
  return time.clone().subtract(1, Date.Month).endOf(Date.Month);
}

export function startOfLastThreeMonth(time: dayjs.Dayjs) {
  return time.clone().subtract(2, Date.Month).startOf(Date.Month);
}

export const TIME_FORMAT = 'YYYY-MM';
export const TIME_FORMAT_DAY = 'YYYY-MM-DD';
