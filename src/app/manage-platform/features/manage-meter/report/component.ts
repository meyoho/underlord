import {
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  Reason,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import dayjs from 'dayjs';
import monthSelectPlugin from 'flatpickr/dist/plugins/monthSelect/index';
import { BehaviorSubject, Subject, of } from 'rxjs';
import {
  catchError,
  finalize,
  map,
  pluck,
  switchMap,
  tap,
} from 'rxjs/operators';

import { AdvanceApi } from 'app/api/advance/api';
import { MeterQueryParams, Report } from 'app/typings';
import { MeterReportMeta, RESOURCE_TYPES } from 'app/utils';

import {
  TIME_FORMAT,
  TYPES,
  endOfDay,
  endOfLastMonth,
  formatReportNameTime,
  getEndTime,
  getStartTime,
  startOfLastMonth,
  startOfLastThreeMonth,
  startOfMonth,
} from '../util';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MeterReportComponent {
  refetch$ = new BehaviorSubject(null);
  loading$ = new Subject();
  exporting$ = new Subject();
  isDeployed$ = this.advanceApi.getDiagnoseInfo().pipe(publishRef());
  isMeterReportDeployed$ = this.advanceApi
    .getMeterReportInfo()
    .pipe(publishRef());

  reason = Reason;
  type: string;
  groupBy: string;
  types = TYPES;
  project = 'all';
  projects: string[];
  allValCpu: number;
  allValMemory: number;
  currentTime = dayjs();
  startTime = startOfMonth(this.currentTime).format(TIME_FORMAT);
  endTime = this.currentTime.clone().format(TIME_FORMAT);

  dateTimeOptions = {
    maxDate: endOfDay(this.currentTime).valueOf(),
    minDate: this.currentTime.clone().subtract(18, 'month').valueOf(),
    plugins: [
      monthSelectPlugin({
        shorthand: true,
        dateFormat: 'Y-m',
        altFormat: 'Y-m',
      }),
    ],
  };

  projects$ = this.k8sApi
    .getGlobalResourceList({
      type: RESOURCE_TYPES.PROJECT,
    })
    .pipe(pluck('items'));

  list$ = this.refetch$.pipe(
    tap(() => {
      this.loading$.next(true);
    }),
    switchMap(() => {
      const params: MeterQueryParams = {
        type: this.type,
        groupBy: this.groupBy,
        startTime: getStartTime(this.startTime),
        endTime: getEndTime(this.endTime),
      };
      if (this.project === 'custom' && this.projects) {
        params.project = this.projects.join(',');
      }
      return this.advanceApi.getMeterTotal(params).pipe(
        map(res => {
          if (res && res.results) {
            this.allValCpu = res.results
              .map(el => el.meter.cpu)
              .reduce((a, b) => a + b, 0);
            this.allValMemory = res.results
              .map(el => el.meter.memory)
              .reduce((a, b) => a + b, 0);
            res.results.sort((a, b) => b.date.localeCompare(a.date));
            return res.results;
          }
          this.allValCpu = 0;
          this.allValMemory = 0;
          return [];
        }),
        catchError(() => of([])),
      );
    }),
    finalize(() => {
      this.loading$.next(false);
    }),
    publishRef(),
  );

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.REPORT,
    action: [K8sResourceAction.CREATE],
  });

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly auiMessageService: MessageService,
    private readonly k8sApiService: K8sApiService,
    private readonly translateService: TranslateService,
    private readonly advanceApi: AdvanceApi,
    private readonly k8sApi: K8sApiService,
    private readonly k8sPermission: K8sPermissionService,
  ) {
    this.initForm();
  }

  onTypeChange(type: string) {
    if (type === 'projectQuota') {
      this.groupBy = 'project';
    }
    this.refetch();
  }

  onStartTimeSelect(time: string) {
    this.startTime = time;
  }

  onEndTimeSelect(time: string) {
    this.endTime = time;
  }

  initForm() {
    this.type = this.route.snapshot.queryParams.type || 'podUsage';
    this.groupBy = this.route.snapshot.queryParams.groupBy || 'project';
    const project = this.route.snapshot.queryParams.project;
    if (project) {
      this.project = 'custom';
      this.projects = [project];
    } else {
      this.project = 'all';
      this.projects = [];
    }
    switch (this.route.snapshot.queryParams.range) {
      case 'last_month':
        this.startTime = startOfLastMonth(this.currentTime).format(TIME_FORMAT);
        this.endTime = endOfLastMonth(this.currentTime).format(TIME_FORMAT);
        break;
      case 'last_three_month':
        this.startTime = startOfLastThreeMonth(this.currentTime).format(
          TIME_FORMAT,
        );
        this.endTime = endOfDay(this.currentTime).format(TIME_FORMAT);
        break;
      default:
        this.startTime = startOfMonth(this.currentTime).format(TIME_FORMAT);
        this.endTime = endOfDay(this.currentTime).format(TIME_FORMAT);
        break;
    }
  }

  refetch() {
    if (this.checkQueryDates()) {
      return;
    }
    this.refetch$.next(null);
  }

  reset() {
    this.initForm();
    this.refetch();
  }

  getTypeText() {
    return this.types.find(el => el.key === this.type).text;
  }

  getgroupByText() {
    return `meter_by_${this.groupBy}`;
  }

  private checkQueryDates() {
    if (!this.startTime || !this.endTime) {
      this.auiMessageService.warning({
        content: this.translateService.get('log_query_timerange_required'),
      });
      return true;
    }
    if (this.startTime > this.endTime) {
      this.auiMessageService.warning(
        this.translateService.get('log_query_timerange_warning'),
      );
      return true;
    }
    return false;
  }

  export() {
    this.exporting$.next(true);
    this.k8sApiService
      .postGlobalResource<Report>({
        type: RESOURCE_TYPES.REPORT,
        resource: {
          kind: MeterReportMeta.kind,
          apiVersion: MeterReportMeta.apiVersion,
          metadata: {
            name: formatReportNameTime(dayjs()),
          },
          spec: {
            kind: 'summary',
            type: this.type,
            groupBy: this.groupBy,
            projects:
              this.project === 'custom' && this.projects ? this.projects : [],
            orderBy: '-cpu',
            startTime: getStartTime(this.startTime),
            endTime: getEndTime(this.endTime),
          },
        },
      })
      .pipe(
        finalize(() => {
          this.exporting$.next(false);
        }),
      )
      .subscribe(() => {
        this.router.navigate(['../export'], {
          relativeTo: this.route,
        });
      });
  }
}
