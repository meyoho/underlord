import {
  API_GATEWAY,
  DATE_TIME_FORMAT,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  Reason,
  publishRef,
} from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import dayjs from 'dayjs';
import { get } from 'lodash-es';
import { map, tap } from 'rxjs/operators';

import { AdvanceApi } from 'app/api/advance/api';
import { Report } from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';

import { TYPES } from '../util';

const ALL_STATUS_SYMBOL = Symbol('All Status');
const transitionTimePath = ['status', 'conditions', '0', 'lastTransitionTime'];

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class MeterExportComponent {
  ALL_STATUS_SYMBOL = ALL_STATUS_SYMBOL;
  selectedStatus = '';
  loading: boolean;
  columns = ['file_name', 'type', 'status', 'description', 'time', 'export'];
  statusOptions = ['Completed', 'Processing', 'Pending', 'Failed'];

  reason = Reason;
  isDeployed$ = this.advanceApi.getMeterReportInfo().pipe(publishRef());
  list = new K8SResourceList<Report>({
    activatedRoute: this.activatedRoute,
    fetcher: this.fetchResources.bind(this),
  });

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.REPORT,
    action: [K8sResourceAction.CREATE],
  });

  constructor(
    private readonly advanceApi: AdvanceApi,
    private readonly http: HttpClient,
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sApiService: K8sApiService,
    private readonly k8sPermission: K8sPermissionService,
  ) {}

  fetchResources() {
    return this.k8sApiService
      .getGlobalResourceList<Report>({
        type: RESOURCE_TYPES.REPORT,
      })
      .pipe(
        map(report => ({
          ...report,
          items: report.items
            .map(item => ({
              ...item,
              status: item?.status?.phase
                ? item.status
                : {
                    phase: 'Pending',
                  },
            }))
            .filter(item => {
              const status = this.getActualStatus(this.selectedStatus);
              if (status) {
                return item.status?.phase === status;
              }
              return true;
            })
            .sort((a, b) => {
              const t1 = get(a, transitionTimePath);
              const t2 = get(b, transitionTimePath);
              return t1 < t2 ? 1 : t1 > t2 ? -1 : 0;
            }),
        })),
        tap(report => {
          if (report.items.some(item => item?.status?.phase === 'Processing')) {
            setTimeout(() => {
              this.list.reload();
            }, 5000);
          }
        }),
      );
  }

  onStatusChange(status: string) {
    this.selectedStatus = status;
    this.list.reload();
  }

  private getActualStatus(status: string | typeof ALL_STATUS_SYMBOL) {
    return status === ALL_STATUS_SYMBOL ? '' : status;
  }

  getTransitionTime(item: Report) {
    return get(item, transitionTimePath);
  }

  getMeterType(type: string) {
    return TYPES.find(t => t.key === type).text;
  }

  getDate(time: string) {
    return dayjs(time).format(DATE_TIME_FORMAT);
  }

  getYearMonth(time: string) {
    const date = time.split('T')[0].split('-');
    return date[0] + date[1];
  }

  getYearMonthDay(time: string) {
    const date = time.split('T')[0].split('-');
    return date[0] + date[1] + date[2];
  }

  download(report: Report) {
    this.http
      .get(API_GATEWAY + report.status.downloadLink, { responseType: 'blob' })
      .subscribe(res => {
        const element = document.createElement('a');
        element.href = URL.createObjectURL(res);
        element.download = report.status.downloadLink.split('/').pop();
        document.body.append(element);
        element.click();
      });
  }

  reExport(report: Report) {
    this.advanceApi
      .clearReportStatus(report, 'meter')
      .subscribe(() => this.list.reload());
  }
}
