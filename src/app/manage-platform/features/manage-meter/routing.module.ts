import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MeterDetailComponent } from 'app/manage-platform/features/manage-meter/detail/component';
import { MeterExportComponent } from 'app/manage-platform/features/manage-meter/report-export/component';
import { MeterReportComponent } from 'app/manage-platform/features/manage-meter/report/component';
import { MeterSummaryComponent } from 'app/manage-platform/features/manage-meter/summary/component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'summary',
  },
  {
    path: 'summary',
    component: MeterSummaryComponent,
  },
  {
    path: 'report',
    component: MeterReportComponent,
  },
  {
    path: 'detail',
    component: MeterDetailComponent,
  },
  {
    path: 'export',
    component: MeterExportComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManageMeterRoutingModule {}
