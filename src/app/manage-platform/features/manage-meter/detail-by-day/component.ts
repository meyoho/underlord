import { ObservableInput, publishRef } from '@alauda/common-snippet';
import { Sort } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import {
  BehaviorSubject,
  EMPTY,
  Observable,
  Subject,
  combineLatest,
} from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import { AdvanceApi } from 'app/api/advance/api';
import { MeterQueryParams } from 'app/typings';

import { getEndDayTime, getStartDayTime } from '../util';

interface MeterParams {
  date: string;
  type: string;
  groupBy: string;
  project: string;
  cluster: string;
  namespace: string;
}

@Component({
  selector: 'alu-meter-report-detail-by-day',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MeterReportDetailByDayComponent {
  @ObservableInput()
  @Input('queryParams')
  queryParams$: Observable<MeterParams>;

  sort$ = new BehaviorSubject<{
    active: string;
    direction: string;
  }>({
    active: 'cpu',
    direction: 'desc',
  });

  pageSize$ = new BehaviorSubject<string>('100');
  loading$ = new Subject();

  next: string;
  pageSize: number;
  columns = [
    'name',
    'namespace',
    'cluster',
    'project',
    'cpu',
    'memory',
    'start_time',
    'end_time',
  ];

  list$ = combineLatest([this.queryParams$, this.pageSize$, this.sort$]).pipe(
    tap(() => {
      this.loading$.next(true);
    }),
    switchMap(
      ([{ type, date, project, cluster, namespace }, pageSize, sort]) => {
        const params: MeterQueryParams = {
          type: type,
          groupBy: 'namespace',
          startTime: getStartDayTime(date),
          endTime: getEndDayTime(date),
          project,
          cluster,
          namespace,
          page: '1',
          pageSize: pageSize,
          orderBy: (sort.direction === 'desc' ? '-' : '') + sort.active,
        };
        return this.advanceApi.getMeterDetail(params).pipe(
          map(res => {
            this.next = res.next;
            this.pageSize = res.page_size;
            return res.results;
          }),
          catchError(() => {
            return EMPTY;
          }),
        );
      },
    ),
    tap(() => {
      this.loading$.next(false);
    }),
    publishRef(),
  );

  constructor(private readonly advanceApi: AdvanceApi) {}

  sortData(sort: Sort) {
    this.sort$.next(sort);
  }

  loadMore() {
    this.pageSize$.next((this.pageSize + 100).toString());
  }
}
