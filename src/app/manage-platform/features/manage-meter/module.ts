import { NgModule } from '@angular/core';

import { AdvanceApi } from 'app/api/advance/api';
import { SharedModule } from 'app/shared/shared.module';

import { MeterReportDetailByDayComponent } from './detail-by-day/component';
import { MeterDetailComponent } from './detail/component';
import { MeterReportDetailComponent } from './report-detail/component';
import { MeterExportComponent } from './report-export/component';
import { MeterReportComponent } from './report/component';
import { ManageMeterRoutingModule } from './routing.module';
import { MeterSummaryComponent } from './summary/component';
import { MeterTopComponent } from './top/component';

@NgModule({
  imports: [SharedModule, ManageMeterRoutingModule],
  declarations: [
    MeterDetailComponent,
    MeterReportDetailByDayComponent,
    MeterExportComponent,
    MeterReportComponent,
    MeterReportDetailComponent,
    MeterSummaryComponent,
    MeterTopComponent,
  ],
  providers: [AdvanceApi],
})
export class ManageMeterModule {}
