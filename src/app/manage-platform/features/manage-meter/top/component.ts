import { ObservableInput, publishRef } from '@alauda/common-snippet';
import { Component, Input } from '@angular/core';
import { get } from 'lodash-es';
import { BehaviorSubject, Observable, combineLatest } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { AdvanceApi } from 'app/api/advance/api';

const COMMON_COLUMNS = ['usage_amount', 'total_proportion'] as const;

@Component({
  selector: 'alu-meter-top',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class MeterTopComponent {
  @ObservableInput()
  @Input('type')
  type$: Observable<string>;

  @ObservableInput()
  @Input('range')
  range$: Observable<{
    text: string;
    startTime: string;
    endTime: string;
  }>;

  @Input()
  orderBy: string;

  columns = ['project_name', 'usage_amount', 'total_proportion'];
  groupBy$ = new BehaviorSubject<string>('project');
  allVal: number;
  list$ = combineLatest([this.range$, this.type$, this.groupBy$]).pipe(
    switchMap(([range, type, groupBy]) => {
      this.columns =
        groupBy === 'project'
          ? ['project_name', ...COMMON_COLUMNS]
          : ['namespace_name', 'cluster', 'project', ...COMMON_COLUMNS];
      return this.advanceApi
        .getMeterTopN({
          type,
          groupBy,
          top: '5',
          startTime: range.startTime,
          endTime: range.endTime,
          orderBy: '-' + this.orderBy,
        })
        .pipe(
          map(res => {
            if (res) {
              const path =
                this.orderBy === 'cpu' ? 'meter.cpu' : 'meter.memory';
              let maxVal = Math.max.apply(
                null,
                res.topN.map(el => get(el, path)),
              );
              maxVal = Math.max(maxVal, get(res.remain, path));
              this.allVal =
                res.topN.reduce((a, b) => a + get(b, path), 0) +
                get(res.remain, path);
              return res.topN
                .map(el => ({
                  projectDisplayName: el.project.displayName,
                  namespaceDisplayName: el.namespace.displayName,
                  cluster: el.cluster.name,
                  project: el.project.name,
                  namespace: el.namespace.name,
                  usage: get(el, path),
                  percentMax: get(el, path) / maxVal,
                  percentAll: get(el, path) / this.allVal || 0 * 100,
                  last: false,
                }))
                .concat({
                  projectDisplayName: '',
                  namespaceDisplayName: '',
                  cluster: '',
                  project: '',
                  namespace: '',
                  usage: get(res.remain, path),
                  percentMax: get(res.remain, path) / maxVal,
                  percentAll: get(res.remain, path) / this.allVal || 0 * 100,
                  last: true,
                });
            } else {
              this.allVal = 0;
              return [];
            }
          }),
          catchError(() => {
            this.allVal = 0;
            return [];
          }),
        );
    }),
    publishRef(),
  );

  constructor(private readonly advanceApi: AdvanceApi) {}
}
