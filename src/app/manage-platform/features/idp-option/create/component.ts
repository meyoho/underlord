import { DESCRIPTION, K8sUtilService } from '@alauda/common-snippet';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Idp } from 'app/typings';

@Component({
  template:
    '<alu-idp-option-form [type]="type" [ngModel]="idp"></alu-idp-option-form>',
})
export class CreateIDPOptionComponent implements OnInit {
  type: string;
  idp: Idp;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  ngOnInit() {
    const params = this.route.snapshot.params;
    this.type = params.type;
    this.idp = this.getDefaultIdp(this.type);
  }

  getDefaultIdp(type: string): Idp {
    return {
      apiVersion: 'dex.coreos.com/v1',
      kind: 'Connector',
      id: '',
      name: '',
      type,
      config: '',
      metadata: {
        name: '',
        annotations: {
          [this.k8sUtil.normalizeType(DESCRIPTION)]: '',
        },
      },
    };
  }
}
