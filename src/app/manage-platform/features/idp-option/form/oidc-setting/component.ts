import { Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { OidcConfig } from 'app/typings';

@Component({
  selector: 'alu-oidc-setting-form',
  templateUrl: 'template.html',
})
export class OidcSettingFormComponent extends BaseResourceFormGroupComponent<
  string,
  OidcConfig
> {
  constructor(public injector: Injector) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      issuer: ['', Validators.required],
      clientID: ['', Validators.required],
      clientSecret: ['', Validators.required],
      redirectURI: ['', Validators.required],
    });
  }

  getDefaultFormModel() {
    return {} as OidcConfig;
  }
}
