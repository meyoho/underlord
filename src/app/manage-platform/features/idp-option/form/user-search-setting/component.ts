import { Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { cloneDeep } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { IdpApi } from 'app/api/idp/api';
import { LdapUserSearch } from 'app/typings';

interface UserSearchFormModel extends LdapUserSearch {
  objectType?: string;
}

@Component({
  selector: 'alu-user-search-setting-form',
  templateUrl: 'template.html',
})
export class UserSearchSettingFormComponent extends BaseResourceFormGroupComponent<
  LdapUserSearch,
  UserSearchFormModel
> {
  constructor(public injector: Injector, private readonly idpApi: IdpApi) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      objectType: ['', Validators.required],
      baseDN: ['', Validators.required],
      username: ['', Validators.required],
      filter: [''],
      scope: [''],
      idAttr: ['', Validators.required],
      emailAttr: ['', Validators.required],
      nameAttr: ['', Validators.required],
    });
  }

  getDefaultFormModel() {
    return {
      objectType: 'inetOrgPerson',
      baseDN: 'dc=example,dc=org',
      username: 'mail',
      filter: '',
      scope: '',
      idAttr: 'uid',
      emailAttr: 'mail',
      nameAttr: 'cn',
    };
  }

  adaptResourceModel(resource: LdapUserSearch) {
    if (resource) {
      const formModel = cloneDeep(resource) as UserSearchFormModel;
      const { objectType, filter } = this.idpApi.getIdpFilter(resource.filter);
      formModel.filter = filter;
      formModel.objectType = objectType;
      return formModel;
    }
    return resource;
  }

  adaptFormModel(form: UserSearchFormModel) {
    if (form) {
      let filter = '';
      const resource = cloneDeep(form);
      if (form.objectType) {
        filter = `(objectClass=${form.objectType})`;
      }
      if (form.filter) {
        filter = filter ? `(&${filter}${form.filter})` : form.filter;
      }
      resource.filter = filter;
      delete resource.objectType;
      return resource;
    }
    return form;
  }
}
