import { DESCRIPTION, K8sUtilService } from '@alauda/common-snippet';
import { Location } from '@angular/common';
import { Component, Injector, Input, OnInit } from '@angular/core';
import { NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { IdpApi } from 'app/api/idp/api';
import { K8S_RESOURCE_NAME_BASE } from 'app/utils';

@Component({
  selector: 'alu-idp-option-form',
  styleUrls: ['style.scss'],
  templateUrl: 'template.html',
})
export class IDPOptionFormComponent extends BaseResourceFormGroupComponent
  implements OnInit {
  @Input()
  update: boolean;

  @Input()
  type: string;

  namePattern = K8S_RESOURCE_NAME_BASE;
  description = this.k8sUtil.normalizeType(DESCRIPTION);

  constructor(
    public injector: Injector,
    private readonly router: Router,
    private readonly location: Location,
    private readonly k8sUtil: K8sUtilService,
    private readonly idpApi: IdpApi,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    const idControl = this.form.get('id');
    if (idControl) {
      idControl.valueChanges.subscribe(value => {
        this.form.get('metadata.name').setValue(value || '');
      });
    }
  }

  createForm() {
    const annotations = this.fb.group({
      [this.description]: [''],
    });
    return this.fb.group({
      metadata: this.fb.group({
        annotations,
        name: [''],
      }),
      config: null,
      idpValidation: null,
      id: [
        '',
        [
          Validators.required,
          Validators.pattern(K8S_RESOURCE_NAME_BASE.pattern),
        ],
      ],
      name: ['', Validators.required],
      type: [''],
    });
  }

  getDefaultFormModel() {
    return {};
  }

  submit(form: NgForm) {
    form.onSubmit(null);
    if (form.invalid) {
      return;
    }
    const method = this.update ? 'updateIdp' : 'createIdp';
    this.idpApi[method](form.value).subscribe(() => {
      this.router.navigate(['manage-platform', 'idp', 'detail', form.value.id]);
    });
  }

  goBack() {
    this.location.back();
  }
}
