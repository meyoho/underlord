import { Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

@Component({
  selector: 'alu-idp-server-setting-check-form',
  templateUrl: 'template.html',
})
export class IdpServerSettingCheckFormComponent extends BaseResourceFormGroupComponent {
  check = true;
  constructor(public injector: Injector) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  getDefaultFormModel() {
    return { username: '', password: '' };
  }
}
