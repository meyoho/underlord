import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { LdapConfig } from 'app/typings';
import { b64EncodeUnicode } from 'app/utils';

@Component({
  selector: 'alu-ldap-setting-form',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class LdapSettingFormComponent
  extends BaseResourceFormGroupComponent<LdapConfig>
  implements OnInit {
  check = false;
  constructor(public injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.form.get('startTLS').valueChanges.subscribe(value => {
      this.applyCaFormItem(value);
    });
  }

  applyCaFormItem(value: boolean) {
    if (!this.form) {
      return;
    }
    if (value) {
      this.form.get('rootCAData').enable();
    } else {
      this.form.get('rootCAData').disable();
    }
  }

  createForm() {
    return this.fb.group({
      host: ['', Validators.required],
      startTLS: [''],
      rootCAData: ['', Validators.required],
      bindDN: ['', Validators.required],
      bindPW: ['', Validators.required],
      usernamePrompt: [''],
      userSearch: null,
      groupSearch: null,
    });
  }

  getDefaultFormModel() {
    return {
      startTLS: false,
    } as LdapConfig;
  }

  adaptFormModel(formModel: LdapConfig) {
    if (formModel) {
      if (!this.check) {
        delete formModel.groupSearch;
      }
      if (formModel.startTLS) {
        delete formModel.insecureNoSSL;
        delete formModel.insecureSkipVerify;
      } else {
        formModel.insecureNoSSL = true;
        formModel.insecureSkipVerify = true;
      }
      if (formModel.rootCAData) {
        formModel.rootCAData = b64EncodeUnicode(formModel.rootCAData);
      }
    }
    return formModel;
  }

  adaptResourceModel(resource: LdapConfig) {
    if (resource) {
      this.check = !!resource.groupSearch;
      resource.rootCAData = '';
      this.applyCaFormItem(!!resource.startTLS);
    } else {
      this.applyCaFormItem(false);
    }
    return resource;
  }
}
