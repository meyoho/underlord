import { Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { cloneDeep } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { IdpApi } from 'app/api/idp/api';
import { LdapGroupSearch } from 'app/typings';

interface GroupSearchFormModel extends LdapGroupSearch {
  objectType?: string;
}

@Component({
  selector: 'alu-group-search-setting-form',
  templateUrl: 'template.html',
})
export class GroupSearchSettingFormComponent extends BaseResourceFormGroupComponent<
  LdapGroupSearch,
  GroupSearchFormModel
> {
  constructor(public injector: Injector, private readonly idpApi: IdpApi) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      objectType: ['', Validators.required],
      baseDN: ['', Validators.required],
      userAttr: ['', Validators.required],
      groupAttr: ['', Validators.required],
      filter: [''],
      scope: [''],
      nameAttr: ['', Validators.required],
    });
  }

  getDefaultFormModel() {
    return {
      objectType: 'posixGroup',
      baseDN: 'dc=example,dc=org',
      filter: '',
      scope: '',
      groupAttr: 'memberuid',
      nameAttr: 'cn',
      userAttr: 'uid',
    };
  }

  adaptResourceModel(resource: LdapGroupSearch) {
    if (resource) {
      const formModel = cloneDeep(resource) as GroupSearchFormModel;
      const { objectType, filter } = this.idpApi.getIdpFilter(resource.filter);
      formModel.filter = filter;
      formModel.objectType = objectType;
      return formModel;
    }
    return resource;
  }

  adaptFormModel(form: GroupSearchFormModel) {
    if (form) {
      let filter = '';
      const resource = cloneDeep(form);
      if (form.objectType) {
        filter = `(objectClass=${form.objectType})`;
      }
      if (form.filter) {
        filter = filter ? `(&${filter}${form.filter})` : form.filter;
      }
      resource.filter = filter;
      delete resource.objectType;
      return resource;
    }
    return form;
  }
}
