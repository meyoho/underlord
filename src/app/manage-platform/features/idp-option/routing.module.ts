import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateIDPOptionComponent } from 'app/manage-platform/features/idp-option/create/component';
import { IdpDetailComponent } from 'app/manage-platform/features/idp-option/detail/component';
import { IDPOptionListComponent } from 'app/manage-platform/features/idp-option/list/component';
import { UpdateIDPOptionComponent } from 'app/manage-platform/features/idp-option/update/component';

const routes: Routes = [
  {
    path: '',
    component: IDPOptionListComponent,
  },
  {
    path: 'create/:type',
    component: CreateIDPOptionComponent,
  },
  {
    path: 'update/:name',
    component: UpdateIDPOptionComponent,
  },
  {
    path: 'detail/:name',
    component: IdpDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IDPOptionRoutingModule {}
