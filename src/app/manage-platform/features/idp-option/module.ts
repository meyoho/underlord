import { NgModule } from '@angular/core';

import { CreateIDPOptionComponent } from 'app/manage-platform/features/idp-option/create/component';
import { IdpDetailComponent } from 'app/manage-platform/features/idp-option/detail/component';
import { IDPOptionFormComponent } from 'app/manage-platform/features/idp-option/form/component';
import { GroupSearchSettingFormComponent } from 'app/manage-platform/features/idp-option/form/group-search-setting/component';
import { IdpServerSettingCheckFormComponent } from 'app/manage-platform/features/idp-option/form/idp-server-setting-check/component';
import { LdapSettingFormComponent } from 'app/manage-platform/features/idp-option/form/ldap-setting/component';
import { OidcSettingFormComponent } from 'app/manage-platform/features/idp-option/form/oidc-setting/component';
import { UserSearchSettingFormComponent } from 'app/manage-platform/features/idp-option/form/user-search-setting/component';
import { IDPOptionListComponent } from 'app/manage-platform/features/idp-option/list/component';
import { IDPOptionRoutingModule } from 'app/manage-platform/features/idp-option/routing.module';
import { UpdateIDPOptionComponent } from 'app/manage-platform/features/idp-option/update/component';
import { SharedModule } from 'app/shared/shared.module';

import { DeleteIDPDialogComponent } from './delete/component';

@NgModule({
  imports: [SharedModule, IDPOptionRoutingModule],
  declarations: [
    IDPOptionListComponent,
    CreateIDPOptionComponent,
    IDPOptionFormComponent,
    UpdateIDPOptionComponent,
    GroupSearchSettingFormComponent,
    IdpServerSettingCheckFormComponent,
    LdapSettingFormComponent,
    OidcSettingFormComponent,
    UserSearchSettingFormComponent,
    IdpDetailComponent,
    DeleteIDPDialogComponent,
  ],
  entryComponents: [DeleteIDPDialogComponent],
})
export class IDPOptionModule {}
