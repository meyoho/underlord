import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IdpApi } from 'app/api/idp/api';
import { Idp } from 'app/typings';
import { b64DecodeUnicode } from 'app/utils';

@Component({
  template:
    '<alu-idp-option-form [update]="true" [type]="type" [ngModel]="idp$ | async"></alu-idp-option-form>',
})
export class UpdateIDPOptionComponent implements OnInit {
  type: string;
  idp$: Observable<Idp>;
  constructor(
    private readonly idpApi: IdpApi,
    private readonly activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    this.idp$ = this.idpApi.getIdp(params.name).pipe(
      map(res => {
        try {
          const config = JSON.parse(b64DecodeUnicode(res.config));
          res.config = config;
          this.type = res.type;
        } catch (e) {}
        return res;
      }),
    );
  }
}
