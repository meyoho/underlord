import {
  AsyncDataLoader,
  K8sPermissionService,
  K8sResourceAction,
  StringMap,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { IdpApi } from 'app/api/idp/api';
import { Idp } from 'app/typings';
import { RESOURCE_TYPES, b64DecodeUnicode } from 'app/utils';

import { DeleteIDPDialogComponent } from '../delete/component';
@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class IdpDetailComponent {
  name: string;
  idp$: Observable<Idp>;
  context = this;

  dataLoader = new AsyncDataLoader<Idp>({
    params$: this.activatedRoute.params.pipe(
      tap(params => {
        this.name = params.name;
      }),
    ),
    fetcher: params => this.fetcher(params),
  });

  permissions$ = this.k8sPermissionService.isAllowed({
    type: RESOURCE_TYPES.CONNECTOR,
    action: [K8sResourceAction.UPDATE, K8sResourceAction.DELETE],
  });

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly translateService: TranslateService,
    private readonly dialogService: DialogService,
    private readonly router: Router,
    private readonly idpApi: IdpApi,
    private readonly k8sPermissionService: K8sPermissionService,
  ) {}

  fetcher(params: StringMap) {
    return this.idpApi.getIdp(params.name).pipe(
      map(res => {
        try {
          const config = JSON.parse(b64DecodeUnicode(res.config));
          res.config = config;
        } catch (e) {}
        return res;
      }),
    );
  }

  confirmDeleteIdp() {
    const ref = this.dialogService.open(DeleteIDPDialogComponent, {
      data: {
        name: this.name,
      },
    });

    ref.componentInstance.close.subscribe((res: boolean) => {
      if (res) {
        this.backToList();
      }
      ref.close();
    });
  }

  backToList() {
    this.router.navigate(['../..'], {
      relativeTo: this.activatedRoute,
      replaceUrl: true,
    });
  }

  async updateIdp(type: string) {
    let content;
    switch (type) {
      case 'oidc':
        content = 'update_oidc_content';
        break;
      case 'ldap':
        content = 'update_ldap_content';
        break;
    }
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('update_idp_title', {
          name: this.name,
        }),
        content: this.translateService.get(content),
        confirmText: this.translateService.get('update'),
        cancelText: this.translateService.get('cancel'),
      });
      this.router.navigate(['manage-platform', 'idp', 'update', this.name]);
    } catch (e) {}
  }

  getFilter = (filter: string, attr: string) => {
    const obj: StringMap = this.idpApi.getIdpFilter(filter);
    return obj[attr] || '';
  };
}
