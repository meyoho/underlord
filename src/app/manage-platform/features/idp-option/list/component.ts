import {
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  ResourceListParams,
  TranslateService,
} from '@alauda/common-snippet';
import { MessageService, TooltipInterface } from '@alauda/ui';
import {
  Component,
  ElementRef,
  OnInit,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IdpApi } from 'app/api/idp/api';
import { CronJob, Idp } from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';

interface TooltipEl extends TooltipInterface {
  toggleTooltip?: Function;
  disposeTooltip?: Function;
  componentIns?: {
    elRef: ElementRef;
  };
}

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class IDPOptionListComponent implements OnInit {
  columns = ['name', 'type', 'description', 'create_time'];
  list = new K8SResourceList<Idp>({
    activatedRoute: this.activatedRoute,
    fetcher: this.fetchResources.bind(this),
  });

  unlistenBody: () => void;

  @ViewChild('tooltipEl') tooltipEl: TooltipEl;
  @ViewChild('buttonRef') buttonRef: { el: ElementRef };

  permissions$ = this.k8sPermissionService.isAllowed({
    type: RESOURCE_TYPES.CONNECTOR,
    action: K8sResourceAction.CREATE,
  });

  syncLdappermissions$ = this.k8sPermissionService.isAllowed({
    type: RESOURCE_TYPES.CRON_JOB,
    action: K8sResourceAction.UPDATE,
    name: 'syncldap',
  });

  cronJob: CronJob;
  submitting = false;
  auto = false;
  interval = '';
  intervalOptions = [
    {
      translate: {
        zh: '15 分钟',
        en: 'fifteen minutes',
      },
      value: '*/15 * * * *',
    },
    {
      translate: {
        zh: '30 分钟',
        en: 'thirty minutes',
      },
      value: '*/30 * * * *',
    },
    {
      translate: {
        zh: '1 小时',
        en: 'one hour',
      },
      value: '0 * * * *',
    },
    {
      translate: {
        zh: '2 小时',
        en: 'tow hours',
      },
      value: '0 */2 * * *',
    },
    {
      translate: {
        zh: '6 小时',
        en: 'six hours',
      },
      value: '0 */6 * * *',
    },
    {
      translate: {
        zh: '12 小时',
        en: 'twelve hours',
      },
      value: '0 */12 * * *',
    },
    {
      translate: {
        zh: '1 天',
        en: 'one day',
      },
      value: '0 0 * * *',
    },
    {
      translate: {
        zh: '2 天',
        en: 'two days',
      },
      value: '0 0 */2 * *',
    },
    {
      translate: {
        zh: '3 天',
        en: 'three days',
      },
      value: '0 0 */3 * *',
    },
    {
      translate: {
        zh: '7 天',
        en: 'seven days',
      },
      value: '0 0 */7 * *',
    },
  ];

  constructor(
    private readonly idpApi: IdpApi,
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sPermissionService: K8sPermissionService,
    private readonly renderer: Renderer2,
    private readonly k8sApi: K8sApiService,
    private readonly message: MessageService,
    private readonly translateService: TranslateService,
  ) {}

  ngOnInit() {
    this.k8sApi
      .getGlobalResource<CronJob>({
        type: RESOURCE_TYPES.CRON_JOB,
        name: 'syncldap',
        namespaced: true,
      })
      .subscribe(res => {
        this.cronJob = res;
      });
  }

  fetchResources(params: ResourceListParams) {
    return this.idpApi.getIdps(params);
  }

  toggleTooltip() {
    this.tooltipEl.toggleTooltip();
    if (this.cronJob) {
      this.auto = !this.cronJob.spec.suspend;
      this.interval = this.cronJob.spec.schedule;
    }
    if (!this.unlistenBody) {
      this.unlistenBody = this.renderer.listen(
        'body',
        'click',
        this.onBodyClick.bind(this),
      );
    } else {
      this.unlistenBody();
      this.unlistenBody = null;
    }
  }

  onBodyClick(event: Event) {
    if (
      !this.tooltipEl.componentIns.elRef.nativeElement.contains(event.target) &&
      !this.buttonRef.el.nativeElement.contains(event.target) &&
      document.querySelector('body').contains(event.target as Node)
    ) {
      this.cancel();
    }
  }

  autoChange() {
    this.interval = '0 * * * *';
  }

  submit() {
    this.submitting = true;
    const part = {
      spec: {
        suspend: !this.auto,
        schedule: this.interval,
      },
    };
    this.k8sApi
      .patchGlobalResource({
        type: RESOURCE_TYPES.CRON_JOB,
        resource: this.cronJob,
        namespaced: true,
        part,
      })
      .subscribe(
        res => {
          this.message.success(
            this.translateService.get('ldap_auto_sync_strategy_success_tip'),
          );
          this.cronJob = res;
          this.submitting = false;
        },
        () => {
          this.submitting = false;
        },
      );
    this.cancel();
  }

  cancel() {
    if (this.unlistenBody) {
      this.unlistenBody();
      this.unlistenBody = null;
    }
    this.tooltipEl.disposeTooltip();
  }
}
