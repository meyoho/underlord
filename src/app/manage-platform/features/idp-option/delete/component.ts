import { TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, MessageService } from '@alauda/ui';
import { Component, EventEmitter, Inject, Output } from '@angular/core';

import { IdpApi } from 'app/api/idp/api';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class DeleteIDPDialogComponent {
  submitting: boolean;

  @Output()
  close = new EventEmitter<boolean>();

  inputValue = '';
  deleteUser = false;

  constructor(
    private readonly idpApi: IdpApi,
    private readonly message: MessageService,
    private readonly translateService: TranslateService,
    @Inject(DIALOG_DATA)
    public data: {
      name: string;
    },
  ) {}

  cancel() {
    this.close.next(false);
  }

  onConfirm() {
    this.submitting = true;
    this.idpApi.deleteIdp(this.inputValue, this.deleteUser).subscribe(
      () => {
        this.message.success(this.translateService.get('idp_delete_successed'));
        this.close.next(true);
      },
      () => {
        this.message.error(this.translateService.get('idp_delete_failed'));
        this.close.next(false);
      },
      () => (this.submitting = false),
    );
  }
}
