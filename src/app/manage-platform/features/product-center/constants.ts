import { Dictionary } from 'ts-essentials';

import { ProductStatus } from 'app/api/product-center/list-types';
export const PRODUCT_API_VERSION = 'product.alauda.io/v1alpha1';
export const ProductStatusMap: Dictionary<ProductStatus> = {
  SystemError: {
    type: 'SystemError',
    icon: 'error',
    text: 'system_error',
  },
  NotDeployed: {
    type: 'NotDeployed',
    icon: 'stopped',
    text: 'not_deployed',
  },
  Running: {
    type: 'Running',
    icon: 'running',
    text: 'running',
  },
  RunningAbnormal: {
    type: 'RunningAbnormal',
    icon: 'abnormal',
    text: 'running_abnormal',
  },
  DeleteFailure: {
    type: 'DeleteFailure',
    icon: 'error',
    text: 'uninstall_failed',
  },
  DeploymentFailure: {
    type: 'DeploymentFailure',
    icon: 'error',
    text: 'deployment_failure',
  },
  Deleting: {
    type: 'Deleting',
    icon: 'deleting',
    text: 'uninstalling',
  },
  InDeployment: {
    type: 'InDeployment',
    icon: 'pending',
    text: 'in_deployment',
  },
  DeploymentSucceed: {
    type: 'DeploymentSucceed',
    icon: 'success',
    text: 'deployment_succeed',
  },
};
