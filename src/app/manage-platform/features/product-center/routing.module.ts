import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProductListComponent } from './list/component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'list',
    component: ProductListComponent,
  },
  {
    path: 'csp',
    loadChildren: () => import('./csp/module').then(M => M.CspModule),
  },
  {
    path: 'tsfproduct',
    loadChildren: () => import('./tsf/module').then(M => M.TsfModule),
  },
  {
    path: 'tdsqldeploy',
    loadChildren: () =>
      import('./middleware/tdsql/module').then(M => M.TdsqlModule),
  },
  {
    path: 'redisinstaller',
    loadChildren: () =>
      import('./middleware/redis/module').then(M => M.RedisModule),
  },
  {
    path: 'coding',
    loadChildren: () => import('./coding/module').then(M => M.CodingModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductCenterRoutingModule {}
