import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { ProductListComponent } from './list/component';
import { ProductCenterRoutingModule } from './routing.module';
import { TIModule } from './ti/module';
@NgModule({
  imports: [SharedModule, ProductCenterRoutingModule, TIModule],
  declarations: [ProductListComponent],
})
export class ProductCenterModule {}
