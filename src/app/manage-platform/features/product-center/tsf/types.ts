import { KubernetesResource } from '@alauda/common-snippet/types/k8s';

export interface KubernetesResourceNode extends KubernetesResource {
  nodes?: KubernetesResource[];
  controlNode?: number;
  computingNode?: number;
  label?: string;
  disableTip?: string;
  isShowTip?: boolean;
}

export interface Address {
  type: string;
  address: string;
}

export interface OperatorInitStatus {
  message: string;
  status: string;
  step: string;
}

export interface Conditions {
  status: string;
  reason: string;
  message: string;
  type: string;
  lastTransitionTime: string;
  lastHeartbeatTime: string;
}

export interface TSFResourceSpec {
  clusterName?: string;
  operationNode?: string;
  businessNode?: string[];
}

export interface TSFResourceStatus {
  operatorStatus?: string;
  businessStatus?: string;
  version?: string;
  phase?: string;
  productEntrypoint?: string;
  operatorInitStatus?: OperatorInitStatus[];
  conditions?: Conditions[];
}

export interface TSFResource extends KubernetesResource {
  spec?: TSFResourceSpec;
  status?: TSFResourceStatus;
}

export interface ValidatorType {
  [key: string]: {
    value: string;
  };
}

export interface NodeSelected {
  [key: number]: string;
}

export interface Condition {
  lastHeartbeatTime?: string;
  lastTransitionTime?: string;
  status?: string;
  type?: string;
}

export interface BusinessForm {
  businessNode: string[];
  regionNode: Array<{
    node: KubernetesResourceNode;
    sshPort: string;
    userName: string;
    password: string;
    ip: string;
  }>;
  registry: {
    type: string;
    addr: string;
    account: string;
    auth: string;
  };
  mysql: {
    account: string;
    password: string;
  };
  redisPassword: string;
}
