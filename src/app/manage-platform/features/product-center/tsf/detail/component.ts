import {
  K8sApiService,
  KubernetesResource,
  publishRef,
} from '@alauda/common-snippet';
import { DialogRef, DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  TemplateRef,
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { get } from 'lodash-es';
import { Observable } from 'rxjs';
import { map, pluck, tap } from 'rxjs/operators';

import { ConfirmDeleteComponent } from 'app/shared/components/confirm-delete/confirm-delete.component';
import { ClusterFeature } from 'app/typings';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';

import { getProductHref } from '../../utils';
import { DeployProcessComponent } from '../components/deploy-process/component';
import { Condition, TSFResource } from '../types';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TsfDetailComponent implements OnInit {
  isShow: boolean;
  // 默认tsf项目名称
  private readonly tsfName = 'tsf-platform';

  context = this;
  clusterName: string;
  getProductHref = getProductHref;

  tsf$ = this.k8sApi
    .watchGlobalResource<TSFResource>({
      type: RESOURCE_TYPES.TSF,
      name: this.tsfName,
    })
    .pipe(publishRef());

  crd$ = this.k8sApi
    .getGlobalResource<KubernetesResource>({
      type: RESOURCE_TYPES.CUSTOM_RESOURCE_DEFINITION,
      name: 'tsfproducts.product.alauda.io',
    })
    .pipe(publishRef());

  metricDocked$: Observable<boolean>;

  cluster$: Observable<KubernetesResource>;

  deleteDialogRef: DialogRef<ConfirmDeleteComponent>;

  isDelete: boolean;

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly dialogService: DialogService,
    private readonly router: Router,
    private readonly sanitizer: DomSanitizer,
  ) {}

  ngOnInit(): void {
    this.getTsf();
  }

  getTsf() {
    this.tsf$.subscribe((tsf: TSFResource) => {
      const phase = get(tsf, 'status.phase');
      this.isShow = phase && phase !== 'Deleting' && phase !== 'Pending';
      this.isDelete = phase === 'Pending' || phase === 'Provisioning';
      if (!this.clusterName) {
        const clusterName = get(tsf, 'spec.clusterName');
        this.getCluster(clusterName);
        this.getMetricDocked(clusterName);
        this.cdr.markForCheck();
      }
    });
  }

  getMetricDocked(clusterName: string) {
    this.metricDocked$ = this.k8sApi
      .getResourceList<ClusterFeature>({
        type: RESOURCE_TYPES.FEATURE,
        cluster: clusterName,
        queryParams: {
          labelSelector: 'instanceType=prometheus',
        },
      })
      .pipe(
        pluck('items'),
        map(nodes => {
          return nodes.length > 0;
        }),
        publishRef(),
      );
  }

  getCluster(clusterName: string) {
    this.cluster$ = this.k8sApi
      .getGlobalResource<KubernetesResource>({
        type: RESOURCE_TYPES.CLUSTER_REGISTRY,
        name: clusterName,
        namespaced: true,
      })
      .pipe(
        publishRef(),
        tap(() => {
          this.clusterName = clusterName;
        }),
      );
  }

  getDeployStatus(tsf: TSFResource) {
    const phase = get(tsf, 'status.phase');
    switch (phase) {
      case 'Running':
        return 'success';
      case 'Failed':
        return 'error';
      case 'Pending':
        return 'unknown';
      case 'Provisioning':
        return 'pending';
      case 'Deleting':
        return 'deleting';
      default:
        return 'unknown';
    }
  }

  getRunningStatus(tsf: TSFResource) {
    const { phase, conditions } = get(tsf, 'status', {
      phase: '',
      conditions: [],
    });
    if (phase === 'Running') {
      const readyCondition = conditions.find(
        condition => condition.type === 'Ready',
      );
      return readyCondition.status.toLowerCase() === 'true'
        ? 'success'
        : 'error';
    }
    return '';
  }

  getClusterStatus(cluster: KubernetesResource) {
    if (cluster && cluster.status) {
      const conditions: Condition[] = get(cluster, 'status.conditions');
      const clusterStatus = conditions.find(
        (condition: Condition) => condition.type === 'NotAccessible',
      );
      if (clusterStatus) {
        return clusterStatus.status.toLowerCase() === 'true'
          ? 'error'
          : 'success';
      }
    }
    return 'unknown';
  }

  deployProcess() {
    this.dialogService.open(DeployProcessComponent, {
      size: DialogSize.Large,
      data: {
        isDetail: true,
      },
    });
  }

  deleteDialog(templateRef: TemplateRef<ConfirmDeleteComponent>) {
    this.deleteDialogRef = this.dialogService.open(templateRef);
  }

  deleteTSF() {
    this.k8sApi
      .deleteGlobalResource({
        type: RESOURCE_TYPES.TSF,
        resource: {
          apiVersion: 'product.alauda.io/v1beta1',
          kind: 'TsfProduct',
          metadata: {
            name: 'tsf-platform',
          },
        },
      })
      .subscribe(() => {
        // 必须要在代码中调用方法删除？？？
        this.deleteDialogRef.close();
        this.router.navigate(['/manage-platform/product']);
      });
  }

  getLogo = (url: string) => {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  };
}
