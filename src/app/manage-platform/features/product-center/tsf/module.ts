import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { DeployProcessComponent } from './components/deploy-process/component';
import { DeployComponent } from './deploy/component';
import { DeployBusinessComponent } from './deploy/components/deploy-business/component';
import { DeployOperationalComponent } from './deploy/components/deploy-operational/component';
import { TsfDetailComponent } from './detail/component';
import { ClusterNodesComponent } from './detail/components/node-list/component';
import { TsfRoutingModule } from './routing.module';

@NgModule({
  imports: [SharedModule, TsfRoutingModule],
  declarations: [
    DeployComponent,
    ClusterNodesComponent,
    TsfDetailComponent,
    DeployOperationalComponent,
    DeployBusinessComponent,
    DeployProcessComponent,
  ],
  entryComponents: [DeployProcessComponent],
  exports: [],
})
export class TsfModule {}
