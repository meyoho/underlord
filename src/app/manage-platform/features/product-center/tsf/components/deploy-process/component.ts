import {
  K8sApiService,
  TranslateService,
  publishRef,
  readonlyOptions,
  viewActions,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { get } from 'lodash-es';
import { map } from 'rxjs/operators';

import { StepProcessItem } from 'app/shared/components/steps-process/steps-process.component';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';

import { Conditions, OperatorInitStatus, TSFResource } from '../../types';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeployProcessComponent {
  // 默认coding项目名称
  private readonly tsfName = 'tsf-platform';
  readonly codeEditorOptions = {
    ...readonlyOptions,
    language: 'log',
  };

  readonly editorActions = {
    ...viewActions,
    diffMode: false,
    copy: false,
    export: false,
    import: false,
  };

  stepsDefault: { [key: string]: string } = {
    CreateTsfZone: 'tsf_CreateTsfZone',
    CreateRegion: 'tsf_CreateRegion',
    CreateZone: 'tsf_CreateZone',
    CreateMachines: 'tsf_CreateMachines',
    SaveDeployModuleParams: 'tsf_SaveDeployModuleParams',
    CreateServiceInstance: 'tsf_CreateServiceInstance',
    ModifyModuleConf: 'tsf_ModifyModuleConf',
    DeployBusiness: 'tsf_DeployBusiness',
  };

  version: string;
  deployLoading = true;
  stepIndex = 0;
  stepInfo: OperatorInitStatus;
  tsfSteps$ = this.k8sApi
    .watchGlobalResource<TSFResource>({
      type: RESOURCE_TYPES.TSF,
      name: this.tsfName,
    })
    .pipe(
      map(tsf => {
        this.version = get(tsf, 'spec.version');
        const operatorInitStatus = get(
          tsf,
          'status.operatorInitStatus',
        ) as OperatorInitStatus[];
        if (operatorInitStatus) {
          this.deployLoading = false;
          this.logContext = '';
          this.stepIndex = operatorInitStatus.length;
          this.stepInfo = operatorInitStatus[this.stepIndex - 1];
          return this.getSteps(tsf, operatorInitStatus);
        }
        return [];
      }),
      publishRef(),
    );

  logContext = '';

  constructor(
    @Inject(DIALOG_DATA)
    public readonly data: {
      isDetail?: boolean;
    },
    public dialogRef: DialogRef<DeployProcessComponent>,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly router: Router,
    private readonly translate: TranslateService,
  ) {}

  getSteps(tsf: TSFResource, operatorInitStatus: OperatorInitStatus[]) {
    const steps: StepProcessItem[] = [];
    Object.keys(this.stepsDefault).forEach(step => {
      // 最后一步特殊处理
      if (
        step === 'DeployBusiness' &&
        operatorInitStatus.length === 7 &&
        operatorInitStatus[6].status === 'Succeed'
      ) {
        const type = this.getStepState(
          this.getDeployStatus(get(tsf, 'status.phase')),
        );
        this.stepIndex += type === 'success' ? 2 : 1;
        this.stepInfo = {
          message: '',
          status: get(tsf, 'status.phase'),
          step: step,
        };
        steps.push({
          label: this.stepsDefault[step],
          type: type,
        });
        const conditions = get(tsf, 'status.conditions') as Conditions[];
        if (Array.isArray(conditions)) {
          conditions.forEach(item => {
            this.logContext += (item && item.message + '\n') || '';
          });
        }
      } else {
        const operatorInit = operatorInitStatus.find(
          item => item.step === step,
        );
        this.logContext += (operatorInit && operatorInit.message + '\n') || '';
        steps.push({
          label: this.stepsDefault[step],
          type: this.getStepState(operatorInit ? operatorInit.status : ''),
        });
      }
    });
    return steps;
  }

  getTranslateStatus = (stepInfo: OperatorInitStatus) => {
    if (!stepInfo) {
      return '';
    }
    if (stepInfo.status === 'success') {
      return this.translate.get('deploy_status_success');
    }
    return this.translate.get(this.stepsDefault[stepInfo.step]);
  };

  getStepState(status: string) {
    switch (status) {
      case 'Succeed':
      case 'success':
        return 'success';
      case 'Failed':
        return 'error';
      case 'error':
        return 'error';
      default:
        return 'info';
    }
  }

  getDeployStatus(phase: string) {
    switch (phase) {
      case 'Succeed':
        return 'success';
      case 'Running':
        return 'success';
      case 'Failed':
        return 'error';
      case 'Pending':
        return 'unknown';
      case 'Provisioning':
        return 'pending';
      case 'Deleting':
        return 'deleting';
      default:
        return 'unknown';
    }
  }

  close() {
    this.dialogRef.close();
    if (!this.data.isDetail) {
      this.router.navigate([
        `/manage-platform/product/tsfproduct/detail`,
        btoa(this.tsfName),
      ]);
    }
  }
}
