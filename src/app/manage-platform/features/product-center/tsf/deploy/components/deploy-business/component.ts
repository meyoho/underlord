import { TranslateService } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import {
  AbstractControl,
  FormGroup,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { get } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import {
  Address,
  KubernetesResourceNode,
  ValidatorType,
} from 'app/manage-platform/features/product-center/tsf/types';
import {
  MYSQL_PASSWORD_PATTERN,
  PORT_PATTERN,
  REDIS_PASSWORD_PATTERN,
  toUnitGi,
} from 'app/utils';

@Component({
  selector: 'alu-deploy-business',
  templateUrl: './template.html',
  styleUrls: ['./component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeployBusinessComponent extends BaseResourceFormGroupComponent {
  @Input()
  clusterName: string;

  @Input()
  operationalNode?: string;

  @Input()
  nodes: KubernetesResourceNode[] = [];

  @Input()
  disabled = false;

  nodeNum = [0, 1, 2];
  columns = ['sshPort', 'userName', 'password'];
  readonly imageRepoType = 'harbor';
  readonly imageRepoAccount = 'admin';

  constructor(
    public injector: Injector,
    private readonly translate: TranslateService,
  ) {
    super(injector);
  }

  /**
   * 校验是否与其他业务组件节点重复
   * @param contrlIndex
   */
  businessRepeatValidator(contrlIndex: number): ValidatorFn {
    return (contrl: AbstractControl): ValidatorType | null => {
      let errMsg = null;
      const nodes: Array<{
        [key: string]: string | KubernetesResourceNode;
      }> = get(contrl, 'parent.parent.value');
      if (nodes) {
        nodes.forEach((node, index) => {
          const otherName = get(node, 'node.metadata.name');
          const currentName = get(contrl.value, 'metadata.name');
          if (index !== contrlIndex && otherName && currentName === otherName) {
            errMsg = { businessRepeat: { value: contrl.value } };
            return false;
          }
        });
      }
      return errMsg;
    };
  }

  /**
   * 校验是否与运营组件节点重复
   */
  operationalRepeatValidator(): ValidatorFn {
    return (contrl: AbstractControl): ValidatorType | null => {
      const name = get(contrl.value, 'metadata.name');
      if (this.operationalNode && name === this.operationalNode) {
        return { operationalRepeat: { value: contrl.value } };
      }
      return null;
    };
  }

  /**
   * 校验资源是否正确
   */
  resourcesValidator(): ValidatorFn {
    return (contrl: AbstractControl): ValidatorType | null => {
      const cpu = get(contrl.value, 'status.capacity.cpu');
      const memory = get(contrl.value, 'status.capacity.memory') + '';
      if (cpu && memory) {
        return this.getMsgStr(contrl, cpu, memory);
      }
      return null;
    };
  }

  getMsgStr(
    contrl: AbstractControl,
    cpu: string,
    memory: string,
  ): ValidatorType | null {
    if (!parseInt(cpu) || parseInt(cpu) < 8) {
      return { businessResources: { value: contrl.value } };
    }
    const memoryNum = this.getMemoryNum(memory);
    if (!memoryNum || memoryNum < 32) {
      return { businessResources: { value: contrl.value } };
    }
    return null;
  }

  getMemoryNum(memory: string) {
    const memoryNum = parseFloat(toUnitGi(memory));
    return memoryNum !== undefined ? Math.ceil(memoryNum) : undefined;
  }

  createForm() {
    const groups: FormGroup[] = [];

    this.nodeNum.forEach((i: number) => {
      groups.push(
        this.fb.group({
          node: this.fb.control('', [
            Validators.required,
            this.businessRepeatValidator(i),
            this.operationalRepeatValidator(),
            this.resourcesValidator(),
          ]),
          sshPort: this.fb.control('', [
            Validators.required,
            Validators.pattern(PORT_PATTERN.pattern),
          ]),
          userName: '',
          password: '',
          ip: '',
        }),
      );
    });
    return this.fb.group({
      businessNode: [],
      regionNode: this.fb.array(groups),
      registry: this.fb.group({
        type: this.imageRepoType,
        addr: '',
        account: this.imageRepoAccount,
        auth: '',
      }),
      mysql: this.fb.group({
        account: '',
        password: this.fb.control('', [
          Validators.required,
          Validators.pattern(MYSQL_PASSWORD_PATTERN.pattern),
        ]),
      }),
      redisPassword: this.fb.control('', [
        Validators.required,
        Validators.pattern(REDIS_PASSWORD_PATTERN.pattern),
      ]),
      acpToken: localStorage.getItem('id_token'),
    });
  }

  getDefaultFormModel() {
    const regionNode: Array<{
      [key: string]: string | KubernetesResourceNode;
    }> = [];
    this.nodeNum.forEach(() => {
      regionNode.push({
        node: '',
      });
    });
    return {
      regionNode,
      registry: {
        account: this.imageRepoAccount,
        type: this.imageRepoType,
      },
      mysql: {
        account: 'root',
      },
      acpToken: localStorage.getItem('id_token'),
    };
  }

  getNodeIP(node: KubernetesResourceNode) {
    const addresses: Address[] = get(node, 'status.addresses');
    let nodeIP: string;
    if (addresses) {
      addresses.forEach(address => {
        if (address.type === 'InternalIP') {
          nodeIP = address.address;
          return false;
        }
      });
    }
    return nodeIP;
  }

  getType(node: KubernetesResourceNode) {
    if ('node-role.kubernetes.io/master' in node.metadata.labels) {
      return this.translate.get('tsf_node_select_options_prefix_control');
    } else if ('node-role.kubernetes.io/node' in node.metadata.labels) {
      return this.translate.get('tsf_node_select_options_prefix_calculation');
    }
  }

  nodeChange(i: number) {
    const businessNode: string[] = [];
    const regionNode = this.form.get('regionNode').value;
    const node = this.form.get(['regionNode', i, 'node']).value;
    this.form.get(['regionNode', i, 'ip']).setValue(this.getNodeIP(node));
    regionNode.forEach((item: KubernetesResourceNode) => {
      businessNode.push(get(item, 'node.metadata.name'));
    });
    this.form.get('businessNode').setValue(businessNode);
  }

  setRegistryAccount() {
    this.form.get('registry.account').setValue(this.imageRepoAccount);
  }

  setRegistryType() {
    this.form.get('registry.type').setValue(this.imageRepoType);
  }
}
