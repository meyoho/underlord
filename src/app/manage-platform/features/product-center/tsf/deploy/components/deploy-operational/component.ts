import {
  BaseNormalizeTypeParams,
  K8sApiService,
  K8sUtilService,
  KubernetesResourceList,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import {
  AbstractControl,
  FormGroup,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { get } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { RESOURCE_TYPES, ResourceType, toUnitGi } from 'app/utils';

import { Address, KubernetesResourceNode, ValidatorType } from '../../../types';

enum NodeLabel {
  MASTER = 'node-role.kubernetes.io/master',
  NODE = 'node-role.kubernetes.io/node',
}

@Component({
  selector: 'alu-deploy-operational',
  templateUrl: './template.html',
  styleUrls: ['./component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeployOperationalComponent extends BaseResourceFormGroupComponent
  implements OnInit {
  @Input()
  clusters$: Observable<KubernetesResourceNode[]>;

  @Input()
  disabled = false;

  formGroup: FormGroup;
  nodeNum = [0];
  productEntrypoint = 'http://10.0.6.2';

  constructor(
    public injector: Injector,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    private readonly translate: TranslateService,
  ) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.getClusters();
  }

  getClusters() {
    this.clusters$ = this.k8sApi
      .getGlobalResourceList<KubernetesResourceList>({
        type: RESOURCE_TYPES.CLUSTER_REGISTRY,
        namespaced: true,
      })
      .pipe(
        map((res: KubernetesResourceList) => {
          return res.items.map((cluster: KubernetesResourceNode) => {
            cluster.controlNode = 0;
            cluster.computingNode = 0;
            cluster.label = '';
            // 初始化，防止node接口报错后页面展示错误
            cluster.disableTip = this.clusterDisableTip(cluster);
            this.fetchNodeList(cluster.metadata.name).subscribe(nodes => {
              cluster.nodes = nodes.items.map(node => {
                if (NodeLabel.MASTER in node.metadata.labels) {
                  cluster.controlNode++;
                } else if (NodeLabel.NODE in node.metadata.labels) {
                  cluster.computingNode++;
                }
                node.label = this.getNodeLabel(node);
                node.disableTip = this.nodeDisableTip(node);
                return node;
              });
              cluster.label = this.getClusterLabel(cluster);

              cluster.disableTip = this.clusterDisableTip(cluster);
            });
            return cluster;
          });
        }),
        publishRef(),
      );
  }

  getClusterLabel(cluster: KubernetesResourceNode) {
    return (
      cluster.metadata.name +
      ' (' +
      this.k8sUtil.getDisplayName(cluster) +
      ') ' +
      this.translate.get('cluster_select_options', {
        controlNode: cluster.controlNode,
        computingNode: cluster.computingNode,
      })
    );
  }

  getNodeLabel(node: KubernetesResourceNode) {
    const cpu = get(node, 'status.capacity.cpu');
    const memory = get(node, 'status.capacity.memory');
    return (
      node.metadata.name +
      ' (' +
      this.translate.get('tsf_node_select_label', {
        type: this.getType(node),
        nodeIP: this.getNodeIP(node),
        nodeCPU: cpu,
        nodeMemory: this.getMemoryNum(memory),
      }) +
      ') '
    );
  }

  fetchNodeList(clusterName: string) {
    return this.k8sApi.getResourceList<KubernetesResourceNode>({
      type: RESOURCE_TYPES.NODE,
      cluster: clusterName,
    });
  }

  clusterDisableTip(cluster: KubernetesResourceNode) {
    const nodes = cluster.computingNode + cluster.controlNode;
    return nodes < 4 ? 'tsf_cluster_node_error' : null;
  }

  nodeDisableTip(node: KubernetesResourceNode) {
    return NodeLabel.MASTER in node.metadata.labels
      ? 'tsf_operational_component_node_disabled_tip'
      : null;
  }

  resourcesValidator(): ValidatorFn {
    return (contrl: AbstractControl): ValidatorType | null => {
      const cpu = get(contrl.value, 'status.capacity.cpu');
      const memory = get(contrl.value, 'status.capacity.memory') + '';
      if (cpu && memory) {
        return this.getMsgStr(contrl, cpu, memory);
      }
      return null;
    };
  }

  getMsgStr(
    contrl: AbstractControl,
    cpu: string,
    memory: string,
  ): ValidatorType | null {
    if (!parseInt(cpu) || parseInt(cpu) < 4) {
      return { operationalResources: { value: contrl.value } };
    }
    const memoryNum = this.getMemoryNum(memory);
    if (!memoryNum || memoryNum < 8) {
      return { operationalResources: { value: contrl.value } };
    }
    return null;
  }

  getMemoryNum(memory: string) {
    const memoryNum = parseFloat(toUnitGi(memory));
    return memoryNum !== undefined ? Math.ceil(memoryNum) : undefined;
  }

  createForm() {
    return this.fb.group({
      selectedCluster: '',
      operationalNode: this.fb.control('', [
        Validators.required,
        this.resourcesValidator(),
      ]),
      productEntrypoint: '',
    });
  }

  getDefaultFormModel() {
    return {};
  }

  getNodeIP(node: KubernetesResourceNode) {
    const addresses: Address[] = get(node, 'status.addresses');
    let nodeIP: string;
    if (addresses) {
      addresses.forEach(address => {
        if (address.type === 'InternalIP') {
          nodeIP = address.address;
          return false;
        }
      });
    }
    return nodeIP;
  }

  getType(node: KubernetesResourceNode) {
    if (NodeLabel.MASTER in node.metadata.labels) {
      return this.translate.get('tsf_node_select_options_prefix_control');
    } else if (NodeLabel.NODE in node.metadata.labels) {
      return this.translate.get('tsf_node_select_options_prefix_calculation');
    }
  }

  operationalNodeChange(node: KubernetesResourceNode) {
    const productEntrypoint =
      this.k8sUtil.getLabel(node, {
        baseDomain: 'kubernetes.io',
        type: 'hostname',
      } as BaseNormalizeTypeParams) || this.productEntrypoint;
    if (
      !productEntrypoint.startsWith('http:') &&
      !productEntrypoint.startsWith('https:')
    ) {
      this.productEntrypoint = 'http://' + productEntrypoint;
    }
    this.setProductEntrypoint();
  }

  setProductEntrypoint() {
    this.form.get('productEntrypoint').setValue(this.productEntrypoint);
  }

  clusterChange() {
    this.form.get('operationalNode').setValue('');
  }
}
