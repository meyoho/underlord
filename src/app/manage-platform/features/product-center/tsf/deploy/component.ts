import {
  K8sApiService,
  KubernetesResource,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { get } from 'lodash-es';
import { Observable } from 'rxjs';

import { RESOURCE_TYPES, ResourceType } from 'app/utils';

import { getProductHref } from '../../utils';
import { DeployProcessComponent } from '../components/deploy-process/component';
import { BusinessForm, KubernetesResourceNode, TSFResource } from '../types';

export enum Step {
  OperationalComponents = 'operational_components',
  BusinessComponents = 'business_components',
}

@Component({
  templateUrl: './template.html',
  styleUrls: ['./component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeployComponent {
  @ViewChild(NgForm)
  operationalForm: NgForm;

  @ViewChild(NgForm)
  businessForm: NgForm;

  readonly stepConfigs = [Step.OperationalComponents, Step.BusinessComponents];

  version: string;
  stepIndex = 1;

  clusters$: Observable<KubernetesResourceNode[]>;
  businessNode: BusinessForm;
  deployLoading: boolean;
  operational: {
    selectedCluster: KubernetesResourceNode;
    operationalNode: KubernetesResourceNode;
    productEntrypoint: string;
  };

  // 默认tsf项目名称
  private readonly tsfName = 'tsf-platform';

  constructor(
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly dialogService: DialogService,
    private readonly translate: TranslateService,
    private readonly route: ActivatedRoute,
    private readonly cdr: ChangeDetectorRef,
    private readonly router: Router,
  ) {
    this.version = get(this.route.snapshot.params, 'version');
  }

  async cancel() {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('deployment_tips'),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
      this.router.navigate(['/manage-platform/product']);
    } catch (e) {}
  }

  deployBusiness() {
    this.businessForm.onSubmit(null);
    if (this.businessForm.valid) {
      this.deployLoading = true;
      const resource = {
        apiVersion: 'product.alauda.io/v1beta1',
        kind: 'TsfProduct',
        metadata: {
          name: 'tsf-platform',
        },
      };
      this.k8sApi
        .patchGlobalResource({
          type: RESOURCE_TYPES.TSF,
          resource: resource,
          part: this.depoyRequestBody('business') as KubernetesResource,
        })
        .subscribe(() => {
          this.deployProcess();
        });
    }
  }

  deployOperational() {
    this.operationalForm.onSubmit(null);
    if (this.operationalForm.valid) {
      this.deployLoading = true;
      this.k8sApi
        .postGlobalResource<TSFResource>({
          type: RESOURCE_TYPES.TSF,
          resource: this.depoyRequestBody('operation'),
        })
        .subscribe(
          () => {
            this.getTSF();
          },
          () => {
            this.deployLoading = false;
            this.cdr.markForCheck();
          },
        );
    }
  }

  depoyRequestBody(type: string) {
    if (type === 'business') {
      const spec = Object.assign({}, this.businessNode);
      if (spec.regionNode) {
        spec.regionNode.forEach(item => {
          delete item.node;
          item.sshPort = item.sshPort.toString();
        });
      }
      return { spec };
    } else if (type === 'operation') {
      return {
        apiVersion: 'product.alauda.io/v1beta1',
        kind: 'TsfProduct',
        metadata: {
          name: this.tsfName,
        },
        spec: {
          clusterName: get(this.operational.selectedCluster, 'metadata.name'),
          version: this.version,
          operationNode: get(this.operational.operationalNode, 'metadata.name'),
          productEntrypoint: this.operational.productEntrypoint,
        },
      };
    }
    return {};
  }

  getTSF() {
    this.deployLoading = true;
    const tsf$ = this.k8sApi
      .watchGlobalResource<TSFResource>({
        type: RESOURCE_TYPES.TSF,
        name: this.tsfName,
      })
      .subscribe((tsf: TSFResource) => {
        const status = get(tsf, 'status.operatorStatus');
        if (status === 'Running') {
          tsf$.unsubscribe();
          this.openTsfOnce(tsf);
        }
      });
  }

  deployProcess() {
    this.dialogService.open(DeployProcessComponent, {
      size: DialogSize.Large,
    });
  }

  openTsfOnce(tsf: TSFResource) {
    let href = get(tsf, 'status.productEntrypoint');
    href = getProductHref(this.getTsfUrl(href));
    const newWin = window.open('_blank');
    newWin.location = href;
    setTimeout(() => {
      newWin.close();
      this.stepIndex = 2;
      this.deployLoading = false;
      this.cdr.markForCheck();
    }, 200);
  }

  getTsfUrl(href: string) {
    if (href) {
      const hrefs = href.split('?');
      if (hrefs.length === 2) {
        return (
          decodeURIComponent(hrefs[1].replace('tsfUrl=', '')) +
          `?ticket=${localStorage.getItem('id_token')}`
        );
      }
    }
    return href;
  }
}
