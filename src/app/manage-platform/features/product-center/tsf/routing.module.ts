import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DeployComponent } from './deploy/component';
import { TsfDetailComponent } from './detail/component';

const routes: Routes = [
  {
    path: 'deploy/:version',
    component: DeployComponent,
  },
  {
    path: 'detail/:name',
    component: TsfDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TsfRoutingModule {}
