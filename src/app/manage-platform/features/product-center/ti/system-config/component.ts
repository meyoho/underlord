import { DIALOG_DATA } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  Output,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { get } from 'lodash-es';

import { TIApiService } from 'app/api/ti/api';

import { TIMatrixCR } from '../types';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SystemConfigDialogComponent {
  @Output()
  complete = new EventEmitter<null>();

  submitting = false;
  grafanaKey = '';
  model = {
    grafanaKey: '',
    tiSecretKey: '',
    tiSecretId: '',
  };

  constructor(
    @Inject(DIALOG_DATA) private readonly data: TIMatrixCR,
    private readonly api: TIApiService,
    private readonly cdr: ChangeDetectorRef,
  ) {
    this.model = {
      grafanaKey: get(data.spec, 'dependency.monitor.grafanaKey'),
      tiSecretKey: get(data.spec, 'exporter.secretKey'),
      tiSecretId: get(data.spec, 'exporter.secretId'),
    };
  }

  config(form: NgForm) {
    form.onSubmit(null);
    if (!form.valid) {
      return;
    }
    this.data.spec.exporter = {
      secretId: this.model.tiSecretId,
      secretKey: this.model.tiSecretKey,
    };

    this.data.status.phase = 'Provisioning';
    this.submitting = true;
    this.api.update(this.data).subscribe(
      () => {
        this.submitting = false;
        this.complete.emit();
      },
      () => {
        this.submitting = false;
        this.cdr.markForCheck();
      },
    );
  }
}
