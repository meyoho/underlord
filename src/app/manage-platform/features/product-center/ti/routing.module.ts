import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TIDeployComponent } from './deploy/component';
import { TIDeployDetailComponent } from './detail/component';
import { TIDeployUpdateComponent } from './update/component';

const routes: Routes = [
  {
    path: 'timatrix/deploy/:version',
    component: TIDeployComponent,
  },
  {
    path: 'timatrix/detail/:name',
    component: TIDeployDetailComponent,
  },
  {
    path: 'timatrix/:name/update',
    component: TIDeployUpdateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TIRoutingModule {}
