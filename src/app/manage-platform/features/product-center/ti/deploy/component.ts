import { TranslateService, publishRef } from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import { Location } from '@angular/common';
import { ChangeDetectorRef, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';

import { TIApiService } from 'app/api/ti/api';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class TIDeployComponent {
  submitting = false;
  isMethodSelected = false;
  deployMethod = '';
  deployYaml = '';
  editorActions = {
    diffMode: false,
    clear: true,
    recover: true,
    copy: true,
    find: true,
    export: true,
    import: true,
  };

  version$ = this.route.paramMap.pipe(
    map(params => params.get('version')),
    publishRef(),
  );

  constructor(
    private readonly location: Location,
    private readonly tiApi: TIApiService,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly route: ActivatedRoute,
    private readonly cdr: ChangeDetectorRef,
    private readonly router: Router,
  ) {}

  selectMethod(method: string) {
    this.deployMethod = method;
    this.isMethodSelected = true;
  }

  deploy() {
    this.submitting = true;
    this.tiApi.deploy(this.deployYaml).subscribe(
      _ => {
        this.message.success({
          content: this.translate.get('ti_deploy_successed'),
        });
        this.submitting = false;
        this.router.navigate([
          '/manage-platform/product/',
          'tmprovision',
          'detail',
          btoa('tmprovisions.product.alauda.io'),
        ]);
      },
      _ => {
        this.submitting = false;
        this.cdr.markForCheck();
      },
    );
  }

  goBack() {
    this.location.back();
  }
}
