import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';

import { NodeAddressOption } from '../../types';

@Component({
  selector: 'alu-platform-args-config',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PlatformArgsConfigComponent implements OnInit {
  @ViewChild('ngForm')
  ngForm: NgForm;

  @Input()
  hosts: string[] = [];

  @Input()
  nodes: NodeAddressOption[] = [];

  @Input()
  version = '';

  pwdPattern = /^(?=.*\d+)(?=.*[a-z]+)(?=.*[A-Z]+)(?!.*\s.*).{5,20}$/;

  formGroup: FormGroup;

  constructor(private readonly fb: FormBuilder) {}

  ngOnInit() {
    this.formGroup = this.fb.group({
      imageRepoAddress: '',
      gpuNodes: [[]],
      sipServer: '',
      sipStreaming: '',
      imagePkgPath: '',
      domainAddress: '',
      adminPassword: '',
      userPassword: '',
      clusterExternalAddress: '',
      localeStorage: '',
      fileServer: '',
    });
  }

  submit() {
    this.ngForm.onSubmit(null);
    if (this.ngForm.valid) {
      return this.ngForm.value;
    }
    return null;
  }
}
