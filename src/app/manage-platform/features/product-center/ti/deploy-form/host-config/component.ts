import { K8sApiService, publishRef } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';

import { PORT_PATTERN, RESOURCE_TYPES } from 'app/utils';

import { NodeAddressOption } from '../../types';

@Component({
  selector: 'alu-host-config',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HostConfigComponent implements OnInit {
  @ViewChild('ngForm')
  ngForm: NgForm;

  @Input()
  nodes: NodeAddressOption[] = [];

  @Output()
  clusterChange = new EventEmitter<string>();

  formGroup: FormGroup;

  portPattern = PORT_PATTERN;

  clusters$ = this.k8sApi
    .getGlobalResourceList({
      type: RESOURCE_TYPES.CLUSTER_REGISTRY,
      namespaced: true,
    })
    .pipe(
      map(list => list.items.map(item => item.metadata.name)),
      startWith([]),
      publishRef(),
    );

  constructor(
    private readonly fb: FormBuilder,
    private readonly k8sApi: K8sApiService,
  ) {}

  ngOnInit() {
    this.formGroup = this.fb.group({
      cluster: '',
      k8sMaster: '',
      username: '',
      password: '',
      port: ['', Validators.pattern(PORT_PATTERN.pattern)],
      hosts: [[]],
    });
  }

  selectCluster(cluster: string) {
    this.clusterChange.emit(cluster);
  }

  submit() {
    this.ngForm.onSubmit(null);
    if (this.ngForm.valid) {
      return this.ngForm.value;
    }
    return null;
  }
}
