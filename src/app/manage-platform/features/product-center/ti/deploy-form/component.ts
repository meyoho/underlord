import { TranslateService, publishRef } from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { get } from 'lodash-es';
import { ReplaySubject } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import {
  TIApiService,
  TI_RESOURCE_NAME,
  mapNodeAddresses,
} from 'app/api/ti/api';

import {
  ComponentDeployMode,
  HostConfigModel,
  PlatformArgsConfigModel,
  PreinstallationServiceConfigModel,
  SSHHostModel,
  TIMatrixCR,
  TimatrixSpec,
} from '../types';

import { HostConfigComponent } from './host-config/component';
import { PlatformArgsConfigComponent } from './platform-args-config/component';
import { PreinstallationServiceConfigComponent } from './preinstallation-service-config/component';

@Component({
  selector: 'alu-ti-deploy-form',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TIDeployFormComponent {
  @Input() version = '';

  @ViewChild(HostConfigComponent)
  hostForm: HostConfigComponent;

  @ViewChild(PreinstallationServiceConfigComponent)
  preinstallationForm: PreinstallationServiceConfigComponent;

  @ViewChild(PlatformArgsConfigComponent)
  platformArgsForm: PlatformArgsConfigComponent;

  submitting = false;

  steps = [
    'host_config',
    'preinstallation_service_config',
    'platform_args_config',
  ];

  currStep = 1;

  hostConfigModel: HostConfigModel;
  preinstallationModel: PreinstallationServiceConfigModel;
  platformArgsModel: PlatformArgsConfigModel;

  clusterChange$ = new ReplaySubject<string>();

  nodes$ = this.clusterChange$.pipe(
    switchMap(cluster => this.tiApi.getClusterNodes(cluster)),
    map(mapNodeAddresses),
    publishRef(),
  );

  constructor(
    private readonly location: Location,
    private readonly tiApi: TIApiService,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly cdr: ChangeDetectorRef,
    private readonly router: Router,
  ) {}

  prev() {
    this.currStep--;
  }

  clusterChange(cluster: string) {
    this.clusterChange$.next(cluster);
  }

  getHosts(model: HostConfigModel) {
    return get(model, 'hosts', [] as SSHHostModel[]).map(
      (host: SSHHostModel) => {
        return host.ip;
      },
    );
  }

  next() {
    switch (this.currStep) {
      case 1:
        this.hostConfigModel = this.hostForm.submit();
        if (this.hostConfigModel) {
          this.currStep++;
        }
        break;
      case 2:
        this.preinstallationModel = this.preinstallationForm.submit();
        if (this.preinstallationModel) {
          this.currStep++;
        }
        break;
      default:
        break;
    }
  }

  deploy() {
    this.platformArgsModel = this.platformArgsForm.submit();
    if (this.platformArgsModel) {
      this.submitting = true;
      const spec = this.toSpec();
      this.tiApi
        .deploy({
          apiVersion: 'product.alauda.io/v1alpha1',
          kind: 'Tmprovision',
          metadata: {
            name: TI_RESOURCE_NAME,
          },
          spec,
          status: {
            phase: 'Provisioning',
            deletable: true,
          },
        } as TIMatrixCR)
        .subscribe(
          _ => {
            this.message.success({
              content: this.translate.get('ti_deploy_successed'),
            });
            this.submitting = false;
            this.router.navigate([
              '/manage-platform/product/',
              'timatrix',
              'detail',
              btoa('timatrixes.product.alauda.io'),
            ]);
          },
          _ => {
            this.submitting = false;
            this.cdr.markForCheck();
          },
        );
    }
  }

  private toSpec(): TimatrixSpec {
    return {
      cluster: this.hostConfigModel.cluster,
      k8sMaster: this.hostConfigModel.k8sMaster,
      version: this.version,
      sshConfig: {
        global: {
          user: this.hostConfigModel.username,
          password: this.hostConfigModel.password,
          port: +this.hostConfigModel.port,
        },
        others: this.hostConfigModel.hosts.reduce((acc, cur) => {
          return {
            ...acc,
            [cur.ip]: {
              user: cur.username || '',
              password: cur.password || '',
              port: +(cur.port || 0),
            },
          };
        }, {}),
      },
      dependency: {
        database: {
          mode: this.preinstallationModel.mysqlServer.mode,
          ...(this.preinstallationModel.mysqlServer.mode ===
          ComponentDeployMode.Deploy
            ? {
                tiInstall: {
                  ip: this.preinstallationModel.mysqlServer.hosts,
                },
              }
            : {
                external: [
                  {
                    host: this.preinstallationModel.mysqlServer.host,
                    user: this.preinstallationModel.mysqlServer.username,
                    password: this.preinstallationModel.mysqlServer.password,
                    port: +this.preinstallationModel.mysqlServer.port,
                    tdsqlAdaptor: this.preinstallationModel.mysqlServer
                      .tdsqlAdaptor
                      ? 1
                      : 0,
                  },
                ],
              }),
        },
        redis: {
          mode: this.preinstallationModel.redis.mode,
          ...(this.preinstallationModel.mysqlServer.mode ===
          ComponentDeployMode.Deploy
            ? {
                tiInstall: {
                  ip: this.preinstallationModel.redis.hosts,
                },
              }
            : {
                external: [
                  {
                    host: this.preinstallationModel.redis.serviceHost,
                    dnsIpPort: this.preinstallationModel.redis.dns,
                    password: this.preinstallationModel.redis.password,
                    port: +this.preinstallationModel.redis.port,
                  },
                ],
              }),
        },
        zookeeper: {
          mode: this.preinstallationModel.zkServer.mode,
          ...(this.preinstallationModel.kfkServer.mode ===
          ComponentDeployMode.Deploy
            ? {
                tiInstall: {
                  ip: this.preinstallationModel.zkServer.hosts,
                },
              }
            : {
                external: [
                  {
                    host: this.preinstallationModel.zkServer.serviceHost,
                  },
                ],
              }),
        },
        etcd: {
          tiInstall: {
            ip: this.preinstallationModel.etcd,
          },
        },
        kafka: {
          mode: this.preinstallationModel.kfkServer.mode,
          ...(this.preinstallationModel.kfkServer.mode ===
          ComponentDeployMode.Deploy
            ? {
                tiInstall: {
                  ip: this.preinstallationModel.kfkServer.hosts,
                },
              }
            : {
                external: [
                  {
                    kafkaAddr: this.preinstallationModel.kfkServer
                      .serviceAddress,
                    kafkaBootStrap: this.preinstallationModel.kfkServer.web,
                  },
                ],
              }),
        },
        keepalived: {
          port: +this.preinstallationModel.keepalivedPort,
          realServerIpPort: this.preinstallationModel.keepalivedRsip,
          vip: this.preinstallationModel.keepalivedVip,
        },
        es: {
          mode: this.preinstallationModel.esMaster.mode,
          ...(this.preinstallationModel.esMaster.mode ===
          ComponentDeployMode.Deploy
            ? {
                tiInstall: {
                  ip: this.preinstallationModel.esMaster.hosts,
                },
              }
            : {
                external: [
                  {
                    user: this.preinstallationModel.esMaster.username,
                    password: this.preinstallationModel.esMaster.password,
                    host: this.preinstallationModel.esMaster.accessHost,
                    port: +this.preinstallationModel.esMaster.port,
                  },
                ],
              }),
        },
        consul: {
          mode: this.preinstallationModel.consulServer.mode,
          ...(this.preinstallationModel.consulServer.mode ===
          ComponentDeployMode.Deploy
            ? {
                tiInstall: {
                  ip: this.preinstallationModel.consulServer.hosts,
                },
              }
            : {
                external: [
                  {
                    host: this.preinstallationModel.consulServer.accessHost,
                    port: +this.preinstallationModel.consulServer.port,
                  },
                ],
              }),
        },
        monitor: {
          externalGrafana: this.preinstallationModel.grafana,
          externalInfluxDB: {
            ip: this.preinstallationModel.influxdbAddress,
            password: this.preinstallationModel.influxdbPassword,
            user: this.preinstallationModel.influxdbUser,
          },
        },
        time: {
          server: this.preinstallationModel.timeServer,
        },
      },
      labelConfig: {
        gpuNode: this.platformArgsModel.gpuNodes.map(node => ({
          ip: node.internalip,
          name: node.hostname,
        })),
        file: {
          server: this.platformArgsModel.fileServer,
        },
        sip: {
          server: this.platformArgsModel.sipServer,
          streaming: this.platformArgsModel.sipStreaming,
        },
      },
      webPortal: {
        clusterManagerIp: this.platformArgsModel.clusterExternalAddress,
        clusterManagerDomain: this.platformArgsModel.domainAddress,
        account: {
          adminDefaultPassword: this.platformArgsModel.adminPassword,
          userDefaultPassword: this.platformArgsModel.userPassword,
        },
      },
      repository: this.platformArgsModel.imageRepoAddress,
      pathToImagesTar: this.platformArgsModel.imagePkgPath,
      storage: {
        mode: this.preinstallationModel.storageMode,
        ceph: {
          host: this.preinstallationModel.ceph,
        },
        cos: {
          bucketName: this.preinstallationModel.cosBucket,
          region: this.preinstallationModel.cosRegion,
          appId: this.preinstallationModel.cosAppId,
          secretId: this.preinstallationModel.cosSecretId,
          secretKey: this.preinstallationModel.cosSecretKey,
        },
      },
    } as TimatrixSpec;
  }

  goBack() {
    this.location.back();
  }
}
