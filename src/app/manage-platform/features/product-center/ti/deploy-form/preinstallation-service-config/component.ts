import { publishRef } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { startWith } from 'rxjs/operators';

import { TIApiService } from 'app/api/ti/api';

import { NodeAddressOption } from '../../types';

@Component({
  selector: 'alu-preinstallation-service-config',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreinstallationServiceConfigComponent implements OnInit {
  @Input()
  hosts: string[] = [];

  @Input()
  nodes: NodeAddressOption[] = [];

  @ViewChild('ngForm')
  ngForm: NgForm;

  formGroup: FormGroup;

  defaultAddresses$ = this.api
    .getSystemDefaultAddresses()
    .pipe(startWith([]), publishRef());

  constructor(
    private readonly fb: FormBuilder,
    private readonly api: TIApiService,
  ) {}

  ngOnInit() {
    this.formGroup = this.fb.group({
      mysqlServer: null,
      redis: null,
      zkServer: null,
      kfkServer: null,
      esMaster: null,
      consulServer: null,
      influxdbAddress: '',
      influxdbPassword: '',
      influxdbUser: '',
      grafana: '',
      ceph: '',
      storageMode: 0,
      cosRegion: '',
      cosBucket: '',
      cosSecretId: '',
      cosSecretKey: '',
      cosAppId: '',
      timeServer: '',
      etcd: [[]],
      keepalivedVip: '',
      keepalivedRsip: [[]],
    });
  }

  submit() {
    this.ngForm.onSubmit(null);
    console.log(this.ngForm.valid);
    if (this.ngForm.valid) {
      return this.ngForm.value;
    }
    return null;
  }
}
