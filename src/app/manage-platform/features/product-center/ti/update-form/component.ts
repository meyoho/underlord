import { K8sApiService, publishRef } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { get } from 'lodash-es';
import { ReplaySubject } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';

import { TIApiService, mapNodeAddresses } from 'app/api/ti/api';
import { RESOURCE_TYPES } from 'app/utils';

import {
  NodeAddressOption,
  TIDeployUpdateFormModel,
  TIMatrixCR,
} from '../types';

@Component({
  selector: 'alu-ti-deploy-update-form',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TIDeployUpdateFormComponent implements OnInit {
  @ViewChild('ngForm')
  ngForm: NgForm;

  @Input()
  cr: TIMatrixCR;

  formGroup: FormGroup;

  selectedCluster$ = new ReplaySubject<string>();

  model: TIDeployUpdateFormModel;

  clusters$ = this.k8sApi
    .getGlobalResourceList({
      type: RESOURCE_TYPES.CLUSTER_REGISTRY,
      namespaced: true,
    })
    .pipe(
      map(list => list.items.map(item => item.metadata.name)),
      startWith([]),
      publishRef(),
    );

  nodes$ = this.selectedCluster$.pipe(
    switchMap(cluster => this.api.getClusterNodes(cluster)),
    map(mapNodeAddresses),
    publishRef(),
  );

  constructor(
    private readonly fb: FormBuilder,
    private readonly k8sApi: K8sApiService,
    private readonly api: TIApiService,
  ) {}

  ngOnInit() {
    this.model = this.getModel(this.cr);
    this.formGroup = this.fb.group(
      Object.keys(this.model).reduce((acc, cur) => {
        return {
          ...acc,
          [cur]: this.fb.control(get(this.model, cur)),
        };
      }, {}),
    );
    this.selectedCluster$.next(this.cr.spec.cluster);
  }

  private getModel(cr: TIMatrixCR): TIDeployUpdateFormModel {
    return {
      storageMode: cr.spec.storage.mode,
      cosRegion: cr.spec.storage.cos?.region || '',
      cosBucket: cr.spec.storage.cos?.bucketName || '',
      cosSecretId: cr.spec.storage.cos?.secretId || '',
      cosSecretKey: cr.spec.storage.cos?.secretKey || '',
      cosAppId: cr.spec.storage.cos?.appId || '',
      clusterExternalAddress: cr.spec.webPortal.clusterManagerIp || '',
      domainAddress: cr.spec.webPortal.clusterManagerDomain || '',
      fileServer: cr.spec.labelConfig.file.server,
      sipServer: cr.spec.labelConfig.sip.server,
      sipStreaming: cr.spec.labelConfig.sip.streaming,
      gpuNodes: cr.spec.labelConfig.gpuNode.map(n => ({
        hostname: n.name,
        internalip: n.ip,
      })),
    };
  }

  nodeTrackFn(node: NodeAddressOption) {
    return node.hostname;
  }

  submit() {
    this.ngForm.onSubmit(null);
    if (this.ngForm.valid) {
      return this.ngForm.value;
    }
    return null;
  }
}
