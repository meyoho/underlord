import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { IP_PATTERN, PORT_PATTERN } from 'app/utils';

import {
  ComponentDeployMode,
  ComponentDeployOption,
  RedisModel,
} from '../../types';

@Component({
  selector: 'alu-redis-field',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RedisFieldComponent extends BaseResourceFormGroupComponent
  implements OnInit {
  @Input()
  hosts: string[] = [];

  @Input()
  isUpdate = false;

  ipPattern = IP_PATTERN;
  portPattern = PORT_PATTERN;

  componentOptions: ComponentDeployOption[] = [
    {
      label: 'deploy_component',
      value: ComponentDeployMode.Deploy,
    },
    {
      label: 'access_component',
      value: ComponentDeployMode.Access,
    },
  ];

  fieldValidators: Array<{
    name: string;
    mode: ComponentDeployMode;
  }> = [
    { name: 'hosts', mode: ComponentDeployMode.Deploy },
    { name: 'serviceHost', mode: ComponentDeployMode.Access },
    { name: 'dns', mode: ComponentDeployMode.Access },
    { name: 'port', mode: ComponentDeployMode.Access },
    { name: 'password', mode: ComponentDeployMode.Access },
  ];

  constructor(public readonly injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.modeChange(this.formModel.mode);
  }

  createForm() {
    return this.fb.group({
      mode: ComponentDeployMode.Deploy,
      hosts: [[]],
      serviceHost: '',
      password: '',
      port: '',
      dns: '',
    });
  }

  getDefaultFormModel(): RedisModel {
    return {
      mode: ComponentDeployMode.Deploy,
      hosts: [],
    };
  }

  modeChange(mode: ComponentDeployMode) {
    this.fieldValidators.forEach(field => {
      const control = this.form.get(field.name);
      if (mode === field.mode) {
        control.enable();
      } else {
        control.disable();
      }
    });
  }
}
