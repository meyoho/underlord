import { ObservableInput } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { get } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Observable } from 'rxjs';

import { TIApiService } from 'app/api/ti/api';
import { IP_PATTERN, PORT_PATTERN } from 'app/utils';

import {
  ComponentDeployMode,
  ComponentDeployOption,
  EsMasterModel,
  NodeAddressOption,
} from '../../types';

@Component({
  selector: 'alu-es-master-field',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EsMasterFieldComponent extends BaseResourceFormGroupComponent
  implements OnInit {
  @Input()
  defaultAddresses: NodeAddressOption[];

  @ObservableInput(true)
  defaultAddresses$: Observable<NodeAddressOption[]>;

  @Input()
  isUpdate = false;

  @Input()
  hosts: string[] = [];

  ipPattern = IP_PATTERN;
  portPattern = PORT_PATTERN;

  fieldValidators: Array<{
    name: string;
    mode: ComponentDeployMode;
  }> = [
    {
      name: 'hosts',
      mode: ComponentDeployMode.Deploy,
    },
    {
      name: 'accessHost',
      mode: ComponentDeployMode.Access,
    },
    {
      name: 'port',
      mode: ComponentDeployMode.Access,
    },
    {
      name: 'username',
      mode: ComponentDeployMode.Access,
    },
    {
      name: 'password',
      mode: ComponentDeployMode.Access,
    },
  ];

  componentOptions: ComponentDeployOption[] = [
    {
      label: 'deploy_component',
      value: ComponentDeployMode.Deploy,
    },
    {
      label: 'access_component',
      value: ComponentDeployMode.Access,
    },
  ];

  constructor(
    public readonly injector: Injector,
    public readonly api: TIApiService,
  ) {
    super(injector);
  }

  ngOnInit() {
    this.api
      .getEsSecret()
      .subscribe((res: { data: { username: string; password: string } }) => {
        if (res.data) {
          this.form.patchValue({
            username: atob(res.data.username),
            password: atob(res.data.password),
          });
        }
      });
    this.modeChange(this.formModel.mode);

    this.defaultAddresses$.subscribe(addrs => {
      this.form
        .get('accessHost')
        .patchValue(this.formModel.host || get(addrs, [0, 'internalip'], ''));
    });
  }

  createForm() {
    return this.fb.group({
      mode: ComponentDeployMode.Deploy,
      hosts: [],
      accessHost: '',
      username: '',
      password: '',
      port: '',
    });
  }

  getDefaultFormModel(): EsMasterModel {
    return {
      mode: ComponentDeployMode.Access,
      hosts: [],
      port: '9200',
      username: '',
      password: '',
    };
  }

  modeChange(mode: ComponentDeployMode) {
    this.fieldValidators.forEach(field => {
      const control = this.form.get(field.name);
      if (mode === field.mode) {
        control.enable();
      } else {
        control.disable();
      }
    });
  }
}
