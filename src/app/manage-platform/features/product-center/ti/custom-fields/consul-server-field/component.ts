import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { IP_PATTERN, PORT_PATTERN } from 'app/utils';

import {
  ComponentDeployMode,
  ComponentDeployOption,
  ConsulServerModel,
} from '../../types';

@Component({
  selector: 'alu-consul-server-field',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConsulServerFieldComponent extends BaseResourceFormGroupComponent
  implements OnInit {
  @Input()
  hosts: string[] = [];

  @Input()
  isUpdate = false;

  ipPattern = IP_PATTERN;
  portPattern = PORT_PATTERN;

  componentOptions: ComponentDeployOption[] = [
    {
      label: 'deploy_component',
      value: ComponentDeployMode.Deploy,
    },
    {
      label: 'access_component',
      value: ComponentDeployMode.Access,
    },
  ];

  fieldValidators: Array<{
    name: string;
    mode: ComponentDeployMode;
  }> = [
    { name: 'hosts', mode: ComponentDeployMode.Deploy },
    { name: 'accessHost', mode: ComponentDeployMode.Access },
    { name: 'port', mode: ComponentDeployMode.Access },
  ];

  constructor(public readonly injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.modeChange(this.formModel.mode);
  }

  createForm() {
    return this.fb.group({
      mode: ComponentDeployMode.Deploy,
      hosts: [[]],
      accessHost: '',
      port: '',
    });
  }

  getDefaultFormModel(): ConsulServerModel {
    return {
      mode: ComponentDeployMode.Deploy,
      hosts: [],
    };
  }

  modeChange(mode: ComponentDeployMode) {
    this.fieldValidators.forEach(field => {
      const control = this.form.get(field.name);
      if (mode === field.mode) {
        control.enable();
      } else {
        control.disable();
      }
    });
  }
}
