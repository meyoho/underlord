import { ObservableInput } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { get } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Observable } from 'rxjs';

import { IP_PATTERN } from 'app/utils';

import {
  ComponentDeployMode,
  ComponentDeployOption,
  NodeAddressOption,
  ZkServerModel,
} from '../../types';

@Component({
  selector: 'alu-zk-server-field',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ZkServerFieldComponent extends BaseResourceFormGroupComponent
  implements OnInit {
  @Input()
  defaultAddresses: NodeAddressOption[];

  @ObservableInput(true)
  defaultAddresses$: Observable<NodeAddressOption[]>;

  @Input()
  isUpdate = false;

  @Input()
  hosts: string[] = [];

  ipPattern = IP_PATTERN;

  fieldValidators: Array<{
    name: string;
    mode: ComponentDeployMode;
  }> = [
    { name: 'hosts', mode: ComponentDeployMode.Deploy },
    { name: 'serviceHost', mode: ComponentDeployMode.Access },
  ];

  componentOptions: ComponentDeployOption[] = [
    {
      label: 'deploy_component',
      value: ComponentDeployMode.Deploy,
    },
    {
      label: 'access_component',
      value: ComponentDeployMode.Access,
    },
  ];

  constructor(public readonly injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.modeChange(this.formModel.mode);

    this.defaultAddresses$.subscribe(addrs => {
      this.form
        .get('serviceHost')
        .patchValue(this.formModel.host || get(addrs, [0, 'internalip'], ''));
    });
  }

  createForm() {
    return this.fb.group({
      mode: ComponentDeployMode.Deploy,
      hosts: [],
      serviceHost: '',
    });
  }

  getDefaultFormModel(): ZkServerModel {
    return {
      mode: ComponentDeployMode.Access,
      hosts: [],
    };
  }

  modeChange(mode: ComponentDeployMode) {
    this.fieldValidators.forEach(field => {
      const control = this.form.get(field.name);
      if (mode === field.mode) {
        control.enable();
      } else {
        control.disable();
      }
    });
  }
}
