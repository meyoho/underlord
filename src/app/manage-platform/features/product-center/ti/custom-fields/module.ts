import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { ConsulServerFieldComponent } from './consul-server-field/component';
import { EsMasterFieldComponent } from './es-master-field/component';
import { HostsFieldComponent } from './hosts-field/component';
import { KfkServerFieldComponent } from './kfk-server-field/component';
import { MysqlServerFieldComponent } from './mysql-server-field/component';
import { RedisFieldComponent } from './redis-field/component';
import { ZkServerFieldComponent } from './zk-server-field/component';

const fields = [
  HostsFieldComponent,
  MysqlServerFieldComponent,
  RedisFieldComponent,
  ZkServerFieldComponent,
  KfkServerFieldComponent,
  EsMasterFieldComponent,
  ConsulServerFieldComponent,
];

@NgModule({
  imports: [SharedModule],
  declarations: [...fields],
  exports: [...fields],
})
export class TICustomFieldsModule {}
