import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';

import { IP_PATTERN, PORT_PATTERN } from 'app/utils';

@Component({
  selector: 'alu-hosts-field',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HostsFieldComponent extends BaseResourceFormArrayComponent
  implements OnInit {
  @Input()
  options: Array<{
    hostname: string;
    internalip: string;
  }>;

  constructor(public readonly injector: Injector) {
    super(injector);
  }

  ipPattern = IP_PATTERN;
  portPattern = PORT_PATTERN;

  customFields = ['username', 'password', 'port'];

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel() {
    return [
      {
        ip: '',
        sshConfig: 'default',
      },
    ];
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  hostConfigModeChange(index: number, value: string) {
    this.customFields.forEach(field => {
      const control = (this.form.controls[index] as FormGroup).get(field);
      if (value === 'custom') {
        control.enable();
      } else {
        control.disable();
      }
    });
  }

  ngOnInit() {
    this.hostConfigModeChange(0, this.formModel[0].sshConfig);
  }

  private createNewControl() {
    return this.fb.group({
      ip: [
        '',
        [Validators.required, Validators.pattern(this.ipPattern.pattern)],
      ],
      sshConfig: 'default',
      username: '',
      password: '',
      port: '',
    });
  }
}
