import {
  AsyncDataLoader,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import { Location } from '@angular/common';
import { ChangeDetectorRef, Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { noop } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';

import { ProductCenterListApi } from 'app/api/product-center/list-api';
import { TIApiService } from 'app/api/ti/api';
import { ProductCRD, ProductCenter } from 'app/typings';

import { mapStatus } from '../../utils';
import { TIDeployUpdateFormModel, TIMatrixCR } from '../types';
import { TIDeployUpdateFormComponent } from '../update-form/component';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class TIDeployUpdateComponent {
  deployment: {
    crd: ProductCRD;
    cr: TIMatrixCR;
  };

  @ViewChild(TIDeployUpdateFormComponent)
  form: TIDeployUpdateFormComponent;

  submitting = false;

  crdName$ = this.route.paramMap.pipe(
    map(params => atob(params.get('name'))),
    publishRef(),
  );

  detail$ = this.crdName$.pipe(
    switchMap(name => this.listApi.getProduct(name)),
    map((product: ProductCenter) => {
      return {
        crd: product.crd,
        cr: product.crs[0] as TIMatrixCR,
      };
    }),
    tap(d => {
      this.deployment = d;
    }),
    publishRef(),
  );

  dataLoader = new AsyncDataLoader({
    fetcher: () => this.detail$,
  });

  constructor(
    private readonly listApi: ProductCenterListApi,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly location: Location,
    private readonly tiApi: TIApiService,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly cdr: ChangeDetectorRef,
    private readonly dialog: DialogService,
  ) {}

  secretName(name: string) {
    return btoa(name);
  }

  getStatusIcon(item: TIMatrixCR) {
    return mapStatus(item).icon;
  }

  getStatusText(item: TIMatrixCR) {
    return mapStatus(item).text;
  }

  getHosts(cr: TIMatrixCR) {
    return Object.keys(cr.spec.sshConfig.others);
  }

  update() {
    const model = this.form.submit();
    if (model) {
      this.dialog
        .confirm({
          title: this.translate.get('ti_matrix_update_confirm_title'),
          content: this.translate.get('ti_matrix_update_confirm_content'),
          confirmText: this.translate.get('update'),
          cancelText: this.translate.get('cancel'),
          beforeConfirm: (resolve, reject) => {
            this.submitting = true;
            const payload = this.buildPayload(model);
            this.tiApi.update(payload).subscribe(
              () => {
                resolve();
              },
              () => {
                this.submitting = false;
                this.cdr.markForCheck();
                reject();
              },
            );
          },
        })
        .then(() => {
          this.submitting = false;
          this.message.success({
            content: this.translate.get('ti_update_successed'),
          });
          this.router.navigate([
            '/manage-platform/product/',
            'timatrix',
            'detail',
            btoa('timatrixes.product.alauda.io'),
          ]);
        })
        .catch(noop);
    }
  }

  private buildPayload(model: TIDeployUpdateFormModel): TIMatrixCR {
    return {
      ...this.deployment.cr,
      spec: {
        ...this.deployment.cr.spec,
        labelConfig: {
          gpuNode: model.gpuNodes.map(n => ({
            ip: n.internalip,
            name: n.hostname,
          })),
          file: {
            server: model.fileServer,
          },
          sip: {
            server: model.sipServer,
            streaming: model.sipStreaming,
          },
        },
        storage: {
          ...this.deployment.cr.spec.storage,
          mode: model.storageMode,
          cos: {
            bucketName: model.cosBucket,
            region: model.cosRegion,
            appId: model.cosAppId,
            secretId: model.cosSecretId,
            secretKey: model.cosSecretKey,
          },
        },
        webPortal: {
          ...this.deployment.cr.spec.webPortal,
          clusterManagerIp: model.clusterExternalAddress,
          clusterManagerDomain: model.domainAddress,
        },
      },
      status: {
        ...this.deployment.cr.status,
        phase: 'Provisioning',
      },
    };
  }

  goBack() {
    this.location.back();
  }
}
