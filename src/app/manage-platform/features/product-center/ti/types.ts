import { KubernetesResource } from '@alauda/common-snippet';

import { ProductCR, ProductStatusCondition } from 'app/typings';

export const enum ComponentDeployMode {
  Deploy,
  Access,
}

export interface ComponentDeployOption {
  label: string;
  value: ComponentDeployMode;
}

// Fields
export interface SSHHostModel {
  ip: string;
  sshConfig: string;
  username: string;
  password: string;
  port: string;
}

export interface MysqlServerModel {
  mode: ComponentDeployMode;
  hosts?: string[];
  host?: string;
  username?: string;
  password?: string;
  port?: string;
  tdsqlAdaptor?: boolean;
}

export interface RedisModel {
  mode: ComponentDeployMode;
  hosts?: string[];
  serviceHost?: string;
  password?: string;
  port?: string;
  dns?: string;
}

export interface ZkServerModel {
  mode: ComponentDeployMode;
  hosts?: string[];
  serviceHost?: string;
}

export interface KfkServerModel {
  mode: ComponentDeployMode;
  hosts?: string[];
  serviceAddress?: string;
  web?: string;
}

export interface EsMasterModel {
  mode: ComponentDeployMode;
  hosts?: string[];
  accessHost?: string;
  username?: string;
  password?: string;
  port?: string;
}

export interface ConsulServerModel {
  mode: ComponentDeployMode;
  hosts?: string[];
  accessHost?: string;
  port?: string;
}

export interface MonitorModel {
  mode: ComponentDeployMode;
  masterHost: string;
  slaveHost: string;
  influxdbUsername?: string;
  influxdbPassword?: string;
}

// Sub forms
export interface HostConfigModel {
  cluster: string;
  k8sMaster: string;
  username: string;
  password: string;
  port: string;
  hosts: SSHHostModel[];
}

export interface PreinstallationServiceConfigModel {
  mysqlServer: MysqlServerModel;
  redis: RedisModel;
  zkServer: ZkServerModel;
  kfkServer: KfkServerModel;
  esMaster: EsMasterModel;
  consulServer: ConsulServerModel;
  influxdbAddress?: string;
  influxdbPassword?: string;
  influxdbUser?: string;
  grafana?: string;
  ceph?: string;
  storageMode: 0 | 1;
  cosRegion?: string;
  cosBucket?: string;
  cosSecretId?: string;
  cosSecretKey?: string;
  cosAppId?: string;
  timeServer?: string;
  etcd: string[];
  keepalivedVip?: string;
  keepalivedRsip?: string[];
  keepalivedPort?: string;
}

export interface PlatformArgsConfigModel {
  imageRepoAddress: string;
  imagePkgPath: string;
  domainAddress: string;
  adminPassword: string;
  userPassword: string;
  clusterExternalAddress: string;
  gpuNodes: NodeAddressOption[];
  sipServer: string;
  sipStreaming: string;
  fileServer?: string;
}

export interface TIDeployUpdateFormModel {
  storageMode: 0 | 1;
  cosRegion?: string;
  cosBucket?: string;
  cosSecretId?: string;
  cosSecretKey?: string;
  cosAppId?: string;
  clusterExternalAddress: string;
  domainAddress: string;
  sipServer: string;
  sipStreaming: string;
  fileServer?: string;
  gpuNodes: NodeAddressOption[];
}

export interface ClusterNodeResource extends KubernetesResource {
  status: {
    addresses: Array<{
      type: string;
      address: string;
    }>;
  };
}

export interface NodeAddressOption {
  hostname: string;
  internalip: string;
}

export interface TimatrixSpec {
  cluster: string;
  pathToImagesTar?: string;
  dependency: {
    consul: {
      mode: ComponentDeployMode;
      tiInstall?: {
        ip: string[];
      };
      external?: Array<{
        host: string;
        port: number;
      }>;
    };
    database: {
      mode: ComponentDeployMode;
      tiInstall?: {
        ip: string[];
      };
      external?: Array<{
        host: string;
        user: string;
        password: string;
        port: number;
        tdsqlAdaptor?: boolean;
      }>;
    };
    es: {
      mode: ComponentDeployMode;
      external: Array<{
        host: string;
        port?: number;
        user?: string;
        password?: string;
      }>;
    };
    etcd: {
      tiInstall: {
        ip: string[];
      };
    };
    kafka: {
      mode: ComponentDeployMode;
      external: Array<{
        kafkaAddr: string;
        kafkaBootStrap?: string;
      }>;
    };
    keepalived: {
      port: number;
      realServerIpPort: string[];
      vip: string;
    };
    redis: {
      mode: ComponentDeployMode;
      tiInstall?: {
        ip: string[];
      };
      external?: Array<{
        host: string;
        dnsIpPort?: string;
        password: string;
        port: number;
      }>;
    };
    monitor?: {
      externalGrafana?: string;
      externalInfluxDB?: {
        ip?: string;
        password?: string;
        user?: string;
      };
    };
    zookeeper: {
      mode: ComponentDeployMode;
      external: Array<{
        host: string;
      }>;
    };
    time?: {
      server: string;
    };
  };
  exporter?: {
    secretId: string;
    secretKey: string;
  };
  k8sMaster: string;
  labelConfig: {
    gpuNode: Array<{ ip: string; name: string }>;
    file?: {
      server: string;
    };
    sip?: {
      server?: string;
      streaming?: string;
    };
  };
  repository: string;
  sshConfig: {
    global: {
      user: string;
      password: string;
      port: number;
    };
    others: {
      [key: string]: {
        user: string;
        password: string;
        port: number;
      };
    };
  };
  version: string;
  webPortal: {
    clusterManagerIp: string;
    clusterManagerDomain?: string;
    account?: {
      adminDefaultPassword?: string;
      userDefaultPassword?: string;
    };
  };
  storage: {
    mode: 0 | 1;
    ceph?: {
      host: string;
    };
    cos?: {
      bucketName: string;
      region: string;
      secretId: string;
      secretKey: string;
      appId: string;
    };
  };
}

export interface TIInstallationStep {
  condition: ProductStatusCondition[];
  log: {
    enable: boolean;
    resource?: {
      apiVersion: string;
      kind: string;
      name: string;
      namespace: string;
    };
  };
  name: string;
  retries: number;
}

export interface TIMatrixCR extends ProductCR {
  spec: TimatrixSpec;
  status: ProductCR['status'] & {
    installationSteps: TIInstallationStep[];
  };
}
