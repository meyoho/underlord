import {
  Dictionary,
  ObservableInput,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Observable, forkJoin, of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import { TIApiService } from 'app/api/ti/api';

import { TIInstallationStep } from '../types';

type Log = TIInstallationStep & { content: string };

@Component({
  selector: 'alu-ti-deploy-log',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TIDeployLogComponent {
  @Input()
  steps: TIInstallationStep[] = [];

  @ObservableInput(true)
  steps$: Observable<TIInstallationStep[]>;

  logs$ = this.steps$.pipe(
    tap(steps => {
      const index = steps.findIndex(
        step =>
          step.condition[0].status !== 'True' ||
          (step.condition[0].status === 'True' &&
            step.condition[1].status !== 'True'),
      );
      this.currStep = index > -1 ? index + 1 : steps.length;
      this.selectedStep = this.selectedStep || this.currStep;
    }),
    switchMap(steps =>
      forkJoin(
        steps.map((step, index) => {
          return step.log.enable
            ? this.logCache[step.name]
              ? of(this.logCache[step.name])
              : this.api
                  .getLog(step.log.resource.name, step.log.resource.namespace)
                  .pipe(
                    map((log: string) => ({
                      ...step,
                      content: log,
                    })),
                    tap(log => {
                      if (
                        index < this.currStep - 1 ||
                        !(
                          step.condition[0].status === 'Unknown' ||
                          (step.condition[0].status === 'True' &&
                            step.condition[1].status === 'Unknown')
                        )
                      ) {
                        this.logCache[step.name] = log;
                      }
                    }),
                    catchError(() =>
                      of({
                        ...step,
                        content: '',
                      }),
                    ),
                  )
            : of({
                ...step,
                content: '',
              });
        }),
      ),
    ),
    publishRef(),
  );

  // Store logs execpt the log of last step
  logCache: Dictionary<Log> = {};

  selectedStep: number;

  currStep = 1;

  editorActions = {
    diffMode: false,
    clear: false,
    recover: false,
    copy: false,
    find: true,
    export: false,
    import: false,
  };

  constructor(
    private readonly api: TIApiService,
    private readonly translate: TranslateService,
  ) {}

  stepSelect(num: number) {
    this.selectedStep = num;
  }

  getProcessSteps(steps: TIInstallationStep[]) {
    return steps.map(step => {
      return {
        label: step.name,
        type: step.condition.find(c => c.status === 'False') ? 'error' : 'info',
      };
    });
  }

  getStatus = (log: Log) => {
    const [createCondition, executeCondition] = log.condition;
    if (createCondition.status === 'False') {
      return {
        icon: 'error',
        text: 'execute_failed',
        message: this.buildMessage([createCondition.reason]),
      };
    } else if (createCondition.status === 'True') {
      switch (executeCondition.status) {
        case 'False':
          return {
            icon: 'error',
            text: 'execute_failed',
            message: this.buildMessage([
              createCondition.reason,
              executeCondition.reason,
            ]),
          };
        case 'True':
          return {
            icon: 'success',
            text: 'execute_successed',
            message: this.buildMessage([
              createCondition.reason,
              executeCondition.reason,
            ]),
          };
        case 'Unknown':
        default:
          return {
            icon: 'running',
            text: 'executing',
            message: this.buildMessage([createCondition.reason, 'executing']),
          };
      }
    } else {
      return {
        icon: 'unknown',
        text: 'not_execute',
        message: '-',
      };
    }
  };

  getCurrentStep(steps: TIInstallationStep[]) {
    const index = steps.findIndex(
      step =>
        step.condition[0].status === 'True' &&
        step.condition[1].status === 'Unknown',
    );
    return Math.max(index + 1, 1);
  }

  private buildMessage(resons: string[]) {
    return resons.map(r => this.translate.get(r.toLowerCase())).join(', ');
  }
}
