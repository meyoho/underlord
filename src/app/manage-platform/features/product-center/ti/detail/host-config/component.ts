import { Component, Input } from '@angular/core';
import { Dictionary } from 'ts-essentials';

import { ProductCRD } from 'app/typings';

import { TIMatrixCR } from './../../types';

@Component({
  selector: 'alu-ti-detail-host-config',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class TiDetailHostConfigComponent {
  @Input()
  deployment: {
    cr: TIMatrixCR;
    crd: ProductCRD;
  };

  showPassword = false;

  getHosts(
    ipMap: Dictionary<{
      user: string;
      port: number;
      password: string;
    }>,
  ) {
    return Object.keys(ipMap).map(ip => {
      const item = ipMap[ip];
      return {
        ip,
        sshConfig:
          item.user && item.port && item.password ? 'custom' : 'default',
        ...item,
      };
    });
  }
}
