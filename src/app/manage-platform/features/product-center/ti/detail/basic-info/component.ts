import { K8sUtilService, TranslateService } from '@alauda/common-snippet';
import {
  DialogRef,
  DialogService,
  DialogSize,
  MessageService,
} from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Input,
  Output,
  TemplateRef,
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { get } from 'lodash-es';
import { noop } from 'rxjs';
import { tap } from 'rxjs/operators';

import { ProductCenterListApi } from 'app/api/product-center/list-api';
import { ConfirmDeleteComponent } from 'app/shared/components/confirm-delete/confirm-delete.component';
import { ProductCRD } from 'app/typings';

import { mapDeployStatus, mapOperatingStatus, mapStatus } from '../../../utils';
import { SystemConfigDialogComponent } from '../../system-config/component';
import { TIInstallationStep, TIMatrixCR } from '../../types';

@Component({
  selector: 'alu-ti-detail-basic',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class TiDetailBasicComponent {
  @Input()
  deployment: {
    cr: TIMatrixCR;
    crd: ProductCRD;
  };

  @Output()
  reload = new EventEmitter();

  mapDeployStatus = mapDeployStatus;
  mapOperatingStatus = mapOperatingStatus;

  context = this;
  deleteConfirmDialog: DialogRef<ConfirmDeleteComponent>;

  constructor(
    private readonly k8sUtil: K8sUtilService,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly listApi: ProductCenterListApi,
    private readonly message: MessageService,
    private readonly router: Router,
    private readonly sanitizer: DomSanitizer,
  ) {}

  getStatusType(item: TIMatrixCR) {
    return mapStatus(item).type;
  }

  getBase64CrdName = (crd: ProductCRD) => {
    return btoa(this.k8sUtil.getName(crd));
  };

  getAvailableVersion(crd: ProductCRD) {
    return this.k8sUtil.getLabel(crd, 'available-version');
  }

  secretName(name: string) {
    return btoa(name);
  }

  getLogo = (crd: ProductCRD) => {
    const url = this.k8sUtil.getAnnotation(crd, 'product-logo');
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  };

  getSteps(cr: TIMatrixCR) {
    return get(cr, 'status.installationSteps', []) as TIInstallationStep[];
  }

  handleUpgradeable = ({ crd, cr }: { crd: ProductCRD; cr: TIMatrixCR }) => {
    if (this.deployment.cr.status.phase !== 'Running') {
      return {
        disabled: true,
        tooltip: 'non_running_status_upgrade_not_supported',
      };
    } else {
      return this.getAvailableVersion(crd) === cr.spec.version
        ? {
            disabled: true,
            tooltip: 'no_new_version_not_executable',
          }
        : {
            disabled: false,
            tooltip: null,
          };
    }
  };

  isDeletable = (cr: TIMatrixCR) => {
    const disabledSet = new Set(['Deleting', 'InDeployment']);
    return !disabledSet.has(this.getStatusType(cr))
      ? this.deployment.cr.status.deletable
      : false;
  };

  isExecutable = (cr: TIMatrixCR) => {
    return ['Running', 'RunningAbnormal'].includes(this.getStatusType(cr));
  };

  upgrade() {
    this.dialog
      .confirm({
        title: this.translate.get('upgrade_confirm', {
          name: this.k8sUtil.getDisplayName(this.deployment.crd),
          version: this.getAvailableVersion(this.deployment.crd),
        }),
        confirmText: this.translate.get('upgrade'),
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) => {
          this.deployment.cr.spec.version = this.k8sUtil.getLabel(
            this.deployment.crd,
            'available-version',
          );
          this.deployment.cr.status.phase = 'Provisioning';
          this.listApi
            .upgradeProduct(this.deployment.crd, this.deployment.cr)
            .subscribe(() => {
              resolve();
            }, reject);
        },
      })
      .then(() => {
        this.message.success(
          this.translate.get('product_upgrade_successed', {
            name: this.k8sUtil.getDisplayName(this.deployment.crd),
          }),
        );
        this.reload.emit();
      })
      .catch(noop);
  }

  delete(templateRef: TemplateRef<ConfirmDeleteComponent>) {
    this.deleteConfirmDialog = this.dialog.open(templateRef);
  }

  deleteApi() {
    return this.listApi
      .deleteProduct(this.deployment.crd, this.deployment.cr)
      .pipe(
        tap(() => {
          this.router.navigate(['/manage-platform/product/list']);
        }),
      );
  }

  systemConfig() {
    const dialogRef = this.dialog.open(SystemConfigDialogComponent, {
      size: DialogSize.Medium,
      data: this.deployment.cr,
    });
    dialogRef.componentInstance.complete.subscribe(() => {
      this.message.success(this.translate.get('system_config_successed'));
      this.reload.emit();
      dialogRef.close();
    });
  }
}
