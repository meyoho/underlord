import { Component, Input } from '@angular/core';

import { ProductCRD } from 'app/typings';

import { TIMatrixCR } from './../../types';

@Component({
  selector: 'alu-ti-detail-platform-args',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class TiDetailPlatformArgsComponent {
  @Input()
  deployment: {
    cr: TIMatrixCR;
    crd: ProductCRD;
  };

  pwdVisible = {
    admin: false,
    user: false,
  };
}
