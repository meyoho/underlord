import {
  AsyncDataLoader,
  K8sApiService,
  publishRef,
} from '@alauda/common-snippet';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject, combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { ProductCRD } from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';

import { TIMatrixCR } from '../types';

@Component({
  templateUrl: './template.html',
})
export class TIDeployDetailComponent {
  deployment: {
    crd: ProductCRD;
    cr: TIMatrixCR;
  };

  timeoutId: number;

  crdName$ = this.route.paramMap.pipe(
    map(params => atob(params.get('name'))),
    publishRef(),
  );

  reload$$ = new Subject<void>();

  cr$ = this.k8sApi
    .watchGlobalResource<TIMatrixCR>({
      type: RESOURCE_TYPES.TIMATRIX,
      name: 'timatrix',
    })
    .pipe(publishRef());

  crd$ = this.crdName$.pipe(
    switchMap(name =>
      this.k8sApi.getGlobalResource<ProductCRD>({
        type: RESOURCE_TYPES.CUSTOM_RESOURCE_DEFINITION,
        name,
      }),
    ),
    publishRef(),
  );

  deployment$ = combineLatest([this.cr$, this.crd$]).pipe(
    map(([cr, crd]) => ({
      cr,
      crd,
    })),
    publishRef(),
  );

  dataLoader = new AsyncDataLoader({
    fetcher: () =>
      this.k8sApi.watchGlobalResource({
        type: RESOURCE_TYPES.TIMATRIX,
        name: 'timatrix',
      }),
  });

  constructor(
    private readonly route: ActivatedRoute,
    private readonly k8sApi: K8sApiService,
  ) {}

  reload() {
    this.reload$$.next();
  }
}
