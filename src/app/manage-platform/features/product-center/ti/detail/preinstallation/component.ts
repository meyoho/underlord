import { Component, Input } from '@angular/core';

import { ProductCRD } from 'app/typings';

import { TIMatrixCR } from './../../types';

@Component({
  selector: 'alu-ti-detail-preinstallation',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class TiDetailPreinstallationComponent {
  @Input()
  deployment: {
    cr: TIMatrixCR;
    crd: ProductCRD;
  };

  deployModes = ['deploy_component', 'access_component'];

  storageModes = ['locale_storage', 'cos_storage'];

  showPassword = false;
}
