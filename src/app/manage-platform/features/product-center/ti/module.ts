import { NgModule } from '@angular/core';

import { TIApiService } from 'app/api/ti/api';
import { SharedModule } from 'app/shared/shared.module';

import { TICustomFieldsModule } from './custom-fields/module';
import { TIDeployFormComponent } from './deploy-form/component';
import { HostConfigComponent } from './deploy-form/host-config/component';
import { PlatformArgsConfigComponent } from './deploy-form/platform-args-config/component';
import { PreinstallationServiceConfigComponent } from './deploy-form/preinstallation-service-config/component';
import { TIDeployLogComponent } from './deploy-log/component';
import { TIDeployComponent } from './deploy/component';
import { TiDetailBasicComponent } from './detail/basic-info/component';
import { TIDeployDetailComponent } from './detail/component';
import { TiDetailHostConfigComponent } from './detail/host-config/component';
import { TiDetailPlatformArgsComponent } from './detail/platform-args/component';
import { TiDetailPreinstallationComponent } from './detail/preinstallation/component';
import { TIRoutingModule } from './routing.module';
import { SystemConfigDialogComponent } from './system-config/component';
import { TIDeployUpdateFormComponent } from './update-form/component';
import { TIDeployUpdateComponent } from './update/component';

@NgModule({
  imports: [SharedModule, TIRoutingModule, TICustomFieldsModule],
  declarations: [
    TIDeployComponent,
    TIDeployFormComponent,
    TIDeployDetailComponent,
    HostConfigComponent,
    PreinstallationServiceConfigComponent,
    PlatformArgsConfigComponent,
    SystemConfigDialogComponent,
    TIDeployUpdateComponent,
    TIDeployUpdateFormComponent,
    TIDeployLogComponent,
    TiDetailBasicComponent,
    TiDetailHostConfigComponent,
    TiDetailPreinstallationComponent,
    TiDetailPlatformArgsComponent,
  ],
  entryComponents: [SystemConfigDialogComponent],
  providers: [TIApiService],
})
export class TIModule {}
