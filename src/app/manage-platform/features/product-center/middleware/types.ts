export type MiddlewareTaskStatus =
  | 'Success'
  | 'Executing'
  | 'UnrecoverableError'
  | 'RecoverableError'
  | 'Terminated'
  | 'Skipped'
  | 'Paused';

export interface MiddlewareTask {
  name: string;
  message: string;
  reason: string;
  state?: MiddlewareTaskStatus;
  log?: {
    pod?: {
      name: string;
      namespace: string;
    };
    mess?: {
      message: string;
    };
  };
}

export type MiddlewareEventType =
  | 'ReProvision'
  | 'ReDelete'
  | 'Terminate'
  | 'Patch';

export interface MiddlewareEvent {
  uuid: string;
  time: string;
  type: MiddlewareEventType;
}
