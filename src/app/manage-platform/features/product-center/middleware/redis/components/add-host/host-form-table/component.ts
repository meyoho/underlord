import { StringMap, TranslateService } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Optional,
} from '@angular/core';
import { AbstractControl, FormGroupDirective, NgForm } from '@angular/forms';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';

import { IP_ADDRESS_PATTERN } from 'app/utils';

interface HostItem {
  ip: string;
  displayName: string;
}

@Component({
  selector: 'alu-host-form-table',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HostFormTableComponent extends BaseResourceFormArrayComponent<
  HostItem
> {
  ipPattern = IP_ADDRESS_PATTERN;
  rowBackgroundColorFn = (control: AbstractControl) =>
    control.invalid &&
    (control.dirty || this.parentForm.submitted) &&
    (control.errors || control.get('ip').errors)
      ? '#fdefef'
      : '';

  parentForm: NgForm | FormGroupDirective;

  constructor(
    public injector: Injector,
    private readonly translateService: TranslateService,
    @Optional() readonly formGroup: FormGroupDirective,
    @Optional() readonly ngForm: NgForm,
  ) {
    super(injector);
    this.parentForm = formGroup || ngForm;
  }

  private getPreviousIPs(index: number) {
    return this.formModel
      .slice(0, index)
      .map(({ ip }) => ip)
      .filter(ip => !!ip);
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  private createNewControl() {
    return this.fb.group(
      {
        ip: '',
        displayName: '',
      },
      {
        validator: [
          (control: AbstractControl) => {
            const index = this.form.controls.indexOf(control);
            const previousKeys = this.getPreviousIPs(index);
            const ipControl = control.get('ip');

            if (previousKeys.includes(ipControl.value)) {
              return {
                existing: ipControl.value,
              };
            } else {
              return null;
            }
          },
        ],
      },
    );
  }

  getRowErrorMessage = (errors: StringMap) => {
    if (errors.duplicateIP) {
      return this.translateService.get('duplicate_ip_error_message');
    }
  };
}
