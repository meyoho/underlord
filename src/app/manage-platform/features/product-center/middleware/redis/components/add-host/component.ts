import { TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  OnDestroy,
  Output,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { intersection } from 'lodash-es';
import { Subject, forkJoin, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { RedisApiService } from 'app/api/redis/api';
import { PORT_PATTERN } from 'app/utils';

import { HostInfo } from '../../types';

interface HostModel {
  hosts: Array<{ ip: string; displayName: string }>;
  sshPort: string;
  authMethod: 'password' | 'secret';
  username: string;
  password?: string;
  secretKey?: string;
  secretPassword?: string;
}

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class AddHostDialogComponent implements OnDestroy {
  private readonly onDestroy$ = new Subject<void>();
  @Output()
  add = new EventEmitter<HostInfo[]>();

  submitting = false;

  portPattern = PORT_PATTERN;

  model: HostModel = {
    hosts: [{ ip: '', displayName: '' }],
    sshPort: '',
    authMethod: 'password',
    username: '',
    password: '',
    secretKey: '',
    secretPassword: '',
  };

  hostsState: {
    successful: HostInfo[];
    duplicated: string[];
    failed: string[];
  } = {
    successful: [],
    duplicated: [],
    failed: [],
  };

  added: string[];

  showPassword = false;

  enableGPU = false;

  constructor(
    @Inject(DIALOG_DATA)
    private readonly data: {
      added: string[];
    } = {
      added: [],
    },
    private readonly translateService: TranslateService,
    private readonly messageService: MessageService,
    private readonly redisApi: RedisApiService,
    private readonly cdr: ChangeDetectorRef,
    private readonly dialogRef: DialogRef,
  ) {
    this.added = Array.isArray(this.data.added) ? this.data.added : [];
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  confirm(formRef: NgForm) {
    formRef.onSubmit(null);

    if (formRef.form.invalid) {
      return;
    }

    this.hostsState = {
      failed: [],
      successful: [],
      duplicated: intersection(
        this.added,
        this.model.hosts.map(({ ip }) => ip),
      ),
    };

    if (this.hostsState.duplicated.length === this.model.hosts.length) {
      return;
    }

    this.submitting = true;

    const hosts2BSubmitted = this.model.hosts.filter(
      host => !this.hostsState.duplicated.includes(host.ip),
    );

    forkJoin(
      hosts2BSubmitted.map(host =>
        this.redisApi
          .getHost({
            host: host.ip,
            port: this.model.sshPort,
            user: this.model.username,
            ...(this.model.authMethod === 'password'
              ? { password: btoa(this.model.password) }
              : {
                  key: btoa(this.model.secretKey),
                  keypass: btoa(this.model.secretPassword),
                }),
          })
          .pipe(
            map(res => ({
              ip: host.ip,
              displayName: host.displayName,
              name: res.data.hostname,
              username: this.model.username,
              port: +this.model.sshPort,
              ...(this.model.authMethod === 'password'
                ? {
                    password: this.model.password,
                  }
                : {
                    secretKey: this.model.secretKey,
                    secretPassword: this.model.secretPassword,
                  }),
              config: {
                cpu: +res.data.cpu,
                memory: +res.data.memory,
                disk: +res.data.disksize,
              },
            })),
            catchError(_ => of(host.ip)),
          ),
      ),
    ).subscribe(hosts => {
      hosts.forEach(item => {
        if (typeof item === 'string') {
          this.hostsState.failed.push(item);
        } else {
          this.added.push(item.ip);
          this.hostsState.successful.push(item);
          this.model.hosts = this.model.hosts.filter(
            host => host.ip !== item.ip,
          );
        }
      });

      if (!this.model.hosts.length) {
        this.dialogRef.close();
        this.messageService.success(
          this.translateService.get('add_hosts_succeeded', {
            number: hosts2BSubmitted.length,
          }),
        );
      }

      this.submitting = false;

      this.cdr.markForCheck();
      this.add.emit(this.hostsState.successful);
    });
  }

  getAlertTranslation = (translationKey: string, nodes: string[]) => {
    return this.translateService.get(translationKey, {
      nodes,
    });
  };
}
