import { TranslateService } from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import { Component, Injector, Input } from '@angular/core';
import { AbstractControl, FormGroup, ValidationErrors } from '@angular/forms';
import { groupBy, values } from 'lodash-es';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { Dictionary } from 'ts-essentials';

import { HostAllocation, HostHardwareConfig, HostInfo } from '../../types';
import { toFix } from '../../utils';
import { AddHostDialogComponent } from '../add-host/component';

@Component({
  selector: 'alu-host-allocate-form-table',
  templateUrl: './template.html',
  styleUrls: ['style.scss'],
})
export class HostAllocateFormTableComponent extends BaseResourceFormArrayComponent<
  HostAllocation
> {
  @Input()
  hostConfigRequirement: Dictionary<HostHardwareConfig>;

  columns = [
    'name',
    'hostIp',
    'machineConfig',
    'proxyCache',
    'controlEquip',
    'lvs',
    'influxdb',
    'action',
  ];

  toFix = toFix;

  isDisabled = false;

  added: string[] = [];

  groupNumber = 0;

  constructor(
    public injector: Injector,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
  ) {
    super(injector);
  }

  getInvalidTip = (name: string) => {
    return this.translate.get('host_config_unqualified', {
      ...this.hostConfigRequirement[name],
      key: this.translate.get(this.hostConfigRequirement[name].key),
    });
  };

  checkStateChange(checked: boolean, name: string, control: AbstractControl) {
    if (name === 'proxyCache') {
      if (checked) {
        ['controlEquip', 'lvs', 'influxdb'].forEach(name =>
          control.get(name).disable(),
        );
      } else {
        ['controlEquip', 'lvs', 'influxdb'].forEach(name =>
          control.get(name).enable(),
        );
      }
    } else {
      if (checked) {
        control.get('proxyCache').disable();
      } else {
        control.get('proxyCache').enable();
      }
    }
  }

  addHost() {
    const dialogRef = this.dialog.open(AddHostDialogComponent, {
      size: DialogSize.Big,
      data: {
        added: this.added,
      },
    });
    dialogRef.componentInstance.add.subscribe((ret: HostInfo[]) => {
      this.createControls(
        ret.map(info => ({
          ...info,
          groupNumber: this.groupNumber,
          proxyCache: false,
          lvs: false,
          controlEquip: false,
          influxdb: false,
        })),
      );
      this.added = [...this.added, ...ret.map(item => item.ip)];
      this.groupNumber++;
      this.cdr.markForCheck();
    });
  }

  adaptResourceModel(resource: HostAllocation[]) {
    if (!resource) {
      return resource;
    }
    this.added = resource.map(item => item.ip);
    this.groupNumber = values(groupBy(resource, 'groupNumber')).length;
    return resource;
  }

  removeHost(index: number) {
    this.form.removeAt(index);
    this.form.controls = [...this.form.controls];
    this.added = (this.form.value as HostAllocation[]).map(item => item.ip);
  }

  createForm() {
    return this.fb.array([]);
  }

  getOnFormArrayResizeFn() {
    return () =>
      this.createNewControl({
        ip: '',
        displayName: '',
        name: '',
        username: '',
        port: null,
        config: {
          cpu: null,
          memory: null,
          disk: null,
        },
        password: '',
        secretPassword: '',
        secretKey: '',
        groupNumber: 0,
        proxyCache: false,
        lvs: false,
        controlEquip: false,
        influxdb: false,
      });
  }

  getDefaultFormModel() {
    return [] as HostAllocation[];
  }

  setDisabledState(disabled: boolean) {
    this.isDisabled = disabled;
    if (disabled) {
      this.form.controls.forEach((control: FormGroup) => control.disable());
    } else {
      this.form.controls.forEach((control: FormGroup) => control.enable());
    }
  }

  private createControls(hosts: HostAllocation[]) {
    this.form.controls = [
      ...this.form.controls,
      ...hosts.map(host => this.createNewControl(host)),
    ];
    this.form.updateValueAndValidity();
  }

  private createNewControl(fields: HostAllocation) {
    return this.fb.group(fields, {
      validators: [this.machineConfigValidator],
    });
  }

  machineConfigValidator = (control: FormGroup): ValidationErrors => {
    if (control && this.hostConfigRequirement) {
      const config = control.value.config;
      const errors = ['proxyCache', 'controlEquip', 'lvs', 'influxdb']
        .map(key => {
          return control.value[key]
            ? config.cpu < this.hostConfigRequirement[key].cpu ||
              config.memory <
                this.hostConfigRequirement[key].memory * 1000 * 1000 ||
              config.disk < this.hostConfigRequirement[key].disk
              ? {
                  [key]: true,
                }
              : null
            : null;
        })
        .filter(error => !!error);
      return errors.length
        ? errors.reduce((acc, cur) => {
            return {
              ...acc,
              ...cur,
            };
          }, {})
        : null;
    }
    return null;
  };
}
