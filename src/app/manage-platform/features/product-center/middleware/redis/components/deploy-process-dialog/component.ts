import { publishRef } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { Component, Inject } from '@angular/core';
import { get } from 'lodash-es';
import { Observable } from 'rxjs';
import { filter, map, pluck, tap } from 'rxjs/operators';

import { RedisApiService } from 'app/api/redis/api';
import { mapDeployStatus } from 'app/manage-platform/features/product-center/utils';
import { ProductCRD } from 'app/typings';

import { taskStatusMap } from '../../../tdsql/types';
import { MiddlewareTask } from '../../../types';
import { RedisCR } from '../../types';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class RedisDeployProcessDialogComponent {
  logLoader = {
    context: this.redisApi,
    func: this.redisApi.getLog,
  };

  cr: RedisCR;
  crd: ProductCRD;

  cr$ = this.data.data$.pipe(
    filter(data => !!data),
    tap(({ cr, crd }) => {
      this.cr = cr;
      this.crd = crd;
    }),
    pluck('cr'),
    publishRef(),
  );

  status$ = this.cr$.pipe(
    map(cr => mapDeployStatus(cr)),
    publishRef(),
  );

  tasks$ = this.cr$.pipe(
    map(cr => get(cr, 'status.tasks', []) as MiddlewareTask[]),
    publishRef(),
  );

  steps$ = this.tasks$.pipe(
    map(tasks =>
      tasks.map((task, idx) => ({
        index: idx + 1,
        label: `redis_${task.name.replace('-', '_').toLowerCase()}_step`,
        status: task.state ? taskStatusMap[task.state] : 'Waiting',
        log: task.log,
      })),
    ),
  );

  redployable$ = this.tasks$.pipe(
    map(tasks => tasks.filter(task => task.state === 'Success').length >= 2),
  );

  constructor(
    @Inject(DIALOG_DATA)
    private readonly data: {
      data$: Observable<{ crd: ProductCRD; cr: RedisCR }>;
    },
    private readonly dialogRef: DialogRef,
    private readonly redisApi: RedisApiService,
  ) {}

  redeploy() {
    this.redisApi.reprovision(this.cr).subscribe(() => {
      this.dialogRef.close('reload');
    });
  }

  terminate() {
    this.redisApi.terminate(this.cr).subscribe(() => {
      this.dialogRef.close('reload');
    });
  }

  edit() {
    this.dialogRef.close('edit');
  }
}
