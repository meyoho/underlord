import { ObservableInput, TranslateService } from '@alauda/common-snippet';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { values } from 'lodash-es';
import { Observable } from 'rxjs';
import { Dictionary } from 'ts-essentials';

import {
  HostHardwareConfig,
  HostOptionRequirement,
  RedisPlanFormModel,
} from '../../types';

@Component({
  selector: 'alu-redis-deploy-plan-form',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class RedisDeployPlanFormComponent implements OnInit {
  @ViewChild('ngForm')
  ngForm: NgForm;

  @Input()
  model: RedisPlanFormModel;

  @ObservableInput(true)
  model$: Observable<RedisPlanFormModel>;

  form: FormGroup;

  isDisabled = false;

  hostConfigRequirement: Dictionary<HostHardwareConfig> = {
    proxyCache: {
      key: 'proxy_or_cache',
      cpu: 4,
      memory: 16,
      disk: 128,
    },
    controlEquip: {
      key: 'control_equipment',
      cpu: 4,
      memory: 16,
      disk: 64,
    },
    lvs: {
      key: 'lvs',
      cpu: 4,
      memory: 16,
      disk: 128,
    },
    influxdb: {
      key: 'influxdb',
      cpu: 4,
      memory: 16,
      disk: 64,
    },
  };

  basicRquirement: HostOptionRequirement = {
    lvs: { count: 1, operator: '>=' },
    influxdb: { count: 1, operator: '===' },
    controlEquip: { count: 1, operator: '>=' },
  };

  hostNumberRequirementMap: Dictionary<HostOptionRequirement> = {
    'minimal.standard': {
      proxyCache: { count: 6, operator: '>=' },
    },
    'minimal.cluster': {
      proxyCache: { count: 9, operator: '>=' },
    },
    'ha.standard': {
      lvs: { count: 2, operator: '>=' },
      controlEquip: { count: 2, operator: '>=' },
      proxyCache: { count: 6, operator: '>=' },
    },
    'ha.cluster': {
      lvs: { count: 2, operator: '>=' },
      controlEquip: { count: 2, operator: '>=' },
      proxyCache: { count: 9, operator: '>=' },
    },
  };

  constructor(
    private readonly fb: FormBuilder,
    private readonly translate: TranslateService,
  ) {}

  ngOnInit() {
    this.form = this.fb.group({
      deployPlan: null,
      systemParamsConfig: null,
      dataDirPrepare: null,
      deployMode: null,
      deployVersion: null,
      hostsAllocation: [],
    });

    this.model$.subscribe(model => {
      if (model) {
        this.form.patchValue(model);
      }
    });
  }

  getConfigRequirements(requirement: Dictionary<HostHardwareConfig>) {
    return values(requirement);
  }

  checkStateChange(checked: boolean, name: string) {
    if (!checked) {
      this.form.get(name).patchValue(null);
    }
  }

  getHostNumberRequirement = ([deployMode, deployVersion]: [
    number,
    number,
  ]) => {
    if (!deployMode || !deployVersion) {
      return null;
    }
    const key = `${deployMode === 1 ? 'minimal' : 'ha'}.${
      deployVersion === 1 ? 'standard' : 'cluster'
    }`;
    return {
      ...this.basicRquirement,
      ...this.hostNumberRequirementMap[key],
    };
  };

  getHostNumberTip = ([deployMode, deployVersion]: [number, number]) => {
    const requirement = this.getHostNumberRequirement([
      deployMode,
      deployVersion,
    ]);
    return this.translate.get(
      deployMode === 1
        ? 'host_number_minimal_requirement'
        : 'host_number_ha_requirement',
      {
        proxyCache: requirement.proxyCache.count,
        controlEquip: requirement.controlEquip.count,
        lvs: requirement.lvs.count,
        influxdb: requirement.influxdb.count,
      },
    );
  };

  getConfigTipItem = (config: HostHardwareConfig) => {
    return this.translate.get('host_config_requirement', {
      key: this.translate.get(config.key),
      cpu: config.cpu,
      memory: config.memory,
      disk: config.disk,
    });
  };

  submit() {
    this.ngForm.onSubmit(null);
    if (this.ngForm.valid || this.form.disabled) {
      return this.ngForm.value as RedisPlanFormModel;
    }
    return null;
  }
}
