import { ObservableInput } from '@alauda/common-snippet';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

import { IP_ADDRESS_PATTERN, PORT_PATTERN } from 'app/utils';

import { RedisConfigFormModel } from '../../types';

@Component({
  selector: 'alu-redis-deployment-form',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class RedisConfigFormComponent implements OnInit {
  @ViewChild('ngForm')
  ngForm: NgForm;

  @Input()
  deployMode: 1 | 2 = 1;

  @Input()
  model: RedisConfigFormModel;

  @ObservableInput(true)
  model$: Observable<RedisConfigFormModel>;

  form: FormGroup;

  ipPattern = IP_ADDRESS_PATTERN;
  portPattern = PORT_PATTERN;

  isDisabled = false;

  pending = false;

  constructor(private readonly fb: FormBuilder) {}

  ngOnInit() {
    this.form = this.fb.group({
      control_db_host: ['', Validators.required],
      control_db_port: ['', Validators.required],
      control_db_user: ['', Validators.required],
      control_db_password: ['', Validators.required],
      lvs_vip: [''],
      lvs_network_interface_card: ['eth0', Validators.required],
    });

    this.model$.subscribe(model => {
      if (!model) return;
      this.form.patchValue(model);
    });
  }

  submit() {
    this.ngForm.onSubmit(null);
    if (this.ngForm.valid || this.form.disabled) {
      return this.ngForm.value as RedisConfigFormModel;
    }
    return null;
  }
}
