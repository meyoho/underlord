import {
  Directive,
  Input,
  OnChanges,
  SimpleChanges,
  forwardRef,
} from '@angular/core';
import {
  AbstractControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
  ValidatorFn,
} from '@angular/forms';
import { toPairs } from 'lodash-es';

import { HostAllocation, HostOptionRequirement } from '../../types';

@Directive({
  selector: 'alu-host-allocate-form-table[aluHostNumberRequirement]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => HostNumberValidatorDirective),
      multi: true,
    },
  ],
})
export class HostNumberValidatorDirective implements Validator, OnChanges {
  @Input()
  aluHostNumberRequirement: HostOptionRequirement;

  private _validator: ValidatorFn;
  private _onChange: () => void;

  ngOnChanges({ aluHostNumberRequirement }: SimpleChanges): void {
    if (aluHostNumberRequirement) {
      this._createValidator();
      if (this._onChange) {
        this._onChange();
      }
    }
  }

  validate(c: AbstractControl): ValidationErrors | null {
    return this._validator(c);
  }

  registerOnValidatorChange(fn: () => void): void {
    this._onChange = fn;
  }

  private _createValidator(): void {
    this._validator = this.validator;
  }

  private isSatisfied(
    value: number,
    requirement: {
      count: number;
      operator: string;
    },
  ) {
    switch (requirement.operator) {
      case '>=':
        return value >= requirement.count;
        break;
      case '===':
        return value === requirement.count;
      case '<=':
        return value <= requirement.count;
      default:
        return false;
    }
  }

  validator(control: AbstractControl): ValidationErrors {
    if (control && control.value) {
      if (!this.aluHostNumberRequirement) {
        return null;
      }
      const hostOptionSelect = this.countHosts(control.value);
      const invalids = toPairs(hostOptionSelect)
        .map((pair: [string, number]) => {
          return this.isSatisfied(
            pair[1],
            this.aluHostNumberRequirement[pair[0]],
          );
        })
        .filter(valid => !valid);

      return invalids.length
        ? {
            hostNumber: true,
          }
        : null;
    }
    return null;
  }

  private countHosts(value: HostAllocation[]) {
    return value.reduce(
      (acc, cur) => {
        return {
          controlEquip: acc.controlEquip + (cur.controlEquip ? 1 : 0),
          proxyCache: acc.proxyCache + (cur.proxyCache ? 1 : 0),
          lvs: acc.lvs + (cur.lvs ? 1 : 0),
          influxdb: acc.influxdb + (cur.influxdb ? 1 : 0),
        };
      },
      {
        controlEquip: 0,
        proxyCache: 0,
        lvs: 0,
        influxdb: 0,
      },
    );
  }
}
