import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { MiddlewareComponentsModule } from '../../components/module';

import { AddHostDialogComponent } from './add-host/component';
import { HostFormTableComponent } from './add-host/host-form-table/component';
import { RedisConfigFormComponent } from './config-form/component';
import { RedisDeployProcessDialogComponent } from './deploy-process-dialog/component';
import { HostAllocateFormTableComponent } from './host-allocate-form-table/component';
import { HostNumberValidatorDirective } from './host-allocate-form-table/validator';
import { RedisDeployPlanFormComponent } from './plan-form/component';

const components = [
  RedisDeployPlanFormComponent,
  AddHostDialogComponent,
  HostFormTableComponent,
  HostAllocateFormTableComponent,
  RedisDeployPlanFormComponent,
  RedisConfigFormComponent,
  RedisDeployProcessDialogComponent,
];

@NgModule({
  imports: [SharedModule, MiddlewareComponentsModule],
  declarations: [...components, HostNumberValidatorDirective],
  exports: [...components, HostNumberValidatorDirective],
  entryComponents: [AddHostDialogComponent, RedisDeployProcessDialogComponent],
  providers: [],
})
export class RedisComponentsModule {}
