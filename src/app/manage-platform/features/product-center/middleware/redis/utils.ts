import { get, groupBy, values } from 'lodash-es';

import {
  HostAllocation,
  RedisCR,
  RedisConfigFormModel,
  RedisPlanFormModel,
} from './types';

export function mapToConfigFormModel(cr: RedisCR): RedisConfigFormModel {
  if (!cr) {
    return null;
  }

  const databaseConfig = get(cr, 'spec.database');
  const lvsConfig = get(cr, 'spec.role.lvs');

  return {
    control_db_host: get(databaseConfig, 'host'),
    control_db_port: get(databaseConfig, 'port'),
    control_db_user: get(databaseConfig, 'user'),
    control_db_password: atob(get(databaseConfig, 'password', '')),
    lvs_vip: get(lvsConfig, 'vip', [])[0],
    lvs_network_interface_card: get(lvsConfig, 'interface', 'eth0'),
  };
}

export function mapToPlanFormModel(cr: RedisCR): RedisPlanFormModel {
  if (!cr) {
    return null;
  }
  return {
    deployPlan: true,
    systemParamsConfig: true,
    dataDirPrepare: true,
    deployMode: cr.spec.option.mode,
    deployVersion: cr.spec.option.version,
    hostsAllocation: getHostAllocations(cr),
  };
}

export function getHostAllocations(cr: RedisCR) {
  if (!cr) {
    return [] as HostAllocation[];
  }
  const role = cr.spec.role;
  return cr.spec.ssh.reduce((acc, cur, i) => {
    return [
      ...acc,
      ...cur.host.map(item => {
        return {
          ip: item.ip,
          name: item.name,
          displayName: item.displayName,
          config: {
            cpu: item.cpu,
            memory: item.memory,
            disk: item.disk,
          },
          groupNumber: i,
          username: cur.user,
          port: cur.port,
          password: cur.password ? atob(cur.password) : '',
          secretKey: cur.rsa ? atob(cur.rsa) : '',
          secretPassword: cur.rsaPass ? atob(cur.rsaPass) : '',
          proxyCache: role.cacheAgent.includes(item.ip),
          controlEquip: role.cc.includes(item.ip),
          lvs: [role.lvs.master, ...(role.lvs.slave || [])].includes(item.ip),
          influxdb: role.influx === item.ip,
        };
      }),
    ];
  }, [] as HostAllocation[]);
}

// eslint-disable-next-line sonarjs/cognitive-complexity
export function mapToPayload(
  version: string,
  namespace: string,
  model: {
    planModel: RedisPlanFormModel;
    configModel: RedisConfigFormModel;
  },
  cr: RedisCR,
): RedisCR {
  const hostAllocationMap = model.planModel.hostsAllocation.reduce(
    (acc, cur) => {
      return {
        lvs: cur.lvs ? [...acc.lvs, cur.ip] : acc.lvs,
        proxyCache: cur.proxyCache
          ? [...acc.proxyCache, cur.ip]
          : acc.proxyCache,
        controlEquip: cur.controlEquip
          ? [...acc.controlEquip, cur.ip]
          : acc.controlEquip,
        influxdb: cur.influxdb ? [...acc.influxdb, cur.ip] : acc.influxdb,
      };
    },
    {
      lvs: [],
      proxyCache: [],
      controlEquip: [],
      influxdb: [],
    },
  );

  return {
    apiVersion: 'product.alauda.io/v1alpha1',
    kind: 'RedisInstaller',
    metadata: get(cr, 'metadata', {
      name: 'redisinstaller-home',
    }),
    spec: {
      version,
      installNamespace: namespace,
      option: {
        mode: model.planModel.deployMode,
        version: model.planModel.deployVersion,
      },
      ssh: values(
        groupBy(model.planModel.hostsAllocation, item => item.groupNumber),
      ).map(allos => ({
        host: allos.map(allo => ({
          ip: allo.ip,
          name: allo.name,
          displayName: allo.displayName,
          ...allo.config,
        })),
        port: allos[0].port,
        user: allos[0].username,
        ...(allos[0].password
          ? {
              password: btoa(allos[0].password),
            }
          : {}),
        ...(allos[0].secretKey
          ? {
              rsa: btoa(allos[0].secretKey),
            }
          : {}),
        ...(allos[0].secretPassword
          ? {
              rsaPass: btoa(allos[0].secretPassword),
            }
          : {}),
      })),
      database: {
        host: model.configModel.control_db_host,
        port: +model.configModel.control_db_port,
        user: model.configModel.control_db_user,
        password: btoa(model.configModel.control_db_password),
      },
      role: {
        lvs: {
          vip: model.configModel.lvs_vip ? [model.configModel.lvs_vip] : [],
          interface: model.configModel.lvs_network_interface_card,
          master: hostAllocationMap.lvs[0],
          slave: hostAllocationMap.lvs.slice(1),
        },
        influx: hostAllocationMap.influxdb[0],
        cc: hostAllocationMap.controlEquip,
        cacheAgent: hostAllocationMap.proxyCache,
      },
    },
    ...(cr
      ? {
          status: cr.status,
        }
      : {}),
  };
}

export function toFix(value: number, precision: number) {
  return Math.round(value * Math.pow(10, precision)) / Math.pow(10, precision);
}
