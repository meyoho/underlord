import {
  COMMON_WRITABLE_ACTIONS,
  K8sPermissionService,
  K8sUtilService,
  ObservableInput,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogRef, DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  TemplateRef,
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { findLast, get } from 'lodash-es';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { ProductCenterListApi } from 'app/api/product-center/list-api';
import { RedisApiService } from 'app/api/redis/api';
import { ProductStatusMap } from 'app/manage-platform/features/product-center/constants';
import {
  mapDeployStatus,
  mapOperatingStatus,
  mapStatus,
} from 'app/manage-platform/features/product-center/utils';
import {
  ConfirmDeleteComponent,
  DeleteType,
} from 'app/shared/components/confirm-delete/confirm-delete.component';
import { ProductCRD } from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';

import { MiddlewareLogDialogComponent } from '../../../components/log-dialog/component';
import { MiddlewareTask } from '../../../types';
import { RedisDeployProcessDialogComponent } from '../../components/deploy-process-dialog/component';
import { RedisCR } from '../../types';

@Component({
  selector: 'alu-redis-detail-info',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RedisDetailInfoComponent {
  @ObservableInput('data')
  data$: Observable<{ crd: ProductCRD; cr: RedisCR }>;

  @Input()
  data: { crd: ProductCRD; cr: RedisCR };

  @Output()
  reload = new EventEmitter();

  @Output()
  edit = new EventEmitter();

  permissions$ = this.k8sPermissionService.isAllowed({
    type: RESOURCE_TYPES.PORTAL,
    action: COMMON_WRITABLE_ACTIONS,
  });

  dialogRef: DialogRef<ConfirmDeleteComponent>;

  type = DeleteType;

  editorActions = {
    diffMode: false,
    clear: false,
    recover: false,
    copy: false,
    find: true,
    export: false,
    import: false,
  };

  constructor(
    private readonly dialogService: DialogService,
    private readonly translateService: TranslateService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly k8sUtilService: K8sUtilService,
    private readonly sanitizer: DomSanitizer,
    private readonly k8sPermissionService: K8sPermissionService,
    private readonly productCenterListApi: ProductCenterListApi,
    private readonly redisApi: RedisApiService,
  ) {}

  getBrief = (crd: ProductCRD) => {
    return this.k8sUtilService.getLabel(crd, 'brief');
  };

  getLogo = (crd: ProductCRD) => {
    const url = this.k8sUtilService.getAnnotation(crd, 'product-logo');
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  };

  mapDeployStatus = mapDeployStatus;

  geOperatingStatus = (cr: RedisCR) => {
    return mapOperatingStatus(cr);
  };

  getProductName = (crd: ProductCRD) => {
    return this.k8sUtilService.getDisplayName(crd);
  };

  getProductType = (crd: ProductCRD) => {
    return this.k8sUtilService.getLabel(crd, 'product-type').toLowerCase();
  };

  getDeploymentMode = (cr: RedisCR) => {
    return `${this.translateService.get(
      get(cr, 'spec.option.mode') === 1
        ? 'minimal_deployment'
        : 'high_availability_deployment',
    )} ${this.translateService.get(
      get(cr, 'spec.option.version') === 1
        ? 'standard_version'
        : 'cluster_version',
    )}`;
  };

  getDbHost = (cr: RedisCR) => {
    return get(cr, 'spec.database.host');
  };

  getDbPort(cr: RedisCR) {
    return get(cr, 'spec.database.port');
  }

  getDbUser = (cr: RedisCR) => {
    return get(cr, 'spec.database.user');
  };

  getLvsVip(cr: RedisCR) {
    return get(cr, 'spec.role.lvs.vip');
  }

  getLvsNetworkInterface(cr: RedisCR) {
    return get(cr, 'spec.role.lvs.interface');
  }

  getVersion = (data: { crd: ProductCRD; cr: RedisCR }) => {
    return data.cr && get(data.cr, 'status.version')
      ? get(data.cr, 'status.version')
      : this.k8sUtilService.getLabel(data.crd, 'available-version');
  };

  handleUpgradeable = (data: { crd: ProductCRD; cr: RedisCR }) => {
    if (mapStatus(data.cr).type !== 'Running') {
      return {
        disabled: true,
        tooltip: 'non_running_status_upgrade_not_supported',
      };
    } else {
      return this.k8sUtilService.getLabel(data.crd, 'available-version') ===
        get(data.cr, 'spec.version')
        ? {
            disabled: true,
            tooltip: 'no_new_version_not_executable',
          }
        : {
            disabled: false,
            tooltip: null,
          };
    }
  };

  isDeletable = (cr: RedisCR) => {
    return ![ProductStatusMap.Deleting.type].includes(mapStatus(cr).type)
      ? get(cr, 'status.deletable', true)
      : false;
  };

  upgrade() {
    // TODO: upgrading redis
  }

  deleteResourceApi = () => {
    this.productCenterListApi
      .deleteProduct(this.data.crd, this.data.cr)
      .subscribe(() => {
        this.closeDialog();
        this.router.navigate(['../../../list'], { relativeTo: this.route });
      });
  };

  uninstall(templateRef: TemplateRef<ConfirmDeleteComponent>) {
    this.dialogRef = this.dialogService.open(templateRef);
  }

  closeDialog() {
    this.dialogRef.close();
  }

  viewProcessLog() {
    const dialogRef = this.dialogService.open(
      RedisDeployProcessDialogComponent,
      {
        size: DialogSize.Large,
        data: {
          data$: this.data$,
        },
      },
    );

    dialogRef.afterClosed().subscribe(event => {
      if (event === 'reload') {
        this.reload.emit();
      } else if (event === 'edit') {
        this.edit.emit();
      }
    });
  }

  viewFailedLog() {
    this.data$
      .pipe(
        switchMap(({ cr }) => {
          const tasks = get(cr, 'status.tasks', []) as MiddlewareTask[];

          const firstFailedTask = findLast(
            tasks,
            task => task.state === 'UnrecoverableError',
          );

          if (firstFailedTask) {
            if (firstFailedTask.log.mess) {
              return of(firstFailedTask.log.mess);
            } else {
              const { name, namespace } = firstFailedTask.log.pod;
              return this.redisApi.getLog(name, namespace);
            }
          } else {
            return of('');
          }
        }),
      )
      .subscribe(log => {
        this.dialogService.open(MiddlewareLogDialogComponent, {
          size: DialogSize.Large,
          data: { log },
        });
      });
  }
}
