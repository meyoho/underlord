import { AsyncDataLoader, K8sUtilService } from '@alauda/common-snippet';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map, pluck } from 'rxjs/operators';

import { RedisApiService } from 'app/api/redis/api';
import { ProductCRD } from 'app/typings';

import { mapStatus } from '../../../utils';
import { RedisCR } from '../types';

@Component({
  templateUrl: './template.html',
})
export class RedisDetailComponent {
  dataLoader = new AsyncDataLoader<{ crd: ProductCRD; cr: RedisCR }, string>({
    params$: this.route.params.pipe(
      pluck('name'),
      map(name => atob(name)),
    ),
    fetcher: name =>
      this.redisApi
        .getProduct(name)
        .pipe(map(({ crd, crs }) => ({ crd, cr: crs[0] }))),
    interval: 1000 * 15,
    intervalFilter: this.intervalFilter.bind(this),
  });

  constructor(
    private readonly route: ActivatedRoute,
    private readonly redisApi: RedisApiService,
    private readonly router: Router,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  intervalFilter() {
    const { data } = this.dataLoader.snapshot;
    const status = mapStatus(data.cr);
    return ['InDeployment', 'Deleting'].includes(status.type);
  }

  edit() {
    this.dataLoader.data$.subscribe(data => {
      this.router.navigate(
        ['../../deploy', this.k8sUtil.getLabel(data.crd, 'available-version')],
        { relativeTo: this.route },
      );
    });
  }
}
