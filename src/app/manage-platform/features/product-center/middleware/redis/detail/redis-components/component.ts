import { ObservableInput, publishRef } from '@alauda/common-snippet';
import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { filter, map, pluck } from 'rxjs/operators';

import { ProductCRD } from 'app/typings';

import { HostInfo, RedisCR } from '../../types';
import { getHostAllocations, toFix } from '../../utils';

@Component({
  selector: 'alu-redis-components',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class RedisComponentsComponent {
  @ObservableInput()
  @Input('data')
  data$: Observable<{ crd: ProductCRD; cr: RedisCR }>;

  columns = [
    'name',
    'host',
    'machine_configuration',
    'proxy_cache',
    'control_equipment',
    'lvs',
    'influx_db',
  ];

  toFix = toFix;

  dataSource$: Observable<HostInfo[]> = this.data$.pipe(
    filter(data => !!data),
    pluck('cr'),
    map(cr => getHostAllocations(cr)),
    publishRef(),
  );
}
