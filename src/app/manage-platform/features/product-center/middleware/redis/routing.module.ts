import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RedisDeployComponent } from './deploy/component';
import { RedisDetailComponent } from './detail/component';

const routes: Routes = [
  { path: '', redirectTo: 'deploy', pathMatch: 'full' },
  {
    path: 'deploy/:version',
    component: RedisDeployComponent,
  },
  {
    path: 'detail/:name',
    component: RedisDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RedisRoutingModule {}
