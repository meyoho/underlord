import { NgModule } from '@angular/core';

import { RedisApiService } from 'app/api/redis/api';
import { SharedModule } from 'app/shared/shared.module';

import { RedisComponentsModule } from './components/module';
import { RedisDeployComponent } from './deploy/component';
import { RedisDetailComponent } from './detail/component';
import { RedisComponentsComponent } from './detail/redis-components/component';
import { RedisDetailInfoComponent } from './detail/redis-info/component';
import { RedisRoutingModule } from './routing.module';

@NgModule({
  declarations: [
    RedisDeployComponent,
    RedisDetailComponent,
    RedisDetailInfoComponent,
    RedisComponentsComponent,
  ],
  imports: [SharedModule, RedisRoutingModule, RedisComponentsModule],
  exports: [RedisDeployComponent],
  providers: [RedisApiService],
})
export class RedisModule {}
