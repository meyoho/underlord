import { Dictionary } from '@alauda/common-snippet';

import { ProductCR, ProductStatusCondition } from 'app/typings';

import { MiddlewareEvent, MiddlewareTask } from '../types';

export interface RedisConfigFormModel {
  control_db_host: string;
  control_db_port: number;
  control_db_user: string;
  control_db_password: string;
  lvs_vip: string;
  lvs_network_interface_card: string;
}

export interface RedisPlanFormModel {
  deployPlan: boolean;
  systemParamsConfig: boolean;
  dataDirPrepare: boolean;
  deployMode: number;
  deployVersion: number;
  hostsAllocation: HostAllocation[];
}

export interface HostAllocation extends HostInfo {
  groupNumber: number;
  proxyCache: boolean;
  lvs: boolean;
  controlEquip: boolean;
  influxdb: boolean;
}

export interface HostHardwareConfig {
  key: string;
  cpu: number;
  memory: number;
  disk: number;
}

export type HostOptionRequirement = Dictionary<{
  count: number;
  operator: '>=' | '===' | '<=';
}>;

export interface HostInfo {
  ip: string;
  displayName: string;
  name: string;
  username: string;
  password?: string;
  port: number;
  secretKey?: string;
  secretPassword?: string;
  config: {
    cpu: number;
    memory: number;
    disk: number;
  };
}

export interface HostOption {
  proxyCache?: boolean;
  controlEquip?: boolean;
  lvs?: boolean;
  influxdb?: boolean;
}

export interface HostResponse {
  data: {
    cpu: string;
    memory: string;
    disksize: string;
    hostname: string;
  };
  errors: Dictionary<any>;
}

interface RedisSSHConfig {
  host: Array<{
    ip: string;
    name: string;
    displayName: string;
    cpu: number;
    memory: number;
    disk: number;
  }>;
  port: number;
  user: string;
  password?: string;
  rsa?: string;
  rsaPass?: string;
}

export interface RedisSpec {
  version: string;
  installNamespace: string;
  option: {
    mode: number;
    version: number;
  };
  ssh: RedisSSHConfig[];
  database: {
    host: string;
    port: number;
    user: string;
    password: string;
  };
  role: {
    lvs: {
      master: string;
      slave: string[];
      vip: string[];
      interface: string;
    };
    influx: string;
    cc: string[];
    cacheAgent: string[];
  };
  event?: MiddlewareEvent;
}

export type RedisCRPhase = 'Provisioning' | 'Running' | 'Failed' | 'Deleting';

export type RedisDeployState =
  | RedisCRPhase
  | 'ToBeDeployed'
  | 'EnvChecking'
  | 'EnvCheckFailed';

export interface RedisCR extends ProductCR {
  apiVersion: 'product.alauda.io/v1alpha1';
  kind: 'RedisInstaller';
  spec: RedisSpec;
  status?: {
    version: string;
    phase?: RedisCRPhase;
    productEntrypoint: string;
    deletable: boolean;
    conditions: ProductStatusCondition[];
    tasks: MiddlewareTask[];
  };
}
