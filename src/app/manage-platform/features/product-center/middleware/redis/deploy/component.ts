import {
  K8sApiService,
  K8sUtilService,
  TOKEN_GLOBAL_NAMESPACE,
  TranslateService,
  noop,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import {
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';

import { ProductCenterListApi } from 'app/api/product-center/list-api';
import { RedisApiService } from 'app/api/redis/api';
import { ProductCRD } from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils/constants';

import { RedisConfigFormComponent } from '../components/config-form/component';
import { RedisDeployPlanFormComponent } from '../components/plan-form/component';
import { RedisCR, RedisConfigFormModel, RedisPlanFormModel } from '../types';
import {
  mapToConfigFormModel,
  mapToPayload,
  mapToPlanFormModel,
} from '../utils';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class RedisDeployComponent implements OnInit {
  @ViewChild(RedisDeployPlanFormComponent)
  planForm: RedisDeployPlanFormComponent;

  @ViewChild(RedisConfigFormComponent)
  configForm: RedisConfigFormComponent;

  steps = ['deploy_plan', 'config_and_deploy'];
  currentStep = 1;
  planFormModel: RedisPlanFormModel;
  configFormModel: RedisConfigFormModel;
  version = this.route.snapshot.paramMap.get('version');
  data: {
    cr: RedisCR;
    crd: ProductCRD;
  };

  initialized = false;

  data$ = this.listApi.getProduct('redisinstallers.product.alauda.io').pipe(
    map(product => ({
      cr: (product.crs ? product.crs[0] : null) as RedisCR,
      crd: product.crd,
    })),
    publishRef(),
  );

  constructor(
    @Inject(TOKEN_GLOBAL_NAMESPACE) private readonly globalNamespace: string,
    private readonly listApi: ProductCenterListApi,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly k8sUtilService: K8sUtilService,
    private readonly dialogService: DialogService,
    private readonly translateService: TranslateService,
    private readonly k8sApi: K8sApiService,
    private readonly redisApi: RedisApiService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.data$.subscribe(data => {
      this.data = data;
      this.currentStep = data.cr ? 2 : 1;
      this.planFormModel = mapToPlanFormModel(data.cr);
      this.configFormModel = mapToConfigFormModel(data.cr);
      this.initialized = true;
      this.cdr.markForCheck();
    });
  }

  deploy() {
    this.configFormModel = this.configForm.submit();

    if (!this.configFormModel) {
      return;
    }

    const payload = mapToPayload(
      this.version,
      this.globalNamespace,
      {
        planModel: this.planFormModel,
        configModel: this.configFormModel,
      },
      this.data.cr,
    );

    if (this.data.cr) {
      this.redisApi.reprovision(payload).subscribe(() => {
        this.navigateToDetailPage();
      });
    } else {
      this.k8sApi
        .postGlobalResource({
          type: RESOURCE_TYPES.REDIS,
          resource: payload,
          namespaced: false,
        })
        .subscribe(() => {
          this.navigateToDetailPage();
        });
    }
  }

  navigateToDetailPage() {
    this.router.navigate(
      ['../../detail', btoa(this.k8sUtilService.getName(this.data.crd))],
      { relativeTo: this.route },
    );
  }

  cancel() {
    this.dialogService
      .confirm({
        title: this.translateService.get('confirm_cancel_deploy'),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      })
      .then(() => {
        if (this.data.cr) {
          this.navigateToDetailPage();
        } else {
          this.router.navigate(['../../../', 'list'], {
            relativeTo: this.route,
          });
        }
      })
      .catch(noop);
  }

  prev() {
    this.configFormModel = this.configForm.submit();
    if (this.configFormModel) {
      this.currentStep--;
    }
  }

  next() {
    this.planFormModel = this.planForm.submit();
    if (this.planFormModel) {
      this.currentStep++;
    }
  }
}
