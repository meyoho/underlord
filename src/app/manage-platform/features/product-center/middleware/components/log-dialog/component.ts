import { CodeEditorActionsConfig } from '@alauda/code-editor';
import {
  IEditorConstructionOptions,
  readonlyOptions,
  viewActions,
} from '@alauda/common-snippet';
import { DIALOG_DATA } from '@alauda/ui';
import { Component, Inject, OnInit } from '@angular/core';
import { editor } from 'monaco-editor';
import { Observable, Subject, combineLatest, of } from 'rxjs';

@Component({
  selector: 'alu-log-dialog',
  templateUrl: './template.html',
})
export class MiddlewareLogDialogComponent implements OnInit {
  editorOptions: IEditorConstructionOptions = {
    ...readonlyOptions,
    language: 'log',
  };

  actionsConfig: CodeEditorActionsConfig = {
    ...viewActions,
    import: false,
    copy: false,
    export: false,
  };

  editorChanged$$ = new Subject<editor.ICodeEditor>();

  log$: Observable<string>;

  constructor(
    @Inject(DIALOG_DATA)
    private readonly data: {
      log: string;
    },
  ) {}

  ngOnInit() {
    this.log$ = of(this.data.log);

    combineLatest([this.editorChanged$$, this.log$]).subscribe(([editor]) => {
      setTimeout(() => {
        this.scrollToBottom(editor);
      });
    });
  }

  scrollToBottom(editor: editor.ICodeEditor) {
    const lineCount = editor.getModel().getLineCount();
    editor.revealLine(lineCount);
  }
}
