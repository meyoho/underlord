import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { MiddlewareDeployProcessComponent } from './deploy-process/component';
import { MiddlewareLogDialogComponent } from './log-dialog/component';

const components = [
  MiddlewareDeployProcessComponent,
  MiddlewareLogDialogComponent,
];

@NgModule({
  imports: [SharedModule],
  exports: components,
  declarations: components,
  entryComponents: [MiddlewareLogDialogComponent],
  providers: [],
})
export class MiddlewareComponentsModule {}
