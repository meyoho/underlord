export type MiddlewareTaskStatus =
  | 'Success'
  | 'Executing'
  | 'UnrecoverableError'
  | 'RecoverableError'
  | 'Terminated'
  | 'Skipped'
  | 'Paused';

export interface MiddlewareTask {
  name: string;
  message: string;
  reason: string;
  state?: MiddlewareTaskStatus;
  log?: {
    pod?: {
      name: string;
      namespace: string;
    };
    mess?: {
      message: string;
    };
  };
}
