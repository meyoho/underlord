import {
  Dictionary,
  ObservableInput,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { findLast } from 'lodash-es';
import { Observable, ReplaySubject, of } from 'rxjs';
import { catchError, filter, retry, switchMap } from 'rxjs/operators';

import { TdsqlTask } from '../../tdsql/types';

export type DeployProcessStatus =
  | 'Waiting'
  | 'Pending'
  | 'Success'
  | 'Failed'
  | 'Skipped';

export interface DeployProcessState {
  name: string;
  status: 'pending' | 'success' | 'failed';
}

export interface DeployProcessStep {
  index: number;
  label: string;
  status: DeployProcessStatus;
  log?: TdsqlTask['log'];
}

@Component({
  selector: 'alu-middleware-deploy-process',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MiddlewareDeployProcessComponent implements OnInit {
  @Input()
  steps: DeployProcessStep[] = [];

  @ObservableInput(true)
  steps$: Observable<DeployProcessStep[]>;

  @Input()
  needTranslate = false;

  @Input()
  switchable = true;

  @Input()
  size: 'small' | 'normal' = 'normal';

  @Input()
  logLoader: {
    context: null;
    func: (name: string, namespace: string) => Observable<string>;
  };

  @Input()
  status: 'pending' | 'success' | 'failed';

  // Number(true) or check_circle icon(false)
  @Input()
  sequential = true;

  @Input()
  errorHint = '';

  @Input()
  operationEnabled = true;

  @Output()
  switch = new EventEmitter<DeployProcessStep>();

  @Output()
  start = new EventEmitter<void>();

  @Output()
  stop = new EventEmitter<void>();

  editorActions = {
    diffMode: false,
    clear: false,
    recover: false,
    copy: false,
    find: true,
    export: false,
    import: false,
  };

  selectedStep: DeployProcessStep;

  latestStep: DeployProcessStep;

  logCache: Dictionary<string> = {};

  loadLog$$ = new ReplaySubject<TdsqlTask['log']>();

  log$ = this.loadLog$$.pipe(
    filter(log => !!log),
    switchMap(log =>
      log.pod
        ? this.logLoader
          ? this.logLoader.func
              .call(this.logLoader.context, log.pod.name, log.pod.namespace)
              .pipe(catchError(() => of('')))
          : of('')
        : of(log.mess?.message || ''),
    ),
    retry(),
    publishRef(),
  );

  constructor(
    private readonly translate: TranslateService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.steps$.subscribe(steps => {
      const latestStep = findLast(
        steps,
        step => !['Skipped', 'Waiting'].includes(step.status),
      );

      this.latestStep = latestStep || steps[0];

      this.selectedStep = this.latestStep;

      if (this.latestStep) {
        this.loadLog$$.next(this.latestStep.log);
      }

      this.cdr.markForCheck();
    });
  }

  getStateText(status: DeployProcessStatus) {
    switch (status) {
      case 'Success':
        return 'success';
      case 'Pending':
        return 'during';
      case 'Failed':
        return 'failed';
      default:
        return '';
    }
  }

  getStepLabel = (label: string) => {
    return this.needTranslate ? this.translate.get(label) : label;
  };

  stepClick(step: DeployProcessStep) {
    if (this.switchable && ['Waiting', 'Disabled'].includes(step.status)) {
      return;
    }

    if (this.selectedStep !== step) {
      this.selectedStep = step;
      this.loadLog$$.next(step.log);
      this.switch.emit(step);
    }
  }

  getIcon(status: DeployProcessStatus, highlighted: boolean) {
    switch (status) {
      case 'Waiting':
        return null;
      case 'Skipped':
        return 'basic:minus_circle';
      case 'Failed':
        return highlighted ? 'basic:close_circle_s' : 'close_circle';
      case 'Pending':
        return highlighted ? 'basic:sync_circle_s' : 'basic:sync_circle';
      case 'Success':
        return highlighted ? 'check_circle_s' : 'basic:check_circle';
      default:
        break;
    }
  }
}
