import dayjs from 'dayjs';
import { v4 } from 'uuid';

import { MiddlewareEvent } from './types';

// spec.event, used for update
export function createEvent(type: MiddlewareEvent['type']) {
  return {
    uuid: v4(),
    time: dayjs.utc().format(),
    type,
  };
}
