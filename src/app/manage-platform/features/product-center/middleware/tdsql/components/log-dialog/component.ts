import {
  IEditorConstructionOptions,
  readonlyOptions,
} from '@alauda/common-snippet';
import { DIALOG_DATA } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
  Optional,
} from '@angular/core';
import { editor } from 'monaco-editor';
import { Observable, Subject, combineLatest } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { TdsqlApiService } from 'app/api/tdsql/api';

import { TdsqlLogType } from '../../types';

type CodeEditor = editor.ICodeEditor;

@Component({
  selector: 'alu-tdsql-log-dialog',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TdsqlLogDialogComponent implements OnInit {
  editorOptions: IEditorConstructionOptions = {
    ...readonlyOptions,
    lineNumbers: 'off',
  };

  editorActions = {
    diffMode: false,
    export: false,
    copy: false,
    find: true,
    search: true,
  };

  log$: Observable<string>;

  logUpdated$$ = new Subject();
  editorUpdated$$ = new Subject<CodeEditor>();

  constructor(
    private readonly tdsqlApi: TdsqlApiService,
    @Optional()
    @Inject(DIALOG_DATA)
    private readonly data: {
      type: TdsqlLogType;
    },
  ) {}

  ngOnInit() {
    if (!this.data.type) return;

    combineLatest([this.editorUpdated$$, this.logUpdated$$]).subscribe(
      ([editor]) => {
        setTimeout(() => {
          this.scrollToBottom(editor);
        });
      },
    );

    this.log$ = this.tdsqlApi.getTdsqlLogs(this.data.type).pipe(
      map(({ status, contents }) =>
        status.toLowerCase() === 'success' ? atob(contents) : '',
      ),
      tap(() => {
        this.logUpdated$$.next();
      }),
    );
  }

  scrollToBottom(editor: CodeEditor) {
    const lineCount = editor.getModel().getLineCount();
    editor.revealLine(lineCount);
  }
}
