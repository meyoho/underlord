import { Dictionary, ObservableInput } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  COMPONENT_STATUS,
  CORE_COMPONENTS_NAME,
  RELIABILITY_COMPONENTS_NAME,
} from '../../constant';
import {
  CoreFormModel,
  ReliabilityFormModel,
  TdsqlDeployCR,
} from '../../types';
import { getComponentsStatus } from '../../utils';

@Component({
  selector: 'alu-tdsql-components-table',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TdsqlComponentsTableComponent implements OnInit {
  @Input()
  showAllComponents: false;

  @ObservableInput()
  @Input('cr')
  cr$: Observable<TdsqlDeployCR>;

  @ObservableInput()
  @Input('model')
  model$: Observable<CoreFormModel | ReliabilityFormModel>;

  list$: Observable<Array<Dictionary<string>>>;

  get componentList() {
    return [
      ...CORE_COMPONENTS_NAME,
      ...(this.showAllComponents ? RELIABILITY_COMPONENTS_NAME : []),
    ];
  }

  get columns() {
    return ['name', ...this.componentList];
  }

  ngOnInit() {
    this.list$ = combineLatest([this.cr$, this.model$]).pipe(
      map(([cr, model]) => getComponentsStatus(cr, model)),
    );
  }

  ifDeployedFailure(status: string) {
    return status === COMPONENT_STATUS.DEPLOYMENT_FAILURE;
  }
}
