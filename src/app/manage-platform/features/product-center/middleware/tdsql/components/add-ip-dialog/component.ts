import { TranslateService } from '@alauda/common-snippet';
import { DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { union, values } from 'lodash-es';

import { HOST_IP_PATTERN } from 'app/utils';

import { IpService } from '../../forms/fields/ip.service';

@Component({
  selector: 'alu-tdsql-add-ip-dialog',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TdsqlAddIpDialogComponent implements OnInit {
  @ViewChild(NgForm, { static: false })
  form: NgForm;

  @Output()
  close = new EventEmitter<string[]>();

  ips: string[] = [];
  columns = ['all_ip', 'actions'];
  newIp = '';

  ipPattern = HOST_IP_PATTERN;

  usedIps: string[] = [];

  constructor(
    public dialogRef: DialogRef,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly ipService: IpService,
  ) {}

  ngOnInit() {
    this.ipService.ips$.subscribe(ips => {
      this.ips = ips;
    });

    this.ipService.usage$.subscribe(usage => {
      this.usedIps = union(...values(usage));
    });
  }

  addIp(ip: string) {
    if (ip.trim() === '') {
      return;
    }
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    if (this.ips.includes(ip)) {
      this.message.error(this.translate.get('tdsql_ip_exist', { ip: ip }));
    } else {
      this.ipService.add([ip]);
      this.newIp = '';
      this.message.success(this.translate.get('tdsql_add_ip_succ', { ip: ip }));
    }
  }

  removeIp(ip: string) {
    this.ipService.remove(ip);
  }

  disableDelete = (ip: string) => {
    return this.usedIps.includes(ip);
  };
}
