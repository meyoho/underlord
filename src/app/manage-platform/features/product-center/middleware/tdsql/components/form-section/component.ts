import { ObservableInput } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'alu-tdsql-form-section',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TdsqlFormSectionComponent implements OnInit {
  @Input()
  sectionTitle: string;

  @Input()
  sectionDescription: string;

  @Input()
  disabled = false;

  @Input()
  isOptional = false; // 是否是可选组件 true 时显示复选框

  @ObservableInput()
  @Input('isChecked')
  isChecked$: Observable<boolean>; // 可选组件 true时勾选

  checked = false;

  @Output()
  stateChange = new EventEmitter<boolean>();

  ngOnInit() {
    if (this.isOptional) {
      this.isChecked$.subscribe(state => {
        this.checked = state;
        this.stateChange.emit(state);
      });
    }
  }
}
