import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { TdsqlAddIpDialogComponent } from './add-ip-dialog/component';
import { TdsqlComponentsOverviewDialogComponent } from './components-overview-dialog/component';
import { TdsqlComponentsTableComponent } from './components-table/component';
import { TdsqlFormSectionComponent } from './form-section/component';
import { TdsqlLogDialogComponent } from './log-dialog/component';

const components = [
  TdsqlAddIpDialogComponent,
  TdsqlComponentsOverviewDialogComponent,
  TdsqlComponentsTableComponent,
  TdsqlLogDialogComponent,
  TdsqlFormSectionComponent,
];

@NgModule({
  imports: [SharedModule],
  declarations: components,
  exports: components,
  entryComponents: [
    TdsqlAddIpDialogComponent,
    TdsqlLogDialogComponent,
    TdsqlComponentsOverviewDialogComponent,
    TdsqlComponentsTableComponent,
  ],
})
export class TdsqlComponentsModule {}
