import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Optional,
} from '@angular/core';
import { get } from 'lodash-es';

import { isProcessMode } from '../../constant';
import {
  CoreFormModel,
  ReliabilityFormModel,
  TdsqlComponentReq,
  TdsqlDeployCR,
} from '../../types';

@Component({
  selector: 'alu-tdsql-components-overview-dialog',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TdsqlComponentsOverviewDialogComponent {
  cr: TdsqlDeployCR;
  model: CoreFormModel | ReliabilityFormModel;
  showAllComponents = false;
  passwordVisible = false;
  isProcessMode = isProcessMode;

  constructor(
    public dialogRef: DialogRef,
    @Optional()
    @Inject(DIALOG_DATA)
    public data: {
      showAllComponents: boolean;
      cr?: TdsqlDeployCR;
      model?: CoreFormModel | ReliabilityFormModel;
    },
  ) {
    this.showAllComponents = data.showAllComponents;
    this.cr = data.cr;
    this.model = data.model;
  }

  getCluster() {
    return this.cr
      ? this.cr.spec.cluster
      : (this.model as CoreFormModel).cluster;
  }

  getZookeeperRootPath() {
    if (this.cr) {
      const zkReq = this.cr.spec.componentreqs.find(req => req.name === 'zk');
      return get(zkReq, 'config.zk_rootdir');
    } else {
      return get(this.model, 'zookeeper.rootPath');
    }
  }

  getSchedulerNetifName() {
    if (this.cr) {
      const schedulerReq = (get(
        this.cr,
        'spec.componentreqs',
        [],
      ) as TdsqlComponentReq[]).find(req => req.name === 'scheduler');
      return get(schedulerReq, 'config.netif_name');
    } else {
      return get(this.model, 'scheduler.netifName');
    }
  }

  getDecodedPassword = () => {
    return this.cr
      ? atob(get(this.cr, 'spec.password', ''))
      : get(this.model, 'password');
  };

  getReplicasNum(name: 'oss' | 'chitu') {
    return this.cr
      ? this.cr.spec.componentreqs.find(req => req.name === name).config
          .replicas_num
      : get(this.model, [name, 'replicas_num']);
  }
}
