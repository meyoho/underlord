import { TdsqlStageStatus } from './types';

export const DEFAULT_REPLICAS_NUM = 2;

export const CORE_COMPONENTS_NAME = [
  'zookeeper',
  'scheduler',
  'oss',
  'chitu',
  'monitor',
  'db',
  'proxy',
];

export const RELIABILITY_COMPONENTS_NAME = [
  'hdfs',
  'kafka',
  'consumer',
  'lvs',
  'es',
  ...(isProcessMode() ? ['backupchituop'] : []),
];

export const CORE_DONE_TASK_NAME = 'TDSQL-Part1Done';

export const CORE_TASK_NAMES = [
  'TDSQL-CreateOrUpdateSecretForAnsible',
  'TDSQL-PreCheckNetConnection',
  'TDSQL-PreCheckDiskSize',
  'TDSQL-PreCheckDiskType',
  'TDSQL-Beginning',
  'TDSQL-ZooKeeper',
  'TDSQL-Chitu',
  'TDSQL-OSS',
  'TDSQL-DB',
  'TDSQL-Proxy',
  'TDSQL-Scheduler',
  'TDSQL-Chart-Chitu-OSS',
  CORE_DONE_TASK_NAME,
];

export const CHITU_DONE_TASK_NAME = 'TDSQL-CoreDone';

export const CHITU_TASK_NAMES = [
  'TDSQL-CreateOrUpdateSecretForAnsibleAgain',
  'TDSQL-WaitForChituInitialize',
  'TDSQL-CheckMonitorDBConnection',
  'TDSQL-Part2',
  'TDSQL-SchedulerCmd',
  'TDSQL-BackupChitu',
  CHITU_DONE_TASK_NAME,
];

export const RELIABILITY_TASK_NAMES = [
  'TDSQL-HDFS',
  'TDSQL-LVS',
  'TDSQL-Kafka',
  'TDSQL-Consumer',
  'TDSQL-ES',
  ...(isProcessMode() ? ['TDSQL-BackupChituOp'] : []),
];

export const DONE_TASK_NAMES = [CORE_DONE_TASK_NAME, CHITU_DONE_TASK_NAME];

export const singleIpComponents = new Set([
  'scheduler',
  'monitor',
  'consumer',
  'es',
  'tempfix',
]);

export const multiIpComponents = new Set([
  'envcheck',
  'zk',
  'oss',
  'db',
  'proxy',
  'hdfs',
  'kafka',
  'lvs',
  'chitu',
]);

export const FORM_DISABLED_STATUS = [
  TdsqlStageStatus.PENDING,
  TdsqlStageStatus.SUCCESS,
];

export const TDSQL_ACTION_BUTTON_TEXTS = {
  deploy: {
    [TdsqlStageStatus.EDIT]: 'start_deploy',
    [TdsqlStageStatus.PENDING]: 'tdsql_deploying',
    [TdsqlStageStatus.FAILED]: 'continue_to_deploy',
    [TdsqlStageStatus.SUCCESS]: 'deployment_succeed',
  },
  testing: {
    [TdsqlStageStatus.EDIT]: 'start_test',
    [TdsqlStageStatus.PENDING]: 'tdsql_testing',
    [TdsqlStageStatus.FAILED]: 'start_test',
    [TdsqlStageStatus.SUCCESS]: 'tdsql_test_pass_button',
  },
};

export enum COMPONENT_STATUS {
  FILED_UP = 'filed_up',
  DEPLOYED = 'deployed',
  TO_BE_DEPLOYED = 'to_be_deployed',
  DEPLOYMENT_FAILURE = 'deployment_failure',
}

// http://confluence.alauda.cn/pages/viewpage.action?pageId=67548654
export function isProcessMode() {
  return localStorage.getItem('TDSQL_DEPLOYMENT_MODE') !== '2';
}
