import { KubernetesResource } from '@alauda/common-snippet';
import { Dictionary } from 'ts-essentials';

import { ProductCenter, ProductStatusCondition } from 'app/typings';

import { MiddlewareEvent } from '../types';

import { DeployProcessStatus } from './../components/deploy-process/component';

export interface TdsqlHostIp {
  [key: string]: string;
}

export interface TdsqlComponentReq {
  config?: {
    [key: string]: string;
  };
  hostip?: TdsqlHostIp;
  name: string;
  enabled?: boolean;
  version?: string;
}

export interface TdsqlSpec {
  version: string;
  componentreqs: TdsqlComponentReq[];
  password: string;
  refresh: string;
  scope: string;
  processdeploy?: boolean;
  front_state?: TdsqlStageName;
  cluster: string;
  request_command?: string;
  event?: MiddlewareEvent;
}

export type TdsqlTaskStatus =
  | 'Success'
  | 'Executing'
  | 'UnrecoverableError'
  | 'RecoverableError'
  | 'Terminated'
  | 'Skipped'
  | 'Paused';

export interface TdsqlTask {
  name: string;
  message: string;
  reason: string;
  state?: TdsqlTaskStatus;
  log?: {
    pod?: {
      name: string;
      namespace: string;
    };
    mess?: {
      message: string;
    };
  };
}

export interface TdsqlInstallationStep {
  lasttransitiontime: string;
  name: string;
  progress: number; // todo: type?
  reason: string;
  status: 'True' | 'False';
  version: string;
}

export interface TdsqlDeploy extends ProductCenter {
  crs: TdsqlDeployCR[];
}

export type TdsqlPhase = 'Provisioning' | 'Deleting' | 'Failed' | 'Running';

export interface TdsqlDeployCR extends KubernetesResource {
  spec: TdsqlSpec;
  status?: {
    tasks: TdsqlTask[];
    lastrefresh: string;
    version: string;
    phase?: TdsqlPhase;
    productEntrypoint: string;
    deletable: boolean;
    conditions: ProductStatusCondition[];
  };
}

export interface CoreFormModel {
  password: string;
  allIps: string[];
  cluster: string;
  zookeeper: {
    rootPath: string;
    ips: string[];
  };
  scheduler: {
    netifName: string;
    ip: string;
  };
  oss: {
    replicas_num?: number;
    ips?: string[];
  };
  chitu: {
    replicas_num?: number;
    ip?: string;
    standbyIp?: string;
  };
  monitor: {
    ip: string;
  };
  db: {
    ips: string[];
  };
  proxy: {
    ips: string[];
  };
}

export interface ChituFormModel {
  metadbIp: string;
  metadbPort: string;
  metadbIpBak?: string;
  metadbPortBak?: string;
  metadbUser: string;
  metadbPassword: string;
}

export interface ReliabilityFormModel {
  hdfs?: {
    selected?: boolean;
    dataDirPrepare: boolean;
    hostNameSetting: boolean;
    sshPort: string;
    dataFilePath: string;
    hostIps: string[];
  };
  kafka?: {
    selected: boolean;
    logFilePath: string;
    hostIps: string[];
  };
  consumer?: {
    selected: boolean;
    hostIp: string;
    networkCardName: string;
  };
  lvs?: {
    selected: boolean;
    hostIps: string[];
  };
  es?: {
    selected: boolean;
    hostIp: string;
    memory: string;
    logDays: string;
    dataPath: string;
  };
  backupchituop?: {
    selected?: boolean;
    hostIp: string;
    standbyIp: string;
  };
}

export interface TdsqlDeployFormModel {
  precheck?: boolean;
  core?: CoreFormModel;
  chitu?: ChituFormModel;
  reliability?: ReliabilityFormModel;
}

// 页面步骤状态 可编辑/已经开始尚未执行完第一个步骤/执行完第一个步骤，本阶段未执行完/本阶段执行完/失败
export enum TdsqlStageStatus {
  EDIT = 'edit',
  PENDING = 'pending',
  SUCCESS = 'success',
  FAILED = 'failed',
  PARTIAL_SUCCESS = 'partial_success',
}

export enum TdsqlStageName {
  CORE = 'core',
  CHITU = 'chitu',
  RELIABILITY = 'reliability',
}

export enum TdsqlStageNumber {
  'core' = 1,
  'chitu',
  'reliability',
}
export enum TdsqlLogType {
  Core = 'core',
  Optional = 'optional',
}

export interface PrecheckItem {
  label: string;
  text: string;
  checked: boolean;
  tip?: string;
}

export interface TdsqlInstallState {
  stage: TdsqlStageName;
  status: TdsqlStageStatus;
  step?: TdsqlInstallationStep;
}

export interface TdsqlLogs {
  status?: string;
  contents?: string; // Base64字符串
  lengths?: number;
}

export type TdsqlReliabilityComponentState = Dictionary<{
  status: TdsqlStageStatus;
  selected: boolean;
  editable: boolean;
}>;

export type TdsqlAction = 'deploy' | 'testing';

export const taskStatusMap: Dictionary<DeployProcessStatus> = {
  Success: 'Success',
  Executing: 'Pending',
  RecoverableError: 'Failed',
  UnrecoverableError: 'Failed',
  Paused: 'Success',
  Terminated: 'Waiting',
  Skipped: 'Skipped',
};
