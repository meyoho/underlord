import {
  K8sUtilService,
  ObservableInput,
  publishRef,
} from '@alauda/common-snippet';
import { DialogRef, DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  TemplateRef,
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { findLast, get } from 'lodash-es';
import { Observable, of } from 'rxjs';
import { filter, map, switchMap, tap } from 'rxjs/operators';

import {
  ProductCenterListApi,
  handleDeployStatus,
} from 'app/api/product-center/list-api';
import { TdsqlApiService } from 'app/api/tdsql/api';
import {
  mapOperatingStatus,
  mapStatus,
} from 'app/manage-platform/features/product-center/utils';
import { ConfirmDeleteComponent } from 'app/shared/components/confirm-delete/confirm-delete.component';
import { ProductCRD } from 'app/typings';

import { MiddlewareLogDialogComponent } from '../../../components/log-dialog/component';
import {
  CHITU_DONE_TASK_NAME,
  RELIABILITY_COMPONENTS_NAME,
  isProcessMode,
} from '../../constant';
import {
  TdsqlDeploy,
  TdsqlDeployCR,
  TdsqlInstallState,
  TdsqlStageName,
  TdsqlStageStatus,
  TdsqlTask,
} from '../../types';
import {
  getDeployTasks,
  getInstallState,
  getProductHref,
  getTdsqlDeployComponentreqs,
  toTdsqlFormModel,
} from '../../utils';

@Component({
  selector: 'alu-tdsql-detail-info',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TdsqlDetailInfoComponent {
  @Input()
  data: TdsqlDeploy;

  @ObservableInput(true)
  data$: Observable<TdsqlDeploy>;

  context = this;

  deleteConfirmDialog: DialogRef<ConfirmDeleteComponent>;

  cr$ = this.data$.pipe(
    map(({ crs }) => crs[0]),
    tap(cr => {
      this.deployState = getInstallState(cr);
    }),
  );

  model$ = this.cr$.pipe(
    map(cr => toTdsqlFormModel(cr)),
    publishRef(),
  );

  deployButtonStates$ = this.cr$.pipe(
    filter(cr => !!cr),
    map(cr => {
      const optionalComponents = getTdsqlDeployComponentreqs(cr).filter(
        req => RELIABILITY_COMPONENTS_NAME.includes(req.name) && req.enabled,
      );
      const coredone = getDeployTasks(cr).find(
        task =>
          task.name === CHITU_DONE_TASK_NAME &&
          ['Success', 'Paused'].includes(task.state),
      );
      return {
        continueDeploy: !coredone,
        continueReliabilityDeploy:
          coredone &&
          optionalComponents.length !== RELIABILITY_COMPONENTS_NAME.length,
      };
    }),
    tap(state => console.log(state)),
    publishRef(),
  );

  completed$ = this.deployButtonStates$.pipe(
    map(state => !state.continueDeploy && !state.continueReliabilityDeploy),
    publishRef(),
  );

  partialCompleted$ = this.deployButtonStates$.pipe(
    map(state => !state.continueDeploy && state.continueReliabilityDeploy),
    publishRef(),
  );

  passwordVisible = false;

  mapStatus = mapStatus;
  mapOperatingStatus = mapOperatingStatus;

  isProcessMode = isProcessMode;

  handleDeployStatus = handleDeployStatus;

  deployState: TdsqlInstallState;

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly k8sUtil: K8sUtilService,
    private readonly dialog: DialogService,
    private readonly sanitizer: DomSanitizer,
    private readonly productCenterApi: ProductCenterListApi,
    private readonly tdsqlApi: TdsqlApiService,
  ) {}

  hostIpDisplay(fullText: string) {
    const prefix = 'ansible_ssh_host=';
    return fullText ? fullText.replace(prefix, '') : '';
  }

  getLogo = (crd: ProductCRD) => {
    const url = this.k8sUtil.getAnnotation(crd, 'product-logo');
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  };

  isPaused(state: TdsqlInstallState) {
    return (
      [TdsqlStageName.CORE, TdsqlStageName.CHITU].includes(state.stage) &&
      state.status === TdsqlStageStatus.SUCCESS
    );
  }

  getUpgradeState(cr: TdsqlDeployCR) {
    if (!get(cr, 'newVersion', '')) {
      return {
        hint: 'tdsql_upgrade_no_version_condition_hint',
        upgradable: false,
      };
    }
    if (cr.status.phase !== 'Running') {
      return {
        hint: 'tdsql_upgrade_running_condition_hint',
        upgradable: false,
      };
    }
    return {
      upgradable: true,
    };
  }

  getChituLink = getProductHref;

  isDeletable = (cr: TdsqlDeployCR) => {
    return !get(cr, 'status.deletable');
  };

  uninstallDisabled(cr: TdsqlDeployCR) {
    return !(
      get(cr, 'status.deletable') &&
      ['Running', 'Failed'].includes(cr.status?.phase)
    );
  }

  delete(templateRef: TemplateRef<ConfirmDeleteComponent>) {
    this.deleteConfirmDialog = this.dialog.open(templateRef);
  }

  deleteApi() {
    return this.data$.pipe(
      switchMap(({ crd, crs }) =>
        this.productCenterApi.deleteProduct(crd, crs[0]),
      ),
      tap(() => {
        this.router.navigate(['../../../list'], { relativeTo: this.route });
      }),
    );
  }

  continueDeploy() {
    this.router.navigate(
      [
        '../../',
        'deploy',
        this.k8sUtil.getLabel(this.data.crd, 'available-version'),
      ],
      {
        relativeTo: this.route,
      },
    );
  }

  viewFailedLog() {
    this.cr$
      .pipe(
        switchMap(cr => {
          const tasks = get(cr, 'status.tasks', []) as TdsqlTask[];

          const firstFailedTask = findLast(
            tasks,
            task => task.state === 'UnrecoverableError',
          );

          if (firstFailedTask) {
            if (firstFailedTask.log.mess) {
              return of(firstFailedTask.log.mess);
            } else {
              const { name, namespace } = firstFailedTask.log.pod;
              return this.tdsqlApi.getLog(name, namespace);
            }
          } else {
            return of('');
          }
        }),
      )
      .subscribe(log => {
        this.dialog.open(MiddlewareLogDialogComponent, {
          size: DialogSize.Large,
          data: { log },
        });
      });
  }
}
