import { AsyncDataLoader } from '@alauda/common-snippet';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

import {
  ProductCenterListApi,
  handleDeployStatus,
} from 'app/api/product-center/list-api';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class TdsqlDeployDetailComponent {
  dataLoader = new AsyncDataLoader({
    params$: this.route.paramMap.pipe(map(params => atob(params.get('name')))),
    fetcher: name => this.listApi.getProduct(name),
    interval: 1000 * 15,
    intervalFilter: this.intervalFilter.bind(this),
  });

  constructor(
    private readonly listApi: ProductCenterListApi,
    private readonly route: ActivatedRoute,
  ) {}

  intervalFilter() {
    const { data } = this.dataLoader.snapshot;
    const status = handleDeployStatus(data);
    return ['InDeployment', 'Deleting'].includes(status.type);
  }
}
