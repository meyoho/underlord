import {
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { PrecheckItem } from '../../../types';

@Component({
  selector: 'alu-tdsql-deploy-precheck-form',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class TdsqlDeployPrecheckFormComponent {
  @ViewChild(NgForm)
  form: NgForm;

  @Input()
  isDisabled = false;

  @Input()
  list: PrecheckItem[] = [];

  @Output()
  stateChange = new EventEmitter<boolean>();

  itemStateChange() {
    this.stateChange.emit(
      this.list.reduce((acc, cur) => {
        return acc && cur.checked;
      }, true),
    );
  }
}
