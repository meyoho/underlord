import { Dictionary, publishRef } from '@alauda/common-snippet';
import { Injectable } from '@angular/core';
import { difference, union } from 'lodash-es';
import { BehaviorSubject, Observable } from 'rxjs';
import { startWith } from 'rxjs/operators';

@Injectable()
export class IpService {
  private ips: string[] = [];
  private usage: Dictionary<string[]> = {};

  private readonly update$$: BehaviorSubject<string[]> = new BehaviorSubject(
    [],
  );

  private readonly updateUsage$$: BehaviorSubject<
    Dictionary<string[]>
  > = new BehaviorSubject({});

  ips$: Observable<string[]> = this.update$$
    .asObservable()
    .pipe(startWith([]), publishRef());

  usage$: Observable<Dictionary<string[]>> = this.updateUsage$$.asObservable();

  add(ips: string[]) {
    this.ips = union(this.ips, ips);
    this.update$$.next(this.ips);
  }

  remove(ips: string[] | string) {
    this.ips = difference(this.ips, typeof ips === 'string' ? [ips] : ips);
    this.update$$.next(this.ips);
  }

  updateUsage(key: string, value: string[] | string) {
    this.usage = {
      ...this.usage,
      [key]: typeof value === 'string' ? [value] : value,
    };
    this.updateUsage$$.next(this.usage);
  }

  getUsage() {
    return {
      ...this.usage,
    };
  }

  clear() {
    this.ips = [];
    this.usage = {};
    this.update$$.next(this.ips);
    this.updateUsage$$.next(this.usage);
  }
}
