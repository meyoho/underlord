import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Injector,
  Output,
} from '@angular/core';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { COMPONENT_DIR_PATH_PATTERN } from 'app/utils';

@Component({
  selector: 'alu-tdsql-kafka-field',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TdsqlKafkaFieldComponent extends BaseResourceFormGroupComponent {
  @Output()
  checkedChange = new EventEmitter<boolean>();

  pathPattern = COMPONENT_DIR_PATH_PATTERN;
  isDisabled = false;

  constructor(public readonly injector: Injector) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      selected: false,
      logFilePath: '',
      hostIps: '',
    });
  }

  getDefaultFormModel() {
    return {};
  }

  setDisabledState(isDisabled: boolean) {
    this.isDisabled = isDisabled;
    this.setFieldsState(Object.keys(this.form.controls), isDisabled);
  }

  select(selected: boolean) {
    this.checkedChange.emit(selected);
    this.setFieldsState(['logFilePath', 'hostIps'], !selected);
  }

  private setFieldsState(names: string[], disabled: boolean) {
    names.forEach(name => {
      disabled ? this.form.get(name).disable() : this.form.get(name).enable();
    });
  }
}
