import { ObservableInput } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { get, values } from 'lodash-es';
import { Observable, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { Dictionary } from 'ts-essentials';

import { COMPONENT_DIR_PATH_PATTERN } from 'app/utils';

import { isProcessMode } from '../../constant';

import {
  ReliabilityFormModel,
  TdsqlReliabilityComponentState,
  TdsqlStageStatus,
} from './../../types';

const defaultModel: Dictionary<any> = {
  hdfs: {
    sshPort: '36000',
    dataFilePath: '/data1/hdfs',
  },
  kafka: {
    logFilePath: '/data1/kafka',
  },
  es: {
    memory: '8',
    logDays: '7',
    dataPath: '/data/application/es-install/es',
  },
};
@Component({
  selector: 'alu-tdsql-deploy-reliability-form',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TdsqlDeployReliabilityFormComponent implements OnInit {
  @ViewChild('ngForm')
  ngForm: NgForm;

  @Input()
  model: ReliabilityFormModel;

  @ObservableInput(true)
  model$: Observable<ReliabilityFormModel>;

  @Input()
  isDisabled = false;

  @ObservableInput(true)
  isDisabled$: Observable<boolean>;

  @Input()
  componentsState: TdsqlReliabilityComponentState;

  @ObservableInput(true)
  componentsState$: Observable<TdsqlReliabilityComponentState>;

  @Input()
  chituHostIp: string;

  esHostIp = '';

  chituStandbyIp = '';

  documentLink = '';

  pathPattern = COMPONENT_DIR_PATH_PATTERN;

  form: FormGroup;

  doneComponentValue: ReliabilityFormModel;

  checkState: Dictionary<boolean>;

  isProcessMode = isProcessMode;

  dependsState: Dictionary<{
    depends: string[];
    checked: boolean;
  }> = {
    consumer: {
      depends: ['hdfs', 'kafka'],
      checked: false,
    },
    es: {
      depends: ['kafka'],
      checked: false,
    },
  };

  constructor(
    private readonly fb: FormBuilder,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.form = this.createForm();

    this.model$.subscribe(model => {
      if (model) {
        // set default
        model = Object.keys(model).reduce((acc, cur) => {
          return {
            ...acc,
            [cur]: get(model, cur) || defaultModel[cur],
          };
        }, {});
        this.checkState = Object.keys(model).reduce((acc, cur) => {
          return {
            ...acc,
            [cur]: !!get(model, [cur, 'selected']),
          };
        }, {});
        this.updateDependsState();
        this.form.patchValue(model);
      }
    });

    this.isDisabled$.subscribe(disabled => {
      Object.keys(this.form.controls).forEach(name => {
        disabled ? this.form.get(name).disable() : this.form.get(name).enable();
      });
    });

    combineLatest([
      this.isDisabled$,
      this.componentsState$.pipe(
        map(state =>
          Object.keys(state).filter(name =>
            [TdsqlStageStatus.SUCCESS, TdsqlStageStatus.PENDING].includes(
              state[name].status,
            ),
          ),
        ),
      ),
    ]).subscribe(([disabled, names]) => {
      Object.keys(this.form.controls).forEach(name => {
        disabled || names.includes(name)
          ? this.form.get(name).disable()
          : this.form.get(name).enable();
      });
      this.doneComponentValue = names.reduce((acc, cur) => {
        return {
          ...acc,
          [cur]: get(this.model, cur),
        };
      }, {});
      console.log('done components: ');
      console.log(this.doneComponentValue);
      this.cdr.markForCheck();
    });
  }

  createForm() {
    return this.fb.group({
      hdfs: null,
      kafka: null,
      consumer: null,
      lvs: null,
      es: null,
      backupchituop: null,
    });
  }

  esHostIpChanged(ip: string) {
    this.esHostIp = ip;
  }

  chituStandbyIpChanged(ip: string) {
    this.chituStandbyIp = ip;
  }

  checkedChange(componentName: string, checked: boolean) {
    this.checkState[componentName] = checked;
    this.updateDependsState();
  }

  private updateDependsState() {
    Object.keys(this.dependsState).forEach(dep => {
      this.dependsState[dep].checked = this.dependsState[dep].depends.reduce(
        (acc, cur) => {
          return acc && this.checkState[cur];
        },
        true,
      );
    });
  }

  submit() {
    this.ngForm.onSubmit(null);
    console.log(`reliability form valid: ${this.ngForm.valid}`);
    if (
      !values(this.ngForm.value as ReliabilityFormModel).filter(
        item => !!item && item.selected,
      ).length
    ) {
      console.warn(`select at least one component to deploy`);
      return null;
    }

    if (this.ngForm.valid) {
      const value = Object.keys(this.ngForm.value).reduce((acc, cur) => {
        return this.ngForm.value[cur] && this.ngForm.value[cur].selected
          ? {
              ...acc,
              [cur]: this.ngForm.value[cur],
            }
          : acc;
      }, {});

      return {
        ...this.doneComponentValue,
        ...value,
      };
    }
    return null;
  }
}
