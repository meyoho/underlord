import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Injector,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { distinctUntilChanged, filter } from 'rxjs/operators';

@Component({
  selector: 'alu-tdsql-chitubackup-field',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TdsqlChitubackupFieldComponent
  extends BaseResourceFormGroupComponent
  implements OnInit {
  @Output()
  checkedChange = new EventEmitter<boolean>();

  @Input()
  chituHostIp: string;

  @Input()
  esHostIp: string;

  @Output()
  standbyIpChange = new EventEmitter<boolean>();

  isDisabled = false;

  constructor(public readonly injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.form
      .get('standbyIp')
      .valueChanges.pipe(
        filter(val => !!val),
        distinctUntilChanged(),
      )
      .subscribe(val => {
        this.standbyIpChange.emit(val);
      });
  }

  createForm() {
    return this.fb.group({
      selected: false,
      hostIp: '',
      standbyIp: '',
    });
  }

  getDefaultFormModel() {
    return {};
  }

  getChituSlaveExcludeOptions(): string[] {
    const ips = [this.chituHostIp];

    if (this.esHostIp) {
      ips.push(this.esHostIp);
    }
    return ips;
  }

  setDisabledState(isDisabled: boolean) {
    this.isDisabled = isDisabled;
    this.setFieldsState(Object.keys(this.form.controls), isDisabled);
  }

  select(selected: boolean) {
    this.checkedChange.emit(selected);
    this.setFieldsState(['standbyIp'], !selected);
  }

  private setFieldsState(names: string[], disabled: boolean) {
    names.forEach(name => {
      disabled ? this.form.get(name).disable() : this.form.get(name).enable();
    });
  }
}
