import { publishRef } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Injector,
  Output,
} from '@angular/core';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { map, startWith } from 'rxjs/operators';

import { COMPONENT_DIR_PATH_PATTERN } from 'app/utils';

import { IpService } from '../ip.service';

@Component({
  selector: 'alu-tdsql-hdfs-field',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TdsqlHdfsFieldComponent extends BaseResourceFormGroupComponent {
  @Output()
  checkedChange = new EventEmitter<boolean>();

  pathPattern = COMPONENT_DIR_PATH_PATTERN;
  isDisabled = false;

  allowedIpCount$ = this.ipService.usage$.pipe(
    map(usage => {
      console.log(usage);
      return 'zookeeper' in usage
        ? usage.zookeeper.length === 1
          ? [1]
          : [1, 3]
        : [1, 3];
    }),
    startWith([1, 3]),
    publishRef(),
  );

  constructor(
    public readonly injector: Injector,
    private readonly ipService: IpService,
  ) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      selected: false,
      dataDirPrepare: false,
      hostNameSetting: false,
      sshPort: '',
      dataFilePath: '',
      hostIps: '',
    });
  }

  getDefaultFormModel() {
    return {};
  }

  setDisabledState(isDisabled: boolean) {
    this.isDisabled = isDisabled;
    this.setFieldsState(Object.keys(this.form.controls), isDisabled);
  }

  select(selected: boolean) {
    this.checkedChange.emit(selected);
    this.setFieldsState(
      [
        'dataDirPrepare',
        'hostNameSetting',
        'sshPort',
        'dataFilePath',
        'hostIps',
      ],
      !selected,
    );
  }

  private setFieldsState(names: string[], disabled: boolean) {
    names.forEach(name => {
      disabled ? this.form.get(name).disable() : this.form.get(name).enable();
    });
  }
}
