import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { TdsqlComponentsModule } from '../../components/module';

import { TdsqlChitubackupFieldComponent } from './chitubackup/component';
import { TdsqlConsumerFieldComponent } from './consumer/component';
import { TdsqlEsFieldComponent } from './es/component';
import { TdsqlHdfsFieldComponent } from './hdfs/component';
import {
  IpSelectorLimitValidatorDirective,
  TdsqlIpSelectorComponent,
} from './ip-selector/component';
import { IpService } from './ip.service';
import { TdsqlKafkaFieldComponent } from './kafka/component';
import { TdsqlLvsFieldComponent } from './lvs/component';
import { TdsqlDeployPrecheckFormComponent } from './precheck-form/component';

const fields = [
  TdsqlHdfsFieldComponent,
  TdsqlKafkaFieldComponent,
  TdsqlConsumerFieldComponent,
  TdsqlLvsFieldComponent,
  TdsqlEsFieldComponent,
  TdsqlChitubackupFieldComponent,
  IpSelectorLimitValidatorDirective,
  TdsqlIpSelectorComponent,
  TdsqlDeployPrecheckFormComponent,
];

@NgModule({
  imports: [SharedModule, TdsqlComponentsModule],
  declarations: fields,
  exports: fields,
  providers: [IpService],
})
export class TdsqlCustomFieldsModule {}
