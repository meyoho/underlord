import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Injector,
  Output,
} from '@angular/core';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

@Component({
  selector: 'alu-tdsql-lvs-field',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TdsqlLvsFieldComponent extends BaseResourceFormGroupComponent {
  @Output()
  checkedChange = new EventEmitter<boolean>();

  isDisabled = false;

  constructor(public readonly injector: Injector) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      selected: false,
      hostIps: '',
    });
  }

  getDefaultFormModel() {
    return {};
  }

  setDisabledState(isDisabled: boolean) {
    this.isDisabled = isDisabled;
    this.setFieldsState(Object.keys(this.form.controls), isDisabled);
  }

  select(selected: boolean) {
    this.checkedChange.emit(selected);
    this.setFieldsState(['hostIps'], !selected);
  }

  private setFieldsState(names: string[], disabled: boolean) {
    names.forEach(name => {
      disabled ? this.form.get(name).disable() : this.form.get(name).enable();
    });
  }
}
