import { Callback, noop } from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Directive,
  EventEmitter,
  Input,
  OnChanges,
  Optional,
  Output,
  SimpleChanges,
  forwardRef,
} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormGroupDirective,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  NgForm,
  ValidationErrors,
  Validator,
  ValidatorFn,
} from '@angular/forms';
import { get } from 'lodash-es';
import { Observable } from 'rxjs';

import { TdsqlAddIpDialogComponent } from '../../../components/add-ip-dialog/component';
import { IpService } from '../ip.service';

@Component({
  selector: 'alu-tdsql-ip-selector',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TdsqlIpSelectorComponent),
      multi: true,
    },
  ],
})
export class TdsqlIpSelectorComponent implements ControlValueAccessor {
  @Input()
  excludeOptions: string[] = [];

  @Input()
  multiSelect = false;

  @Input()
  minCount: number;

  @Input()
  placeholder = '';

  @Input()
  allowedCount: number[];

  @Input()
  selectDisabled = false;

  @Output()
  valueChange = new EventEmitter<string[] | string>();

  options$: Observable<string[]> = this.ipService.ips$;

  onCvaChange: Callback;
  onCvaTouched: Callback;

  model: string[] | string;

  disabled = false;

  parentForm: NgForm | FormGroupDirective;

  get submitted() {
    return this.parentForm && this.parentForm.submitted;
  }

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly ipService: IpService,
    private readonly dialog: DialogService,
    @Optional() readonly formGroup: FormGroupDirective,
    @Optional() readonly ngForm: NgForm,
  ) {
    this.parentForm = formGroup || ngForm;
  }

  ipSelectChange(ips: string[]) {
    this.onCvaChange(this.model);
    this.valueChange.emit(ips);
  }

  onValidatorChange = noop;

  registerOnChange(fn: (value: string[]) => void): void {
    this.onCvaChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onCvaTouched = fn;
  }

  writeValue(_model: string[] | string) {
    this.model = _model || (this.multiSelect ? [] : '');
    this.cdr.markForCheck();
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
    this.cdr.markForCheck();
  }

  openAddIpDialog() {
    this.dialog.open(TdsqlAddIpDialogComponent);
  }
}

@Directive({
  selector:
    // tslint:disable-next-line: directive-selector
    'alu-tdsql-ip-selector[allowedCount],alu-tdsql-ip-selector[minCount],alu-tdsql-ip-selector[excludeOptions]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => IpSelectorLimitValidatorDirective),
      multi: true,
    },
  ],
})
export class IpSelectorLimitValidatorDirective implements Validator, OnChanges {
  @Input()
  allowedCount: number[];

  @Input()
  minCount: number;

  @Input()
  excludeOptions: string[];

  private _validator: ValidatorFn;
  private _onChange: () => void;

  ngOnChanges({ allowedCount, minCount, excludeOptions }: SimpleChanges): void {
    if (allowedCount || minCount || excludeOptions) {
      this._createValidator();
      if (this._onChange) {
        this._onChange();
      }
    }
  }

  validate(c: AbstractControl): ValidationErrors | null {
    return this._validator(c);
  }

  registerOnValidatorChange(fn: () => void): void {
    this._onChange = fn;
  }

  private _createValidator(): void {
    this._validator = this.limitValidator;
  }

  limitValidator(control: AbstractControl) {
    if (control) {
      const len = get(control, 'value.length');
      if (this.allowedCount && !this.allowedCount.includes(len)) {
        return {
          allowedCountError: {
            allowedCount: this.allowedCount,
            currentLength: len,
          },
        };
      }
      if (this.minCount && len < this.minCount) {
        return {
          minCountError: {
            minCount: this.minCount,
            currentLength: len,
          },
        };
      }

      const excludeLen = get(this.excludeOptions, 'length', 0);
      if (excludeLen && this.excludeOptions.includes(control.value)) {
        return {
          excludeOptionsError: {
            excludeOptions: this.excludeOptions,
            currentValue: control.value,
          },
        };
      }
      return null;
    }
    return null;
  }
}
