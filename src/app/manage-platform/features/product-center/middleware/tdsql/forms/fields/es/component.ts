import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Injector,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { distinctUntilChanged, filter } from 'rxjs/operators';

@Component({
  selector: 'alu-tdsql-es-field',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TdsqlEsFieldComponent extends BaseResourceFormGroupComponent
  implements OnInit {
  @Input()
  dependsChecked = false;

  @Output()
  checkedChange = new EventEmitter<boolean>();

  @Output()
  hostIpChange = new EventEmitter<boolean>();

  @Input()
  chituHostIp = '';

  @Input()
  chituStandbyIp = '';

  isDisabled = false;

  constructor(public readonly injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.form
      .get('hostIp')
      .valueChanges.pipe(
        filter(val => !!val),
        distinctUntilChanged(),
      )
      .subscribe(val => {
        this.hostIpChange.emit(val);
      });
  }

  createForm() {
    return this.fb.group({
      selected: false,
      hostIp: '',
      memory: '',
      logDays: '',
      dataPath: '',
    });
  }

  getDefaultFormModel() {
    return {};
  }

  getEsExcludeOptions(): string[] {
    const ips = [this.chituHostIp];

    if (this.chituStandbyIp) {
      ips.push(this.chituStandbyIp);
    }
    return ips;
  }

  setDisabledState(isDisabled: boolean) {
    this.isDisabled = isDisabled;
    this.setFieldsState(Object.keys(this.form.controls), isDisabled);
  }

  select(selected: boolean) {
    this.checkedChange.emit(selected);
    this.setFieldsState(['hostIp', 'memory', 'logDays'], !selected);
  }

  private setFieldsState(names: string[], disabled: boolean) {
    names.forEach(name => {
      disabled ? this.form.get(name).disable() : this.form.get(name).enable();
    });
  }
}
