import { ObservableInput } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

import { HOST_IP_PATTERN, PORT_PATTERN } from 'app/utils';

import { ChituFormModel } from '../../types';

@Component({
  selector: 'alu-tdsql-deploy-chitu-form',
  templateUrl: 'template.html',
  styleUrls: ['../form-common.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TdsqlDeployChituFormComponent implements OnInit {
  @ViewChild('ngForm')
  ngForm: NgForm;

  @Input()
  model: ChituFormModel;

  @ObservableInput(true)
  model$: Observable<ChituFormModel>;

  @Input()
  isDisabled = false;

  @ObservableInput(true)
  isDisabled$: Observable<boolean>;

  ipPattern = HOST_IP_PATTERN;
  portPattern = PORT_PATTERN;

  form: FormGroup;

  constructor(private readonly fb: FormBuilder) {}

  ngOnInit() {
    this.form = this.createForm();

    this.model$.subscribe(model => {
      if (model) {
        this.form.patchValue(model);
      }
    });

    this.isDisabled$.subscribe(disabled => {
      Object.keys(this.form.controls).forEach(key => {
        if (disabled) {
          this.form.get(key).disable();
        } else {
          this.form.get(key).enable();
        }
      });
    });
  }

  createForm() {
    return this.fb.group({
      metadbIp: [
        '',
        [Validators.required, Validators.pattern(HOST_IP_PATTERN.pattern)],
      ],
      metadbPort: [
        '',
        [Validators.required, Validators.pattern(PORT_PATTERN.pattern)],
      ],
      metadbIpBak: ['', [Validators.pattern(HOST_IP_PATTERN.pattern)]],
      metadbPortBak: ['', [Validators.pattern(PORT_PATTERN.pattern)]],
      metadbUser: ['', Validators.required],
      metadbPassword: ['', Validators.required],
    });
  }

  submit() {
    this.ngForm.onSubmit(null);
    console.log(`chitu form valid: ${this.ngForm.valid}`);
    if (this.ngForm.valid) {
      return this.ngForm.value;
    }
    return null;
  }
}
