import {
  K8sApiService,
  ObservableInput,
  publishRef,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { distinctUntilChanged, pluck } from 'rxjs/operators';

import {
  COMPONENT_DIR_PATH_PATTERN,
  POSITIVE_INTEGER_PATTERN,
  RESOURCE_TYPES,
} from 'app/utils';

import { DEFAULT_REPLICAS_NUM, isProcessMode } from '../../constant';
import { CoreFormModel } from '../../types';
import { IpService } from '../fields/ip.service';

@Component({
  selector: 'alu-tdsql-deploy-core-form',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TdsqlDeployCoreFormComponent implements OnInit {
  @ViewChild('ngForm')
  ngForm: NgForm;

  @Input()
  model: CoreFormModel;

  @ObservableInput(true)
  model$: Observable<CoreFormModel>;

  @Input()
  isDisabled = false;

  @ObservableInput(true)
  isDisabled$: Observable<boolean>;

  pathPattern = COMPONENT_DIR_PATH_PATTERN;

  form: FormGroup;

  clusters$ = this.k8sApi
    .getGlobalResourceList({
      type: RESOURCE_TYPES.CLUSTER_REGISTRY,
    })
    .pipe(pluck('items'), publishRef());

  positiveIntegerPattern = POSITIVE_INTEGER_PATTERN;
  isProcessMode = isProcessMode;

  constructor(
    private readonly ipService: IpService,
    private readonly fb: FormBuilder,
    private readonly k8sApi: K8sApiService,
  ) {}

  ngOnInit() {
    this.form = this.createForm();

    this.model$.subscribe(model => {
      if (model) {
        this.form.patchValue(model);
      }
    });

    this.isDisabled$.subscribe(disabled => {
      Object.keys(this.form.controls).forEach(name => {
        if (disabled) {
          this.form.get(name).disable();
        } else {
          this.form.get(name).enable();
        }
      });
    });

    this.ipService.ips$
      .pipe(distinctUntilChanged())
      .subscribe(ips => this.form.get('allIps').patchValue(ips));
  }

  createForm() {
    const zookeeper = this.fb.group({
      rootPath: ['/tdsqlzk', Validators.required],
      ips: [[], Validators.required],
    });

    const scheduler = this.fb.group({
      netifName: ['eth0', Validators.required],
      ip: ['', Validators.required],
    });

    const oss = this.fb.group(
      this.isProcessMode()
        ? {
            ips: [[], Validators.required],
          }
        : {
            replicas_num: [DEFAULT_REPLICAS_NUM, Validators.required],
          },
    );

    const chitu = this.fb.group(
      this.isProcessMode()
        ? {
            ip: ['', Validators.required],
            standbyIp: [''],
          }
        : {
            replicas_num: [DEFAULT_REPLICAS_NUM, Validators.required],
          },
    );

    const monitor = this.fb.group({
      ip: ['', Validators.required],
    });

    const db = this.fb.group({
      ips: [[], Validators.required],
    });

    const proxy = this.fb.group({
      ips: [[], Validators.required],
    });

    const group = this.fb.group({
      password: ['', Validators.required],
      allIps: [[], Validators.required],
      zookeeper,
      scheduler,
      oss,
      chitu,
      monitor,
      db,
      proxy,
    });

    if (!this.isProcessMode()) {
      group.addControl('cluster', this.fb.control('', [Validators.required]));
    }

    return group;
  }

  getchituExcludeOptions(ipType: string) {
    const control = this.form.get(['chitu', ipType]);
    const excludeValue = control && control.value;
    return excludeValue ? [excludeValue] : [];
  }

  usedIpsChange(key: string, ips: string[] | string) {
    this.ipService.updateUsage(key, ips);
  }

  submit() {
    this.ngForm.onSubmit(null);
    console.log(`core form valid: ${this.ngForm.valid}`);
    if (this.ngForm.valid) {
      return this.ngForm.value;
    }
    return null;
  }
}
