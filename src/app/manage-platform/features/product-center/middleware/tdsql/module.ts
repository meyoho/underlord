import { Inject, NgModule } from '@angular/core';

import { Environments } from 'app/api/envs/types';
import { ENVIRONMENTS } from 'app/services/services.module';
import { SharedModule } from 'app/shared/shared.module';

import { MiddlewareComponentsModule } from './../components/module';
import { TdsqlComponentsModule } from './components/module';
import { TdsqlDeployChituStepComponent } from './deploy/chitu-step/component';
import { TdsqlDeployComponent } from './deploy/component';
import { TdsqlDeployCoreStepComponent } from './deploy/core-step/component';
import { TdsqlDeployReliabilityStepComponent } from './deploy/reliability-step/component';
import { TdsqlDeployDetailComponent } from './detail/component';
import { TdsqlDetailInfoComponent } from './detail/detail-info/component';
import { TdsqlDeployChituFormComponent } from './forms/chitu-form/component';
import { TdsqlDeployCoreFormComponent } from './forms/core-form/component';
import { TdsqlCustomFieldsModule } from './forms/fields/module';
import { TdsqlDeployReliabilityFormComponent } from './forms/reliability-form/component';
import { TdsqlRoutingModule } from './routing.module';
const COMPONENTS = [
  TdsqlDeployDetailComponent,
  TdsqlDetailInfoComponent,
  TdsqlDeployCoreStepComponent,
  TdsqlDeployChituStepComponent,
  TdsqlDeployReliabilityStepComponent,

  TdsqlDeployComponent,
  TdsqlDeployCoreFormComponent,
  TdsqlDeployChituFormComponent,
  TdsqlDeployReliabilityFormComponent,
];

@NgModule({
  imports: [
    SharedModule,
    TdsqlRoutingModule,
    TdsqlCustomFieldsModule,
    TdsqlComponentsModule,
    MiddlewareComponentsModule,
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
})
export class TdsqlModule {
  constructor(@Inject(ENVIRONMENTS) private readonly env: Environments) {
    localStorage.setItem(
      'TDSQL_DEPLOYMENT_MODE',
      this.env.TDSQL_DEPLOYMENT_MODE || '1',
    );
  }
}
