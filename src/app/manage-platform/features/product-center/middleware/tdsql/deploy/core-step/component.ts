import { ObservableInput, publishRef } from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { Observable } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';

import { TdsqlApiService } from 'app/api/tdsql/api';

import { MiddlewareEventType } from '../../../types';
import { TdsqlComponentsOverviewDialogComponent } from '../../components/components-overview-dialog/component';
import { TdsqlLogDialogComponent } from '../../components/log-dialog/component';
import {
  CORE_DONE_TASK_NAME,
  CORE_TASK_NAMES,
  FORM_DISABLED_STATUS,
} from '../../constant';
import { TdsqlDeployPrecheckFormComponent } from '../../forms/fields/precheck-form/component';
import {
  CoreFormModel,
  PrecheckItem,
  TdsqlAction,
  TdsqlDeployCR,
  TdsqlInstallState,
  TdsqlLogType,
  TdsqlStageName,
  TdsqlStageStatus,
  TdsqlTask,
  taskStatusMap,
} from '../../types';

import { TdsqlDeployCoreFormComponent } from './../../forms/core-form/component';

@Component({
  selector: 'alu-tdsql-deploy-core-step',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TdsqlDeployCoreStepComponent implements OnInit {
  @ViewChild(TdsqlDeployPrecheckFormComponent)
  precheckForm: TdsqlDeployPrecheckFormComponent;

  @ViewChild(TdsqlDeployCoreFormComponent)
  coreForm: TdsqlDeployCoreFormComponent;

  @Input()
  model: CoreFormModel;

  @Input()
  installState: TdsqlInstallState = {
    stage: TdsqlStageName.CORE,
    status: TdsqlStageStatus.EDIT,
  };

  @Input()
  cr: TdsqlDeployCR;

  @ObservableInput(true)
  installState$: Observable<TdsqlInstallState>;

  @Input()
  tasks: TdsqlTask[] = [];

  @Output()
  next = new EventEmitter<void>();

  @Output() start = new EventEmitter<{
    model: CoreFormModel;
    eventType: MiddlewareEventType;
  }>();

  @Output() stop = new EventEmitter();

  @Output() cancel = new EventEmitter<TdsqlAction>();

  action: TdsqlAction = 'deploy';

  submitting = false;

  pendingState = {
    stage: TdsqlStageName.CORE,
    status: TdsqlStageStatus.PENDING,
  };

  nextDisabled$ = this.installState$.pipe(
    filter(state => !!state),
    tap(() => (this.submitting = false)),
    map(state => state.status !== TdsqlStageStatus.SUCCESS),
    publishRef(),
  );

  editDisabled$ = this.installState$.pipe(
    filter(state => !!state),
    map(state => FORM_DISABLED_STATUS.includes(state.status)),
    publishRef(),
  );

  total = CORE_TASK_NAMES.length;

  precheckList: PrecheckItem[] = [
    {
      label: 'tdsql_deploy_planning',
      tip: 'deployment_plan_tip',
    },
    {
      label: 'tdsql_password_free_config',
      tip: 'free_password_tip',
    },
    {
      label: 'tdsql_prepare_data_dir',
      tip: 'data_dir_preparation_tip',
    },
  ].map(({ label, tip }) => ({
    label,
    tip,
    checked: false,
    text: 'read_and_complete_operations_on_host',
  }));

  logLoader = {
    context: this.tdsqlApi,
    func: this.tdsqlApi.getLog,
  };

  constructor(
    private readonly dialog: DialogService,
    private readonly tdsqlApi: TdsqlApiService,
  ) {}

  ngOnInit() {
    this.installState$.subscribe(state => {
      if (state && state.status !== TdsqlStageStatus.EDIT) {
        this.precheckList = this.precheckList.map(item => ({
          ...item,
          checked: true,
        }));
      }
    });
  }

  getSteps(tasks: TdsqlTask[]) {
    return tasks
      .filter(task => task.name !== CORE_DONE_TASK_NAME)
      .map((task, i) => ({
        index: i + 1,
        label: `${task.name.replace(/-/g, '_')}_step`.toLowerCase(),
        status: task.state ? taskStatusMap[task.state] : 'Waiting',
        log: task.log,
      }));
  }

  jumpDocument() {
    // todo: document link
  }

  openComponentsOverviewDialog() {
    const model = this.coreForm.form.value;

    this.dialog.open(TdsqlComponentsOverviewDialogComponent, {
      size: DialogSize.Large,
      data: {
        showAllComponents: false,
        cr: this.cr,
        model,
      },
    });
  }

  openLogDialog() {
    this.dialog.open(TdsqlLogDialogComponent, {
      size: DialogSize.Big,
      data: {
        type: TdsqlLogType.Core,
      },
    });
  }

  startDeploy() {
    this.precheckForm.form.onSubmit(null);

    const model: CoreFormModel = this.coreForm.submit();

    if (this.precheckForm.form.valid && model) {
      this.submitting = true;
      this.start.emit({
        model,
        eventType: 'ReProvision',
      });
    }
  }
}
