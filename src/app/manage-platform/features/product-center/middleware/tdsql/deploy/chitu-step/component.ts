import { ObservableInput, publishRef } from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { get } from 'lodash-es';
import { Observable } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';

import { TdsqlApiService } from 'app/api/tdsql/api';

import { MiddlewareEventType } from '../../../types';
import { TdsqlComponentsOverviewDialogComponent } from '../../components/components-overview-dialog/component';
import { TdsqlLogDialogComponent } from '../../components/log-dialog/component';
import {
  CHITU_DONE_TASK_NAME,
  CORE_TASK_NAMES,
  FORM_DISABLED_STATUS,
} from '../../constant';
import { TdsqlDeployChituFormComponent } from '../../forms/chitu-form/component';
import {
  ChituFormModel,
  TdsqlAction,
  TdsqlDeployCR,
  TdsqlInstallState,
  TdsqlLogType,
  TdsqlStageName,
  TdsqlStageStatus,
  TdsqlTask,
  taskStatusMap,
} from '../../types';
import { getProductHref } from '../../utils';

@Component({
  selector: 'alu-tdsql-deploy-chitu-step',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TdsqlDeployChituStepComponent {
  @ViewChild(TdsqlDeployChituFormComponent)
  form: TdsqlDeployChituFormComponent;

  @Input()
  cr: TdsqlDeployCR;

  @Input()
  model: ChituFormModel;

  @Input()
  installState: TdsqlInstallState = {
    stage: TdsqlStageName.CORE,
    status: TdsqlStageStatus.EDIT,
  };

  @ObservableInput(true)
  installState$: Observable<TdsqlInstallState>;

  @Input()
  tasks: TdsqlTask[] = [];

  @Output() next = new EventEmitter<void>();

  @Output() start = new EventEmitter<{
    model: ChituFormModel;
    eventType: MiddlewareEventType;
  }>();

  @Output() stop = new EventEmitter();

  @Output() cancel = new EventEmitter<TdsqlAction>();

  total = CORE_TASK_NAMES.length;

  action: TdsqlAction = 'testing';

  submitting = false;

  pendingState = {
    stage: TdsqlStageName.CHITU,
    status: TdsqlStageStatus.PENDING,
  };

  getChituLink = getProductHref;

  nextDisabled$ = this.installState$.pipe(
    filter(state => !!state),
    tap(() => (this.submitting = false)),
    map(state => state.status !== TdsqlStageStatus.SUCCESS),
    publishRef(),
  );

  editDisabled$ = this.installState$.pipe(
    filter(state => !!state),
    map(state => FORM_DISABLED_STATUS.includes(state.status)),
    publishRef(),
  );

  logLoader = {
    context: this.tdsqlApi,
    func: this.tdsqlApi.getLog,
  };

  constructor(
    private readonly dialog: DialogService,
    private readonly tdsqlApi: TdsqlApiService,
  ) {}

  getSteps(tasks: TdsqlTask[]) {
    return tasks
      .filter(task => task.name !== CHITU_DONE_TASK_NAME)
      .map((task, i) => ({
        index: i + 1,
        label: `${task.name.replace('-', '_')}_step`.toLowerCase(),
        status: task.state ? taskStatusMap[task.state] : 'Waiting',
        log: task.log,
      }));
  }

  openLogDialog() {
    this.dialog.open(TdsqlLogDialogComponent, {
      data: {
        type: TdsqlLogType.Core,
      },
    });
  }

  getEntryPoint(cr: TdsqlDeployCR) {
    return get(cr, ['status', 'productEntrypoint']);
  }

  startTest() {
    const model: ChituFormModel = this.form.submit();
    if (model) {
      this.submitting = true;
      this.start.emit({
        model,
        eventType: 'ReProvision',
      });
    }
  }

  openComponentsOverviewDialog() {
    this.dialog.open(TdsqlComponentsOverviewDialogComponent, {
      size: DialogSize.Large,
      data: {
        showAllComponents: false,
        cr: this.cr,
      },
    });
  }
}
