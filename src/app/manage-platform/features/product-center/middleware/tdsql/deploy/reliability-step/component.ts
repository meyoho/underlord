import {
  Dictionary,
  ObservableInput,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { get } from 'lodash-es';
import { Observable, combineLatest } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';

import { TdsqlApiService } from 'app/api/tdsql/api';

import { MiddlewareEventType } from '../../../types';
import { TdsqlComponentsOverviewDialogComponent } from '../../components/components-overview-dialog/component';
import { TdsqlLogDialogComponent } from '../../components/log-dialog/component';
import {
  RELIABILITY_COMPONENTS_NAME,
  RELIABILITY_TASK_NAMES,
  TDSQL_ACTION_BUTTON_TEXTS,
} from '../../constant';
import { TdsqlDeployReliabilityFormComponent } from '../../forms/reliability-form/component';
import {
  ReliabilityFormModel,
  TdsqlAction,
  TdsqlComponentReq,
  TdsqlDeployCR,
  TdsqlInstallState,
  TdsqlLogType,
  TdsqlReliabilityComponentState,
  TdsqlStageName,
  TdsqlStageStatus,
  TdsqlTask,
  TdsqlTaskStatus,
  taskStatusMap,
} from '../../types';
import { getProductHref } from '../../utils';

const defaultComponentsState: TdsqlReliabilityComponentState = RELIABILITY_COMPONENTS_NAME.reduce(
  (acc, cur) => {
    return {
      ...acc,
      [cur]: {
        status: TdsqlStageStatus.EDIT,
        selected: false,
        editable: true,
      },
    };
  },
  {},
);

const componentTasksMap: Dictionary<string> = {
  hdfs: 'TDSQL-HDFS',
  lvs: 'TDSQL-LVS',
  kafka: 'TDSQL-Kafka',
  consumer: 'TDSQL-Consumer',
  es: 'TDSQL-ES',
  backupchituop: 'TDSQL-BackupChituOp',
};

@Component({
  selector: 'alu-tdsql-deploy-reliability-step',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TdsqlDeployReliabilityStepComponent {
  @ViewChild(TdsqlDeployReliabilityFormComponent)
  form: TdsqlDeployReliabilityFormComponent;

  @Input()
  cr: TdsqlDeployCR;

  @Input()
  model: ReliabilityFormModel;

  @Input()
  installState: TdsqlInstallState = {
    stage: TdsqlStageName.RELIABILITY,
    status: TdsqlStageStatus.EDIT,
  };

  @Input()
  chituHostIp: string;

  @ObservableInput(true)
  installState$: Observable<TdsqlInstallState>;

  @Input()
  tasks: TdsqlTask[] = [];

  @ObservableInput(true)
  tasks$: Observable<TdsqlTask[]>;

  @Input()
  componentReqs: TdsqlComponentReq[] = [];

  @ObservableInput(true)
  componentReqs$: Observable<TdsqlComponentReq[]>;

  @Output() start = new EventEmitter<{
    model: ReliabilityFormModel;
    eventType: MiddlewareEventType;
  }>();

  @Output() stop = new EventEmitter();

  @Output() complete = new EventEmitter<TdsqlAction>();

  action: TdsqlAction = 'deploy';

  selectedComponentNames: string[] = [];

  docLinks = ['hdfs', 'lvs', 'es'];

  buttonTexts = TDSQL_ACTION_BUTTON_TEXTS;

  total = RELIABILITY_TASK_NAMES.length;

  submitting = false;

  pendingState = {
    stage: TdsqlStageName.RELIABILITY,
    status: TdsqlStageStatus.PENDING,
  };

  componentState: TdsqlReliabilityComponentState = defaultComponentsState;

  startDisabled$ = this.installState$.pipe(
    filter(state => !!state),
    tap(() => (this.submitting = false)),
    map(state => TdsqlStageStatus.PENDING === state.status),
    publishRef(),
  );

  selectedComponentReqs$ = this.componentReqs$.pipe(
    map(reqs => reqs.filter(req => req.enabled)),
    publishRef(),
  );

  getChituLink = getProductHref;

  getDocHref = (anchor: string) => {
    return `/platform-docs/10usermanual/platformmanagement/50ace/03tdsql/06install/04availability/#${anchor}`;
  };

  componentsState$ = combineLatest([
    this.tasks$,
    this.selectedComponentReqs$,
  ]).pipe(
    map(([tasks, reqs]) => {
      const selectedComponentsState: TdsqlReliabilityComponentState = reqs.reduce(
        (acc, cur) => {
          const status = this.getOptionalComponentStatus(cur.name, tasks);
          return {
            ...acc,
            [cur.name]: {
              status,
              selected: true,
              editable: TdsqlStageStatus.FAILED === status,
            },
          };
        },
        {},
      );
      return {
        ...defaultComponentsState,
        ...selectedComponentsState,
      };
    }),
    tap(state => {
      this.componentState = state;
    }),
    publishRef(),
  );

  hasActiveStep$ = this.tasks$.pipe(
    map(tasks => !tasks.every(task => !task.state || task.state === 'Skipped')),
    tap(res => console.log(`has active step: ${res}`)),
    publishRef(),
  );

  steps$ = this.tasks$.pipe(
    map(tasks =>
      tasks.map((task, i) => ({
        index: i + 1,
        label: `${task.name.replace('-', '_')}_step`.toLowerCase(),
        status: task.state ? taskStatusMap[task.state] : 'Waiting',
        log: task.log,
      })),
    ),
    tap(steps => console.log(steps)),
    publishRef(),
  );

  completed$ = combineLatest([
    this.installState$,
    this.selectedComponentReqs$,
  ]).pipe(
    map(
      ([state, reqs]) =>
        state.status === TdsqlStageStatus.SUCCESS &&
        reqs.length === RELIABILITY_COMPONENTS_NAME.length,
    ),
    tap(completed => console.log(`completed: ${completed}`)),
    publishRef(),
  );

  logLoader = {
    context: this.tdsqlApi,
    func: this.tdsqlApi.getLog,
  };

  constructor(
    public readonly cdr: ChangeDetectorRef,
    private readonly dialog: DialogService,
    private readonly tdsqlApi: TdsqlApiService,
  ) {}

  startDeploy() {
    const model = this.form.submit();
    if (model) {
      this.submitting = true;
      this.start.emit({
        model,
        eventType: 'Patch',
      });
    }
  }

  getEntryPoint(cr: TdsqlDeployCR) {
    return get(cr, ['status', 'productEntrypoint']);
  }

  openComponentsOverviewDialog() {
    const model = this.form.form.value;
    this.dialog.open(TdsqlComponentsOverviewDialogComponent, {
      size: DialogSize.Large,
      data: {
        showAllComponents: true,
        cr: this.cr,
        model,
      },
    });
  }

  getOptionalComponentStatus(reqName: string, tasks: TdsqlTask[]) {
    const statusMap = tasks.reduce((acc, cur) => {
      return {
        ...acc,
        [cur.name]: cur.state,
      };
    }, {} as Dictionary<TdsqlTaskStatus>);

    const status = statusMap[componentTasksMap[reqName]];

    if (status === 'RecoverableError' || status === 'UnrecoverableError') {
      return TdsqlStageStatus.FAILED;
    } else if (status === 'Executing') {
      return TdsqlStageStatus.PENDING;
    } else if (status === 'Success') {
      return TdsqlStageStatus.SUCCESS;
    }
  }

  openLogDialog() {
    this.dialog.open(TdsqlLogDialogComponent, {
      data: {
        type: TdsqlLogType.Optional,
      },
    });
  }

  getComponentDocName(name: string) {
    return `tdsql_${name}`;
  }
}
