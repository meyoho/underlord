import {
  Dictionary,
  K8sUtilService,
  TranslateService,
  noop,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { get, merge } from 'lodash-es';
import { Subject } from 'rxjs';
import { map, startWith, switchMap, tap } from 'rxjs/operators';

import { ProductCenterListApi } from 'app/api/product-center/list-api';
import { TdsqlApiService } from 'app/api/tdsql/api';
import { ProductCRD } from 'app/typings';

import { MiddlewareEventType } from '../../types';
import { createEvent } from '../../utils';
import { RELIABILITY_COMPONENTS_NAME, isProcessMode } from '../constant';
import { IpService } from '../forms/fields/ip.service';
import {
  ChituFormModel,
  CoreFormModel,
  ReliabilityFormModel,
  TdsqlAction,
  TdsqlDeployCR,
  TdsqlDeployFormModel,
  TdsqlInstallState,
  TdsqlStageName,
  TdsqlStageNumber,
  TdsqlStageStatus,
} from '../types';
import {
  getDeployTasks,
  getInstallState,
  getNextStage,
  getSchedulerEsReq,
  getTdsqlDeployComponentreqs,
  toPayload,
  toTdsqlFormModel,
} from '../utils';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class TdsqlDeployComponent {
  deploying = false;
  pullInterval = 5 * 1000;
  timeoutId: number;
  version = this.route.snapshot.paramMap.get('version');
  crdName = 'tdsqldeploys.product.alauda.io';

  chituHostIp = '';

  steps = [
    'tdsql_deploy_core_component',
    'tdsql_init_chitu_and_deploy_scheduler',
    'tdsql_deploy_reliability_component',
  ];

  deployMessageMap: Dictionary<
    {
      success: string;
      fail: string;
    },
    TdsqlStageName
  > = {
    core: {
      success: 'tdsql_core_component_deploy_succeed',
      fail: 'tdsql_core_component_deploy_failed',
    },
    chitu: {
      success: 'tdsql_db_test_passed',
      fail: 'tdsql_db_test_failed',
    },
    reliability: {
      success: 'tdsql_component_deploy_succeed',
      fail: 'tdsql_component_deploy_failed',
    },
  };

  currentState: TdsqlInstallState;
  apiVersion = '';
  model: TdsqlDeployFormModel;

  load$$ = new Subject<void>();

  cr: TdsqlDeployCR;
  crd: ProductCRD;

  detail$ = this.load$$.pipe(
    startWith(null as void),
    switchMap(() => this.listApi.getProduct(this.crdName)),
    tap(detail => {
      this.crd = detail.crd;
      this.apiVersion = `${detail.crd.spec.group}/${detail.crd.spec.version}`;
    }),
    publishRef(),
  );

  cr$ = this.detail$.pipe(
    map(({ crs }) => {
      if (crs && crs[0]) {
        const cr = crs[0] as TdsqlDeployCR;
        // 因为后端是异步更新task中的state的，所以可能会出现一个问题：用户在页面上部署了某个可靠性组件后
        // 组件的task state仍然是“Skiped”。如果满足一定的条件，这种情况会导致轮询被终止掉(实际上组件正在安装)
        // 所以此处通过前端提交的数据去掉“skiped”状态
        if (cr.spec.front_state === TdsqlStageName.RELIABILITY) {
          const reqs = getTdsqlDeployComponentreqs(cr);
          const stageTasks = getDeployTasks(cr, cr.spec.front_state);

          reqs.forEach(req => {
            if (RELIABILITY_COMPONENTS_NAME.includes(req.name)) {
              const taskName = `TDSQL-${req.name}`.toUpperCase();
              const task = stageTasks.find(({ name }) => name === taskName);

              if (task && task.state === 'Skipped') {
                delete task.state;
              }
            }
          });
        }
        return cr;
      }
      return null;
    }),
    tap((cr: TdsqlDeployCR) => {
      this.cr = cr;
      this.model = toTdsqlFormModel(cr);
      this.chituHostIp = get(this.model, 'core.chitu.ip', '');
      this.ipService.add(this.model.core?.allIps || []);
      // Currently only the usage of zookeeper need to be initialized.
      this.ipService.updateUsage(
        'zookeeper',
        this.model.core?.zookeeper.ips || [],
      );
    }),
    publishRef(),
  );

  installState$ = this.cr$.pipe(
    map(cr => getInstallState(cr)),
    tap(state => {
      this.currentState = state;
      console.log(`install state: `);
      console.log(state);
      if (
        [
          TdsqlStageStatus.FAILED,
          TdsqlStageStatus.EDIT,
          TdsqlStageStatus.SUCCESS,
          TdsqlStageStatus.PARTIAL_SUCCESS,
        ].includes(state.status)
      ) {
        window.clearTimeout(this.timeoutId);
        if (this.deploying) {
          if (state.status === TdsqlStageStatus.SUCCESS) {
            this.message.success(
              this.translate.get(this.deployMessageMap[state.stage].success),
            );
          } else if (state.status === TdsqlStageStatus.FAILED) {
            this.message.error(
              this.translate.get(this.deployMessageMap[state.stage].fail),
            );
          }
        }
        this.deploying = false;
      } else {
        this.timeoutId = window.setTimeout(() => {
          this.load$$.next();
        }, this.pullInterval);
      }
    }),
    publishRef(),
  );

  constructor(
    private readonly listApi: ProductCenterListApi,
    private readonly route: ActivatedRoute,
    private readonly tdsqlApi: TdsqlApiService,
    private readonly router: Router,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly k8sUtil: K8sUtilService,
    private readonly ipService: IpService,
    private readonly message: MessageService,
  ) {}

  getRefresh() {
    return new Date().getTime().toString();
  }

  getStageTasks(cr: TdsqlDeployCR, stage: TdsqlStageName) {
    return getDeployTasks(cr, stage);
  }

  getStageNumber(state: TdsqlInstallState) {
    return state ? TdsqlStageNumber[state.stage] : 0;
  }

  deploy(cr: TdsqlDeployCR, isUpdate = false, eventType?: MiddlewareEventType) {
    cr.spec.version = this.version;
    cr.spec.front_state = this.currentState.stage;

    if (isUpdate) {
      cr.spec.refresh = this.getRefresh();
    }

    if (eventType) {
      cr.spec.event = createEvent(eventType);
    }

    cr.spec.processdeploy = isProcessMode();
    this.deploying = true;
    const request = isUpdate
      ? this.tdsqlApi.updateTdsqlDeploy.bind(this.tdsqlApi)
      : this.tdsqlApi.createTdsqlDeploy.bind(this.tdsqlApi);
    request(cr).subscribe(() => {
      this.load$$.next();
    });
  }

  deployCore({
    model,
    eventType,
  }: {
    model: CoreFormModel;
    eventType: MiddlewareEventType;
  }) {
    this.model.core = model;
    const td = toPayload({ core: model }, TdsqlStageName.CORE, this.apiVersion);
    const body = merge(this.cr, td);
    this.cr ? this.deploy(body, true, eventType) : this.deploy(body);
  }

  deployChitu({
    model,
    eventType,
  }: {
    model: ChituFormModel;
    eventType: MiddlewareEventType;
  }) {
    this.model.chitu = model;
    const payload = toPayload(
      {
        core: this.model.core,
        chitu: model,
      },
      TdsqlStageName.CHITU,
      this.apiVersion,
    );

    // 此处需要选择allIps中的任意一个非赤兔主机 非赤兔备机的ip 作为默认选项 添加到componentreqs的es中
    const esReq = getSchedulerEsReq(this.cr, this.model.core);
    payload.spec.componentreqs = esReq
      ? [...payload.spec.componentreqs, esReq]
      : payload.spec.componentreqs;

    const body = merge(this.cr, payload);
    this.deploy(body, true, eventType);
  }

  deployReliability({
    model,
    eventType,
  }: {
    model: ReliabilityFormModel;
    eventType: MiddlewareEventType;
  }) {
    this.model.reliability = model;
    const td = toPayload(
      {
        core: this.model.core,
        chitu: this.model.chitu,
        reliability: model,
      },
      TdsqlStageName.RELIABILITY,
      this.apiVersion,
    );
    const body = {
      ...this.cr,
      spec: td.spec,
    };

    this.deploy(body, true, eventType);
  }

  stopDeploy() {
    this.tdsqlApi
      .getTdsqlDeploy()
      .pipe(
        switchMap(({ crs }) => {
          const cr = crs[0];
          cr.spec.event = createEvent('Terminate');
          return this.tdsqlApi.updateTdsqlDeploy(cr);
        }),
      )
      .subscribe(() => this.load$$.next());
  }

  toNextStep() {
    this.currentState = {
      stage: getNextStage(this.currentState.stage),
      status: TdsqlStageStatus.EDIT,
    };
  }

  close(action: TdsqlAction, operate: 'complete' | 'cancel' = 'cancel') {
    this.dialog
      .confirm({
        title: this.translate.get(
          operate === 'cancel'
            ? 'confirm_cancel_deploy'
            : 'confirm_complete_deploy',
        ),
        content:
          TdsqlStageStatus.PENDING === this.currentState.status
            ? this.translate.get(
                action === 'deploy'
                  ? operate === 'cancel'
                    ? 'tdsql_confirm_cancel_deploy_hint'
                    : 'tdsql_confirm_complete_deploy_hint'
                  : 'tdsql_confirm_cancel_test_hint',
              )
            : '',
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      })
      .then(() => {
        this.router.navigate(
          this.cr
            ? ['../../detail', btoa(this.k8sUtil.getName(this.crd))]
            : ['../../../list'],
          {
            relativeTo: this.route,
          },
        );
      })
      .catch(noop);
  }
}
