import { Dictionary } from '@alauda/common-snippet';
import { findLast, get, groupBy, keyBy } from 'lodash-es';

import {
  CHITU_TASK_NAMES,
  COMPONENT_STATUS,
  CORE_TASK_NAMES,
  DEFAULT_REPLICAS_NUM,
  DONE_TASK_NAMES,
  RELIABILITY_TASK_NAMES,
  isProcessMode,
  multiIpComponents,
  singleIpComponents,
} from './constant';
import {
  ChituFormModel,
  CoreFormModel,
  ReliabilityFormModel,
  TdsqlComponentReq,
  TdsqlDeployCR,
  TdsqlDeployFormModel,
  TdsqlInstallState,
  TdsqlInstallationStep,
  TdsqlStageName,
  TdsqlStageNumber,
  TdsqlStageStatus,
  TdsqlTask,
  TdsqlTaskStatus,
} from './types';

export function removeHostIpPrefix(fullText: string) {
  const prefix = 'ansible_ssh_host=';
  return fullText ? fullText.replace(prefix, '') : '';
}

// form ip -> resource ip
export function toHostip(name: string, ips: string[] | string) {
  if (typeof ips === 'string') {
    return {
      [`${name}1`]: `ansible_ssh_host=${ips}`,
    };
  } else {
    return ips.reduce((acc, ip: string, index: number) => {
      return {
        ...acc,
        [`${name}${index + 1}`]: `ansible_ssh_host=${ip}`,
      };
    }, {});
  }
}

// resource ip -> form ip
export function parseHostip(
  name: string,
  ips: { [key: string]: string },
): string[] | string {
  if (singleIpComponents.has(name)) {
    return removeHostIpPrefix(ips[`${name}1`]);
  } else if (multiIpComponents.has(name)) {
    return Object.keys(ips || {}).map((_, index: number) => {
      const key = name === 'envcheck' ? 'mac' : name;
      return removeHostIpPrefix(ips[`${key}${index + 1}`]);
    });
  } else {
    return '';
  }
}

// core form -> td
export function toCoreComponentReqs(
  model: TdsqlDeployFormModel,
): TdsqlComponentReq[] {
  if (!model.core) {
    return [];
  }
  return [
    {
      hostip: toHostip('mac', model.core.allIps),
      name: 'envcheck',
    },
    {
      hostip: toHostip('zk', model.core.zookeeper.ips),
      config: {
        zk_rootdir: model.core.zookeeper.rootPath,
      },
      name: 'zk',
      version: '',
    },
    {
      config: {
        netif_name: model.core.scheduler.netifName,
      },
      hostip: toHostip('scheduler', model.core.scheduler.ip),
      name: 'scheduler',
      version: '',
    },
    {
      config: {
        replicas_num: String(model.core.oss.replicas_num),
      },
      hostip: model.core.oss.ips ? toHostip('oss', model.core.oss.ips) : {},
      name: 'oss',
      version: '',
    },
    {
      config: {
        replicas_num: String(model.core.chitu.replicas_num),
      },
      hostip: toHostip(
        'chitu',
        [model.core.chitu.ip, model.core.chitu.standbyIp].filter(ip => !!ip),
      ),
      name: 'chitu',
      version: '',
    },
    {
      config: toChituConfig(model), // 在第二步
      hostip: toHostip('monitor', model.core.monitor.ip),
      name: 'monitor',
      version: '',
    },
    {
      hostip: toHostip('db', model.core.db.ips),
      name: 'db',
      version: '',
    },
    {
      hostip: toHostip('proxy', model.core.proxy.ips),
      name: 'proxy',
      version: '',
    },
  ];
}
// chitu form -> td
export function toChituConfig(formModel: TdsqlDeployFormModel) {
  const chitu = get(formModel, 'chitu', {}) as ChituFormModel;
  return {
    metadb_ip: chitu.metadbIp || '',
    metadb_ip_bak: chitu.metadbIpBak || '',
    metadb_password: chitu.metadbPassword || '',
    metadb_port: chitu.metadbPort || '',
    metadb_port_bak: chitu.metadbPortBak || '',
    metadb_user: chitu.metadbUser || '',
  };
}
// reliability form -> td
export function toReliabilityComponentReqs(
  form: TdsqlDeployFormModel,
): TdsqlComponentReq[] {
  const reliability = get(form, 'reliability', {} as ReliabilityFormModel);
  const reliabilityComponentreqs = [
    reliability.hdfs
      ? {
          config: {
            hdfs_datadir: reliability.hdfs.dataFilePath,
            ssh_port: reliability.hdfs.sshPort,
          },
          hostip: toHostip('hdfs', reliability.hdfs.hostIps),
          name: 'hdfs',
          enabled: reliability.hdfs.selected,
          version: '',
        }
      : null,
    reliability.kafka
      ? {
          config: {
            kafka_logdir: reliability.kafka.logFilePath,
          },
          hostip: toHostip('kafka', reliability.kafka.hostIps),
          name: 'kafka',
          enabled: reliability.kafka.selected,
          version: '',
        }
      : null,
    reliability.consumer
      ? {
          hostip: toHostip('consumer', reliability.consumer.hostIp),
          config: {
            netif_name: reliability.consumer.networkCardName,
          },
          name: 'consumer',
          enabled: reliability.consumer.selected,
          version: '',
        }
      : null,
    reliability.lvs
      ? {
          hostip: toHostip('lvs', reliability.lvs.hostIps),
          name: 'lvs',
          enabled: reliability.lvs.selected,
          version: '',
        }
      : null,
    reliability.es
      ? {
          config: {
            es_log_days: reliability.es.logDays.toString(),
            es_mem: reliability.es.memory.toString(),
            es_base_path: reliability.es.dataPath,
          },
          hostip: toHostip('es', reliability.es.hostIp),
          name: 'es',
          enabled: reliability.es.selected,
          version: '',
        }
      : null,
    reliability.backupchituop
      ? {
          hostip: toHostip(
            'chitu',
            [
              reliability.backupchituop.hostIp,
              reliability.backupchituop.standbyIp,
            ].filter(ip => !!ip),
          ),
          name: 'backupchituop',
          enabled: reliability.backupchituop.selected,
          version: '',
        }
      : null,
  ].filter(c => !!c);
  console.log('reliability form -> tdsql deploy', reliabilityComponentreqs);
  return reliabilityComponentreqs;
}

// form -> td
export function toPayload(
  form: TdsqlDeployFormModel,
  stage: TdsqlStageName,
  apiVersion = 'product.alauda.io/v1alpha1',
) {
  return {
    apiVersion,
    kind: 'TDSQLDeploy',
    metadata: {
      name: 'tdsqldeploy-home1',
    },
    spec: {
      cluster: get(form, 'core.cluster'),
      componentreqs: [
        ...toCoreComponentReqs(form),
        ...(stage === TdsqlStageName.RELIABILITY
          ? toReliabilityComponentReqs(form)
          : []),
      ],
      password: btoa(get(form, 'core.password')),
      refresh: '',
      version: '',
      scope: 'CLUSTER',
    },
  };
}

// td -> form
export function toTdsqlFormModel(cr: TdsqlDeployCR) {
  const core = toCoreFormModel(cr);
  const chitu = toChituFormModel(cr);
  const reliability = toReliabilityFormModel(cr);
  return {
    core,
    chitu,
    reliability,
  };
}

export function getTdsqlDeployComponentreqs(td: TdsqlDeployCR) {
  return get(td, 'spec.componentreqs', []) as TdsqlComponentReq[];
}

// td -> core form
export function toCoreFormModel(cr: TdsqlDeployCR): CoreFormModel {
  if (!cr) {
    return null;
  }
  const reqsObj = keyBy(cr.spec.componentreqs, req => req.name);
  return {
    cluster: cr.spec.cluster,
    password: atob(cr.spec.password || ''),
    allIps: parseHostip('envcheck', reqsObj.envcheck.hostip) as string[],
    zookeeper: {
      rootPath: reqsObj.zk.config.zk_rootdir,
      ips: parseHostip('zk', reqsObj.zk.hostip) as string[],
    },
    scheduler: {
      netifName: reqsObj.scheduler.config.netif_name,
      ip: parseHostip('scheduler', reqsObj.scheduler.hostip) as string,
    },
    oss: {
      ...(isProcessMode()
        ? {
            ips: parseHostip('oss', reqsObj.oss.hostip) as string[],
          }
        : {
            replicas_num:
              +reqsObj.oss.config.replicas_num || DEFAULT_REPLICAS_NUM,
          }),
    },
    chitu: {
      ...(isProcessMode()
        ? {
            ip: removeHostIpPrefix(
              get(reqsObj, 'chitu.hostip.chitu1', '') as string,
            ),
            standbyIp: removeHostIpPrefix(
              get(reqsObj, 'chitu.hostip.chitu2', '') as string,
            ),
          }
        : {
            replicas_num:
              +reqsObj.chitu.config.replicas_num || DEFAULT_REPLICAS_NUM,
          }),
    },
    monitor: {
      ip: parseHostip('monitor', reqsObj.monitor.hostip) as string,
    },
    db: {
      ips: parseHostip('db', reqsObj.db.hostip) as string[],
    },
    proxy: {
      ips: parseHostip('proxy', reqsObj.proxy.hostip) as string[],
    },
  };
}

// td -> chitu form
export function toChituFormModel(cr: TdsqlDeployCR): ChituFormModel {
  if (!cr) {
    return null;
  }
  const reqsObj = keyBy(cr.spec.componentreqs, req => req.name);
  const config = reqsObj.monitor.config || {};
  return {
    metadbIp: config.metadb_ip,
    metadbPort: config.metadb_port,
    metadbIpBak: config.metadb_ip_bak,
    metadbPortBak: config.metadb_port_bak,
    metadbUser: config.metadb_user,
    metadbPassword: config.metadb_password,
  };
}

export function findComponentReq(reqs: TdsqlComponentReq[], name: string) {
  return (reqs || []).find(req => req.name === name) || null;
}

// td -> reliability form
export function toReliabilityFormModel(
  cr: TdsqlDeployCR,
): ReliabilityFormModel {
  if (!cr) {
    return null;
  }
  const reqsObj = keyBy(cr.spec.componentreqs, req => req.name);
  return {
    hdfs: reqsObj.hdfs
      ? {
          selected: reqsObj.hdfs.enabled,
          dataDirPrepare: true,
          hostNameSetting: true,
          sshPort: reqsObj.hdfs.config.ssh_port,
          dataFilePath: reqsObj.hdfs.config.hdfs_datadir,
          hostIps: parseHostip('hdfs', reqsObj.hdfs.hostip) as string[],
        }
      : null,
    kafka: reqsObj.kafka
      ? {
          selected: reqsObj.kafka.enabled,
          logFilePath: reqsObj.kafka.config.kafka_logdir,
          hostIps: parseHostip('kafka', reqsObj.kafka.hostip) as string[],
        }
      : null,
    consumer: reqsObj.consumer
      ? {
          selected: reqsObj.consumer.enabled,
          hostIp: parseHostip('consumer', reqsObj.consumer.hostip) as string,
          networkCardName: reqsObj.consumer.config?.netif_name,
        }
      : null,
    lvs: reqsObj.lvs
      ? {
          selected: reqsObj.lvs.enabled,
          hostIps: parseHostip('lvs', reqsObj.lvs.hostip) as string[],
        }
      : null,
    es: reqsObj.es
      ? {
          selected: reqsObj.es.enabled,
          memory: reqsObj.es.config.es_mem,
          logDays: reqsObj.es.config.es_log_days,
          hostIp: parseHostip('es', reqsObj.es.hostip) as string,
          dataPath: reqsObj.es.config.es_base_path,
        }
      : null,
    backupchituop:
      isProcessMode() && (reqsObj.backupchituop || reqsObj.chitu)
        ? {
            selected:
              !!get(reqsObj, 'chitu.hostip.chitu2', '') ||
              reqsObj.backupchituop?.enabled,
            hostIp: removeHostIpPrefix(
              get(reqsObj, 'chitu.hostip.chitu1', '') as string,
            ),
            standbyIp: removeHostIpPrefix(
              reqsObj.chitu?.hostip?.chitu2 ||
                reqsObj.backupchituop?.hostip?.chitu2,
            ),
          }
        : null,
  };
}

export function getDeployTasks(
  cr: TdsqlDeployCR,
  stageName?: TdsqlStageName,
): TdsqlTask[] {
  const tasks = get(cr, 'status.tasks', []);
  switch (stageName) {
    case TdsqlStageName.CORE:
      return tasks.filter((step: TdsqlInstallationStep) => {
        return CORE_TASK_NAMES.includes(step.name);
      });
    case TdsqlStageName.CHITU:
      return tasks.filter((step: TdsqlInstallationStep) => {
        return CHITU_TASK_NAMES.includes(step.name);
      });
    case TdsqlStageName.RELIABILITY: {
      return tasks.filter((step: TdsqlInstallationStep) => {
        return RELIABILITY_TASK_NAMES.includes(step.name);
      });
    }
    default:
      return tasks;
  }
}

export function getRefresh(td: TdsqlDeployCR) {
  return get(td, 'spec.refresh', '');
}

export function getLastRefresh(td: TdsqlDeployCR) {
  return get(td, 'status.lastrefresh', '');
}

// eslint-disable-next-line sonarjs/cognitive-complexity
export function getCurrentStageStatus(cr: TdsqlDeployCR) {
  if (!cr) {
    return TdsqlStageStatus.EDIT;
  }

  const stageTasks = getDeployTasks(cr, cr.spec.front_state);
  const latestTask = findLast(
    stageTasks,
    task => !!task.state && task.state !== 'Skipped',
  );
  if (!latestTask) {
    return TdsqlStageStatus.PENDING;
  }

  if (
    latestTask.state === 'UnrecoverableError' ||
    latestTask.state === 'RecoverableError' ||
    latestTask.state === 'Terminated'
  ) {
    return TdsqlStageStatus.FAILED;
  } else if (
    DONE_TASK_NAMES.includes(latestTask.name) &&
    ['Success', 'Paused'].includes(latestTask.state)
  ) {
    return TdsqlStageStatus.SUCCESS;
  } else {
    if (cr.spec.front_state === 'reliability') {
      const groupedTasks = groupBy(
        stageTasks,
        task => get(task, 'state', '').toLowerCase() || 'waiting',
      );
      const total = RELIABILITY_TASK_NAMES.length;
      if (groupedTasks.executing) {
        return TdsqlStageStatus.PENDING;
      }
      if (groupedTasks.success) {
        if (groupedTasks.success.length === total) {
          return TdsqlStageStatus.SUCCESS;
        } else if (groupedTasks.waiting && groupedTasks.waiting.length) {
          return TdsqlStageStatus.PENDING;
        } else if (
          groupedTasks.unrecoverableerror &&
          groupedTasks.unrecoverableerror.length
        ) {
          return TdsqlStageStatus.FAILED;
        } else {
          return TdsqlStageStatus.PARTIAL_SUCCESS;
        }
      }
    } else {
      return TdsqlStageStatus.PENDING;
    }
  }
}

export function getNextStage(stage: TdsqlStageName): TdsqlStageName {
  const stageNumber = TdsqlStageNumber[stage];
  return TdsqlStageNumber[stageNumber + 1] as TdsqlStageName;
}

export function getInstallState(cr?: TdsqlDeployCR): TdsqlInstallState {
  if (!cr) {
    return {
      stage: TdsqlStageName.CORE,
      status: TdsqlStageStatus.EDIT,
    };
  }

  return {
    stage: cr.spec.front_state,
    status: getCurrentStageStatus(cr),
  };
}

// todo: temp hack, 腾讯的bug
export function getSchedulerEsReq(
  cr: TdsqlDeployCR,
  coreModel: CoreFormModel,
): TdsqlComponentReq {
  const reqs = getTdsqlDeployComponentreqs(cr);
  const avaliableIps = coreModel.allIps.filter((ip: string) => {
    return ![coreModel.chitu.ip, coreModel.chitu.standbyIp].includes(ip);
  });

  const esIp = get(avaliableIps, [0], '');
  const esReq = {
    name: 'tempfix',
    hostip: {
      es1: `ansible_ssh_host=${esIp}`,
    },
  };
  const hasEsConfig = reqs.find((req: TdsqlComponentReq) => {
    return req.name === 'tempfix';
  });

  if (hasEsConfig) {
    return null;
  } else {
    return esReq;
  }
}

// eslint-disable-next-line sonarjs/cognitive-complexity
export function getComponentsStatus(
  cr: TdsqlDeployCR,
  model?: CoreFormModel | ReliabilityFormModel,
) {
  // 只在容器化部署的时候才显示集群这一行
  const cluster = get(cr, 'spec.cluster', '') || get(model, 'cluster', '');
  const componentMap = genComponetMap(model, cluster);

  const taskMap = (get(cr, ['status', 'tasks'], []) as TdsqlTask[]).reduce(
    (acc, curr) => {
      const ret = /^tdsql-([\W\w]*)$/.exec(curr.name.toLowerCase());

      if (ret) {
        acc[ret[1]] = curr;
      }

      return acc;
    },
    {} as Dictionary<TdsqlTask>,
  );

  /**
   * 计算组件状态
   * 1.当cr.spec.componentreqs与cr.status.tasks中的项目的name相同时，直接取该task的status
   * 2.当componentreqs中的name为monitor时，取TDSQL-CheckMonitorDBConnection这一task的status
   * 3.当componentreqs中的name为zk时，取zookeeper这一task的status
   * 4.其它组件为COMPONENT_STATUS.TO_BE_DEPLOYED状态
   */
  const reqs = getTdsqlDeployComponentreqs(cr);

  reqs.forEach(req => {
    if (!req.hostip) return;

    let name = req.name;
    let state = COMPONENT_STATUS.TO_BE_DEPLOYED;
    const ips = parseHostip(req.name, req.hostip);

    if (taskMap[req.name]) {
      state = getStatusTextByStatus(taskMap[name].state);
    } else if (req.name === 'monitor' && taskMap.checkmonitordbconnection) {
      state = getStatusTextByStatus(taskMap.checkmonitordbconnection.state);
    } else if (req.name === 'zk' && taskMap.zookeeper) {
      name = 'zookeeper';
      state = getStatusTextByStatus(taskMap[name].state);
    } else {
      status = COMPONENT_STATUS.TO_BE_DEPLOYED;
    }

    if (typeof ips === 'string') {
      componentMap[getComponentId(name, ips)] = state;
    } else if (Array.isArray(ips)) {
      ips.forEach(ip => {
        componentMap[getComponentId(name, ip)] = state;
      });
    }
  });

  /**
   * oss、chitu的状态计算逻辑与其他组件不一样，需要单独处理
   * 1.在容器化安装的时候，oss、chitu的组件状态依赖于cr tasks里的TDSQL-Chart-Chitu-OSS，而且它们的状态仅仅在
   * cluster这一行展示，所以需要将part1done的状态取出，填充在cluster行与oss、chitu列交叉的单元格中
   * 2.在进程化安装的时候，oss、chitu组件需要填写ip，oss对应的installationstep为oss，无需单独处理，
   * chitu主机对应installationsteps中的chitu，赤兔备机对应backupchitu、backupchituop
   */
  if (isProcessMode()) {
    let chituHostIp = '';
    let chituSlaveIp = '';

    if (model) {
      chituHostIp =
        get(model, 'chitu.ip', '') || get(model, 'backupchituop.hostIp', '');
      chituSlaveIp =
        get(model, 'chitu.standbyIp', '') ||
        get(model, 'backupchituop.standbyIp', '');
    } else {
      chituHostIp = removeHostIpPrefix(
        get(findComponentReq(reqs, 'chitu'), 'hostip.chitu1', ''),
      );

      chituSlaveIp = removeHostIpPrefix(
        get(findComponentReq(reqs, 'chitu'), 'hostip.chitu2', '') ||
          get(findComponentReq(reqs, 'backupchituop'), 'hostip.chitu2', ''),
      );
    }

    const chituSlaveTask = taskMap.backupchitu || taskMap.backupchituop;
    const state = chituSlaveIp
      ? chituSlaveTask
        ? getStatusTextByStatus(chituSlaveTask.state)
        : COMPONENT_STATUS.TO_BE_DEPLOYED
      : '';

    delete componentMap[getComponentId('chitu', chituSlaveIp)];
    delete componentMap[getComponentId('backupchituop', chituHostIp)];
    componentMap[getComponentId('backupchituop', chituSlaveIp)] = state;
  } else {
    const ossChituStep = taskMap['chart-chitu-oss'];

    if (cluster && ossChituStep) {
      const state = cr
        ? getStatusTextByStatus(ossChituStep.state)
        : COMPONENT_STATUS.TO_BE_DEPLOYED;

      componentMap[getComponentId('oss', cluster)] = state;
      componentMap[getComponentId('chitu', cluster)] = state;
    }
  }

  // 表格的第一列作为这一行的名称
  const rowNames =
    model && (model as CoreFormModel).allIps
      ? getIpsFromModel(model as CoreFormModel)
      : getIpsFromCr(cr);

  const firstColumn = cluster ? [cluster, ...rowNames] : rowNames;

  const componentArr: Array<Dictionary<string>> = firstColumn.map(name => ({
    name,
  }));

  Object.entries(componentMap).forEach(([id, status]) => {
    const [componentName, rowName] = extractComponentId(id);
    const item = componentArr.find(component => component.name === rowName);
    if (item) item[componentName] = status;
  });

  return componentArr;
}

/**
 * 通过formModel生成一个map，map的key是一个由组件的名字和ip按照一定的规则生成的id，
 * value表示该ip下此组件的状态
 */
function genComponetMap(
  model: CoreFormModel | ReliabilityFormModel,
  cluster?: string,
) {
  return Object.entries(model || {}).reduce((acc, [componentName, value]) => {
    if (!value) return acc;

    if (singleIpComponents.has(componentName)) {
      const ip = get(value, 'ip', '') || get(value, 'hostIp', '');
      acc[getComponentId(componentName, ip)] = COMPONENT_STATUS.FILED_UP;
    } else if (
      ['zookeeper', 'db', 'proxy', 'hdfs', 'kafka', 'lvs'].includes(
        componentName,
      )
    ) {
      const ips = (get(value, 'ips') || get(value, 'hostIps', [])) as string[];
      ips.forEach(ip => {
        acc[getComponentId(componentName, ip)] = COMPONENT_STATUS.FILED_UP;
      });
    } else if (componentName === 'oss') {
      if (isProcessMode()) {
        const ips = get(value, 'ips', []) as string[];
        ips.forEach(ip => {
          acc[getComponentId(componentName, ip)] = COMPONENT_STATUS.FILED_UP;
        });
      } else if (cluster) {
        acc[getComponentId('oss', cluster)] = COMPONENT_STATUS.FILED_UP;
      }
    } else if (componentName === 'chitu') {
      if (isProcessMode()) {
        [value.ip, value.standbyIp].forEach(ip => {
          acc[getComponentId(componentName, ip)] = COMPONENT_STATUS.FILED_UP;
        });
      } else if (cluster) {
        acc[getComponentId('chitu', cluster)] = COMPONENT_STATUS.FILED_UP;
      }
    } else if (componentName === 'backupchituop') {
      [value.hostIp, value.standbyIp].forEach(ip => {
        acc[getComponentId(componentName, ip)] = COMPONENT_STATUS.FILED_UP;
      });
    }

    return acc;
  }, {} as Dictionary<string>);
}

const ILLEGAL_CHARACTER = '$';

function getComponentId(componentName: string, rowName: string) {
  return componentName + ILLEGAL_CHARACTER + rowName;
}

function extractComponentId(id: string) {
  return id.split(ILLEGAL_CHARACTER);
}

function getIpsFromModel(model: CoreFormModel) {
  return get(model, 'allIps', []) as string[];
}

function getIpsFromCr(cr: TdsqlDeployCR) {
  const reqs = getTdsqlDeployComponentreqs(cr);
  const envConfig = reqs.find(req => req.name === 'envcheck');

  return envConfig
    ? Object.values(envConfig.hostip).map(val => val.split('=')[1])
    : [];
}

function getStatusTextByStatus(state: TdsqlTaskStatus) {
  if (state === 'Success') {
    return COMPONENT_STATUS.DEPLOYED;
  } else if (['UnrecoverableError', 'RecoverableError'].includes(state)) {
    return COMPONENT_STATUS.DEPLOYMENT_FAILURE;
  } else {
    return COMPONENT_STATUS.TO_BE_DEPLOYED;
  }
}

export function getProductHref(href: string) {
  if (!href) return '';
  href = href.trim();
  return href.startsWith('http://') || href.startsWith('https://')
    ? href
    : `${window.location.origin}${href}?id_token=${localStorage.getItem(
        'id_token',
      )}`;
}
