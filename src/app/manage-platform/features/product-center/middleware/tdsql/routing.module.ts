import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TdsqlDeployComponent } from './deploy/component';
import { TdsqlDeployDetailComponent } from './detail/component';

const routes: Routes = [
  { path: '', redirectTo: 'deploy', pathMatch: 'full' },
  {
    path: 'deploy/:version',
    component: TdsqlDeployComponent,
  },
  {
    path: 'detail/:name',
    component: TdsqlDeployDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TdsqlRoutingModule {}
