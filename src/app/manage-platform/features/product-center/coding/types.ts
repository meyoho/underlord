import { KubernetesResource } from '@alauda/common-snippet/types/k8s';

export interface KubernetesResourceNode extends KubernetesResource {
  nodes?: KubernetesResource[];
  controlNode?: number;
  computingNode?: number;
  label?: string;
  disableTip?: string;
}

export interface Address {
  type: string;
  address: string;
}

export interface Condition {
  status?: string;
  type?: string;
  reason?: string;
  message?: string;
  lastHeartbeatTime?: string;
  lastTransitionTime?: string;
}

export interface ModulesStatusLogs {
  type: string;
  reason: string;
  message: string;
}

export interface ModulesStatus {
  name: string;
  displayName: string;
  status: string;
  logs?: ModulesStatusLogs[];
}

export interface CodingResourceSpec {
  clusterName?: string;
  nodes?: string[];
  rootDomain?: string;
  secondLevelDomain?: string;
  version?: string;
}

export interface CodingResourceStatus {
  conditions?: Condition[];
  modulesStatus?: ModulesStatus[];
  version?: string;
  phase?: string;
  productEntrypoint?: string;
  deletable?: boolean;
}

export interface CodingResource extends KubernetesResource {
  spec?: CodingResourceSpec;
  status?: CodingResourceStatus;
}

export interface ValidatorType {
  [key: string]: {
    value: string;
  };
}

export interface NodeSelected {
  [key: number]: string;
}
