import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { NodeInfoComponent } from './components/deploy-nodes/component';
import { DeployProcessComponent } from './components/deploy-process/component';
import { DeployComponent } from './deploy/component';
import { CodingDetailComponent } from './detail/component';
import { ClusterNodesComponent } from './detail/components/node-list/component';
import { CodingRoutingModule } from './routing.module';

@NgModule({
  imports: [SharedModule, CodingRoutingModule],
  declarations: [
    DeployComponent,
    NodeInfoComponent,
    CodingDetailComponent,
    DeployProcessComponent,
    ClusterNodesComponent,
  ],
  exports: [],
  entryComponents: [DeployProcessComponent],
})
export class CodingModule {}
