import {
  K8sApiService,
  K8sUtilService,
  KubernetesResourceList,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { get } from 'lodash-es';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { RESOURCE_TYPES, ResourceType } from 'app/utils';

import { DeployProcessComponent } from '../components/deploy-process/component';
import { CodingResource, KubernetesResourceNode, NodeSelected } from '../types';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeployComponent implements OnInit {
  @ViewChild(NgForm)
  clusterForm: NgForm;

  version: string;

  clusters$: Observable<KubernetesResourceNode[]>;
  selectedCluster: KubernetesResourceNode;
  nodes: NodeSelected;
  businessNode: NodeSelected;
  deployLoading: boolean;
  secondLevelDomain: string;
  rootDomain: string;

  // 默认coding项目名称
  private readonly codingName = 'coding-platform';

  constructor(
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly dialogService: DialogService,
    private readonly translate: TranslateService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly k8sUtil: K8sUtilService,
  ) {
    this.version = get(this.route.snapshot.params, 'version') || 'v2.5';
  }

  ngOnInit(): void {
    this.getClusters();
  }

  getClusters() {
    this.clusters$ = this.k8sApi
      .getGlobalResourceList<KubernetesResourceList>({
        type: RESOURCE_TYPES.CLUSTER_REGISTRY,
        namespaced: true,
      })
      .pipe(
        map((res: KubernetesResourceList) => {
          return res.items.map((cluster: KubernetesResourceNode) => {
            cluster.controlNode = 0;
            cluster.computingNode = 0;
            cluster.label = '';
            cluster.disableTip = this.clusterDisableTip(cluster);
            this.fetchNodeList(cluster.metadata.name).subscribe(nodes => {
              cluster.nodes = nodes.items;
              nodes.items.forEach(node => {
                if ('node-role.kubernetes.io/master' in node.metadata.labels) {
                  cluster.controlNode++;
                } else if (
                  'node-role.kubernetes.io/node' in node.metadata.labels
                ) {
                  cluster.computingNode++;
                }
              });
              const version = get(cluster, 'status.version');
              cluster.label =
                cluster.metadata.name +
                ' (' +
                this.k8sUtil.getAnnotation(cluster, 'display-name') +
                ') ' +
                version +
                this.translate.get('coding_cluster_select_options', {
                  controlNode: cluster.controlNode,
                  computingNode: cluster.computingNode,
                  version: version,
                });
              cluster.disableTip = this.clusterDisableTip(cluster);
            });
            return cluster;
          });
        }),
        publishRef(),
      );
  }

  fetchNodeList(clusterName: string) {
    return this.k8sApi.getResourceList<KubernetesResourceNode>({
      type: RESOURCE_TYPES.NODE,
      cluster: clusterName,
    });
  }

  async cancel() {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('deployment_tips'),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
      this.router.navigate(['/manage-platform/product']);
    } catch (e) {}
  }

  deploy() {
    this.clusterForm.onSubmit(null);
    if (this.clusterForm.valid) {
      this.deployLoading = true;
      this.k8sApi
        .postGlobalResource<CodingResource>({
          type: RESOURCE_TYPES.CODING,
          resource: this.depoyRequestBody() as CodingResource,
        })
        .subscribe(
          () => this.deployProcess(),
          () => this.deployProcess(),
        );
    }
  }

  depoyRequestBody() {
    return {
      apiVersion: 'product.alauda.io/v1',
      kind: 'Coding',
      metadata: {
        name: this.codingName,
      },
      spec: {
        nodes: Object.keys(this.nodes).map(
          (key: string) => this.nodes[parseInt(key)],
        ),
        rootDomain: this.rootDomain,
        secondLevelDomain: this.secondLevelDomain,
        clusterName: get(this.selectedCluster, 'metadata.name'),
        version: this.version,
      },
    };
  }

  getNodes() {
    return this.selectedCluster ? this.selectedCluster.nodes : [];
  }

  deployProcess() {
    this.dialogService.open(DeployProcessComponent, {
      size: DialogSize.Large,
    });
  }

  clusterDisableTip(cluster: KubernetesResourceNode) {
    const version = get(cluster, 'status.version');
    if (!version || version < '1.16') {
      return 'coding_cluster_version_error';
    }
    const nodes = cluster.computingNode + cluster.controlNode;
    return nodes < 3 ? 'coding_cluster_node_error' : null;
  }
}
