import {
  BaseNormalizeTypeParams,
  K8SResourceList,
  K8sApiService,
  K8sUtilService,
  NAME,
  ObservableInput,
  StringMap,
  ValueHook,
  publishRef,
} from '@alauda/common-snippet';
import { Component, Inject, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, combineLatest } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { v4 as uuidV4 } from 'uuid';

import {
  Metric,
  MetricQueries,
  MetricService,
} from 'app/api/alarm/metric.service';
import { Environments } from 'app/api/envs/types';
import { ENVIRONMENTS } from 'app/services/services.module';
import {
  Node,
  NodeMetrics,
  NodeStatusColorMapper,
  NodeStatusEnum,
  NodeStatusIconMapper,
  NodeTaint,
  TKEMachine,
} from 'app/typings';
import { RESOURCE_TYPES, ResourceType, STATUS, getNodeStatus } from 'app/utils';

import { formatCommonUnit, formatNumUnit } from '../../../../utils';

interface NodeExtended extends Node {
  type?: string;
  displayName?: string;
}

const MASTERLABEL = 'node-role.kubernetes.io/master';

@Component({
  selector: 'alu-cluster-nodes',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class ClusterNodesComponent {
  @ValueHook<ClusterNodesComponent, 'clusterName'>(function () {
    this.list = new K8SResourceList({
      fetcher: this.fetchNodeList.bind(this),
      activatedRoute: this.activatedRoute,
    });
  })
  @Input()
  clusterName: string;

  @Input()
  isOCP: boolean;

  @Input()
  isBaremetal: boolean;

  @Input()
  isGPU: boolean;

  @ObservableInput()
  @Input('metricDocked')
  metricDocked$: Observable<boolean>;

  columns = [
    NAME,
    STATUS,
    'privateIp',
    'deployCoding',
    'nodeType',
    'schedule',
    'cpu',
    'memory',
  ];

  TYPE_TKE_MACHINE = 'Machine';
  searchKey = '';
  items: NodeExtended[];
  machines: TKEMachine[];
  MASTERLABEL = MASTERLABEL;
  getNodeStatus = getNodeStatus;
  formatCPU = formatNumUnit;
  NodeStatusEnum = NodeStatusEnum;
  NodeStatusColorMapper = NodeStatusColorMapper;
  NodeStatusIconMapper = NodeStatusIconMapper;
  nodeCpuMemMap: {
    [key: string]: StringMap;
  } = {};

  list = new K8SResourceList({
    fetcher: this.fetchNodeList.bind(this),
    activatedRoute: this.activatedRoute,
  });

  nodeLabel: BaseNormalizeTypeParams = {
    type: 'coding-group',
    baseDomain: `product.${this.env.LABEL_BASE_DOMAIN}`,
    prefix: 'coding',
  };

  constructor(
    @Inject(ENVIRONMENTS) private readonly env: Environments,
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    private readonly metricService: MetricService,
  ) {}

  fetchNodeList() {
    return combineLatest([
      this.k8sApi.getGlobalResourceList<TKEMachine>({
        type: RESOURCE_TYPES.MACHINE,
        queryParams: {
          fieldSelector: 'spec.clusterName=' + this.clusterName,
        },
      }),
      this.k8sApi.getResourceList<Node>({
        type: RESOURCE_TYPES.NODE,
        cluster: this.clusterName,
      }),
    ]).pipe(
      map(([machines, nodes]) => {
        this.machines = machines.items;
        const deploying: NodeExtended[] = machines.items
          .filter(
            machine =>
              machine.status.phase === 'Initializing' &&
              !nodes.items.find(node => node.metadata.name === machine.spec.ip),
          )
          .map(machine => ({
            ...machine,
            type: this.TYPE_TKE_MACHINE,
            displayName: '',
          })) as NodeExtended[];
        return {
          ...nodes,
          items: nodes.items
            .map(
              it =>
                ({
                  ...it,
                  displayName: this.k8sUtil.getDisplayName(
                    machines.items.find(
                      machine => it.metadata.name === machine.spec.ip,
                    ),
                  ),
                } as NodeExtended),
            )
            .concat(deploying)
            .sort((a, b) => {
              // L1 已部署 Coding 节点；L2 未部署 Coding 控制节点；L3 未部署 Coding 计算节点
              if (this.k8sUtil.getLabel(a, this.nodeLabel) === 'enabled') {
                return -1;
              }
              if (this.k8sUtil.getLabel(b, this.nodeLabel) === 'enabled') {
                return 1;
              }
              if (
                this.k8sUtil.getLabel(a, {
                  type: 'master',
                  baseDomain: 'node-role.kubernetes.io',
                })
              ) {
                return 1;
              }
              if (
                this.k8sUtil.getLabel(b, {
                  type: 'master',
                  baseDomain: 'node-role.kubernetes.io',
                })
              ) {
                return -1;
              }
              return a.metadata.name.localeCompare(b.metadata.name);
            }),
        };
      }),
      tap(res => {
        this.items = res.items;
        this.metricDocked$.pipe(publishRef()).subscribe(metricDocked => {
          res.items.forEach(node => {
            if (!node.type) {
              return metricDocked
                ? this.queryPrometheus(node)
                : this.queryMetricServer(node);
            }
          });
        });
      }),
    );
  }

  search(keyword: string) {
    this.list.scanItems(() =>
      this.items.filter(item =>
        item.type !== this.TYPE_TKE_MACHINE
          ? item.metadata.name.includes(keyword) ||
            this.getPrivateIp(item).includes(keyword) ||
            this.k8sUtil.getDisplayName(item)?.includes(keyword) ||
            this.searchByLables(keyword, item)
          : item.spec.ip.includes(keyword) ||
            this.k8sUtil.getDisplayName(item)?.includes(keyword),
      ),
    );
  }

  searchByLables(keyword: string, item: NodeExtended) {
    let contains = false;
    let key = '';
    let value = '';
    const match = /([\w-./]+)\s*([^\w-./])?\s*([\w-./]*)/.exec(keyword);
    if (match) {
      key = match[1];
      if (match[3]) {
        value = match[3];
      }
    }
    Object.keys(item.metadata.labels).forEach(labelKey => {
      if (value) {
        if (
          labelKey === key &&
          item.metadata.labels[labelKey].includes(value)
        ) {
          contains = true;
        }
      } else if (
        labelKey.includes(key) ||
        item.metadata.labels[labelKey].includes(key)
      ) {
        contains = true;
      }
    });
    return contains;
  }

  private queryPrometheus(node: NodeExtended) {
    const privateIp = this.getPrivateIp(node);
    const hostname = this.getHostname(node);
    const nodeName = this.k8sUtil.getName(node);
    const nodeMemory = this.formatMemory(node.status.allocatable.memory);
    let name = '';
    if (this.isOCP) {
      name = hostname;
    } else {
      name = `${privateIp}:.*`;
    }
    const metrics = [] as Array<{
      uuid: string;
      query: string;
    }>;
    const queries = ['cpu.utilization', 'memory.utilization'].map(query => {
      const uuid = uuidV4();
      metrics.push({
        uuid,
        query,
      });
      return {
        aggregator: 'avg',
        id: uuid,
        labels: [
          {
            type: 'EQUAL',
            name: '__name__',
            value: `node.${query}`,
          },
          {
            type: 'EQUAL',
            name: 'kind',
            value: 'Node',
          },
          {
            type: 'EQUAL',
            name: NAME,
            value: name,
          },
        ],
      } as MetricQueries;
    });
    this.metricService
      .queryMetric(this.clusterName, {
        time: Math.floor(Date.now() / 1000),
        queries,
      })
      .then((results: Metric[]) => {
        const resultMap: StringMap = {};
        results.forEach(result => {
          const ratioIndex = metrics.findIndex(
            ratio => ratio.uuid === result.metric.__query_id__,
          );
          const metric = metrics[ratioIndex];
          const value = Number(result.value[1]) || 0;
          if (metric.query === 'cpu.utilization') {
            resultMap.cpu_usage_rate = (value * 100).toFixed(2);
            resultMap.cpu_usage = (
              value * formatNumUnit(node.status.allocatable.cpu)
            ).toFixed(2);
            resultMap.cpu_total = formatNumUnit(
              node.status.allocatable.cpu,
            ).toFixed(0);
          } else if (metric.query === 'memory.utilization') {
            resultMap.memory_usage_rate = (value * 100).toFixed(2);
            resultMap.memory_usage = (value * +nodeMemory).toFixed(2);
            resultMap.memory_total = String(nodeMemory);
          }
        });
        if (!this.nodeCpuMemMap[nodeName]) {
          this.nodeCpuMemMap[nodeName] = {};
        }
        this.nodeCpuMemMap[nodeName] = resultMap;
      });
  }

  private queryMetricServer(node: NodeExtended) {
    const hostname = this.getHostname(node);
    const nodeName = this.k8sUtil.getName(node);
    const nodeCPU = this.formatCPU(node.status.allocatable.cpu);
    const nodeMemory = this.formatMemory(node.status.allocatable.memory);
    this.k8sApi
      .getResource<NodeMetrics>({
        type: RESOURCE_TYPES.NODE_METRICS,
        cluster: this.clusterName,
        name: hostname,
      })
      .subscribe(metrics => {
        if (!this.nodeCpuMemMap[nodeName]) {
          this.nodeCpuMemMap[nodeName] = {};
        }
        this.nodeCpuMemMap[nodeName] = {
          cpu_usage_rate: (
            (+metrics.usage.cpu / 1000 ** 3 / +nodeCPU) *
            100
          ).toFixed(2),
          cpu_usage: (+metrics.usage.cpu / 1000 ** 3).toFixed(2),
          cpu_total: String(nodeCPU),
          memory_usage_rate: (
            (+formatCommonUnit(metrics.usage.memory) / +nodeMemory) *
            100
          ).toFixed(2),
          memory_usage: (+formatCommonUnit(metrics.usage.memory)).toFixed(2),
          memory_total: String(nodeMemory),
        };
      });
  }

  getPrivateIp(node: Node) {
    return node.status.addresses?.find(addr => addr.type === 'InternalIP')
      .address;
  }

  getHostname(node: Node) {
    return node.status.addresses?.find(addr => addr.type === 'Hostname')
      .address;
  }

  getNodeType(node: Node) {
    return node.metadata.labels ? MASTERLABEL in node.metadata.labels : false;
  }

  formatMemory(memory: string) {
    return formatCommonUnit(memory, true);
  }

  transformTaint(taints: NodeTaint[]) {
    return taints?.map(
      taint => `${taint.key}=${taint.value || ''}: ${taint.effect}`,
    );
  }

  isNodeAbnormal(status: NodeStatusEnum) {
    return status === NodeStatusEnum.abnormal;
  }
}
