import {
  K8sApiService,
  KubernetesResource,
  publishRef,
} from '@alauda/common-snippet';
import { DialogRef, DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  TemplateRef,
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { get } from 'lodash-es';
import { Observable } from 'rxjs';
import { map, pluck, tap } from 'rxjs/operators';

import { ConfirmDeleteComponent } from 'app/shared/components/confirm-delete/confirm-delete.component';
import { ClusterFeature } from 'app/typings';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';

import { getProductHref } from '../../utils';
import { DeployProcessComponent } from '../components/deploy-process/component';
import { CodingResource, Condition } from '../types';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CodingDetailComponent implements OnInit {
  isShow: boolean;
  // 默认coding项目名称
  readonly codingName = 'coding-platform';
  context = this;
  clusterName: string;
  getProductHref = getProductHref;

  coding$ = this.k8sApi
    .watchGlobalResource<CodingResource>({
      type: RESOURCE_TYPES.CODING,
      name: this.codingName,
    })
    .pipe(publishRef());

  crd$ = this.k8sApi
    .getGlobalResource<KubernetesResource>({
      type: RESOURCE_TYPES.CUSTOM_RESOURCE_DEFINITION,
      name: 'codings.product.alauda.io',
    })
    .pipe(publishRef());

  metricDocked$: Observable<boolean>;

  cluster$: Observable<KubernetesResource>;

  deleteDialogRef: DialogRef<ConfirmDeleteComponent>;

  isDelete: boolean;

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly dialogService: DialogService,
    private readonly router: Router,
    private readonly sanitizer: DomSanitizer,
  ) {}

  ngOnInit(): void {
    this.getCoding();
  }

  getCoding() {
    this.coding$.subscribe((coding: CodingResource) => {
      const phase = get(coding, 'status.phase');
      this.isShow = phase && phase !== 'Deleting' && phase !== 'Pending';
      this.isDelete = phase === 'Pending' || phase === 'Provisioning';
      if (!this.clusterName) {
        const clusterName = get(coding, 'spec.clusterName');
        this.getCluster(clusterName);
        this.getMetricDocked(clusterName);
        this.cdr.markForCheck();
      }
    });
  }

  getMetricDocked(clusterName: string) {
    this.metricDocked$ = this.k8sApi
      .getResourceList<ClusterFeature>({
        type: RESOURCE_TYPES.FEATURE,
        cluster: clusterName,
        queryParams: {
          labelSelector: 'instanceType=prometheus',
        },
      })
      .pipe(
        pluck('items'),
        map(nodes => {
          return nodes.length > 0;
        }),
        publishRef(),
      );
  }

  getCluster(clusterName: string) {
    this.cluster$ = this.k8sApi
      .getGlobalResource<KubernetesResource>({
        type: RESOURCE_TYPES.CLUSTER_REGISTRY,
        name: clusterName,
        namespaced: true,
      })
      .pipe(
        publishRef(),
        tap(() => {
          this.clusterName = clusterName;
        }),
      );
  }

  getDeployStatus(coding: CodingResource) {
    const phase = get(coding, 'status.phase');
    switch (phase) {
      case 'Running':
        return 'success';
      case 'Failed':
        return 'error';
      case 'Pending':
        return 'unknown';
      case 'Provisioning':
        return 'pending';
      case 'Deleting':
        return 'deleting';
      default:
        return 'unknown';
    }
  }

  getRunningStatus(coding: CodingResource) {
    const { phase, conditions } = get(coding, 'status', {
      phase: '',
      conditions: [],
    });
    if (phase === 'Running') {
      const readyCondition = conditions.find(
        condition => condition.type === 'Ready',
      );
      return readyCondition.status.toLowerCase() === 'true'
        ? 'success'
        : 'error';
    }
    return '';
  }

  getClusterStatus(cluster: KubernetesResource) {
    if (cluster && cluster.status) {
      const conditions: Condition[] = get(cluster, 'status.conditions');
      const clusterStatus = conditions.find(
        (condition: Condition) => condition.type === 'NotAccessible',
      );
      if (clusterStatus) {
        return clusterStatus.status.toLowerCase() === 'true'
          ? 'error'
          : 'success';
      }
    }
    return 'unknown';
  }

  deployProcess() {
    this.dialogService.open(DeployProcessComponent, {
      size: DialogSize.Large,
      data: {
        isDetail: true,
      },
    });
  }

  deleteDialog(templateRef: TemplateRef<ConfirmDeleteComponent>) {
    this.deleteDialogRef = this.dialogService.open(templateRef);
  }

  deleteCoding() {
    return this.k8sApi
      .deleteGlobalResource({
        type: RESOURCE_TYPES.CODING,
        resource: {
          apiVersion: 'product.alauda.io/v1',
          kind: 'Coding',
          metadata: {
            name: this.codingName,
          },
        },
      })
      .pipe(
        tap(() => {
          this.router.navigate(['/manage-platform/product']);
        }),
      );
  }

  getLogo = (url: string) => {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  };
}
