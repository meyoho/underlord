import { TranslateService } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { get } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { toUnitGi } from 'app/utils';

import { Address, KubernetesResourceNode, ValidatorType } from '../../types';

@Component({
  selector: 'alu-node-info',
  templateUrl: './template.html',
  styleUrls: ['./component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NodeInfoComponent extends BaseResourceFormGroupComponent {
  @Input()
  nodes: KubernetesResourceNode[] = [];

  @Input()
  disabled = false;

  formGroup: FormGroup;
  nodeNum = [0, 1, 2];

  constructor(
    public injector: Injector,
    private readonly translate: TranslateService,
  ) {
    super(injector);
  }

  /**
   * 校验是否与其他业务组件节点重复
   * @param contrlIndex
   */
  nodeRepeatValidator(contrlIndex: number): ValidatorFn {
    return (contrl: AbstractControl): ValidatorType | null => {
      let errMsg = null;
      if (contrl.parent && contrl.parent.value) {
        const value = contrl.parent.value;
        Object.keys(value).forEach(key => {
          if (parseInt(key) !== contrlIndex && value[key] === contrl.value) {
            errMsg = { nodeRepeat: { value: contrl.value } };
            return false;
          }
        });
      }
      return errMsg;
    };
  }

  /**
   * 校验资源是否正确
   */
  resourcesValidator(): ValidatorFn {
    return (contrl: AbstractControl): ValidatorType | null => {
      let errMsg = null;
      this.nodes.forEach(node => {
        const cpu = get(node, 'status.capacity.cpu');
        const memory = get(node, 'status.capacity.memory') + '';
        if (contrl.value === node.metadata.name && cpu && memory) {
          errMsg = this.getMsgStr(contrl, cpu, memory);
          if (errMsg) {
            return false;
          }
        }
      });
      return errMsg;
    };
  }

  getMsgStr(
    contrl: AbstractControl,
    cpu: string,
    memory: string,
  ): ValidatorType | null {
    if (!parseInt(cpu) || parseInt(cpu) < 8) {
      return { nodeResources: { value: contrl.value } };
    }
    const memoryNum = this.getMemoryNum(memory);
    if (!memoryNum || memoryNum < 16) {
      return { nodeResources: { value: contrl.value } };
    }
    return null;
  }

  getMemoryNum(memory: string) {
    const memoryNum = parseFloat(toUnitGi(memory));
    return memoryNum !== undefined ? Math.ceil(memoryNum) : undefined;
  }

  createForm() {
    const group: { [key: number]: FormControl } = {};

    this.nodeNum.forEach((i: number) => {
      group[i] = this.fb.control('', [
        Validators.required,
        this.nodeRepeatValidator(i),
        this.resourcesValidator(),
      ]);
    });
    return this.fb.group(group);
  }

  getDefaultFormModel() {
    return {};
  }

  getNodeIP(node: KubernetesResourceNode) {
    const addresses: Address[] = get(node, 'status.addresses');
    let nodeIP: string;
    if (addresses) {
      addresses.forEach(address => {
        if (address.type === 'InternalIP') {
          nodeIP = address.address;
          return false;
        }
      });
    }
    return nodeIP;
  }

  getLabel = (node: KubernetesResourceNode) => {
    return (
      node.metadata.name +
      ' (' +
      this.translate.get('coding_node_select_options', {
        type: this.getNodeType(node),
        nodeIP: this.getNodeIP(node),
        nodeCPU: get(node, 'status.capacity.cpu'),
        nodeMemory: this.getMemoryNum(get(node, 'status.capacity.memory')),
      }) +
      ')'
    );
  };

  getNodeType(node: KubernetesResourceNode) {
    let type = '';
    if ('node-role.kubernetes.io/master' in node.metadata.labels) {
      type = 'coding_node_type_control';
    } else if ('node-role.kubernetes.io/node' in node.metadata.labels) {
      type = 'coding_node_type_computing';
    }
    return this.translate.get(type);
  }
}
