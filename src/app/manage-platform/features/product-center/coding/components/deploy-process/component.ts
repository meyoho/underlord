import {
  K8sApiService,
  publishRef,
  readonlyOptions,
  viewActions,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { get } from 'lodash-es';
import { map } from 'rxjs/operators';

import { StepProcessItem } from 'app/shared/components/steps-process/steps-process.component';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';

import { CodingResource, ModulesStatus } from '../../types';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeployProcessComponent {
  // 默认coding项目名称
  private readonly codingName = 'coding-platform';
  readonly codeEditorOptions = {
    ...readonlyOptions,
    language: 'log',
  };

  readonly editorActions = {
    ...viewActions,
    diffMode: false,
    copy: false,
    export: false,
    import: false,
  };

  version: string;
  deployLoading = true;
  stepIndex = 0;
  stepInfo: ModulesStatus;
  codingSteps$ = this.k8sApi
    .watchGlobalResource<CodingResource>({
      type: RESOURCE_TYPES.CODING,
      name: this.codingName,
    })
    .pipe(
      map(coding => {
        this.version = get(coding, 'spec.version');
        const steps: StepProcessItem[] = [];
        const modulesStatus = get(
          coding,
          'status.modulesStatus',
        ) as ModulesStatus[];
        if (modulesStatus) {
          this.logContext = '';
          this.stepIndex = 0;
          modulesStatus.forEach(item => {
            if (item.status !== 'Pending') {
              this.stepIndex++;
              this.stepInfo = item;
              if (item.logs) {
                item.logs.forEach(log => {
                  this.logContext += log.message + '\n';
                });
              }
              this.deployLoading =
                item.status !== 'Running' && item.status !== 'Failed';
            }
            steps.push({
              label: item.displayName,
              type: this.getStepState(item.status),
            });
          });
        }
        console.log(modulesStatus);
        return steps;
      }),
      publishRef(),
    );

  logContext = '';

  constructor(
    @Inject(DIALOG_DATA)
    public readonly data: {
      isDetail?: boolean;
    },
    public dialogRef: DialogRef<DeployProcessComponent>,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly router: Router,
  ) {}

  getStepState(status: string) {
    switch (status) {
      case 'Running':
        return 'success';
      case 'Failed':
        return 'error';
      case 'Deleting':
        return 'info';
      case 'Provisioning':
        return 'info';
      case 'Pending':
        return 'info';
      default:
        return 'info';
    }
  }

  getDeployStatus(phase: string) {
    switch (phase) {
      case 'Running':
        return 'success';
      case 'Failed':
        return 'error';
      case 'Pending':
        return 'unknown';
      case 'Provisioning':
        return 'pending';
      case 'Deleting':
        return 'deleting';
      default:
        return 'unknown';
    }
  }

  close() {
    this.dialogRef.close();
    if (!this.data.isDetail) {
      this.router.navigate([
        `/manage-platform/product/coding/detail/${this.version}`,
      ]);
    }
  }
}
