import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DeployComponent } from './deploy/component';
import { CodingDetailComponent } from './detail/component';

const routes: Routes = [
  {
    path: 'deploy/:version',
    component: DeployComponent,
  },
  {
    path: 'detail/:name',
    component: CodingDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CodingRoutingModule {}
