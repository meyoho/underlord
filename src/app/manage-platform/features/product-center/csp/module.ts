import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';

import { CspDeployComponent } from './deploy.component';
import { CspDetailComponent } from './detail.component';

export const EXPORTABLE_COMPONENTS = [CspDeployComponent, CspDetailComponent];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: 'deploy/:version',
        component: CspDeployComponent,
      },
      {
        path: 'detail/:crdName',
        component: CspDetailComponent,
      },
    ]),
  ],
  declarations: EXPORTABLE_COMPONENTS,
  exports: EXPORTABLE_COMPONENTS,
})
export class CspModule {}
