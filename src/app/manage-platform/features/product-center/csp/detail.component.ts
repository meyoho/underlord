import {
  API_GATEWAY,
  AsyncDataLoader,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  TRUE,
  isAllowed,
  readonlyOptions,
  skipError,
  viewActions,
} from '@alauda/common-snippet';
import { DialogRef, DialogService, DialogSize } from '@alauda/ui';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import {
  distinctUntilChanged,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { ConfirmDeleteComponent } from 'app/shared/components/confirm-delete/confirm-delete.component';
import { Csp } from 'app/typings';
import { RESOURCE_TYPES, b64DecodeUnicode } from 'app/utils';

import { mapStatus } from '../utils';

export const TIME_LINE_REG = /^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d+Z) (.*)/;

@Component({
  templateUrl: 'detail.component.html',
  styleUrls: ['detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CspDetailComponent implements OnDestroy {
  dataLoader = new AsyncDataLoader<Csp>({
    fetcher: this.fetchCsp.bind(this),
  });

  deletePermission$ = this.dataLoader.data$.pipe(
    switchMap(() =>
      this.k8sPermission.getAccess({
        type: RESOURCE_TYPES.CSP,
        action: K8sResourceAction.DELETE,
        name: 'csp',
      }),
    ),
    isAllowed(),
  );

  lastLogTime: string;
  fetchLogTimeoutId: number;

  destroy$$ = new Subject<void>();

  logs$$ = new BehaviorSubject<string>(null);

  logs$ = this.logs$$.pipe(distinctUntilChanged());

  codeEditorOptions = {
    ...readonlyOptions,
    language: 'log',
  };

  editorActions = viewActions;

  mapStatus = mapStatus;

  b64DecodeUnicode = b64DecodeUnicode;

  @ViewChild('confirmUninstallCsp')
  confirmUninstallCsp: TemplateRef<ConfirmDeleteComponent>;

  confirmUninstallCspRef: DialogRef<ConfirmDeleteComponent>;

  constructor(
    private readonly location: Location,
    private readonly http: HttpClient,
    private readonly dialog: DialogService,
    private readonly k8sApi: K8sApiService,
    private readonly k8sPermission: K8sPermissionService,
  ) {}

  ngOnDestroy() {
    this.destroy$$.next();
    this.destroy$$.complete();
  }

  fetchCsp() {
    return this.k8sApi.watchGlobalResource<Csp>({
      type: RESOURCE_TYPES.CSP,
      name: 'csp',
    });
  }

  onConfirmDelete() {
    this.confirmUninstallCspRef = this.dialog.open(this.confirmUninstallCsp);
  }

  deleteCsp() {
    return this.k8sApi
      .deleteGlobalResource({
        type: RESOURCE_TYPES.CSP,
        resource: this.dataLoader.snapshot.data,
      })
      .pipe(tap(() => this.location.back()));
  }

  fetchLogs(csp: Csp) {
    this.http
      .get(
        `${API_GATEWAY}/kubernetes/global/api/v1/namespaces/${csp.status.installerNamespaceName}/log`,
        {
          responseType: 'text',
          params: {
            container: 'csp',
            tailLines: '1000',
            timestamps: TRUE,
            ...(this.lastLogTime && {
              sinceTime: this.lastLogTime,
            }),
          },
        },
      )
      .pipe(skipError(''), takeUntil(this.destroy$$))
      .subscribe((rawLog: string) => {
        if (rawLog) {
          let logs = this.logs$$.value;
          const lines = rawLog.split(/\r?\n/);
          for (const line of lines) {
            const matched = TIME_LINE_REG.exec(line);
            if (!matched) {
              continue;
            }
            const log = matched[2];
            if ((!this.lastLogTime || matched[1] > this.lastLogTime) && log) {
              this.lastLogTime = matched[1];
              logs += (logs ? '\n' : '') + log;
            }
          }
          this.logs$$.next(logs);
        }
        this.fetchLogTimeoutId = window.setTimeout(
          () => this.fetchLogs(csp),
          15 * 1000,
        );
      });
  }

  onShowLogs(csp: Csp, templateRef: TemplateRef<any>) {
    if (!csp.status.installerNamespaceName) {
      return;
    }
    this.lastLogTime = null;
    this.logs$$.next('');
    this.fetchLogs(csp);
    this.dialog
      .open(templateRef, {
        size: DialogSize.Big,
      })
      .afterClosed()
      .subscribe(() => {
        clearTimeout(this.fetchLogTimeoutId);
      });
  }
}
