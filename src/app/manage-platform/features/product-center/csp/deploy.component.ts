import {
  DESCRIPTION,
  DISPLAY_NAME,
  K8sApiService,
  K8sUtilService,
  TRUE,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { FormBuilder, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Csp } from 'app/typings';
import {
  IP_ADDRESS_PATTERN,
  RESOURCE_TYPES,
  ResourceType,
  getYamlApiVersion,
} from 'app/utils';

const TOGGLE_FIELDS = ['vip', 'slaveIp', 'slavePort', 'slaveRootPwd'];

export interface CspFormModel {
  hostIp: string;
  hostPort: number;
  hostRootPwd: string;
  highAvailable: boolean;
  vip: string;
  slaveIp: string;
  slavePort: number;
  slaveRootPwd: string;
}

@Component({
  templateUrl: 'deploy.component.html',
  styleUrls: ['deploy.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CspDeployComponent implements OnInit, OnDestroy {
  destroy$$ = new Subject<void>();

  form = this.fb.group({
    hostIp: '',
    hostPort: '',
    hostRootPwd: '',
    highAvailable: false,
    vip: '',
    slaveIp: '',
    slavePort: '',
    slaveRootPwd: '',
  });

  hostRootPwdVisible = false;
  slaveRootPwdVisible = false;

  IP_ADDRESS_PATTERN = IP_ADDRESS_PATTERN;

  constructor(
    private readonly location: Location,
    private readonly fb: FormBuilder,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly dialog: DialogService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    private readonly translate: TranslateService,
  ) {}

  ngOnInit() {
    this.form
      .get('highAvailable')
      .valueChanges.pipe(takeUntil(this.destroy$$))
      .subscribe(highAvailable =>
        TOGGLE_FIELDS.forEach(field =>
          this.form.get(field)[highAvailable ? 'enable' : 'disable'](),
        ),
      );
  }

  ngOnDestroy() {
    this.destroy$$.next();
    this.destroy$$.complete();
  }

  onCancel() {
    this.location.back();
  }

  async onSubmit(ngForm: NgForm) {
    if (ngForm.invalid) {
      return;
    }

    try {
      await this.dialog.confirm({
        title: this.translate.get('confirm_deploy_product', {
          name: 'CSP',
        }),
        content: this.translate.get('confirm_deploy_product_tips'),
        confirmText: this.translate.get('continue_to_deploy'),
        cancelText: this.translate.get('cancel'),
      });
    } catch {
      return;
    }

    const value: CspFormModel = ngForm.value;

    value.hostRootPwd = btoa(value.hostRootPwd);

    if (value.slaveRootPwd) {
      value.slaveRootPwd = btoa(value.slaveRootPwd);
    }

    const version = this.route.snapshot.paramMap.get('version');

    const csp: Csp = {
      kind: 'Csp',
      apiVersion: getYamlApiVersion(RESOURCE_TYPES.CSP),
      metadata: {
        name: 'csp',
        labels: {
          [this.k8sUtil.normalizeType('available-version')]: version,
          [this.k8sUtil.normalizeType('product')]: 'csp',
          [this.k8sUtil.normalizeType('installable')]: TRUE,
        },
        annotations: {
          [this.k8sUtil.normalizeType(DISPLAY_NAME)]: 'csp',
          [this.k8sUtil.normalizeType(DESCRIPTION)]: 'csp',
        },
      },
      spec: {
        productName: 'csp',
        productType: 'middleware',
        version,
        highAvailable: value.highAvailable,
        machines: [
          {
            type: 'master',
            ip: value.hostIp,
            port: value.hostPort,
            password: value.hostRootPwd,
          },
        ],
      },
    };

    if (value.highAvailable) {
      csp.spec.machines.push({
        type: 'slave',
        ip: value.slaveIp,
        port: value.slavePort,
        password: value.slaveRootPwd,
      });
      csp.spec.vip = value.vip;
    }

    this.k8sApi
      .postGlobalResource<Csp>({
        type: RESOURCE_TYPES.CSP,
        resource: csp,
      })
      .subscribe(() => {
        this.router.navigate(['../../detail/csps.product.alauda.io'], {
          relativeTo: this.route,
        });
      });
  }
}
