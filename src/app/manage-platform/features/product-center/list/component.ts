import {
  COMMON_WRITABLE_ACTIONS,
  FeatureGateService,
  K8sPermissionService,
  K8sUtilService,
  Reason,
  TranslateService,
  noop,
  publishRef,
} from '@alauda/common-snippet';
import { DialogRef, DialogService } from '@alauda/ui';
import { ChangeDetectorRef, Component, TemplateRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { get } from 'lodash-es';
import { Subject, combineLatest, interval } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';

import {
  ProductCenterListApi,
  handleDeployStatus,
  handleOperatingStatus,
  handleStatus,
} from 'app/api/product-center/list-api';
import { ProductStatusMap } from 'app/manage-platform/features/product-center/constants';
import {
  ConfirmDeleteComponent,
  DeleteType,
} from 'app/shared/components/confirm-delete/confirm-delete.component';
import { ProductCRD, ProductCenter } from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';
enum JumpType {
  DETAIL = 'detail',
  DEPLOY = 'deploy',
  UPGRADE = 'upgrade',
}
const INSTALLWITHSETTINGS = 'install-with-settings';
const UPGRADEWITSETTING = 'upgrade-with-settings';
const OPSENTRYPOINT = 'ops-entrypoint';
@Component({
  selector: 'alu-product-list',
  styleUrls: ['./styles.scss'],
  templateUrl: './template.html',
})
export class ProductListComponent {
  columns = [
    'name',
    'deploy_status',
    'operating_status',
    'version',
    'type',
    'action',
  ];

  pullInterval = 30 * 1000;
  keyword = '';
  displayAllProduct = true;
  displayDeployedProduct = false;
  reason = Reason;
  resource: ProductCenter;
  deleteDialogRef: DialogRef<ConfirmDeleteComponent>;
  type = DeleteType;
  permissions$ = this.k8sPermissionService.isAllowed({
    type: RESOURCE_TYPES.PORTAL,
    action: COMMON_WRITABLE_ACTIONS,
  });

  featGuardStatus$ = this.fg.isEnabled('ace3');

  private readonly reloadAction$$ = new Subject();
  list$ = combineLatest([
    interval(this.pullInterval).pipe(startWith(0)),
    this.reloadAction$$.pipe(startWith(null as void)),
  ]).pipe(
    switchMap(() => this.getProducts()),
    publishRef(),
  );

  constructor(
    private readonly api: ProductCenterListApi,
    private readonly k8sUtil: K8sUtilService,
    private readonly cdr: ChangeDetectorRef,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly sanitizer: DomSanitizer,
    private readonly k8sPermissionService: K8sPermissionService,
    private readonly fg: FeatureGateService,
  ) {}

  getProducts() {
    return this.api.getProducts().pipe(map(res => res.items));
  }

  checkDisplayProduct(displayAll: boolean) {
    this.displayAllProduct = displayAll;
    this.displayDeployedProduct = !displayAll;
    this.cdr.markForCheck();
  }

  reload() {
    this.reloadAction$$.next();
  }

  keywordChanged(keyword: string) {
    this.keyword = keyword;
  }

  displayList(items: ProductCenter[]) {
    const allProducts = (items || []).filter(
      item =>
        (this.k8sUtil.getLabel(item.crd, 'installable') || '').toLowerCase() ===
          'true' &&
        (this.k8sUtil.getLabel(item.crd, 'isdisplay') || '').toLowerCase() !==
          'false',
    );
    const products = this.displayAllProduct
      ? allProducts
      : allProducts.filter(item => this.checkCr(item));
    return this.filterByName(products || []);
  }

  filterByName(items: ProductCenter[]) {
    const locale = this.translate.locale;
    return items.filter(item => {
      const data = this.api.getResourcesTranslate(item.crd, 'displayName');
      const value = get(data, locale, data);
      if (!value.includes) {
        return '';
      }
      return value.includes(this.keyword.trim());
    });
  }

  getBase64CrdName = (item: ProductCenter) => {
    return btoa(this.k8sUtil.getName(item.crd));
  };

  getCr = (item: ProductCenter) => {
    return item.crs[0];
  };

  getAvailableVersion = (crd: ProductCRD) =>
    this.k8sUtil.getLabel(crd, 'available-version');

  getVersion = (item: ProductCenter) => {
    return this.checkCr(item) && get(this.getCr(item), 'status.version')
      ? get(this.getCr(item), 'status.version')
      : this.getAvailableVersion(item.crd);
  };

  getProductType = (item: ProductCenter) => {
    return this.k8sUtil.getLabel(item.crd, 'product-type').toLowerCase();
  };

  getLogo = (item: ProductCenter) => {
    const url = this.k8sUtil.getAnnotation(item.crd, 'product-logo');
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  };

  getStatusType = (item: ProductCenter) => {
    return handleStatus(item).type;
  };

  getDeployStatus = (item: ProductCenter) => {
    return handleDeployStatus(item);
  };

  geOperatingStatus = (item: ProductCenter) => {
    return handleOperatingStatus(item);
  };

  getKind = (item: ProductCenter) => {
    return item.crd.spec.names.kind.toLowerCase();
  };

  checkCr(item: ProductCenter) {
    return (get(item, 'crs') || []).length;
  }

  canVisitDetail = (item: ProductCenter) => {
    return this.getStatusType(item) !== ProductStatusMap.NotDeployed.type;
  };

  isInstallable = (item: ProductCenter) => {
    return this.getStatusType(item) === ProductStatusMap.NotDeployed.type;
  };

  handleUpgradeable = (item: ProductCenter) => {
    switch (this.getStatusType(item)) {
      case ProductStatusMap.InDeployment.type:
        return { disabled: true, tooltip: 'disabled_deploy_tip' };
      case ProductStatusMap.Running.type:
      case ProductStatusMap.DeploymentSucceed.type:
        return this.deployStatus(item);
      case ProductStatusMap.RunningAbnormal.type:
        return this.noNewVersion(item)
          ? { disabled: true, tooltip: 'no_new_version_not_executable' }
          : { disabled: true, tooltip: 'disabled_running_abnormal_tip' };
      case ProductStatusMap.DeploymentFailure.type:
        return this.deployStatus(item);
      case ProductStatusMap.Deleting.type:
        return { disabled: true, tooltip: 'disabled_uninstall_tip' };
      case ProductStatusMap.DeleteFailure.type:
        return { disabled: true, tooltip: 'disabled_uninstall_failure_tip' };
      case ProductStatusMap.SystemError.type:
        return { disabled: true, tooltip: 'disabled_system_error_tip' };
      default:
        break;
    }
  };

  deployStatus(item: ProductCenter) {
    return this.noNewVersion(item)
      ? { disabled: true, tooltip: 'no_new_version_not_executable' }
      : { disabled: false, tooltip: null };
  }

  noNewVersion(item: ProductCenter) {
    return this.getAvailableVersion(item.crd) === this.getCr(item).spec.version;
  }

  isDeletable = (item: ProductCenter) => {
    return this.checkCr(item) &&
      ![
        ProductStatusMap.Deleting.type,
        ProductStatusMap.InDeployment.type,
      ].includes(this.getStatusType(item))
      ? get(this.getCr(item), 'status.deletable', true)
      : false;
  };

  disabledDeleteTip = (item: ProductCenter) => {
    switch (this.getStatusType(item)) {
      case ProductStatusMap.InDeployment.type:
        return 'disabled_deploy_tip';
      case ProductStatusMap.Deleting.type:
        return 'disabled_uninstall_tip';
      default:
        break;
    }
  };

  deployProduct(item: ProductCenter) {
    if (this.isDefaultDeploy(item)) {
      this.internalJump(item, JumpType.DEPLOY);
    } else if (this.isExternalDeploy(item)) {
      this.productProductJump(item, JumpType.DEPLOY);
    } else {
      this.dialog
        .confirm({
          title: this.translate.get('confirm_deployment_title', {
            name: this.api.getResourcesTranslate(item.crd, 'displayName'),
            version: this.getAvailableVersion(item.crd),
          }),
          confirmText: this.translate.get('deployments'),
          cancelText: this.translate.get('cancel'),
          beforeConfirm: (resolve, reject) => {
            this.api.postProduct(item.crd).subscribe(() => {
              resolve();
            }, reject);
          },
        })
        .then(() => {
          this.internalJump(item, JumpType.DETAIL);
        })
        .catch(noop);
    }
  }

  isDefaultDeploy(item: ProductCenter) {
    return this.k8sUtil.getLabel(item.crd, INSTALLWITHSETTINGS) === undefined;
  }

  isExternalDeploy(item: ProductCenter) {
    return (
      (
        this.k8sUtil.getLabel(item.crd, INSTALLWITHSETTINGS) || 'false'
      ).toLowerCase() === 'true'
    );
  }

  isDefaultUpgrade(item: ProductCenter) {
    return this.k8sUtil.getLabel(item.crd, UPGRADEWITSETTING) === undefined;
  }

  isExternalUpgrade(item: ProductCenter) {
    return (
      (
        this.k8sUtil.getLabel(item.crd, UPGRADEWITSETTING) || 'false'
      ).toLowerCase() === 'true'
    );
  }

  upgradeProduct(item: ProductCenter) {
    if (this.isDefaultUpgrade(item)) {
      this.internalJump(item, JumpType.UPGRADE);
    } else if (this.isExternalUpgrade(item)) {
      this.productProductJump(item, JumpType.UPGRADE);
    } else {
      this.dialog
        .confirm({
          title: this.translate.get('upgrade_confirm', {
            name: this.api.getResourcesTranslate(item.crd, 'displayName'),
            version: this.getAvailableVersion(item.crd),
          }),
          confirmText: this.translate.get('upgrade'),
          cancelText: this.translate.get('cancel'),
          beforeConfirm: (resolve, reject) => {
            const cr = this.getCr(item);
            cr.spec.version = this.getAvailableVersion(item.crd);
            cr.status.phase = 'Provisioning';
            this.api.upgradeProduct(item.crd, cr).subscribe(() => {
              resolve();
            }, reject);
          },
        })
        .then(() => {
          this.internalJump(item, JumpType.DETAIL);
        })
        .catch(noop);
    }
  }

  deleteProduct(
    templateRef: TemplateRef<ConfirmDeleteComponent>,
    resource: ProductCenter,
  ) {
    this.resource = resource;
    this.deleteDialogRef = this.dialog.open(templateRef);
  }

  deleteProductApi = () => {
    return this.api.deleteProduct(this.resource.crd, this.getCr(this.resource));
  };

  closeDeleteProductDialog(ev: boolean) {
    if (ev) {
      this.reload();
    }
    this.deleteDialogRef.close();
  }

  disallowAccess = (item: ProductCenter) => {
    return [
      ProductStatusMap.SystemError.type,
      ProductStatusMap.Deleting.type,
    ].includes(this.getStatusType(item));
  };

  productProductJump(item: ProductCenter, type: JumpType) {
    const href = this.k8sUtil.getAnnotation(item.crd, OPSENTRYPOINT);
    const params = this.getJumpParams(item, type);
    const paramsUrl = `${type}/${params}`;
    try {
      const path = new URL(href);
      if (path.origin !== window.location.origin) {
        return window.open(
          `${href}/${paramsUrl}?id_token=${window.localStorage.getItem(
            'id_token',
          )}`,
        );
      }
      return href;
    } catch (error) {
      const urlHost = `${window.location.protocol}//${window.location.host}`;
      if (!href) {
        return window.open(
          `${urlHost}/${this.getProductRouteName(item)}/${paramsUrl}`,
        );
      }

      return window.open(`${urlHost}/${href}/${paramsUrl}`);
    }
  }

  internalJump(item: ProductCenter, type: JumpType) {
    const params = this.getJumpParams(item, type);
    return this.router.navigate(['../', this.getKind(item), type, params], {
      relativeTo: this.route,
    });
  }

  getProductRouteName(item: ProductCenter) {
    const group = item.crd.spec.group;
    const name = this.k8sUtil.getName(item.crd).replace(`.${group}`, '');
    return `product-${name}`;
  }

  getJumpParams(item: ProductCenter, type: JumpType) {
    if (type === JumpType.DEPLOY) {
      return this.getAvailableVersion(item.crd);
    }
    return this.k8sUtil.getName(item.crd);
  }
}
