import { get } from 'lodash-es';

import { ProductCR } from 'app/typings';

import { ProductStatusMap } from './constants';

export function getProductHref(href: string) {
  if (!href) return '';
  href = href.trim();
  return href.startsWith('http://') || href.startsWith('https://')
    ? href
    : `${window.location.origin}${href}${
        href.includes('?') ? '&' : '?'
      }id_token=${localStorage.getItem('id_token')}`;
}

export function mapStatus(cr: ProductCR) {
  const { phase, conditions } = get(cr, 'status', {
    phase: '',
    conditions: [],
  });

  const deletionTimestamp = cr.metadata.deletionTimestamp;
  if (deletionTimestamp) {
    return phase === 'Failed' &&
      conditions
        .find(condition => condition.type === 'Initialized')
        .status.toLocaleLowerCase() === 'true'
      ? ProductStatusMap.DeleteFailure
      : ProductStatusMap.Deleting;
  } else {
    switch (phase) {
      case 'Running': {
        const readyCondition = conditions.find(
          condition => condition.type === 'Ready',
        );
        return readyCondition.status.toLowerCase() === 'true'
          ? ProductStatusMap.Running
          : ProductStatusMap.RunningAbnormal;
      }
      case 'Failed': {
        const initializedCondition = conditions.find(
          condition => condition.type === 'Initialized',
        );
        return initializedCondition.status.toLowerCase() === 'true'
          ? ProductStatusMap.DeleteFailure
          : ProductStatusMap.DeploymentFailure;
      }
      case 'Deleting':
        return ProductStatusMap.Deleting;
      case 'Provisioning':
      default:
        return ProductStatusMap.InDeployment;
    }
  }
}

function getConditions(cr: ProductCR) {
  const { phase, conditions } = get(cr, 'status', {
    phase: '',
    conditions: [],
  });
  const deletionTimestamp = cr?.metadata?.deletionTimestamp;
  return { phase, conditions, deletionTimestamp };
}

export function mapDeployStatus(cr: ProductCR) {
  const { phase, conditions, deletionTimestamp } = getConditions(cr);
  if (deletionTimestamp) {
    return phase === 'Failed' &&
      conditions
        .find(condition => condition.type === 'Initialized')
        .status.toLocaleLowerCase() === 'true'
      ? ProductStatusMap.DeleteFailure
      : ProductStatusMap.Deleting;
  } else {
    switch (phase) {
      case 'Running':
        return ProductStatusMap.DeploymentSucceed;
      case 'Failed': {
        const initializedCondition = conditions.find(
          condition => condition.type === 'Initialized',
        );
        return initializedCondition.status.toLowerCase() === 'true'
          ? ProductStatusMap.DeleteFailure
          : ProductStatusMap.DeploymentFailure;
      }
      case 'Deleting':
        return ProductStatusMap.Deleting;
      case 'Provisioning':
      default:
        return ProductStatusMap.InDeployment;
    }
  }
}

export function mapOperatingStatus(cr: ProductCR) {
  const { phase, conditions } = getConditions(cr);
  if (phase === 'Running') {
    const readyCondition = conditions.find(
      condition => condition.type === 'Ready',
    );
    return readyCondition.status.toLowerCase() === 'true'
      ? ProductStatusMap.Running
      : ProductStatusMap.RunningAbnormal;
  } else {
    return null;
  }
}

// could be 500m, 0.5, 5k, return number
export const formatNumUnit = (value: string) => {
  let num = parseFloat(value);

  if (num === 0) {
    return 0;
  }

  if (!num) {
    return null;
  }

  switch (value[value.length - 1].toLowerCase()) {
    case 'm':
      num = num / 1000;
      break;
    case 'k':
      num = num * 1000;
      break;
  }

  return num;
};

function formatCommonUnit(value: string): string;
function formatCommonUnit(
  value: string,
  // tslint:disable-next-line: bool-param-default
  fixed?: boolean,
  base?: number,
): string | number;
function formatCommonUnit(value = '', fixed = false, base?: number) {
  value = value.replace(/i$/, '');

  let num = parseFloat(value);
  if (!base) {
    base = 1024;
  }

  if (!num) {
    return 0;
  }

  switch (value[value.length - 1]) {
    case 'M':
      num = num / base;
      break;
    case 'Mi':
      num = num / 1024;
      break;
    case 'm':
      num = num / 1000 / 1024 / 1024 / 1024;
      break;
    case 'G':
      break;
    case 'Gi':
      num = (num / 1000) * 1024;
      break;
    case 'K':
      num = num / base / base;
      break;
    case 'Ki':
    case 'k':
      num = num / 1024 / 1024;
      break;
    case 'T':
      num = num * base;
      break;
    case 'Ti':
      num = num * 1024;
      break;
    case 'P':
      num = num * base * base;
      break;
    case 'Pi':
      num = num * 1024 * 1024;
      break;
    case 'E':
      num = num * base * base * base;
      break;
    case 'Ei':
      num = num * 1024 * 1024 * 1024;
      break;
    default:
      num = num / 1024 / 1024 / 1024;
      break;
  }

  if (!fixed) {
    return num;
  }

  return num.toFixed(2).replace(/0+$/, '').replace(/\.$/, '');
}

function formatK8sCommonUnit(value: string): string;
function formatK8sCommonUnit(value: string, fixed: boolean): string | number;
function formatK8sCommonUnit(value: string, fixed = false) {
  return formatCommonUnit(value, fixed, 1000);
}

export { formatCommonUnit, formatK8sCommonUnit };
