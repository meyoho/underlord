import {
  API_GATEWAY,
  K8SResourceList,
  K8sApiService,
  KubernetesResourceList,
  NAME,
  ResourceListParams,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogService, DialogSize, MessageService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Subject } from 'rxjs';
import { map, takeUntil, tap } from 'rxjs/operators';

import { Environments } from 'app/api/envs/types';
import { ManagePlatformComponent } from 'app/manage-platform/component';
import { ENVIRONMENTS } from 'app/services/services.module';
import {
  ClusterAddOn,
  ClusterAddOnStatusColorMapper,
  ClusterAddOnStatusIconMapper,
  ClusterAddOnType,
} from 'app/typings';
import {
  ACTION,
  ClusterAddonTypeMeta,
  RESOURCE_TYPES,
  ResourceType,
  STATUS,
  getClusterAddonStatus,
  getClusterAddonText,
} from 'app/utils';

import { ClusterAddOnDeployComponent } from '../deploy/component';
import { addOnTypeMap } from '../utils';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListPageComponent implements OnInit, OnDestroy {
  constructor(
    private readonly layoutComponent: ManagePlatformComponent,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly http: HttpClient,
    private readonly dialog: DialogService,
    private readonly messageService: MessageService,
    private readonly translate: TranslateService,
    @Inject(ENVIRONMENTS) public env: Environments,
  ) {}

  private readonly onDestroy$ = new Subject<void>();

  clusterName: string;
  clusterAddOnTypes: ClusterAddOnType[];
  clusterAddOnDeployed: ClusterAddOn[];

  getClusterAddonStatus = getClusterAddonStatus;
  getClusterAddonText = getClusterAddonText;
  ClusterAddOnStatusColorMapper = ClusterAddOnStatusColorMapper;
  ClusterAddOnStatusIconMapper = ClusterAddOnStatusIconMapper;

  cluster$ = this.layoutComponent.getCluster$();

  params$ = this.cluster$.pipe(
    tap(cluster => {
      this.clusterName = cluster.metadata.name;
    }),
    map(cluster => ({
      cluster: cluster.metadata.name,
    })),
  );

  list = new K8SResourceList({
    fetchParams$: this.params$,
    fetcher: this.fetchAddOns.bind(this),
  });

  columns = [NAME, 'source', STATUS, 'version', 'created_time', ACTION];

  fetchAddOns({ cluster }: ResourceListParams) {
    return this.http
      .get<KubernetesResourceList<ClusterAddOn>>(
        `${API_GATEWAY}/apis/${ClusterAddonTypeMeta.apiVersion}/clusters/${cluster}/addons`,
      )
      .pipe(
        tap(res => {
          this.clusterAddOnDeployed = res.items;
        }),
        map(addons => {
          addons.items.sort((a, b) =>
            ('' + a.spec.type).localeCompare(b.spec.type),
          );
          return addons;
        }),
      );
  }

  trackByFn(_index: number, item: ClusterAddOn) {
    return item.metadata.uid;
  }

  ngOnInit() {
    this.k8sApi
      .getGlobalResourceList<ClusterAddOnType>({
        type: RESOURCE_TYPES.CLUSTERADDONTYPE,
      })
      .pipe(
        map(({ items }) => {
          items.sort((a, b) => ('' + a.type).localeCompare(b.type));
          return items.filter(item => addOnTypeMap[item.type.toLowerCase()]);
        }),
        takeUntil(this.onDestroy$),
      )
      .subscribe(clusterAddOnTypes => {
        this.clusterAddOnTypes = clusterAddOnTypes;
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  deploy() {
    const dialogRef = this.dialog.open(ClusterAddOnDeployComponent, {
      size: DialogSize.Large,
      data: {
        clusterName: this.clusterName,
        clusterAddOnTypes: this.clusterAddOnTypes,
        clusterAddOnDeployed: this.clusterAddOnDeployed,
      },
    });
    dialogRef.afterClosed().subscribe(item => {
      if (item) {
        this.messageService.success(
          this.translate.get('addon_deploy_success', {
            name: item.kind,
          }),
        );
        this.list.reload();
      }
    });
  }

  async delete(addon: ClusterAddOn) {
    try {
      await this.dialog.confirm({
        title: this.translate.get('addon_delete_confirm', {
          name: addon.spec.type,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }
    const requestType = addOnTypeMap[addon.spec.type.toLowerCase()];
    this.http
      .delete(
        `${API_GATEWAY}/apis/${ClusterAddonTypeMeta.apiVersion}/${requestType}/${addon.metadata.name}`,
      )
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(() => {
        this.messageService.success(this.translate.get('addon_delete_success'));
        this.list.reload();
      });
  }
}
