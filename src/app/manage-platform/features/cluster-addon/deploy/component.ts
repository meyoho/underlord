import {
  API_GATEWAY,
  KubernetesResourceList,
  NAME,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, DialogService, DialogSize } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';

import { ClusterAddOn, ClusterAddOnType } from 'app/typings';
import { ClusterAddonTypeMeta } from 'app/utils';

import { ClusterAddOnDetailComponent } from '../detail/component';
import { addOnTypeMap } from '../utils';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class ClusterAddOnDeployComponent implements OnDestroy {
  submitting = false;
  columns = [NAME, 'desc', 'detail', 'deploy'];
  addWithDesc = ['GPUManager', 'TappController', 'IPAM'];

  onDestroy$ = new Subject<void>();

  isAddOnDeployed = (item: ClusterAddOnType) => {
    return this.modalData.clusterAddOnDeployed.some(
      addons => addons.spec.type === item.type,
    );
  };

  constructor(
    @Inject(DIALOG_DATA)
    public readonly modalData: {
      clusterName: string;
      clusterAddOnTypes: ClusterAddOnType[];
      clusterAddOnDeployed: ClusterAddOn[];
    },
    private readonly http: HttpClient,
    private readonly dialogRef: DialogRef,
    private readonly dialog: DialogService,
  ) {}

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  showDetail(clusterAddOnType: ClusterAddOnType) {
    const dialogRef = this.dialog.open(ClusterAddOnDetailComponent, {
      size: DialogSize.Large,
      data: {
        clusterName: this.modalData.clusterName,
        clusterAddOnType,
        disableDeploy: this.isAddOnDeployed(clusterAddOnType),
      },
    });
    dialogRef.afterClosed().subscribe(addon => {
      if (addon) {
        this.dialogRef.close(addon);
      }
    });
  }

  deploy(clusterAddOnType: ClusterAddOnType) {
    const requestType = addOnTypeMap[clusterAddOnType.type.toLowerCase()];
    this.http
      .post<KubernetesResourceList<ClusterAddOn>>(
        `${API_GATEWAY}/apis/${ClusterAddonTypeMeta.apiVersion}/${requestType}`,
        {
          apiVersion: ClusterAddonTypeMeta.apiVersion,
          kind: clusterAddOnType.type,
          metadata: {
            generateName: clusterAddOnType.metadata.name,
          },
          spec: {
            clusterName: this.modalData.clusterName,
          },
        },
      )
      .pipe(
        takeUntil(this.onDestroy$),
        finalize(() => (this.submitting = false)),
      )
      .subscribe(addon => {
        this.dialogRef.close(addon);
      });
  }

  cancel() {
    this.dialogRef.close(null);
  }
}
