import { StringMap } from '@alauda/common-snippet';

export const addOnTypeMap: StringMap = {
  gpumanager: 'gpumanagers',
  tappcontroller: 'tappcontrollers',
  ipam: 'ipams',
};
