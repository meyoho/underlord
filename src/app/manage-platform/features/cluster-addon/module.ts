import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { ClusterAddOnDeployComponent } from './deploy/component';
import { ClusterAddOnDetailComponent } from './detail/component';
import { ListPageComponent } from './list/component';
import { ClusterAddonRoutingModule } from './routing.module';

@NgModule({
  imports: [SharedModule, ClusterAddonRoutingModule],
  declarations: [
    ListPageComponent,
    ClusterAddOnDetailComponent,
    ClusterAddOnDeployComponent,
  ],
  entryComponents: [ClusterAddOnDeployComponent, ClusterAddOnDetailComponent],
})
export class ClusterAddonModule {}
