import { API_GATEWAY, KubernetesResourceList } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnDestroy } from '@angular/core';
import marked from 'marked';
import { Subject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';

import { ClusterAddOn, ClusterAddOnType } from 'app/typings';
import { ClusterAddonTypeMeta } from 'app/utils';

import { descriptions } from '../addon-descriptions';
import { addOnTypeMap } from '../utils';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class ClusterAddOnDetailComponent implements OnDestroy {
  onDestroy$ = new Subject<void>();

  submitting = false;
  addOnDetail = '';

  constructor(
    @Inject(DIALOG_DATA)
    public readonly modalData: {
      clusterName: string;
      clusterAddOnType: ClusterAddOnType;
      disableDeploy: boolean;
    },
    private readonly http: HttpClient,
    private readonly dialogRef: DialogRef,
  ) {
    if (modalData.clusterAddOnType.type === 'GPUManager') {
      this.addOnDetail = marked(descriptions.gpumanager);
    }
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  deploy() {
    const requestType =
      addOnTypeMap[this.modalData.clusterAddOnType.type.toLowerCase()];
    this.http
      .post<KubernetesResourceList<ClusterAddOn>>(
        `${API_GATEWAY}/apis/${ClusterAddonTypeMeta.apiVersion}/${requestType}`,
        {
          apiVersion: ClusterAddonTypeMeta.apiVersion,
          kind: this.modalData.clusterAddOnType.type,
          metadata: {
            generateName: this.modalData.clusterAddOnType.metadata.name,
          },
          spec: {
            clusterName: this.modalData.clusterName,
          },
        },
      )
      .pipe(
        takeUntil(this.onDestroy$),
        finalize(() => (this.submitting = false)),
      )
      .subscribe(addon => {
        this.dialogRef.close(addon);
      });
  }

  cancel() {
    this.dialogRef.close(null);
  }
}
