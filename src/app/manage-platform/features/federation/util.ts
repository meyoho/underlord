import {
  ClusterFed,
  ClusterFedStatusEnum,
  ClusterFedStatusMapper,
} from 'app/typings';

export function getClusterFedStatus(clusterFed: ClusterFed) {
  if (clusterFed.status && clusterFed.status.phase) {
    const status = clusterFed.status.phase.toLowerCase();
    return ClusterFedStatusMapper[status] || status;
  }
  return ClusterFedStatusEnum.NORMAL;
}
