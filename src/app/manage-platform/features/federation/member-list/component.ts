import {
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  ObservableInput,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import { ComponentSize, DialogService, MessageService } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { Observable, from, of } from 'rxjs';
import {
  catchError,
  map,
  mergeMap,
  pluck,
  timeout,
  toArray,
} from 'rxjs/operators';

import { Environments } from 'app/api/envs/types';
import { ENVIRONMENTS } from 'app/services/services.module';
import {
  ClusterFed,
  ClusterFedStatusEnum,
  ClusterStatusColorMapper,
  ClusterStatusIconMapper,
  Node,
  TKECluster,
} from 'app/typings';
import { ACTION, RESOURCE_TYPES, ResourceType, STATUS } from 'app/utils';
import { getTKEClusterStatus } from 'app/utils/resource-status';

import { ClusterFedAddMemberComponent } from '../add-member/component';
import { ClusterFedRemoveMemberComponent } from '../remove-member/component';
import { getClusterFedStatus } from '../util';

@Component({
  selector: 'alu-cluster-fed-members',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class ClusterFedMembersComponent implements OnInit {
  // tslint:disable-next-line: no-output-on-prefix
  @Output()
  onUpdate = new EventEmitter<void>();

  // tslint:disable-next-line: no-input-rename
  @ObservableInput()
  @Input('clusterFed')
  clusterFed$: Observable<ClusterFed>;

  clusters$ = from(this.clusterFed$).pipe(
    pluck('spec', 'clusters'),
    mergeMap(items =>
      from(items).pipe(
        mergeMap(item =>
          this.getCluster(item.name).pipe(
            mergeMap(cluster =>
              this.k8sApi
                .getResourceList<Node>({
                  type: RESOURCE_TYPES.NODE,
                  cluster: cluster.metadata.name,
                })
                .pipe(
                  pluck('items'),
                  publishRef(),
                  map(nodes => ({
                    ...item,
                    cluster,
                    nodes,
                    isHighAvailability:
                      nodes.filter(
                        node =>
                          'node-role.kubernetes.io/master' in
                          node.metadata.labels,
                      ).length > 1
                        ? 'yes'
                        : 'no',
                  })),
                  timeout(5000),
                  catchError(() =>
                    of({
                      ...item,
                      cluster,
                    }),
                  ),
                ),
            ),
          ),
        ),
        toArray(),
        map(arr => {
          arr.sort((a, b) => {
            if (a.type === 'Host') {
              return -1;
            } else if (b.type === 'Host') {
              return 1;
            }
            return 0;
          });
          return arr;
        }),
      ),
    ),
  );

  columns = [
    'cluster_name',
    'k8s_version',
    'node_count',
    'high_availability',
    STATUS,
    'create_time',
    ACTION,
  ];

  clusterFed: ClusterFed;

  ComponentSize = ComponentSize;
  getClusterStatus = getTKEClusterStatus;
  getClusterFedStatus = getClusterFedStatus;
  ClusterFedStatusEnum = ClusterFedStatusEnum;
  ClusterStatusColorMapper = ClusterStatusColorMapper;
  ClusterStatusIconMapper = ClusterStatusIconMapper;

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.CLUSTER_FED,
    action: [
      K8sResourceAction.CREATE,
      K8sResourceAction.UPDATE,
      K8sResourceAction.DELETE,
    ],
  });

  constructor(
    @Inject(ENVIRONMENTS) public env: Environments,
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly dialogService: DialogService,
    private readonly translate: TranslateService,
    private readonly messageService: MessageService,
  ) {}

  ngOnInit() {
    this.clusterFed$.subscribe(clusterFed => {
      this.clusterFed = clusterFed;
    });
  }

  getCluster(name: string) {
    return this.k8sApi.getGlobalResource<TKECluster>({
      type: RESOURCE_TYPES.TKE_CLUSTER,
      name,
    });
  }

  addMember() {
    const dialogRef = this.dialogService.open(ClusterFedAddMemberComponent, {
      data: {
        clusterFed: this.clusterFed,
      },
    });
    dialogRef.afterClosed().subscribe(() => {
      this.onUpdate.next(null);
    });
  }

  removeMember(cluster: TKECluster) {
    const dialogRef = this.dialogService.open(ClusterFedRemoveMemberComponent, {
      data: {
        cluster,
        clusterFed: this.clusterFed,
      },
    });

    dialogRef.afterClosed().subscribe(cluster => {
      if (cluster) {
        this.messageService.success(this.translate.get('delete_success'));
        this.onUpdate.next(null);
      }
    });
  }
}
