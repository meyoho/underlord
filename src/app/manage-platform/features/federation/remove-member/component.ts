import {
  K8sApiService,
  K8sUtilService,
  TranslateService,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
} from '@angular/core';

import { Cluster, ClusterFed, ClusterStatusEnum } from 'app/typings';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';
import { getClusterStatus } from 'app/utils/resource-status';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClusterFedRemoveMemberComponent {
  inputValue = '';
  clearResource = false;
  submitting = false;
  getClusterStatus = getClusterStatus;
  ClusterStatusEnum = ClusterStatusEnum;

  constructor(
    @Inject(DIALOG_DATA)
    public readonly modalData: {
      cluster: Cluster;
      clusterFed: ClusterFed;
    },
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    private readonly dialogRef: DialogRef,
    private readonly translateService: TranslateService,
  ) {}

  confirm() {
    this.submitting = true;
    this.k8sApi
      .patchGlobalResource<ClusterFed>({
        type: RESOURCE_TYPES.CLUSTER_FED,
        resource: this.modalData.clusterFed,
        part: {
          spec: {
            clusters: this.modalData.clusterFed.spec.clusters.filter(
              cluster => cluster.name !== this.modalData.cluster.metadata.name,
            ),
            unjoinpolicy: this.clearResource ? 'Delete' : 'Retain',
          },
        },
      })
      .subscribe(
        cluster => {
          this.dialogRef.close(cluster);
        },
        () => {
          this.submitting = false;
          this.cdr.markForCheck();
        },
      );
  }

  cancel() {
    this.dialogRef.close(null);
  }

  get disabled() {
    return this.inputValue !== this.modalData.cluster.metadata.name;
  }

  get deleteConfirm() {
    const status = getClusterStatus(this.modalData.cluster);
    if (status === ClusterStatusEnum.normal) {
      return this.translateService.get('delete_federation_member_content', {
        name: this.modalData.cluster.metadata.name,
      });
    } else if (status === ClusterStatusEnum.abnormal) {
      return this.translateService.get(
        'delete_federation_abnormal_member_content',
        {
          name: this.modalData.cluster.metadata.name,
        },
      );
    }
  }

  label = (item: Cluster) => {
    const displayable = {
      name: this.k8sUtil.getName(item),
      displayName: this.k8sUtil.getDisplayName(item),
    };
    if (displayable.displayName) {
      return `${displayable.name} (${displayable.displayName})`;
    }
    return displayable.name;
  };
}
