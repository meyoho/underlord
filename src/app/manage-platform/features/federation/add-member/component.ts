import {
  K8sApiService,
  K8sUtilService,
  NAME,
  StringMap,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import { AdvanceApi } from 'app/api/advance/api';
import { AvailableResourceCapacity, ClusterFed, TKECluster } from 'app/typings';
import {
  RESOURCE_TYPES,
  ResourceType,
  getClusterStatus,
  toUnitGi,
  toUnitNum,
} from 'app/utils';

interface TKEClusterExtended extends TKECluster {
  allocatable: StringMap;
  capacity: AvailableResourceCapacity;
}

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClusterFedAddMemberComponent implements OnInit {
  @ViewChild('form', { static: true })
  form: NgForm;

  members: TKEClusterExtended[];
  submitting = false;
  clusters: TKEClusterExtended[];
  clusters$ = combineLatest([
    this.k8sApi.getGlobalResourceList<TKEClusterExtended>({
      type: RESOURCE_TYPES.CLUSTER_REGISTRY,
    }),
    this.k8sApi.getGlobalResourceList<ClusterFed>({
      type: RESOURCE_TYPES.CLUSTER_FED,
    }),
    this.advanceApi.getAvailableResource(),
  ]).pipe(
    map(([clusterList, fedList, limits]) => {
      const clusters = clusterList.items.filter(
        cluster => !this.k8sUtil.getLabel(cluster, NAME, 'federation'),
      );
      const fedClusters = fedList.items.reduce(
        (a, b) => a.concat(b.spec.clusters.map(cluster => cluster.name)),
        [],
      );
      return clusters
        .filter(cluster => !fedClusters.includes(cluster.metadata.name))
        .map(cluster => {
          const limit = limits.items?.find(
            _limit => _limit.metadata.name === cluster.metadata.name,
          );
          return {
            ...cluster,
            allocatable: limit ? limit.status.allocatable : {},
            capacity: limit ? limit.status.capacity : null,
          };
        });
    }),
  );

  toUnitNum = toUnitNum;
  toUnitGi = toUnitGi;
  getClusterStatus = getClusterStatus;

  constructor(
    @Inject(DIALOG_DATA)
    public readonly modalData: {
      clusterFed: ClusterFed;
    },
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    private readonly dialogRef: DialogRef,
    private readonly advanceApi: AdvanceApi,
  ) {}

  ngOnInit() {
    this.clusters$.subscribe(clusters => {
      this.clusters = clusters;
    });
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.submitting = true;
    this.k8sApi
      .patchGlobalResource<ClusterFed>({
        type: RESOURCE_TYPES.CLUSTER_FED,
        resource: this.modalData.clusterFed,
        part: {
          spec: {
            clusters: this.modalData.clusterFed.spec.clusters.concat(
              this.members.map(member => ({
                name: member.metadata.name,
                type: 'Member',
              })),
            ),
            unjoinpolicy: 'Retain',
          },
        },
      })
      .subscribe(
        cluster => {
          this.dialogRef.close(cluster);
        },
        () => {
          this.submitting = false;
          this.cdr.markForCheck();
        },
      );
  }

  cancel() {
    this.dialogRef.close(null);
  }
}
