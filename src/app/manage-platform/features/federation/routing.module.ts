import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ClusterFedDetailComponent } from './detail/component';
import { ClusterFedFormComponent } from './form/component';
import { ClusterFedListComponent } from './list/component';

const federationRoutes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'list',
    component: ClusterFedListComponent,
  },
  {
    path: 'create',
    component: ClusterFedFormComponent,
  },
  {
    path: 'detail/:name',
    component: ClusterFedDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(federationRoutes)],
  exports: [RouterModule],
})
export class FederationRoutingModule {}
