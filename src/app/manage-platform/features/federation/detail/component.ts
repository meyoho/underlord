import {
  AsyncDataLoader,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  NAME,
  publishRef,
} from '@alauda/common-snippet';
import { DialogRef, DialogService } from '@alauda/ui';
import { Component, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { ConfirmDeleteComponent } from 'app/shared/components/confirm-delete/confirm-delete.component';
import {
  ClusterFed,
  ClusterFedStatusColorMapper,
  ClusterFedStatusEnum,
  ClusterFedStatusIconMapper,
} from 'app/typings';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';

import { ClusterFedUpdateDisplayNameComponent } from '../update-display-name/component';
import { getClusterFedStatus } from '../util';

@Component({
  templateUrl: 'template.html',
})
export class ClusterFedDetailComponent {
  context = this;
  deleteDialogRef: DialogRef<ConfirmDeleteComponent>;
  getClusterFedStatus = getClusterFedStatus;
  ClusterFedStatusColorMapper = ClusterFedStatusColorMapper;
  ClusterFedStatusIconMapper = ClusterFedStatusIconMapper;
  params$ = this.route.paramMap.pipe(
    map(params => ({
      name: params.get(NAME),
    })),
    publishRef(),
  );

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.CLUSTER_FED,
    action: [K8sResourceAction.DELETE, K8sResourceAction.UPDATE],
  });

  dataManager = new AsyncDataLoader<{
    resource: ClusterFed;
    permissions: {
      update: boolean;
      delete: boolean;
    };
  }>({
    params$: this.params$,
    fetcher: params => {
      return combineLatest([
        this.k8sApi.getGlobalResource<ClusterFed>({
          type: RESOURCE_TYPES.CLUSTER_FED,
          name: params.name,
        }),
        this.permissions$,
      ]).pipe(
        map(([resource, permissions]) => ({
          resource,
          permissions,
        })),
      );
    },
  });

  constructor(
    private readonly route: ActivatedRoute,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sPermission: K8sPermissionService,
    private readonly dialogService: DialogService,
    private readonly router: Router,
  ) {}

  update(clusterFed: ClusterFed) {
    const dialogRef = this.dialogService.open(
      ClusterFedUpdateDisplayNameComponent,
      {
        data: {
          clusterFed,
        },
      },
    );
    dialogRef.afterClosed().subscribe(
      item =>
        !!item &&
        this.dataManager.reload({
          resource: item,
          permissions: this.dataManager.snapshot.data.permissions,
        }),
    );
  }

  deleteClusterFedApi() {
    return this.k8sApi
      .deleteGlobalResource<ClusterFed>({
        type: RESOURCE_TYPES.CLUSTER_FED,
        resource: this.dataManager.snapshot.data.resource,
      })
      .pipe(
        tap(() => {
          this.jumpToListPage();
        }),
      );
  }

  delete(templateRef: TemplateRef<ConfirmDeleteComponent>) {
    this.deleteDialogRef = this.dialogService.open(templateRef);
  }

  jumpToListPage = () => {
    this.router.navigate(['/manage-platform/federation']);
  };

  onUpdate() {
    this.dataManager.reload();
  }

  isClusterAbnormal(status: ClusterFedStatusEnum) {
    return status === ClusterFedStatusEnum.ABNORMAL;
  }
}
