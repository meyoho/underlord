import { NgModule } from '@angular/core';

import { FeaturesModule } from 'app/shared/features/module';
import { SharedModule } from 'app/shared/shared.module';

import { ClusterFedAddMemberComponent } from './add-member/component';
import { ClusterFedDetailComponent } from './detail/component';
import { ClusterFedFormComponent } from './form/component';
import { ClusterFedListComponent } from './list/component';
import { ClusterFedMembersComponent } from './member-list/component';
import { ClusterFedRemoveMemberComponent } from './remove-member/component';
import { FederationRoutingModule } from './routing.module';
import { ClusterFedUpdateDisplayNameComponent } from './update-display-name/component';

@NgModule({
  imports: [SharedModule, FederationRoutingModule, FeaturesModule],
  declarations: [
    ClusterFedAddMemberComponent,
    ClusterFedDetailComponent,
    ClusterFedFormComponent,
    ClusterFedListComponent,
    ClusterFedMembersComponent,
    ClusterFedRemoveMemberComponent,
    ClusterFedUpdateDisplayNameComponent,
  ],
  entryComponents: [
    ClusterFedAddMemberComponent,
    ClusterFedRemoveMemberComponent,
    ClusterFedUpdateDisplayNameComponent,
  ],
})
export class FederationModule {}
