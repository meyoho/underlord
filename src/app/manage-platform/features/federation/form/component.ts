import {
  DISPLAY_NAME,
  K8sApiService,
  NAME,
  StringMap,
  TranslateService,
} from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import { Location } from '@angular/common';
import { Component, Injector } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import { AdvanceApi } from 'app/api/advance/api';
import { K8sUtilService } from 'app/services/k8s-util.service';
import {
  AvailableResourceCapacity,
  Cluster,
  ClusterFed,
  TKECluster,
} from 'app/typings';
import {
  ClusterFedMeta,
  K8S_RESOURCE_NAME_BASE,
  RESOURCE_TYPES,
  ResourceType,
  getClusterStatus,
  toUnitGi,
  toUnitNum,
} from 'app/utils';

const ControlPlaceholder = 'please_select_fed_members';
interface TKEClusterExtended extends TKECluster {
  allocatable: StringMap;
  capacity: AvailableResourceCapacity;
}

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class ClusterFedFormComponent {
  submitting: boolean;
  name: string;
  displayName: string;
  controlPlaceholder = ControlPlaceholder;
  controlDisabled = true;
  control: Cluster;
  members: TKEClusterExtended[];
  clusters$ = combineLatest([
    this.k8sApi.getGlobalResourceList<TKEClusterExtended>({
      type: RESOURCE_TYPES.CLUSTER_REGISTRY,
    }),
    this.advanceApi.getAvailableResource(),
  ]).pipe(
    map(([clusterList, limits]) => {
      const clusters = clusterList.items.filter(
        cluster => !this.k8sUtil.getLabel(cluster, NAME, 'federation'),
      );
      return clusters.map(cluster => {
        const limit = limits.items?.find(
          _limit => _limit.metadata.name === cluster.metadata.name,
        );
        return {
          ...cluster,
          allocatable: limit ? limit.status.allocatable : {},
          capacity: limit ? limit.status.capacity : null,
        };
      });
    }),
  );

  K8S_RESOURCE_NAME_BASE = K8S_RESOURCE_NAME_BASE;
  toUnitNum = toUnitNum;
  toUnitGi = toUnitGi;
  getClusterStatus = getClusterStatus;

  constructor(
    public injector: Injector,
    private readonly location: Location,
    private readonly translate: TranslateService,
    private readonly k8sUtil: K8sUtilService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly messageService: MessageService,
    private readonly router: Router,
    private readonly advanceApi: AdvanceApi,
  ) {}

  confirm(form: NgForm) {
    form.onSubmit(null);
    if (form.invalid) {
      return;
    }
    this.k8sApi
      .postGlobalResource<ClusterFed>({
        type: RESOURCE_TYPES.CLUSTER_FED,
        resource: {
          apiVersion: ClusterFedMeta.apiVersion,
          kind: ClusterFedMeta.kind,
          metadata: {
            name: this.name,
            annotations: {
              [this.k8sUtil.normalizeType(DISPLAY_NAME)]: this.displayName,
            },
          },
          spec: {
            clusters: this.members.map(member => ({
              name: member.metadata.name,
              type:
                this.control.metadata.name === member.metadata.name
                  ? 'Host'
                  : 'Member',
            })),
          },
        },
      })
      .subscribe(
        () => {
          this.messageService.success(this.translate.get('create_success'));
          this.router.navigate(['/manage-platform/federation']);
        },
        () => {
          this.submitting = false;
        },
      );
  }

  cancel() {
    this.location.back();
  }

  membersChange(members: Cluster[]) {
    if (members.length === 0) {
      this.controlDisabled = true;
      this.controlPlaceholder = ControlPlaceholder;
    } else {
      this.controlDisabled = false;
      this.controlPlaceholder = '';
    }
    if (
      this.control &&
      !members.find(mem => mem.metadata.name === this.control.metadata.name)
    ) {
      this.control = null;
    }
  }
}
