import {
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  NAME,
  matchLabelsToString,
  stringToMatchLabels,
} from '@alauda/common-snippet';
import { DialogRef, DialogService } from '@alauda/ui';
import { Component, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';

import { ConfirmDeleteComponent } from 'app/shared/components/confirm-delete/confirm-delete.component';
import {
  ClusterFed,
  ClusterFedItem,
  ClusterFedStatusColorMapper,
  ClusterFedStatusEnum,
  ClusterFedStatusIconMapper,
  TKECluster,
} from 'app/typings';
import { RESOURCE_TYPES, ResourceType, STATUS } from 'app/utils';

import { ClusterFedUpdateDisplayNameComponent } from '../update-display-name/component';
import { getClusterFedStatus } from '../util';

interface ClusterFedDisplayItem extends ClusterFedItem {
  displayName: string;
}

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class ClusterFedListComponent {
  resource: ClusterFed;
  context = this;
  deleteDialogRef: DialogRef<ConfirmDeleteComponent>;
  list = new K8SResourceList({
    fetcher: this.fetchList.bind(this),
    activatedRoute: this.activatedRoute,
  });

  columns = [NAME, STATUS, 'member', 'create_time', 'action'];
  getClusterFedStatus = getClusterFedStatus;
  ClusterFedStatusColorMapper = ClusterFedStatusColorMapper;
  ClusterFedStatusIconMapper = ClusterFedStatusIconMapper;

  searchKey$ = this.activatedRoute.queryParamMap.pipe(
    map(
      params =>
        stringToMatchLabels(params.get('fieldSelector'))['metadata.name'],
    ),
  );

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.CLUSTER_FED,
    action: [
      K8sResourceAction.CREATE,
      K8sResourceAction.UPDATE,
      K8sResourceAction.DELETE,
    ],
  });

  constructor(
    private readonly dialogService: DialogService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sPermission: K8sPermissionService,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
  ) {}

  fetchList({ ...queryParams }) {
    return combineLatest([
      this.k8sApi.getGlobalResourceList<ClusterFed>({
        type: RESOURCE_TYPES.CLUSTER_FED,
        queryParams,
      }),
      this.k8sApi
        .getGlobalResourceList<TKECluster>({
          type: RESOURCE_TYPES.TKE_CLUSTER,
        })
        .pipe(map(tkeCluster => tkeCluster.items)),
    ]).pipe(
      switchMap(([{ kind, items, metadata }, clusters]) =>
        of({
          kind,
          items: items.map(item => ({
            ...item,
            spec: {
              clusters: item.spec.clusters.map(cluster => {
                let displayName = '';
                const found = clusters.find(
                  val => val.metadata.name === cluster.name,
                );
                if (found) {
                  displayName = found.spec.displayName;
                }
                return {
                  ...cluster,
                  displayName,
                };
              }),
            },
          })),
          metadata,
        }),
      ),
    );
  }

  searchByName(name: string) {
    this.router.navigate(['./'], {
      queryParams: {
        fieldSelector: matchLabelsToString({
          'metadata.name': name,
        }),
      },
      queryParamsHandling: 'merge',
      relativeTo: this.activatedRoute,
    });
  }

  update(clusterFed: ClusterFed) {
    const dialogRef = this.dialogService.open(
      ClusterFedUpdateDisplayNameComponent,
      {
        data: {
          clusterFed,
        },
      },
    );
    dialogRef.afterClosed().subscribe(item => !!item && this.list.update(item));
  }

  deleteClusterFedApi() {
    return this.k8sApi
      .deleteGlobalResource<ClusterFed>({
        type: RESOURCE_TYPES.CLUSTER_FED,
        resource: this.resource,
      })
      .pipe(
        tap(() => {
          this.list.reload();
        }),
      );
  }

  delete(
    templateRef: TemplateRef<ConfirmDeleteComponent>,
    resource: ClusterFed,
  ) {
    this.resource = resource;
    this.deleteDialogRef = this.dialogService.open(templateRef);
  }

  trackByFn(index: number) {
    return index;
  }

  getClusterMember(members: ClusterFedDisplayItem[]) {
    return members.map(member => {
      if (member.displayName) {
        return `${member.name}(${member.displayName})`;
      }
      return member.name;
    });
  }

  getCommonMask(status: string) {
    return (
      status === ClusterFedStatusEnum.CREATING ||
      status === ClusterFedStatusEnum.DELETING
    );
  }

  getActionMask(status: string) {
    return status === ClusterFedStatusEnum.DELETING;
  }

  getUpdateMask(status: string) {
    return status !== ClusterFedStatusEnum.CREATING;
  }
}
