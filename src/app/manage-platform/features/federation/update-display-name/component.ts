import {
  DISPLAY_NAME,
  K8sApiService,
  TranslateService,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { K8sUtilService } from 'app/services/k8s-util.service';
import { ClusterFed } from 'app/typings';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClusterFedUpdateDisplayNameComponent {
  @ViewChild('form', { static: true })
  form: NgForm;

  name = this.k8sUtil.getName(this.modalData.clusterFed);
  displayName: string;
  submitting = false;

  constructor(
    @Inject(DIALOG_DATA)
    private readonly modalData: {
      clusterFed: ClusterFed;
    },
    private readonly k8sUtil: K8sUtilService,
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly dialogRef: DialogRef,
    private readonly translate: TranslateService,
    private readonly message: MessageService,
  ) {
    this.displayName = this.k8sUtil.getDisplayName(this.modalData.clusterFed);
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.submitting = true;
    this.k8sApi
      .patchGlobalResource<ClusterFed>({
        type: RESOURCE_TYPES.CLUSTER_FED,
        resource: this.modalData.clusterFed,
        part: {
          metadata: {
            annotations: {
              [this.k8sUtil.normalizeType(DISPLAY_NAME)]: this.displayName,
            },
          },
          spec: {
            unjoinpolicy: 'Retain',
          },
        },
      })
      .subscribe(
        cluster => {
          this.message.success(this.translate.get('update_success'));
          this.dialogRef.close(cluster);
        },
        () => {
          this.submitting = false;
          this.cdr.markForCheck();
        },
      );
  }

  cancel() {
    this.dialogRef.close(null);
  }
}
