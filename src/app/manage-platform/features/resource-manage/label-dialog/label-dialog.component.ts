import {
  K8sApiService,
  KubernetesResource,
  StringMap,
  TranslateService,
  getApiPrefixParts,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, NotificationService } from '@alauda/ui';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { get } from 'lodash-es';

import { APIResource } from 'app/typings';

import { RSRCManagementService } from '../service';

@Component({
  selector: 'alu-resource-label-dialog',
  templateUrl: './label-dialog.component.html',
  styleUrls: ['./label-dialog.component.scss'],
})
export class ResourceLabelDialogComponent implements OnInit {
  @ViewChild('form', { static: true })
  form: NgForm;

  loading: boolean;
  resourceType: APIResource;

  labels: StringMap;
  nullLabels: StringMap;

  constructor(
    private readonly dialogRef: DialogRef,
    private readonly rsrcManagementService: RSRCManagementService,
    private readonly notificationService: NotificationService,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService,
    @Inject(DIALOG_DATA) private readonly data: KubernetesResource,
  ) {}

  ngOnInit() {
    this.rsrcManagementService.resourceType$.subscribe(
      type => (this.resourceType = type),
    );
    this.labels = get(this.data, 'metadata.labels', {});
    this.nullLabels = Object.keys(this.labels).reduce<Record<string, null>>(
      (acc, cur) => {
        acc[cur] = null;
        return acc;
      },
      {},
    );
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    try {
      this.loading = true;
      this.k8sApi.patchResource({
        definition: {
          type: this.resourceType.name,
          ...getApiPrefixParts(this.resourceType.groupVersion),
        },
        cluster: this.rsrcManagementService.cluster,
        resource: this.data,
        part: {
          metadata: {
            labels: {
              ...this.nullLabels,
              ...this.labels,
            },
          },
        },
      });
      this.notificationService.success(this.translate.get('update_successed'));
      this.loading = false;
      this.dialogRef.close(true);
    } catch (err) {
      this.loading = false;
    }
  }
}
