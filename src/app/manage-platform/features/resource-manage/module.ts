import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { ResourceEditPageComponent } from './edit-page.component';
import { ResourceCreateComponent } from './edit/edit.component';
import { ResourceLabelDialogComponent } from './label-dialog/label-dialog.component';
import { ResourceListPageComponent } from './list-page.component';
import { ResourceListComponent } from './list/list.component';
import { ResourceManageRoutingModule } from './routing.module';

@NgModule({
  imports: [SharedModule, ResourceManageRoutingModule],
  declarations: [
    ResourceListComponent,
    ResourceCreateComponent,
    ResourceLabelDialogComponent,
    ResourceListPageComponent,
    ResourceEditPageComponent,
  ],
  entryComponents: [ResourceLabelDialogComponent],
})
export class ResourceManageModule {}
