import {
  Callback,
  K8sApiService,
  K8sResourceDefinition,
  KubernetesResource,
  TranslateService,
  noop,
} from '@alauda/common-snippet';
import { ConfirmType, DialogService, NotificationService } from '@alauda/ui';
import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { safeDump, safeLoad, safeLoadAll } from 'js-yaml';
import { of } from 'rxjs';

import { AcpApiService } from 'app/api/resource-manage/acp-api.service';
import { APIResource } from 'app/typings';
import { createActions, updateActions, yamlWriteOptions } from 'app/utils';

import { RSRCManagementService } from '../service';

@Component({
  selector: 'alu-resource-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class ResourceCreateComponent implements OnInit, OnDestroy {
  @Input()
  isUpdate: boolean;

  editorActions = createActions;
  editorOptions = yamlWriteOptions;
  yamlInputValue = '';
  originalYaml = '';
  resourceData: KubernetesResource;
  resourceType: APIResource;
  resourceDefinition: K8sResourceDefinition;
  updatePayload: KubernetesResource;
  createPayload: KubernetesResource[];

  @ViewChild('form', { static: false })
  form: NgForm;

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly dialogService: DialogService,
    private readonly k8sApi: K8sApiService,
    private readonly notification: NotificationService,
    private readonly translate: TranslateService,
    public readonly RSRCService: RSRCManagementService,
    private readonly acpApi: AcpApiService,
  ) {}

  ngOnInit() {
    this.RSRCService.resourceData$.subscribe(data => this.onViewChange(data));
    this.RSRCService.resourceType$.subscribe(type => {
      this.resourceType = type;
      this.resourceDefinition = this.RSRCService.getResourceDefinition(type);
    });
  }

  ngOnDestroy() {
    this.RSRCService.setResourceData(null);
  }

  private onViewChange(data: KubernetesResource) {
    this.resourceData = data;
    this.yamlInputValue = data && safeDump(data, { lineWidth: 9999 });
    if (this.isUpdate) {
      this.updatePayload = safeLoad(this.yamlInputValue);
    }
    this.originalYaml = this.yamlInputValue;
    this.editorActions = updateActions;
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }

    try {
      if (this.isUpdate) {
        this.updatePayload = safeLoad(this.yamlInputValue);
      } else {
        const loadedYaml = safeLoadAll(this.yamlInputValue).filter(i => !!i);
        this.createPayload = loadedYaml;
      }
    } catch (error) {
      this.notification.error(this.translate.get('yaml_format_hint'));
      return;
    }

    this.dialogService
      .confirm({
        title: this.translate.get(
          this.isUpdate
            ? 'confirm_update_resource_content'
            : 'confirm_create_resource_content',
        ),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        confirmType: ConfirmType.Primary,
        beforeConfirm: this.isUpdate
          ? this.updateResource
          : this.createResource,
      })
      .then(() =>
        this.router.navigate(['../list'], {
          relativeTo: this.route,
        }),
      )
      .catch(noop);
  }

  cancel() {
    if (!this.yamlInputValue) {
      return this.router.navigate(['../list'], {
        relativeTo: this.route,
      });
    }

    this.dialogService
      .confirm({
        title: this.translate.get(
          this.isUpdate
            ? 'cancel_resource_update_content'
            : 'cancel_resource_create_content',
        ),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        confirmType: ConfirmType.Warning,
      })
      .then(() => {
        this.router.navigate(['../list'], {
          relativeTo: this.route,
        });
      })
      .catch(noop);
  }

  private readonly createResource = (resolve: Callback, reject: Callback) => {
    (this.createPayload.length > 0
      ? this.acpApi.createResources(
          this.RSRCService.cluster,
          this.createPayload,
        )
      : of([])
    ).subscribe(resolve, reject);
  };

  private readonly updateResource = (resolve: Callback, reject: Callback) => {
    this.k8sApi
      .putResource({
        definition: this.resourceDefinition,
        cluster: this.RSRCService.cluster,
        resource: this.updatePayload,
      })
      .subscribe(resolve, reject);
  };
}
