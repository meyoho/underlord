import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ResourceEditPageComponent } from './edit-page.component';
import { ResourceListPageComponent } from './list-page.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'list',
  },
  {
    path: 'list',
    component: ResourceListPageComponent,
  },
  {
    path: 'edit',
    component: ResourceEditPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResourceManageRoutingModule {}
