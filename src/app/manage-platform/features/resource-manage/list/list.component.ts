import {
  COMMON_WRITABLE_ACTIONS,
  JSONPatchOp,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  KubernetesResource,
  NAME,
  NAMESPACE,
  PatchOperation,
  TranslateService,
  isAllowed,
  noop,
  publishRef,
} from '@alauda/common-snippet';
import {
  ConfirmType,
  DialogService,
  DialogSize,
  NotificationService,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  TemplateRef,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { safeDump } from 'js-yaml';
import {
  debounce,
  find,
  get,
  includes,
  isEqual,
  memoize,
  omitBy,
} from 'lodash-es';
import { BehaviorSubject, Observable, Subject, combineLatest, of } from 'rxjs';
import {
  catchError,
  distinctUntilChanged,
  filter,
  map,
  pluck,
  shareReplay,
  switchMap,
  takeUntil,
  tap,
  withLatestFrom,
} from 'rxjs/operators';

import { Environments } from 'app/api/envs/types';
import { ManagePlatformComponent } from 'app/manage-platform/component';
import { K8sUtilService } from 'app/services/k8s-util.service';
import { ENVIRONMENTS } from 'app/services/services.module';
import { APIResource, Namespace, WorkspaceListParams } from 'app/typings';
import {
  ACTION,
  RESOURCE_TYPES,
  ResourceType,
  viewActions,
  yamlReadOptions,
} from 'app/utils';

import { ResourceLabelDialogComponent } from '../label-dialog/label-dialog.component';
import { Category, RSRCManagementService } from '../service';

export interface ColumnDef {
  name: string;
  label?: string;
  sortable?: boolean;
  sortStart?: 'asc' | 'desc';
}

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: NAME,
    label: NAME,
  },
  {
    name: 'label',
    label: 'label',
  },
  {
    name: NAMESPACE,
    label: NAMESPACE,
  },
  {
    name: 'creationTimestamp',
    label: 'create_time',
  },
  {
    name: ACTION,
    label: ACTION,
  },
];

const EXPANDED_MAP = {
  namespaced: false,
  cluster: false,
};

const ALL_NAMESPACE_SYMBOL = Symbol('All Namespaces');

type ResourceListComponentParams = WorkspaceListParams & {
  resourceType: APIResource;
};

@Component({
  selector: 'alu-resource-management-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourceListComponent implements OnInit, OnDestroy {
  get selectedNamespace() {
    const namespaceSnapshot = this.route.snapshot.queryParamMap.get(NAMESPACE);
    return this.clusterContext$.getValue() || namespaceSnapshot == null
      ? ''
      : namespaceSnapshot.trim();
  }

  get resourceType() {
    return this.RSRCService.resourceType;
  }

  get filteredCategory() {
    return this.getFilteredCategory(this.category, this.filterName);
  }

  private readonly onDestroy$$ = new Subject<void>();

  clusterContext$ = new BehaviorSubject<boolean>(false);

  columnDefs = ALL_COLUMN_DEFS;

  ALL_NAMESPACE_SYMBOL = ALL_NAMESPACE_SYMBOL;

  columns$: Observable<string[]>;
  keyword$ = this.route.queryParamMap.pipe(
    map(params => params.get('keyword')),
  );

  expandedMap = EXPANDED_MAP;

  activePath = '';
  yamlInputValue = '';

  namespacedKinds: string[] = [];
  clusterKinds: string[] = [];

  category: Category = {
    namespaced: [],
    cluster: [],
  };

  editorActions = viewActions;
  editorOptions = yamlReadOptions;

  cluster$ = this.layoutComponent.getCluster$().pipe(
    map(this.k8sUtil.getName),
    distinctUntilChanged(),
    tap(cluster =>
      this.RSRCService.initService(cluster, this.selectedNamespace),
    ),
    takeUntil(this.onDestroy$$),
    publishRef(),
  );

  namespaceOptions$ = this.cluster$.pipe(
    switchMap(cluster =>
      this.k8sApi
        .getResourceList<Namespace>({
          type: RESOURCE_TYPES.NAMESPACE,
          cluster,
        })
        .pipe(
          pluck('items'),
          catchError(() => of<Namespace[]>([])),
        ),
    ),
    map(items => items.map(this.k8sUtil.getName)),
    takeUntil(this.onDestroy$$),
    publishRef(),
  );

  navLoading$ = this.RSRCService.navLoading$;

  initialized: boolean;
  filterName: string;

  getFilteredCategory = memoize(
    (category: Category, filterName: string) =>
      filterName
        ? {
            namespaced: category.namespaced.filter(item =>
              item.kind.toLowerCase().includes(filterName.toLowerCase()),
            ),
            cluster: category.cluster.filter(item =>
              item.kind.toLowerCase().includes(filterName.toLowerCase()),
            ),
          }
        : category,
    (category: Category, filterName: string) =>
      JSON.stringify(
        category.namespaced
          .concat(category.cluster)
          .map(({ kind }) => kind)
          .concat(filterName),
      ),
  );

  fetchParams$ = combineLatest([
    this.cluster$,
    this.RSRCService.resourceType$,
    this.keyword$,
  ]).pipe(
    withLatestFrom(this.RSRCService.namespace$),
    distinctUntilChanged(isEqual),
    map(([[cluster, resourceType, keyword], namespace]) => ({
      cluster,
      namespace,
      resourceType,
      keyword,
    })),
    publishRef(),
  );

  list = new K8SResourceList({
    fetchParams$: this.fetchParams$,
    fetcher: this.fetchResourceList.bind(this),
  });

  permissions$ = this.RSRCService.resourceType$.pipe(
    switchMap(resourceType =>
      this.k8sPermission.getAccess({
        type: this.RSRCService.getResourceDefinition(resourceType),
        action: COMMON_WRITABLE_ACTIONS,
      }),
    ),
    isAllowed(),
    takeUntil(this.onDestroy$$),
    shareReplay(1),
  );

  onChangeHandler = debounce(function (
    this: ResourceListComponent,
    queryString: string,
  ) {
    this.navSearch(queryString);
  },
  100);

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly notification: NotificationService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sUtil: K8sUtilService,
    public readonly RSRCService: RSRCManagementService,
    private readonly layoutComponent: ManagePlatformComponent,
    @Inject(ENVIRONMENTS) private readonly env: Environments,
  ) {}

  fetchResourceList({
    cluster,
    namespace,
    resourceType,
    ...queryParams
  }: ResourceListComponentParams) {
    return this.k8sApi.getResourceList({
      definition: this.RSRCService.getResourceDefinition(resourceType),
      cluster,
      namespace,
      queryParams,
    });
  }

  ngOnInit() {
    this.RSRCService.category$
      .pipe(
        takeUntil(this.onDestroy$$),
        filter(_ => !!_),
      )
      .subscribe(category => {
        this.category = category;
        this.initNav();
        this.initColumns$();
      });

    this.clusterContext$
      .pipe(distinctUntilChanged())
      .subscribe(clusterContext => {
        if (clusterContext) {
          this.onNamespaceChange();
        } else {
          this.onNamespaceChange(this.selectedNamespace);
        }
      });
  }

  getActualNamespace(namespace: string | typeof ALL_NAMESPACE_SYMBOL) {
    return namespace === ALL_NAMESPACE_SYMBOL ? '' : namespace;
  }

  initNav() {
    if (!this.resourceType.kind) {
      this.expandedMap.namespaced = true;
      this.activePath = 'namespaced';
    } else {
      const isNamespacedKind = !!find(this.category.namespaced, [
        'kind',
        this.resourceType.kind,
      ]);
      this.expandedMap = {
        namespaced: isNamespacedKind,
        cluster: !isNamespacedKind,
      };
      this.activePath = isNamespacedKind ? 'namespaced' : 'cluster';
      this.clusterContext$.next(!isNamespacedKind);
    }
  }

  initColumns$() {
    const allColumns = this.columnDefs.map(colDef => colDef.name);
    const columnsWithoutNamespace = allColumns.filter(
      column => column !== NAMESPACE,
    );
    this.columns$ = this.clusterContext$.pipe(
      map(clusterContext =>
        clusterContext ? columnsWithoutNamespace : allColumns,
      ),
    );
  }

  ngOnDestroy() {
    this.onDestroy$$.next();
  }

  getLabelValue(resource: KubernetesResource) {
    return omitBy(get(resource, 'metadata.labels', {}), (_v, k) =>
      includes(k, '.' + this.env.LABEL_BASE_DOMAIN),
    );
  }

  viewResource(resource: KubernetesResource, templateRef: TemplateRef<any>) {
    this.yamlInputValue = safeDump(resource, {
      lineWidth: 9999,
      sortKeys: true,
    });
    this.dialog.open(templateRef, {
      size: DialogSize.Large,
      data: {
        name: resource.metadata.name,
      },
    });
  }

  /**
   * http://jira.alaudatech.com/browse/DEV-19150
   * 集群主机详情容器组和资源管理的容器组都不支持更新
   */
  canUpdate(item: KubernetesResource) {
    return item.kind !== 'Pod';
  }

  /**
   * Actions
   */
  updateAction(item: KubernetesResource) {
    this.RSRCService.setResourceData(item);
    this.router.navigate(['../edit'], {
      relativeTo: this.route,
    });
  }

  updateLabelAction(item: KubernetesResource) {
    const dialogRef = this.dialog.open(ResourceLabelDialogComponent, {
      data: item,
    });
    dialogRef
      .afterClosed()
      .subscribe(result => result && this.list.update(item));
  }

  deleteAction(item: KubernetesResource) {
    this.dialog
      .confirm({
        title: this.translate.get('confirm_delete_resource_content', {
          kind: this.resourceType.kind,
          name: item.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        confirmType: ConfirmType.Danger,
        beforeConfirm: (resolve, reject) => {
          this.k8sApi
            .deleteResource({
              definition: this.RSRCService.getResourceDefinition(
                this.resourceType,
              ),
              cluster: this.RSRCService.cluster,
              resource: item,
            })
            .subscribe(() => {
              resolve();
              this.notification.success(this.translate.get('delete_success'));
            }, reject);
        },
      })
      .then(() => this.list.delete(item))
      .catch(noop);
  }

  forceDelete(item: KubernetesResource) {
    this.dialog
      .confirm({
        title: this.translate.get('confirm_force_delete_resource_content', {
          kind: this.resourceType.kind,
          name: item.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        confirmType: ConfirmType.Danger,
        beforeConfirm: (resolve, reject) => {
          this.k8sApi
            .patchResource({
              definition: this.RSRCService.getResourceDefinition(
                this.resourceType,
              ),
              cluster: this.RSRCService.cluster,
              resource: item,
              operation: PatchOperation.JSON,
              part: [
                {
                  op: JSONPatchOp.Replace,
                  path: '/metadata/finalizers',
                  value: null,
                },
              ],
            })
            .subscribe(() => {
              resolve();
              this.notification.success(this.translate.get('delete_success'));
            }, reject);
        },
      })
      .then(() => {
        this.list.delete(item);
      })
      .catch(noop);
  }

  /* Nav list */
  toggleExpand(path: keyof typeof EXPANDED_MAP) {
    this.expandedMap[path] = !this.expandedMap[path];
    this.activePath = path;
  }

  onResourceTypeChange(resourceType: APIResource) {
    this.clusterContext$.next(!resourceType.namespaced);
    this.RSRCService.setResourceType(resourceType);
    this.router.navigate(['.'], {
      relativeTo: this.route,
      queryParams: {
        keyword: '',
      },
      queryParamsHandling: 'merge',
    });
  }

  isPathActive(path: string) {
    return this.activePath.startsWith(path);
  }

  isPathExpanded(path: keyof typeof EXPANDED_MAP) {
    return this.expandedMap[path];
  }

  navSearch(queryString: string) {
    this.filterName = queryString.trim();
    this.expandedMap.namespaced = true;
    this.expandedMap.cluster = true;
    this.cdr.markForCheck();
  }

  trackByFn(index: number, item: KubernetesResource) {
    return get(item, 'metadata.uid', index);
  }

  onNamespaceChange(
    namespace: string | typeof ALL_NAMESPACE_SYMBOL = ALL_NAMESPACE_SYMBOL,
  ) {
    const actualNamespace = this.getActualNamespace(namespace);
    this.RSRCService.setNamespace(actualNamespace);

    if (!this.initialized) {
      this.initialized = true;
      return;
    }

    this.router.navigate(['.'], {
      relativeTo: this.route,
      queryParams: {
        keyword: '',
        namespace: actualNamespace,
      },
      queryParamsHandling: 'merge',
    });
  }

  onSearch(keyword: string) {
    this.router.navigate(['.'], {
      relativeTo: this.route,
      queryParams: {
        keyword,
      },
      queryParamsHandling: 'merge',
    });
  }
}
