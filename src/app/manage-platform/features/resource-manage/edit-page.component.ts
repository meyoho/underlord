import { publishRef } from '@alauda/common-snippet';
import { Component } from '@angular/core';
import { map } from 'rxjs/operators';

import { RSRCManagementService } from './service';

@Component({
  templateUrl: './edit-page.component.html',
})
export class ResourceEditPageComponent {
  isUpdate$ = this.RSRCService.resourceData$.pipe(
    map(data => !!data),
    publishRef(),
  );

  constructor(private readonly RSRCService: RSRCManagementService) {}
}
