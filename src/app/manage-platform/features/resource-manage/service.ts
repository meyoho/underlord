import {
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  K8sResourceDefinition,
  KubernetesResource,
  TranslateService,
  getApiPrefixParts,
} from '@alauda/common-snippet';
import { NotificationService } from '@alauda/ui';
import { Injectable } from '@angular/core';
import { concat, get, sortBy, uniqBy } from 'lodash-es';
import { BehaviorSubject } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { AcpApiService } from 'app/api/resource-manage/acp-api.service';
import { SessionStorageService } from 'app/api/resource-manage/storage.service';
import { APIResource, APIResourceList } from 'app/typings';

export interface ResourceManagementListFetchParams {
  definition: K8sResourceDefinition;
}

export interface Category {
  namespaced: APIResource[];
  cluster: APIResource[];
}

export const RSRC_RESOURCE_CLUSTER = 'RSRC_RESOURCE_CLUSTER';
export const RSRC_RESOURCE_DATA = 'RSRC_RESOURCE_DATA';
export const RSRC_RESOURCE_TYPE = 'RSRC_RESOURCE_TYPE';

@Injectable({ providedIn: 'root' })
export class RSRCManagementService {
  private _resourceType = this.sessionStorage.getItem<APIResource>(
    RSRC_RESOURCE_TYPE,
  );

  private readonly resourceType$$ = new BehaviorSubject<APIResource>(
    this._resourceType,
  );

  readonly resourceType$ = this.resourceType$$
    .asObservable()
    .pipe(filter(_ => !!_));

  get resourceType() {
    return this._resourceType;
  }

  private _namespace: string;
  private readonly namespace$$ = new BehaviorSubject<string>(null);
  readonly namespace$ = this.namespace$$
    .asObservable()
    .pipe(filter(_ => _ != null));

  get namespace() {
    return this._namespace;
  }

  readonly category$ = new BehaviorSubject<Category>(null);

  readonly resourceData$ = new BehaviorSubject<KubernetesResource>(
    this.sessionStorage.getItem(RSRC_RESOURCE_DATA),
  );

  readonly navLoading$ = new BehaviorSubject<boolean>(true);

  cluster = this.sessionStorage.getItem<string>(RSRC_RESOURCE_CLUSTER);
  project: string;
  createEnabled: boolean;
  initialized = false;

  constructor(
    private readonly notification: NotificationService,
    private readonly translate: TranslateService,
    public k8sApi: K8sApiService,
    private readonly acpApi: AcpApiService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly sessionStorage: SessionStorageService,
  ) {
    this.reducer = this.reducer.bind(this);
  }

  getResourceDefinition(resourceType: APIResource) {
    return {
      type: resourceType.name,
      ...getApiPrefixParts(resourceType.groupVersion),
    };
  }

  initService(cluster: string, namespace: string) {
    this.project = '';
    this.cluster = cluster;
    this.sessionStorage.setItem(RSRC_RESOURCE_CLUSTER, cluster);
    this.setNamespace(namespace);

    if (cluster) {
      this.initCreateEnabled();
      this.initResourceList();
    } else {
      this.navLoading$.next(false);
    }
  }

  private initCreateEnabled() {
    this.k8sPermission
      .isAllowed({
        type: 'APIResourceList',
        cluster: this.cluster,
        namespace: this.namespace,
        project: this.project,
        action: K8sResourceAction.CREATE,
      })
      .subscribe(isAllowed => (this.createEnabled = isAllowed));
  }

  initResourceList() {
    this.navLoading$.next(true);
    this.acpApi
      .getResourceTypes(this.cluster)
      .pipe(
        map(apiResources =>
          this.sortCategory(
            apiResources.reduce<{
              namespaced: APIResource[];
              cluster: APIResource[];
            }>(this.reducer, {
              namespaced: [],
              cluster: [],
            }),
          ),
        ),
      )
      .subscribe(
        category => {
          const resourceType = this._resourceType;
          if (!this.initialized || !(resourceType && resourceType.kind)) {
            this.setResourceType(category.namespaced[0]);
            this.initialized = true;
          }
          this.category$.next(category);
        },
        ({ status, err }) => {
          if (status === 403) {
            this.notification.error({
              title: this.translate.get('permission_denied'),
            });
          } else {
            this.notification.error(
              get(err, 'errors[0].message', this.translate.get('server_error')),
            );
          }
        },
        () => this.navLoading$.next(false),
      );
  }

  private reducer(
    accum: { namespaced: APIResource[]; cluster: APIResource[] },
    curr: APIResourceList,
  ) {
    const categories = curr.resources.filter(item => {
      item.groupVersion = curr.groupVersion;
      return !item.name.includes('/');
    });
    const namespacedKinds = categories.filter(
      ({ namespaced, verbs }) =>
        namespaced && verbs.includes(K8sResourceAction.LIST),
    );
    const clusterKinds = categories.filter(
      ({ namespaced, verbs }) =>
        !namespaced && verbs.includes(K8sResourceAction.LIST),
    );
    return {
      namespaced: uniqBy(concat(accum.namespaced, namespacedKinds), 'kind'),
      cluster: uniqBy(concat(accum.cluster, clusterKinds), 'kind'),
    };
  }

  /**
   * TODO: 资源类别排序，暂用首字母默认排序
   * @param rawCategory
   */
  private sortCategory(rawCategory: {
    namespaced: APIResource[];
    cluster: APIResource[];
  }) {
    return {
      namespaced: sortBy(rawCategory.namespaced, 'kind'),
      cluster: sortBy(rawCategory.cluster, 'kind'),
    };
  }

  setNamespace(namespace: string) {
    this._namespace = namespace;
    this.namespace$$.next(namespace);
  }

  setResourceType(resourceType: APIResource): void {
    this._resourceType = resourceType;
    this.sessionStorage.setItem(RSRC_RESOURCE_TYPE, resourceType);
    this.resourceType$$.next(resourceType);
  }

  setResourceData(data: KubernetesResource | null) {
    this.sessionStorage.setItem(RSRC_RESOURCE_DATA, data);
    this.resourceData$.next(data);
  }
}
