import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ManageLicenseListComponent } from 'app/manage-platform/features/manage-license/list/component';

const routes: Routes = [
  {
    path: '',
    component: ManageLicenseListComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManageLicenseRoutingModule {}
