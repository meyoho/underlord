import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { ImportLicenseComponent } from './import-license/component';
import { ManageLicenseListComponent } from './list/component';
import { ManageLicenseRoutingModule } from './routing.module';

@NgModule({
  imports: [SharedModule, ManageLicenseRoutingModule],
  declarations: [ManageLicenseListComponent, ImportLicenseComponent],
  entryComponents: [ImportLicenseComponent],
})
export class ManageLicenseModule {}
