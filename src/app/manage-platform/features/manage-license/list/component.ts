import {
  DATE_FORMAT,
  K8SResourceList,
  K8sPermissionService,
  K8sResourceAction,
  K8sUtilService,
  KubernetesResourceList,
  ResourceListParams,
  TimeService,
  TranslateService,
  catchPromise,
} from '@alauda/common-snippet';
import { DialogService, DialogSize, MessageService } from '@alauda/ui';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import dayjs from 'dayjs';
import { Observable } from 'rxjs';
import { concatMap } from 'rxjs/operators';

import { LicenseApi } from 'app/api/license/api';
import { CfcMeta, License } from 'app/api/license/types';
import { RESOURCE_TYPES } from 'app/utils';

import { ImportLicenseComponent } from '../import-license/component';
const KUBE_PUBLIC_NAMESPACE = 'kube-public';

enum LicenseStatus {
  ACTIVATED = 'activated',
  INVALID = 'invalid',
  EXPIRED = 'expired',
}

const STATUS_ICON_MAPPER = {
  activated: 'check_circle_s',
  invalid: 'basic:minus_circle_s',
  expired: 'exclamation_circle_s',
};
const PLATFORM_INFO_FILE_NAME = 'platform_info.cfc';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ManageLicenseListComponent {
  columns = [
    'products',
    'status',
    'version_type',
    'effective_time',
    'deadline_time',
    'action',
  ];

  cfcInfo: CfcMeta;
  list = new K8SResourceList<License>({
    activatedRoute: this.activatedRoute,
    fetcher: this.fetchResource.bind(this),
  });

  permissions$ = this.k8sPermissionService.isAllowed({
    type: RESOURCE_TYPES.SECRET,
    action: [K8sResourceAction.CREATE, K8sResourceAction.DELETE],
    namespace: KUBE_PUBLIC_NAMESPACE,
  });

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sPermissionService: K8sPermissionService,
    private readonly licenseApi: LicenseApi,
    private readonly dialogService: DialogService,
    private readonly timeService: TimeService,
    private readonly translate: TranslateService,
    private readonly message: MessageService,
    private readonly k8sUtil: K8sUtilService,
  ) {
    this.formatDate = this.formatDate.bind(this);
    this.resolveStatus = this.resolveStatus.bind(this);
    this.licenseApi.getCfcInfo().subscribe(cfc => {
      this.cfcInfo = cfc;
    });
  }

  importLicense() {
    const dialogRef = this.dialogService.open(ImportLicenseComponent, {
      data: { cfcInfo: this.cfcInfo },
      size: DialogSize.Big,
    });
    dialogRef.componentInstance.commit.subscribe((license: License) => {
      dialogRef.close();
      if (license) {
        this.list.create(license);
      }
    });
  }

  fetchResource(
    params: ResourceListParams,
  ): Observable<KubernetesResourceList<License>> {
    return this.licenseApi.getLicenseList(params);
  }

  formatProductsName(name: string) {
    return name.replace(/-/g, ' ');
  }

  deleteLicense(license: License) {
    const { notBefore, notAfter } = license.spec.validity;
    catchPromise(
      this.dialogService.confirm({
        title: this.translate.get('delete_license_title'),
        content: this.translate.get('delete_license_content', {
          notBefore: this.formatDate(notBefore),
          notAfter: this.formatDate(notAfter),
        }),
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
      }),
    )
      .pipe(concatMap(_ => this.licenseApi.deleteLicense(license)))
      .subscribe(_ => {
        this.list.delete(license);
        this.message.success(this.translate.get('license_delete_success'));
      });
  }

  formatDate(date: string) {
    return this.timeService.format(date, DATE_FORMAT);
  }

  nearbyOutdate(date: string) {
    return dayjs(date).diff(dayjs(), 'day') <= 30;
  }

  resolveStatus(license: License) {
    return license.spec.enabled
      ? LicenseStatus.ACTIVATED
      : this.k8sUtil.getAnnotation(license, 'reason', 'license') === 'Invalid'
      ? LicenseStatus.INVALID
      : LicenseStatus.EXPIRED;
  }

  getLicenseStatusIcon(status: LicenseStatus) {
    return STATUS_ICON_MAPPER[status];
  }

  downloadCfcInfo() {
    saveAs(new Blob([this.cfcInfo.cfc]), PLATFORM_INFO_FILE_NAME);
  }
}
