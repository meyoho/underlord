import { TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder, NgForm } from '@angular/forms';
import { EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { LicenseApi } from 'app/api/license/api';
import { CfcMeta, License, LicenseStatus } from 'app/api/license/types';

import { readFileAsString, resolveLicenseError } from '../util';

@Component({
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImportLicenseComponent implements OnInit {
  @ViewChild('form', { static: true })
  ngForm: NgForm;

  @ViewChild('uploadInput', { static: true })
  uploadInput: ElementRef<HTMLInputElement>;

  clientName: string;
  commit = new EventEmitter<License>();
  licenseStatus = LicenseStatus.Activated;
  licenseControl = this.fb.control('');

  constructor(
    private readonly licenseApi: LicenseApi,
    private readonly cdr: ChangeDetectorRef,
    private readonly fb: FormBuilder,
    private readonly notification: NotificationService,
    private readonly translate: TranslateService,
    @Inject(DIALOG_DATA) readonly data: { cfcInfo: CfcMeta },
  ) {
    this.uploadFile = this.uploadFile.bind(this);
    this.clientName = data.cfcInfo.name;
  }

  ngOnInit() {
    // 仅在点击import按钮时校验，改变内容时重置状态
    this.licenseControl.valueChanges.subscribe(_ => {
      this.licenseStatus = LicenseStatus.Activated;
      this.cdr.markForCheck();
    });
  }

  importLicense() {
    this.ngForm.onSubmit(null);
    const licenseKey = this.licenseControl.value;
    if (!this.licenseControl.valid) {
      return;
    }
    this.licenseApi
      .importLicense(licenseKey)
      .pipe(
        catchError(err => {
          this.licenseControl.setErrors({ invalid: true });
          this.licenseStatus = resolveLicenseError(err);
          this.cdr.markForCheck();
          return EMPTY;
        }),
      )
      .subscribe(entity => {
        this.commit.next(entity);
      });
  }

  onUpload(event: Event) {
    const fileInput = event.target as HTMLInputElement;
    const file = fileInput.files[0];
    fileInput.value = null;
    readFileAsString(file).then(
      content => {
        this.licenseControl.setValue(content);
      },
      err => {
        this.notification.error({
          title: this.translate.get(err.message),
        });
      },
    );
  }

  cancel() {
    this.commit.next(null);
  }

  formatError(status: LicenseStatus) {
    return `license_${status}_tip`;
  }

  uploadFile(ev: Event) {
    ev.preventDefault();
    this.uploadInput.nativeElement.click();
  }
}
