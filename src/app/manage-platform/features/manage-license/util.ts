import { Status } from '@alauda/common-snippet';

import { LicenseStatus } from 'app/api/license/types';

function checkFileIsBinary(content: string) {
  return [...content].some(char => char.charCodeAt(0) > 127);
}

export function readFileAsString(file: File): Promise<string> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onloadend = () => {
      const result = reader.result as string;
      if (checkFileIsBinary(reader.result as string)) {
        reject(new Error('unsupported_file_type'));
      } else {
        resolve(result);
      }
    };

    reader.readAsText(file);
  });
}

export function resolveLicenseError(err: Status) {
  const { code, reason } = err;

  if (code === 409) {
    return LicenseStatus.Existed;
  }

  if (code === 400 && reason === 'Expired') {
    return LicenseStatus.Expired;
  }

  return LicenseStatus.Unauthorized;
}
