import {
  K8sApiService,
  K8sUtilService,
  Translation,
} from '@alauda/common-snippet';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { uniqBy } from 'lodash-es';
import { forkJoin } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { RoleTemplate } from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';

import {
  AcpPlatformAdmin,
  AcpPlatformAuditor,
  getFunctionResources,
} from '../utils';
@Component({
  template:
    '<alu-role-form [ngModel]="role$ | async" [managePermission]="true" [translation]="translation"></alu-role-form>',
})
export class ManageRoleComponent {
  role$ = this.activatedRoute.paramMap.pipe(
    switchMap(params => this.getRole(params.get('name'))),
  );

  translation: Translation = {};

  constructor(
    public readonly k8sApiService: K8sApiService,
    private readonly activatedRoute: ActivatedRoute,
    public readonly k8sUtil: K8sUtilService,
  ) {}

  getRole(name: string) {
    return forkJoin([
      this.k8sApiService.getGlobalResource<RoleTemplate>({
        type: RESOURCE_TYPES.ROLE_TEMPLATE,
        name,
      }),
      getFunctionResources.call(this, name),
    ]).pipe(
      map(([role, res]) => {
        this.translation = res.funcsLocal;
        if (name === AcpPlatformAdmin || name === AcpPlatformAuditor) {
          role.spec.rules = [];
        }
        if (role.spec.rules.length > 0) {
          res.funcs.forEach(defaultRule => {
            const find = role.spec.rules.find(
              rule =>
                `${rule.module}_${rule.functionResourceRef}` ===
                `${defaultRule.module}_${defaultRule.functionResourceRef}`,
            );
            if (find) {
              defaultRule.verbs = find.verbs;
            }
          });
        }
        role.spec.rules = res.funcs;
        role.spec.customRules = uniqBy(
          role.spec.customRules,
          rule => `${rule.apiGroup}_${rule.resources[0]}`,
        );
        return role;
      }),
    );
  }
}
