import {
  AsyncDataLoader,
  COMMON_WRITABLE_ACTIONS,
  K8sApiService,
  K8sPermissionService,
  K8sUtilService,
  KubernetesResourceList,
  StringMap,
  Translation,
} from '@alauda/common-snippet';
import { DialogRef, DialogService } from '@alauda/ui';
import { Component, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { AdvanceApi } from 'app/api/advance/api';
import { UpdateBasicInfoDialogComponent } from 'app/manage-platform/features/manage-role/form/update-basic-info/component';
import {
  PREFIXES,
  ROLE_NAME,
  ROLE_TEMPLATE_OFFICIAL,
  USER_EMAIL,
} from 'app/services/k8s-util.service';
import { ConfirmDeleteComponent } from 'app/shared/components/confirm-delete/confirm-delete.component';
import {
  RoleRule,
  RoleRuleGroup,
  RoleTemplate,
  UserBinding,
} from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';

import {
  AcpPlatformAdmin,
  AcpPlatformAuditor,
  FUNCTION_DISPLAY_NAME,
  FunctionsTable,
  MODULE_DISPLAY_NAME,
  customRuleList2Map,
  ruleList2Map,
  sortFunctions,
  verbs2Items,
  verbsMap,
} from '../utils';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class ManageRoleDetailComponent {
  role: RoleTemplate;
  funcs: KubernetesResourceList;
  name: string;
  notOfficial: boolean;
  roleRulesList: KubernetesResourceList<RoleRuleGroup>;
  columns = ['resource_type', 'permission_operation', 'resource_name'];
  deleteDialogRef: DialogRef<ConfirmDeleteComponent>;
  columnsRules = ['function', 'view', 'create', 'update', 'delete'];
  columnsCustomRules = ['resource', 'view', 'create', 'update', 'delete'];
  params$ = this.activatedRoute.params.pipe(
    tap(params => {
      this.name = params.name;
    }),
  );

  dataLoader = new AsyncDataLoader<RoleTemplate>({
    params$: this.params$,
    fetcher: params => this.fetcher(params),
  });

  emails$: Observable<string[]>;
  translation: Translation = {};

  permissions$ = this.k8sPermissionService.isAllowed({
    type: RESOURCE_TYPES.ROLE_TEMPLATE,
    action: COMMON_WRITABLE_ACTIONS,
  });

  constructor(
    private readonly k8sApiService: K8sApiService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sPermissionService: K8sPermissionService,
    private readonly dialogService: DialogService,
    private readonly router: Router,
    public readonly k8sUtil: K8sUtilService,
    private readonly advanceApi: AdvanceApi,
  ) {}

  fetcher(params: StringMap) {
    return forkJoin([
      this.k8sApiService.getGlobalResource<RoleTemplate>({
        type: RESOURCE_TYPES.ROLE_TEMPLATE,
        name: params.name,
      }),
      this.k8sApiService.getGlobalResourceList({
        type: RESOURCE_TYPES.FUNCTION_RESOURCE,
      }),
    ]).pipe(
      tap(([role, funcs]) => {
        this.role = role;
        this.funcs = funcs;
        this.notOfficial =
          this.k8sUtil.getLabel(role, ROLE_TEMPLATE_OFFICIAL, PREFIXES.AUTH) ===
          'false';
        this.genTranslation();
      }),
      map(([role]) => {
        return role;
      }),
    );
  }

  getBindingUserList() {
    const labelSelector = `${this.k8sUtil.normalizeType(
      ROLE_NAME,
      PREFIXES.AUTH,
    )}=${this.name}`;
    return this.k8sApiService
      .getGlobalResourceList({
        type: RESOURCE_TYPES.USER_BINDING,
        queryParams: { labelSelector },
      })
      .pipe(
        map(res => {
          return res.items.map((item: UserBinding) =>
            this.k8sUtil.getAnnotation(item, USER_EMAIL, PREFIXES.AUTH),
          );
        }),
      );
  }

  updateBasicInfo(resource: RoleTemplate) {
    const ref = this.dialogService.open(UpdateBasicInfoDialogComponent, {
      data: {
        resource,
      },
    });
    ref.componentInstance.close.subscribe((res: boolean) => {
      ref.close();
      if (res) {
        this.dataLoader.reload();
      }
    });
  }

  confirmDeleteRole(templateRef: TemplateRef<ConfirmDeleteComponent>) {
    this.emails$ = this.getBindingUserList();
    this.deleteDialogRef = this.dialogService.open(templateRef);
  }

  deleteRole() {
    return this.advanceApi
      .deleteRoleTemplate(this.k8sUtil.getName(this.role))
      .pipe(tap(() => this.backToList()));
  }

  backToList() {
    this.router.navigate(['../..'], {
      relativeTo: this.activatedRoute,
      replaceUrl: true,
    });
  }

  format = (resource: RoleTemplate) => {
    const name = this.k8sUtil.getName(resource);
    let rules: RoleRule[];
    switch (name) {
      case AcpPlatformAdmin:
        rules = this.getFunctionResources([
          ...verbsMap.view,
          ...verbsMap.create,
          ...verbsMap.update,
          ...verbsMap.delete,
        ]);
        break;
      case AcpPlatformAuditor:
        rules = this.getFunctionResources([...verbsMap.view]);
        break;
      default:
        rules = resource.spec.rules;
    }
    let ruleMap: FunctionsTable[] = ruleList2Map(rules);
    if (resource.spec.customRules) {
      ruleMap = ruleMap.concat(customRuleList2Map(resource.spec.customRules));
    }
    ruleMap.forEach(rule => {
      rule.verbsItem = rule.functions.map(func => verbs2Items(func));
    });
    return ruleMap;
  };

  getFunctionResources = (verbs: string[]) => {
    return sortFunctions.call(this, this.funcs.items).map(item => {
      return {
        module: this.k8sUtil.getLabel(
          item,
          'functionresource.module',
          PREFIXES.AUTH,
        ),
        functionResourceRef: this.k8sUtil.getLabel(
          item,
          'functionresource.function',
          PREFIXES.AUTH,
        ),
        verbs: verbs || [],
      };
    });
  };

  formatLocaleKey(key: string) {
    return {
      en: `${key}.en`,
      zh: key,
    };
  }

  genTranslation() {
    this.funcs.items.forEach(item => {
      const module = this.k8sUtil.getLabel(
        item,
        'functionresource.module',
        PREFIXES.AUTH,
      );
      const functionResourceRef = this.k8sUtil.getLabel(
        item,
        'functionresource.function',
        PREFIXES.AUTH,
      );
      if (!this.translation[module]) {
        this.translation[module] = {
          en: this.k8sUtil.getAnnotation(item, `${MODULE_DISPLAY_NAME}.en`),
          zh: this.k8sUtil.getAnnotation(item, MODULE_DISPLAY_NAME),
        };
      }
      if (!this.translation[`${module}_${functionResourceRef}`]) {
        this.translation[`${module}_${functionResourceRef}`] = {
          en: this.k8sUtil.getAnnotation(item, `${FUNCTION_DISPLAY_NAME}.en`),
          zh: this.k8sUtil.getAnnotation(item, FUNCTION_DISPLAY_NAME),
        };
      }
    });
  }
}
