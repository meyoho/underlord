import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateRoleComponent } from 'app/manage-platform/features/manage-role/create/component';
import { ManageRoleDetailComponent } from 'app/manage-platform/features/manage-role/detail/component';
import { ManageRoleListComponent } from 'app/manage-platform/features/manage-role/list/component';
import { ManageRoleComponent } from 'app/manage-platform/features/manage-role/manage-role/component';

const routes: Routes = [
  {
    path: '',
    component: ManageRoleListComponent,
  },
  {
    path: 'detail/:name',
    component: ManageRoleDetailComponent,
  },
  {
    path: 'create',
    component: CreateRoleComponent,
  },
  {
    path: 'permission/:name',
    component: ManageRoleComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManageRoleRoutingModule {}
