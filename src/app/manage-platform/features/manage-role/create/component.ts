import {
  AuthorizationStateService,
  DESCRIPTION,
  DISPLAY_NAME,
  K8sApiService,
  Locale,
  TRUE,
  TranslateService,
  Translation,
  publishRef,
} from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { cloneDeep, merge } from 'lodash-es';
import { Observable, combineLatest } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { Md5 } from 'ts-md5';

import {
  K8sUtilService,
  PREFIXES,
  ROLE_TEMPLATE_LEVEL,
  ROLE_TEMPLATE_OFFICIAL,
} from 'app/services/k8s-util.service';
import {
  AccountInfo,
  RoleCustomRule,
  RoleRule,
  RoleTemplate,
} from 'app/typings';
import { FALSY, RESOURCE_TYPES } from 'app/utils';

import {
  AcpPlatformAdmin,
  AcpPlatformAuditor,
  getFunctionResources,
} from '../utils';

@Component({
  template:
    '<alu-role-form [ngModel]="role$ | async" [copyName]="copyName" [copyDisplayName]="copyDisplayName" [translation]="translation"></alu-role-form>',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateRoleComponent {
  role$: Observable<RoleTemplate>;
  copyName: string;
  copyDisplayName: string;
  translation: Translation = {};

  constructor(
    public readonly k8sUtil: K8sUtilService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly auth: AuthorizationStateService,
    public readonly k8sApiService: K8sApiService,
    private readonly translateService: TranslateService,
  ) {
    this.copyName = this.activatedRoute.snapshot.queryParams.copy_name;
    this.role$ = getFunctionResources.call(this, this.copyName).pipe(
      tap(res => (this.translation = res.funcsLocal)),
      switchMap(res => {
        return this.copyName
          ? this.getRoleTemplate(this.copyName, res.funcs)
          : this.getDefaultRole(res.funcs);
      }),
    );
  }

  getDefaultRole(defaultRules: RoleRule[]) {
    return this.auth.getTokenPayload<AccountInfo>().pipe(
      map(account => {
        let email = '';
        if (account.email) {
          email = Md5.hashStr(account.email).toString().toLowerCase();
        }
        return {
          apiVersion: 'auth.alauda.io/v1beta1',
          kind: 'RoleTemplate',
          spec: {
            rules: defaultRules,
            customRules: [] as RoleCustomRule[],
          },
          metadata: {
            name: '',
            annotations: {
              [this.k8sUtil.normalizeType(DESCRIPTION)]: '',
              [this.k8sUtil.normalizeType(DISPLAY_NAME)]: '',
            },
            labels: {
              [this.k8sUtil.normalizeType(
                ROLE_TEMPLATE_LEVEL,
                PREFIXES.AUTH,
              )]: 'platform',
              [this.k8sUtil.normalizeType(
                ROLE_TEMPLATE_OFFICIAL,
                PREFIXES.AUTH,
              )]: 'false',
              [this.k8sUtil.normalizeType(
                'creator.email',
                PREFIXES.AUTH,
              )]: email,
            },
          },
        };
      }),
    );
  }

  getRoleTemplate(name: string, defaultRules: RoleRule[]) {
    return combineLatest([
      this.k8sApiService.getGlobalResource<RoleTemplate>({
        type: RESOURCE_TYPES.ROLE_TEMPLATE,
        name,
      }),
      this.translateService.locale$,
    ]).pipe(
      map(([originalRole, locale]) => {
        const role = cloneDeep(originalRole);
        if (
          this.k8sUtil.getLabel(role, ROLE_TEMPLATE_OFFICIAL, PREFIXES.AUTH) ===
          TRUE
        ) {
          if (locale === Locale.EN) {
            role.metadata.annotations[
              this.k8sUtil.normalizeType(`${DISPLAY_NAME}`)
            ] = this.k8sUtil.getAnnotation(role, `${DISPLAY_NAME}.en`);
            role.metadata.annotations[
              this.k8sUtil.normalizeType(`${DESCRIPTION}`)
            ] = this.k8sUtil.getAnnotation(role, `${DESCRIPTION}.en`);
          }
          delete role.metadata.annotations[
            this.k8sUtil.normalizeType(`${DISPLAY_NAME}.en`)
          ];
          delete role.metadata.annotations[
            this.k8sUtil.normalizeType(`${DESCRIPTION}.en`)
          ];
        }
        this.copyDisplayName = this.k8sUtil.getDisplayName(role);
        merge(role.metadata, {
          name: `${this.k8sUtil.getName(role)}${this.translateService.get(
            'role_copy_suffix',
          )}`,
          labels: {
            [this.k8sUtil.normalizeType(
              ROLE_TEMPLATE_OFFICIAL,
              PREFIXES.AUTH,
            )]: FALSY,
          },
          annotations: {
            [this.k8sUtil.normalizeType(
              DISPLAY_NAME,
            )]: `${this.k8sUtil.getDisplayName(
              role,
            )}${this.translateService.get('role_copy_suffix')}`,
          },
        });
        delete role.metadata.resourceVersion;
        if (name === AcpPlatformAdmin || name === AcpPlatformAuditor) {
          role.spec.rules = [];
        }
        if (role.spec.rules.length > 0) {
          defaultRules.forEach(defaultRule => {
            const find = role.spec.rules.find(
              rule =>
                `${rule.module}_${rule.functionResourceRef}` ===
                `${defaultRule.module}_${defaultRule.functionResourceRef}`,
            );
            if (find) {
              defaultRule.verbs = find.verbs;
            }
          });
        }
        role.spec.rules = defaultRules;
        return role;
      }),
      publishRef(),
    );
  }
}
