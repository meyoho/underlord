import {
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  ResourceListParams,
  matchLabelsToString,
} from '@alauda/common-snippet';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';

import {
  K8sUtilService,
  PREFIXES,
  ROLE_TEMPLATE_OFFICIAL,
} from 'app/services/k8s-util.service';
import { RoleTemplate } from 'app/typings';
import { FALSE, RESOURCE_TYPES, TRUE } from 'app/utils';

import { compareOfficialRole } from '../utils';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class ManageRoleListComponent {
  columns = [
    'name',
    'type',
    'attribute',
    'description',
    'creator',
    'create_time',
  ];

  firstLoad = true;

  list = new K8SResourceList<RoleTemplate>({
    activatedRoute: this.activatedRoute,
    fetcher: this.fetchResources.bind(this),
  });

  constructor(
    private readonly k8sApiService: K8sApiService,
    private readonly activatedRoute: ActivatedRoute,
    public readonly k8sUtil: K8sUtilService,
    private readonly k8sPermissionService: K8sPermissionService,
  ) {}

  permissions$ = this.k8sPermissionService.isAllowed({
    type: RESOURCE_TYPES.ROLE_TEMPLATE,
    action: K8sResourceAction.CREATE,
  });

  fetchResources(params: ResourceListParams) {
    if (this.firstLoad) {
      return this.k8sApiService
        .getGlobalResourceList({
          type: RESOURCE_TYPES.ROLE_TEMPLATE,
          queryParams: {
            labelSelector: matchLabelsToString({
              [this.k8sUtil.normalizeType(
                ROLE_TEMPLATE_OFFICIAL,
                PREFIXES.AUTH,
              )]: TRUE,
            }),
          },
        })
        .pipe(
          switchMap(officialRole => {
            params.limit = `${+params.limit - officialRole.items.length}`;
            params.labelSelector = matchLabelsToString({
              [this.k8sUtil.normalizeType(
                ROLE_TEMPLATE_OFFICIAL,
                PREFIXES.AUTH,
              )]: FALSE,
            });
            return this.k8sApiService
              .getGlobalResourceList({
                type: RESOURCE_TYPES.ROLE_TEMPLATE,
                queryParams: params,
              })
              .pipe(
                map(res => {
                  officialRole.items.sort(compareOfficialRole.bind(this));
                  res.items = officialRole.items.concat(res.items);
                  this.firstLoad = false;
                  return res;
                }),
              );
          }),
        );
    } else {
      params.labelSelector = matchLabelsToString({
        [this.k8sUtil.normalizeType(
          ROLE_TEMPLATE_OFFICIAL,
          PREFIXES.AUTH,
        )]: FALSE,
      });
      return this.k8sApiService.getGlobalResourceList({
        type: RESOURCE_TYPES.ROLE_TEMPLATE,
        queryParams: params,
      });
    }
  }

  formatLocaleKey(key: string) {
    return {
      en: `${key}.en`,
      zh: key,
    };
  }
}
