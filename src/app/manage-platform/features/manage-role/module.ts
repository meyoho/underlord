import { NgModule } from '@angular/core';

import { CreateRoleComponent } from 'app/manage-platform/features/manage-role/create/component';
import { ManageRoleDetailComponent } from 'app/manage-platform/features/manage-role/detail/component';
import { AddCustomResourceDialogComponent } from 'app/manage-platform/features/manage-role/form/add-custom-resource/component';
import { BasicInfoFormFormComponent } from 'app/manage-platform/features/manage-role/form/basic-info/component';
import { RoleFormComponent } from 'app/manage-platform/features/manage-role/form/component';
import { PermissionSettingFormFormComponent } from 'app/manage-platform/features/manage-role/form/permission-setting/component';
import { UpdateBasicInfoDialogComponent } from 'app/manage-platform/features/manage-role/form/update-basic-info/component';
import { VerbsFormTableComponent } from 'app/manage-platform/features/manage-role/form/verbs-table/component';
import { VerbsFormItemComponent } from 'app/manage-platform/features/manage-role/form/verbs-table/items/component';
import { ManageRoleListComponent } from 'app/manage-platform/features/manage-role/list/component';
import { ManageRoleComponent } from 'app/manage-platform/features/manage-role/manage-role/component';
import { ManageRoleRoutingModule } from 'app/manage-platform/features/manage-role/routing.module';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  imports: [SharedModule, ManageRoleRoutingModule],
  declarations: [
    ManageRoleListComponent,
    ManageRoleDetailComponent,
    CreateRoleComponent,
    RoleFormComponent,
    BasicInfoFormFormComponent,
    PermissionSettingFormFormComponent,
    VerbsFormTableComponent,
    VerbsFormItemComponent,
    AddCustomResourceDialogComponent,
    UpdateBasicInfoDialogComponent,
    ManageRoleComponent,
  ],
  entryComponents: [
    AddCustomResourceDialogComponent,
    UpdateBasicInfoDialogComponent,
  ],
})
export class ManageRoleModule {}
