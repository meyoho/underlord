import { Component, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';

import { RoleCustomRule } from 'app/typings';
import { K8S_API_RESOURCE } from 'app/utils';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class AddCustomResourceDialogComponent {
  namePattern = K8S_API_RESOURCE;
  submitting: boolean;
  resources: string[];
  apiGroupType = '*';
  groupName: string;

  @Output()
  close = new EventEmitter<RoleCustomRule[]>();

  cancel() {
    this.close.next([]);
  }

  add(formRef: NgForm) {
    formRef.onSubmit(null);
    if (formRef.invalid) {
      return;
    }
    let apiGroup = '*';
    if (this.apiGroupType === 'custom') {
      apiGroup = this.groupName;
    }
    const rules = this.resources.map(resource => {
      return {
        apiGroup,
        resources: [resource],
        verbs: [],
      };
    });
    this.close.next(rules);
  }
}
