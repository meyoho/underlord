import { Translation } from '@alauda/common-snippet';
import {
  Component,
  EventEmitter,
  Injector,
  Input,
  Output,
} from '@angular/core';
import { FormArray } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Md5 } from 'ts-md5';

import {
  RoleCustomRule,
  RoleTemplateSpec,
  RoleTemplateSpecForm,
} from 'app/typings';

import {
  customRuleList2Map,
  ruleList2Map,
  ruleMap2CustomList,
  ruleMap2List,
} from '../../utils';

@Component({
  selector: 'alu-role-permission-setting-form',
  styleUrls: ['style.scss'],
  templateUrl: 'template.html',
})
export class PermissionSettingFormFormComponent extends BaseResourceFormGroupComponent {
  @Output()
  removeCustomResourceChange = new EventEmitter<RoleCustomRule>();

  @Input()
  translation: Translation;

  rules: FormArray;
  customRules: FormArray;
  hashStr = Md5.hashStr.bind(Md5);

  constructor(public readonly injector: Injector) {
    super(injector);
  }

  createForm() {
    this.rules = this.fb.array([this.fb.control('')]);
    this.customRules = this.fb.array([this.fb.control('')]);
    return this.fb.group({
      rules: this.rules,
      customRules: this.customRules,
    });
  }

  getDefaultFormModel() {
    return {};
  }

  adaptResourceModel(resource: RoleTemplateSpec): RoleTemplateSpecForm {
    if (resource) {
      const form: RoleTemplateSpecForm = {
        rules: [],
        customRules: [],
      };
      if (resource.rules) {
        form.rules = ruleList2Map(resource.rules);
      }
      if (resource.customRules) {
        form.customRules = customRuleList2Map(resource.customRules);
      }
      return form;
    }
  }

  adaptFormModel(formModel: RoleTemplateSpecForm): RoleTemplateSpec {
    if (formModel) {
      const resource: RoleTemplateSpec = {
        rules: [],
        customRules: [],
      };
      if (formModel.rules) {
        resource.rules = ruleMap2List(formModel.rules);
      }
      if (formModel.customRules) {
        resource.customRules = ruleMap2CustomList(formModel.customRules);
      }
      return resource;
    }
  }
}
