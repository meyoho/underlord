import {
  DESCRIPTION,
  DISPLAY_NAME,
  K8sApiService,
  K8sUtilService,
  StringMap,
} from '@alauda/common-snippet';
import { DIALOG_DATA } from '@alauda/ui';
import { Component, EventEmitter, Inject, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { cloneDeep } from 'lodash-es';

import { RoleTemplate } from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class UpdateBasicInfoDialogComponent {
  @Output()
  close = new EventEmitter<boolean>();

  submitting: boolean;

  description = this.k8sUtil.normalizeType(DESCRIPTION);
  displayName = this.k8sUtil.normalizeType(DISPLAY_NAME);
  annotations: StringMap = {
    [this.description]: '',
    [this.displayName]: '',
  };

  constructor(
    @Inject(DIALOG_DATA)
    public data: {
      resource: RoleTemplate;
    },
    private readonly k8sUtil: K8sUtilService,
    private readonly k8sApiService: K8sApiService,
  ) {
    if (this.data.resource.metadata.annotations) {
      this.annotations = cloneDeep(this.data.resource.metadata.annotations);
    }
  }

  cancel() {
    this.close.next(false);
  }

  update(formRef: NgForm) {
    formRef.onSubmit(null);
    if (formRef.invalid) {
      return;
    }
    this.data.resource.metadata.annotations = cloneDeep(this.annotations);
    this.k8sApiService
      .putGlobalResource({
        type: RESOURCE_TYPES.ROLE_TEMPLATE,
        resource: this.data.resource,
      })
      .subscribe(
        () => {
          this.close.next(true);
        },
        () => {
          this.close.next(false);
        },
      );
  }
}
