import { DESCRIPTION, DISPLAY_NAME } from '@alauda/common-snippet';
import { Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import {
  K8sUtilService,
  PREFIXES,
  ROLE_TEMPLATE_LEVEL,
} from 'app/services/k8s-util.service';
import { K8S_RESOURCE_NAME_BASE } from 'app/utils';

@Component({
  selector: 'alu-role-basic-info-form',
  styleUrls: ['style.scss'],
  templateUrl: 'template.html',
})
export class BasicInfoFormFormComponent extends BaseResourceFormGroupComponent {
  namePattern = K8S_RESOURCE_NAME_BASE;
  description = this.k8sUtil.normalizeType(DESCRIPTION);
  displayName = this.k8sUtil.normalizeType(DISPLAY_NAME);
  level = this.k8sUtil.normalizeType(ROLE_TEMPLATE_LEVEL, PREFIXES.AUTH);

  constructor(
    public readonly injector: Injector,
    private readonly k8sUtil: K8sUtilService,
  ) {
    super(injector);
  }

  createForm() {
    const annotations = this.fb.group({
      [this.description]: [''],
      [this.displayName]: ['', Validators.required],
    });
    const labels = this.fb.group({
      [this.level]: [''],
    });
    return this.fb.group({
      annotations,
      labels,
      name: [
        '',
        [
          Validators.required,
          Validators.pattern(K8S_RESOURCE_NAME_BASE.pattern),
        ],
      ],
    });
  }

  getDefaultFormModel() {
    return {};
  }
}
