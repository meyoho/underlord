import { Translation } from '@alauda/common-snippet';
import {
  Component,
  EventEmitter,
  Injector,
  Input,
  Output,
} from '@angular/core';
import { FormArray } from '@angular/forms';
import { difference } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import {
  VERBS,
  Verb,
  verbsMap,
} from 'app/manage-platform/features/manage-role/utils';
import { FunctionFrom, RoleCustomRule } from 'app/typings';

@Component({
  selector: 'alu-verbs-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class VerbsFormTableComponent extends BaseResourceFormGroupComponent {
  verbs = VERBS;
  functions: FormArray;

  @Input()
  custom: boolean;

  @Input()
  moduleName: string;

  @Output()
  removeResource = new EventEmitter<RoleCustomRule>();

  @Input()
  translation: Translation;

  constructor(public injector: Injector) {
    super(injector);
  }

  createForm() {
    this.functions = this.fb.array([]);
    return this.fb.group({
      module: [''],
      functions: this.functions,
    });
  }

  getDefaultFormModel() {
    return {};
  }

  remove(resource: string) {
    this.removeResource.next({
      apiGroup: this.form.get('module').value,
      resources: [resource],
      verbs: [],
    });
  }

  verbAction(verb: Verb, action: boolean) {
    const fucs = this.functions.value;
    if (action) {
      fucs.forEach((func: FunctionFrom) => {
        func.verbs = func.verbs.concat(verbsMap[verb]);
      });
    } else {
      fucs.forEach((func: FunctionFrom) => {
        func.verbs = difference(func.verbs, verbsMap[verb]);
      });
    }
    this.functions.patchValue(fucs);
  }
}
