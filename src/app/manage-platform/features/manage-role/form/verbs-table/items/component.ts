import { Translation } from '@alauda/common-snippet';
import {
  Component,
  EventEmitter,
  Injector,
  Input,
  Output,
} from '@angular/core';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import {
  VerbsItem,
  items2Verbs,
  verbs2Items,
} from 'app/manage-platform/features/manage-role/utils';
import { FunctionFrom } from 'app/typings';

@Component({
  // tslint:disable-next-line: component-selector
  selector: '[aluVerbsItem]',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class VerbsFormItemComponent extends BaseResourceFormGroupComponent {
  @Input()
  custom: boolean;

  @Input()
  moduleName: string;

  @Input()
  translation: Translation;

  @Output()
  remove = new EventEmitter<string>();

  constructor(public injector: Injector) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      func_name: [''],
      view: [''],
      create: [''],
      update: [''],
      delete: [''],
    });
  }

  getDefaultFormModel() {
    return {};
  }

  adaptResourceModel(resource: FunctionFrom): VerbsItem {
    if (resource) {
      return verbs2Items(resource);
    }
  }

  adaptFormModel(fomrModel: VerbsItem): FunctionFrom {
    if (fomrModel) {
      return items2Verbs(fomrModel);
    }
  }
}
