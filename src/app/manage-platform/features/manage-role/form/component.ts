import {
  DISPLAY_NAME,
  K8sApiService,
  K8sUtilService,
  TranslateService,
  Translation,
} from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import { Location } from '@angular/common';
import { Component, Injector, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { cloneDeep, remove, uniqBy } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { RoleCustomRule, RoleTemplate } from 'app/typings';
import { K8S_RESOURCE_NAME_BASE, RESOURCE_TYPES } from 'app/utils';

import { AddCustomResourceDialogComponent } from './add-custom-resource/component';

enum CREATE_STEPS {
  BASIC = 1,
  ROLES = 2,
}

@Component({
  selector: 'alu-role-form',
  styleUrls: ['style.scss'],
  templateUrl: 'template.html',
})
export class RoleFormComponent extends BaseResourceFormGroupComponent
  implements OnInit {
  @Input()
  managePermission: boolean;

  @Input()
  copyName: string;

  @Input()
  copyDisplayName: string;

  @Input()
  translation: Translation;

  steps = ['basic_info', 'permission_setting'];
  stepsEnum = CREATE_STEPS;
  curStep = 'BASIC';
  namePattern = K8S_RESOURCE_NAME_BASE;
  displayNameLabel = this.k8sUtil.normalizeType(DISPLAY_NAME);

  constructor(
    public readonly injector: Injector,
    private readonly router: Router,
    private readonly location: Location,
    private readonly k8sUtil: K8sUtilService,
    private readonly k8sApiService: K8sApiService,
    private readonly message: MessageService,
    private readonly translateService: TranslateService,
    private readonly dialogService: DialogService,
  ) {
    super(injector);
  }

  ngOnInit() {
    if (this.managePermission) {
      this.curStep = 'ROLES';
    }
  }

  createForm() {
    return this.fb.group({
      apiVersion: [''],
      kind: [''],
      metadata: this.fb.control(''),
      spec: this.fb.control(''),
    });
  }

  getDefaultFormModel() {
    return {};
  }

  next(form: NgForm) {
    form.onSubmit(null);
    if (form.invalid) {
      return;
    }
    this.curStep = 'ROLES';
  }

  addCustomResource(rules: RoleCustomRule[]) {
    const control = this.form.get(['spec']);
    if (control.value.customRules) {
      const uniqRules = uniqBy(
        control.value.customRules.concat(rules) as RoleCustomRule[],
        rule => `${rule.apiGroup}_${rule.resources[0]}`,
      );
      control.value.customRules = uniqRules;
    } else {
      control.value.customRules = rules;
    }
    control.setValue(control.value);
    this.cdr.markForCheck();
  }

  removeCustomResourceChange(rule: RoleCustomRule) {
    const control = this.form.get(['spec']);
    control.value.customRules = control.value.customRules.filter(
      (_rule: RoleCustomRule) => {
        return (
          _rule.apiGroup !== rule.apiGroup ||
          rule.resources[0] !== _rule.resources[0]
        );
      },
    );
    control.setValue(control.value);
    this.cdr.markForCheck();
  }

  addCustomResourceDialog() {
    const ref = this.dialogService.open(AddCustomResourceDialogComponent);
    ref.componentInstance.close.subscribe((res: RoleCustomRule[]) => {
      ref.close();
      this.addCustomResource(res);
    });
  }

  submit(form: NgForm) {
    form.onSubmit(null);
    if (form.invalid) {
      return;
    }
    const resource: RoleTemplate = cloneDeep(form.value);
    remove(resource.spec.rules, rule => !rule.verbs.length);
    remove(resource.spec.customRules, rule => !rule.verbs.length);
    const action = this.managePermission
      ? 'putGlobalResource'
      : 'postGlobalResource';
    this.k8sApiService[action]({
      type: RESOURCE_TYPES.ROLE_TEMPLATE,
      resource,
    }).subscribe(() => {
      this.message.success(
        this.translateService.get(
          this.managePermission ? 'update_successed' : 'create_successed',
        ),
      );
      this.router.navigate([
        'manage-platform',
        'role',
        'detail',
        form.value.metadata.name,
      ]);
    });
  }

  goBack() {
    this.location.back();
  }
}
