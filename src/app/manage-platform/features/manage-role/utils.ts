import {
  K8sApiService,
  K8sUtilService,
  KubernetesResource,
  Translation,
} from '@alauda/common-snippet';
import { map } from 'rxjs/operators';

import { PREFIXES } from 'app/services/k8s-util.service';
import {
  FunctionFrom,
  RoleCustomRule,
  RoleRule,
  RoleRuleForm,
  RoleTemplate,
} from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';

export interface VerbsItem {
  func_name: string;
  view?: boolean;
  create?: boolean;
  update?: boolean;
  delete?: boolean;
}

export const verbsMap = {
  view: ['get', 'list', 'watch'],
  create: ['create'],
  update: ['update', 'patch'],
  delete: ['delete', 'deletecollection'],
};

export const VERBS: string[] = Object.keys(verbsMap);
export type Verb = keyof typeof verbsMap;
export const AcpPlatformAdmin = 'acp-platform-admin';
export const AcpPlatformAuditor = 'acp-platform-auditor';
export const MODULE_DISPLAY_NAME = 'functionresource.module.display-name';
export const FUNCTION_DISPLAY_NAME = 'functionresource.function.display-name';

export interface FunctionsTable extends RoleRuleForm {
  verbsItem?: VerbsItem[];
}

export function ruleList2Map(ruleList: RoleRule[]) {
  const rulesMap: RoleRuleForm[] = [];
  ruleList.forEach(rule => {
    const find = rulesMap.find(_rule => {
      return _rule.module === rule.module;
    });
    const func = {
      name: rule.functionResourceRef,
      verbs: rule.verbs,
    };
    if (find) {
      find.functions.push(func);
    } else {
      rulesMap.push({
        module: rule.module,
        functions: [func],
      });
    }
  });
  return rulesMap;
}

export function customRuleList2Map(ruleList: RoleCustomRule[]) {
  const rulesMap: RoleRuleForm[] = [];
  ruleList.forEach(rule => {
    const find = rulesMap.find(_rule => {
      return _rule.module === rule.apiGroup;
    });
    const func = {
      name: rule.resources[0],
      verbs: rule.verbs,
    };
    if (find) {
      find.functions.push(func);
    } else {
      rulesMap.push({
        module: rule.apiGroup,
        custom: true,
        functions: [func],
      });
    }
  });
  return rulesMap;
}

export function ruleMap2List(ruleMap: RoleRuleForm[]) {
  const ruleList: RoleRule[] = [];
  ruleMap.forEach(rule => {
    rule.functions.forEach(_fuc => {
      ruleList.push({
        module: rule.module,
        functionResourceRef: _fuc.name,
        verbs: _fuc.verbs,
      });
    });
  });
  return ruleList;
}

export function ruleMap2CustomList(ruleMap: RoleRuleForm[]) {
  const ruleList: RoleCustomRule[] = [];
  ruleMap.forEach(rule => {
    rule.functions.forEach(_fuc => {
      ruleList.push({
        apiGroup: rule.module,
        resources: [_fuc.name],
        verbs: _fuc.verbs,
      });
    });
  });
  return ruleList;
}

export function verbs2Items(resource: FunctionFrom) {
  const verbs = resource.verbs || [];
  const vervsObj: VerbsItem = {
    func_name: resource.name,
  };
  if (verbs.includes('*')) {
    VERBS.forEach((verb: Verb) => {
      vervsObj[verb] = true;
    });
  } else {
    VERBS.forEach((verb: Verb) => {
      vervsObj[verb] = verbsMap[verb].every(verb => verbs.includes(verb));
    });
  }
  return vervsObj;
}

export function items2Verbs(formModel: VerbsItem) {
  let verbs: string[] = [];
  VERBS.forEach((verb: Verb) => {
    if (formModel[verb]) {
      verbs = verbs.concat(verbsMap[verb]);
    }
  });
  return {
    name: formModel.func_name,
    verbs,
  };
}

/**
 * 自定义角色编辑权限页面模块和功能排序
 * 模块排序字段 'functionresource.module.id'
 * 功能排序字段 'functionresource.function.id'
 */
export function sortFunctions(
  this: { k8sUtil: K8sUtilService },
  funcs: KubernetesResource[],
) {
  return funcs.sort((a, b) => {
    return (
      +this.k8sUtil.getAnnotation(
        a,
        'functionresource.module.id',
        PREFIXES.AUTH,
      ) *
        100 +
      +this.k8sUtil.getAnnotation(
        a,
        'functionresource.function.id',
        PREFIXES.AUTH,
      ) -
      (+this.k8sUtil.getAnnotation(
        b,
        'functionresource.module.id',
        PREFIXES.AUTH,
      ) *
        100 +
        +this.k8sUtil.getAnnotation(
          b,
          'functionresource.function.id',
          PREFIXES.AUTH,
        ))
    );
  });
}

export function getFunctionResources(
  this: {
    k8sUtil: K8sUtilService;
    k8sApiService: K8sApiService;
  },
  name: string,
) {
  return this.k8sApiService
    .getGlobalResourceList({
      type: RESOURCE_TYPES.FUNCTION_RESOURCE,
    })
    .pipe(
      map(res => {
        let verbs: string[];
        switch (name) {
          case AcpPlatformAdmin:
            verbs = [
              ...verbsMap.view,
              ...verbsMap.create,
              ...verbsMap.update,
              ...verbsMap.delete,
            ];
            break;
          case AcpPlatformAuditor:
            verbs = [...verbsMap.view];
            break;
          default:
            verbs = [];
        }
        const funcsLocal: Translation = {};
        const funcs = sortFunctions.call(this, res.items).map(item => {
          const module = this.k8sUtil.getLabel(
            item,
            'functionresource.module',
            PREFIXES.AUTH,
          );
          const functionResourceRef = this.k8sUtil.getLabel(
            item,
            'functionresource.function',
            PREFIXES.AUTH,
          );
          if (!funcsLocal[module]) {
            funcsLocal[module] = {
              en: this.k8sUtil.getAnnotation(item, `${MODULE_DISPLAY_NAME}.en`),
              zh: this.k8sUtil.getAnnotation(item, MODULE_DISPLAY_NAME),
            };
          }
          if (!funcsLocal[`${module}_${functionResourceRef}`]) {
            funcsLocal[`${module}_${functionResourceRef}`] = {
              en: this.k8sUtil.getAnnotation(
                item,
                `${FUNCTION_DISPLAY_NAME}.en`,
              ),
              zh: this.k8sUtil.getAnnotation(item, FUNCTION_DISPLAY_NAME),
            };
          }
          return {
            module,
            functionResourceRef,
            verbs,
          };
        });
        return {
          funcsLocal,
          funcs,
        };
      }),
    );
}

export function compareOfficialRole(
  this: {
    k8sUtil: K8sUtilService;
  },
  a: RoleTemplate,
  b: RoleTemplate,
) {
  const aId = this.k8sUtil.getAnnotation(a, 'role.id', 'auth');
  const bId = this.k8sUtil.getAnnotation(b, 'role.id', 'auth');
  if (aId && bId) {
    return +aId - +bId;
  }
  if (aId) {
    return -1;
  }
  if (bId) {
    return 1;
  }
  return 0;
}
