import {
  K8SResourceList,
  K8sApiService,
  StringMap,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogService, DialogSize, MessageService } from '@alauda/ui';
import { Component, EventEmitter, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { startWith } from 'rxjs/operators';

import { AdvanceApi } from 'app/api/advance/api';
import {
  K8sUtilService,
  PREFIXES,
  USER_VALID,
} from 'app/services/k8s-util.service';
import { LoadingDialogComponent } from 'app/shared/features/loading-dialog/component';
import { Status, User } from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';

interface UserCheck extends User {
  check?: boolean;
}

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class CleanUpUserDialogComponent {
  submitting: boolean;
  checkAll = false;
  list = new K8SResourceList({
    fetchParams$: new Subject<StringMap>().pipe(startWith(null as StringMap)),
    fetcher: this.fetchResources.bind(this),
  });

  columns = ['checkbox', 'email', 'user_group', 'status'];

  @Output()
  close = new EventEmitter<Status[] | boolean>();

  constructor(
    private readonly k8sApiService: K8sApiService,
    private readonly advanceApi: AdvanceApi,
    private readonly message: MessageService,
    private readonly translateService: TranslateService,
    private readonly dialogService: DialogService,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  fetchResources(params: StringMap) {
    params.labelSelector = `${this.k8sUtil.normalizeType(
      USER_VALID,
      PREFIXES.AUTH,
    )}=false`;
    return this.k8sApiService.getGlobalResourceList({
      type: RESOURCE_TYPES.USER,
      queryParams: params,
    });
  }

  cancel() {
    this.close.next(false);
  }

  getCheckListNames(items: UserCheck[]) {
    return items?.length > 0
      ? items.filter(item => item.check).map(_item => _item.metadata.name)
      : [];
  }

  cleanUp(items: UserCheck[], range = '') {
    const ref = this.dialogService.open(LoadingDialogComponent, {
      size: DialogSize.Small,
      data: {
        title: this.translateService.get('cleaning_please_wait'),
        iconColor: 'info',
        closeConfirmContent: this.translateService.get(
          'clean_user_loading_close_content',
        ),
      },
    });
    ref.componentInstance.close.subscribe((res: boolean) => {
      if (res) {
        ref.close();
        this.close.next(false);
      }
    });
    this.advanceApi.deleteUsers(this.getCheckListNames(items), range).subscribe(
      () => {
        ref.close();
        this.message.success(this.translateService.get('clean_up_success'));
        this.close.next(true);
      },
      () => {
        this.close.next(false);
      },
    );
  }
}
