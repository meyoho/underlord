import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateUserComponent } from 'app/manage-platform/features/manage-user/create/component';
import { ManageUserDetailComponent } from 'app/manage-platform/features/manage-user/detail/component';
import { ManageUserListComponent } from 'app/manage-platform/features/manage-user/list/component';

const routes: Routes = [
  {
    path: '',
    component: ManageUserListComponent,
  },
  {
    path: 'create',
    component: CreateUserComponent,
  },
  {
    path: 'detail/:name',
    component: ManageUserDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManageUserRoutingModule {}
