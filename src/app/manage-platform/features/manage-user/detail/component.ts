import {
  AsyncDataLoader,
  AuthorizationStateService,
  COMMON_WRITABLE_ACTIONS,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  K8sUtilService,
  StringMap,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';
import { Md5 } from 'ts-md5';

import { AdvanceApi } from 'app/api/advance/api';
import { AddRoleDialogComponent } from 'app/manage-platform/features/manage-user/form/add-role/component';
import { ResetPasswordDialogComponent } from 'app/manage-platform/features/manage-user/form/reset-password/component';
import { PREFIXES, USER_EMAIL } from 'app/services/k8s-util.service';
import { UpdateDisplayNameDialogComponent } from 'app/shared/features/update-displayname/component';
import { AccountInfo, User, UserBinding } from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class ManageUserDetailComponent {
  userbindingNameList: string[] = [];
  params$ = this.activatedRoute.params.pipe(
    map(params => ({
      name: params.name,
    })),
  );

  accountEmail$ = this.auth.getTokenPayload<AccountInfo>().pipe(
    filter(res => !!res.email),
    map(res => Md5.hashStr(res.email).toString().toLowerCase()),
  );

  dataLoader = new AsyncDataLoader<{ user: User }>({
    params$: this.params$,
    fetcher: params => this.fetcher(params),
  });

  permissions$ = this.k8sPermissionService.isAllowed({
    type: RESOURCE_TYPES.USER,
    action: COMMON_WRITABLE_ACTIONS,
  });

  permissionsRole$ = this.k8sPermissionService.isAllowed({
    type: RESOURCE_TYPES.USER_BINDING,
    action: [K8sResourceAction.CREATE, K8sResourceAction.DELETE],
  });

  list = new K8SResourceList({
    fetchParams$: this.params$,
    fetcher: this.fetchResources.bind(this),
  });

  constructor(
    private readonly k8sApiService: K8sApiService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sPermissionService: K8sPermissionService,
    private readonly dialogService: DialogService,
    private readonly router: Router,
    public readonly k8sUtil: K8sUtilService,
    private readonly advanceApi: AdvanceApi,
    private readonly translate: TranslateService,
    private readonly auth: AuthorizationStateService,
  ) {}

  fetcher(params: StringMap) {
    return forkJoin([
      this.advanceApi.getUserInfo(params.name),
      this.permissions$,
    ]).pipe(
      map(([user, permissions]) => {
        return {
          user,
          permissions,
        };
      }),
    );
  }

  fetchResources(params: StringMap) {
    const labelSelector = `${this.k8sUtil.normalizeType(
      USER_EMAIL,
      PREFIXES.AUTH,
    )}=${params.name}`;
    return this.k8sApiService
      .getGlobalResourceList<UserBinding>({
        type: RESOURCE_TYPES.USER_BINDING,
        queryParams: { labelSelector },
      })
      .pipe(
        tap(
          res =>
            (this.userbindingNameList = res.items.map(item =>
              this.k8sUtil.getName(item),
            )),
        ),
      );
  }

  updateDisplayName(user: User) {
    const ref = this.dialogService.open(UpdateDisplayNameDialogComponent, {
      data: {
        user,
      },
    });
    ref.componentInstance.close.subscribe((res: boolean) => {
      ref.close();
      if (res) {
        this.dataLoader.reload();
      }
    });
  }

  resetPassword(user: User) {
    const ref = this.dialogService.open(ResetPasswordDialogComponent, {
      data: {
        user,
      },
    });
    ref.componentInstance.close.subscribe(() => {
      ref.close();
    });
  }

  addRole(user: User) {
    const ref = this.dialogService.open(AddRoleDialogComponent, {
      data: {
        userbindingNameList: this.userbindingNameList,
        user,
      },
      size: DialogSize.Big,
    });
    ref.componentInstance.close.subscribe((res: boolean) => {
      ref.close();
      if (res) {
        this.list.reload();
      }
    });
  }

  async deleteUser(user: User) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete_user_confirm_title', {
          name: user.spec.username
            ? `${user.spec.email} (${user.spec.username})`
            : user.spec.email,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
      this.advanceApi.deleteUser(user.metadata.name).subscribe(() => {
        this.router.navigate(['/manage-platform/user']);
      });
    } catch (e) {}
  }

  getDisabledTooltip(user: User) {
    return user.spec.is_admin
      ? 'not_allow_manage_admin_account'
      : 'not_allow_manage_own_account';
  }

  isDisabled(md5: string, user: User) {
    return user.spec.is_admin || md5 === user.metadata.name;
  }
}
