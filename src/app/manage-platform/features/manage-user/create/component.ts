import { ChangeDetectionStrategy, Component } from '@angular/core';

import { User } from 'app/typings';

@Component({
  template: '<alu-user-form [ngModel]="user"></alu-user-form>',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateUserComponent {
  user: User = null;
}
