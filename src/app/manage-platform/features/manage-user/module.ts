import { NgModule } from '@angular/core';

import { CleanUpUserDialogComponent } from 'app/manage-platform/features/manage-user/clean-up-user-dialog/component';
import { CreateUserComponent } from 'app/manage-platform/features/manage-user/create/component';
import { ManageUserDetailComponent } from 'app/manage-platform/features/manage-user/detail/component';
import { AddRoleDialogComponent } from 'app/manage-platform/features/manage-user/form/add-role/component';
import { UserFormComponent } from 'app/manage-platform/features/manage-user/form/component';
import { ResetPasswordDialogComponent } from 'app/manage-platform/features/manage-user/form/reset-password/component';
import { UserRoleFormTableComponent } from 'app/manage-platform/features/manage-user/form/role/component';
import { UserRoleItemComponent } from 'app/manage-platform/features/manage-user/form/role/item/component';
import { ManageUserListComponent } from 'app/manage-platform/features/manage-user/list/component';
import { ManageUserRoutingModule } from 'app/manage-platform/features/manage-user/routing.module';
import { FeaturesModule } from 'app/shared/features/module';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  imports: [SharedModule, FeaturesModule, ManageUserRoutingModule],
  declarations: [
    ManageUserListComponent,
    CleanUpUserDialogComponent,
    CreateUserComponent,
    UserFormComponent,
    UserRoleFormTableComponent,
    UserRoleItemComponent,
    ManageUserDetailComponent,
    AddRoleDialogComponent,
    ResetPasswordDialogComponent,
  ],
  entryComponents: [
    CleanUpUserDialogComponent,
    AddRoleDialogComponent,
    ResetPasswordDialogComponent,
  ],
})
export class ManageUserModule {}
