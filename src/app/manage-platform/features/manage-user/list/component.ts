import {
  AuthorizationStateService,
  COMMON_WRITABLE_ACTIONS,
  K8SResourceList,
  K8sPermissionService,
  StringMap,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService, DialogSize, MessageService } from '@alauda/ui';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { isEqual } from 'lodash-es';
import { Observable } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  first,
  map,
  pluck,
  tap,
} from 'rxjs/operators';
import { Md5 } from 'ts-md5';

import { AdvanceApi } from 'app/api/advance/api';
import { IdpApi } from 'app/api/idp/api';
import { CleanUpUserDialogComponent } from 'app/manage-platform/features/manage-user/clean-up-user-dialog/component';
import { ResetPasswordDialogComponent } from 'app/manage-platform/features/manage-user/form/reset-password/component';
import { LoadingDialogComponent } from 'app/shared/features/loading-dialog/component';
import { AccountInfo, Status, User } from 'app/typings';
import { COMMA_ENTITY, RESOURCE_TYPES } from 'app/utils';

interface SyncUserResult {
  fail: number;
  success: number;
  total: number;
}

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class ManageUserListComponent {
  searchTypeList = ['account', 'display_name', 'user_group'];
  searchType = 'account';
  searchKeyMap: StringMap = {
    account: 'email',
    display_name: 'username',
    user_group: 'group',
  };

  userRoleMap: {
    [key: string]: string[];
  } = {};

  columns = [
    'account',
    'user_group',
    'role',
    'source',
    'status',
    'create_time',
    'action',
  ];

  accountEmail$ = this.auth.getTokenPayload<AccountInfo>().pipe(
    filter(res => !!res.email),
    map(res => Md5.hashStr(res.email).toString().toLowerCase()),
  );

  params$ = this.route.queryParamMap.pipe(
    map(queryParams => ({
      keywords: queryParams.get('keywords') || '',
      search_type: queryParams.get('search_type') || '',
    })),
    distinctUntilChanged((x, y) => {
      if (x.search_type !== y.search_type && !y.keywords) {
        return true;
      }
      return isEqual(x, y);
    }),
    publishRef(),
  );

  keywords$ = this.params$.pipe(
    map(params => params.keywords),
    publishRef(),
  );

  userParams$: Observable<StringMap> = this.params$.pipe(
    map(params => {
      if (!params.keywords) {
        return null;
      }
      let searchKey = 'email';
      if (params.search_type) {
        this.searchType = params.search_type;
        searchKey = this.searchKeyMap[params.search_type];
      }
      return {
        filterBy: `${searchKey},${params.keywords.replace(',', COMMA_ENTITY)}`,
      };
    }),
    publishRef(),
  );

  permissions$ = this.k8sPermissionService.isAllowed({
    type: RESOURCE_TYPES.USER,
    action: COMMON_WRITABLE_ACTIONS,
  });

  list = new K8SResourceList<User>({
    fetchParams$: this.userParams$,
    fetcher: this.fetchResources.bind(this),
  });

  idps$ = this.idpApi.getIdps({}).pipe(pluck('items'));

  constructor(
    private readonly dialogService: DialogService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly translateService: TranslateService,
    private readonly advanceApi: AdvanceApi,
    private readonly k8sPermissionService: K8sPermissionService,
    private readonly messageService: MessageService,
    private readonly auth: AuthorizationStateService,
    private readonly idpApi: IdpApi,
  ) {}

  fetchResources(params: StringMap) {
    return this.advanceApi.filterUsers(params).pipe(
      tap(res => {
        const users = res.items.map(item => item.metadata.name);
        this.advanceApi.getUsersRoles(users.join(',')).subscribe(userRoles => {
          userRoles.items.forEach(
            userRole => (this.userRoleMap[userRole.name] = userRole.roles),
          );
        });
      }),
    );
  }

  search(keywords: string) {
    this.router.navigate([], {
      queryParams: { keywords },
      queryParamsHandling: 'merge',
    });
  }

  searchTypeChange(searchType: string) {
    this.router.navigate([], {
      queryParams: { search_type: searchType },
      queryParamsHandling: 'merge',
    });
  }

  resetPassword(item: User) {
    const dialogRef = this.dialogService.open(ResetPasswordDialogComponent, {
      data: {
        user: item,
      },
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((_res: boolean) => {
        dialogRef.close();
      });
  }

  async deleteUser(item: User) {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('delete_user_confirm_title', {
          name: item.spec.username
            ? `${item.spec.email} (${item.spec.username})`
            : item.spec.email,
        }),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
      this.advanceApi
        .deleteUser(item.metadata.name)
        .subscribe(() => this.list.reload());
    } catch (e) {}
  }

  cleanUpInvalidUser() {
    const dialogRef = this.dialogService.open(CleanUpUserDialogComponent);
    dialogRef.componentInstance.close
      .pipe(first())
      // tslint:disable-next-line: no-identical-functions
      .subscribe((res: Status[]) => {
        if (res) {
          this.list.reload();
        }
        dialogRef.close();
      });
  }

  async syncUser() {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('sync_user_confirm_title'),
        content: this.translateService.get('sync_user_confirm_content'),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
    } catch (e) {
      return;
    }
    const ref = this.dialogService.open(LoadingDialogComponent, {
      size: DialogSize.Small,
      data: {
        title: this.translateService.get('syncing_please_wait'),
        closeConfirmContent: this.translateService.get(
          'sync_loading_close_content',
        ),
      },
    });
    ref.componentInstance.close.subscribe((res: boolean) => {
      if (res) {
        ref.close();
      }
    });
    this.advanceApi.syncUser('all').subscribe(
      res => {
        const message = res.details.causes[0];
        if (message) {
          try {
            const result: SyncUserResult = JSON.parse(message.message);
            if (result.fail) {
              this.messageService.error(
                this.translateService.get('sync_success_part', {
                  fail: result.fail,
                  success: result.success,
                }),
              );
            } else {
              this.messageService.success(
                this.translateService.get('sync_success_total', {
                  total: result.total,
                }),
              );
            }
          } catch (e) {
            this.messageService.success(
              this.translateService.get('sync_success'),
            );
          }
        } else {
          this.messageService.success(
            this.translateService.get('sync_success'),
          );
        }
        ref.close();
        this.list.reload();
      },
      () => {
        ref.close();
      },
    );
  }

  getPlaceHolder(type: string) {
    switch (type) {
      case 'account':
        return 'search_by_account_placeholder';
      case 'display_name':
        return 'search_by_display_name_placeholder';
      case 'user_group':
        return 'search_by_group_placeholder';
    }
  }

  getDisabledTooltip(user: User) {
    return user.spec.is_admin
      ? 'not_allow_manage_admin_account'
      : 'not_allow_manage_own_account';
  }

  isDisabled(md5: string, user: User) {
    return user.spec.is_admin || md5 === user.metadata.name;
  }
}
