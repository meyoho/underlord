import { K8sUtilService, TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, MessageService } from '@alauda/ui';
import { Component, EventEmitter, Inject, Output } from '@angular/core';
import { NgForm } from '@angular/forms';

import { AdvanceApi } from 'app/api/advance/api';
import { User } from 'app/typings';
import { PASSWORD_PATTERN, randomPassword } from 'app/utils';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class ResetPasswordDialogComponent {
  submitting: boolean;
  newPassword: string;
  passwordType = 'random';
  passwordPattern = PASSWORD_PATTERN;

  @Output()
  close = new EventEmitter<boolean>();

  constructor(
    @Inject(DIALOG_DATA) readonly data: { user: User },
    private readonly k8sUtil: K8sUtilService,
    private readonly advanceApi: AdvanceApi,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
  ) {
    this.newPassword = randomPassword(12);
  }

  changePassword(type: string) {
    if (type === 'random') {
      this.newPassword = randomPassword(12);
    } else {
      this.newPassword = '';
    }
  }

  cancel() {
    this.close.next(false);
  }

  getTitle(user: User) {
    return user.spec.username
      ? `${user.spec.email}(${user.spec.username})`
      : user.spec.email;
  }

  reset(form: NgForm) {
    form.onSubmit(null);
    if (form.invalid) {
      return;
    }
    const part: User = {
      spec: {
        password: btoa(this.newPassword),
      },
    };
    this.advanceApi
      .patchUser(this.k8sUtil.getName(this.data.user), part)
      .subscribe(
        () => {
          this.message.success(this.translate.get('reset_password_successed'));
          this.close.next(true);
        },
        () => this.close.next(false),
      );
    this.close.next(true);
  }
}
