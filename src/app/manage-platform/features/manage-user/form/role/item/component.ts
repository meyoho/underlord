import {
  CLUSTER,
  DISPLAY_NAME,
  K8sUtilService,
  KubernetesResourceList,
  Locale,
  TRUE,
  TranslateService,
} from '@alauda/common-snippet';
import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Observable, forkJoin, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { AdvanceApi } from 'app/api/advance/api';
import {
  PREFIXES,
  ROLE_TEMPLATE_LEVEL,
  ROLE_TEMPLATE_OFFICIAL,
} from 'app/services/k8s-util.service';
import { Namespace, Project, RoleGroup, RoleTemplate } from 'app/typings';

export interface ClusterNamespaceGroup {
  cluster: string;
  namespaces: Namespace[];
}

export interface ProjectNamespaceGroups {
  [key: string]: ClusterNamespaceGroup[];
}

@Component({
  // tslint:disable-next-line: component-selector
  selector: '[aluUserRoleItem]',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class UserRoleItemComponent extends BaseResourceFormGroupComponent
  implements OnInit {
  @Input()
  roleGroups: RoleGroup[] = [];

  @Input()
  projects: Project[] = [];

  @Input()
  userEmail: string;

  @Input()
  userbindingNameList: string[] = [];

  @Input()
  projectNamespaceGroups: ProjectNamespaceGroups;

  @Output()
  addProjectNamespaceGroups = new EventEmitter<ProjectNamespaceGroups>();

  @Output()
  remove = new EventEmitter<string>();

  namespaceGroups$: Observable<ClusterNamespaceGroup[]>;
  namespaceGroups: ClusterNamespaceGroup[] = [];
  loading = false;
  continueCreateProject: Project;

  constructor(
    public injector: Injector,
    public readonly k8sUtil: K8sUtilService,
    private readonly translate: TranslateService,
    private readonly advanceApi: AdvanceApi,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    // 继续创建时候需要在init中触发下 valueChange 中的逻辑
    this.roleSelectChange(this.form.get('role').value);
    this.continueCreateProject = this.form.get('project').value;
    if (
      this.continueCreateProject &&
      this.k8sUtil.getLabel(
        this.form.get('role').value,
        ROLE_TEMPLATE_LEVEL,
        PREFIXES.AUTH,
      ) === 'namespace'
    ) {
      this.selectProjectChange(this.continueCreateProject);
    }

    this.form.get('role').valueChanges.subscribe(value => {
      this.form.get('project').setValue('');
      this.form.get('namespace').setValue('');
      this.roleSelectChange(value);
    });

    this.form.get('project').valueChanges.subscribe((project: Project) => {
      !this.continueCreateProject && this.form.get('namespace').setValue('');
      if (
        this.k8sUtil.getLabel(
          this.form.get('role').value,
          ROLE_TEMPLATE_LEVEL,
          PREFIXES.AUTH,
        ) === 'namespace'
      ) {
        this.selectProjectChange(project);
      }
    });
  }

  createForm() {
    return this.fb.group({
      role: [''],
      project: [
        {
          value: '',
          disabled: true,
        },
        Validators.required,
      ],
      namespace: [
        {
          value: '',
          disabled: true,
        },
        Validators.required,
      ],
    });
  }

  getDefaultFormModel() {
    return {};
  }

  roleSelectChange(value: RoleTemplate) {
    const projectCtl = this.form.get('project');
    const namespacetCtl = this.form.get('namespace');
    if (value) {
      const roleType = this.k8sUtil.getLabel(
        value,
        'roletemplate.level',
        'auth',
      );
      switch (roleType) {
        case 'platform':
          projectCtl.disable();
          namespacetCtl.disable();
          break;
        case 'project':
          projectCtl.enable();
          namespacetCtl.disable();
          break;
        case 'namespace':
          projectCtl.enable();
          namespacetCtl.enable();
          break;
      }
    }
  }

  unionDisplayName = (role: RoleTemplate) => {
    let displayName = '';
    const name = this.k8sUtil.getName(role);
    if (
      this.translate.locale === Locale.EN &&
      this.k8sUtil.getLabel(role, ROLE_TEMPLATE_OFFICIAL, PREFIXES.AUTH) ===
        TRUE
    ) {
      displayName = this.k8sUtil.getAnnotation(role, `${DISPLAY_NAME}.en`);
    } else {
      displayName = this.k8sUtil.getDisplayName(role);
    }
    return displayName ? `${name} (${displayName})` : name;
  };

  selectProjectChange(project: Project) {
    if (!project?.spec?.clusters?.length) {
      this.namespaceGroups = [];
      return;
    }
    const projectName = this.k8sUtil.getName(project);
    if (this.projectNamespaceGroups[projectName]) {
      this.namespaceGroups = this.projectNamespaceGroups[projectName];
      return;
    }
    this.loading = true;
    const obs = project.spec.clusters.map(cluster => {
      return this.advanceApi
        .getNamespaces({
          cluster: cluster.name,
          project: projectName,
        })
        .pipe(
          map((res: KubernetesResourceList<Namespace>) => {
            res.items.forEach(
              item =>
                (item.metadata.labels[
                  this.k8sUtil.normalizeType(CLUSTER, PREFIXES.AUTH)
                ] = cluster.name),
            );
            return {
              cluster: cluster.name,
              namespaces: res.items,
            };
          }),
          catchError(() => of({} as ClusterNamespaceGroup)),
        );
    });
    return forkJoin(obs).subscribe(
      (namespaceGroups: ClusterNamespaceGroup[]) => {
        this.addProjectNamespaceGroups.next({
          [projectName]: namespaceGroups,
        });
        this.namespaceGroups = namespaceGroups;
        this.loading = false;
      },
    );
  }
}
