import {
  CLUSTER,
  FALSE,
  K8sApiService,
  K8sUtilService,
  KubernetesResourceList,
  TRUE,
  TranslateService,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

import { compareOfficialRole } from 'app/manage-platform/features/manage-role/utils';
import {
  PREFIXES,
  ROLE_TEMPLATE_LEVEL,
  ROLE_TEMPLATE_OFFICIAL,
} from 'app/services/k8s-util.service';
import { Namespace, Project, RoleGroup, RoleTemplate } from 'app/typings';
import { RESOURCE_TYPES, genUserbindingByFormModel } from 'app/utils';

import { ProjectNamespaceGroups } from './item/component';

interface FormModel {
  role: RoleTemplate;
  project: Project;
  namespace: Namespace;
}
@Component({
  selector: 'alu-user-role-form-table',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserRoleFormTableComponent
  extends BaseResourceFormArrayComponent<FormModel>
  implements OnInit {
  @Input()
  userEmail: string;

  @Input()
  userbindingNameList: string[] = [];

  roleGroup: RoleGroup[] = [];
  projects: Project[] = [];
  projectNamespaceGroups: ProjectNamespaceGroups = {};

  constructor(
    public injector: Injector,
    private readonly k8sApi: K8sApiService,
    public readonly k8sUtil: K8sUtilService,
    private readonly translateService: TranslateService,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    forkJoin([this.getRoleGroup(), this.getAllProject()]).subscribe(
      ([roleGroup, projects]) => {
        this.roleGroup = roleGroup;
        this.projects = projects;
      },
    );
  }

  addRow() {
    this.form.insert(this.form.length, this.getOnFormArrayResizeFn()());
    this.cdr.markForCheck();
  }

  addProjectNamespaceGroups(ob: ProjectNamespaceGroups) {
    Object.assign(this.projectNamespaceGroups, ob);
  }

  remove(index: number) {
    this.form.removeAt(index);
    this.cdr.markForCheck();
  }

  rowBackgroundColorFn(c: FormControl) {
    if (c.invalid) {
      return '#fdeded';
    } else {
      return '';
    }
  }

  getErrorName(formModel: FormModel) {
    if (!formModel.project) {
      return 'project';
    }
    if (!formModel.namespace) {
      return 'namespace';
    }
  }

  getDuplicateName = (formModel: FormModel) => {
    const level = this.k8sUtil.getLabel(
      formModel.role,
      ROLE_TEMPLATE_LEVEL,
      PREFIXES.AUTH,
    );
    if (level === 'platform') {
      return 'role';
    }
    return level;
  };

  duplicateValidator = (c: FormControl) => {
    if (!this.form.value || !this.form.value.length) {
      return null;
    }
    if (c && c.value && c.value.role) {
      const level = this.k8sUtil.getLabel(
        c.value.role,
        ROLE_TEMPLATE_LEVEL,
        PREFIXES.AUTH,
      );
      if (
        this.userbindingNameList.length &&
        this.userbindingNameList.includes(this.genUserbindingName(c.value))
      ) {
        return {
          duplicate: true,
        };
      }
      const index = this.form.controls.indexOf(c);
      const selectRoleName = this.k8sUtil.getName(c.value.role);
      const selectProjectName = this.k8sUtil.getName(c.value.project);
      const find = this.formModel.slice(0, index).find((item: FormModel) => {
        const roleName = this.k8sUtil.getName(item.role);
        const projectName = this.k8sUtil.getName(item.project);
        switch (level) {
          case 'platform':
            return roleName === selectRoleName;
          case 'project':
            return (
              roleName === selectRoleName && projectName === selectProjectName
            );
          case 'namespace':
            return (
              roleName === selectRoleName &&
              projectName === selectProjectName &&
              c.value.namespace &&
              this.k8sUtil.getLabel(item.namespace, CLUSTER, PREFIXES.AUTH) ===
                this.k8sUtil.getLabel(
                  c.value.namespace,
                  CLUSTER,
                  PREFIXES.AUTH,
                ) &&
              this.k8sUtil.getName(item.namespace) ===
                this.k8sUtil.getName(c.value.namespace)
            );
        }
      });
      return find ? { duplicate: true } : null;
    }
    return null;
  };

  getOnFormArrayResizeFn() {
    return () => this.fb.control('', this.duplicateValidator);
  }

  genUserbindingName = (formModel: FormModel) => {
    return this.k8sUtil.getName(
      genUserbindingByFormModel.call(
        this,
        this.userEmail,
        formModel,
        this.translateService.locale,
      ),
    );
  };

  getRoleGroup() {
    return this.k8sApi
      .getGlobalResourceList({
        type: RESOURCE_TYPES.ROLE_TEMPLATE,
      })
      .pipe(
        map((res: KubernetesResourceList<RoleTemplate>) => {
          return [
            {
              group: 'system_role',
              items: res.items
                .filter(
                  item =>
                    this.k8sUtil.getLabel(
                      item,
                      ROLE_TEMPLATE_OFFICIAL,
                      PREFIXES.AUTH,
                    ) === TRUE,
                )
                .sort(compareOfficialRole.bind(this)),
            },
            {
              group: 'custom_role',
              items: res.items.filter(
                item =>
                  this.k8sUtil.getLabel(
                    item,
                    ROLE_TEMPLATE_OFFICIAL,
                    PREFIXES.AUTH,
                  ) === FALSE,
              ),
            },
          ];
        }),
      );
  }

  getAllProject() {
    return this.k8sApi
      .getGlobalResourceList({
        type: RESOURCE_TYPES.PROJECT,
      })
      .pipe(map((res: KubernetesResourceList<Project>) => res.items));
  }
}
