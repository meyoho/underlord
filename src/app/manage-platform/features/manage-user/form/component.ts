import { K8sUtilService, TranslateService } from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import { Location } from '@angular/common';
import { Component, Injector, OnInit } from '@angular/core';
import { NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { forkJoin, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { Md5 } from 'ts-md5';

import { AdvanceApi } from 'app/api/advance/api';
import { User, UserbindingFormModel } from 'app/typings';
import {
  EMAIL_PATTERN,
  PASSWORD_PATTERN,
  USERNAME_PATTERN,
  genUserbindingByFormModel,
  randomPassword,
} from 'app/utils';

interface UserFormModel extends User {
  userbindings?: UserbindingFormModel[];
}

@Component({
  selector: 'alu-user-form',
  styleUrls: ['style.scss'],
  templateUrl: 'template.html',
})
export class UserFormComponent extends BaseResourceFormGroupComponent
  implements OnInit {
  namePattern = USERNAME_PATTERN;
  emailPattern = EMAIL_PATTERN;
  passwordPattern = PASSWORD_PATTERN;
  continueCreate = false;
  passwordType = 'random';
  submitting = false;

  constructor(
    public readonly injector: Injector,
    private readonly router: Router,
    private readonly location: Location,
    public readonly k8sUtil: K8sUtilService,
    private readonly message: MessageService,
    private readonly translateService: TranslateService,
    private readonly advanceApi: AdvanceApi,
  ) {
    super(injector);
  }

  createForm() {
    const spec = this.fb.group({
      email: [
        '',
        [Validators.required, Validators.pattern(this.emailPattern.pattern)],
      ],
      groups: [''],
      username: [''],
      account: [
        '',
        [Validators.required, Validators.pattern(this.namePattern.pattern)],
      ],
      password: [
        '',
        [Validators.required, Validators.pattern(this.passwordPattern.pattern)],
      ],
    });
    return this.fb.group({
      apiVersion: [''],
      kind: [''],
      metadata: [''],
      spec,
      userbindings: [''],
    });
  }

  getDefaultFormModel(): UserFormModel {
    return {
      apiVersion: 'auth.alauda.io/v1',
      kind: 'User',
      metadata: {},
      spec: {
        email: '',
        groups: ['ungrouped'],
        username: '',
        account: '',
        password: randomPassword(12),
      },
      userbindings: [],
    };
  }

  passwordTypeChange(type: string) {
    let password = '';
    if (type === 'random') {
      password = randomPassword(12);
    }
    this.form.get('spec.password').setValue(password);
  }

  submit(form: NgForm) {
    form.onSubmit(null);
    if (form.invalid) {
      return;
    }
    this.submitting = true;
    const userbindings: UserbindingFormModel[] = form.value.userbindings.filter(
      (item: UserbindingFormModel) => !!this.k8sUtil.getName(item.role),
    );
    const originPassword = form.value.spec.password;
    const email = form.value.spec.email;
    form.value.spec.password = btoa(originPassword);
    form.value.metadata.name = Md5.hashStr(email).toString().toLowerCase();
    delete form.value.userbindings;
    this.advanceApi
      .createUser(form.value)
      .pipe(
        switchMap(() =>
          userbindings.length
            ? forkJoin(
                userbindings.map(item =>
                  this.advanceApi
                    .createUserbinding(
                      genUserbindingByFormModel.call(
                        this,
                        email,
                        item,
                        this.translateService.locale,
                      ),
                    )
                    .pipe(catchError(() => null)),
                ),
              )
            : of(null),
        ),
      )
      .subscribe(() => {
        this.message.success(this.translateService.get('create_successed'));
        if (this.continueCreate) {
          const password =
            this.passwordType === 'random'
              ? randomPassword(12)
              : originPassword;
          const groups = form.value.spec.groups;
          form.resetForm();
          this.form.setValue(
            Object.assign(this.getDefaultFormModel(), {
              spec: {
                password,
                groups,
                email: '',
                account: '',
                username: '',
              },
              userbindings,
            }),
          );
        } else {
          this.router.navigate([
            'manage-platform',
            'user',
            'detail',
            form.value.metadata.name,
          ]);
        }
        this.submitting = false;
      });
  }

  goBack() {
    this.location.back();
  }
}
