import { K8sUtilService, TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  Output,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { forkJoin, noop } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AdvanceApi } from 'app/api/advance/api';
import { User, UserbindingFormModel } from 'app/typings';
import { genUserbindingByFormModel } from 'app/utils';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddRoleDialogComponent {
  submitting: boolean;
  userbindings: UserbindingFormModel[] = [];

  @Output()
  close = new EventEmitter<boolean>();

  constructor(
    @Inject(DIALOG_DATA)
    readonly data: {
      userbindingNameList: string[];
      user: User;
    },
    public readonly k8sUtil: K8sUtilService,
    private readonly advanceApi: AdvanceApi,
    private readonly translate: TranslateService,
    private readonly message: MessageService,
  ) {}

  cancel() {
    this.close.next(false);
  }

  update(form: NgForm) {
    form.onSubmit(null);
    if (form.invalid) {
      return;
    }
    const userbindings = this.userbindings.filter(
      item => !!this.k8sUtil.getName(item.role),
    );
    if (!userbindings.length) {
      this.message.success(
        this.translate.get('add_role_successed', { num: 0 }),
      );
      this.close.next(false);
      return;
    }
    forkJoin(
      userbindings.map(item =>
        this.advanceApi
          .createUserbinding(
            genUserbindingByFormModel.call(
              this,
              this.data.user.spec.email,
              item,
              this.translate.locale,
            ),
          )
          .pipe(catchError(() => null)),
      ),
    ).subscribe(
      res =>
        this.message.success(
          this.translate.get('add_role_successed', {
            num: res.filter(item => !!item).length,
          }),
        ),
      noop,
      () => this.close.next(true),
    );
  }
}
