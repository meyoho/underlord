import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AccessPageComponent } from './access-page/component';
import { CreatePageComponent } from './create-page/component';
import { DetailComponent } from './detail/component';
import { NodeDetailComponent } from './detail/node-detail/component';
import { ListComponent } from './list/component';

const clusterRoutes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'list',
    component: ListComponent,
  },
  {
    path: 'create',
    component: CreatePageComponent,
  },
  {
    path: 'access',
    component: AccessPageComponent,
  },
  {
    path: 'detail/:name',
    component: DetailComponent,
  },
  {
    path: 'detail/:clusterName/:nodeName',
    component: NodeDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(clusterRoutes)],
  exports: [RouterModule],
})
export class ClusterRoutingModule {}
