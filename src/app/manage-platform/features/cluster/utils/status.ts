import { StatusType } from '@alauda/ui';

import { Node, Pod } from 'app/typings';

interface Entities {
  [ip: string]: Node;
}

interface NodeStateEntities {
  [clusterName: string]: {
    entities: Entities;
    ips: string[];
  };
}

function getPrivateIp(node: Node) {
  return node.status.addresses.find(addr => addr.type === 'InternalIP').address;
}

function nodes2State(cluster: string, nodes: Node[]): NodeStateEntities {
  const ips = nodes.map(node => getPrivateIp(node));
  const entities: Entities = {};
  nodes.forEach(node => {
    entities[getPrivateIp(node)] = node;
  });
  return {
    [cluster]: {
      ips,
      entities,
    },
  };
}

export { nodes2State };

const reg = /^(\d?\.?\d+)(\w+)*$/;

export function formatMemory(mem: string) {
  const base = parseFloat(mem);
  const res = reg.exec(mem);
  const unit = res[2];
  if (unit) {
    return getMemory(base, unit);
  } else {
    return base;
  }
}

export function getMemory(base: number, unit: string) {
  switch (unit) {
    case 'K':
      return base * 1e3;
    case 'Ki':
      return base * 1024;
    case 'M':
      return base * 1e6;
    case 'Mi':
      return base * 1024 ** 2;
    case 'G':
      return base * 1e9;
    case 'Gi':
      return base * 1024 ** 3;
    case 'T':
      return base * 1e12;
    case 'Ti':
      return base * 1024 ** 4;
    case 'P':
      return base * 1e15;
    case 'Pi':
      return base * 1024 ** 5;
    case 'E':
      return base * 1e18;
    case 'Ei':
      return base * 1024 ** 6;
    default:
      return base;
  }
}

export function formatCPU(cpu: string) {
  const base = parseFloat(cpu);
  const res = reg.exec(cpu);
  const unit = res[2];
  return getCpu(base, unit);
}

export function getCpu(base: number, unit: string) {
  switch (unit) {
    case 'm':
      return base;
      break;
    case 'c':
      return base * 1e3;
      break;
    case 'k':
      return base * 1e6;
      break;
    case 'M':
      return base * 1e9;
      break;
    case 'G':
      return base * 1e12;
      break;
    case 'T':
      return base * 1e15;
      break;
    default:
      return base * 1e3;
      break;
  }
}

// 返回 CPU, Memory。若为''，则解析为无限制
export function getPodResourceLimit(
  pod: Pod,
): { cpu: number | string; memory: number | string } {
  const { containers = [], initContainers = [] } = pod.spec;
  const allContainers = initContainers.concat(...containers);
  let cpuResult = 0;
  let memoryResult = 0;
  allContainers.forEach(container => {
    // 通过强行修改yaml，删除limits的字段，可以让cpu，memory无限制
    let { cpu = '', memory = '' } = container.resources.limits || {};
    // 未限制置为 NaN，无需解析计算
    if (cpu === '' || isNaN(cpuResult)) {
      cpuResult = NaN;
    } else {
      if (!isNaN(+cpu)) {
        cpu += 'c';
      }
      cpuResult += formatCPU(cpu);
    }
    if (memory === '' || isNaN(memoryResult)) {
      memoryResult = NaN;
    } else {
      memoryResult += formatMemory(memory);
    }
  });
  return {
    cpu: isNaN(cpuResult) ? '' : cpuResult,
    memory: isNaN(memoryResult) ? '' : memoryResult,
  };
}

export function getBarStatus(util: number) {
  return [
    {
      scale: util / 100,
      type:
        util > 90
          ? StatusType.Error
          : util > 70
          ? StatusType.Warning
          : StatusType.Success,
    },
    {
      scale: 1 - util / 100,
      type: StatusType.Info,
    },
  ];
}
