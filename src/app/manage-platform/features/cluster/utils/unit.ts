// could be 500m, 0.5, 5k, return number
export const formatNumUnit = (value: string) => {
  let num = parseFloat(value);

  if (num === 0) {
    return 0;
  }

  if (!num) {
    return null;
  }

  switch (value[value.length - 1].toLowerCase()) {
    case 'm':
      num = num / 1000;
      break;
    case 'k':
      num = num * 1000;
      break;
  }

  return num;
};

function formatCommonUnit(value: string): string;
function formatCommonUnit(
  value: string,
  // tslint:disable-next-line: bool-param-default
  fixed?: boolean,
  base?: number,
): string | number;
function formatCommonUnit(value = '', fixed = false, base?: number) {
  value = value.replace(/i$/, '');

  let num = parseFloat(value);
  if (!base) {
    base = 1024;
  }

  if (!num) {
    return 0;
  }

  switch (value[value.length - 1]) {
    case 'M':
      num = num / base;
      break;
    case 'Mi':
      num = num / 1024;
      break;
    case 'm':
      num = num / 1000 / 1024 / 1024 / 1024;
      break;
    case 'G':
      break;
    case 'Gi':
      num = (num / 1000) * 1024;
      break;
    case 'K':
      num = num / base / base;
      break;
    case 'Ki':
    case 'k':
      num = num / 1024 / 1024;
      break;
    case 'T':
      num = num * base;
      break;
    case 'Ti':
      num = num * 1024;
      break;
    case 'P':
      num = num * base * base;
      break;
    case 'Pi':
      num = num * 1024 * 1024;
      break;
    case 'E':
      num = num * base * base * base;
      break;
    case 'Ei':
      num = num * 1024 * 1024 * 1024;
      break;
    default:
      num = num / 1024 / 1024 / 1024;
      break;
  }

  if (!fixed) {
    return num;
  }

  return num.toFixed(2).replace(/0+$/, '').replace(/\.$/, '');
}

function formatK8sCommonUnit(value: string): string;
function formatK8sCommonUnit(value: string, fixed: boolean): string | number;
function formatK8sCommonUnit(value: string, fixed = false) {
  return formatCommonUnit(value, fixed, 1000);
}

export { formatCommonUnit, formatK8sCommonUnit };
