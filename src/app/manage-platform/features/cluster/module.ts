import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { AccessPageComponent } from './access-page/component';
import { CreatePageComponent } from './create-page/component';
import { ClusterNodeFieldsetComponent } from './create-page/fieldset/component';
import { ClusterFormNodeDialogComponent } from './create-page/node-dialog/component';
import { ClusterFormNodeListComponent } from './create-page/node-list/component';
import { DetailModule } from './detail/module';
import { ListComponent } from './list/component';
import { ExecutionProgressDialogComponent } from './list/execution-progress-dialog/component';
import { NodeStatusPieChartComponent } from './list/node-status-pie-chart/component';
import { ClusterRoutingModule } from './routing.module';

@NgModule({
  imports: [SharedModule, ClusterRoutingModule, DetailModule],
  declarations: [
    AccessPageComponent,
    ClusterNodeFieldsetComponent,
    ClusterFormNodeListComponent,
    ClusterFormNodeDialogComponent,
    CreatePageComponent,
    ListComponent,
    NodeStatusPieChartComponent,
    ExecutionProgressDialogComponent,
  ],
  entryComponents: [
    ExecutionProgressDialogComponent,
    ClusterFormNodeDialogComponent,
  ],
})
export class ClusterModule {}
