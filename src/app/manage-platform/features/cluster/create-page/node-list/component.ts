import { TranslateService } from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  forwardRef,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import {
  ClusterFormNodeDialogComponent,
  NodeGroup,
} from '../node-dialog/component';

@Component({
  selector: 'alu-cluster-form-node-list',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ClusterFormNodeListComponent),
      multi: true,
    },
  ],
})
export class ClusterFormNodeListComponent
  implements OnInit, ControlValueAccessor {
  @Input()
  enableGPU: boolean;

  nodes: NodeGroup[] = [];

  onChange: (nodes: NodeGroup[]) => void;

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly dialogService: DialogService,
    public translate: TranslateService,
  ) {}

  ngOnInit() {
    this.cdr.markForCheck();
  }

  add() {
    const dialogRef = this.dialogService.open(ClusterFormNodeDialogComponent, {
      size: DialogSize.Large,
      data: {
        enableGPU: this.enableGPU,
        ips: this.getOtherNodeGroupIps(),
      },
    });
    dialogRef.componentInstance.close.subscribe((el: NodeGroup) => {
      if (el) {
        this.nodes.push(el);
        this.onChange(this.nodes);
      }
      dialogRef.close();
    });
  }

  edit(index: number) {
    const dialogRef = this.dialogService.open(ClusterFormNodeDialogComponent, {
      data: {
        data: this.nodes[index],
        enableGPU: this.enableGPU,
        ips: this.getOtherNodeGroupIps(index),
      },
      size: DialogSize.Large,
    });
    dialogRef.componentInstance.close.subscribe((el: NodeGroup) => {
      if (el) {
        this.nodes[index] = el;
        this.onChange(this.nodes);
      }
      dialogRef.close();
    });
  }

  remove(index: number) {
    this.nodes.splice(index, 1);
    this.onChange(this.nodes);
  }

  writeValue(nodes: NodeGroup[]): void {
    if (nodes && this.nodes !== nodes) {
      this.nodes = nodes;
      this.cdr.markForCheck();
    }
  }

  registerOnChange(fn: (nodes: NodeGroup[]) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(_fn: () => void): void {
    //
  }

  private getOtherNodeGroupIps(index = -1) {
    return this.nodes
      .filter((_el, i) => i !== index)
      .map(el => el.machines.map(curr => curr.ip))
      .reduce((acc, val) => acc.concat(val), []);
  }
}
