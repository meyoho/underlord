import { DIALOG_DATA } from '@alauda/ui';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';

import { PORT_PATTERN } from 'app/utils';

import { NodeModel } from '../fieldset/component';

export interface NodeGroup {
  port: number;
  certMethod: string;
  username: string;
  password: string;
  privateKey: string;
  privateKeyPassword: string;
  machines: NodeModel[];
}

@Component({
  templateUrl: 'template.html',
})
export class ClusterFormNodeDialogComponent implements OnInit {
  @Output()
  close: EventEmitter<boolean | NodeGroup> = new EventEmitter();

  portPattern = PORT_PATTERN;
  showPassword = false;
  formSubmitText = 'add';
  data: NodeGroup = {
    port: undefined,
    certMethod: 'password',
    username: '',
    password: '',
    privateKey: '',
    privateKeyPassword: '',
    machines: [
      {
        type: 'master',
        ip: '',
        displayname: '',
        switch: false,
        deployapp: false,
      },
    ],
  };

  constructor(
    @Inject(DIALOG_DATA)
    public modalData: {
      data: NodeGroup;
      enableGPU: boolean;
      ips: string[];
    },
  ) {}

  ngOnInit() {
    if (this.modalData.data) {
      this.formSubmitText = 'edit';
      this.data = this.modalData.data;
    }
  }

  confirm(ruleForm: NgForm) {
    ruleForm.onSubmit(null);
    if (ruleForm.invalid) {
      return;
    }
    this.data.port = Number(this.data.port);
    this.close.next(this.data);
  }

  cancel() {
    this.close.next(false);
  }
}
