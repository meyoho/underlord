import { StringMap, TranslateService } from '@alauda/common-snippet';
import { Component, Injector, Input, Optional } from '@angular/core';
import {
  AbstractControl,
  ControlContainer,
  FormGroup,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';

import { IP_ADDRESS_PATTERN } from 'app/utils';

export interface NodeModel {
  type: string;
  ip: string;
  displayname: string;
  switch: boolean;
  deployapp: boolean;
}

const ipPatternValidator: ValidatorFn = control => {
  const ipControl = (control as FormGroup).get('ip');
  if (ipControl.invalid && ipControl.errors) {
    if (ipControl.errors.required) {
      return {
        ipRequired: true,
      };
    }
    if (ipControl.errors.pattern) {
      return {
        ipPatternInvalid: true,
      };
    }
  } else {
    return null;
  }
};

@Component({
  selector: 'alu-cluster-node-fieldset',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class ClusterNodeFieldsetComponent extends BaseResourceFormArrayComponent<
  NodeModel
> {
  @Input()
  enableGPU: boolean;

  @Input()
  ips: string[];

  constructor(
    public injector: Injector,
    private readonly translateService: TranslateService,
    @Optional() public readonly controlContainer: ControlContainer,
  ) {
    super(injector);
  }

  private getPreviousIPs(index: number) {
    return this.formModel
      .slice(0, index)
      .map(({ ip }) => ip)
      .filter(ip => !!ip);
  }

  rowBackgroundColorFn = ({ invalid, dirty }: AbstractControl) =>
    dirty && invalid ? '#fdefef' : '';

  getOnFormArrayResizeFn() {
    return () => {
      let type = 'master';
      const arr = this.form.value;
      if (arr?.length) {
        type = arr[arr.length - 1].type;
      }
      return this.fb.group(
        {
          type,
          ip: [
            '',
            [
              Validators.required,
              Validators.pattern(IP_ADDRESS_PATTERN.pattern),
            ],
          ],
          displayname: '',
          switch: '',
          deployapp: '',
        },
        {
          validator: [
            ipPatternValidator,
            (control: AbstractControl) => {
              const index = this.form.controls.indexOf(control);
              const previousKeys = this.getPreviousIPs(index);

              const { ip } = control.value;

              if (previousKeys.includes(ip) || this.ips?.includes(ip)) {
                return {
                  duplicateIP: ip,
                };
              } else {
                return null;
              }
            },
          ],
        },
      );
    };
  }

  getRowErrorMessage = (errors: StringMap) => {
    if (errors.ipRequired) {
      return this.translateService.get('field_is_required', {
        field: this.translateService.get('node_ip'),
      });
    }
    if (errors.ipPatternInvalid) {
      return this.translateService.get(IP_ADDRESS_PATTERN.tip);
    }
    if (errors.duplicateIP) {
      return this.translateService.get('duplicate_ip_error_message');
    }
  };
}
