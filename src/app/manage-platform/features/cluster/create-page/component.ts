import {
  K8sApiService,
  K8sUtilService,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import { Location } from '@angular/common';
import { ChangeDetectorRef, Component, ViewChild } from '@angular/core';
import { NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { set } from 'lodash-es';
import { BehaviorSubject } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { TKECluster, TKEClusterSpecMachine } from 'app/typings';
import {
  CLUSTER_PARAM_KEY_PATTERN,
  CLUSTER_PARAM_VALUE_PATTERN,
  IP_ADDRESS_HOSTNAME_PATTERN,
  K8S_RESOURCE_NAME_BASE,
  PORT_PATTERN,
  RESOURCE_TYPES,
  TKEClusterMeta,
} from 'app/utils';

import { NodeModel } from './fieldset/component';
import { NodeGroup } from './node-dialog/component';

const NVIDIA_DEVICE_ENABLE = 'nvidia-device-enable';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class CreatePageComponent {
  baseRSReg = K8S_RESOURCE_NAME_BASE;
  IP_ADDRESS_HOSTNAME_PATTERN = IP_ADDRESS_HOSTNAME_PATTERN;
  portPattern = PORT_PATTERN;
  submitting: boolean;

  networkType = 'flannel';
  enableGPU: boolean;
  masterNodeNumber = -1;
  nodes: NodeGroup[] = [];
  cluster: TKECluster = {
    apiVersion: TKEClusterMeta.apiVersion,
    kind: TKEClusterMeta.kind,
    metadata: {
      name: '',
    },
    spec: {
      displayName: '',
      type: 'Baremetal',
      clusterCIDR: '',
      serviceCIDR: '',
      features: {
        ipvs: true,
        enableMasterSchedule: true,
        files: [
          {
            src: '/app/provider/baremetal/audit/policy.yaml',
            dst: '/etc/kubernetes/audit/policy.yaml',
          },
          {
            src: '/app/provider/baremetal/install/check-job.sh',
            dst: '/tmp/check-job.sh',
          },
        ],
        hooks: {
          PostInstall: '/tmp/check-job.sh',
        },
        ha: {
          thirdParty: {
            vip: '',
            vport: null,
          },
        },
      },
      properties: {
        maxNodePodNum: 128,
      },
      networkDevice: 'eth0',
      machines: [],
      version: '1.16.9',
      dockerExtraArgs: {},
      kubeletExtraArgs: {},
      controllerManagerExtraArgs: {},
      schedulerExtraArgs: {},
      apiServerExtraArgs: {},
      publicAlternativeNames: [],
    },
  };

  labelValidator = {
    key: [
      Validators.pattern(CLUSTER_PARAM_KEY_PATTERN.pattern),
      Validators.maxLength(253),
    ],
    value: [
      Validators.pattern(CLUSTER_PARAM_VALUE_PATTERN.pattern),
      Validators.maxLength(253),
    ],
  };

  labelErrorMapper = {
    key: {
      pattern: this.translate.get(CLUSTER_PARAM_KEY_PATTERN.tip),
      maxlength: this.translate.get(CLUSTER_PARAM_KEY_PATTERN.tip),
    },
    value: {
      pattern: this.translate.get(CLUSTER_PARAM_VALUE_PATTERN.tip),
      maxlength: this.translate.get(CLUSTER_PARAM_VALUE_PATTERN.tip),
    },
  };

  addressValidator = [
    Validators.pattern(IP_ADDRESS_HOSTNAME_PATTERN.pattern),
    Validators.maxLength(253),
  ];

  addressErrorMapper = {
    pattern: this.translate.get('cluster_endpoint_ip_hostname_tip'),
    maxlength: this.translate.get('cluster_endpoint_ip_hostname_tip'),
  };

  @ViewChild('Form', { static: true })
  form: NgForm;

  nodeMaxNum$ = new BehaviorSubject<number>(null);

  constructor(
    private readonly location: Location,
    private readonly k8sApi: K8sApiService,
    private readonly k8sUtil: K8sUtilService,
    private readonly translate: TranslateService,
    private readonly message: MessageService,
    private readonly dialog: DialogService,
    private readonly router: Router,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  renderNodeMax(clusterCIDR?: string) {
    let nodeMaxNum = null;
    const cidr = clusterCIDR || this.cluster.spec.clusterCIDR;
    const fieldCIDR = cidr.split('/')[1];
    if (fieldCIDR) {
      nodeMaxNum =
        Math.pow(2, 32 - parseInt(fieldCIDR)) /
        this.cluster.spec.properties.maxNodePodNum;
    }
    this.nodeMaxNum$.next(nodeMaxNum);
  }

  enableGPUChange(enable: boolean) {
    if (!enable) {
      delete this.cluster.spec.features.gpuType;
    } else {
      set(this.cluster.spec, 'features.gpuType', 'Virtual');
    }
  }

  confirm() {
    this.form.onSubmit(null);
    this.masterNodeNumber = this.nodes
      .map(el =>
        el.machines.filter(ma => ma.type === 'master').map(curr => curr.ip),
      )
      .reduce((acc, val) => acc.concat(val), []).length;
    if (
      this.form.invalid ||
      this.masterNodeNumber === 0 ||
      this.masterNodeNumber === 2
    ) {
      return;
    }
    if (this.masterNodeNumber > 2 && this.masterNodeNumber % 2 === 0) {
      this.dialog
        .confirm({
          title: this.translate.get('master_node_double_title', {
            count: this.masterNodeNumber,
          }),
          content: this.translate.get('master_node_double_content', {
            name: this.cluster.metadata.name,
            displayname: this.cluster.spec.displayName,
          }),
          confirmText: this.translate.get('back'),
          cancelText: this.translate.get('still_create'),
        })
        .then(() => false)
        .catch(() => {
          this.create();
        });
    } else {
      this.create();
    }
  }

  private create() {
    const masterNodes: TKEClusterSpecMachine[] = [];
    const slaveNodes: Record<string, TKEClusterSpecMachine> = {};
    this.nodes.forEach(node => {
      node.machines.forEach(machine => {
        const nodeToAdd =
          node.certMethod === 'password'
            ? this.generatePasswordNode(node, machine)
            : this.generateSecretNode(node, machine);
        if (machine.type === 'master') {
          // master 节点的 displayName 和 slave 节点一样放在 annotations 中
          const { ip, displayName, ...masterNode } = nodeToAdd;
          masterNodes.push({
            ip,
            ...masterNode,
          });
          slaveNodes[ip] = {
            displayName,
          };
        } else {
          const { ip, ...slaveNode } = nodeToAdd;
          slaveNodes[ip] = slaveNode;
        }
      });
    });
    this.cluster.spec.machines = masterNodes;
    this.cluster.metadata.annotations = {
      ...this.cluster.metadata.annotations,
      [this.k8sUtil.normalizeType('machines')]: JSON.stringify(slaveNodes),
    };
    const network =
      this.networkType === 'flannel' ? 'galaxy' : this.networkType;
    if (network !== 'galaxy') {
      this.cluster.spec.features = {
        ...this.cluster.spec.features,
        skipConditions: ['EnsureGalaxy'],
      };
      if (network !== 'none') {
        this.cluster.metadata.annotations = {
          ...this.cluster.metadata.annotations,
          [this.k8sUtil.normalizeType('network-type')]: network,
        };
      }
    }
    this.cluster.spec.features.ha.thirdParty.vport = Number(
      this.cluster.spec.features.ha.thirdParty.vport,
    );
    this.submitting = true;
    this.k8sApi
      .postGlobalResource({
        type: RESOURCE_TYPES.TKE_CLUSTER,
        resource: this.cluster,
      })
      .pipe(
        finalize(() => {
          this.submitting = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(() => {
        this.message.success(this.translate.get('cluster_create_success'));
        this.submitting = false;
        this.router.navigateByUrl('/manage-platform/cluster/list', {
          replaceUrl: true,
        });
      });
  }

  private generatePasswordNode(node: NodeGroup, machine: NodeModel) {
    const nodeToAdd: TKEClusterSpecMachine = {
      ip: machine.ip,
      displayName: machine.displayname,
      username: node.username,
      password: btoa(node.password),
      port: node.port,
    };
    return this.addNodeLabelTaint(nodeToAdd, machine);
  }

  private generateSecretNode(node: NodeGroup, machine: NodeModel) {
    const nodeToAdd: TKEClusterSpecMachine = {
      ip: machine.ip,
      displayName: machine.displayname,
      username: node.username,
      privateKey: btoa(node.privateKey),
      port: node.port,
    };
    if (node.privateKeyPassword) {
      nodeToAdd.passPhrase = btoa(node.privateKeyPassword);
    }
    return this.addNodeLabelTaint(nodeToAdd, machine);
  }

  private addNodeLabelTaint(
    nodeToAdd: TKEClusterSpecMachine,
    machine: NodeModel,
  ) {
    if (machine.switch && this.enableGPU) {
      nodeToAdd.labels = {};
      nodeToAdd.labels[NVIDIA_DEVICE_ENABLE] = 'enable';
    }
    if (machine.type === 'master' && !machine.deployapp) {
      nodeToAdd.taints = [
        {
          key: 'node-role.kubernetes.io/master',
          value: '',
          effect: 'NoSchedule',
        },
      ];
    }
    return nodeToAdd;
  }

  cancel() {
    this.location.back();
  }
}
