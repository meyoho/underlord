import {
  ApiGatewayService,
  K8sApiService,
  K8sUtilService,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import { Location } from '@angular/common';
import {
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { saveAs } from 'file-saver';
import { BehaviorSubject, Subject } from 'rxjs';
import { catchError, finalize, switchMap, takeUntil } from 'rxjs/operators';

import { ClusterCredential, Secret, TKECluster } from 'app/typings';
import {
  CERT_FILE_PATTERN,
  ClusterCredentialMeta,
  IP_ADDRESS_HOSTNAME_PATTERN,
  K8S_RESOURCE_NAME_BASE,
  PORT_PATTERN,
  RESOURCE_TYPES,
  ResourceType,
  TKEClusterMeta,
} from 'app/utils';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class AccessPageComponent implements OnInit, OnDestroy {
  @ViewChild('form', { static: true })
  ngForm: NgForm;

  baseRSReg = K8S_RESOURCE_NAME_BASE;
  IP_ADDRESS_HOSTNAME_PATTERN = IP_ADDRESS_HOSTNAME_PATTERN;
  portPattern = PORT_PATTERN;
  caFileReg = CERT_FILE_PATTERN;
  manageEndpoint = '';
  manageEndpointPath = this.k8sUtil.normalizeType(
    'k8s-dashboard-endpoint',
    'infrastructure',
  );

  cluster: TKECluster = {
    apiVersion: TKEClusterMeta.apiVersion,
    kind: TKEClusterMeta.kind,
    metadata: {
      name: '',
      labels: {
        'cluster-type': '',
      },
      annotations: {
        [this.manageEndpointPath]: '',
      },
    },
    spec: {
      displayName: '',
      type: 'Imported',
      clusterCredentialRef: {
        name: '',
      },
    },
    status: {
      addresses: [
        {
          host: '',
          port: null,
          type: 'Advertise',
        },
      ],
    },
  };

  clusterCredential: ClusterCredential = {
    apiVersion: ClusterCredentialMeta.apiVersion,
    kind: ClusterCredentialMeta.kind,
    metadata: {
      generateName: 'clustercredential',
    },
    clusterName: '',
    caCert: '',
    token: '',
  };

  clusterType = 'kubernetes';
  caFileBlob: Blob;
  tokenUpdated$ = new BehaviorSubject(null);
  onDestroy$ = new Subject<void>();
  dexHost$ = this.apiGateway.getApiAddress();
  loading: boolean;
  createClusterFailed: boolean;

  constructor(
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    private readonly apiGateway: ApiGatewayService,
    private readonly router: Router,
    private readonly translate: TranslateService,
    private readonly dialogService: DialogService,
    private readonly location: Location,
    private readonly message: MessageService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.k8sApi
      .getGlobalResourceList<Secret>({
        type: RESOURCE_TYPES.SECRET,
        namespaced: true,
      })
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(({ items }) => {
        const dexCaToken = items.find(
          token => token.metadata.name === 'dex.tls',
        );
        if (dexCaToken) {
          this.caFileBlob = new Blob([atob(dexCaToken.data['tls.crt'])], {
            type: 'text/plain;charset=utf-8',
          });
        }
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  clusterTypeChange(type: string) {
    switch (type) {
      case 'kubernetes':
        delete this.cluster.metadata.labels['cluster-type'];
        break;
      case 'openshift':
        this.cluster.metadata.labels['cluster-type'] = 'OCP';
        break;
    }
  }

  downloadCAFile() {
    saveAs(this.caFileBlob, 'apiserver-dex-ca.crt');
  }

  cancel() {
    this.location.back();
  }

  async confirm() {
    this.ngForm.onSubmit(null);
    if (this.ngForm.invalid) {
      return;
    }
    await this.dialogService.confirm({
      title: this.translate.get('access_cluster_confirm', {
        name: this.cluster.spec.displayName,
      }),
      content: this.translate.get('access_cluster_confirm_info'),
      confirmText: this.translate.get('access'),
      cancelText: this.translate.get('cancel'),
    });
    this.loading = true;
    this.createClusterFailed = false;
    this.cdr.markForCheck();
    this.cluster.metadata.annotations[
      this.manageEndpointPath
    ] = this.manageEndpoint;
    this.clusterCredential.clusterName = this.cluster.metadata.name;
    this.cdr.markForCheck();
    this.k8sApi
      .postGlobalResource({
        type: RESOURCE_TYPES.CLUSTER_CREDENTIAL,
        resource: {
          ...this.clusterCredential,
          caCert: btoa(this.clusterCredential.caCert),
        },
      })
      .pipe(
        switchMap(credential => {
          this.cluster.spec.clusterCredentialRef.name =
            credential.metadata.name;
          this.cluster.status.addresses[0].port = Number(
            this.cluster.status.addresses[0].port,
          );
          return this.k8sApi
            .postGlobalResource({
              type: RESOURCE_TYPES.TKE_CLUSTER,
              resource: this.cluster,
            })
            .pipe(
              catchError(() => {
                this.createClusterFailed = true;
                return this.k8sApi.deleteGlobalResource({
                  type: RESOURCE_TYPES.CLUSTER_CREDENTIAL,
                  resource: credential,
                });
              }),
            );
        }),
        finalize(() => {
          this.loading = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(() => {
        if (!this.createClusterFailed) {
          this.message.success(this.translate.get('cluster_access_success'));
          this.router.navigateByUrl('/manage-platform/cluster/list', {
            replaceUrl: true,
          });
        }
      });
  }

  replaceProtocol(protocol: string) {
    return protocol?.replace('http://', 'https://');
  }
}
