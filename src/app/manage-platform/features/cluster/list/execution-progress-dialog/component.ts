import { DIALOG_DATA } from '@alauda/ui';
import { Component, Inject } from '@angular/core';

import {
  GenericStatusColor,
  GenericStatusIcon,
  TKEClusterStatusCondition,
} from 'app/typings';

@Component({
  templateUrl: 'template.html',
})
export class ExecutionProgressDialogComponent {
  constructor(
    @Inject(DIALOG_DATA)
    public data: {
      conditions: TKEClusterStatusCondition;
      name: string;
    },
  ) {}

  columns = ['type', 'status', 'last_detect_time', 'reason'];
  statusColorMapper = {
    success: GenericStatusColor.Running,
    failed: GenericStatusColor.Failed,
  };

  statusIconMapper = {
    success: GenericStatusIcon.Check,
    failed: GenericStatusIcon.Error,
    pending: GenericStatusIcon.Pending,
  };

  getStatus(condition: TKEClusterStatusCondition) {
    switch (condition.status) {
      case 'True':
        return 'success';
      case 'False':
        return 'failed';
      default:
        return 'pending';
    }
  }
}
