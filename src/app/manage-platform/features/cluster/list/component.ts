import {
  AuthorizationStateService,
  COMMON_WRITABLE_ACTIONS,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  K8sUtilService,
  StringMap,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import { DialogRef, DialogService, DialogSize, Sort } from '@alauda/ui';
import {
  ChangeDetectorRef,
  Component,
  Inject,
  TemplateRef,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { combineLatest, from, of } from 'rxjs';
import {
  catchError,
  filter,
  first,
  map,
  mergeMap,
  pluck,
  tap,
  timeout,
  toArray,
} from 'rxjs/operators';

import {
  Metric,
  MetricQueries,
  MetricService,
} from 'app/api/alarm/metric.service';
import { Environments } from 'app/api/envs/types';
import { ENVIRONMENTS } from 'app/services/services.module';
import { ConfirmDeleteComponent } from 'app/shared/components/confirm-delete/confirm-delete.component';
import { ProjectListDialogComponent } from 'app/shared/features/cluster-project-list/component';
import {
  Cluster,
  ClusterFed,
  ClusterStatusColorMapper,
  ClusterStatusEnum,
  ClusterStatusIconMapper,
  Node,
  NodeStatusEnum,
  Project,
  TKECluster,
} from 'app/typings';
import { RESOURCE_TYPES, getNodeStatus, getTKEClusterStatus } from 'app/utils';

import {
  formatCommonUnit,
  formatNumUnit,
  getBarStatus,
  nodes2State,
} from '../utils';

import { ExecutionProgressDialogComponent } from './execution-progress-dialog/component';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class ListComponent {
  resource: TKECluster;
  context = this;
  deleteDialogRef: DialogRef<ConfirmDeleteComponent>;
  fetchParams$ = this.route.queryParams;
  keyword = '';
  items: TKECluster[];
  columns = [
    'name',
    'status',
    'type',
    'count',
    'usage',
    'create_time',
    'action',
  ];

  getBarStatus = getBarStatus;
  getClusterStatus = getTKEClusterStatus;
  ClusterStatusColorMapper = ClusterStatusColorMapper;
  ClusterStatusIconMapper = ClusterStatusIconMapper;
  clusterStatusEnum = ClusterStatusEnum;

  // type 为 avg 使用预设的监控指标 http://confluence.alauda.cn/x/SIeeAQ
  // type 为空使用自定义的 Prometheus 表达式
  metricMap: {
    [key: string]: StringMap;
  } = {
    cluster_cpu_usage_rate: {
      type: 'avg',
      value: 'cluster.cpu.utilization',
    },
    cluster_memory_usage_rate: {
      type: 'avg',
      value: 'cluster.memory.utilization',
    },
    cluster_cpu_total: {
      type: '',
      value:
        'count(avg by(instance,cpu)(node_cpu_seconds_total{job="node-exporter",mode="idle"}))',
    },
    cluster_memory_usage: {
      type: '',
      value: 'cluster_memory_MemUsed',
    },
    cluster_memory_total: {
      type: '',
      value: 'sum(node_memory_MemTotal_bytes)',
    },
    cluster_cpu_request: {
      type: '',
      value: 'sum(node_resource_requests_cpu_cores)',
    },
    cluster_memory_request: {
      type: '',
      value: 'sum(node_resource_requests_memory_bytes)',
    },
    cluster_cpu_limit: {
      type: '',
      value: 'sum(kube_pod_container_resource_limits_cpu_cores{node!=""})',
    },
    cluster_memory_limit: {
      type: '',
      value: 'sum(kube_pod_container_resource_limits_memory_bytes{node!=""})',
    },
  };

  clusterCpuMemMap: {
    [key: string]: StringMap;
  } = {};

  federations$ = this.k8sApi
    .getGlobalResourceList<ClusterFed>({
      type: RESOURCE_TYPES.CLUSTER_FED,
    })
    .pipe(pluck('items'), publishRef());

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.CLUSTER,
    action: COMMON_WRITABLE_ACTIONS,
  });

  list = new K8SResourceList<TKECluster>({
    fetchParams$: this.fetchParams$,
    fetcher: this.fetchResources.bind(this),
  });

  constructor(
    @Inject(ENVIRONMENTS) private readonly env: Environments,
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sApi: K8sApiService,
    private readonly k8sUtil: K8sUtilService,
    private readonly route: ActivatedRoute,
    private readonly metricService: MetricService,
    public readonly authState: AuthorizationStateService,
    private readonly dialogService: DialogService,
    private readonly translateService: TranslateService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  fetchResources() {
    return this.k8sApi
      .getGlobalResourceList<TKECluster>({
        type: RESOURCE_TYPES.TKE_CLUSTER,
      })
      .pipe(
        mergeMap(clusterList =>
          from(clusterList.items).pipe(
            mergeMap(cluster =>
              this.k8sApi
                .getResourceList<Node>({
                  type: RESOURCE_TYPES.NODE,
                  cluster: cluster.metadata.name,
                })
                .pipe(
                  map(res => {
                    const state = nodes2State(cluster.metadata.name, res.items);
                    return state[cluster.metadata.name];
                  }),
                  filter(data => !!data),
                  map(state => state.ips.map(ip => state.entities[ip])),
                  publishRef(),
                  map(nodes => ({
                    ...cluster,
                    nodes,
                    isHighAvailability:
                      nodes.filter(
                        (node: Node) =>
                          'node-role.kubernetes.io/master' in
                          node.metadata.labels,
                      ).length > 1,
                    resourceAmount: nodes.reduce(
                      (accumulator, currentValue) => ({
                        cpu:
                          accumulator.cpu +
                          formatNumUnit(currentValue.status.allocatable.cpu),
                        memory:
                          accumulator.memory +
                          +formatCommonUnit(
                            currentValue.status.allocatable.memory,
                            true,
                          ),
                      }),
                      {
                        cpu: 0,
                        memory: 0,
                      },
                    ),
                  })),
                  timeout(5000),
                  catchError(() =>
                    of({
                      ...cluster,
                    }),
                  ),
                ),
            ),
            toArray(),
            map(items => {
              items.sort((a, b) =>
                a.metadata.name.localeCompare(b.metadata.name),
              );
              return { ...clusterList, items };
            }),
          ),
        ),
        tap(res => {
          this.items = res.items;
          res.items.forEach(cluster => {
            this.queryClusterMetric(cluster.metadata.name);
          });
        }),
      );
  }

  search(keyword: string) {
    this.list.scanItems(() =>
      this.items.filter(
        item =>
          item.metadata.name.includes(keyword) ||
          item.spec.displayName.includes(keyword),
      ),
    );
  }

  sortData(sort: Sort) {
    this.list.scanItems(() => {
      this.items.sort((a, b) => {
        if (sort.active === 'name') {
          return sort.direction === 'desc'
            ? b.metadata.name.localeCompare(a.metadata.name)
            : a.metadata.name.localeCompare(b.metadata.name);
        } else if (sort.active === 'create_time') {
          return sort.direction === 'desc'
            ? b.metadata.creationTimestamp.localeCompare(
                a.metadata.creationTimestamp,
              )
            : a.metadata.creationTimestamp.localeCompare(
                b.metadata.creationTimestamp,
              );
        }
      });
      return this.items;
    });
  }

  abnormalNodes = (nodes: Node[]) => {
    return nodes.filter(this.isAbnormalNode);
  };

  isAbnormalNode = (node: Node) => {
    return [NodeStatusEnum.abnormal].includes(getNodeStatus(node));
  };

  getFederation = (item: TKECluster, federations: ClusterFed[]) => {
    return federations.find(fed =>
      fed.spec.clusters.some(
        cluster => cluster.name === this.k8sUtil.getName(item),
      ),
    );
  };

  viewExecutionProgress(cluster: TKECluster) {
    this.dialogService.open(ExecutionProgressDialogComponent, {
      data: {
        conditions: cluster.status.conditions,
      },
      size: DialogSize.Big,
    });
  }

  private queryClusterMetric(clusterName: string) {
    const queries = Object.keys(this.metricMap).map(metricType => {
      const metricQuery: MetricQueries = {
        aggregator: this.metricMap[metricType].type,
        id: metricType,
      };
      if (this.metricMap[metricType].type === 'avg') {
        metricQuery.labels = [
          {
            type: 'EQUAL',
            name: '__name__',
            value: this.metricMap[metricType].value,
          },
          {
            type: 'EQUAL',
            name: 'kind',
            value: 'Cluster',
          },
        ];
      } else {
        metricQuery.labels = [
          {
            name: '__name__',
            value: 'custom',
          },
          {
            name: 'expr',
            value: this.metricMap[metricType].value,
          },
        ];
      }
      return metricQuery;
    });
    return this.metricService
      .queryMetric(clusterName, {
        time: Math.floor(Date.now() / 1000),
        queries,
      })
      .then((results: Metric[]) => {
        results.forEach(result => {
          if (!this.clusterCpuMemMap[clusterName]) {
            this.clusterCpuMemMap[clusterName] = {};
          }
          const map = this.clusterCpuMemMap[clusterName];
          map[result.metric.__query_id__] = this.parseMetricsResponse(result);
        });
        this.cdr.markForCheck();
      });
  }

  private parseMetricsResponse(metric: Metric) {
    if (metric.metric.__query_id__.includes('usage_rate')) {
      return (metric.value[1] * 100)
        .toFixed(2)
        .replace(/^(\d+\.)(\d+)(e\+\d+)?$/, (_match, p1, p2, p3) => {
          return p1 + p2.slice(0, 2) + (p3 || '');
        });
    }
    if (metric.metric.__query_id__.includes('cpu')) {
      return (+metric.value[1]).toFixed(2);
    }
    if (metric.metric.__query_id__.includes('memory')) {
      return (metric.value[1] / Math.pow(1024, 3)).toFixed(2);
    }
  }

  deleteCluster(
    resource: TKECluster,
    templateRef: TemplateRef<ConfirmDeleteComponent>,
  ) {
    const name = this.k8sUtil.getName(resource);

    this.k8sApi
      .getGlobalResourceList<ClusterFed>({
        type: RESOURCE_TYPES.CLUSTER_FED,
      })
      .subscribe(federations => {
        const found = federations.items.find(i => {
          return i.spec.clusters.some(cluster => cluster.name === name);
        });
        if (found) {
          this.dialogService.confirm({
            title: this.translateService.get(
              'delete_cluser_federation_warning_title',
              {
                name: this.k8sUtil.getUnionDisplayName(resource),
              },
            ),
            content: this.translateService.get(
              'delete_cluser_federation_warning_content',
              {
                name: this.k8sUtil.getUnionDisplayName(found),
              },
            ),
            confirmText: this.translateService.get('got_it'),
            cancelButton: false,
          });
        } else {
          this.deleteClusterCheckProject(resource, templateRef);
        }
      });
  }

  deleteClusterCheckProject(
    resource: TKECluster,
    templateRef: TemplateRef<ConfirmDeleteComponent>,
  ) {
    const name = this.k8sUtil.getName(resource);

    combineLatest([
      this.k8sApi
        .getGlobalResource<Cluster>({
          type: RESOURCE_TYPES.CLUSTER,
          name,
          namespaced: true,
        })
        .pipe(
          catchError(() =>
            of({
              metadata: {
                finalizers: [],
              },
            }),
          ),
        ),
      this.k8sApi.getGlobalResourceList<Project>({
        type: RESOURCE_TYPES.PROJECT,
      }),
    ]).subscribe(([cluster, projectList]) => {
      const {
        metadata: { finalizers },
      } = cluster;
      if (finalizers?.length > 0) {
        const projectItems = projectList.items.filter(item =>
          finalizers.some(
            project =>
              project ===
              `${this.env.LABEL_BASE_DOMAIN}.project.${item.metadata.name}`,
          ),
        );
        this.dialogService
          .open(ProjectListDialogComponent, {
            data: {
              projectItems,
              name,
              unionDisplayName: this.k8sUtil.getUnionDisplayName(cluster),
            },
          })
          .afterClosed()
          .pipe(first())
          .subscribe(res => {
            if (res) {
              this.deleteTKECluster(resource);
            }
          });
      } else {
        this.resource = resource;
        this.deleteDialogRef = this.dialogService.open(templateRef);
      }
    });
  }

  deleteClusterApi() {
    return this.k8sApi
      .deleteGlobalResource<TKECluster>({
        type: RESOURCE_TYPES.TKE_CLUSTER,
        resource: this.resource,
      })
      .pipe(
        tap(() => {
          this.list.reload();
        }),
      );
  }

  deleteTKECluster(resource: TKECluster) {
    this.k8sApi
      .deleteGlobalResource<TKECluster>({
        type: RESOURCE_TYPES.TKE_CLUSTER,
        resource,
      })
      .subscribe(() => {
        this.list.reload();
      });
  }

  getClusterNameDisable(status: string) {
    return (
      status === ClusterStatusEnum.accessing ||
      status === ClusterStatusEnum.deploying ||
      status === ClusterStatusEnum.deleting
    );
  }

  getViewProgressVisible(status: string) {
    return status === ClusterStatusEnum.deploying;
  }

  getDeleteDisable(status: string) {
    return status === ClusterStatusEnum.deleting;
  }
}
