import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
// eslint-disable-next-line node/no-extraneous-import
import { arc, pie } from 'd3-shape';

@Component({
  selector: 'alu-node-status-pie-chart',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NodeStatusPieChartComponent {
  width = 36;
  height = 36;

  @Input()
  total = 0;

  @Input()
  abnormal = 0;

  private readonly valueAccessor: (item: any) => number = (item: any) => item;

  get transform() {
    return 'translate(18, 18)';
  }

  get arcsValue() {
    return [this.abnormal, this.total - this.abnormal];
  }

  get arcs() {
    return pie()
      .startAngle(0)
      .endAngle(Math.PI * 2)
      .value(this.valueAccessor)
      .sort(undefined)(this.arcsValue);
  }

  get AbnormalFractionArcD() {
    const { startAngle, endAngle } = this.arcs[0];
    return arc()({
      startAngle,
      endAngle,
      innerRadius: 14,
      outerRadius: 18,
    });
  }

  get NormalFractionArcD() {
    const { startAngle, endAngle } = this.arcs[1];
    return arc()({
      startAngle,
      endAngle,
      innerRadius: 14,
      outerRadius: 18,
    });
  }

  get NoneFractionArcD() {
    return arc()({
      startAngle: 0,
      endAngle: Math.PI * 2,
      innerRadius: 14,
      outerRadius: 18,
    });
  }
}
