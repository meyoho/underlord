import {
  K8sApiService,
  TranslateService,
  catchPromise,
} from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import { Injectable } from '@angular/core';
import { concatMapTo } from 'rxjs/operators';

import { TerminalService } from 'app/api/terminal/api';
import { Container, Pod, WorkspaceBaseParams } from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';

import { ExecInfo } from './detail/exec/exec-command/component';

@Injectable({ providedIn: 'root' })
export class PodUtilService {
  constructor(
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService,
    private readonly terminal: TerminalService,
  ) {}

  deletePod(pod: Pod, cluster: string) {
    return catchPromise(
      this.dialog.confirm({
        title: this.translate.get('pod_delete_confirm', {
          name: pod.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      }),
    ).pipe(
      concatMapTo(
        this.k8sApi.deleteResource<Pod>({
          type: RESOURCE_TYPES.POD,
          cluster: cluster,
          resource: pod,
        }),
      ),
    );
  }

  handleExecInfo(
    pod: Pod,
    container: Container,
    baseParams: WorkspaceBaseParams,
    data: ExecInfo,
  ) {
    if (!data) {
      return;
    }
    const podName = pod.metadata.name;
    this.terminal.openTerminal({
      // TODO: FIX ME
      // app: this.appName,
      // ctl: this.k8sUtil.getName(this.podController) || '',
      pods: podName,
      selectedPod: podName,
      container: container.name,
      namespace: pod.metadata.namespace,
      cluster: baseParams.cluster,
      user: data.user,
      command: data.command,
    });
  }
}
