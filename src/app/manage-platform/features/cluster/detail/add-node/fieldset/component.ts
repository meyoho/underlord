import { StringMap, TranslateService } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  Optional,
} from '@angular/core';
import {
  AbstractControl,
  ControlContainer,
  FormGroup,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';

import { IP_ADDRESS_PATTERN } from 'app/utils';

interface Resource {
  ip: string;
  display_name: string;
}

const ipPatternValidator: ValidatorFn = control => {
  const ipControl = (control as FormGroup).get('ip');
  if (ipControl.invalid && ipControl.errors) {
    if (ipControl.errors.required) {
      return {
        ipRequired: true,
      };
    }
    if (ipControl.errors.pattern) {
      return {
        ipPatternInvalid: true,
      };
    }
  } else {
    return null;
  }
};

@Component({
  selector: 'alu-node-fieldset',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NodeFieldsetComponent extends BaseResourceFormArrayComponent<
  Resource
> {
  @Input()
  isGPUCluster: boolean;

  rowBackgroundColorFn = ({ invalid, dirty }: AbstractControl) =>
    dirty && invalid ? '#fdefef' : '';

  constructor(
    public injector: Injector,
    private readonly translateService: TranslateService,
    @Optional() public readonly controlContainer: ControlContainer,
  ) {
    super(injector);
  }

  private getPreviousIPs(index: number) {
    return this.formModel
      .slice(0, index)
      .map(({ ip }) => ip)
      .filter(ip => !!ip);
  }

  getOnFormArrayResizeFn() {
    return () =>
      this.fb.group(
        {
          ip: [
            '',
            [
              Validators.required,
              Validators.pattern(IP_ADDRESS_PATTERN.pattern),
            ],
          ],
          display_name: '',
          gpuEnabled: '',
        },
        {
          validator: [
            ipPatternValidator,
            (control: AbstractControl) => {
              const index = this.form.controls.indexOf(control);
              const previousKeys = this.getPreviousIPs(index);

              const { ip } = control.value;

              if (previousKeys.includes(ip)) {
                return {
                  duplicateIP: ip,
                };
              } else {
                return null;
              }
            },
          ],
        },
      );
  }

  getRowErrorMessage = (errors: StringMap) => {
    if (errors.ipRequired) {
      return this.translateService.get('field_is_required', {
        field: this.translateService.get('node_ip'),
      });
    }
    if (errors.ipPatternInvalid) {
      return this.translateService.get(IP_ADDRESS_PATTERN.tip);
    }
    if (errors.duplicateIP) {
      return this.translateService.get('duplicate_ip_error_message');
    }
  };
}
