import {
  K8sApiService,
  K8sUtilService,
  TranslateService,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import { Component, Inject, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { EMPTY, Subject, from } from 'rxjs';
import { catchError, mergeMap, tap } from 'rxjs/operators';

import { TKEMachine } from 'app/typings';
import {
  MachineMeta,
  POSITIVE_INTEGER_PATTERN,
  RESOURCE_TYPES,
} from 'app/utils';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class AddNodeComponent implements OnDestroy {
  private readonly onDestroy$ = new Subject<void>();

  positiveIntegerPattern = POSITIVE_INTEGER_PATTERN;
  submitting = false;
  nodeList: Array<{
    ip: string;
    display_name: string;
    gpuEnabled: boolean;
  }> = [
    {
      ip: '',
      display_name: '',
      gpuEnabled: false,
    },
  ];

  sshPort: string;
  sshUsername: string;
  sshPassword: string;
  sshKey: string;
  sshKeyPass: string;
  usePassword = true;
  showPassword = false;
  isGPUCluster = false;
  nodesSuccess: string[] = [];
  nodesCannotReach: string[] = [];
  nodesDuplicate: string[] = [];
  nodesFailed: Array<{
    ip: string;
    message: string;
  }> = [];

  constructor(
    private readonly dialogRef: DialogRef,
    private readonly k8sApi: K8sApiService,
    private readonly k8sUtil: K8sUtilService,
    private readonly translateService: TranslateService,
    private readonly messageService: MessageService,
    @Inject(DIALOG_DATA)
    private readonly modalData: {
      clusterName: string;
      isGPU: boolean;
      nodes: string[];
    },
  ) {
    this.isGPUCluster = modalData.isGPU;
    if (modalData.nodes?.length) {
      this.nodeList = modalData.nodes.map(ip => ({
        ip,
        display_name: '',
        gpuEnabled: false,
      }));
    }
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  cancel() {
    this.dialogRef.close(true);
  }

  confirm(formRef: NgForm) {
    formRef.onSubmit(null);
    if (formRef.form.invalid) {
      return;
    }
    this.nodesSuccess = [];
    this.nodesCannotReach = [];
    this.nodesDuplicate = [];
    this.nodesFailed = [];
    this.submitting = true;
    from(this.nodeList.map(this.getMachine.bind(this)))
      .pipe(
        mergeMap(spec =>
          this.k8sApi
            .postGlobalResource<TKEMachine>({
              type: RESOURCE_TYPES.MACHINE,
              notifyOnError: false,
              resource: {
                apiVersion: MachineMeta.apiVersion,
                kind: MachineMeta.kind,
                ...spec,
              },
            })
            .pipe(
              tap(res => {
                this.nodeList = this.nodeList.filter(
                  node => node.ip !== res.spec.ip,
                );
                this.nodesSuccess.push(res.spec.ip);
              }),
              catchError(e => {
                this.catchAddError(e, spec.spec.ip);
                this.submitting = false;
                return EMPTY;
              }),
            ),
        ),
      )
      .subscribe({
        complete: () => {
          if (
            !this.nodesCannotReach.length &&
            !this.nodesDuplicate.length &&
            !this.nodesFailed.length
          ) {
            this.dialogRef.close(true);
            this.messageService.success(
              this.translateService.get('add_cluster_node_success_msg'),
            );
          }
        },
      });
  }

  getMachine = ({
    ip,
    display_name,
    gpuEnabled,
  }: {
    ip: string;
    display_name: string;
    gpuEnabled: boolean;
  }) => {
    const spec = {
      clusterName: this.modalData.clusterName,
      port: Number(this.sshPort),
      username: this.sshUsername,
      labels:
        this.isGPUCluster && gpuEnabled
          ? {
              'nvidia-device-enable': 'enable',
            }
          : null,
      type: 'Baremetal',
    };
    const metadata = {
      annotations: {
        [this.k8sUtil.normalizeType('display-name')]: display_name,
      },
    };
    const machine: TKEMachine = this.usePassword
      ? {
          metadata,
          spec: {
            ip,
            password: btoa(this.sshPassword),
            ...spec,
          },
        }
      : {
          metadata,
          spec: {
            ip,
            privateKey: btoa(this.sshKey),
            ...spec,
          },
        };
    if (!this.usePassword && this.sshKeyPass) {
      machine.spec.passPhrase = btoa(this.sshKeyPass);
    }
    return machine;
  };

  catchAddError = (e: any, ip: string) => {
    const causeCannotReach = e.details.causes.find(
      ({ reason }: { reason: string }) => reason === 'FieldValueForbidden',
    );
    if (causeCannotReach) {
      this.nodesCannotReach.push(ip);
    }
    const causeDuplicate = e.details.causes.find(
      ({ reason }: { reason: string }) => reason === 'FieldValueDuplicate',
    );
    if (causeDuplicate) {
      this.nodesDuplicate.push(ip);
    }
    const causeFailed = e.details.causes.find(
      ({ reason }: { reason: string }) =>
        reason !== 'FieldValueForbidden' && reason !== 'FieldValueDuplicate',
    );
    if (causeFailed) {
      this.nodesFailed.push({
        ip,
        message: causeFailed.message,
      });
    }
  };

  getAlertTranslation = (translationKey: string, nodes: string[]) => {
    return this.translateService.get(translationKey, {
      nodes,
    });
  };

  getFailedTranslation = (
    nodes: Array<{
      ip: string;
      message: string;
    }>,
  ) => {
    return nodes
      .map(({ ip, message }) =>
        this.translateService.get('node_failed_detail', {
          ip,
          message,
        }),
      )
      .join('');
  };
}
