import { FALSE } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Pod, PodStatusColorMapper, PodStatusIconMapper } from 'app/typings';
import { getPodAggregatedStatus, getPodStatus } from 'app/utils';

@Component({
  selector: 'alu-pod-status',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodStatusComponent {
  @Input()
  pod: Pod;

  getReadyContainer(pod: Pod) {
    return {
      ready: (pod.status.containerStatuses || []).filter(i => i.ready).length,
      all: pod.spec.containers.length,
    };
  }

  getConditionMessage(pod: Pod) {
    return (pod.status.conditions || [])
      .filter(c => c.status.toLowerCase() === FALSE && c.message)
      .map(c => c.message)
      .join('\n');
  }

  getPodAggregatedStatus = getPodAggregatedStatus;
  getPodStatus = getPodStatus;
  PodStatusColorMapper = PodStatusColorMapper;
  PodStatusIconMapper = PodStatusIconMapper;
}
