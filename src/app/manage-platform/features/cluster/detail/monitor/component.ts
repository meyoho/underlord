/* eslint-disable sonarjs/no-duplicate-string */
// tslint:disable: no-duplicate-string
import { AnyArray, NAME, TranslateService } from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import Highcharts, { SeriesLineOptions } from 'highcharts';
import { BehaviorSubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { v4 } from 'uuid';

import {
  Metric,
  MetricQueries,
  MetricQuery,
  MetricService,
} from 'app/api/alarm/metric.service';
import { BaseTimeSelectComponent } from 'app/maintenance-center/features-shared/alarm/base-time-select.component';
import {
  AGGREGATORS,
  TIME_STAMP_OPTIONS,
  getMetricNumericOptions,
  getMetricPercentOptions,
  parseMetricsResponse,
} from 'app/maintenance-center/features-shared/utils';
import { TRUE } from 'app/utils';

const QUERY_TIME_STAMP_OPTIONS = TIME_STAMP_OPTIONS.concat([
  {
    type: 'custom_time_range',
    offset: 0,
  },
]);

const CHART_NAMES = {
  cpuChartSeries: ['cpu.utilization'],
  memChartSeries: ['memory.utilization'],
  loadChartSeries: ['load.1', 'load.5', 'load.15'],
  apiserverChartSeries: ['kube.apiserver.request.latency.per.verb'],
  etcdNetworkChartSeries: [
    'kube.etcd.client.traffic.in',
    'kube.etcd.client.traffic.out',
  ],
  diskChartSeries: ['disk.utilization'],
  diskRWChartSeries: ['disk.written.bytes', 'disk.read.bytes'],
  networkChartSeries: ['network.receive_bytes', 'network.transmit_bytes'],
  gpuChartSeries: ['gpu.utilization'],
  gpuMemChartSeries: ['gpu.memory.utilization'],
};

const METRICS = [
  'cpu.utilization',
  'memory.utilization',
  'load.1',
  'load.5',
  'load.15',
  'disk.utilization',
  'disk.read.bytes',
  'disk.written.bytes',
  'network.receive_bytes',
  'network.transmit_bytes',
  'kube.apiserver.request.latency.per.verb',
  'kube.etcd.client.traffic.in',
  'kube.etcd.client.traffic.out',
  'gpu.utilization',
  'gpu.memory.utilization',
] as const;

const METRIC_NAMES: Record<
  string,
  {
    chart: string;
    unit: string;
    values?: Array<Array<string | number>>;
    __query_id__?: string;
    metric?: {
      container_name?: string;
      pod_name?: string;
      __query_id__?: string;
      metric_name?: string;
      unit?: string;
      instance?: string;
      device?: string;
    };
  }
> = {
  'cpu.utilization': {
    chart: 'cpuChartSeries',
    unit: '%',
  },
  'memory.utilization': {
    chart: 'memChartSeries',
    unit: '%',
  },
  'kube.apiserver.request.latency.per.verb': {
    chart: 'apiserverChartSeries',
    unit: 'ms',
  },
  'kube.etcd.client.traffic.in': {
    chart: 'etcdNetworkChartSeries',
    unit: 'byte/second',
  },
  'kube.etcd.client.traffic.out': {
    chart: 'etcdNetworkChartSeries',
    unit: 'byte/second',
  },
  'load.1': {
    chart: 'loadChartSeries',
    unit: '',
  },
  'load.5': {
    chart: 'loadChartSeries',
    unit: '',
  },
  'load.15': {
    chart: 'loadChartSeries',
    unit: '',
  },
  'disk.utilization': {
    chart: 'diskChartSeries',
    unit: '%',
  },
  'disk.read.bytes': {
    chart: 'diskRWChartSeries',
    unit: 'byte/second',
  },
  'disk.written.bytes': {
    chart: 'diskRWChartSeries',
    unit: 'byte/second',
  },
  'network.receive_bytes': {
    chart: 'networkChartSeries',
    unit: 'byte/second',
  },
  'network.transmit_bytes': {
    chart: 'networkChartSeries',
    unit: 'byte/second',
  },
  'gpu.utilization': {
    chart: 'gpuChartSeries',
    unit: '%',
  },
  'gpu.memory.utilization': {
    chart: 'gpuMemChartSeries',
    unit: '%',
  },
};

@Component({
  selector: 'alu-cluster-monitor',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClusterMonitorComponent extends BaseTimeSelectComponent
  implements OnInit, OnDestroy {
  @Input()
  privateIp: string;

  @Input()
  hostname: string;

  @Input()
  isOCP: string;

  @Input()
  clusterName: string;

  @Input()
  isGPU = true;

  private readonly onDestroy$ = new Subject<void>();
  private intervalTimer: number;
  private readonly loadMetrics$ = new BehaviorSubject(null);

  aggregators = AGGREGATORS.map(aggregator => {
    return {
      key: aggregator.key,
      name: aggregator.name,
    };
  });

  timeStampOptions = QUERY_TIME_STAMP_OPTIONS.map(option => {
    return {
      ...option,
      name: option.type,
    };
  });

  selectedAggregator = this.aggregators[0].key;
  cpuChartSeries: Highcharts.SeriesLineOptions[];
  memChartSeries: Highcharts.SeriesLineOptions[];
  gpuChartSeries: Highcharts.SeriesLineOptions[];
  gpuMemChartSeries: Highcharts.SeriesLineOptions[];
  apiserverChartSeries: Highcharts.SeriesLineOptions[];
  etcdNetworkChartSeries: Highcharts.SeriesLineOptions[];
  loadChartSeries: Highcharts.SeriesLineOptions[];
  diskChartSeries: Highcharts.SeriesLineOptions[];
  diskRWChartSeries: Highcharts.SeriesLineOptions[];
  networkChartSeries: Highcharts.SeriesLineOptions[];
  metricPercentOptions = getMetricPercentOptions();
  metricNumericOptions = getMetricNumericOptions();
  systemLoadOptions = getMetricNumericOptions();
  metricName: typeof METRIC_NAMES = METRIC_NAMES;
  chartName: typeof CHART_NAMES = CHART_NAMES;
  metrics: Array<typeof METRICS[number]> = METRICS.slice(0, 10);
  diskSeries: Metric[] = [];

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly metricService: MetricService,
    auiMessageService: MessageService,
    translateService: TranslateService,
  ) {
    super(auiMessageService, translateService);
  }

  ngOnInit() {
    this.initTimer();
    Highcharts.setOptions({
      lang: {
        noData: this.translateService.get('no_data'),
      },
    });
    if (!this.privateIp) {
      this.metrics = [
        'cpu.utilization',
        'memory.utilization',
        'kube.apiserver.request.latency.per.verb',
        'kube.etcd.client.traffic.in',
        'kube.etcd.client.traffic.out',
      ];
    }
    if (this.isGPU) {
      this.metrics.push('gpu.utilization', 'gpu.memory.utilization');
    }
    this.metricNumericOptions.tooltip.valueSuffix = this.translateService.get(
      'byte/second',
    );
    this.systemLoadOptions.tooltip.valueDecimals = 2;
    this.queryMetrics();
  }

  ngOnDestroy() {
    window.clearInterval(this.intervalTimer);
    this.onDestroy$.next();
  }

  initTimer() {
    this.intervalTimer = window.setInterval(() => {
      if (!this.chartLoading) {
        if (this.selectedTimeStampOption !== 'custom_time_range') {
          this.resetTimeRange();
        }
        if (this.checkTimeRange()) {
          this.loadCharts();
        }
      }
    }, 60000);
  }

  loadCharts() {
    this.chartLoading = true;
    this.cdr.markForCheck();
    this.loadMetrics$.next({
      aggregator: this.selectedAggregator,
      start: parseInt((this.queryDates.start_time / 1000).toFixed(0), 10),
      end: parseInt((this.queryDates.end_time / 1000).toFixed(0), 10),
    });
  }

  private generateQueries(
    query: MetricQuery,
    aggregator: string,
    metricTypes: AnyArray<keyof typeof METRIC_NAMES>,
  ) {
    const queries = (metricTypes as Array<keyof typeof METRIC_NAMES>).map(
      metricType => {
        const uuid = v4();
        // @ts-ignore
        this.metricName[metricType].__query_id__ = uuid;
        const metricQuery: MetricQueries = {
          aggregator,
          id: uuid,
        };
        if (this.privateIp) {
          let name = '';
          if (
            this.isOCP === TRUE ||
            metricType === 'gpu.utilization' ||
            metricType === 'gpu.memory.utilization'
          ) {
            name = this.hostname;
          } else {
            name = `${this.privateIp}:.*`;
          }
          metricQuery.labels = [
            {
              type: 'EQUAL',
              name: '__name__',
              value: `node.${metricType}`,
            },
            {
              type: 'EQUAL',
              name: NAME,
              value: name,
            },
            {
              type: 'EQUAL',
              name: 'kind',
              value: 'Node',
            },
          ];
        } else {
          metricQuery.labels = [
            {
              type: 'EQUAL',
              name: '__name__',
              value: `cluster.${metricType}`,
            },
            {
              type: 'EQUAL',
              name: 'kind',
              value: 'Cluster',
            },
          ];
        }
        return metricQuery;
      },
    );
    return this.metricService.queryMetrics(this.clusterName, {
      ...query,
      queries,
    });
  }

  private getName(el: Metric) {
    return el.metric?.metric_name;
  }

  // eslint-disable-next-line sonarjs/cognitive-complexity
  private queryMetrics() {
    this.loadMetrics$.pipe(takeUntil(this.onDestroy$)).subscribe(args => {
      if (args === null) {
        return;
      }
      // http://jira.alaudatech.com/browse/DEV-12815
      let start = 0;
      if (args.end - args.start < 900) {
        this.step = 30;
        start = args.start;
      } else {
        this.step = parseInt(((args.end - args.start) / 30).toFixed(0), 10);
        start = args.start + this.step;
      }
      this.endTime = args.end;
      const query = {
        start,
        end: args.end,
        step: this.step,
      };
      this.generateQueries(query, args.aggregator, this.metrics)
        .then((results: Metric[]) => {
          this.parseMetrics(results);
        })
        .catch(error => {
          throw error;
        })
        .finally(() => {
          this.chartLoading = false;
          this.cdr.markForCheck();
        });
    });

    this.resetTimeRange();
    this.loadCharts();
  }

  // eslint-disable-next-line sonarjs/cognitive-complexity
  private parseMetrics(results: Metric[]) {
    let metricPrefix = 'cluster';
    if (this.privateIp) {
      metricPrefix = 'node';
    }
    /*
     * 对于一个metric_name，results中可能包含一个或多个__query_id__与之对应的结果
     * 下面是把__query_id__相同的results合并到一个数组并保存在value['values']中
     * 主机监控的磁盘利用率单独取值和赋值给图表的 series
     */
    this.diskSeries = [];
    Object.entries(this.metricName).forEach(([key, value]) => {
      value.values = [];
      results.forEach(result => {
        if (result.metric.__query_id__ === value.__query_id__) {
          value.metric = {
            ...result.metric,
            metric_name: `${metricPrefix}.${key}`,
            unit: value.unit,
          };
          value.values = value.values.concat(result.values);
          if (key === 'disk.utilization') {
            this.diskSeries.push({
              metric: value.metric,
              values: result.values,
            });
          }
        }
      });
      /*
       * value['values']是包含多个Metric的数组
       * 一个Metric是一个包含两个元素的数组：[1552626688, "1.6875"]
       * 第一个元素表示时间，第二个元素表示数值
       * 下面是对Metric值按照时间排序，再去除时间相同的Metric
       */
      value.values.sort((a: number[], b: number[]) => {
        return a[0] - b[0];
      });
      value.values = value.values.filter((item: number[], pos: number, ary) => {
        return !pos || item[0] !== ary[pos - 1][0];
      });
    });
    Object.entries(this.chartName).forEach(([key, value]) => {
      if (key === 'diskChartSeries') {
        this.diskChartSeries = parseMetricsResponse(
          this.diskSeries,
          (el: Metric) => {
            return el.metric.instance + el.metric.device;
          },
          this.endTime,
          this.step,
        ) as SeriesLineOptions[];
      } else if (
        value.some(el => this.metrics.includes(el as typeof METRICS[number]))
      ) {
        this[key as keyof typeof CHART_NAMES] = parseMetricsResponse(
          value.map(metric => this.metricName[metric]),
          this.getName,
          this.endTime,
          this.step,
        ) as SeriesLineOptions[];
      }
    });
  }
}
