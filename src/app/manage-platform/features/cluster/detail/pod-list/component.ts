import {
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  KubernetesResource,
  NAME,
  NAMESPACE,
  ObservableInput,
  StringMap,
  TranslateService,
  isAllowed,
  matchLabelsToString,
  publishRef,
  viewActions,
  yamlReadOptions,
} from '@alauda/common-snippet';
import { DialogService, DialogSize, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  TemplateRef,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { safeDump } from 'js-yaml';
import { get, isEqual } from 'lodash-es';
import { Observable, combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  first,
  map,
  startWith,
  switchMap,
} from 'rxjs/operators';

import { TerminalService } from 'app/api/terminal/api';
import { CodeDisplayDialogComponent } from 'app/shared/components/code-display-dialog/component';
import {
  Container,
  ContainerStatus,
  Pod,
  PodStatus,
  WorkspaceBaseParams,
  WorkspaceListParams,
  getResourceYaml,
} from 'app/typings';
import { ACTION, RESOURCE_TYPES, STATUS, getPodStatus } from 'app/utils';

import { PodUtilService } from '../../util.service';
import { getPodResourceLimit } from '../../utils';
import {
  ExecCommandDialogComponent,
  ExecCommandDialogParams,
} from '../exec/exec-command/component';

@Component({
  selector: 'alu-pod-list',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodListComponent implements OnInit {
  @Input()
  baseParams: WorkspaceBaseParams;

  @ObservableInput(true)
  private readonly baseParams$: Observable<WorkspaceBaseParams>;

  @Input() matchFields: StringMap;
  @ObservableInput(true)
  private readonly matchFields$: Observable<StringMap>;

  private readonly fieldSelector$ = this.matchFields$.pipe(
    filter(o => !!o),
    distinctUntilChanged(isEqual),
    map(matchFields => matchLabelsToString(matchFields)),
  );

  private readonly fetchParams$ = combineLatest([
    this.baseParams$,
    this.fieldSelector$.pipe(startWith('')),
    this.activatedRoute.queryParams,
  ]).pipe(
    map(([baseParams, fieldSelector, queryParams]) => ({
      ...baseParams,
      fieldSelector,
      ...queryParams,
    })),
  );

  columns: string[] = [
    NAME,
    STATUS,
    'resource_limit',
    'restart_count',
    NAMESPACE,
    'pod_ip',
    'create_time',
    ACTION,
  ];

  list = new K8SResourceList<Pod>({
    fetchParams$: this.fetchParams$,
    fetcher: this.fetchPodResourceList.bind(this),
  });

  getPodStatus = getPodStatus;
  editorActions = viewActions;
  editorOptions = yamlReadOptions;
  yamlInputValue = '';

  permissions$ = this.baseParams$.pipe(
    switchMap(baseParams => {
      return this.k8sPermission
        .getAccess({
          type: RESOURCE_TYPES.POD,
          action: K8sResourceAction.DELETE,
          cluster: baseParams.cluster,
          namespace: baseParams.namespace,
        })
        .pipe(isAllowed());
    }),
    publishRef(),
  );

  execPermissions$: Observable<Record<string, boolean>>;

  constructor(
    private readonly dialog: DialogService,
    private readonly message: MessageService,
    private readonly k8sApi: K8sApiService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly terminal: TerminalService,
    public readonly translate: TranslateService,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly podService: PodUtilService,
  ) {}

  ngOnInit() {
    this.execPermissions$ = this.terminal.getPermissions({
      cluster: this.baseParams.cluster,
      namespace: this.baseParams.namespace,
    });
  }

  delete(pod: Pod) {
    this.podService.deletePod(pod, this.baseParams.cluster).subscribe(res => {
      this.list.delete(res);
      this.delayedReload();
      this.message.success(this.translate.get('delete_success'));
    });
  }

  canExec(container: Container, pod: Pod) {
    const containerStatuses = get(
      pod,
      [STATUS, 'containerStatuses'],
      [] as ContainerStatus[],
    );
    return !!get(
      containerStatuses.find(status => status.name === container.name),
      ['state', 'running'],
    );
  }

  onExec(pod: Pod, container: Container) {
    const dialogRef = this.dialog.open<
      ExecCommandDialogComponent,
      ExecCommandDialogParams
    >(ExecCommandDialogComponent, {
      data: {
        cluster: this.baseParams.cluster,
        namespace: this.baseParams.namespace,
      },
    });
    dialogRef.componentInstance.close.pipe(first()).subscribe(data => {
      dialogRef.close();
      this.podService.handleExecInfo(pod, container, this.baseParams, data);
    });
  }

  trackByFn(_: number, pod: Pod) {
    return pod.metadata.name;
  }

  // reference: http://confluence.alauda.cn/pages/viewpage.action?pageId=61913591
  getContainerRestartCountAcc(status: PodStatus) {
    const { containerStatuses = [], initContainerStatuses = [] } = status;
    const statuses = containerStatuses.concat(...initContainerStatuses);

    return statuses.length > 0
      ? statuses
          .map(i => i.restartCount)
          .reduce((a, b) => {
            return a + b;
          })
      : '';
  }

  openPodYaml(pod: Pod) {
    this.dialog.open(CodeDisplayDialogComponent, {
      size: DialogSize.Big,
      data: {
        code: getResourceYaml(pod),
        title: pod.metadata.name,
      },
    });
  }

  private delayedReload() {
    setTimeout(() => {
      this.list.reload();
    }, 5000);
  }

  private fetchPodResourceList({
    cluster,
    namespace,
    name: _name,
    project: _project,
    ...queryParams
  }: WorkspaceListParams) {
    return this.k8sApi.getResourceList<Pod>({
      type: RESOURCE_TYPES.POD,
      cluster,
      namespace,
      queryParams,
    });
  }

  searchByName(keyword: string) {
    this.router.navigate(['./'], {
      queryParams: {
        keyword,
      },
      relativeTo: this.activatedRoute,
      queryParamsHandling: 'merge',
    });
  }

  viewResource(pod: KubernetesResource, templateRef: TemplateRef<any>) {
    this.yamlInputValue = safeDump(pod, {
      lineWidth: 9999,
      sortKeys: true,
    });
    this.dialog.open(templateRef, {
      size: DialogSize.Large,
      data: {
        name: pod.metadata.name,
      },
    });
  }

  getPodResourceLimit = getPodResourceLimit;
}
