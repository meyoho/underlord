import { StringMap, TranslateService } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Optional,
} from '@angular/core';
import {
  AbstractControl,
  ControlContainer,
  FormGroup,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';

import {
  K8S_RESOURCE_LABEL_KEY_NAME_PATTERN,
  K8S_RESOURCE_LABEL_KEY_PREFIX_PATTERN,
  K8S_RESOURCE_LABEL_VALUE_PATTERN,
  k8sResourceLabelKeyValidator,
} from 'app/utils';

interface Resource {
  key: string;
  value: string;
  effect: string;
}

const keyPatternValidator: ValidatorFn = control => {
  const keyControl = (control as FormGroup).get('key');
  if (keyControl.invalid && keyControl.errors) {
    if (keyControl.errors.required) {
      return {
        keyRequired: true,
      };
    }
    if (keyControl.errors.maxlength) {
      return {
        keyMaxlength: true,
      };
    }
    if (keyControl.errors.namePattern) {
      return {
        namePattern: true,
      };
    }
    if (keyControl.errors.prefixPattern) {
      return {
        prefixPattern: true,
      };
    }
  } else {
    return null;
  }
};

const valuePatternValidator: ValidatorFn = control => {
  const valueControl = (control as FormGroup).get('value');
  if (valueControl.invalid && valueControl.errors) {
    if (valueControl.errors.maxlength) {
      return {
        valueMaxlength: true,
      };
    }
    if (valueControl.errors.pattern) {
      return {
        valuePattern: true,
      };
    }
  } else {
    return null;
  }
};

const missingKeyValidator: ValidatorFn = control => {
  const { key, value } = control.value;

  if (!key && value) {
    return {
      keyIsMissing: true,
    };
  } else {
    return null;
  }
};

@Component({
  selector: 'alu-node-taint-fieldset',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NodeTaintFieldsetComponent extends BaseResourceFormArrayComponent<
  Resource
> {
  rowBackgroundColorFn = ({ invalid, dirty }: AbstractControl) =>
    dirty && invalid ? '#fdefef' : '';

  keyNamePattern = K8S_RESOURCE_LABEL_KEY_NAME_PATTERN;
  keyPrefixPattern = K8S_RESOURCE_LABEL_KEY_PREFIX_PATTERN;
  valuePattern = K8S_RESOURCE_LABEL_VALUE_PATTERN;
  effects = ['NoSchedule', 'PreferNoSchedule', 'NoExecute'];

  constructor(
    public injector: Injector,
    private readonly translateService: TranslateService,
    @Optional() public readonly controlContainer: ControlContainer,
  ) {
    super(injector);
  }

  getOnFormArrayResizeFn() {
    return () =>
      this.fb.group(
        {
          key: [
            '',
            [
              Validators.required,
              k8sResourceLabelKeyValidator,
              Validators.maxLength(63),
            ],
          ],
          value: [
            '',
            [
              Validators.pattern(K8S_RESOURCE_LABEL_VALUE_PATTERN.pattern),
              Validators.maxLength(63),
            ],
          ],
          effect: 'NoSchedule',
        },
        {
          validator: [
            keyPatternValidator,
            valuePatternValidator,
            missingKeyValidator,
          ],
        },
      );
  }

  getRowErrorMessage = (errors: StringMap) => {
    if (errors.keyIsMissing) {
      return this.translateService.get('please_input_key');
    }
    if (errors.keyRequired) {
      return this.translateService.get('field_is_required', {
        field: this.translateService.get('key'),
      });
    }
    if (errors.keyMaxlength) {
      return this.translateService.get('key_length_should_less_than_63');
    }
    if (errors.namePattern) {
      return this.translateService.get(K8S_RESOURCE_LABEL_KEY_NAME_PATTERN.tip);
    }
    if (errors.prefixPattern) {
      return this.translateService.get(
        K8S_RESOURCE_LABEL_KEY_PREFIX_PATTERN.tip,
      );
    }
    if (errors.valueMaxlength) {
      return this.translateService.get('value_length_should_less_than_63');
    }
    if (errors.valuePattern) {
      return this.translateService.get(K8S_RESOURCE_LABEL_VALUE_PATTERN.tip);
    }
  };
}
