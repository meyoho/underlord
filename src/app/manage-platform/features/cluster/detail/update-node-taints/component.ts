import { K8sApiService, TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import { Component, Inject, OnDestroy, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { cloneDeep } from 'lodash-es';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Node, NodeTaint } from 'app/typings';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';

@Component({
  templateUrl: 'template.html',
})
export class UpdateNodeTaintsComponent implements OnDestroy {
  taints: NodeTaint[];

  submitting = false;
  @ViewChild('form', { static: true })
  form: NgForm;

  onDestroy$ = new Subject<void>();

  constructor(
    @Inject(DIALOG_DATA)
    public modalData: { node: Node; clusterName: string },
    private readonly translateService: TranslateService,
    private readonly dialogRef: DialogRef,
    private readonly messageService: MessageService,
    private readonly k8sApi: K8sApiService<ResourceType>,
  ) {
    this.taints = cloneDeep(modalData.node.spec.taints);
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.submitting = true;
    this.k8sApi
      .patchResource<Node>({
        type: RESOURCE_TYPES.NODE,
        cluster: this.modalData.clusterName,
        resource: this.modalData.node,
        part: {
          spec: {
            taints: this.taints,
          },
        },
      })
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(
        node => {
          this.messageService.success(
            this.translateService.get('update_success'),
          );
          this.dialogRef.close(node);
        },
        () => {
          this.submitting = false;
        },
      );
  }

  cancel() {
    this.dialogRef.close(null);
  }
}
