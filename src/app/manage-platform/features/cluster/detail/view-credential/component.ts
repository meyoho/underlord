import {
  AsyncDataLoader,
  K8sApiService,
  matchLabelsToString,
} from '@alauda/common-snippet';
import { DIALOG_DATA } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { map, pluck } from 'rxjs/operators';

import { ClusterCredential } from 'app/typings';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewCredentialComponent {
  dataManager = new AsyncDataLoader<{
    resource: ClusterCredential;
  }>({
    fetcher: () => {
      return this.k8sApi
        .getGlobalResourceList<ClusterCredential>({
          type: RESOURCE_TYPES.CLUSTER_CREDENTIAL,
          queryParams: {
            fieldSelector: matchLabelsToString({
              clusterName: this.modalData.clusterName,
            }),
          },
        })
        .pipe(
          pluck('items'),
          map(resource => ({
            resource: {
              ...resource[0],
              caCert: atob(resource[0].caCert),
            },
          })),
        );
    },
  });

  constructor(
    @Inject(DIALOG_DATA)
    public readonly modalData: {
      clusterName: string;
    },
    private readonly k8sApi: K8sApiService<ResourceType>,
  ) {}
}
