import {
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  KubernetesResource,
  TranslateService,
  matchLabelsToString,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService, DialogSize, MessageService } from '@alauda/ui';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, EMPTY, Subject, noop } from 'rxjs';
import {
  catchError,
  map,
  pluck,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import {
  ClusterFeature,
  Node,
  NodeStatusColorMapper,
  NodeStatusEnum,
  NodeStatusIconMapper,
  NodeTaint,
  Pod,
  TKEMachine,
} from 'app/typings';
import { RESOURCE_TYPES, ResourceType, getNodeStatus } from 'app/utils';

import { formatCommonUnit, formatNumUnit } from '../../utils';
import { DrainNodeComponent } from '../drain-node/component';
import { ClusterRemoveNodeComponent } from '../remove-node/component';
import { UpdateDisplayNameComponent } from '../update-displayname/component';
import { UpdateNodeLabelsComponent } from '../update-node-labels/component';
import { UpdateNodeTaintsComponent } from '../update-node-taints/component';

interface NodeVm {
  privateIp: string;
  hostname: string;
  type: 'master' | 'node';
}

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class NodeDetailComponent implements OnInit, OnDestroy {
  clusterName: string;
  nodeName = '';
  showMonitorTab = false;
  updated$ = new BehaviorSubject(null);
  nodeVm: NodeVm;
  node: Node;
  machine: TKEMachine;
  metricDocked: boolean;
  onDestroy$ = new Subject<void>();
  isOCP: string;
  isBaremetal: string;

  node$ = this.updated$
    .pipe(
      takeUntil(this.onDestroy$),
      switchMap(() =>
        this.k8sApi
          .getResource<Node>({
            type: RESOURCE_TYPES.NODE,
            cluster: this.clusterName,
            name: this.nodeName,
          })
          .pipe(
            catchError(() => {
              this.router.navigate(['../'], {
                relativeTo: this.route,
              });
              return EMPTY;
            }),
          ),
      ),
    )
    .pipe(
      tap(node => {
        this.node = node;
        this.nodeVm = {
          privateIp: this.node.status.addresses.find(
            addr => addr.type === 'InternalIP',
          ).address,
          hostname: this.node.status.addresses.find(
            addr => addr.type === 'Hostname',
          ).address,
          type:
            'node-role.kubernetes.io/master' in this.node.metadata.labels
              ? 'master'
              : 'node',
        };
      }),
    );

  getNodeStatus = getNodeStatus;
  NodeStatusColorMapper = NodeStatusColorMapper;
  NodeStatusEnum = NodeStatusEnum;
  NodeStatusIconMapper = NodeStatusIconMapper;
  formatNumUnit = formatNumUnit;
  formatCommonUnit = formatCommonUnit;

  pods: KubernetesResource[];
  pods$ = this.route.paramMap.pipe(
    switchMap(paramMap =>
      this.k8sApi.getResourceList<Pod>({
        type: RESOURCE_TYPES.POD,
        cluster: paramMap.get('clusterName'),
        queryParams: {
          fieldSelector: matchLabelsToString({
            'spec.nodeName': paramMap.get('nodeName'),
          }),
        },
      }),
    ),
    pluck('items'),
    tap(items => {
      this.pods = items;
    }),
    map(items =>
      items.filter(
        item =>
          item.status.phase !== 'Succeeded' && item.status.phase !== 'Failed',
      ),
    ),
    publishRef(),
  );

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.NODE,
    action: [K8sResourceAction.UPDATE, K8sResourceAction.DELETE],
  });

  constructor(
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sPermission: K8sPermissionService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly dialogService: DialogService,
    private readonly translateService: TranslateService,
    private readonly messageService: MessageService,
  ) {
    this.isOCP = route.snapshot.queryParams.isOCP;
    this.isBaremetal = route.snapshot.queryParams.isBaremetal;
    this.clusterName = route.snapshot.paramMap.get('clusterName');
    this.nodeName = route.snapshot.paramMap.get('nodeName');
  }

  ngOnInit() {
    this.k8sApi
      .getResourceList<ClusterFeature>({
        type: RESOURCE_TYPES.FEATURE,
        cluster: this.clusterName,
        queryParams: {
          labelSelector: 'instanceType=prometheus',
        },
      })
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(features => {
        this.metricDocked = !!features.items.length;
      });
    this.updated$
      .pipe(
        takeUntil(this.onDestroy$),
        switchMap(() =>
          this.k8sApi.getGlobalResourceList<TKEMachine>({
            type: RESOURCE_TYPES.MACHINE,
            queryParams: {
              fieldSelector: 'spec.clusterName=' + this.clusterName,
            },
          }),
        ),
      )
      .subscribe(machines => {
        this.machine = machines.items.find(
          machine => this.nodeName === machine.spec.ip,
        );
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  onUpdate() {
    this.updated$.next(null);
  }

  deleteNode = () => {
    const { node, clusterName } = this;
    const dialogRef = this.dialogService.open(ClusterRemoveNodeComponent, {
      data: {
        node,
        machine: this.machine,
        ip: this.getPrivateIp(node),
        clusterName,
      },
    });

    dialogRef.afterClosed().subscribe(node => {
      if (node) {
        this.router.navigate([
          '/manage-platform/cluster/detail/' + clusterName,
        ]);
      }
    });
  };

  updateDisplayName(node: Node) {
    const dialogRef = this.dialogService.open(UpdateDisplayNameComponent, {
      data: {
        node,
        clusterName: this.clusterName,
      },
    });
    dialogRef.afterClosed().subscribe(item => !!item && this.onUpdate());
  }

  getPrivateIp(node: Node) {
    return node.status.addresses.find(addr => addr.type === 'InternalIP')
      .address;
  }

  updateNodeTag() {
    const { node, clusterName } = this;
    const dialogRef = this.dialogService.open(UpdateNodeLabelsComponent, {
      data: {
        clusterName,
        node,
      },
    });

    dialogRef.afterClosed().subscribe(node => node && this.onUpdate());
  }

  drainNode() {
    const { node, clusterName } = this;
    const dialogRef = this.dialogService.open(DrainNodeComponent, {
      data: {
        clusterName,
        node,
        pods: this.pods,
      },
    });

    dialogRef.afterClosed().subscribe(node => node && this.onUpdate());
  }

  updateNodeTaint() {
    const { node, clusterName } = this;
    const dialogRef = this.dialogService.open(UpdateNodeTaintsComponent, {
      size: DialogSize.Large,
      data: {
        clusterName,
        node,
      },
    });

    dialogRef.afterClosed().subscribe(node => node && this.onUpdate());
  }

  updateNodeCordon(action: string) {
    this.dialogService
      .confirm({
        title: this.translateService.get('cluster_node_' + action),
        content: this.translateService.get(
          `cluster_node_maintain_${action}_confirm_message`,
        ),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      })
      .then(() => {
        this.k8sApi
          .patchResource<Node>({
            type: RESOURCE_TYPES.NODE,
            cluster: this.clusterName,
            resource: this.node,
            part: {
              spec: {
                unschedulable: action !== 'uncordon',
              },
            },
          })
          .subscribe(() => {
            this.messageService.success(
              this.translateService.get('setup_success'),
            );
            this.onUpdate();
          });
      })
      .catch(noop);
  }

  transformTaint(taints: NodeTaint[]) {
    return taints?.map(
      taint => `${taint.key}=${taint.value || ''}: ${taint.effect}`,
    );
  }

  isNodeAbnormal(status: NodeStatusEnum) {
    return status === NodeStatusEnum.abnormal;
  }
}
