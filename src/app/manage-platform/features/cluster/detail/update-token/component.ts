import {
  K8sApiService,
  TranslateService,
  matchLabelsToString,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subject } from 'rxjs';
import { map, pluck, takeUntil } from 'rxjs/operators';

import { ClusterCredential } from 'app/typings';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClusterUpdateTokenComponent implements OnInit, OnDestroy {
  @ViewChild('form', { static: true })
  form: NgForm;

  onDestroy$ = new Subject<void>();

  submitting = false;
  token: string;
  clusterCredential: ClusterCredential;

  constructor(
    @Inject(DIALOG_DATA)
    private readonly modalData: {
      cluster: string;
    },
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly dialogRef: DialogRef,
    private readonly translateService: TranslateService,
    private readonly messageService: MessageService,
  ) {}

  ngOnInit() {
    this.k8sApi
      .getGlobalResourceList<ClusterCredential>({
        type: RESOURCE_TYPES.CLUSTER_CREDENTIAL,
        queryParams: {
          fieldSelector: matchLabelsToString({
            clusterName: this.modalData.cluster,
          }),
        },
      })
      .pipe(
        pluck('items'),
        map(resource => resource[0]),
        takeUntil(this.onDestroy$),
      )
      .subscribe(res => {
        this.token = res.token;
        this.clusterCredential = res;
        this.cdr.markForCheck();
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.submitting = true;
    this.k8sApi
      .patchGlobalResource<ClusterCredential>({
        type: RESOURCE_TYPES.CLUSTER_CREDENTIAL,
        resource: this.clusterCredential,
        part: {
          token: this.token,
        },
      })
      .pipe(
        map(resource => ({
          resource,
        })),
        takeUntil(this.onDestroy$),
      )
      .subscribe(
        () => {
          this.messageService.success(
            this.translateService.get('update_success'),
          );
          this.dialogRef.close(null);
        },
        () => {
          this.submitting = false;
          this.cdr.markForCheck();
        },
      );
  }

  cancel() {
    this.dialogRef.close(null);
  }
}
