import { CREATION_TIMESTAMP, NAME } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { get } from 'lodash-es';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

import { Project } from 'app/typings';

@Component({
  selector: 'alu-cluster-project-list',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectListTableComponent {
  @Input()
  data: Project[];

  @Input()
  clusterName: string;

  columns = [NAME, 'clusters', CREATION_TIMESTAMP];
  projectName = '';
  projectName$ = new BehaviorSubject<string>(this.projectName);
  projects$ = this.projectName$.pipe(
    map(name => {
      return this.data
        .map(el => {
          if (!el.spec.clusters) {
            return null;
          }
          const cluster = el.spec.clusters.find(
            c => c.name === this.clusterName,
          );
          if (cluster) {
            el.spec.clusters = [cluster];
            return el;
          } else {
            return null;
          }
        })
        .filter(el => {
          return el !== null && el.metadata.name.includes(name.trim());
        });
    }),
  );

  getClusters(item: Project) {
    return get(item, 'spec.clusters', []);
  }
}
