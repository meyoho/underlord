import { ValueHook } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { ResourceQuotaItem } from 'app/typings';

export const RESOURCE_QUOTA_DEFINITIONS: Array<{
  name: string;
  key: string;
  unit: string;
  color: string;
  icon?: string;
}> = [
  {
    name: 'cpu',
    key: 'limits.cpu',
    unit: 'core',
    color: '6359B4',
  },
  {
    name: 'memory',
    key: 'limits.memory',
    unit: 'Gi',
    color: '007cb5',
  },
  {
    name: 'storage',
    key: 'requests.storage',
    unit: 'Gi',
    color: '7da41b',
    icon: 'storage_s',
  },
];

@Component({
  selector: 'alu-project-quota-display-list',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectQuotaDisplayListComponent {
  @ValueHook<ProjectQuotaDisplayListComponent, 'values'>(values => !!values)
  @Input()
  values: ResourceQuotaItem = {};

  definitions = RESOURCE_QUOTA_DEFINITIONS;
}
