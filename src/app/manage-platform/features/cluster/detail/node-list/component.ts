import {
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  K8sUtilService,
  NAME,
  ObservableInput,
  StringMap,
  TranslateService,
  noop,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService, DialogSize, MessageService } from '@alauda/ui';
import { ChangeDetectorRef, Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, combineLatest } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import {
  Metric,
  MetricQueries,
  MetricService,
} from 'app/api/alarm/metric.service';
import {
  Node,
  NodeStatusColorMapper,
  NodeStatusEnum,
  NodeStatusIconMapper,
  NodeTaint,
  TKEMachine,
} from 'app/typings';
import {
  ACTION,
  RESOURCE_TYPES,
  ResourceType,
  STATUS,
  getNodeStatus,
} from 'app/utils';

import { ExecutionProgressDialogComponent } from '../../list/execution-progress-dialog/component';
import { formatCommonUnit, formatNumUnit, getBarStatus } from '../../utils';
import { AddNodeComponent } from '../add-node/component';
import { ClusterRemoveNodeComponent } from '../remove-node/component';
import { UpdateNodeLabelsComponent } from '../update-node-labels/component';
import { UpdateNodeTaintsComponent } from '../update-node-taints/component';

interface NodeExtended extends Node {
  type?: string;
  displayName?: string;
}

const MASTERLABEL = 'node-role.kubernetes.io/master';

@Component({
  selector: 'alu-cluster-node-list',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class ClusterNodesComponent {
  @Input()
  clusterName: string;

  @Input()
  isOCP: boolean;

  @Input()
  isBaremetal: boolean;

  @Input()
  isGPU: boolean;

  @Input()
  isClusterAbnormal: boolean;

  @ObservableInput()
  @Input('metricDocked')
  metricDocked$: Observable<boolean>;

  columns = [
    NAME,
    STATUS,
    'privateIp',
    'nodeType',
    'labels',
    'schedule',
    'taints',
    'usage',
    ACTION,
  ];

  TYPE_TKE_MACHINE = 'Machine';
  searchKey = '';
  items: NodeExtended[];
  machines: TKEMachine[];
  MASTERLABEL = MASTERLABEL;
  getBarStatus = getBarStatus;
  getNodeStatus = getNodeStatus;
  formatCPU = formatNumUnit;
  formatCommonUnit = formatCommonUnit;
  NodeStatusEnum = NodeStatusEnum;
  NodeStatusColorMapper = NodeStatusColorMapper;
  NodeStatusIconMapper = NodeStatusIconMapper;
  nodeCpuMemMap: {
    [key: string]: StringMap;
  } = {};

  list = new K8SResourceList({
    fetcher: this.fetchNodeList.bind(this),
    activatedRoute: this.activatedRoute,
  });

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.NODE,
    action: [
      K8sResourceAction.CREATE,
      K8sResourceAction.UPDATE,
      K8sResourceAction.DELETE,
    ],
  });

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sUtil: K8sUtilService,
    private readonly translateService: TranslateService,
    private readonly dialogService: DialogService,
    private readonly metricService: MetricService,
    private readonly messageService: MessageService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  fetchNodeList() {
    return combineLatest([
      this.k8sApi.getGlobalResourceList<TKEMachine>({
        type: RESOURCE_TYPES.MACHINE,
        queryParams: {
          fieldSelector: 'spec.clusterName=' + this.clusterName,
        },
      }),
      this.k8sApi.getResourceList<Node>({
        type: RESOURCE_TYPES.NODE,
        cluster: this.clusterName,
      }),
    ]).pipe(
      map(([machines, nodes]) => {
        this.machines = machines.items;
        const deploying: NodeExtended[] = machines.items
          .filter(
            machine =>
              machine.status.phase === 'Initializing' &&
              !nodes.items.find(node => node.metadata.name === machine.spec.ip),
          )
          .map(machine => ({
            ...machine,
            type: this.TYPE_TKE_MACHINE,
            displayName: '',
          })) as NodeExtended[];
        return {
          ...nodes,
          items: nodes.items
            .map(
              it =>
                ({
                  ...it,
                  displayName: this.k8sUtil.getDisplayName(
                    machines.items.find(
                      machine => it.metadata.name === machine.spec.ip,
                    ),
                  ),
                } as NodeExtended),
            )
            .concat(deploying)
            .sort((a, b) => a.metadata.name.localeCompare(b.metadata.name)),
        };
      }),
      tap(res => {
        this.items = res.items;
        this.metricDocked$.pipe(publishRef()).subscribe(metricDocked => {
          res.items.forEach(node => {
            if (!node.type && metricDocked) {
              this.queryPrometheus(node);
            }
          });
        });
      }),
    );
  }

  search(keyword: string) {
    this.list.scanItems(() =>
      this.items.filter(item =>
        item.type !== this.TYPE_TKE_MACHINE
          ? item.metadata.name.includes(keyword) ||
            this.getPrivateIp(item).includes(keyword) ||
            this.k8sUtil.getDisplayName(item)?.includes(keyword) ||
            this.searchByLables(keyword, item)
          : item.spec.ip.includes(keyword) ||
            this.k8sUtil.getDisplayName(item)?.includes(keyword),
      ),
    );
  }

  searchByLables(keyword: string, item: NodeExtended) {
    let contains = false;
    let key = '';
    let value = '';
    const match = /([\w-./]+)\s*([^\w-./])?\s*([\w-./]*)/.exec(keyword);
    if (match) {
      key = match[1];
      if (match[3]) {
        value = match[3];
      }
    }
    Object.keys(item.metadata.labels).forEach(labelKey => {
      if (value) {
        if (
          labelKey === key &&
          item.metadata.labels[labelKey].includes(value)
        ) {
          contains = true;
        }
      } else if (
        labelKey.includes(key) ||
        item.metadata.labels[labelKey].includes(key)
      ) {
        contains = true;
      }
    });
    return contains;
  }

  private queryPrometheus(node: NodeExtended) {
    const privateIp = this.getPrivateIp(node);
    const hostname = this.getHostname(node);
    const nodeName = this.k8sUtil.getName(node);
    let name = '';
    if (this.isOCP) {
      name = hostname;
    } else {
      name = `${privateIp}:.*`;
    }
    const metrics = [] as Array<{
      uuid: string;
      query: string;
    }>;
    const exprs = [
      {
        key: 'cpu_request',
        value: `node_resource_requests_cpu_cores{node="${hostname}"}`,
      },
      {
        key: 'cpu_limit',
        value: `sum(kube_pod_container_resource_limits_cpu_cores{node="${hostname}"})`,
      },
      {
        key: 'memory_request',
        value: `sum(kube_pod_container_resource_requests_memory_bytes{node="${hostname}"})`,
      },
      {
        key: 'memory_limit',
        value: `sum(kube_pod_container_resource_limits_memory_bytes{node="${hostname}"})`,
      },
      {
        key: 'memory_total',
        value: `node_memory_MemTotal_bytes{instance=~"${privateIp}:.*"}`,
      },
    ];
    const queries = ['cpu.utilization', 'memory.utilization']
      .map(query => {
        metrics.push({
          uuid: query,
          query,
        });
        return {
          aggregator: 'avg',
          id: query,
          labels: [
            {
              type: 'EQUAL',
              name: '__name__',
              value: `node.${query}`,
            },
            {
              type: 'EQUAL',
              name: 'kind',
              value: 'Node',
            },
            {
              type: 'EQUAL',
              name: NAME,
              value: name,
            },
          ],
        } as MetricQueries;
      })
      .concat(
        exprs.map(({ key, value }) => {
          metrics.push({
            uuid: key,
            query: value,
          });
          return {
            aggregator: 'avg',
            id: key,
            labels: [
              {
                type: 'EQUAL',
                name: '__name__',
                value: 'custom',
              },
              {
                type: 'EQUAL',
                name: 'expr',
                value,
              },
            ],
          } as MetricQueries;
        }),
      );
    this.metricService
      .queryMetric(this.clusterName, {
        time: Math.floor(Date.now() / 1000),
        queries,
      })
      .then((results: Metric[]) => {
        const resultMap: StringMap = {};
        results.forEach(result => {
          const ratioIndex = metrics.findIndex(
            ratio => ratio.uuid === result.metric.__query_id__,
          );
          const metric = metrics[ratioIndex];
          let value = Number(result.value[1]) || 0;
          if (metric.uuid.includes('memory_')) {
            value = value / Math.pow(1024, 3);
          }
          if (metric.query === 'cpu.utilization') {
            resultMap.cpu_usage_rate = (value * 100).toFixed(2);
            resultMap.cpu_usage = (
              value * formatNumUnit(node.status.allocatable.cpu)
            ).toFixed(2);
            resultMap.cpu_total = formatNumUnit(
              node.status.allocatable.cpu,
            ).toFixed(0);
          } else if (metric.query === 'memory.utilization') {
            resultMap.memory_usage_rate = (value * 100).toFixed(2);
          } else {
            resultMap[metric.uuid] = String(value);
          }
        });
        if (!this.nodeCpuMemMap[nodeName]) {
          this.nodeCpuMemMap[nodeName] = {};
        }
        this.nodeCpuMemMap[nodeName] = resultMap;
        this.cdr.markForCheck();
      });
  }

  addNode(nodes: string[] = []) {
    this.dialogService
      .open(AddNodeComponent, {
        size: DialogSize.Large,
        data: {
          clusterName: this.clusterName,
          isGPU: this.isGPU,
          nodes,
        },
      })
      .afterClosed()
      .subscribe(() => {
        this.list.reload();
      });
  }

  removeNode = (node: NodeExtended) => {
    const dialogRef = this.dialogService.open(ClusterRemoveNodeComponent, {
      data: {
        node,
        machine: this.machines.find(
          machine => node.metadata.name === machine.spec.ip,
        ),
        ip: this.getPrivateIp(node),
        clusterName: this.clusterName,
        isMachine: node.type === this.TYPE_TKE_MACHINE,
      },
    });

    dialogRef.afterClosed().subscribe(n => {
      if (n) {
        this.list.reload();
        this.messageService.success(
          this.translateService.get('cluster_node_delete_success'),
        );
      }
    });
  };

  updateNodeCordon(node: Node, action: string) {
    this.dialogService
      .confirm({
        title: this.translateService.get('cluster_node_' + action),
        content: this.translateService.get(
          `cluster_node_maintain_${action}_confirm_message`,
        ),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      })
      .then(() => {
        this.k8sApi
          .patchResource<Node>({
            type: RESOURCE_TYPES.NODE,
            cluster: this.clusterName,
            resource: node,
            part: {
              spec: {
                unschedulable: action !== 'uncordon',
              },
            },
          })
          .subscribe(n => {
            this.messageService.success(
              this.translateService.get('setup_success'),
            );
            this.list.update(n);
          });
      })
      .catch(noop);
  }

  viewExecutionProgress(node: Node) {
    this.dialogService.open(ExecutionProgressDialogComponent, {
      data: {
        conditions: node.status.conditions,
        name: node.spec.ip,
      },
      size: DialogSize.Big,
    });
  }

  updateNodeTag(node: Node) {
    const { clusterName } = this;
    const dialogRef = this.dialogService.open(UpdateNodeLabelsComponent, {
      data: {
        clusterName,
        node,
      },
    });

    dialogRef.afterClosed().subscribe(n => n && this.list.update(n));
  }

  updateNodeTaint(node: Node) {
    const { clusterName } = this;
    const dialogRef = this.dialogService.open(UpdateNodeTaintsComponent, {
      size: DialogSize.Large,
      data: {
        clusterName,
        node,
      },
    });

    dialogRef.afterClosed().subscribe(node => node && this.list.update(node));
  }

  getPrivateIp(node: Node) {
    return node.status.addresses?.find(addr => addr.type === 'InternalIP')
      .address;
  }

  getHostname(node: Node) {
    return node.status.addresses?.find(addr => addr.type === 'Hostname')
      .address;
  }

  getNodeType(node: Node) {
    return node.metadata.labels ? MASTERLABEL in node.metadata.labels : false;
  }

  formatMemory(memory: string) {
    return formatCommonUnit(memory, true);
  }

  transformTaint(taints: NodeTaint[]) {
    return taints?.map(
      taint => `${taint.key}=${taint.value || ''}: ${taint.effect}`,
    );
  }

  isNodeAbnormal(status: NodeStatusEnum) {
    return status === NodeStatusEnum.abnormal;
  }

  isNodeNormal(status: NodeStatusEnum) {
    return status === NodeStatusEnum.normal;
  }
}
