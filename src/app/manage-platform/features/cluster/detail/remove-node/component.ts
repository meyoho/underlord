import {
  K8sApiService,
  KubernetesResourceList,
  matchLabelsToString,
  publishRef,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
} from '@angular/core';
import { saveAs } from 'file-saver';
import { pluck } from 'rxjs/operators';

import { Node, TKEMachine } from 'app/typings';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';

import { script } from './script';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClusterRemoveNodeComponent {
  inputValue = '';
  clearResource = false;
  submitting = false;
  columns = ['name', 'namespace'];

  constructor(
    @Inject(DIALOG_DATA)
    public readonly modalData: {
      clusterName: string;
      ip: string;
      node: Node;
      machine: TKEMachine;
      isMachine?: boolean;
    },
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly dialogRef: DialogRef,
  ) {}

  pods$ = this.k8sApi
    .getResourceList<KubernetesResourceList>({
      type: RESOURCE_TYPES.POD,
      cluster: this.modalData.clusterName,
      queryParams: {
        fieldSelector: matchLabelsToString({
          'spec.nodeName': this.modalData.node.metadata.name,
        }),
      },
    })
    .pipe(pluck('items'), publishRef());

  confirm() {
    this.submitting = true;
    this.k8sApi
      .deleteGlobalResource<TKEMachine>({
        type: RESOURCE_TYPES.MACHINE,
        resource: this.modalData.isMachine
          ? (this.modalData.node as TKEMachine)
          : this.modalData.machine,
        notifyOnError: false,
      })
      .subscribe(
        machine => {
          this.dialogRef.close(machine);
        },
        () => {
          this.submitting = false;
          this.cdr.markForCheck();
        },
      );
    if (!this.modalData.isMachine) {
      this.k8sApi
        .deleteResource<Node>({
          type: RESOURCE_TYPES.NODE,
          cluster: this.modalData.clusterName,
          resource: this.modalData.node,
          notifyOnError: false,
        })
        .subscribe(
          node => {
            this.dialogRef.close(node);
          },
          () => {
            this.submitting = false;
            this.cdr.markForCheck();
          },
        );
    }
  }

  downloadScript() {
    saveAs(
      new Blob([script], {
        type: 'text/plain;charset=utf-8',
      }),
      'nodeclean.sh',
    );
  }

  cancel() {
    this.dialogRef.close(null);
  }

  get disabled() {
    return !this.modalData.isMachine
      ? this.inputValue !== this.modalData.node.metadata.name
      : this.inputValue !== this.modalData.node.spec.ip;
  }
}
