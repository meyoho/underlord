export const script = `#!/bin/bash
set -x
set +e
/usr/local/bin/kubeadm reset -f
/usr/local/bin/kubeadm reset -f
/usr/local/bin/kubeadm reset -f
kubeadm reset -f
kubeadm reset -f
kubeadm reset -f
systemctl disable kubelet
systemctl daemon-reload
systemctl stop kubelet
rm -rf /etc/systemd/system/kubelet.service
rm -rf /etc/systemd/system/kubelet.service.d
for j in {1..5}
do
    for i in kubelet kubeadm kubectl kubectl-captain helm
    do
        rm -rf $(command -v $i)
    done
done
docker ps -qa|xargs docker rm -f
ip link set dummy0 down
ip link delete dummy0
ip link set tunl0 down
ip link delete tunl0
ip link set kube-ipvs0
ip link delete kube-ipvs0
ip link set flannel.1 down
ip link set cni0 down
ip link delete flannel.1
ip link delete cni0

rm -rf /etc/kube*
rm -rf /var/lib/kubelet
rm -rf /etc/etcd/
rm -rf /etc/cni/net.d
rm -rf /var/lib/etcd
rm -rf /opt/cni/bin
rm -rf /var/lib/cni/
rm -rf /etc/cni

rm -rf $HOME/.kube
rm -rf $HOME/.helm
rm -rf $HOME/.ansible/
rm -rf /root/.helm
rm -rf /root/.kube
rm -rf /root/.ansible/

rm -rf /var/run/openvswitch
rm -rf /etc/origin/openvswitch
rm -rf /etc/openvswitch

rm -rf /cpaas/install.log

df -l --output=target |grep ^/var/lib/kubelet |grep subpath | xargs -r umount
df -l --output=target |grep ^/var/lib/kubelet | xargs -r umount
ip link del ipvlan0
ip link del macvlan0
ip link del tunl0
for i in $(arp -an |grep 'PERM on macvlan0' | awk '{print $2}' | sed -e 's/(//g' -e 's/)//g') ; do arp -i macvlan0 -d $i; done;

iptables -P INPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT
iptables -t nat -F
iptables -t mangle -F
iptables -F
iptables -X
systemctl restart docker`;
