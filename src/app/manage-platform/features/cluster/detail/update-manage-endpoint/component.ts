import { K8sApiService, TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { K8sUtilService } from 'app/services/k8s-util.service';
import { TKECluster } from 'app/typings';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClusterUpdateEndpointComponent {
  @ViewChild('form', { static: true })
  form: NgForm;

  submitting = false;
  k8sDashboardEndpoint: string;

  constructor(
    @Inject(DIALOG_DATA)
    public readonly modalData: {
      cluster: TKECluster;
    },
    private readonly k8sUtil: K8sUtilService,
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly dialogRef: DialogRef,
    private readonly translateService: TranslateService,
    private readonly messageService: MessageService,
  ) {
    this.k8sDashboardEndpoint = this.k8sUtil.getAnnotation(
      this.modalData.cluster,
      'k8s-dashboard-endpoint',
      'infrastructure',
    );
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.submitting = true;
    this.k8sApi
      .patchGlobalResource<TKECluster>({
        type: RESOURCE_TYPES.TKE_CLUSTER,
        resource: this.modalData.cluster,
        part: {
          metadata: {
            annotations: {
              [this.k8sUtil.normalizeType(
                'k8s-dashboard-endpoint',
                'infrastructure',
              )]: this.k8sDashboardEndpoint,
            },
          },
        },
      })
      .subscribe(
        cluster => {
          this.messageService.success(
            this.translateService.get('update_success'),
          );
          this.dialogRef.close(cluster);
        },
        () => {
          this.submitting = false;
          this.cdr.markForCheck();
        },
      );
  }

  cancel() {
    this.dialogRef.close(null);
  }
}
