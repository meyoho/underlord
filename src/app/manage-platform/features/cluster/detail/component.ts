import {
  AsyncDataLoader,
  AuthorizationStateService,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  K8sUtilService,
  NAME,
  TranslateService,
  publishRef,
} from '@alauda/common-snippet';
import {
  DialogRef,
  DialogService,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import {
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, combineLatest, of } from 'rxjs';
import { catchError, first, map, pluck, switchMap, tap } from 'rxjs/operators';

import { Environments } from 'app/api/envs/types';
import { ProjectApiService } from 'app/api/project/api';
import { ENVIRONMENTS } from 'app/services/services.module';
import { ConfirmDeleteComponent } from 'app/shared/components/confirm-delete/confirm-delete.component';
import { ProjectListDialogComponent } from 'app/shared/features/cluster-project-list/component';
import {
  Cluster,
  ClusterFeature,
  ClusterFed,
  ClusterStatusColorMapper,
  ClusterStatusEnum,
  ClusterStatusIconMapper,
  Node,
  Project,
  TKECluster,
  TKEClusterSpecMachine,
} from 'app/typings';
import { RESOURCE_TYPES, ResourceType, getTKEClusterStatus } from 'app/utils';

import { formatCommonUnit, formatNumUnit } from '../utils';

import { ClusterNodesComponent } from './node-list/component';
import { UpdateDisplayNameComponent } from './update-displayname/component';
import { ClusterUpdateEndpointComponent } from './update-manage-endpoint/component';
import { UpdateResourceRatioComponent } from './update-resource-ratio/component';
import { ClusterUpdateTokenComponent } from './update-token/component';
import { ViewCredentialComponent } from './view-credential/component';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class DetailComponent implements OnDestroy {
  resource: TKECluster;
  showNodeErrAlert = true;
  context = this;
  deleteDialogRef: DialogRef<ConfirmDeleteComponent>;
  projects$ = this.projectApi
    .getProjects({})
    .pipe(map(projectList => projectList.items));

  @ViewChild(ClusterNodesComponent) nodeListViewChild: ClusterNodesComponent;
  private readonly onDestroy$ = new Subject<void>();

  params$ = this.route.paramMap.pipe(
    map(params => ({
      name: params.get(NAME),
    })),
    publishRef(),
  );

  federations$ = this.k8sApi
    .getGlobalResourceList<ClusterFed>({
      type: RESOURCE_TYPES.CLUSTER_FED,
    })
    .pipe(pluck('items'), publishRef());

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.CLUSTER_FED,
    action: [K8sResourceAction.DELETE, K8sResourceAction.UPDATE],
  });

  dataManager = new AsyncDataLoader<{
    resource: TKECluster;
    permissions: {
      update: boolean;
      delete: boolean;
    };
  }>({
    params$: this.params$,
    fetcher: params => {
      return combineLatest([
        this.k8sApi.getGlobalResource<TKECluster>({
          type: RESOURCE_TYPES.TKE_CLUSTER,
          name: params.name,
        }),
        this.permissions$,
      ]).pipe(
        map(([resource, permissions]) => ({
          resource,
          permissions,
        })),
      );
    },
  });

  nodes$ = this.params$.pipe(
    switchMap(params =>
      this.k8sApi.getResourceList<Node>({
        type: RESOURCE_TYPES.NODE,
        cluster: params.name,
      }),
    ),
    pluck('items'),
    publishRef(),
  );

  isHighAvailability$ = this.nodes$.pipe(
    map(
      nodes =>
        nodes.filter(
          node => 'node-role.kubernetes.io/master' in node.metadata.labels,
        ).length > 1,
    ),
  );

  resourceAmount$ = this.nodes$.pipe(
    map(nodes => {
      let cpu = 0;
      let memory = 0;
      let gpu = 0;
      let gpuMemory = 0;
      nodes.forEach(node => {
        cpu += formatNumUnit(node.status.allocatable.cpu);
        memory += +formatCommonUnit(node.status.allocatable.memory, true);
        gpu += +(node.status.allocatable['tencent.com/vcuda-core'] || 0) / 100;
        gpuMemory +=
          +(node.status.allocatable['tencent.com/vcuda-memory'] || 0) / 4;
      });
      return {
        cpu,
        memory,
        gpu,
        gpuMemory,
      };
    }),
  );

  metricDocked$ = this.params$.pipe(
    switchMap(params =>
      this.k8sApi.getResourceList<ClusterFeature>({
        type: RESOURCE_TYPES.FEATURE,
        cluster: params.name,
        queryParams: {
          labelSelector: 'instanceType=prometheus',
        },
      }),
    ),
    pluck('items'),
    map(nodes => nodes.length > 0),
    publishRef(),
  );

  getClusterStatus = getTKEClusterStatus;
  ClusterStatusColorMapper = ClusterStatusColorMapper;
  ClusterStatusIconMapper = ClusterStatusIconMapper;

  constructor(
    @Inject(ENVIRONMENTS) private readonly env: Environments,
    public authState: AuthorizationStateService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly dialog: DialogService,
    private readonly notification: NotificationService,
    private readonly message: MessageService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sUtil: K8sUtilService,
    private readonly translate: TranslateService,
    private readonly projectApi: ProjectApiService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  updateDisplayName(cluster: TKECluster) {
    const dialogRef = this.dialog.open(UpdateDisplayNameComponent, {
      data: {
        cluster,
      },
    });
    dialogRef
      .afterClosed()
      .subscribe(item => !!item && this.dataManager.reload());
  }

  viewClusterCredential(cluster: TKECluster) {
    this.dialog.open(ViewCredentialComponent, {
      data: {
        clusterName: cluster.metadata.name,
      },
    });
  }

  deleteCluster(templateRef: TemplateRef<ConfirmDeleteComponent>) {
    const resource = this.dataManager.snapshot.data.resource;
    const name = this.k8sUtil.getName(resource);

    this.k8sApi
      .getGlobalResourceList<ClusterFed>({
        type: RESOURCE_TYPES.CLUSTER_FED,
      })
      .subscribe(federations => {
        const found = federations.items.find(i => {
          return i.spec.clusters.some(cluster => cluster.name === name);
        });
        if (found) {
          this.dialog.confirm({
            title: this.translate.get(
              'delete_cluser_federation_warning_title',
              {
                name: this.k8sUtil.getUnionDisplayName(resource),
              },
            ),
            content: this.translate.get(
              'delete_cluser_federation_warning_content',
              {
                name: this.k8sUtil.getUnionDisplayName(found),
              },
            ),
            confirmText: this.translate.get('got_it'),
            cancelButton: false,
          });
        } else {
          this.deleteClusterCheckProject(templateRef);
        }
      });
  }

  deleteClusterCheckProject(templateRef: TemplateRef<ConfirmDeleteComponent>) {
    const resource = this.dataManager.snapshot.data.resource;
    const name = this.k8sUtil.getName(resource);

    combineLatest([
      this.k8sApi
        .getGlobalResource<Cluster>({
          type: RESOURCE_TYPES.CLUSTER,
          name,
          namespaced: true,
        })
        .pipe(
          catchError(() =>
            of({
              metadata: {
                finalizers: [],
              },
            }),
          ),
        ),
      this.k8sApi.getGlobalResourceList<Project>({
        type: RESOURCE_TYPES.PROJECT,
      }),
    ]).subscribe(([cluster, projectList]) => {
      const {
        metadata: { finalizers },
      } = cluster;
      if (finalizers && finalizers.length > 0) {
        const projectItems = projectList.items.filter(item =>
          finalizers.some(
            project =>
              project ===
              `${this.env.LABEL_BASE_DOMAIN}.project.${item.metadata.name}`,
          ),
        );
        this.dialog
          .open(ProjectListDialogComponent, {
            data: {
              projectItems,
              name,
              unionDisplayName: this.k8sUtil.getUnionDisplayName(cluster),
            },
          })
          .afterClosed()
          .pipe(first())
          .subscribe(res => {
            if (res) {
              this.deleteTKECluster(this.dataManager.snapshot.data.resource);
            }
          });
      } else {
        this.deleteDialogRef = this.dialog.open(templateRef);
      }
    });
  }

  deleteClusterApi() {
    return this.k8sApi
      .deleteGlobalResource<TKECluster>({
        type: RESOURCE_TYPES.TKE_CLUSTER,
        resource: this.dataManager.snapshot.data.resource,
      })
      .pipe(
        tap(() => {
          this.gotoList();
        }),
      );
  }

  deleteTKECluster(resource: TKECluster) {
    this.k8sApi
      .deleteGlobalResource<TKECluster>({
        type: RESOURCE_TYPES.TKE_CLUSTER,
        resource,
      })
      .subscribe(
        () => {
          this.gotoList();
        },
        () => {
          this.notification.error(this.translate.get('delete_cluster_failed'));
        },
      );
  }

  gotoList() {
    this.router.navigateByUrl('/manage-platform/cluster/list', {
      replaceUrl: true,
    });
  }

  updateResourceRatio(cluster: TKECluster) {
    const dialogRef = this.dialog.open(UpdateResourceRatioComponent, {
      data: {
        cluster,
      },
    });
    dialogRef.componentInstance.close.pipe(first()).subscribe(cluster => {
      dialogRef.close();
      if (cluster) {
        this.dataManager.reload();
        this.message.success(this.translate.get('update_over_commit_success'));
      }
    });
  }

  updateToken(cluster: TKECluster) {
    this.dialog.open(ClusterUpdateTokenComponent, {
      data: {
        cluster: cluster.metadata.name,
      },
    });
  }

  updateManageEndpoint(cluster: TKECluster) {
    const dialogRef = this.dialog.open(ClusterUpdateEndpointComponent, {
      data: {
        cluster,
      },
    });
    dialogRef
      .afterClosed()
      .subscribe(item => !!item && this.dataManager.reload());
  }

  jumpToListPage = () => {
    this.router.navigate(['/manage-platform/cluster/list']);
  };

  getFederation = (item: TKECluster, federations: ClusterFed[]) => {
    return federations.find(fed =>
      fed.spec.clusters.some(
        cluster => cluster.name === this.k8sUtil.getName(item),
      ),
    );
  };

  getNodeMax = (cluster: TKECluster) => {
    let nodeMaxNum = null;
    const cidr = cluster.spec.clusterCIDR;
    const fieldCIDR = cidr.split('/')[1];
    if (fieldCIDR) {
      const maxNodePodNum = cluster.spec.properties.maxNodePodNum;
      const maxClusterServiceNum = cluster.spec.properties.maxClusterServiceNum;
      nodeMaxNum = Math.pow(2, 32 - parseInt(fieldCIDR)) / maxNodePodNum;
      if (maxClusterServiceNum) {
        nodeMaxNum -= Math.ceil(maxClusterServiceNum / maxNodePodNum);
      }
    }
    return (
      this.translate.get('container_network_create_tip1') +
      ' ' +
      nodeMaxNum +
      ' ' +
      this.translate.get('container_network_create_tip2')
    );
  };

  getClusterEndpoint(cluster: TKECluster) {
    return cluster.status.addresses.find(
      address => address.type === 'Advertise',
    );
  }

  isClusterAbnormal(status: ClusterStatusEnum) {
    return status === ClusterStatusEnum.abnormal;
  }

  getSlaveMachines(machines: string) {
    try {
      const obj = JSON.parse(machines) as TKEClusterSpecMachine;
      const arr: string[] = [];
      Object.entries(obj).forEach(([key, value]) => {
        if (value.error) {
          arr.push(key);
        }
      });
      return arr.length ? arr : null;
    } catch {
      return null;
    }
  }

  addNodeAgain(nodes: string[]) {
    this.nodeListViewChild.addNode(nodes);
  }

  clearAnnotation() {
    const resource = this.dataManager.snapshot.data.resource;
    this.k8sApi
      .patchGlobalResource<TKECluster>({
        type: RESOURCE_TYPES.TKE_CLUSTER,
        resource,
        part: {
          metadata: {
            annotations: {
              [this.k8sUtil.normalizeType('machines')]: '{}',
            },
          },
        },
      })
      .subscribe(() => {
        this.showNodeErrAlert = false;
        this.cdr.markForCheck();
      });
  }
}
