import { TranslateService } from '@alauda/common-snippet';
import { Component, Input } from '@angular/core';
import { endsWith } from 'lodash-es';

import { formatNumber } from 'app/utils';

type ResourceType = 'cpu' | 'memory';

interface ResourceInfo {
  icon: string;
  formatter: (rawData: number | string, locale?: string) => string;
}
@Component({
  selector: 'alu-resource-label',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class ResourceLabelComponent {
  // 各资源的原始值
  @Input()
  rawValue: number;

  // 新增类型需要进行扩充
  @Input()
  type: ResourceType;

  labelIconMapper: Record<ResourceType, ResourceInfo> = {
    cpu: {
      icon: 'basic:cpu',
      formatter: this.formatCPU.bind(this),
    },
    memory: {
      icon: 'basic:memory',
      formatter: this.formatMemory.bind(this),
    },
  };

  constructor(public readonly translate: TranslateService) {
    this.formatCPU = this.formatCPU.bind(this);
    this.formatMemory = this.formatMemory.bind(this);
    this.getLabelInfo = this.getLabelInfo.bind(this);
  }

  getLabelInfo(type: ResourceType) {
    return this.labelIconMapper[type];
  }

  formatCPU(rawData: number | string) {
    const cpuUnit = this.getResourceUnit(rawData, this.formatCpuUnit);
    if (!cpuUnit) {
      return this.translate.get('unlimited');
    }
    return endsWith(cpuUnit, 'm')
      ? cpuUnit
      : cpuUnit + this.translate.get('unit_core');
  }

  formatMemory(rawData: number | string) {
    const memoryInit = this.getResourceUnit(rawData, this.formatMemoryUnit);
    if (!memoryInit) {
      return this.translate.get('unlimited');
    }
    return memoryInit;
  }

  private getResourceUnit(
    rawData: number | string,
    converter?: (_: number) => string,
  ) {
    return typeof rawData === 'string' ? rawData : converter(rawData);
  }

  private formatCpuUnit(cpu: number) {
    let cpuUnit = 'm';
    if (cpu >= 1000) {
      cpu /= 1000;
      cpuUnit = '';
    }
    return isNaN(cpu) ? '' : formatNumber(cpu, [1, 0, 2]) + cpuUnit;
  }

  private formatMemoryUnit(memory: number) {
    let memoryUnit = 'Mi';
    memory /= 1024 ** 2;
    if (memory >= 1024) {
      memory /= 1024;
      memoryUnit = 'Gi';
    }
    return isNaN(memory) ? '' : formatNumber(memory, [1, 0, 2]) + memoryUnit;
  }
}
