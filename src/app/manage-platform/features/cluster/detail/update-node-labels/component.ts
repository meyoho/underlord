import { K8sApiService, TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import { Component, Inject, OnDestroy, ViewChild } from '@angular/core';
import { NgForm, Validators } from '@angular/forms';
import { cloneDeep, mergeWith } from 'lodash-es';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Node } from 'app/typings';
import {
  K8S_RESOURCE_LABEL_KEY_NAME_PATTERN,
  K8S_RESOURCE_LABEL_KEY_PREFIX_PATTERN,
  K8S_RESOURCE_LABEL_VALUE_PATTERN,
  RESOURCE_TYPES,
  ResourceType,
  k8sResourceLabelKeyValidator,
} from 'app/utils';

@Component({
  templateUrl: 'template.html',
})
export class UpdateNodeLabelsComponent implements OnDestroy {
  labels: { [key: string]: string };
  submitting = false;
  @ViewChild('form', { static: true })
  form: NgForm;

  onDestroy$ = new Subject<void>();
  labelValidator = {
    key: [k8sResourceLabelKeyValidator, Validators.maxLength(63)],
    value: [
      Validators.pattern(K8S_RESOURCE_LABEL_VALUE_PATTERN.pattern),
      Validators.maxLength(63),
    ],
  };

  labelErrorMapper = {
    key: {
      namePattern: this.translateService.get(
        K8S_RESOURCE_LABEL_KEY_NAME_PATTERN.tip,
      ),
      prefixPattern: this.translateService.get(
        K8S_RESOURCE_LABEL_KEY_PREFIX_PATTERN.tip,
      ),
    },
    value: {
      pattern: this.translateService.get(K8S_RESOURCE_LABEL_VALUE_PATTERN.tip),
    },
  };

  constructor(
    @Inject(DIALOG_DATA)
    public modalData: { node: Node; clusterName: string },
    private readonly translateService: TranslateService,
    private readonly dialogRef: DialogRef,
    private readonly messageService: MessageService,
    private readonly k8sApi: K8sApiService<ResourceType>,
  ) {
    this.labels = cloneDeep(modalData.node.metadata.labels);
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.submitting = true;
    // todo: 推进后端优化 api 后可以删除 mergeWith
    const labels = mergeWith(
      this.labels,
      this.modalData.node.metadata.labels,
      obj => {
        if (obj === undefined) {
          return null;
        } else {
          return obj;
        }
      },
    );
    this.k8sApi
      .patchResource<Node>({
        type: RESOURCE_TYPES.NODE,
        cluster: this.modalData.clusterName,
        resource: this.modalData.node,
        part: {
          metadata: {
            labels,
          },
        },
      })
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(
        node => {
          this.messageService.success(
            this.translateService.get('update_success'),
          );
          this.dialogRef.close(node);
        },
        () => {
          this.submitting = false;
        },
      );
  }

  cancel() {
    this.dialogRef.close(null);
  }
}
