import {
  K8sApiService,
  K8sUtilService,
  TranslateService,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Node, TKECluster } from 'app/typings';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateDisplayNameComponent implements OnDestroy {
  @ViewChild('form', { static: true })
  form: NgForm;

  onDestroy$ = new Subject<void>();
  submitting = false;
  displayName: string;

  constructor(
    @Inject(DIALOG_DATA)
    public readonly modalData: {
      cluster: TKECluster;
      node: Node;
      clusterName: string;
    },
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    private readonly dialogRef: DialogRef,
    private readonly translateService: TranslateService,
    private readonly messageService: MessageService,
  ) {
    if (modalData.cluster) {
      this.displayName = modalData.cluster.spec.displayName;
    } else {
      this.displayName = this.k8sUtil.getDisplayName(modalData.node);
    }
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.submitting = true;
    if (this.modalData.cluster) {
      this.k8sApi
        .patchGlobalResource<TKECluster>({
          type: RESOURCE_TYPES.TKE_CLUSTER,
          resource: this.modalData.cluster,
          part: {
            spec: {
              displayName: this.displayName,
            },
          },
        })
        .pipe(takeUntil(this.onDestroy$))
        .subscribe(
          cluster => {
            this.messageService.success(
              this.translateService.get('update_success'),
            );
            this.dialogRef.close(cluster);
          },
          () => {
            this.submitting = false;
            this.cdr.markForCheck();
          },
        );
    } else {
      this.k8sApi
        .patchResource<Node>({
          type: RESOURCE_TYPES.NODE,
          cluster: this.modalData.clusterName,
          resource: this.modalData.node,
          part: {
            metadata: {
              annotations: {
                [this.k8sUtil.normalizeType('display-name')]: this.displayName,
              },
            },
          },
        })
        .pipe(takeUntil(this.onDestroy$))
        .subscribe(
          node => {
            this.messageService.success(
              this.translateService.get('update_success'),
            );
            this.dialogRef.close(node);
          },
          () => {
            this.submitting = false;
            this.cdr.markForCheck();
          },
        );
    }
  }

  cancel() {
    this.dialogRef.close(null);
  }
}
