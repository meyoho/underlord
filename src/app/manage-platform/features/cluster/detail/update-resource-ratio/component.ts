import { K8sApiService } from '@alauda/common-snippet';
import { DIALOG_DATA } from '@alauda/ui';
import { Component, EventEmitter, Inject, Output } from '@angular/core';
import { get } from 'lodash-es';
import { Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { TKECluster } from 'app/typings';
import { POSITIVE_INT_PATTERN, RESOURCE_TYPES } from 'app/utils';

interface ResourceRatioModel {
  cpu: string;
  memory: string;
  cpuEnabled: boolean;
  memoryEnabled: boolean;
}

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class UpdateResourceRatioComponent {
  @Output()
  close = new EventEmitter<TKECluster>();

  confirming$ = new Subject<boolean>();

  model: ResourceRatioModel;

  POSITIVE_INT_PATTERN = POSITIVE_INT_PATTERN;

  constructor(
    private readonly k8sApi: K8sApiService,
    @Inject(DIALOG_DATA)
    private readonly modalData: {
      cluster: TKECluster;
    },
  ) {
    const { cpu, memory } = get(
      this.modalData.cluster,
      ['spec', 'properties', 'oversoldRatio'],
      {} as {
        cpu: string;
        memory: string;
      },
    );
    this.model = {
      cpu,
      memory,
      cpuEnabled: !!cpu,
      memoryEnabled: !!memory,
    };
  }

  submitForm() {
    this.confirming$.next(true);
    const oversoldRatio = {
      cpu: this.model.cpuEnabled ? this.model.cpu : '',
      memory: this.model.memoryEnabled ? this.model.memory : '',
    };
    this.k8sApi
      .patchGlobalResource<TKECluster>({
        type: RESOURCE_TYPES.TKE_CLUSTER,
        resource: this.modalData.cluster,
        part: {
          spec: {
            properties: {
              oversoldRatio,
            },
          },
        },
      })
      .pipe(finalize(() => this.confirming$.next(false)))
      .subscribe(cluster => this.close.emit(cluster));
  }
}
