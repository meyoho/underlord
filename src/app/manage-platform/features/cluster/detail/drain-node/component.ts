import { API_GATEWAY, TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnDestroy,
} from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Node, Pod } from 'app/typings';
import { MachineMeta } from 'app/utils';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DrainNodeComponent implements OnDestroy {
  onDestroy$ = new Subject<void>();

  columns = ['name', 'namespace'];
  submitting = false;
  pods: Pod[];

  constructor(
    @Inject(DIALOG_DATA)
    public readonly modalData: {
      clusterName: string;
      node: Node;
      pods: Pod[];
    },
    private readonly dialogRef: DialogRef,
    private readonly translateService: TranslateService,
    private readonly messageService: MessageService,
    private readonly http: HttpClient,
  ) {
    this.pods = modalData.pods.filter(pod => {
      const ref = pod.metadata.ownerReferences;
      return ref && ref[0] ? ref[0].kind !== 'DaemonSet' : true;
    });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  confirm() {
    this.submitting = true;
    this.http
      .post(
        `${API_GATEWAY}/apis/${MachineMeta.apiVersion}/clusters/${this.modalData.clusterName}/drain/${this.modalData.node.metadata.name}`,
        {},
      )
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(
        () => {
          this.messageService.success(
            this.translateService.get('drain_success'),
          );
          this.dialogRef.close(null);
        },
        () => {
          this.submitting = false;
        },
      );
  }

  cancel() {
    this.dialogRef.close(null);
  }
}
