import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';

import { AddNodeComponent } from './add-node/component';
import { NodeFieldsetComponent } from './add-node/fieldset/component';
import { DetailComponent } from './component';
import { DrainNodeComponent } from './drain-node/component';
import { ExecCommandDialogComponent } from './exec/exec-command/component';
import { ClusterMonitorComponent } from './monitor/component';
import { NodeDetailComponent } from './node-detail/component';
import { ClusterNodesComponent } from './node-list/component';
import { PodListComponent } from './pod-list/component';
import { PodStatusComponent } from './pod-status/component';
import { ProjectListTableComponent } from './project-list/component';
import { ProjectQuotaDisplayListComponent } from './project-list/quota-display-list/component';
import { ClusterRemoveNodeComponent } from './remove-node/component';
import { ResourceLabelComponent } from './resource-label/component';
import { UpdateDisplayNameComponent } from './update-displayname/component';
import { ClusterUpdateEndpointComponent } from './update-manage-endpoint/component';
import { UpdateNodeLabelsComponent } from './update-node-labels/component';
import { UpdateNodeTaintsComponent } from './update-node-taints/component';
import { NodeTaintFieldsetComponent } from './update-node-taints/fieldset/component';
import { UpdateResourceRatioComponent } from './update-resource-ratio/component';
import { ClusterUpdateTokenComponent } from './update-token/component';
import { ViewCredentialComponent } from './view-credential/component';
@NgModule({
  imports: [SharedModule, RouterModule],
  declarations: [
    DetailComponent,
    ClusterMonitorComponent,
    DrainNodeComponent,
    ProjectListTableComponent,
    ProjectQuotaDisplayListComponent,
    NodeFieldsetComponent,
    NodeTaintFieldsetComponent,
    ClusterNodesComponent,
    NodeDetailComponent,
    UpdateNodeLabelsComponent,
    UpdateNodeTaintsComponent,
    AddNodeComponent,
    ClusterRemoveNodeComponent,
    UpdateResourceRatioComponent,
    ClusterUpdateTokenComponent,
    ClusterUpdateEndpointComponent,
    UpdateDisplayNameComponent,
    ViewCredentialComponent,
    PodListComponent,
    PodStatusComponent,
    ResourceLabelComponent,
    ExecCommandDialogComponent,
  ],
  entryComponents: [
    DrainNodeComponent,
    UpdateNodeLabelsComponent,
    UpdateNodeTaintsComponent,
    AddNodeComponent,
    ClusterRemoveNodeComponent,
    UpdateResourceRatioComponent,
    ClusterUpdateTokenComponent,
    ClusterUpdateEndpointComponent,
    UpdateDisplayNameComponent,
    ViewCredentialComponent,
    ExecCommandDialogComponent,
  ],
})
export class DetailModule {}
