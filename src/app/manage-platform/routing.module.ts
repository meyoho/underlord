import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ManagePlatformComponent } from './component';
@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: ManagePlatformComponent,
        children: [
          {
            path: 'cluster',
            loadChildren: () =>
              import('./features/cluster/module').then(M => M.ClusterModule),
          },
          {
            path: 'federation',
            loadChildren: () =>
              import('./features/federation/module').then(
                M => M.FederationModule,
              ),
          },
          {
            path: 'cluster_addon',
            loadChildren: () =>
              import('./features/cluster-addon/module').then(
                M => M.ClusterAddonModule,
              ),
          },
          {
            path: 'resource_management',
            loadChildren: () =>
              import('./features/resource-manage/module').then(
                M => M.ResourceManageModule,
              ),
          },
          {
            path: 'user',
            loadChildren: () =>
              import('./features/manage-user/module').then(
                M => M.ManageUserModule,
              ),
          },
          {
            path: 'role',
            loadChildren: () =>
              import('./features/manage-role/module').then(
                M => M.ManageRoleModule,
              ),
          },
          {
            path: 'idp',
            loadChildren: () =>
              import('./features/idp-option/module').then(
                M => M.IDPOptionModule,
              ),
          },
          {
            path: 'audit',
            loadChildren: () =>
              import('./features/manage-audit/module').then(
                M => M.ManageAuditModule,
              ),
          },
          {
            path: 'license',
            loadChildren: () =>
              import('./features/manage-license/module').then(
                M => M.ManageLicenseModule,
              ),
          },
          {
            path: 'meter',
            loadChildren: () =>
              import('./features/manage-meter/module').then(
                M => M.ManageMeterModule,
              ),
          },
          {
            path: 'product',
            loadChildren: () =>
              import('./features/product-center/module').then(
                M => M.ProductCenterModule,
              ),
          },
          {
            path: '',
            redirectTo: 'cluster',
            pathMatch: 'full',
          },
          {
            path: '**',
            redirectTo: 'cluster',
          },
        ],
      },
    ]),
  ],
})
export class ManagePlatformRoutingModule {}
