import { PageModule, PlatformNavModule } from '@alauda/ui';
import { PortalModule } from '@angular/cdk/portal';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';

import { ManagePlatformComponent } from './component';
import { ManagePlatformRoutingModule } from './routing.module';

@NgModule({
  imports: [
    PageModule,
    PlatformNavModule,
    SharedModule,
    PortalModule,
    ManagePlatformRoutingModule,
  ],
  declarations: [ManagePlatformComponent],
  exports: [RouterModule],
})
export class ManagePlatformModule {}
