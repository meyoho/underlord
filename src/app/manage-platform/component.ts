import { publishRef } from '@alauda/common-snippet';
import { animate, style, transition, trigger } from '@angular/animations';
import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { NavigationEnd } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { distinctUntilChanged, filter, takeUntil } from 'rxjs/operators';

import { BaseLayoutComponent } from 'app/base-layout-component';
import { Cluster } from 'app/typings';

/**
 * 白名单内的 url 路径会展示集群选择组件
 * 目前只在 feature 列表页响应集群变化事件
 * feature 的列表页路由需要是 xxx/list 格式
 */
const CLUSTER_FEATURE_URL_WHITELIST = [
  '/resource_management/list',
  '/cluster_addon/list',
];
@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('zoomInOut', [
      transition('* => *', [
        style({ transform: 'scale(0.99) translateY(5px)', opacity: '0.1' }),
        animate(
          '0.3s ease',
          style({ transform: 'scale(1) translateY(0)', opacity: '1' }),
        ),
      ]),
    ]),
  ],
})
export class ManagePlatformComponent extends BaseLayoutComponent {
  navConfigAddress = 'custom/navconfig-platform.yaml';
  isLocalNavConfig = false;

  shouldShowClusterBadge = true;
  private readonly cluster$$ = new BehaviorSubject<Cluster>(null);
  private readonly cluster$ = this.cluster$$.asObservable().pipe(
    filter(_ => !!_),
    distinctUntilChanged(),
    publishRef(),
  );

  constructor(protected injector: Injector) {
    super(injector);
    this.initRouterObserver();
    // 确保在页面没有其他订阅的情况下，保持第一次获得的值
    this.cluster$.pipe(takeUntil(this.onDestroy$)).subscribe();
  }

  getCluster$() {
    return this.cluster$;
  }

  getCluster() {
    return this.cluster$$.value;
  }

  onClusterChange(cluster: Cluster) {
    this.cluster$$.next(cluster);
  }

  private initRouterObserver() {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(({ urlAfterRedirects }: NavigationEnd) => {
        if (
          CLUSTER_FEATURE_URL_WHITELIST.find(url =>
            urlAfterRedirects.includes(url),
          )
        ) {
          this.shouldShowClusterBadge = true;
        } else {
          this.shouldShowClusterBadge = false;
        }
      });
  }
}
