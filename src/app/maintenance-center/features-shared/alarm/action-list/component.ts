import { Component, Input, OnInit } from '@angular/core';

import { TYPE } from 'app/utils';

@Component({
  selector: 'alu-alarm-action-list',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class AlarmActionListComponent implements OnInit {
  @Input()
  notifications: string[];

  columns = [TYPE, 'resource'];
  actions: Array<{
    type: string;
    resource: string[];
  }> = [];

  ngOnInit() {
    if (this.notifications.length > 0) {
      this.actions = [
        {
          type: 'notification',
          resource: this.notifications,
        },
      ];
    }
  }

  trackByFn(index: number) {
    return index;
  }
}
