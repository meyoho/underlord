import { DIALOG_DATA } from '@alauda/ui';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import {
  debounceTime,
  distinctUntilKeyChanged,
  startWith,
} from 'rxjs/operators';

import { IndicatorType } from 'app/api/alarm/metric.service';
import { PrometheusRuleItem } from 'app/typings';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class PrometheusAlarmRuleDialogComponent implements OnInit {
  @Output()
  close: EventEmitter<boolean | object> = new EventEmitter();

  formSubmitText = 'add';
  rule: PrometheusRuleItem;
  rule$: Observable<PrometheusRuleItem>;
  ruleChanged: Subject<PrometheusRuleItem> = new Subject<PrometheusRuleItem>();

  constructor(
    @Inject(DIALOG_DATA)
    public modalData: {
      kind: string;
      cluster: string;
      isOCP: boolean;
      node: string;
      hostname: string;
      application: string;
      project: string;
      namespace: string;
      podName: string;
      metricType: IndicatorType[];
      data: PrometheusRuleItem;
    },
  ) {
    this.rule$ = this.ruleChanged.pipe(
      startWith(this.modalData.data || ({} as PrometheusRuleItem)),
      debounceTime(500),
      distinctUntilKeyChanged('expr'),
    );
  }

  ngOnInit() {
    if (this.modalData.data) {
      this.formSubmitText = 'edit';
      this.rule = this.modalData.data;
    }
  }

  onRuleChange(rule: PrometheusRuleItem) {
    this.ruleChanged.next(rule);
  }

  confirm(ruleForm: NgForm) {
    ruleForm.onSubmit(null);
    if (ruleForm.invalid) {
      return;
    }
    this.close.next(this.rule);
  }

  cancel() {
    this.close.next(false);
  }
}
