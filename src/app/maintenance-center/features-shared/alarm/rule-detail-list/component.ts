/* eslint-disable @typescript-eslint/no-unused-vars */
import { StringMap, TranslateService } from '@alauda/common-snippet';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { forkJoin } from 'rxjs';

import { IndicatorType, MetricService } from 'app/api/alarm/metric.service';
import {
  getAlarmTypeTitle,
  getPrometheusRule,
} from 'app/maintenance-center/features/alarm/alarm.util';
import { PrometheusRuleItem } from 'app/typings';
import { STATUS, getAlarmStatus } from 'app/utils';

interface Dictionary<T> {
  [key: string]: T;
}

interface PrometheusRuleItemParsed extends PrometheusRuleItem {
  labelsParsed?: StringMap;
  annotationsParsed?: StringMap;
}

@Component({
  selector: 'alu-alarm-detail-rule-list',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  animations: [
    trigger('expand', [
      state('*', style({ height: 0 })),
      state('expanded', style({ height: '*' })),
      transition('* => expanded', [animate(250)]),
      transition('expanded => *', [animate(250)]),
    ]),
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PrometheusDetailRuleListComponent implements OnInit {
  @Input()
  rules: PrometheusRuleItemParsed[];

  @Input()
  cluster: string;

  metricType: IndicatorType[];
  labels: StringMap;
  annotations: StringMap;

  columns = ['id', 'rule', 'alarm_type', 'level', STATUS, 'detail'];
  getAlarmStatus = getAlarmStatus;
  getPrometheusRule = getPrometheusRule;
  getAlarmTypeTitle = getAlarmTypeTitle;

  constructor(
    public translate: TranslateService,
    private readonly cdr: ChangeDetectorRef,
    private readonly metricService: MetricService,
  ) {}

  rowExpanded: Dictionary<boolean> = {};

  ngOnInit() {
    forkJoin([
      this.metricService.getIndicators(this.cluster),
      this.metricService.getPrometheusMetricLabels(this.cluster, 'ALERTS{}'),
    ]).subscribe(([metricType, labels]) => {
      this.metricType = metricType;
      this.setStatus(labels);
      this.cdr.markForCheck();
    });
  }

  setStatus(
    alarmState: Array<{
      metric: {
        alertname: string;
        alertstate: string;
      };
    }>,
  ) {
    this.rules.forEach(rule => {
      const found = alarmState
        ? alarmState.find(el => {
            return el.metric.alertname === rule.alert;
          })
        : null;
      rule.labels = {
        ...rule.labels,
        alarm_status: found ? found.metric.alertstate : '',
      };
      if (rule.labels) {
        const {
          severity,
          application,
          alert_name,
          alert_involved_object_kind,
          alert_involved_object_name,
          alert_involved_object_namespace,
          alert_cluster,
          alert_project,
          alert_creator,
          alert_indicator,
          alert_indicator_aggregate_range,
          alert_indicator_aggregate_function,
          alert_indicator_comparison,
          alert_indicator_threshold,
          alert_indicator_query,
          alert_indicator_unit,
          alarm_status,
          ...labelsParsed
        } = rule.labels;
        rule.labelsParsed = labelsParsed;
      }
      if (rule.annotations) {
        const {
          alert_current_value,
          alert_notifications,
          ...annotationsParsed
        } = rule.annotations;
        rule.annotationsParsed = annotationsParsed;
      }
    });
  }

  toggleRow(id: string) {
    this.rowExpanded = {
      [id]: !this.rowExpanded[id],
    };
  }
}
