import { LABELS } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import dayjs from 'dayjs';
import Highcharts, { Options, SeriesLineOptions } from 'highcharts';
import { get } from 'lodash-es';

import {
  IndicatorType,
  Metric,
  MetricService,
} from 'app/api/alarm/metric.service';
import {
  getMetricNumericOptions,
  getMetricPercentOptions,
  parseMetricsResponse,
} from 'app/maintenance-center/features-shared/utils';
import {
  getPrometheusExpr,
  getUnitType,
  parseChartOptions,
} from 'app/maintenance-center/features/alarm/alarm.util';
import { PrometheusRuleItem } from 'app/typings';

@Component({
  selector: 'alu-base-alarm-metric-chart',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AlarmMetricChartComponent implements OnInit, OnChanges {
  @Input()
  rule: PrometheusRuleItem;

  @Input()
  metricType: IndicatorType[];

  private endTime: number;
  private step: number;

  chartLoading = false;
  Highcharts = Highcharts;
  chartPercentOptions: Options = getMetricPercentOptions(false);

  chartNumericOptions: Options = getMetricNumericOptions(false);

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly metricService: MetricService,
  ) {}

  ngOnInit() {
    parseChartOptions(this.chartPercentOptions);
    parseChartOptions(this.chartNumericOptions);
  }

  ngOnChanges({ rule }: SimpleChanges) {
    if (rule && rule.currentValue) {
      const unit = get(rule, ['currentValue', LABELS, 'alert_indicator_unit']);
      if (unit) {
        this.chartNumericOptions.tooltip.valueSuffix = unit;
      } else {
        const unitType = this.getUnitType(
          get(rule, ['currentValue', LABELS, 'alert_indicator']),
        );
        if (!unitType) {
          this.chartNumericOptions.tooltip.valueDecimals = 2;
          this.chartNumericOptions.tooltip.valueSuffix = '';
        } else {
          this.chartNumericOptions.tooltip.valueDecimals = 4;
          this.chartNumericOptions.tooltip.valueSuffix = unitType;
        }
      }
      this.loadChart(rule.currentValue);
    }
  }

  private loadChart(rule: PrometheusRuleItem) {
    if (!this.rule.expr) {
      return;
    }
    const currentTime = parseInt((dayjs().valueOf() / 1000).toFixed(0), 10);
    const name = get(rule, [LABELS, 'alert_involved_object_name']);
    const cluster = get(rule, [LABELS, 'alert_cluster']);
    const range = parseInt(
      get(rule, [LABELS, 'alert_indicator_aggregate_range']),
      10,
    );
    const aggregator = get(rule, [
      LABELS,
      'alert_indicator_aggregate_function',
    ]);
    this.step = range || 300;
    this.endTime = currentTime;
    this.chartLoading = true;
    this.chartPercentOptions.series = [];
    this.chartNumericOptions.series = [];
    this.cdr.markForCheck();
    this.metricService
      .queryMetrics(cluster, {
        start: currentTime - this.step * 29,
        end: currentTime,
        step: this.step,
        queries: [
          {
            aggregator,
            range,
            labels: [
              {
                name: '__name__',
                value: 'custom',
              },
              {
                name: 'expr',
                value: getPrometheusExpr(this.rule),
              },
            ],
          },
        ],
      })
      .then(
        (result: Metric[]) => {
          if (result) {
            if (this.percentFlag()) {
              this.chartPercentOptions.series = parseMetricsResponse(
                result,
                name,
                this.endTime,
                this.step,
                100,
              ) as SeriesLineOptions[];
              this.chartNumericOptions.series = [];
            } else {
              this.chartNumericOptions.series = parseMetricsResponse(
                result,
                name,
                this.endTime,
                this.step,
              ) as SeriesLineOptions[];
              this.chartPercentOptions.series = [];
            }
            this.chartLoading = false;
            this.cdr.markForCheck();
          }
        },
        error => {
          this.chartLoading = false;
          this.cdr.markForCheck();
          throw error;
        },
      );
  }

  private percentFlag() {
    if (
      get(this.rule, [LABELS, 'alert_indicator_unit']) === '%' ||
      this.getUnitType(get(this.rule, [LABELS, 'alert_indicator'])) === '%'
    ) {
      return true;
    }
    return false;
  }

  private getUnitType(metric: string) {
    return getUnitType(metric, this.metricType);
  }
}
