import { K8sApiService } from '@alauda/common-snippet';
import { Component, Injector, OnInit } from '@angular/core';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { of } from 'rxjs';
import { catchError, map, pluck } from 'rxjs/operators';

import { K8sUtilService } from 'app/services/k8s-util.service';
import { AlarmAction, Notification } from 'app/typings';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';
interface AlarmActionFormModel {
  type: string;
  actions: AlarmAction[];
}

@Component({
  selector: 'alu-alarm-action-form',
  templateUrl: 'template.html',
})
export class AlarmActionFormComponent
  extends BaseResourceFormArrayComponent<AlarmAction, AlarmActionFormModel>
  implements OnInit {
  notifications: AlarmAction[];
  trackFn = (val: AlarmAction) => val.name;
  filterFn = (
    filter: string,
    option: {
      value: AlarmAction;
    },
  ) => option.value.name.includes(filter);

  constructor(
    injector: Injector,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
  ) {
    super(injector);
  }

  ngOnInit() {
    this.k8sApi
      .getGlobalResourceList<Notification>({
        type: RESOURCE_TYPES.NOTIFICATION,
        namespaced: true,
        queryParams: {
          labelSelector: `${this.k8sUtil.normalizeType('version')}!=1.0`,
        },
      })
      .pipe(
        pluck('items'),
        map(notifications =>
          notifications.map(notification => ({
            name: notification.metadata.name,
            namespace: notification.metadata.namespace,
          })),
        ),
        catchError(() => of([])),
      )
      .subscribe(notifications => {
        this.notifications = notifications;
      });
  }

  getOnFormArrayResizeFn() {
    return () =>
      this.fb.group({
        type: ['notification'],
        actions: [[]],
      });
  }

  adaptResourceModel(resource: AlarmAction[]): AlarmActionFormModel[] {
    if (resource && resource.length > 0) {
      return [
        {
          type: 'notification',
          actions: resource.reduce((acc, cur) => {
            return acc.concat(cur);
          }, []),
        },
      ];
    }
  }

  adaptFormModel(formModel: AlarmActionFormModel[]): AlarmAction[] {
    if (formModel && formModel.length > 0) {
      return formModel.reduce((acc, cur) => {
        return acc
          .concat(cur.actions)
          .filter(
            (notification, index, self) =>
              self.findIndex(t => t.name === notification.name) === index,
          );
      }, []);
    }
  }
}
