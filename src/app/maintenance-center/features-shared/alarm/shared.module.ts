import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';

import { AlarmActionFormComponent } from './action-form/component';
import { AlarmActionListComponent } from './action-list/component';
import { AlarmMetricChartComponent } from './metric-chart/component';
import { BaseAlarmRuleFormComponent } from './rule-base-form/component';
import { PrometheusDetailChartComponent } from './rule-chart/component';
import { PrometheusDetailRuleListComponent } from './rule-detail-list/component';
import { PrometheusAlarmRuleDialogComponent } from './rule-dialog/component';
import { PrometheusAlarmRuleFormComponent } from './rule-form/component';
import { PrometheusAlarmRuleListComponent } from './rule-list/component';
import { AlarmStatusBarComponent } from './status-bar/component';

const components = [
  AlarmActionFormComponent,
  AlarmActionListComponent,
  BaseAlarmRuleFormComponent,
  AlarmStatusBarComponent,
  AlarmMetricChartComponent,
  PrometheusDetailChartComponent,
  PrometheusDetailRuleListComponent,
  PrometheusAlarmRuleDialogComponent,
  PrometheusAlarmRuleFormComponent,
  PrometheusAlarmRuleListComponent,
];

@NgModule({
  imports: [RouterModule, SharedModule],
  declarations: components,
  exports: components,
  entryComponents: [PrometheusAlarmRuleDialogComponent],
})
export class AlarmSharedModule {}
