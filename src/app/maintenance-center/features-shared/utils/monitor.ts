import { AxisTypeValue } from 'highcharts';

import { Metric } from 'app/api/alarm/metric.service';

export const LEVELS = ['Critical', 'High', 'Medium', 'Low'];

export const MONITOR_COLOR_SET = [
  '#7cb5ec',
  '#61d44a',
  '#434348',
  '#f7a35c',
  '#8085e9',
  '#f15c80',
  '#e4d354',
  '#2b908f',
  '#f45b5b',
  '#08dccb',
];

export const AGGREGATORS = [
  {
    key: 'avg',
    name: 'aggregator_avg',
  },
  {
    key: 'max',
    name: 'aggregator_max',
  },
  {
    key: 'min',
    name: 'aggregator_min',
  },
];

export const MODES = [
  {
    key: 'workload',
    name: 'workload',
  },
  {
    key: 'pod',
    name: 'Pod',
  },
  {
    key: 'container',
    name: 'container',
  },
];

export const UNITS = [
  '%',
  'byte/second',
  'byte',
  'kb',
  'mb',
  'gb',
  'core',
  's',
  'ms',
];

export type QueryMetricsSeries =
  | 'cpuChartOptions'
  | 'memChartOptions'
  | 'sentBytesChartOptions'
  | 'receivedBytesChartOptions';

export interface QueryMetric {
  metric: string;
  series: QueryMetricsSeries;
  unit?: string;
  componentName?: string;
}

export const QueryMetrics: QueryMetric[] = [
  {
    metric: 'cpu.utilization',
    series: 'cpuChartOptions',
    unit: '%',
  },
  {
    metric: 'memory.utilization',
    series: 'memChartOptions',
    unit: '%',
  },
  {
    metric: 'network.transmit_bytes',
    series: 'sentBytesChartOptions',
  },
  {
    metric: 'network.receive_bytes',
    series: 'receivedBytesChartOptions',
  },
];

export const TIME_STAMP_OPTIONS = [
  {
    type: 'last_30_minutes',
    offset: 30 * 60 * 1000,
  },
  {
    type: 'last_hour',
    offset: 60 * 60 * 1000,
  },
  {
    type: 'last_6_hours',
    offset: 6 * 3600 * 1000,
  },
  {
    type: 'last_day',
    offset: 24 * 3600 * 1000,
  },
  {
    type: 'last_3_days',
    offset: 3 * 24 * 3600 * 1000,
  },
  {
    type: 'last_7_days',
    offset: 7 * 24 * 3600 * 1000,
  },
];

export const TIME_STAMP_OPTIONS_CUSTOM = [
  ...TIME_STAMP_OPTIONS,
  {
    type: 'custom_time_range',
    offset: 0,
  },
];

export function getMetricPercentOptions(shared = true) {
  return {
    chart: {
      type: 'line',
    },
    colors: MONITOR_COLOR_SET,
    credits: {
      enabled: false,
    },
    legend: {
      enabled: false,
    },
    title: {
      text: null as string,
    },
    tooltip: {
      valueDecimals: 4,
      valueSuffix: '%',
      xDateFormat: '%Y-%m-%d %H:%M:%S',
      shared,
    },
    plotOptions: {
      line: {
        connectNulls: true,
        marker: {
          enabled: false,
        },
      },
    },
    xAxis: {
      gridLineWidth: 0,
      dateTimeLabelFormats: {
        day: {
          main: '%m-%d',
        },
      },
      type: 'datetime' as AxisTypeValue,
      crosshair: true,
    },
    yAxis: {
      gridLineWidth: 0,
      title: {
        text: null as string,
      },
      labels: {
        formatter(this: { value: number }) {
          if (this.value > 0.01 || this.value === 0) {
            return this.value.toPrecision(3) + '%';
          }
          return this.value.toFixed(4) + '%';
        },
      },
      min: 0,
    },
    time: {
      useUTC: false,
    },
  };
}

export function getMetricNumericOptions(shared = true) {
  return {
    chart: {
      type: 'line',
    },
    colors: MONITOR_COLOR_SET,
    credits: {
      enabled: false,
    },
    legend: {
      enabled: false,
    },
    title: {
      text: null as string,
    },
    tooltip: {
      valueDecimals: 4,
      xDateFormat: '%Y-%m-%d %H:%M:%S',
      shared,
      valueSuffix: '',
    },
    plotOptions: {
      line: {
        connectNulls: true,
        marker: {
          enabled: false,
        },
      },
    },
    xAxis: {
      gridLineWidth: 0,
      dateTimeLabelFormats: {
        day: {
          main: '%m-%d',
        },
      },
      type: 'datetime' as AxisTypeValue,
      crosshair: true,
    },
    yAxis: {
      gridLineWidth: 0,
      title: {
        text: null as string,
      },
      minRange: 0.1,
      min: 0,
    },
    time: {
      useUTC: false,
    },
  };
}

function fillUpResult(
  values: Array<Array<number | string>>,
  endTime: number,
  step: number,
) {
  // fill in the chart when result is less than 30 points
  let dateNewest = endTime;
  const obj = {} as Record<string | number, string | number>;
  for (let i = 0; i < 30; i++) {
    obj[(dateNewest -= step)] = '';
  }
  if (values.length > 0) {
    values.forEach((element: Array<number | string>) => {
      obj[element[0]] = element[1];
    });
  }
  return Object.keys(obj).map(key => [Number(key), obj[key]]);
}

export function parseMetricsResponse(
  metric: Metric[],
  getName: string | Function,
  endTime: number,
  step: number,
  multiplier?: number,
) {
  let name = getName;
  return metric.map(el => {
    if (el.values.length < 30) {
      el.values = fillUpResult(el.values, endTime, step);
    }
    if (getName instanceof Function) {
      name = getName(el);
    } else {
      const metricMap = Object.entries(el.metric);
      if (metricMap.length > 0) {
        name = metricMap.map(([key, value]) => `${key}: ${value}`).join('<br>');
      }
    }
    return {
      name,
      data: el.values.map((value: Array<number | string>) => {
        let y = null;
        if (value[1] !== '+Inf' && value[1] !== '') {
          y = Number(value[1]);
          if (multiplier || el.metric.unit === '%') {
            y = y * 100;
          }
        }
        return {
          x: Number(value[0]) * 1000,
          y,
        };
      }),
    };
  });
}
