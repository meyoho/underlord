import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NotificationReceiverListComponent } from './list/component';

const routes: Routes = [
  {
    path: '',
    component: NotificationReceiverListComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotificationReceiverRoutingModule {}
