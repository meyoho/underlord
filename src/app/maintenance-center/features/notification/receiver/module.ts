import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { NotificationReceiverListComponent } from './list/component';
import { NotificationReceiverRoutingModule } from './routing.module';

@NgModule({
  imports: [SharedModule, NotificationReceiverRoutingModule],
  declarations: [NotificationReceiverListComponent],
})
export class NotificationReceiverModule {}
