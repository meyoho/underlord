import {
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  NAME,
  TranslateService,
  matchLabelsToString,
  noop,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import { Component } from '@angular/core';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { Md5 } from 'ts-md5';

import { K8sUtilService } from 'app/services/k8s-util.service';
import { NotificationReceiverFormComponent } from 'app/shared/features/notification/receiver-form/component';
import { NotificationReceiver } from 'app/typings';
import { ACTION, RESOURCE_TYPES, ResourceType, TYPE } from 'app/utils';

import { getTemplateTypeTranslateKey } from '../../util';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class NotificationReceiverListComponent {
  searchType$ = new BehaviorSubject('object_name');
  searchKey$ = new BehaviorSubject('');
  columns = [NAME, TYPE, 'create_time', ACTION];
  getTemplateTypeTranslateKey = getTemplateTypeTranslateKey;
  fetchParams$ = combineLatest([this.searchKey$, this.searchType$]).pipe(
    map(([searchKey, searchType]) => {
      if (!searchKey) {
        return null;
      }
      let labelSelector = '';
      if (searchType === 'object_name') {
        labelSelector = matchLabelsToString({
          [this.k8sUtil.normalizeType('destination-md5')]: Md5.hashStr(
            searchKey,
          ).toString(),
        });
      } else if (searchType === 'display_name') {
        labelSelector = matchLabelsToString({
          [this.k8sUtil.normalizeType('display-name-md5')]: Md5.hashStr(
            searchKey,
          ).toString(),
        });
      }
      return {
        labelSelector,
      };
    }),
    publishRef(),
  );

  list = new K8SResourceList({
    fetcher: this.fetcher.bind(this),
    fetchParams$: this.fetchParams$,
  });

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.NOTIFICATION_RECEIVER,
    action: [
      K8sResourceAction.CREATE,
      K8sResourceAction.UPDATE,
      K8sResourceAction.DELETE,
    ],
  });

  constructor(
    private readonly dialog: DialogService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly translate: TranslateService,
    private readonly messageService: MessageService,
  ) {}

  fetcher({ ...queryParams }) {
    return this.k8sApi.getGlobalResourceList<NotificationReceiver>({
      type: RESOURCE_TYPES.NOTIFICATION_RECEIVER,
      namespaced: true,
      queryParams,
    });
  }

  create() {
    const dialogRef = this.dialog.open(NotificationReceiverFormComponent);
    dialogRef.componentInstance.close.pipe(first()).subscribe(res => {
      dialogRef.close();
      if (res) {
        this.list.reload();
      }
    });
  }

  update(receiver: NotificationReceiver) {
    const dialogRef = this.dialog.open(NotificationReceiverFormComponent, {
      data: {
        receiver,
      },
    });
    dialogRef.componentInstance.close.pipe(first()).subscribe(res => {
      dialogRef.close();
      if (res) {
        this.list.update(res);
      }
    });
  }

  delete(data: NotificationReceiver) {
    this.dialog
      .confirm({
        title: this.translate.get('delete_notification_receiver_title'),
        content: `${data.spec.destination}（${this.k8sUtil.getDisplayName(
          data,
        )}）`,
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) => {
          this.k8sApi
            .deleteGlobalResource({
              type: RESOURCE_TYPES.NOTIFICATION_RECEIVER,
              namespaced: true,
              resource: data,
            })
            .subscribe(() => {
              resolve();
              this.messageService.success(this.translate.get('delete_success'));
            }, reject);
        },
      })
      .then(() => this.list.delete(data))
      .catch(noop);
  }

  getPlaceHolder(type: string) {
    switch (type) {
      case 'object_name':
        return 'search_by_object_name_placeholder';
      case 'display_name':
        return 'search_by_display_name_placeholder';
    }
  }
}
