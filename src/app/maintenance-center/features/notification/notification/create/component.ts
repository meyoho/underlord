import {
  DESCRIPTION,
  DISPLAY_NAME,
  TOKEN_GLOBAL_NAMESPACE,
} from '@alauda/common-snippet';
import { Component, Inject, OnInit } from '@angular/core';

import { K8sUtilService } from 'app/services/k8s-util.service';
import { Notification } from 'app/typings';
import { NotificationMeta } from 'app/utils';

@Component({
  template: `
    <alu-notification-form [ngModel]="notification"></alu-notification-form>
  `,
})
export class CreateNotificationComponent implements OnInit {
  notification: Notification;

  constructor(
    private readonly k8sUtil: K8sUtilService,
    @Inject(TOKEN_GLOBAL_NAMESPACE) private readonly globalNamespace: string,
  ) {}

  ngOnInit() {
    this.notification = this.getDefaultValue();
  }

  getDefaultValue(): Notification {
    return {
      apiVersion: NotificationMeta.apiVersion,
      kind: NotificationMeta.kind,
      metadata: {
        name: '',
        namespace: this.globalNamespace,
        annotations: {
          [this.k8sUtil.normalizeType(DESCRIPTION)]: '',
          [this.k8sUtil.normalizeType(DISPLAY_NAME)]: '',
        },
      },
    };
  }
}
