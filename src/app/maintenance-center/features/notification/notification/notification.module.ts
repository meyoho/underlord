import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { CreateNotificationComponent } from './create/component';
import { NotificationDetailComponent } from './detail/component';
import { NotificationFormComponent } from './form/component';
import { NotificationMethodItemComponent } from './form/method-item/component';
import { NotificationMethodComponent } from './form/method/component';
import { NotificationListComponent } from './list/component';
import { NotificationRoutingModule } from './notification-routing.module';
import { UpdateNotificationComponent } from './update/component';

@NgModule({
  imports: [SharedModule, NotificationRoutingModule],
  declarations: [
    CreateNotificationComponent,
    UpdateNotificationComponent,
    NotificationListComponent,
    NotificationDetailComponent,
    NotificationFormComponent,
    NotificationMethodComponent,
    NotificationMethodItemComponent,
  ],
})
export class NotificationModule {}
