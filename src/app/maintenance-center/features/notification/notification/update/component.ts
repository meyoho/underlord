import { K8sApiService, NAME, publishRef } from '@alauda/common-snippet';
import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EMPTY } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { Notification } from 'app/typings';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';

@Component({
  template: `
    <alu-notification-form
      [ngModel]="data$ | async"
      [update]="true"
    ></alu-notification-form>
  `,
  preserveWhitespaces: false,
})
export class UpdateNotificationComponent {
  params$ = this.route.paramMap.pipe(
    map(params => ({
      name: params.get(NAME),
    })),
    publishRef(),
  );

  data$ = this.params$.pipe(
    switchMap(params => {
      return this.k8sApi
        .getGlobalResource<Notification>({
          type: RESOURCE_TYPES.NOTIFICATION,
          namespaced: true,
          name: params.name,
        })
        .pipe(
          catchError(() => {
            this.location.back();
            return EMPTY;
          }),
        );
    }),
  );

  constructor(
    private readonly route: ActivatedRoute,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly location: Location,
  ) {}
}
