import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateNotificationComponent } from './create/component';
import { NotificationDetailComponent } from './detail/component';
import { NotificationListComponent } from './list/component';
import { UpdateNotificationComponent } from './update/component';

const notificationRoutes: Routes = [
  {
    path: '',
    component: NotificationListComponent,
  },
  {
    path: 'create',
    component: CreateNotificationComponent,
  },
  {
    path: 'update/:name',
    component: UpdateNotificationComponent,
  },
  {
    path: 'detail/:name',
    component: NotificationDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(notificationRoutes)],
  exports: [RouterModule],
})
export class NotificationRoutingModule {}
