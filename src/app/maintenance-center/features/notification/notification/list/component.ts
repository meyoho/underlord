import {
  DESCRIPTION,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  NAME,
  matchLabelsToString,
  stringToMatchLabels,
} from '@alauda/common-snippet';
import { DialogRef, DialogService } from '@alauda/ui';
import { Component, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map, tap } from 'rxjs/operators';

import { ConfirmDeleteComponent } from 'app/shared/components/confirm-delete/confirm-delete.component';
import { Notification } from 'app/typings';
import { ACTION, RESOURCE_TYPES, ResourceType } from 'app/utils';

@Component({
  templateUrl: './template.html',
})
export class NotificationListComponent {
  resource: Notification;
  context = this;
  deleteDialogRef: DialogRef<ConfirmDeleteComponent>;
  list = new K8SResourceList({
    fetcher: this.fetchNotificationList.bind(this),
    activatedRoute: this.activatedRoute,
  });

  columns = [NAME, DESCRIPTION, 'creator', 'create_time', ACTION];

  searchKey$ = this.activatedRoute.queryParamMap.pipe(
    map(
      params =>
        stringToMatchLabels(params.get('fieldSelector'))['metadata.name'],
    ),
  );

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.NOTIFICATION,
    action: [
      K8sResourceAction.CREATE,
      K8sResourceAction.UPDATE,
      K8sResourceAction.DELETE,
    ],
  });

  constructor(
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sPermission: K8sPermissionService,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly dialogService: DialogService,
  ) {}

  fetchNotificationList({ ...queryParams }) {
    return this.k8sApi.getGlobalResourceList<Notification>({
      type: RESOURCE_TYPES.NOTIFICATION,
      namespaced: true,
      queryParams: {
        ...queryParams,
        labelSelector: 'alauda.io/version!=1.0',
      },
    });
  }

  searchByName(name: string) {
    this.router.navigate(['./'], {
      queryParams: {
        fieldSelector: matchLabelsToString({
          'metadata.name': name,
        }),
      },
      queryParamsHandling: 'merge',
      relativeTo: this.activatedRoute,
    });
  }

  trackByFn(index: number) {
    return index;
  }

  update(data: Notification) {
    this.router.navigate([
      '/maintenance-center/notification/update',
      data.metadata.name,
    ]);
  }

  delete(
    templateRef: TemplateRef<ConfirmDeleteComponent>,
    resource: Notification,
  ) {
    this.resource = resource;
    this.deleteDialogRef = this.dialogService.open(templateRef);
  }

  deleteNotification() {
    return this.k8sApi
      .deleteGlobalResource({
        type: RESOURCE_TYPES.NOTIFICATION,
        namespaced: true,
        resource: this.resource,
      })
      .pipe(tap(() => this.list.delete(this.resource)));
  }
}
