import {
  DESCRIPTION,
  DISPLAY_NAME,
  K8sApiService,
  TranslateService,
} from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import { Location } from '@angular/common';
import { Component, Injector, Input } from '@angular/core';
import { AbstractControl, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { finalize } from 'rxjs/operators';

import { K8sUtilService } from 'app/services/k8s-util.service';
import { Notification, NotificationSubscription } from 'app/typings';
import {
  K8S_RESOURCE_NAME_BASE,
  RESOURCE_TYPES,
  ResourceType,
} from 'app/utils';

import { Method } from '../../util';

@Component({
  selector: 'alu-notification-form',
  templateUrl: 'template.html',
  styles: [
    `
      .resource-form {
        display: block;
        margin-bottom: 52px;
      }
    `,
  ],
})
export class NotificationFormComponent extends BaseResourceFormGroupComponent {
  @Input()
  update: boolean;

  submitting: boolean;
  description = this.k8sUtil.normalizeType(DESCRIPTION);
  displayName = this.k8sUtil.normalizeType(DISPLAY_NAME);
  K8S_RESOURCE_NAME_BASE = K8S_RESOURCE_NAME_BASE;

  constructor(
    public injector: Injector,
    private readonly location: Location,
    private readonly translate: TranslateService,
    private readonly k8sUtil: K8sUtilService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly messageService: MessageService,
    private readonly router: Router,
  ) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      apiVersion: [''],
      kind: [''],
      metadata: this.fb.group({
        annotations: this.fb.group({
          [this.description]: [''],
          [this.displayName]: ['', Validators.required],
        }),
        name: [
          '',
          [
            Validators.required,
            Validators.pattern(K8S_RESOURCE_NAME_BASE.pattern),
          ],
        ],
        namespace: [''],
        resourceVersion: [''],
      }),
      spec: this.fb.group({
        subscriptions: [
          '',
          Validators.compose([Validators.required, this.validateMethodItem]),
        ],
      }),
    });
  }

  getDefaultFormModel() {
    return {};
  }

  validateMethodItem(ctrl: AbstractControl) {
    if (ctrl.value && ctrl.value.length > 0) {
      const found = ctrl.value.find((el: NotificationSubscription) => {
        if (el.method === Method.DINGTALK || el.method === Method.WECHAT) {
          return el.receivers.length <= 0 || !el.template;
        }
        return el.receivers.length <= 0 || !el.sender || !el.template;
      });
      if (found) {
        return {
          itemrequired: true,
        };
      }
    }
    return null;
  }

  confirm(form: NgForm) {
    form.onSubmit(null);
    if (form.invalid) {
      return;
    }
    if (this.checkDuplicates(form.value.spec.subscriptions)) {
      this.messageService.error(
        this.translate.get('notification_method_duplicate'),
      );
      return;
    }
    this.submitting = true;
    this.k8sApi[this.update ? 'putGlobalResource' : 'postGlobalResource']<
      Notification
    >({
      type: RESOURCE_TYPES.NOTIFICATION,
      namespaced: true,
      resource: form.value,
    })
      .pipe(
        finalize(() => {
          this.submitting = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(notification => {
        this.messageService.success(
          this.translate.get(this.update ? 'update_success' : 'create_success'),
        );
        this.router.navigate([
          '/maintenance-center/notification/detail',
          notification.metadata.name,
        ]);
      });
  }

  cancel() {
    this.location.back();
  }

  checkDuplicates(subscriptions: NotificationSubscription[]) {
    const subscriptionMethods = subscriptions.map(
      subscription => subscription.method,
    );
    return new Set(subscriptionMethods).size < subscriptions.length;
  }
}
