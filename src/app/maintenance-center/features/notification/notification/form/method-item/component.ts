import { ObservableInput } from '@alauda/common-snippet';
import {
  AfterViewInit,
  Component,
  EventEmitter,
  Injector,
  Input,
  Output,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Observable, ReplaySubject, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import { K8sUtilService } from 'app/services/k8s-util.service';
import {
  NotificationReceiver,
  NotificationSubscription,
  NotificationSubscriptionFormModel,
  NotificationTemplate,
  Secret,
} from 'app/typings';
import { TYPE } from 'app/utils';

import { Method, types } from '../../../util';

@Component({
  selector: 'alu-notification-method-item',
  templateUrl: './template.html',
  styleUrls: ['styles.scss'],
})
export class NotificationMethodItemComponent
  extends BaseResourceFormGroupComponent<
    NotificationSubscription,
    NotificationSubscriptionFormModel
  >
  implements AfterViewInit {
  methods = types;

  @ObservableInput()
  @Input('receivers')
  receivers$: Observable<NotificationReceiver[]>;

  @ObservableInput()
  @Input('templates')
  templates$: Observable<NotificationTemplate[]>;

  @ObservableInput()
  @Input('senders')
  senders$: Observable<Secret[]>;

  @Output()
  addNotificationObject: EventEmitter<string> = new EventEmitter();

  methodChange$ = new ReplaySubject<string>(1);
  receivers: NotificationReceiver[];
  templates: NotificationTemplate[];
  senders: Secret[];
  filterReceivers: NotificationReceiver[] = [];
  filterReceivers$ = combineLatest([this.receivers$, this.methodChange$]).pipe(
    map(([list, method]) => {
      this.filterReceivers = this.filterReceiver(
        list,
        method,
      ) as NotificationReceiver[];
      return this.filterReceivers;
    }),
  );

  filterTemplates$ = combineLatest([this.templates$, this.methodChange$]).pipe(
    map(([list, method]) => this.filterTemplate(list, method)),
  );

  filterSenders$ = combineLatest([this.senders$, this.methodChange$]).pipe(
    map(([list, method]) => this.filterReceiver(list, method)),
  );

  filterTemplates: NotificationTemplate[] = [];
  filterSenders: Secret[] = [];

  templateTrackFn = (val: string) =>
    this.filterTemplates.length > 0 ? val : '';

  senderTrackFn = (val: string) => (this.filterSenders.length > 0 ? val : '');
  constructor(injector: Injector, private readonly k8sUtil: K8sUtilService) {
    super(injector);
  }

  // eslint-disable-next-line sonarjs/cognitive-complexity
  ngAfterViewInit() {
    super.ngAfterViewInit();
    this.filterTemplates$.subscribe(templates => {
      this.filterTemplates = templates;
      if (this.form.get('template').value === '' && templates.length > 0) {
        this.form.get('template').setValue(templates[0].metadata.name);
      }
    });
    this.filterSenders$.subscribe(senders => {
      this.filterSenders = senders;
      if (this.form.get('sender').value === '' && senders.length > 0) {
        this.form.get('sender').setValue(senders[0].metadata.name);
      }
    });
    this.form.get('method').valueChanges.subscribe(method => {
      this.methodChange$.next(method);
      let sender = '';
      if (
        method !== Method.EMAIL &&
        method !== Method.SMS &&
        method !== Method.WEBHOOK
      ) {
        this.form.get('sender').disable();
      } else {
        this.form.get('sender').enable();
        if (this.filterSenders.length > 0) {
          sender = this.filterSenders[0].metadata.name;
        }
      }
      if (method === Method.WEBHOOK) {
        sender = 'default-webhook-sender';
      }
      this.form.patchValue({
        receivers: '',
        sender,
        template:
          this.filterTemplates.length > 0
            ? this.filterTemplates[0].metadata.name
            : '',
      });
    });
    const method = this.form.get('method').value;
    if (
      method !== Method.EMAIL &&
      method !== Method.SMS &&
      method !== Method.WEBHOOK
    ) {
      this.form.get('sender').disable();
    } else {
      this.form.get('sender').enable();
    }
  }

  createForm() {
    return this.fb.group({
      method: ['', Validators.required],
      receivers: ['', Validators.required],
      sender: ['', Validators.required],
      template: ['', Validators.required],
    });
  }

  adaptFormModel(formModel: NotificationSubscriptionFormModel) {
    if (formModel) {
      return {
        ...formModel,
        receivers: (formModel.receivers || []).map(receiver => {
          const receiverFind = this.filterReceivers.find(
            _receiver => _receiver.metadata.name === receiver,
          );
          return {
            namespace: receiverFind ? receiverFind.metadata.namespace : '',
            name: receiver,
          };
        }),
      };
    }
  }

  adaptResourceModel(resource: NotificationSubscription) {
    if (resource) {
      this.methodChange$.next(resource.method);
      return {
        ...resource,
        receivers: (resource.receivers || []).map(receiver => receiver.name),
      };
    }
  }

  getDefaultFormModel() {
    return {} as NotificationSubscriptionFormModel;
  }

  filterReceiver = (
    items: Array<NotificationReceiver | Secret>,
    method: string,
  ): Array<NotificationReceiver | Secret> => {
    if (!items) {
      return [];
    }
    return items.filter(item => this.k8sUtil.getLabel(item, TYPE) === method);
  };

  filterTemplate = (items: NotificationTemplate[], method: string) => {
    if (!items) {
      return [];
    }
    if (method !== Method.EMAIL && method !== Method.SMS) {
      method = 'other';
    }
    return items.filter(item => this.k8sUtil.getLabel(item, TYPE) === method);
  };

  getReceiverLabel = (receiver: NotificationReceiver) => {
    return `${this.k8sUtil.getDisplayName(receiver) || ''}(${
      receiver.spec.destination
    })`;
  };

  getDisplayName = (template: NotificationTemplate) => {
    return `${
      this.k8sUtil.getDisplayName(template) || ''
    }(${this.k8sUtil.getName(template)})`;
  };

  getSenderName = (sender: Secret) => {
    if (sender.data && sender.data.username) {
      return atob(sender.data.username);
    } else {
      return sender.metadata.name;
    }
  };

  addObject() {
    this.addNotificationObject.emit(this.form.get('method').value);
  }
}
