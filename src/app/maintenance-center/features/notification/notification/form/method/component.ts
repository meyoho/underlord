import { K8sApiService } from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import { AfterViewInit, Component, Injector } from '@angular/core';
import { xor } from 'lodash-es';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { ReplaySubject } from 'rxjs';
import { first, switchMap } from 'rxjs/operators';

import { NotificationReceiverFormComponent } from 'app/shared/features/notification/receiver-form/component';
import {
  NotificationReceiver,
  NotificationSpec,
  NotificationSubscription,
  NotificationTemplate,
  Secret,
} from 'app/typings';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';

import { Method, types } from '../../../util';

@Component({
  selector: 'alu-notification-method',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class NotificationMethodComponent
  extends BaseResourceFormArrayComponent<NotificationSpec>
  implements AfterViewInit {
  methods = types;
  update$ = new ReplaySubject<string>(1);
  receivers$ = this.update$.pipe(
    switchMap(() => {
      return this.k8sApi.getGlobalResourceList<NotificationReceiver>({
        type: RESOURCE_TYPES.NOTIFICATION_RECEIVER,
        namespaced: true,
      });
    }),
  );

  senders$ = this.k8sApi.getGlobalResourceList<Secret>({
    type: RESOURCE_TYPES.SECRET,
    namespaced: true,
  });

  templates$ = this.k8sApi.getGlobalResourceList<NotificationTemplate>({
    type: RESOURCE_TYPES.NOTIFICATION_TEMPLATE,
  });

  constructor(
    injector: Injector,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly dialog: DialogService,
  ) {
    super(injector);
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
    this.update$.next();
  }

  getOnFormArrayResizeFn() {
    let method = Method.EMAIL;
    const remainMethods = xor(
      [
        Method.EMAIL,
        Method.SMS,
        Method.WEBHOOK,
        Method.DINGTALK,
        Method.WECHAT,
      ],
      this.form.value.map(
        (subscriptions: NotificationSubscription) => subscriptions.method,
      ),
    );
    if (remainMethods.length > 0) {
      method = remainMethods[0];
    }
    let sender = '';
    if (method === Method.WEBHOOK) {
      sender = 'default-webhook-sender';
    }
    return () =>
      this.fb.control({
        method,
        receivers: [],
        sender,
        template: '',
      });
  }

  onAddNotificationObject(method: string) {
    const dialogRef = this.dialog.open(NotificationReceiverFormComponent, {
      data: {
        method,
      },
    });
    dialogRef.componentInstance.close.pipe(first()).subscribe(res => {
      dialogRef.close();
      if (res) {
        this.update$.next();
      }
    });
  }
}
