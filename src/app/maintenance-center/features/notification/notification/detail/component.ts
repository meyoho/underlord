import {
  AsyncDataLoader,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  NAME,
  matchLabelsToString,
  publishRef,
} from '@alauda/common-snippet';
import { DialogRef, DialogService } from '@alauda/ui';
import {
  ChangeDetectorRef,
  Component,
  OnInit,
  TemplateRef,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { get } from 'lodash-es';
import { combineLatest, forkJoin } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { K8sUtilService } from 'app/services/k8s-util.service';
import { ConfirmDeleteComponent } from 'app/shared/components/confirm-delete/confirm-delete.component';
import {
  Notification,
  NotificationReceiver,
  NotificationTemplate,
  Secret,
} from 'app/typings';
import { RESOURCE_TYPES, ResourceType, TYPE } from 'app/utils';

import { Method } from '../../util';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class NotificationDetailComponent implements OnInit {
  resource: Notification;
  context = this;
  deleteDialogRef: DialogRef<ConfirmDeleteComponent>;
  receivers$ = this.k8sApi.getGlobalResourceList<NotificationReceiver>({
    type: RESOURCE_TYPES.NOTIFICATION_RECEIVER,
    namespaced: true,
  });

  senders$ = this.k8sApi.getGlobalResourceList<Secret>({
    type: RESOURCE_TYPES.SECRET,
    queryParams: {
      labelSelector: matchLabelsToString({
        [this.k8sUtil.normalizeType(TYPE)]: Method.EMAIL,
      }),
    },
  });

  templates$ = this.k8sApi.getGlobalResourceList<NotificationTemplate>({
    type: RESOURCE_TYPES.NOTIFICATION_TEMPLATE,
  });

  receivers: NotificationReceiver[];
  templates: NotificationTemplate[];
  senders: Secret[];

  params$ = this.route.paramMap.pipe(
    map(params => ({
      name: params.get(NAME),
    })),
    publishRef(),
  );

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.NOTIFICATION,
    action: [K8sResourceAction.DELETE, K8sResourceAction.UPDATE],
  });

  dataManager = new AsyncDataLoader<{
    resource: Notification;
    permissions: {
      update: boolean;
      delete: boolean;
    };
  }>({
    params$: this.params$,
    fetcher: params => {
      return combineLatest([
        this.k8sApi.getGlobalResource<Notification>({
          type: RESOURCE_TYPES.NOTIFICATION,
          namespaced: true,
          name: params.name,
        }),
        this.permissions$,
      ]).pipe(
        tap(([resource]) => {
          this.resource = resource;
        }),
        map(([resource, permissions]) => ({
          resource,
          permissions,
        })),
      );
    },
  });

  constructor(
    private readonly route: ActivatedRoute,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly dialogService: DialogService,
    private readonly router: Router,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    forkJoin([this.receivers$, this.senders$, this.templates$]).subscribe(
      ([receivers, senders, templates]) => {
        this.receivers = receivers.items;
        this.senders = senders.items;
        this.templates = templates.items;
        this.cdr.markForCheck();
      },
    );
  }

  update(resource: Notification) {
    this.router.navigate([
      '/maintenance-center/notification/update',
      resource.metadata.name,
    ]);
  }

  delete(templateRef: TemplateRef<ConfirmDeleteComponent>) {
    this.deleteDialogRef = this.dialogService.open(templateRef);
  }

  deleteNotification() {
    return this.k8sApi
      .deleteGlobalResource({
        type: RESOURCE_TYPES.NOTIFICATION,
        namespaced: true,
        resource: this.resource,
      })
      .pipe(tap(() => this.jumpToListPage()));
  }

  getReceiverLabel = (receiverName: string) => {
    const receiver = this.receivers.find(
      _receiver => receiverName === _receiver.metadata.name,
    );
    return `${this.k8sUtil.getDisplayName(receiver) || ''}(${
      get(receiver, ['spec', 'destination']) || receiverName
    })`;
  };

  getTemplateLabel = (templateName: string) => {
    const template = this.templates.find(
      ({ metadata: { name } }) => templateName === name,
    );
    return this.k8sUtil.getUnionDisplayName(template);
  };

  getSenderLabel = (senderName: string) => {
    const sender = this.senders.find(
      ({ metadata: { name } }) => senderName === name,
    );
    if (get(sender, ['data', 'username'])) {
      return atob(sender.data.username);
    } else if (sender) {
      return this.k8sUtil.getName(sender);
    } else {
      return senderName;
    }
  };

  jumpToListPage = () => {
    this.router.navigate(['/maintenance-center/notification']);
  };
}
