export interface Notification {
  method: string;
  recipient: string;
  remark: string;
  secret: string;
}

export interface NotificationFormModel {
  method: string;
  email?: string[];
  sms?: string[];
  webhook_address?: string;
  webhook_secret?: string;
  dingtalk_token?: string;
  remark: string;
}

export enum Method {
  EMAIL = 'email',
  SMS = 'sms',
  WEBHOOK = 'webhook',
  DINGTALK = 'dingtalk',
  WECHAT = 'wechat',
}

export const types = Object.values(Method);

export const NOTIFICATION_TEMPLATE_SUBJECT =
  '[Alarm] {{ .status }} --{{ range $key, $value := .labels }}{{ if eq $key "alertname" }}{{ $value}}{{ end }}{{ end }}';

export const NOTIFICATION_TEMPLATE_CONTENT = `You are receiving this notification because your alarm status was changed.
{{- $alert_object_namespace := "" -}}
{{- $alert_object_kind := "" -}}
{{- $alert_object_name := "" -}}
{{- $alert_indicator := "" -}}
{{- $alert_current_value := "" -}}
{{- range $key, $value := .labels }}
    {{- if eq $key "alert_involved_object_namespace" }}{{ $alert_object_namespace = $value }}{{- end }}
    {{- if eq $key "alert_involved_object_kind" }}{{ $alert_object_kind = $value }}{{- end }}
    {{- if eq $key "alert_involved_object_name" }}{{ $alert_object_name = $value }}{{- end }}
    {{- if eq $key "alert_indicator" }}{{ $alert_indicator = $value }}{{- end }}
{{- end }}
{{- range $key, $value := .annotations }}
    {{- if eq $key "alert_current_value" }}{{ $alert_current_value = $value}}{{- end }}
{{- end }}
Alarm Info:
{{- if ne $alert_object_namespace "" }}- Alarm Namespace: {{ $alert_object_namespace }}{{- end }}
- Alarm Object: {{ $alert_object_kind }}/{{ $alert_object_name }}
- Alarm Indicator: {{ $alert_indicator }}
- Current Value: {{ $alert_current_value }}
- StartTime: {{ .startsAt }}

Alarm Details:
status: {{ .status }}
startsAt: {{ .startsAt }}
endsAt: {{ .endsAt }}
generatorURL: {{ .generatorURL }}
labels:
{{- range $key, $value := .labels }}
  {{ $key }}: {{ $value }} {{- end }}
annotations:
{{- range $key, $value := .annotations }}
  {{ $key }}: {{ $value }} {{- end }}`;

export const NOTIFICATION_TEMPLATE_CONTENT_WITH_TIP = `You are receiving this notification because your alarm status was changed.
{{- $alert_object_namespace := "" -}}       // 告警资源对象所在命名空间
{{- $alert_object_kind := "" -}}            // 告警资源对象的类型
{{- $alert_object_name := "" -}}            // 告警资源对象的名称
{{- $alert_indicator := "" -}}              // 告警指标
{{- $alert_current_value := "" -}}          // 告警指标的当前值
{{- range $key, $value := .labels }}        // 判断是否显示以上几个参数
    {{- if eq $key "alert_involved_object_namespace" }}{{ $alert_object_namespace = $value }}{{- end }}
    {{- if eq $key "alert_involved_object_kind" }}{{ $alert_object_kind = $value }}{{- end }}
    {{- if eq $key "alert_involved_object_name" }}{{ $alert_object_name = $value }}{{- end }}
    {{- if eq $key "alert_indicator" }}{{ $alert_indicator = $value }}{{- end }}
{{- end }}
{{- range $key, $value := .annotations }}
    {{- if eq $key "alert_current_value" }}{{ $alert_current_value = $value}}{{- end }}
{{- end }}
Alarm Info:          // 如果显示以上几个参数，参数的取值
{{- if ne $alert_object_namespace "" }}- Alarm Namespace: {{ $alert_object_namespace }}{{- end }}
- Alarm Object: {{ $alert_object_kind }}/{{ $alert_object_name }}
- Alarm Indicator: {{ $alert_indicator }}
- Current Value: {{ $alert_current_value }}
- StartTime: {{ .startsAt }}

Alarm Details:                        // 告警的详细信息
status: {{ .status }}                 // 告警的状态
startsAt: {{ .startsAt }}             // 告警的开始时间
endsAt: {{ .endsAt }}                 // 告警的结束时间
generatorURL: {{ .generatorURL }}     // 指向 Prometheus 查询表达式的地址
labels:                               // 告警的标签
{{- range $key, $value := .labels }}
  {{ $key }}: {{ $value }} {{- end }}
annotations:
{{- range $key, $value := .annotations }}
  {{ $key }}: {{ $value }} {{- end }}`;

export function getTemplateTypeTranslateKey(type: string) {
  return type === 'other' ? 'notification_template_type_other' : type;
}

export function generateName(name: string) {
  return (
    name.toLowerCase().replace(/[^\da-z-]/g, '-') +
    '-' +
    Math.random().toString(36).slice(2, 7)
  );
}
