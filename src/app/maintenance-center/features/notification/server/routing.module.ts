import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateNotificationServerComponent } from './create/component';
import { NotificationServerDetailComponent } from './detail/component';
import { NotificationServerListComponent } from './list/component';
import { UpdateNotificationServerComponent } from './update/component';

const routes: Routes = [
  {
    path: '',
    component: NotificationServerListComponent,
  },
  {
    path: 'create',
    component: CreateNotificationServerComponent,
  },
  {
    path: 'update/:name',
    component: UpdateNotificationServerComponent,
  },
  {
    path: 'detail/:name',
    component: NotificationServerDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotificationServerRoutingModule {}
