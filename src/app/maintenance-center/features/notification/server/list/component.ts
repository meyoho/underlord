import {
  DESCRIPTION,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  NAME,
  matchLabelsToString,
} from '@alauda/common-snippet';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { K8sUtilService } from 'app/services/k8s-util.service';
import { NotificationServer } from 'app/typings';
import { RESOURCE_TYPES, ResourceType, TYPE } from 'app/utils';

import { Method } from '../../util';

@Component({
  templateUrl: './template.html',
})
export class NotificationServerListComponent {
  columns = [NAME, TYPE, DESCRIPTION, 'create_time'];

  list = new K8SResourceList({
    fetcher: this.fetcher.bind(this),
    activatedRoute: this.activatedRoute,
  });

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.NOTIFICATION_SERVER,
    action: [K8sResourceAction.CREATE],
  });

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    private readonly k8sPermission: K8sPermissionService,
  ) {}

  fetcher() {
    return this.k8sApi.getGlobalResourceList<NotificationServer>({
      type: RESOURCE_TYPES.NOTIFICATION_SERVER,
      queryParams: {
        labelSelector: matchLabelsToString({
          [this.k8sUtil.normalizeType(TYPE)]: Method.EMAIL,
        }),
      },
    });
  }
}
