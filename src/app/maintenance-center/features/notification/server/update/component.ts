import { K8sApiService, NAME, publishRef } from '@alauda/common-snippet';
import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EMPTY } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { NotificationServer } from 'app/typings';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';

@Component({
  template: `
    <alu-notification-server-form
      [ngModel]="data$ | async"
      [update]="true"
    ></alu-notification-server-form>
  `,
})
export class UpdateNotificationServerComponent {
  params$ = this.route.paramMap.pipe(
    map(params => ({
      name: params.get(NAME),
    })),
    publishRef(),
  );

  data$ = this.params$.pipe(
    switchMap(params => {
      return this.k8sApi
        .getGlobalResource<NotificationServer>({
          type: RESOURCE_TYPES.NOTIFICATION_SERVER,
          name: params.name,
        })
        .pipe(
          catchError(() => {
            this.location.back();
            return EMPTY;
          }),
        );
    }),
  );

  constructor(
    private readonly route: ActivatedRoute,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly location: Location,
  ) {}
}
