import { Component, Injector, Input } from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { NotificationEmailServer } from 'app/typings';
import { PORT_PATTERN } from 'app/utils';

@Component({
  selector: 'alu-notification-email-setting-form',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class NotificationEmailSettingFormComponent extends BaseResourceFormGroupComponent {
  @Input()
  update: boolean;

  submitting: boolean;
  portPattern = PORT_PATTERN;

  constructor(public injector: Injector) {
    super(injector);
  }

  createForm() {
    const email = this.fb.group({
      host: ['', Validators.required],
      port: [
        '',
        [Validators.required, Validators.pattern(this.portPattern.pattern)],
      ],
      ssl: [''],
      insecureSkipVerify: [''],
    });
    return this.fb.group({ email });
  }

  getDefaultFormModel() {
    return {};
  }

  adaptFormModel(formModel: { email: NotificationEmailServer }) {
    if (formModel?.email) {
      formModel.email.port = +formModel.email.port;
    }
    return formModel;
  }
}
