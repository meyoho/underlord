import {
  DESCRIPTION,
  DISPLAY_NAME,
  K8sApiService,
  TranslateService,
} from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import { Location } from '@angular/common';
import { Component, Injector, Input } from '@angular/core';
import { NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { get } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { finalize } from 'rxjs/operators';

import { K8sUtilService } from 'app/services/k8s-util.service';
import { NotificationServer } from 'app/typings';
import {
  K8S_RESOURCE_NAME_BASE,
  RESOURCE_TYPES,
  ResourceType,
  TYPE,
} from 'app/utils';

@Component({
  selector: 'alu-notification-server-form',
  templateUrl: 'template.html',
  styles: [
    `
      :host ::ng-deep .aui-card {
        margin-bottom: 52px;
      }
    `,
  ],
})
export class NotificationServerFormComponent extends BaseResourceFormGroupComponent {
  @Input()
  update: boolean;

  description = this.k8sUtil.normalizeType(DESCRIPTION);
  displayName = this.k8sUtil.normalizeType(DISPLAY_NAME);
  type = this.k8sUtil.normalizeType(TYPE);
  K8S_RESOURCE_NAME_BASE = K8S_RESOURCE_NAME_BASE;

  submitting: boolean;
  get = get;

  constructor(
    public injector: Injector,
    private readonly location: Location,
    private readonly translate: TranslateService,
    private readonly k8sUtil: K8sUtilService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly messageService: MessageService,
    private readonly router: Router,
  ) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      apiVersion: [''],
      kind: [''],
      metadata: this.fb.group({
        labels: this.fb.group({
          [this.type]: [''],
        }),
        annotations: this.fb.group({
          [this.description]: [''],
          [this.displayName]: ['', Validators.required],
        }),
        name: [
          '',
          [
            Validators.required,
            Validators.pattern(K8S_RESOURCE_NAME_BASE.pattern),
          ],
        ],
        resourceVersion: [''],
      }),
      spec: this.fb.control({}),
    });
  }

  getDefaultFormModel() {
    return {};
  }

  confirm(form: NgForm) {
    form.onSubmit(null);
    if (form.invalid) {
      return;
    }
    this.submitting = true;
    this.k8sApi[this.update ? 'putGlobalResource' : 'postGlobalResource']<
      NotificationServer
    >({
      type: RESOURCE_TYPES.NOTIFICATION_SERVER,
      resource: form.value,
    })
      .pipe(
        finalize(() => {
          this.submitting = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(notificationServer => {
        this.messageService.success(
          this.translate.get(this.update ? 'update_success' : 'create_success'),
        );
        this.router.navigate([
          '/maintenance-center/notification_server/detail',
          notificationServer.metadata.name,
        ]);
      });
  }

  cancel() {
    this.location.back();
  }
}
