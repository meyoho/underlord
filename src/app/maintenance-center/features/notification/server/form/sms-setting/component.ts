import { Component, Injector, Input } from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

@Component({
  selector: 'alu-notification-sms-setting-form',
  templateUrl: 'template.html',
})
export class NotificationSMSSettingFormComponent extends BaseResourceFormGroupComponent {
  @Input()
  update: boolean;

  submitting: boolean;

  constructor(public injector: Injector) {
    super(injector);
  }

  createForm() {
    const sms = this.fb.group({
      host: ['', Validators.required],
      port: ['', Validators.required],
      provider: ['', Validators.required],
      softVersion: ['', Validators.required],
    });
    return this.fb.group({ sms });
  }

  getDefaultFormModel() {
    return {};
  }
}
