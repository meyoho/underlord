import { DESCRIPTION, DISPLAY_NAME } from '@alauda/common-snippet';
import { Component, OnInit } from '@angular/core';

import { K8sUtilService } from 'app/services/k8s-util.service';
import { NotificationServer } from 'app/typings';
import { NotificationServerMeta, TYPE } from 'app/utils';

import { Method } from '../../util';

@Component({
  template:
    '<alu-notification-server-form [ngModel]="server"></alu-notification-server-form>',
})
export class CreateNotificationServerComponent implements OnInit {
  server: NotificationServer;

  constructor(private readonly k8sUtil: K8sUtilService) {}

  ngOnInit() {
    this.server = this.getDefaultIdp();
  }

  getDefaultIdp(): NotificationServer {
    return {
      apiVersion: NotificationServerMeta.apiVersion,
      kind: NotificationServerMeta.kind,
      metadata: {
        name: '',
        annotations: {
          [this.k8sUtil.normalizeType(DESCRIPTION)]: '',
          [this.k8sUtil.normalizeType(DISPLAY_NAME)]: '',
        },
        labels: {
          [this.k8sUtil.normalizeType(TYPE)]: Method.EMAIL,
        },
      },
      spec: {},
    };
  }
}
