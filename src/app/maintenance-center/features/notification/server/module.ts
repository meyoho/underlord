import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { CreateNotificationServerComponent } from './create/component';
import { NotificationServerDetailComponent } from './detail/component';
import { NotificationServerFormComponent } from './form/component';
import { NotificationEmailSettingFormComponent } from './form/email-setting/component';
import { NotificationSMSSettingFormComponent } from './form/sms-setting/component';
import { NotificationServerListComponent } from './list/component';
import { NotificationServerRoutingModule } from './routing.module';
import { UpdateNotificationServerComponent } from './update/component';

@NgModule({
  imports: [SharedModule, NotificationServerRoutingModule],
  declarations: [
    NotificationServerListComponent,
    NotificationServerFormComponent,
    NotificationServerDetailComponent,
    NotificationEmailSettingFormComponent,
    NotificationSMSSettingFormComponent,
    CreateNotificationServerComponent,
    UpdateNotificationServerComponent,
  ],
})
export class NotificationServerModule {}
