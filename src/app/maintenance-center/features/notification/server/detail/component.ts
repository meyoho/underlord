import {
  AsyncDataLoader,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  NAME,
  publishRef,
} from '@alauda/common-snippet';
import { DialogRef, DialogService } from '@alauda/ui';
import { Component, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { ConfirmDeleteComponent } from 'app/shared/components/confirm-delete/confirm-delete.component';
import { NotificationServer } from 'app/typings';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';

@Component({
  templateUrl: './template.html',
  styles: [
    `
      :host .field-set-header {
        margin-bottom: 16px;
        font-size: 18px;
        line-height: 24px;
      }
    `,
  ],
})
export class NotificationServerDetailComponent {
  resource: NotificationServer;
  context = this;
  deleteDialogRef: DialogRef<ConfirmDeleteComponent>;
  params$ = this.route.paramMap.pipe(
    map(params => ({
      name: params.get(NAME),
    })),
    publishRef(),
  );

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.NOTIFICATION_SERVER,
    action: [K8sResourceAction.DELETE, K8sResourceAction.UPDATE],
  });

  dataManager = new AsyncDataLoader<{
    resource: NotificationServer;
    permissions: {
      update: boolean;
      delete: boolean;
    };
  }>({
    params$: this.params$,
    fetcher: params => {
      return combineLatest([
        this.k8sApi.getGlobalResource<NotificationServer>({
          type: RESOURCE_TYPES.NOTIFICATION_SERVER,
          name: params.name,
        }),
        this.permissions$,
      ]).pipe(
        tap(([resource]) => {
          this.resource = resource;
        }),
        map(([resource, permissions]) => ({
          resource,
          permissions,
        })),
      );
    },
  });

  constructor(
    private readonly route: ActivatedRoute,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sPermission: K8sPermissionService,
    private readonly dialogService: DialogService,
    private readonly router: Router,
  ) {}

  trackByFn(i: number) {
    return i;
  }

  update(data: NotificationServer) {
    this.router.navigate([
      '/maintenance-center/notification_server/update',
      data.metadata.name,
    ]);
  }

  delete(templateRef: TemplateRef<ConfirmDeleteComponent>) {
    this.deleteDialogRef = this.dialogService.open(templateRef);
  }

  deleteNotificationServer() {
    return this.k8sApi
      .deleteGlobalResource({
        type: RESOURCE_TYPES.NOTIFICATION_SERVER,
        resource: this.resource,
      })
      .pipe(tap(() => this.jumpToListPage()));
  }

  jumpToListPage = () => {
    this.router.navigate(['/maintenance-center/notification_server']);
  };
}
