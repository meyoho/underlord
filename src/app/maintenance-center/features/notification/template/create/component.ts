import { DESCRIPTION, DISPLAY_NAME } from '@alauda/common-snippet';
import { Component, OnInit } from '@angular/core';

import { K8sUtilService } from 'app/services/k8s-util.service';
import { NotificationTemplate } from 'app/typings';
import { NotificationTemplateMeta, TYPE } from 'app/utils';

import {
  Method,
  NOTIFICATION_TEMPLATE_CONTENT,
  NOTIFICATION_TEMPLATE_SUBJECT,
} from '../../util';

@Component({
  template:
    '<alu-notification-template-form [ngModel]="template"></alu-notification-template-form>',
})
export class CreateNotificationTemplateComponent implements OnInit {
  template: NotificationTemplate;

  constructor(private readonly k8sUtil: K8sUtilService) {}

  ngOnInit() {
    this.template = this.getDefaultValue();
  }

  getDefaultValue(): NotificationTemplate {
    return {
      apiVersion: NotificationTemplateMeta.apiVersion,
      kind: NotificationTemplateMeta.kind,
      metadata: {
        name: '',
        annotations: {
          [this.k8sUtil.normalizeType(DESCRIPTION)]: '',
          [this.k8sUtil.normalizeType(DISPLAY_NAME)]: '',
        },
        labels: {
          [this.k8sUtil.normalizeType(TYPE)]: Method.EMAIL,
        },
      },
      spec: {
        subject: NOTIFICATION_TEMPLATE_SUBJECT,
        content: NOTIFICATION_TEMPLATE_CONTENT,
      },
    };
  }
}
