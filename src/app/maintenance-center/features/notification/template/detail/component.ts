import {
  AsyncDataLoader,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  K8sUtilService,
  NAME,
  TranslateService,
  noop,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService, MessageService, NotificationService } from '@alauda/ui';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import { NotificationTemplate } from 'app/typings';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';

import { getTemplateTypeTranslateKey } from '../../util';

@Component({
  templateUrl: './template.html',
  styleUrls: ['styles.scss'],
})
export class NotificationTemplateDetailComponent {
  getTemplateTypeTranslateKey = getTemplateTypeTranslateKey;
  params$ = this.route.paramMap.pipe(
    map(params => ({
      name: params.get(NAME),
    })),
    publishRef(),
  );

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.NOTIFICATION_TEMPLATE,
    action: [K8sResourceAction.DELETE, K8sResourceAction.UPDATE],
  });

  dataManager = new AsyncDataLoader<{
    resource: NotificationTemplate;
    permissions: {
      update: boolean;
      delete: boolean;
    };
  }>({
    params$: this.params$,
    fetcher: params => {
      return combineLatest([
        this.k8sApi.getGlobalResource<NotificationTemplate>({
          type: RESOURCE_TYPES.NOTIFICATION_TEMPLATE,
          name: params.name,
        }),
        this.permissions$,
      ]).pipe(
        map(([resource, permissions]) => ({
          resource,
          permissions,
        })),
      );
    },
  });

  constructor(
    private readonly route: ActivatedRoute,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sUtil: K8sUtilService,
    private readonly dialogService: DialogService,
    private readonly messageService: MessageService,
    private readonly notificationService: NotificationService,
    private readonly translate: TranslateService,
    private readonly router: Router,
  ) {}

  trackByFn(i: number) {
    return i;
  }

  update(data: NotificationTemplate) {
    this.router.navigate([
      '/maintenance-center/notification_template/update',
      data.metadata.name,
    ]);
  }

  delete(data: NotificationTemplate) {
    this.k8sApi
      .getGlobalResourceList({
        type: RESOURCE_TYPES.NOTIFICATION,
        namespaced: true,
        queryParams: {
          labelSelector: this.k8sUtil.normalizeType(
            data.metadata.name,
            'notificationtemplate',
          ),
        },
      })
      .subscribe(res => {
        if (res.items?.length) {
          this.notificationService.error(
            this.translate.get('notification_template_in_use'),
          );
        } else {
          this.dialogService
            .confirm({
              title: this.translate.get('delete_notification_template_title', {
                name: data.metadata.name,
              }),
              confirmText: this.translate.get('confirm'),
              cancelText: this.translate.get('cancel'),
              beforeConfirm: (resolve, reject) => {
                this.k8sApi
                  .deleteGlobalResource({
                    type: RESOURCE_TYPES.NOTIFICATION_TEMPLATE,
                    resource: data,
                  })
                  .subscribe(() => {
                    resolve();
                    this.messageService.success(
                      this.translate.get('delete_success'),
                    );
                  }, reject);
              },
            })
            .then(this.jumpToListPage)
            .catch(noop);
        }
      });
  }

  jumpToListPage = () => {
    this.router.navigate(['/maintenance-center/notification_template']);
  };
}
