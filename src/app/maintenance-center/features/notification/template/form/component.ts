import {
  DESCRIPTION,
  DISPLAY_NAME,
  K8sApiService,
  LABELS,
  METADATA,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogService, DialogSize, MessageService } from '@alauda/ui';
import { Location } from '@angular/common';
import { Component, Injector, Input, OnInit, TemplateRef } from '@angular/core';
import { NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { get } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { finalize } from 'rxjs/operators';

import { K8sUtilService } from 'app/services/k8s-util.service';
import { NotificationTemplate } from 'app/typings';
import {
  K8S_RESOURCE_NAME_BASE,
  RESOURCE_TYPES,
  ResourceType,
  TYPE,
} from 'app/utils';

import {
  Method,
  NOTIFICATION_TEMPLATE_CONTENT_WITH_TIP,
  NOTIFICATION_TEMPLATE_SUBJECT,
} from '../../util';

@Component({
  selector: 'alu-notification-template-form',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class NotificationTemplateFormComponent
  extends BaseResourceFormGroupComponent
  implements OnInit {
  @Input()
  update: boolean;

  submitting: boolean;
  get = get;
  K8S_RESOURCE_NAME_BASE = K8S_RESOURCE_NAME_BASE;
  description = this.k8sUtil.normalizeType(DESCRIPTION);
  displayName = this.k8sUtil.normalizeType(DISPLAY_NAME);
  type = this.k8sUtil.normalizeType(TYPE);
  typeControlPath = [METADATA, LABELS, this.type];
  NOTIFICATION_TEMPLATE_CONTENT_WITH_TIP = NOTIFICATION_TEMPLATE_CONTENT_WITH_TIP;

  constructor(
    public injector: Injector,
    private readonly location: Location,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly k8sUtil: K8sUtilService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly message: MessageService,
    private readonly router: Router,
  ) {
    super(injector);
  }

  ngOnInit() {
    this.form
      .get([METADATA, LABELS, this.type])
      .valueChanges.subscribe(type => {
        const control = this.form.get('spec.subject');
        if (type === Method.EMAIL) {
          control.enable();
          control.setValue(NOTIFICATION_TEMPLATE_SUBJECT);
        } else {
          control.setValue('');
          control.disable();
        }
      });
  }

  createForm() {
    return this.fb.group({
      apiVersion: [''],
      kind: [''],
      metadata: this.fb.group({
        annotations: this.fb.group({
          [this.description]: [''],
          [this.displayName]: ['', Validators.required],
        }),
        labels: this.fb.group({
          [this.type]: [''],
        }),
        name: [
          '',
          [
            Validators.required,
            Validators.pattern(K8S_RESOURCE_NAME_BASE.pattern),
          ],
        ],
        resourceVersion: [''],
      }),
      spec: this.fb.group({
        subject: ['', Validators.required],
        content: ['', Validators.required],
      }),
    });
  }

  getDefaultFormModel() {
    return {};
  }

  confirm(form: NgForm) {
    form.onSubmit(null);
    if (form.invalid) {
      return;
    }
    this.submitting = true;
    this.k8sApi[this.update ? 'putGlobalResource' : 'postGlobalResource']<
      NotificationTemplate
    >({
      type: RESOURCE_TYPES.NOTIFICATION_TEMPLATE,
      resource: form.value,
    })
      .pipe(
        finalize(() => {
          this.submitting = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe(notificationTemplate => {
        this.message.success(
          this.translate.get(this.update ? 'update_success' : 'create_success'),
        );
        this.router.navigate([
          '/maintenance-center/notification_template/detail',
          notificationTemplate.metadata.name,
        ]);
      });
  }

  cancel() {
    this.location.back();
  }

  showDemoTemplate(template: TemplateRef<any>) {
    this.dialog.open(template, {
      size: DialogSize.Big,
    });
  }
}
