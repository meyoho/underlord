import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateNotificationTemplateComponent } from './create/component';
import { NotificationTemplateDetailComponent } from './detail/component';
import { NotificationTemplateListComponent } from './list/component';
import { UpdateNotificationTemplateComponent } from './update/component';

const routes: Routes = [
  {
    path: '',
    component: NotificationTemplateListComponent,
  },
  {
    path: 'create',
    component: CreateNotificationTemplateComponent,
  },
  {
    path: 'update/:name',
    component: UpdateNotificationTemplateComponent,
  },
  {
    path: 'detail/:name',
    component: NotificationTemplateDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotificationTemplateRoutingModule {}
