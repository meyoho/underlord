import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { CreateNotificationTemplateComponent } from './create/component';
import { NotificationTemplateDetailComponent } from './detail/component';
import { NotificationTemplateFormComponent } from './form/component';
import { NotificationTemplateListComponent } from './list/component';
import { NotificationTemplateRoutingModule } from './routing.module';
import { UpdateNotificationTemplateComponent } from './update/component';

@NgModule({
  imports: [SharedModule, NotificationTemplateRoutingModule],
  declarations: [
    NotificationTemplateListComponent,
    CreateNotificationTemplateComponent,
    UpdateNotificationTemplateComponent,
    NotificationTemplateDetailComponent,
    NotificationTemplateFormComponent,
  ],
})
export class NotificationTemplateModule {}
