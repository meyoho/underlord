import { K8sApiService, NAME, publishRef } from '@alauda/common-snippet';
import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { noop } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { NotificationTemplate } from 'app/typings';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';

@Component({
  template:
    '<alu-notification-template-form [ngModel]="data$ | async" [update]="true"></alu-notification-template-form>',
})
export class UpdateNotificationTemplateComponent {
  params$ = this.route.paramMap.pipe(
    map(params => ({
      name: params.get(NAME),
    })),
    publishRef(),
  );

  data$ = this.params$.pipe(
    switchMap(params => {
      return this.k8sApi
        .getGlobalResource<NotificationTemplate>({
          type: RESOURCE_TYPES.NOTIFICATION_TEMPLATE,
          name: params.name,
        })
        .pipe(
          catchError(() => {
            this.location.back();
            return noop;
          }),
        );
    }),
  );

  constructor(
    private readonly route: ActivatedRoute,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly location: Location,
  ) {}
}
