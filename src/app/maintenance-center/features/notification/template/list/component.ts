import {
  DESCRIPTION,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  K8sUtilService,
  NAME,
  TranslateService,
  catchPromise,
} from '@alauda/common-snippet';
import { DialogService, MessageService, NotificationService } from '@alauda/ui';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NotificationTemplate } from 'app/typings';
import { ACTION, RESOURCE_TYPES, ResourceType, TYPE } from 'app/utils';

import { getTemplateTypeTranslateKey } from '../../util';

@Component({
  templateUrl: './template.html',
})
export class NotificationTemplateListComponent {
  getTemplateTypeTranslateKey = getTemplateTypeTranslateKey;
  columns = [NAME, TYPE, DESCRIPTION, 'create_time', ACTION];

  list = new K8SResourceList({
    fetcher: this.fetcher.bind(this),
    activatedRoute: this.activatedRoute,
  });

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.NOTIFICATION_TEMPLATE,
    action: [
      K8sResourceAction.CREATE,
      K8sResourceAction.UPDATE,
      K8sResourceAction.DELETE,
    ],
  });

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sUtil: K8sUtilService,
    private readonly router: Router,
    private readonly dialogService: DialogService,
    private readonly messageService: MessageService,
    private readonly notificationService: NotificationService,
    private readonly translate: TranslateService,
  ) {}

  fetcher() {
    return this.k8sApi.getGlobalResourceList<NotificationTemplate>({
      type: RESOURCE_TYPES.NOTIFICATION_TEMPLATE,
    });
  }

  update(data: NotificationTemplate) {
    this.router.navigate([
      '/maintenance-center/notification_template/update',
      data.metadata.name,
    ]);
  }

  delete(data: NotificationTemplate) {
    this.k8sApi
      .getGlobalResourceList({
        type: RESOURCE_TYPES.NOTIFICATION,
        namespaced: true,
        queryParams: {
          labelSelector: this.k8sUtil.normalizeType(
            data.metadata.name,
            'notificationtemplate',
          ),
        },
      })
      .subscribe(res => {
        if (res.items?.length) {
          this.notificationService.error(
            this.translate.get('notification_template_in_use'),
          );
        } else {
          catchPromise(
            this.dialogService.confirm({
              title: this.translate.get('delete_notification_template_title', {
                name: data.metadata.name,
              }),
              confirmText: this.translate.get('confirm'),
              cancelText: this.translate.get('cancel'),
              beforeConfirm: (resolve, reject) => {
                this.k8sApi
                  .deleteGlobalResource({
                    type: RESOURCE_TYPES.NOTIFICATION_TEMPLATE,
                    resource: data,
                  })
                  .subscribe(() => {
                    resolve();
                    this.messageService.success(
                      this.translate.get('delete_success'),
                    );
                  }, reject);
              },
            }),
          ).subscribe(() => this.list.delete(data));
        }
      });
  }
}
