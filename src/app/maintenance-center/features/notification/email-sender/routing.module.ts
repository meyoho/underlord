import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EmailSenderListComponent } from './list/component';

const routes: Routes = [
  {
    path: '',
    component: EmailSenderListComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmailSenderRoutingModule {}
