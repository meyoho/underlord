import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { EmailSenderFormComponent } from './form/component';
import { EmailSenderListComponent } from './list/component';
import { EmailSenderRoutingModule } from './routing.module';

@NgModule({
  imports: [SharedModule, EmailSenderRoutingModule],
  declarations: [EmailSenderListComponent, EmailSenderFormComponent],
  entryComponents: [EmailSenderFormComponent],
})
export class EmailSenderModule {}
