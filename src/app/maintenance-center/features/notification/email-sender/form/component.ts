import {
  K8sApiService,
  TOKEN_GLOBAL_NAMESPACE,
  TranslateService,
  matchLabelsToString,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  Output,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { cloneDeep } from 'lodash-es';
import { finalize } from 'rxjs/operators';

import { K8sUtilService } from 'app/services/k8s-util.service';
import { NotificationServer, Secret, SecretType } from 'app/typings';
import {
  DEFAULT,
  FALSE,
  RESOURCE_TYPES,
  ResourceType,
  SecretTypeMeta,
  TYPE,
} from 'app/utils';

import { Method, generateName } from '../../util';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class EmailSenderFormComponent {
  @Output()
  close: EventEmitter<Secret> = new EventEmitter();

  creator: string;
  submitting: boolean;
  update: boolean;
  showPassword = false;
  emailSender: Secret;
  emailServers$ = this.k8sApi.getGlobalResourceList<NotificationServer>({
    type: RESOURCE_TYPES.NOTIFICATION_SERVER,
    queryParams: {
      labelSelector: matchLabelsToString({
        [this.k8sUtil.normalizeType(TYPE)]: 'email',
      }),
    },
  });

  notificationServer = this.k8sUtil.normalizeType('server');

  constructor(
    @Inject(DIALOG_DATA)
    public dialogData: {
      emailSender: Secret;
    },
    @Inject(TOKEN_GLOBAL_NAMESPACE) private readonly globalNamespace: string,
    private readonly dialogRef: DialogRef,
    private readonly translate: TranslateService,
    private readonly k8sUtil: K8sUtilService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly messageService: MessageService,
    private readonly cdr: ChangeDetectorRef,
  ) {
    if (dialogData.emailSender) {
      this.update = true;
      this.emailSender = cloneDeep(dialogData.emailSender);
      this.emailSender.data.username = atob(this.emailSender.data.username);
      this.emailSender.data.password = atob(this.emailSender.data.password);
    } else {
      this.emailSender = this.genEmailSender();
    }
  }

  genEmailSender() {
    return {
      apiVersion: SecretTypeMeta.apiVersion,
      kind: SecretTypeMeta.kind,
      type: SecretType.NotificationSender,
      metadata: {
        labels: {
          [this.k8sUtil.normalizeType(TYPE)]: Method.EMAIL,
          [this.k8sUtil.normalizeType(DEFAULT)]: FALSE,
          [this.notificationServer]: '',
        },
        namespace: this.globalNamespace,
        name: '',
      },
      data: {
        password: '',
        username: '',
      },
    };
  }

  cancel() {
    this.dialogRef.close();
  }

  submit(form: NgForm) {
    form.onSubmit(null);
    if (form.invalid || this.submitting) {
      return;
    }
    this.submitting = true;
    const resource = cloneDeep(this.emailSender);
    if (!this.update) {
      resource.metadata.name = generateName(resource.data.username);
    }
    resource.data.password = btoa(resource.data.password);
    resource.data.username = btoa(resource.data.username);
    this.k8sApi[this.update ? 'putGlobalResource' : 'postGlobalResource']({
      type: RESOURCE_TYPES.SECRET,
      resource,
    })
      .pipe(
        finalize(() => {
          this.submitting = false;
          this.cdr.markForCheck();
        }),
      )
      .subscribe((secret: Secret) => {
        this.messageService.success(
          this.translate.get(this.update ? 'update_success' : 'add_success'),
        );
        this.close.next(secret);
      });
  }

  getServerLabel = (server: NotificationServer) => {
    return `${this.k8sUtil.getDisplayName(server) || ''}(${this.k8sUtil.getName(
      server,
    )})`;
  };
}
