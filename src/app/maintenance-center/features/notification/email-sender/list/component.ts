import {
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  TranslateService,
  matchLabelsToString,
  noop,
} from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import { K8sUtilService } from 'app/services/k8s-util.service';
import { Secret } from 'app/typings';
import { ACTION, RESOURCE_TYPES, ResourceType, TYPE } from 'app/utils';

import { Method } from '../../util';
import { EmailSenderFormComponent } from '../form/component';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class EmailSenderListComponent {
  columns = ['email', 'password', 'send_server', 'create_time', ACTION];
  atob = atob;

  list = new K8SResourceList({
    fetcher: this.fetcher.bind(this),
    activatedRoute: this.activatedRoute,
  });

  visibleMap: { [key: string]: boolean } = {};
  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.SECRET,
    action: [
      K8sResourceAction.CREATE,
      K8sResourceAction.UPDATE,
      K8sResourceAction.DELETE,
    ],
  });

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly dialog: DialogService,
    private readonly k8sUtil: K8sUtilService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sPermission: K8sPermissionService,
    private readonly translate: TranslateService,
    private readonly messageService: MessageService,
  ) {}

  fetcher() {
    return this.k8sApi.getGlobalResourceList<Secret>({
      type: RESOURCE_TYPES.SECRET,
      namespaced: true,
      queryParams: {
        labelSelector: matchLabelsToString({
          [this.k8sUtil.normalizeType(TYPE)]: Method.EMAIL,
        }),
      },
    });
  }

  create() {
    const dialogRef = this.dialog.open(EmailSenderFormComponent);
    dialogRef.componentInstance.close.pipe(first()).subscribe(res => {
      dialogRef.close();
      if (res) {
        this.list.reload();
      }
    });
  }

  update(emailSender: Secret) {
    const dialogRef = this.dialog.open(EmailSenderFormComponent, {
      data: {
        emailSender,
      },
    });
    dialogRef.componentInstance.close.pipe(first()).subscribe(res => {
      dialogRef.close();
      if (res) {
        this.list.update(res);
      }
    });
  }

  delete(data: Secret) {
    this.dialog
      .confirm({
        title: this.translate.get('delete_notification_sender_title'),
        content: atob(data.data.username),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) => {
          this.k8sApi
            .deleteGlobalResource({
              type: RESOURCE_TYPES.SECRET,
              resource: data,
            })
            .subscribe(() => {
              resolve();
              this.messageService.success(this.translate.get('delete_success'));
            }, reject);
        },
      })
      .then(() => this.list.delete(data))
      .catch(noop);
  }

  toggleCollapse(key: string) {
    this.visibleMap[key] = !this.visibleMap[key];
  }
}
