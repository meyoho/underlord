import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AlarmDashboardComponent } from './dashboard/component';
import { PrometheusDetailComponent } from './detail/component';
import { PrometheusAlarmFormComponent } from './form/component';

const alarmRoutes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'list',
    component: AlarmDashboardComponent,
  },
  {
    path: 'detail/:name',
    component: PrometheusDetailComponent,
  },
  {
    path: 'create',
    component: PrometheusAlarmFormComponent,
  },
  {
    path: 'update/:name',
    component: PrometheusAlarmFormComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(alarmRoutes)],
  exports: [RouterModule],
})
export class AlarmRoutingModule {}
