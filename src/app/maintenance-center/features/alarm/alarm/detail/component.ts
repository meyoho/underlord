import {
  AsyncDataLoader,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  NAME,
  TranslateService,
  noop,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import { K8sUtilService } from 'app/services/k8s-util.service';
import { PrometheusRule } from 'app/typings';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';

import { parseNotifications } from '../../alarm.util';
@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PrometheusDetailComponent {
  cluster: string;
  parseNotifications = parseNotifications;

  params$ = combineLatest([this.route.paramMap, this.route.queryParams]).pipe(
    map(([params, query]) => ({
      name: params.get(NAME),
      cluster: query.cluster,
      namespace: query.namespace,
    })),
    publishRef(),
  );

  dataManager = new AsyncDataLoader<{
    resource: PrometheusRule;
    permissions: {
      update: boolean;
      delete: boolean;
    };
  }>({
    params$: this.params$,
    fetcher: params => {
      return combineLatest([
        this.k8sApi.getResource<PrometheusRule>({
          type: RESOURCE_TYPES.PROMETHEUS_RULE,
          cluster: params.cluster,
          name: params.name,
          namespace: params.namespace,
        }),
        this.k8sPermission.isAllowed({
          type: RESOURCE_TYPES.PROMETHEUS_RULE,
          cluster: params.cluster,
          namespace: params.namespace,
          action: [K8sResourceAction.DELETE, K8sResourceAction.UPDATE],
        }),
      ]).pipe(
        map(([resource, permissions]) => ({
          resource,
          permissions,
        })),
      );
    },
  });

  constructor(
    private readonly k8sUtil: K8sUtilService,
    private readonly dialogService: DialogService,
    private readonly translate: TranslateService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sPermission: K8sPermissionService,
    private readonly messageService: MessageService,
  ) {
    this.cluster = route.snapshot.queryParams.cluster;
  }

  update(rule: PrometheusRule) {
    this.router.navigate(
      ['/maintenance-center/alarm/update', rule.metadata.name],
      {
        queryParams: {
          cluster: this.cluster,
          namespace: rule.metadata.namespace,
        },
      },
    );
  }

  delete(rule: PrometheusRule) {
    this.dialogService
      .confirm({
        title: this.translate.get('delete_alarm_title'),
        content: this.translate.get('delete_alarm_content', {
          alarm_name: rule.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) => {
          this.k8sApi
            .deleteResource({
              type: RESOURCE_TYPES.PROMETHEUS_RULE,
              cluster: this.k8sUtil.getLabel(rule, 'cluster', 'alert'),
              resource: rule,
            })
            .subscribe(() => {
              resolve();
              this.messageService.success(this.translate.get('delete_success'));
            }, reject);
        },
      })
      .then(this.jumpToListPage)
      .catch(noop);
  }

  shouldDisplay = (rule: PrometheusRule) => {
    const kind = this.k8sUtil.getLabel(rule, 'kind', 'alert');
    return kind === 'Cluster' || kind === 'Node';
  };

  jumpToListPage = () => {
    this.router.navigate(['/maintenance-center/alarm']);
  };
}
