import { NgModule } from '@angular/core';

import { AlarmSharedModule } from 'app/maintenance-center/features-shared/alarm/shared.module';
import { EventSharedModule } from 'app/maintenance-center/features-shared/event/shared.module';
import { SharedModule } from 'app/shared/shared.module';

import { AlarmTemplateApplyComponent } from './apply-template/component';
import { AlarmDashboardComponent } from './dashboard/component';
import { PrometheusDetailComponent } from './detail/component';
import { PrometheusAlarmFormComponent } from './form/component';
import { PrometheusAlarmComponent } from './list/component';
import { AlarmRoutingModule } from './routing.module';

@NgModule({
  imports: [
    SharedModule,
    AlarmRoutingModule,
    AlarmSharedModule,
    EventSharedModule,
  ],
  declarations: [
    AlarmDashboardComponent,
    AlarmTemplateApplyComponent,
    PrometheusAlarmComponent,
    PrometheusAlarmFormComponent,
    PrometheusDetailComponent,
  ],
  entryComponents: [AlarmTemplateApplyComponent],
})
export class AlarmModule {}
