import {
  DESCRIPTION,
  K8sApiService,
  NAME,
  NAMESPACE,
  TOKEN_GLOBAL_NAMESPACE,
  TranslateService,
} from '@alauda/common-snippet';
import { MessageService, NotificationService } from '@alauda/ui';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { upperFirst } from 'lodash-es';
import { EMPTY, Subject } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { MaintenanceCenterComponent } from 'app/maintenance-center/component';
import { K8sUtilService } from 'app/services/k8s-util.service';
import {
  AlarmAction,
  ClusterFeature,
  Node,
  PrometheusRule,
  PrometheusRuleItem,
} from 'app/typings';
import {
  LOOSE_K8S_RESOURCE_NAME_BASE,
  PrometheusRuleMeta,
  RESOURCE_TYPES,
  ResourceType,
  hasDuplicateBy,
} from 'app/utils';
interface NodeResource {
  address: string;
  hostname: string;
}
interface PrometheusRuleModel {
  name: string;
  kind: string;
  node?: NodeResource;
  description: string;
  rules: PrometheusRuleItem[];
  actions: AlarmAction[];
}
@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [
    `
      .form-wrapper {
        margin-bottom: 52px;
      }
    `,
  ],
})
export class PrometheusAlarmFormComponent implements OnInit, OnDestroy {
  @ViewChild('alarmForm', { static: false })
  form: NgForm;

  clusterName: string;
  clusterDisplayName: string;
  name: string;
  namespace: string;
  prometheusName: string;
  prometheusNamespace: string;
  resource: PrometheusRule;
  formSubmitText = 'create';
  submitting = false;
  canChangeResourceType = true;
  resourceNameReg = LOOSE_K8S_RESOURCE_NAME_BASE;
  nodes: NodeResource[] = [];
  allNodes = { address: '0.0.0.0', hostname: '' };
  isOCP = false;

  viewModel: PrometheusRuleModel = {
    name: '',
    kind: 'cluster',
    description: '',
    rules: [],
    actions: [],
  };

  onDestroy$ = new Subject<void>();

  constructor(
    @Inject(TOKEN_GLOBAL_NAMESPACE) private readonly globalNamespace: string,
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly location: Location,
    private readonly translate: TranslateService,
    private readonly auiNotificationService: NotificationService,
    private readonly messageService: MessageService,
    private readonly layoutComponent: MaintenanceCenterComponent,
  ) {
    this.clusterName = route.snapshot.queryParams.cluster;
    this.namespace = route.snapshot.queryParams.namespace;
    this.name = route.snapshot.params.name;
  }

  ngOnInit() {
    this.layoutComponent.getCluster$().subscribe(cluster => {
      this.clusterDisplayName = this.k8sUtil.getDisplayName(cluster);
      this.isOCP = this.k8sUtil.getLabel(cluster, 'cluster-type') === 'OCP';
    });
    // eslint-disable-next-line sonarjs/cognitive-complexity
    this.route.paramMap.subscribe(params => {
      if (params.get(NAME)) {
        this.k8sApi
          .getResource<PrometheusRule>({
            type: RESOURCE_TYPES.PROMETHEUS_RULE,
            cluster: this.clusterName,
            namespace: this.namespace || this.globalNamespace,
            name: params.get(NAME),
          })
          .pipe(
            catchError(() => {
              return EMPTY;
            }),
          )
          .subscribe(result => {
            if (result) {
              this.formSubmitText = 'update';
              this.resource = result;
              let actions: AlarmAction[];
              try {
                actions = JSON.parse(
                  this.k8sUtil.getAnnotation(result, 'notifications', 'alert'),
                );
              } catch {
                actions = [];
              }
              this.viewModel = {
                name: result.metadata.name,
                kind: this.k8sUtil
                  .getLabel(result, 'kind', 'alert')
                  .toLowerCase(),
                description: this.k8sUtil.getDescription(result),
                rules: result.spec.groups[0].rules,
                actions,
              };
              if (this.viewModel.kind === 'node') {
                this.canChangeResourceType = false;
                const address = this.k8sUtil.getLabel(result, NAME, 'alert');
                if (address === '0.0.0.0') {
                  this.viewModel.node = this.allNodes;
                } else if (this.nodes.length > 0) {
                  this.viewModel.node = this.nodes.find(
                    node => node.address === address,
                  );
                } else {
                  this.viewModel.node = {
                    address,
                    hostname: '',
                  };
                }
              }
            }
            this.cdr.markForCheck();
          });
      }
    });
    this.k8sApi
      .getResourceList<Node>({
        type: RESOURCE_TYPES.NODE,
        cluster: this.clusterName,
      })
      .subscribe(({ items }) => {
        this.nodes = items.map(node => ({
          address: node.status.addresses.find(
            addr => addr.type === 'InternalIP',
          ).address,
          hostname: node.status.addresses.find(addr => addr.type === 'Hostname')
            .address,
        }));
        if (this.viewModel.node && this.viewModel.node.address !== '0.0.0.0') {
          this.viewModel.node = this.nodes.find(
            node => node.address === this.viewModel.node.address,
          );
        } else {
          this.viewModel.node = this.allNodes;
        }
        this.cdr.markForCheck();
      });
    this.k8sApi
      .getResourceList<ClusterFeature>({
        type: RESOURCE_TYPES.FEATURE,
        cluster: this.clusterName,
        queryParams: {
          labelSelector: 'instanceType=prometheus',
        },
      })
      .subscribe(features => {
        if (features.items.length > 0) {
          this.prometheusName = features.items[0].spec.accessInfo.name;
          this.prometheusNamespace =
            features.items[0].spec.accessInfo.namespace;
        }
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    if (this.viewModel.rules.length === 0) {
      this.auiNotificationService.error(
        this.translate.get('alarm_rules_error'),
      );
      return;
    }
    const duplicate = hasDuplicateBy(this.viewModel.rules, 'expr');
    if (duplicate) {
      this.auiNotificationService.error(
        this.translate.get('alarm_template_rules_duplicate'),
      );
      return;
    }
    this.viewModel.actions = this.viewModel.actions || [];
    this.viewModel.rules.forEach(rule => {
      rule.annotations.alert_notifications = JSON.stringify(
        this.viewModel.actions,
      );
    });
    this.submitting = true;
    if (this.resource) {
      const updateTime = new Date();
      this.k8sApi
        .patchResource<PrometheusRule>({
          type: RESOURCE_TYPES.PROMETHEUS_RULE,
          cluster: this.clusterName,
          resource: this.resource,
          part: {
            metadata: {
              annotations: {
                [this.k8sUtil.normalizeType(DESCRIPTION)]: this.viewModel
                  .description,
                [this.k8sUtil.normalizeType(
                  'updated-at',
                )]: updateTime.toISOString(),
                [this.k8sUtil.normalizeType(
                  'notifications',
                  'alert',
                )]: JSON.stringify(this.viewModel.actions),
              },
              labels: {
                [this.k8sUtil.normalizeType(NAME, 'alert')]:
                  this.viewModel.kind === 'node'
                    ? this.viewModel.node && this.viewModel.node.address
                    : this.clusterName,
              },
            },
            spec: {
              groups: [
                {
                  name: 'general',
                  rules: this.viewModel.rules,
                },
              ],
            },
          },
        })
        .subscribe(
          () => {
            this.location.back();
            this.messageService.success(
              this.translate.get('alarm_update_success'),
            );
          },
          () => {
            this.submitting = false;
            this.cdr.markForCheck();
          },
        );
    } else {
      this.k8sApi
        .postResource<PrometheusRule>({
          type: RESOURCE_TYPES.PROMETHEUS_RULE,
          cluster: this.clusterName,
          resource: {
            apiVersion: PrometheusRuleMeta.apiVersion,
            kind: PrometheusRuleMeta.kind,
            metadata: {
              name: this.viewModel.name,
              namespace: this.prometheusNamespace,
              annotations: {
                [this.k8sUtil.normalizeType(DESCRIPTION)]: this.viewModel
                  .description,
                [this.k8sUtil.normalizeType(
                  'notifications',
                  'alert',
                )]: JSON.stringify(this.viewModel.actions),
              },
              labels: {
                [this.k8sUtil.normalizeType('cluster', 'alert')]: this
                  .clusterName,
                [this.k8sUtil.normalizeType('kind', 'alert')]: upperFirst(
                  this.viewModel.kind,
                ),
                [this.k8sUtil.normalizeType(NAME, 'alert')]:
                  this.viewModel.kind === 'node'
                    ? this.viewModel.node && this.viewModel.node.address
                    : this.clusterName,
                [this.k8sUtil.normalizeType(NAMESPACE, 'alert')]: this
                  .prometheusNamespace,
                [this.k8sUtil.normalizeType('project', 'alert')]: '',
                [this.k8sUtil.normalizeType('owner', 'alert')]: 'System',
                prometheus: this.prometheusName,
              },
            },
            spec: {
              groups: [
                {
                  name: 'general',
                  rules: this.viewModel.rules,
                },
              ],
            },
          },
        })
        .subscribe(
          () => {
            this.router.navigate(
              ['/maintenance-center/alarm/detail', this.viewModel.name],
              {
                queryParams: {
                  cluster: this.clusterName,
                  namespace: this.prometheusNamespace,
                },
              },
            );
            this.messageService.success(
              this.translate.get('alarm_create_success'),
            );
          },
          () => {
            this.submitting = false;
            this.cdr.markForCheck();
          },
        );
    }
  }

  cancel() {
    this.location.back();
  }

  onRulesChange(rules: PrometheusRule[]): void {
    if (rules) {
      this.canChangeResourceType = rules.length === 0;
    }
  }
}
