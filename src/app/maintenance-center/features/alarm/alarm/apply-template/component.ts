import {
  K8sApiService,
  NAME,
  NAMESPACE,
  TranslateService,
} from '@alauda/common-snippet';
import { DIALOG_DATA, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { upperFirst } from 'lodash-es';
import { Subject, of } from 'rxjs';
import { catchError, pluck, takeUntil } from 'rxjs/operators';
import { Md5 } from 'ts-md5';

import { IndicatorType } from 'app/api/alarm/metric.service';
import { K8sUtilService } from 'app/services/k8s-util.service';
import {
  AlertTemplate,
  AlertTemplateItem,
  ClusterFeature,
  Node,
  PrometheusRule,
  PrometheusRuleItem,
  PrometheusRuleItemLabel,
} from 'app/typings';
import {
  LOOSE_K8S_RESOURCE_NAME_BASE,
  PrometheusRuleMeta,
  RESOURCE_TYPES,
  ResourceType,
} from 'app/utils';

import {
  getAlarmTypeTitle,
  getRule,
  parseAlertList,
  parseGolangTemplate,
  parseNotifications,
  randomString,
} from '../../alarm.util';

enum TemplateKind {
  Cluster,
  Node,
}

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AlarmTemplateApplyComponent implements OnInit, OnDestroy {
  @ViewChild('form', { static: true })
  form: NgForm;

  @Output()
  close: EventEmitter<boolean> = new EventEmitter();

  onDestroy$ = new Subject<void>();

  loading = true;
  initialized = false;
  submitting = false;

  nodes: Array<{
    address: string;
    hostname: string;
  }> = [];

  metricType: IndicatorType[];
  viewModel: {
    template?: AlertTemplate;
    name?: string;
    node?: {
      address: string;
      hostname: string;
    };
  } = {};

  resourceNameReg = LOOSE_K8S_RESOURCE_NAME_BASE;
  templatesGroup: AlertTemplate[][];
  prometheusName: string;
  prometheusNamespace: string;
  parseAlertList = parseAlertList;
  parseNotifications = parseNotifications;

  getRule = getRule;
  getAlarmTypeTitle = getAlarmTypeTitle;

  constructor(
    @Inject(DIALOG_DATA)
    public modalData: {
      metricType: IndicatorType[];
      cluster: string;
      isOCP: boolean;
    },
    private readonly cdr: ChangeDetectorRef,
    private readonly translate: TranslateService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    private readonly router: Router,
    private readonly messageService: MessageService,
  ) {}

  ngOnInit() {
    this.metricType = this.modalData.metricType;
    this.k8sApi
      .getGlobalResourceList<AlertTemplate>({
        type: RESOURCE_TYPES.ALERT_TEMPLATE,
      })
      .pipe(
        pluck('items'),
        catchError(() => of([])),
      )
      .subscribe(alarmTemplates => {
        this.templatesGroup = [[], []];
        alarmTemplates.forEach(el => {
          const kind = this.k8sUtil.getLabel(el, 'kind').toLowerCase();
          if (kind === 'cluster') {
            this.templatesGroup[TemplateKind.Cluster].push(el);
          } else if (kind === 'node') {
            this.templatesGroup[TemplateKind.Node].push(el);
          }
        });
        this.loading = false;
        this.initialized = true;
        this.cdr.markForCheck();
      });
    this.k8sApi
      .getResourceList<Node>({
        type: RESOURCE_TYPES.NODE,
        cluster: this.modalData.cluster,
      })
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(({ items }) => {
        this.nodes = items.map(node => ({
          address: node.status.addresses.find(
            addr => addr.type === 'InternalIP',
          ).address,
          hostname: node.status.addresses.find(addr => addr.type === 'Hostname')
            .address,
        }));
      });
    this.k8sApi
      .getResourceList<ClusterFeature>({
        type: RESOURCE_TYPES.FEATURE,
        cluster: this.modalData.cluster,
        queryParams: {
          labelSelector: 'instanceType=prometheus',
        },
      })
      .subscribe(features => {
        if (features.items.length > 0) {
          this.prometheusName = features.items[0].spec.accessInfo.name;
          this.prometheusNamespace =
            features.items[0].spec.accessInfo.namespace;
        }
      });
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.submitting = true;

    const kind = this.k8sUtil.getLabel(this.viewModel.template, 'kind');
    const rules = this.viewModel.template.spec.templates.map(template =>
      this.getPrometheusRule(template, kind),
    );
    this.k8sApi
      .postResource<PrometheusRule>({
        type: RESOURCE_TYPES.PROMETHEUS_RULE,
        cluster: this.modalData.cluster,
        resource: {
          apiVersion: PrometheusRuleMeta.apiVersion,
          kind: PrometheusRuleMeta.kind,
          metadata: {
            name: this.viewModel.name,
            namespace: this.prometheusNamespace,
            annotations: {
              [this.k8sUtil.normalizeType(
                'notifications',
                'alert',
              )]: this.k8sUtil.getAnnotation(
                this.viewModel.template,
                'notifications',
                'alert',
              ),
            },
            labels: {
              [this.k8sUtil.normalizeType('cluster', 'alert')]: this.modalData
                .cluster,
              [this.k8sUtil.normalizeType('kind', 'alert')]: upperFirst(kind),
              [this.k8sUtil.normalizeType(NAME, 'alert')]:
                (this.viewModel.node && this.viewModel.node.address) ||
                this.modalData.cluster,
              [this.k8sUtil.normalizeType(NAMESPACE, 'alert')]: this
                .prometheusNamespace,
              [this.k8sUtil.normalizeType('project', 'alert')]: '',
              [this.k8sUtil.normalizeType('owner', 'alert')]: 'System',
              prometheus: this.prometheusName,
            },
          },
          spec: {
            groups: [
              {
                name: 'general',
                rules,
              },
            ],
          },
        },
      })
      .subscribe(
        () => {
          this.router.navigate(
            ['/maintenance-center/alarm/detail', this.viewModel.name],
            {
              queryParams: {
                cluster: this.modalData.cluster,
                namespace: this.prometheusNamespace,
              },
            },
          );
          this.messageService.success(
            this.translate.get('alarm_template_apply_success'),
          );
          this.close.next(true);
        },
        () => {
          this.submitting = false;
          this.cdr.markForCheck();
        },
      );
  }

  // eslint-disable-next-line sonarjs/cognitive-complexity
  getPrometheusRule(
    template: AlertTemplateItem,
    kind: string,
  ): PrometheusRuleItem {
    let resourceName: string;
    let groupName: string;
    let alertInvolvedObjectName: string;
    let alertIndicator = '';
    let alertIndicatorUnit: string;
    let customExpr: string;
    let expr: string;
    let alertIndicatorQuery: string;
    if (kind === 'cluster') {
      resourceName = `cluster-${this.modalData.cluster}.rules`;
      groupName = `cluster.${this.modalData.cluster}`;
      alertInvolvedObjectName = this.modalData.cluster;
    } else {
      resourceName = `node-${this.viewModel.node.address}.rules`;
      groupName = `node.${this.viewModel.node.address}`;
      alertInvolvedObjectName = this.viewModel.node.address;
    }
    const query = template.metric.queries[0];
    query.labels.forEach((label: { name: string; value: string }) => {
      switch (label.name) {
        case '__name__': {
          alertIndicator = label.value;
          const found = this.metricType.find(el => {
            return el.name === label.value;
          });
          if (found) {
            alertIndicatorUnit = found.unit;
          }
          break;
        }
        case 'expr': {
          customExpr = label.value;
          break;
        }
        case 'query': {
          alertIndicatorQuery = label.value;
          break;
        }
      }
    });
    if (alertIndicator === 'custom') {
      alertIndicatorUnit = template.unit;
    }
    const labels: PrometheusRuleItemLabel = {
      ...template.labels,
      severity: template.labels.severity,
      alert_name: '',
      alert_involved_object_kind: upperFirst(kind),
      alert_involved_object_name: alertInvolvedObjectName,
      alert_involved_object_namespace: this.prometheusNamespace || '',
      alert_cluster: this.modalData.cluster,
      alert_project: '',
      alert_indicator: alertIndicator,
      alert_indicator_aggregate_range: String(query.range),
      alert_indicator_aggregate_function: query.aggregator,
      alert_indicator_comparison: template.compare,
      alert_indicator_threshold: String(template.threshold),
      alert_indicator_query: alertIndicatorQuery,
      alert_indicator_unit: alertIndicatorUnit,
    };
    const annotations = {
      ...template.annotations,
      alert_current_value: '{{ $value }}',
    };
    if (alertIndicator && alertIndicator !== 'custom') {
      const metric = this.metricType.find(
        ({ name }) => name === alertIndicator,
      );
      if (metric.query) {
        let nodeName = '';
        if (kind === 'node') {
          if (this.modalData.isOCP) {
            nodeName = this.viewModel.node.hostname;
          } else {
            nodeName = this.viewModel.node.address + ':.*';
          }
          if (labels.alert_involved_object_name === '0.0.0.0') {
            nodeName = '.*';
          }
        }
        expr = parseGolangTemplate(metric.query, {
          ...labels,
          alert_involved_object_name:
            nodeName || labels.alert_involved_object_name,
          hostname:
            kind === 'cluster'
              ? ''
              : labels.alert_involved_object_name === '0.0.0.0'
              ? '.*'
              : this.viewModel.node.hostname,
        });
      }
    } else {
      expr = customExpr;
    }
    const alertName = `${labels.alert_indicator}-${randomString()}`;
    labels.alert_name = alertName;
    return {
      alert: `${alertName}-${Md5.hashStr(resourceName + groupName + expr)}`,
      annotations,
      expr:
        expr +
        labels.alert_indicator_comparison +
        labels.alert_indicator_threshold,
      for: (template.wait || 60) + 's',
      labels,
    };
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  cancel() {
    this.close.next(false);
  }
}
