import {
  COMMON_WRITABLE_ACTIONS,
  DESCRIPTION,
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  METADATA,
  NAME,
  ResourceListParams,
  TranslateService,
  matchLabelsToString,
  noop,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService, DialogSize, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { get } from 'lodash-es';
import { BehaviorSubject, combineLatest, of } from 'rxjs';
import { catchError, map, pluck, switchMap } from 'rxjs/operators';

import { IndicatorType, MetricService } from 'app/api/alarm/metric.service';
import { MaintenanceCenterComponent } from 'app/maintenance-center/component';
import { K8sUtilService } from 'app/services/k8s-util.service';
import { Namespace, Node, PrometheusRule } from 'app/typings';
import { ACTION, RESOURCE_TYPES, ResourceType, STATUS } from 'app/utils';

import { parseNotifications } from '../../alarm.util';
import { AlarmTemplateApplyComponent } from '../apply-template/component';
const ALL_NAMESPACE_SYMBOL = Symbol('All Namespaces');

@Component({
  selector: 'alu-prometheus-list',
  styleUrls: ['styles.scss'],
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PrometheusAlarmComponent implements OnInit {
  selectedNamespace:
    | string
    | typeof ALL_NAMESPACE_SYMBOL = ALL_NAMESPACE_SYMBOL;

  ALL_NAMESPACE_SYMBOL = ALL_NAMESPACE_SYMBOL;
  parseNotifications = parseNotifications;
  columns = [
    NAME,
    'metric',
    STATUS,
    'notification',
    DESCRIPTION,
    'create_time',
    ACTION,
  ];

  clusterName: string;
  metricType: IndicatorType[];
  isOCP = false;
  clusterDisplayName: string;
  nodes: Array<{
    address: string;
    hostname: string;
  }> = [];

  alertState: Array<{
    name: string;
    state: string;
  }>;

  cluster$ = this.layoutComponent.getCluster$();
  selectedNamespace$ = new BehaviorSubject<
    string | typeof ALL_NAMESPACE_SYMBOL
  >(ALL_NAMESPACE_SYMBOL);

  params$ = combineLatest([
    this.activatedRoute.queryParamMap,
    this.cluster$,
    this.selectedNamespace$,
  ]).pipe(
    map(([params, cluster, selectedNamespace]) => ({
      cluster: cluster.metadata.name,
      keyword: params.get('keyword'),
      labelSelector: matchLabelsToString({
        [this.k8sUtil.normalizeType('owner', 'alert')]: 'System',
      }),
      namespace: this.getActualNamespace(selectedNamespace),
    })),
  );

  searchKey$ = this.activatedRoute.queryParamMap.pipe(
    map(params => params.get('keyword')),
  );

  list = new K8SResourceList({
    fetcher: this.fetchAlarmList.bind(this),
    fetchParams$: this.params$,
  });

  namespaceOptions$ = this.cluster$.pipe(
    switchMap(cluster =>
      this.k8sApi
        .getResourceList<Namespace>({
          type: RESOURCE_TYPES.NAMESPACE,
          cluster: get(cluster, [METADATA, NAME]),
        })
        .pipe(
          pluck('items'),
          catchError(() => of([] as Namespace[])),
        ),
    ),
    map(items => items.map(({ metadata: { name } }) => name)),
    publishRef(),
  );

  permissions$ = combineLatest([this.cluster$, this.selectedNamespace$]).pipe(
    switchMap(([cluster, selectedNamespace]) =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.PROMETHEUS_RULE,
        cluster: get(cluster, [METADATA, NAME]),
        namespace: this.getActualNamespace(selectedNamespace),
        action: COMMON_WRITABLE_ACTIONS,
      }),
    ),
  );

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly activatedRoute: ActivatedRoute,
    private readonly dialogService: DialogService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly layoutComponent: MaintenanceCenterComponent,
    private readonly messageService: MessageService,
    private readonly metricService: MetricService,
    private readonly router: Router,
    private readonly translate: TranslateService,
  ) {}

  ngOnInit() {
    this.layoutComponent.getCluster$().subscribe(cluster => {
      this.metricService
        .getIndicators(cluster.metadata.name)
        .subscribe(metricType => {
          this.metricType = metricType;
        });
      this.clusterName = cluster.metadata.name;
      this.clusterDisplayName = this.k8sUtil.getDisplayName(cluster);
      this.isOCP = this.k8sUtil.getLabel(cluster, 'cluster-type') === 'OCP';
      this.metricService
        .getPrometheusMetricLabels(cluster.metadata.name, 'ALERTS{}')
        .subscribe(labels => {
          if (labels) {
            this.alertState = labels.map(el => ({
              name: el.metric.alertname,
              state: el.metric.alertstate,
            }));
            this.cdr.markForCheck();
          }
        });
      this.k8sApi
        .getResourceList<Node>({
          type: RESOURCE_TYPES.NODE,
          cluster: cluster.metadata.name,
        })
        .subscribe(({ items }) => {
          this.nodes = items.map(node => ({
            address: node.status.addresses.find(
              addr => addr.type === 'InternalIP',
            ).address,
            hostname: node.status.addresses.find(
              addr => addr.type === 'Hostname',
            ).address,
          }));
          this.cdr.markForCheck();
        });
    });
  }

  getDisplayName = (item: PrometheusRule) => {
    const kind = this.k8sUtil.getLabel(item, 'kind', 'alert');
    if (kind === 'Cluster') {
      return this.clusterDisplayName ? ` (${this.clusterDisplayName})` : '';
    } else if (kind === 'Node') {
      const address = this.k8sUtil.getLabel(item, NAME, 'alert');
      const node = this.nodes.find(node => node.address === address);
      if (node) {
        return ` (${node.hostname})`;
      }
    }
    return '';
  };

  trackByFn(_: number, item: PrometheusRule) {
    return item.metadata.uid;
  }

  getActualNamespace(namespace: string | typeof ALL_NAMESPACE_SYMBOL) {
    return namespace === ALL_NAMESPACE_SYMBOL ? '' : namespace;
  }

  onNamespaceChange(namespace: string | typeof ALL_NAMESPACE_SYMBOL) {
    this.selectedNamespace$.next(namespace);
  }

  fetchAlarmList({ cluster, namespace, ...queryParams }: ResourceListParams) {
    return this.k8sApi.getResourceList<PrometheusRule>({
      type: RESOURCE_TYPES.PROMETHEUS_RULE,
      cluster,
      namespace,
      queryParams,
    });
  }

  searchByName(keyword: string) {
    this.router.navigate(['./'], {
      queryParams: {
        keyword,
      },
      queryParamsHandling: 'merge',
      relativeTo: this.activatedRoute,
    });
  }

  createAlarm() {
    this.router.navigate(['/maintenance-center/alarm/create'], {
      queryParams: {
        cluster: this.clusterName,
      },
    });
  }

  createAlarmTemplate() {
    const dialogRef = this.dialogService.open(AlarmTemplateApplyComponent, {
      data: {
        metricType: this.metricType,
        cluster: this.clusterName,
        isOCP: this.isOCP,
      },
      size: DialogSize.Large,
    });
    dialogRef.componentInstance.close.subscribe(() => {
      dialogRef.close();
      this.list.reload();
    });
  }

  updateAlarm(rule: PrometheusRule) {
    this.router.navigate(
      ['/maintenance-center/alarm/update', rule.metadata.name],
      {
        queryParams: {
          cluster: this.clusterName,
          namespace: rule.metadata.namespace,
        },
      },
    );
  }

  deleteAlarm(rule: PrometheusRule) {
    this.dialogService
      .confirm({
        title: this.translate.get('delete_alarm_title'),
        content: this.translate.get('delete_alarm_content', {
          alarm_name: rule.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) => {
          this.k8sApi
            .deleteResource({
              type: RESOURCE_TYPES.PROMETHEUS_RULE,
              cluster: this.k8sUtil.getLabel(rule, 'cluster', 'alert'),
              resource: rule,
            })
            .subscribe(() => {
              resolve();
              this.messageService.success(this.translate.get('delete_success'));
            }, reject);
        },
      })
      .then(() => this.list.delete(rule))
      .catch(noop);
  }

  shouldDisable = (rule: PrometheusRule) => {
    const kind = this.k8sUtil.getLabel(rule, 'kind', 'alert');
    return kind !== 'Cluster' && kind !== 'Node';
  };
}
