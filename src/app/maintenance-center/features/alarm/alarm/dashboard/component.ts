import { K8sApiService } from '@alauda/common-snippet';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, of } from 'rxjs';
import { catchError, map, switchMap, takeUntil } from 'rxjs/operators';

import { MaintenanceCenterComponent } from 'app/maintenance-center/component';
import { ClusterFeature } from 'app/typings';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class AlarmDashboardComponent implements OnInit, OnDestroy {
  loading = true;
  metricDocked: boolean;
  private readonly onDestroy$ = new Subject<void>();
  constructor(
    private readonly layoutComponent: MaintenanceCenterComponent,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.layoutComponent
      .getCluster$()
      .pipe(
        takeUntil(this.onDestroy$),
        switchMap(cluster =>
          this.k8sApi
            .getResourceList<ClusterFeature>({
              type: RESOURCE_TYPES.FEATURE,
              cluster: cluster.metadata.name,
              queryParams: {
                labelSelector: 'instanceType=prometheus',
              },
            })
            .pipe(
              map(res => res.items),
              catchError(() => of([])),
            ),
        ),
      )
      .subscribe(
        features => {
          this.metricDocked = features.length > 0;
          this.loading = false;
          this.cdr.markForCheck();
        },
        _err => {
          this.loading = false;
          this.cdr.markForCheck();
        },
      );
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }
}
