import {
  AsyncDataLoader,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  NAME,
  TranslateService,
  noop,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import { IndicatorType, MetricService } from 'app/api/alarm/metric.service';
import { AlertTemplate } from 'app/typings';
import { RESOURCE_TYPES, ResourceType, TYPE } from 'app/utils';

import {
  getAlarmTypeTitle,
  getRule,
  parseAlertList,
  parseNotifications,
} from '../../alarm.util';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AlarmTemplateDetailComponent implements OnInit {
  metricType: IndicatorType[] = [];
  parseAlertList = parseAlertList;
  parseNotifications = parseNotifications;
  columns = ['rule', TYPE, 'level'];

  getRule = getRule;
  getAlarmTypeTitle = getAlarmTypeTitle;

  params$ = this.route.paramMap.pipe(
    map(params => ({
      name: params.get(NAME),
    })),
    publishRef(),
  );

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.ALERT_TEMPLATE,
    action: [K8sResourceAction.DELETE, K8sResourceAction.UPDATE],
  });

  dataManager = new AsyncDataLoader<{
    resource: AlertTemplate;
    permissions: {
      update: boolean;
      delete: boolean;
    };
  }>({
    params$: this.params$,
    fetcher: params => {
      return combineLatest([
        this.k8sApi.getGlobalResource<AlertTemplate>({
          type: RESOURCE_TYPES.ALERT_TEMPLATE,
          name: params.name,
        }),
        this.permissions$,
      ]).pipe(
        map(([resource, permissions]) => ({
          resource,
          permissions,
        })),
      );
    },
  });

  constructor(
    private readonly router: Router,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sPermission: K8sPermissionService,
    private readonly dialogService: DialogService,
    private readonly route: ActivatedRoute,
    private readonly metricService: MetricService,
    private readonly translate: TranslateService,
    private readonly messageService: MessageService,
  ) {}

  ngOnInit() {
    this.metricService.getIndicators().subscribe(metricType => {
      this.metricType = metricType;
    });
  }

  trackByFn(index: number) {
    return index;
  }

  updateAlarmTemplate(alarmTemplate: AlertTemplate) {
    this.router.navigate([
      '/maintenance-center/alarm_template/update',
      alarmTemplate.metadata.name,
    ]);
  }

  deleteAlarmTemplate(alertTemplate: AlertTemplate) {
    this.dialogService
      .confirm({
        title: this.translate.get('delete_alarm_template_title'),
        content: this.translate.get('delete_alarm_template_content', {
          alarm_name: alertTemplate.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) => {
          this.k8sApi
            .deleteGlobalResource<AlertTemplate>({
              type: RESOURCE_TYPES.ALERT_TEMPLATE,
              resource: alertTemplate,
            })
            .subscribe(() => {
              resolve();
              this.messageService.success(this.translate.get('delete_success'));
            }, reject);
        },
      })
      .then(this.jumpToListPage)
      .catch(noop);
  }

  jumpToListPage = () => {
    this.router.navigate(['/maintenance-center/alarm_template']);
  };
}
