import {
  DESCRIPTION,
  K8sApiService,
  NAME,
  TranslateService,
} from '@alauda/common-snippet';
import { MessageService, NotificationService } from '@alauda/ui';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EMPTY, Subject } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { K8sUtilService } from 'app/services/k8s-util.service';
import {
  AlarmAction,
  AlertTemplate,
  AlertTemplateItem,
  AlertTemplateSpec,
} from 'app/typings';
import {
  AlertTemplateMeta,
  K8S_RESOURCE_NAME_BASE,
  RESOURCE_TYPES,
  ResourceType,
} from 'app/utils';

interface AlertTemplateModel extends AlertTemplateSpec {
  name: string;
  kind: string;
  description: string;
  actions: AlarmAction[];
}

@Component({
  templateUrl: 'template.html',
  styles: [
    `
      .form-wrapper {
        margin-bottom: 52px;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AlarmTemplateFormComponent implements OnInit, OnDestroy {
  @ViewChild('form', { static: false })
  form: NgForm;

  cluster: string;
  name: string;
  resource: AlertTemplate;
  formSubmitText = 'create';
  initialized = true;
  submitting = false;
  canChangeResourceType = true;
  resourceNameReg = K8S_RESOURCE_NAME_BASE;

  viewModel: AlertTemplateModel = {
    name: '',
    kind: 'cluster',
    description: '',
    templates: [],
    actions: [],
  };

  onDestroy$ = new Subject<void>();

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly location: Location,
    private readonly translate: TranslateService,
    private readonly auiNotificationService: NotificationService,
    private readonly messageService: MessageService,
  ) {}

  ngOnInit() {
    // eslint-disable-next-line sonarjs/cognitive-complexity
    this.route.paramMap.subscribe(params => {
      if (params.get(NAME)) {
        this.initialized = false;
        this.k8sApi
          .getGlobalResource<AlertTemplate>({
            type: RESOURCE_TYPES.ALERT_TEMPLATE,
            name: params.get(NAME),
          })
          .pipe(
            catchError(() => {
              return EMPTY;
            }),
          )
          .subscribe(result => {
            if (result) {
              this.formSubmitText = 'update';
              this.resource = result;
              result.spec.templates = result.spec.templates.map(item => {
                let metricName = '';
                let expr = '';
                item.metric.queries[0].labels.forEach(
                  (label: { name: string; value: string }) => {
                    if (label.name === '__name__') {
                      metricName = label.value;
                    }
                    if (label.name === 'expr') {
                      expr = label.value;
                    }
                  },
                );
                return {
                  ...item,
                  metric_name: metricName,
                  expr,
                };
              });
              let actions: AlarmAction[];
              try {
                actions = JSON.parse(
                  this.k8sUtil.getAnnotation(result, 'notifications', 'alert'),
                );
              } catch {
                actions = [];
              }
              this.viewModel = {
                name: result.metadata.name,
                kind: this.k8sUtil.getLabel(result, 'kind'),
                description: this.k8sUtil.getDescription(result),
                templates: result.spec.templates,
                actions,
              };
            }
            this.initialized = true;
            this.cdr.markForCheck();
          });
      }
    });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    if (this.viewModel.templates.length === 0) {
      this.auiNotificationService.error(
        this.translate.get('alarm_template_rules_error'),
      );
      return;
    }
    const metricExpressions = this.viewModel.templates.map(
      template =>
        (template.expr || template.metric_name) +
        template.compare +
        template.threshold,
    );
    const duplicates = metricExpressions.filter(
      (item, index) => metricExpressions.indexOf(item) !== index,
    );
    if (duplicates.length > 0) {
      this.auiNotificationService.error(
        this.translate.get('alarm_template_rules_duplicate'),
      );
      return;
    }
    this.viewModel.actions = this.viewModel.actions || [];
    this.viewModel.templates.forEach(template => {
      template.notifications = this.viewModel.actions;
    });
    const body = this.viewModel;
    this.submitting = true;
    if (this.resource) {
      const updateTime = new Date();
      this.k8sApi
        .patchGlobalResource<AlertTemplate>({
          type: RESOURCE_TYPES.ALERT_TEMPLATE,
          resource: this.resource,
          part: {
            metadata: {
              annotations: {
                [this.k8sUtil.normalizeType(DESCRIPTION)]: this.viewModel
                  .description,
                [this.k8sUtil.normalizeType(
                  'updated-at',
                )]: updateTime.toISOString(),
                [this.k8sUtil.normalizeType(
                  'notifications',
                  'alert',
                )]: JSON.stringify(this.viewModel.actions),
              },
              labels: {
                [this.k8sUtil.normalizeType('kind')]: this.viewModel.kind,
              },
            },
            spec: {
              templates: this.viewModel.templates,
            },
          },
        })
        .subscribe(
          () => {
            this.location.back();
            this.messageService.success(
              this.translate.get('alarm_template_update_success'),
            );
          },
          () => {
            this.submitting = false;
            this.cdr.markForCheck();
          },
        );
    } else {
      this.k8sApi
        .postGlobalResource<AlertTemplate>({
          type: RESOURCE_TYPES.ALERT_TEMPLATE,
          resource: {
            apiVersion: AlertTemplateMeta.apiVersion,
            kind: AlertTemplateMeta.kind,
            metadata: {
              name: this.viewModel.name,
              annotations: {
                [this.k8sUtil.normalizeType(DESCRIPTION)]: this.viewModel
                  .description,
                [this.k8sUtil.normalizeType(
                  'notifications',
                  'alert',
                )]: JSON.stringify(this.viewModel.actions),
              },
              labels: {
                [this.k8sUtil.normalizeType('kind')]: this.viewModel.kind,
              },
            },
            spec: {
              templates: this.viewModel.templates,
            },
          },
        })
        .subscribe(
          () => {
            this.router.navigate([
              '/maintenance-center/alarm_template/detail',
              body.name,
            ]);
            this.messageService.success(
              this.translate.get('alarm_template_create_success'),
            );
          },
          () => {
            this.submitting = false;
            this.cdr.markForCheck();
          },
        );
    }
  }

  cancel() {
    this.location.back();
  }

  onRulesChange(rules: AlertTemplateItem[]): void {
    if (rules) {
      this.canChangeResourceType = rules.length === 0;
    }
  }
}
