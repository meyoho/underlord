import { DIALOG_DATA } from '@alauda/ui';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, NgForm } from '@angular/forms';

import { Alarm } from 'app/api/alarm/alarm.service';
import { IndicatorType, MetricQueries } from 'app/api/alarm/metric.service';

import { parseAlertList, randomString } from '../../alarm.util';

@Component({
  templateUrl: 'template.html',
})
export class AlarmTemplateRuleDialogComponent implements OnInit {
  @Output()
  close = new EventEmitter<boolean | {}>();

  formSubmitText = 'add';
  form = this.fb.group({
    rule: this.fb.control({}),
  });

  constructor(
    @Inject(DIALOG_DATA)
    public modalData: {
      kind: string;
      metricType: IndicatorType[];
      data: Alarm;
    },
    private readonly fb: FormBuilder,
  ) {}

  ngOnInit() {
    if (this.modalData.data) {
      this.formSubmitText = 'edit';
      const data = parseAlertList(
        [this.modalData.data],
        '',
        this.modalData.metricType,
      )[0];
      const rule = {
        alarm_type: '',
        data_type: false,
        aggregate_range: 0,
        aggregate_function: '',
        threshold: data.threshold,
        unit: '',
        expr: '',
        query: '',
        compare: data.compare,
        metric: data.metric_name,
        wait: data.wait,
        severity: data.labels.severity,
        labels: data.labels,
        annotations: data.annotations,
      };
      if (data.metric_name === 'custom') {
        rule.alarm_type = 'custom';
        rule.expr = data.expr;
        rule.unit = data.unit;
      } else if (data.metric_name === 'workload.log.keyword.count') {
        rule.alarm_type = 'log';
        rule.query = data.query;
      } else if (data.metric_name === 'workload.event.reason.count') {
        rule.alarm_type = 'event';
        rule.query = data.query;
      } else {
        rule.alarm_type = 'default';
      }
      if (data.metric.queries && data.metric.queries[0]) {
        const aggregator = data.metric.queries[0].aggregator;
        if (aggregator !== undefined && aggregator !== '') {
          rule.data_type = true;
          rule.aggregate_function = aggregator;
        }
        rule.aggregate_range = data.metric.queries[0].range;
      }
      this.form.patchValue({
        rule,
      });
    }
  }

  confirm(ruleForm: NgForm) {
    ruleForm.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    const rule = this.form.value.rule;
    const query: MetricQueries = {
      aggregator: rule.aggregate_function,
      range: rule.aggregate_range,
      labels: [],
    };
    if (rule.alarm_type === 'custom') {
      query.labels = [
        {
          type: 'EQUAL',
          name: '__name__',
          value: 'custom',
        },
        {
          type: 'EQUAL',
          name: 'expr',
          value: rule.expr,
        },
      ];
    } else if (rule.alarm_type === 'log' || rule.alarm_type === 'event') {
      query.labels = query.labels.concat([
        {
          type: 'EQUAL',
          name: '__name__',
          value: rule.metric,
        },
        {
          type: 'EQUAL',
          name: 'query',
          value: rule.query,
        },
      ]);
    } else {
      query.labels = [
        {
          type: 'EQUAL',
          name: '__name__',
          value: rule.metric,
        },
      ];
    }
    const resource: Alarm = {
      metric: {
        queries: [query],
      },
      compare: rule.compare,
      wait: rule.wait || 60,
      expr: rule.expr,
      threshold: rule.threshold,
    };
    if (this.modalData.data) {
      resource.name = this.modalData.data.name;
    } else {
      if (rule.alarm_type === 'custom') {
        resource.name = `custom-${randomString()}`;
      } else {
        resource.name = `${rule.metric}-${randomString()}`;
      }
    }
    if (rule.alarm_type === 'custom') {
      resource.unit = rule.unit;
      resource.metric_name = rule.alarm_type;
    } else {
      resource.metric_name = rule.metric;
    }
    resource.labels = {
      ...rule.labels,
      severity: rule.severity,
    };
    resource.annotations = rule.annotations;
    resource.query = rule.query;
    this.close.next(resource);
  }

  cancel() {
    this.close.next(false);
  }
}
