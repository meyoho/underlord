import { NgModule } from '@angular/core';

import { AlarmSharedModule } from 'app/maintenance-center/features-shared/alarm/shared.module';
import { EventSharedModule } from 'app/maintenance-center/features-shared/event/shared.module';
import { SharedModule } from 'app/shared/shared.module';

import { AlarmTemplateDetailComponent } from './detail/component';
import { AlarmTemplateFormComponent } from './form/component';
import { AlarmTemplateComponent } from './list/component';
import { AlarmTemplateRoutingModule } from './routing.module';
import { AlarmTemplateRuleDialogComponent } from './rule-dialog/component';
import { AlarmTemplateRuleListComponent } from './rule-list/component';

@NgModule({
  imports: [
    SharedModule,
    AlarmTemplateRoutingModule,
    AlarmSharedModule,
    EventSharedModule,
  ],
  declarations: [
    AlarmTemplateComponent,
    AlarmTemplateDetailComponent,
    AlarmTemplateFormComponent,
    AlarmTemplateRuleDialogComponent,
    AlarmTemplateRuleListComponent,
  ],
  entryComponents: [AlarmTemplateRuleDialogComponent],
})
export class AlarmTemplateModule {}
