import {
  K8SResourceList,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  NAME,
  TranslateService,
  matchLabelsToString,
  noop,
  stringToMatchLabels,
} from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';

import { Alarm } from 'app/api/alarm/alarm.service';
import { IndicatorType, MetricService } from 'app/api/alarm/metric.service';
import { AlertTemplate } from 'app/typings';
import { ACTION, RESOURCE_TYPES, ResourceType, TYPE } from 'app/utils';

import {
  getAlarmTypeTitle,
  getRule,
  parseAlertList,
  parseNotifications,
} from '../../alarm.util';

@Component({
  styleUrls: ['styles.scss'],
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AlarmTemplateComponent implements OnInit {
  parseNotifications = parseNotifications;
  columns = [NAME, TYPE, 'rules', 'alarm_action', 'create_time', ACTION];
  metricType: IndicatorType[] = [];
  getRule = getRule;
  getAlarmTypeTitle = getAlarmTypeTitle;
  list = new K8SResourceList({
    fetcher: this.fetchAlertTemplateList.bind(this),
    activatedRoute: this.activatedRoute,
  });

  searchKey$ = this.activatedRoute.queryParamMap.pipe(
    map(
      params =>
        stringToMatchLabels(params.get('fieldSelector'))['metadata.name'],
    ),
  );

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.ALERT_TEMPLATE,
    action: [
      K8sResourceAction.CREATE,
      K8sResourceAction.UPDATE,
      K8sResourceAction.DELETE,
    ],
  });

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly dialogService: DialogService,
    private readonly translate: TranslateService,
    private readonly metricService: MetricService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sPermission: K8sPermissionService,
    private readonly messageService: MessageService,
  ) {}

  fetchAlertTemplateList({ ...queryParams }) {
    return this.k8sApi.getGlobalResourceList<AlertTemplate>({
      type: RESOURCE_TYPES.ALERT_TEMPLATE,
      queryParams,
    });
  }

  searchByName(name: string) {
    this.router.navigate(['./'], {
      queryParams: {
        fieldSelector: matchLabelsToString({
          'metadata.name': name,
        }),
      },
      queryParamsHandling: 'merge',
      relativeTo: this.activatedRoute,
    });
  }

  ngOnInit() {
    this.metricService.getIndicators().subscribe(metricType => {
      this.metricType = metricType;
    });
  }

  trackByFn(_: number, item: AlertTemplate) {
    return item.metadata.uid;
  }

  createAlarmTemplate() {
    this.router.navigate(['/maintenance-center/alarm_template/create']);
  }

  updateAlarmTemplate(alertTemplate: AlertTemplate) {
    this.router.navigate([
      '/maintenance-center/alarm_template/update',
      alertTemplate.metadata.name,
    ]);
  }

  deleteAlarmTemplate(alertTemplate: AlertTemplate) {
    this.dialogService
      .confirm({
        title: this.translate.get('delete_alarm_template_title'),
        content: this.translate.get('delete_alarm_template_content', {
          alarm_name: alertTemplate.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) => {
          this.k8sApi
            .deleteGlobalResource<AlertTemplate>({
              type: RESOURCE_TYPES.ALERT_TEMPLATE,
              resource: alertTemplate,
            })
            .subscribe(() => {
              resolve();
              this.messageService.success(this.translate.get('delete_success'));
            }, reject);
        },
      })
      .then(() => {
        this.list.delete(alertTemplate);
      })
      .catch(noop);
  }

  parseRules(templates: Alarm[]) {
    return parseAlertList(templates, '', this.metricType);
  }
}
