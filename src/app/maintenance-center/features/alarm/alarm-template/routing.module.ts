import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AlarmTemplateDetailComponent } from './detail/component';
import { AlarmTemplateFormComponent } from './form/component';
import { AlarmTemplateComponent } from './list/component';

const alarmRoutes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'list',
    component: AlarmTemplateComponent,
  },
  {
    path: 'create',
    component: AlarmTemplateFormComponent,
  },
  {
    path: 'update/:name',
    component: AlarmTemplateFormComponent,
  },
  {
    path: 'detail/:name',
    component: AlarmTemplateDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(alarmRoutes)],
  exports: [RouterModule],
})
export class AlarmTemplateRoutingModule {}
