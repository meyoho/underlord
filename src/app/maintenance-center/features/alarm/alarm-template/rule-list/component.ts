import { TranslateService, noop } from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  forwardRef,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { IndicatorType, MetricService } from 'app/api/alarm/metric.service';
import { AlertTemplateItem } from 'app/typings';

import { getAlarmTypeTitle, getRule } from '../../alarm.util';
import { AlarmTemplateRuleDialogComponent } from '../rule-dialog/component';

@Component({
  selector: 'alu-alarm-template-rule-list',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AlarmTemplateRuleListComponent),
      multi: true,
    },
  ],
})
export class AlarmTemplateRuleListComponent
  implements OnInit, ControlValueAccessor {
  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly dialogService: DialogService,
    private readonly metricService: MetricService,
    // getRule 方法中会用到 TranslateService，在这里需要保留
    public translate: TranslateService,
  ) {}

  @Input()
  kind: string;

  templates: AlertTemplateItem[];
  metricType: IndicatorType[] = [];

  getRule = getRule;
  getAlarmTypeTitle = getAlarmTypeTitle;
  onChange = noop;

  ngOnInit() {
    this.metricService.getIndicators().subscribe(res => {
      this.metricType = res;
    });
    this.cdr.markForCheck();
  }

  add() {
    const dialogRef = this.dialogService.open(
      AlarmTemplateRuleDialogComponent,
      {
        data: {
          kind: this.kind,
          metricType: this.metricType,
        },
        size: DialogSize.Large,
      },
    );
    dialogRef.componentInstance.close.subscribe((el: AlertTemplateItem) => {
      if (el) {
        this.templates.push(el);
        this.onChange(this.templates);
      }
      dialogRef.close();
    });
  }

  edit(index: number) {
    const dialogRef = this.dialogService.open(
      AlarmTemplateRuleDialogComponent,
      {
        data: {
          kind: this.kind,
          metricType: this.metricType,
          data: this.templates[index],
        },
        size: DialogSize.Large,
      },
    );
    dialogRef.componentInstance.close.subscribe((el: AlertTemplateItem) => {
      if (el) {
        this.templates[index] = el;
        this.onChange(this.templates);
      }
      dialogRef.close();
    });
  }

  remove(index: number) {
    this.templates.splice(index, 1);
    this.onChange(this.templates);
  }

  writeValue(templates: AlertTemplateItem[]): void {
    if (templates && this.templates !== templates) {
      this.templates = templates;
      this.cdr.markForCheck();
    }
  }

  registerOnChange(fn: (templates: AlertTemplateItem[]) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(_fn: () => void) {
    //
  }
}
