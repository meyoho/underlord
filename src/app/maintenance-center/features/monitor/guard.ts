import { K8sApiService } from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { K8sUtilService } from 'app/services/k8s-util.service';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';

import { ChooseClusterComponent } from './dialog/component';

@Injectable()
export class MonitorGuard implements CanActivate {
  constructor(
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    private readonly dialogService: DialogService,
  ) {}

  canActivate(): Observable<boolean> {
    return this.k8sApi
      .getGlobalResourceList({
        type: RESOURCE_TYPES.CLUSTER_REGISTRY,
        namespaced: true,
      })
      .pipe(
        map(cluster => cluster.items || []),
        map(clusters => {
          const index = clusters.findIndex(
            cluster => this.k8sUtil.getLabel(cluster, 'cluster-type') === 'OCP',
          );
          if (index >= 0) {
            this.dialogService.open(ChooseClusterComponent, {
              data: {
                clusters,
              },
            });
          }
          return index < 0;
        }),
      );
  }
}
