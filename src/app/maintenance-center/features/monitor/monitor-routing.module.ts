import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GrafanaDashboardComponent } from './grafana-dashboard/component';
import { MonitorGuard } from './guard';

const moitorRoutes: Routes = [
  {
    path: '',
    canActivate: [MonitorGuard],
    component: GrafanaDashboardComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(moitorRoutes)],
  exports: [RouterModule],
})
export class MonitorRoutingModule {}
