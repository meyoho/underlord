import {
  K8sApiService,
  K8sUtilService,
  TranslateService,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, DialogService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { Cluster, ClusterFeature } from 'app/typings';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChooseClusterComponent implements OnInit {
  @ViewChild('form', { static: true })
  form: NgForm;

  submitting = false;
  cluster: Cluster;

  constructor(
    @Inject(DIALOG_DATA)
    public data: {
      clusters: Cluster[];
    },
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly k8sUtil: K8sUtilService,
    private readonly dialogRef: DialogRef,
    private readonly dialogService: DialogService,
    private readonly translate: TranslateService,
  ) {}

  ngOnInit() {
    if (this.data.clusters && this.data.clusters.length > 0) {
      this.cluster = this.data.clusters[0];
    }
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.submitting = true;
    this.k8sApi
      .getResourceList<ClusterFeature>({
        type: RESOURCE_TYPES.FEATURE,
        cluster: this.cluster.metadata.name,
        queryParams: {
          labelSelector: 'instanceType=prometheus',
        },
      })
      .pipe(
        map(res => res.items),
        catchError(() => of([])),
      )
      .subscribe(features => {
        this.submitting = false;
        this.dialogRef.close();
        if (features.length > 0) {
          const grafanaUrl = features[0].spec.accessInfo.grafanaUrl;
          if (grafanaUrl) {
            window.open(grafanaUrl);
          }
        } else {
          this.dialogService.confirm({
            title: this.translate.get('cluster_no_monitor_title'),
            content: this.translate.get('cluster_no_monitor'),
            confirmText: this.translate.get('got_it'),
            cancelButton: false,
          });
        }
      });
  }

  cancel() {
    this.dialogRef.close();
  }

  label = (item: Cluster) => {
    const displayable = {
      name: this.k8sUtil.getName(item),
      displayName: this.k8sUtil.getDisplayName(item),
    };
    if (displayable.displayName) {
      return `${displayable.name} (${displayable.displayName})`;
    }
    return displayable.name;
  };
}
