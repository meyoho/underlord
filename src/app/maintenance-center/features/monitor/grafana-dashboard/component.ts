import { K8sApiService } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostListener,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Subject, of } from 'rxjs';
import { catchError, map, switchMap, takeUntil } from 'rxjs/operators';

import { MaintenanceCenterComponent } from 'app/maintenance-center/component';
import { FullScreenService } from 'app/services/full-screen.service';
import { ClusterFeature } from 'app/typings';
import { RESOURCE_TYPES, ResourceType } from 'app/utils';
@Component({
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GrafanaDashboardComponent implements OnInit, OnDestroy {
  grafanaUrl: SafeResourceUrl;
  unsafeScript = false;
  isFullScreen = false;
  private readonly onDestroy$ = new Subject<void>();
  @ViewChild('iframe', { static: false })
  iframe: ElementRef;

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.isFullScreen = this.fullScreenService.isElementFullScreen();
  }

  loading = true;
  constructor(
    private readonly layoutComponent: MaintenanceCenterComponent,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly sanitizer: DomSanitizer,
    private readonly cdr: ChangeDetectorRef,
    private readonly fullScreenService: FullScreenService,
  ) {}

  ngOnInit() {
    if (window.location.protocol === 'https:') {
      this.unsafeScript = true;
    }
    this.layoutComponent
      .getCluster$()
      .pipe(
        takeUntil(this.onDestroy$),
        switchMap(cluster =>
          this.k8sApi
            .getResourceList<ClusterFeature>({
              type: RESOURCE_TYPES.FEATURE,
              cluster: cluster.metadata.name,
              queryParams: {
                labelSelector: 'instanceType=prometheus',
              },
            })
            .pipe(
              map(res => res.items),
              catchError(() => of([])),
            ),
        ),
      )
      .subscribe(
        features => {
          if (features.length > 0) {
            const grafanaUrl = features[0].spec.accessInfo.grafanaUrl;
            if (grafanaUrl) {
              this.grafanaUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
                grafanaUrl,
              );
            }
          } else {
            this.grafanaUrl = null;
          }
          this.loading = false;
          this.cdr.markForCheck();
        },
        _err => {
          this.loading = false;
          this.cdr.markForCheck();
        },
      );
  }

  toggleFullScreen() {
    if (this.isFullScreen) {
      this.fullScreenService.exitFullscreen();
    } else {
      this.fullScreenService.requestFullscreen('#grafana');
    }
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }
}
