import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { ChooseClusterComponent } from './dialog/component';
import { GrafanaDashboardComponent } from './grafana-dashboard/component';
import { MonitorGuard } from './guard';
import { MonitorRoutingModule } from './monitor-routing.module';

@NgModule({
  imports: [SharedModule, MonitorRoutingModule],
  declarations: [ChooseClusterComponent, GrafanaDashboardComponent],
  entryComponents: [ChooseClusterComponent],
  providers: [MonitorGuard],
})
export class MonitorModule {}
