import {
  K8sPermissionService,
  K8sResourceAction,
  K8sUtilService,
} from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { get } from 'lodash-es';
import { BehaviorSubject, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { Environments } from 'app/api/envs/types';
import { ClusterPolicy, LogPolicyService, PolicyType } from 'app/api/log';
import { ENVIRONMENTS } from 'app/services/services.module';
import { RESOURCE_TYPES } from 'app/utils';

import { GlobalStrategyDialogComponent } from '../global-strategy-dialog/component';
@Component({
  selector: 'alu-global-strategy',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GlobalStrategyComponent {
  type = PolicyType;
  refresh$ = new BehaviorSubject(null);
  permissions$ = this.k8sPermissionService.isAllowed({
    type: RESOURCE_TYPES.LOGPOLICISE,
    action: K8sResourceAction.UPDATE,
  });

  data$ = this.refresh$.pipe(
    switchMap(() => this.api.getGlobalPolicyList()),
    map(res => this.handledPolicy(res.items)),
    catchError(() => of({})),
  );

  constructor(
    @Inject(ENVIRONMENTS) private readonly env: Environments,
    private readonly dialog: DialogService,
    private readonly api: LogPolicyService,
    private readonly k8sUtil: K8sUtilService,
    private readonly k8sPermissionService: K8sPermissionService,
  ) {}

  handledPolicy(res: ClusterPolicy[]) {
    return Object.values(PolicyType).reduce((prev, curr) => {
      const value = res.find(
        item =>
          this.k8sUtil.getName(item) === `${this.env.LOG_PROXY_PREFIX}${curr}`,
      );
      return {
        ...prev,
        [curr]: {
          value: get(value, 'spec.indicesKeepDays'),
          source: value,
        },
      };
    }, {});
  }

  openDialog(
    type: PolicyType,
    data: { [key: string]: { value: number; source: ClusterPolicy } },
  ) {
    const dialogRef = this.dialog.open(GlobalStrategyDialogComponent, {
      data: {
        type,
        value: get(data[type], 'value'),
        source: get(data[type], 'source'),
      },
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.refresh$.next(null);
      }
    });
  }
}
