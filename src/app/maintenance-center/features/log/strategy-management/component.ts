import {
  K8SResourceList,
  K8sPermissionService,
  K8sResourceAction,
  K8sUtilService,
  ResourceListParams,
  StringMap,
  TOKEN_BASE_DOMAIN,
  TranslateService,
  noop,
  publishRef,
} from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  INDICES_KEEP_DAYS,
  LogPolicyService,
  POLICY_ENABLED,
} from 'app/api/log';
import { GenericStatus } from 'app/shared/components/status-icon/component';
import { Project } from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';

import { ProjectStrategyDialogComponent } from './project-strategy-dialog/component';
@Component({
  selector: 'alu-strategy-management',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StrategyManagementComponent {
  columns = [
    'project_name',
    'log_retention_time',
    'status',
    'time',
    'create_time',
    'action',
  ];

  resource: Project;
  params$ = this.route.queryParamMap.pipe(
    map(queryParams => ({
      keywords: queryParams.get('keywords') || '',
    })),
    publishRef(),
  );

  searchKey$ = this.params$.pipe(
    map(params => params.keywords),
    publishRef(),
  );

  fetchParams$: Observable<StringMap> = this.params$.pipe(
    map(params =>
      params.keywords
        ? {
            filterBy: `name,${params.keywords}`,
          }
        : null,
    ),
    publishRef(),
  );

  list = new K8SResourceList<Project>({
    fetchParams$: this.fetchParams$,
    fetcher: this.fetchProjects.bind(this),
  });

  permissions$ = this.k8sPermission.isAllowed({
    type: RESOURCE_TYPES.PROJECT,
    action: K8sResourceAction.UPDATE,
  });

  constructor(
    private readonly route: ActivatedRoute,
    private readonly dialog: DialogService,
    private readonly api: LogPolicyService,
    private readonly router: Router,
    private readonly k8sUtil: K8sUtilService,
    @Inject(TOKEN_BASE_DOMAIN)
    private readonly baseDomain: string,
    private readonly translate: TranslateService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly messageService: MessageService,
  ) {}

  fetchProjects(params: ResourceListParams) {
    return this.api.getProjectPolicy(params);
  }

  searchByName(keywords: string) {
    this.router.navigate([], {
      queryParams: { keywords },
      queryParamsHandling: 'merge',
    });
  }

  addProjectStrategy() {
    const dialogRef = this.dialog.open(ProjectStrategyDialogComponent);
    dialogRef.afterClosed().subscribe(res => {
      this.reload(res);
    });
  }

  switch(resource: Project) {
    const enable = this.getEnable(resource);
    const template = !enable
      ? {
          title: 'log_project_strategy_enable_confirm_title',
          content: 'log_project_strategy_enable_confirm_content',
          confirmText: 'enable',
        }
      : {
          title: 'log_project_strategy_deactivate_confirm_title',
          content: 'log_project_strategy_deactivate_confirm_content',
          confirmText: 'deactivate',
        };
    this.dialog
      .confirm({
        title: this.translate.get(template.title, {
          name: `${this.k8sUtil.getName(resource)} ${this.displayNameTemplate(
            resource,
          )}`,
        }),
        content: this.translate.get(template.content),
        confirmText: this.translate.get(template.confirmText),
        cancelText: this.translate.get('cancel'),
      })
      .then(() => {
        this.api
          .updateProjectPolicy(
            resource,
            this.handleProjectProxy(resource, !enable),
          )
          .subscribe(res => {
            const message = enable
              ? 'project_policy_disabled_successfully'
              : 'project_policy_activated_successfully';
            this.messageService.success(
              this.translate.get(message, { name: this.k8sUtil.getName(res) }),
            );
            this.list.reload();
          });
      })
      .catch(noop);
  }

  delete(resource: Project) {
    this.dialog
      .confirm({
        title: this.translate.get('log_project_strategy_delete_confirm_title', {
          name: `${this.k8sUtil.getName(resource)} ${this.displayNameTemplate(
            resource,
          )}`,
        }),
        content: this.translate.get(
          'log_project_strategy_delete_confirm_content',
        ),
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
      })
      .then(() => {
        this.api
          .updateProjectPolicy(
            resource,
            this.handleProjectProxy(resource, !this.getEnable(resource), true),
          )
          .subscribe(res => {
            this.messageService.success(
              this.translate.get('project_policy_deleted_successfully', {
                name: this.k8sUtil.getName(res),
              }),
            );
            this.list.reload();
          });
      })
      .catch(noop);
  }

  displayNameTemplate(resource: Project) {
    return this.k8sUtil.getDisplayName(resource)
      ? `(${this.k8sUtil.getDisplayName(resource)})`
      : '';
  }

  update(resource: Project) {
    const dialogRef = this.dialog.open(ProjectStrategyDialogComponent, {
      data: {
        resource,
      },
    });
    dialogRef.afterClosed().subscribe(res => {
      this.reload(res);
    });
  }

  reload(res: boolean) {
    if (res) {
      this.list.reload();
    }
  }

  handleProjectProxy(project: Project, enable: boolean, isDelete = false) {
    const {
      metadata: { labels },
    } = project;
    const policy = isDelete
      ? {
          [`${this.baseDomain}/${POLICY_ENABLED}`]: null,
          [`${this.baseDomain}/${INDICES_KEEP_DAYS}`]: null,
        }
      : {
          [`${this.baseDomain}/${POLICY_ENABLED}`]: '' + enable,
        };
    return {
      metadata: {
        labels: {
          ...labels,
          ...policy,
        },
      },
    };
  }

  getEnable = (resource: Project) => {
    const status = this.k8sUtil.getLabel(resource, POLICY_ENABLED);
    return status === 'true';
  };

  trackByFn(index: number) {
    return index;
  }

  getStatus = (resource: Project) => {
    return this.getEnable(resource)
      ? GenericStatus.Success
      : GenericStatus.Stopped;
  };
}
