import { TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { ClusterPolicy, LogPolicyService, PolicyType } from 'app/api/log';
const POLICY_MAP = {
  [PolicyType.SYSTEM]: {
    title: 'system_log_time',
    header: 'system_log',
  },
  [PolicyType.PLATFORM]: {
    title: 'platform_log_time',
    header: 'platform_log',
  },
  [PolicyType.KUBERNETES]: {
    title: 'kubenetes_log_time',
    header: 'kubenetes_log',
  },
  [PolicyType.WORKLOAD]: {
    title: 'application_log_time',
    header: 'application_log',
  },
};
@Component({
  selector: 'alu-global-strategy-dialog',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GlobalStrategyDialogComponent implements OnInit {
  @ViewChild('form', { static: true })
  form: NgForm;

  min = 1;
  max = 30;
  submitting: boolean;
  viewModel = {
    title: '',
    header: '',
    value: 7,
  };

  constructor(
    @Inject(DIALOG_DATA)
    public dialogData: {
      type: PolicyType;
      value: number;
      source: ClusterPolicy;
    },
    private readonly dialogRef: DialogRef,
    private readonly messageService: MessageService,
    private readonly translate: TranslateService,
    private readonly api: LogPolicyService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    if (this.dialogData) {
      const { type, value = 7 } = this.dialogData;
      this.viewModel = {
        title: POLICY_MAP[type].title,
        header: POLICY_MAP[type].header,
        value,
      };
    }
  }

  valueMathCeil() {
    this.viewModel.value = Math.ceil(this.viewModel.value);
  }

  confirm(form: NgForm) {
    form.onSubmit(null);
    if (form.invalid) {
      return;
    }
    this.submitting = true;
    const payload = this.handlePayload();
    this.api.updateGlobalPoicy(payload).subscribe(
      () => {
        this.dialogRef.close(true);
        this.submitting = false;
        this.messageService.success(
          `${this.translate.get('global_strategy')}
          (${this.translate.get(this.viewModel.header)})
          ${this.translate.get('update_successed')}
          `,
        );
        this.cdr.markForCheck();
      },
      () => {
        this.submitting = false;
        this.cdr.markForCheck();
      },
    );
  }

  handlePayload() {
    if (!this.dialogData.source) {
      return;
    }
    const { spec, ...res } = this.dialogData.source;
    return {
      ...res,
      spec: {
        ...spec,
        indicesKeepDays: this.viewModel.value,
      },
    };
  }
}
