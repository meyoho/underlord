import {
  K8sUtilService,
  TOKEN_BASE_DOMAIN,
  TranslateService,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { map, tap } from 'rxjs/operators';

import {
  INDICES_KEEP_DAYS,
  LogPolicyService,
  POLICY_ENABLED,
} from 'app/api/log';
import { ProjectApiService } from 'app/api/project/api';
import { Project } from 'app/typings';
interface ProjectPolicyModel {
  project: string;
  log: number;
}
@Component({
  selector: 'alu-project-strategy-dialog',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class ProjectStrategyDialogComponent implements OnInit {
  @ViewChild('form', { static: true })
  form: NgForm;

  submitting: boolean;
  projects: Project[];
  viewModel: ProjectPolicyModel = {
    log: 7,
    project: '',
  };

  project$ = this.projectApi.getProjects({}).pipe(
    map(res => res.items),
    tap(res => {
      this.projects = res;
    }),
  );

  constructor(
    @Inject(DIALOG_DATA)
    public dialogData: {
      resource: Project;
    },
    private readonly dialogRef: DialogRef,
    private readonly messageService: MessageService,
    private readonly translate: TranslateService,
    private readonly projectApi: ProjectApiService,
    private readonly api: LogPolicyService,
    private readonly k8sUtil: K8sUtilService,
    @Inject(TOKEN_BASE_DOMAIN)
    private readonly baseDomain: string,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    if (this.dialogData?.resource) {
      this.viewModel.project = this.k8sUtil.getName(this.dialogData.resource);
      this.viewModel.log = +this.k8sUtil.getLabel(
        this.dialogData.resource,
        'project.esIndicesKeepDays',
      );
    }
  }

  valueMathCeil(value: number) {
    this.viewModel.log = Math.ceil(value);
    this.cdr.markForCheck();
  }

  confirm(form: NgForm) {
    form.onSubmit(null);
    if (form.invalid) {
      return;
    }
    this.submitting = true;
    const { project, part } = this.handleProjectProxy();
    this.api.updateProjectPolicy(project, part).subscribe(
      res => {
        this.submitting = false;
        this.dialogRef.close(true);
        const message = this.dialogData?.resource
          ? 'project_policy_updated_successfully'
          : 'project_policy_add_successfully';
        this.messageService.success(
          this.translate.get(message, { name: this.k8sUtil.getName(res) }),
        );
      },
      () => {
        this.submitting = false;
      },
    );
  }

  getProjectLabel = (resource: Project) => {
    const display = this.k8sUtil.getDisplayName(resource);
    const template = display ? `(${display})` : '';
    const name = this.k8sUtil.getName(resource);
    return `${name} ${template}`;
  };

  handleProjectProxy() {
    const project = this.dialogData?.resource
      ? this.dialogData.resource
      : this.projects.find(
          item => this.k8sUtil.getName(item) === this.viewModel.project,
        );
    const {
      metadata: { labels },
    } = project;
    const part = {
      metadata: {
        labels: {
          ...labels,
          [`${this.baseDomain}/${INDICES_KEEP_DAYS}`]: '' + this.viewModel.log,
          [`${this.baseDomain}/${POLICY_ENABLED}`]:
            this.k8sUtil.getLabel(project, POLICY_ENABLED) || '' + true,
        },
      },
    };
    return { project, part };
  }

  disabledProject = (res: Project) => {
    return !!this.k8sUtil.getLabel(res, INDICES_KEEP_DAYS);
  };
}
