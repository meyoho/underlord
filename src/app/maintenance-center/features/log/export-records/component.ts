import {
  ApiGatewayService,
  AuthorizationStateService,
  DATE_TIME_FORMAT,
  K8SResourceList,
  K8sApiService,
  Reason,
  TranslateService,
} from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import dayjs from 'dayjs';
import { get } from 'lodash-es';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import { AdvanceApi } from 'app/api/advance/api';
import { ArchiveLog } from 'app/typings';
import { RESOURCE_TYPES } from 'app/utils';

const ALL_STATUS_SYMBOL = Symbol('All Status');
const transitionTimePath = ['status', 'conditions', '0', 'lastTransitionTime'];
const DATE_TIME_FORMAT_EXPORT = 'YYYYMMDD-HH:mm:ss';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class ExportRecordsComponent {
  ALL_STATUS_SYMBOL = ALL_STATUS_SYMBOL;
  selectedStatus = '';
  loading: boolean;
  columns = ['file_name', 'type', 'status', 'description', 'time', 'export'];
  statusOptions = ['Completed', 'Processing', 'Pending', 'Failed'];
  reason = Reason;
  isDeployed$ = this.advanceApi.getMeterReportInfo();

  list = new K8SResourceList<ArchiveLog>({
    activatedRoute: this.activatedRoute,
    fetcher: this.fetchResources.bind(this),
    watcher: seed =>
      this.k8sApiService.watchGlobalResourceChange(seed, {
        type: RESOURCE_TYPES.ARCHIVELOG,
      }),
  });

  constructor(
    private readonly advanceApi: AdvanceApi,
    private readonly activatedRoute: ActivatedRoute,
    private readonly k8sApiService: K8sApiService,
    private readonly messageService: MessageService,
    private readonly translateService: TranslateService,
    private readonly auth: AuthorizationStateService,
    private readonly apiGateway: ApiGatewayService,
  ) {}

  fetchResources() {
    return this.k8sApiService
      .getGlobalResourceList<ArchiveLog>({
        type: RESOURCE_TYPES.ARCHIVELOG,
      })
      .pipe(
        map(report => ({
          ...report,
          items: report.items
            .map(item => ({
              ...item,
              status: item?.status?.phase
                ? item.status
                : {
                    phase: 'Pending',
                  },
            }))
            .filter(item => {
              const status = this.getActualStatus(this.selectedStatus);
              if (status) {
                return item.status?.phase === status;
              }
              return true;
            })
            .sort((a, b) => {
              const t1 = get(a, transitionTimePath);
              const t2 = get(b, transitionTimePath);
              return t1 < t2 ? 1 : t1 > t2 ? -1 : 0;
            }),
        })),
      );
  }

  onStatusChange(status: string) {
    this.selectedStatus = status;
    this.list.reload();
  }

  private getActualStatus(status: string | typeof ALL_STATUS_SYMBOL) {
    return status === ALL_STATUS_SYMBOL ? '' : status;
  }

  getTransitionTime(item: ArchiveLog) {
    return get(item, transitionTimePath);
  }

  getDate(time: string) {
    return dayjs(time).format(DATE_TIME_FORMAT);
  }

  getMessage(item: ArchiveLog) {
    return get(item, ['status', 'conditions', '0', 'message']);
  }

  getDescription = (item: ArchiveLog) => {
    const type = item.spec.type;
    const url = new URL(window.location.origin + item.spec.queryString);
    const startTime = url.searchParams.get('start_time');
    const endTime = url.searchParams.get('end_time');
    const projectName = url.searchParams.get('project_name');
    const regionName = url.searchParams.get('region_name');
    const component = url.searchParams.get('component');
    const namespace = url.searchParams.get('kubernetes_namespace');
    const application = url.searchParams.get('application_name');
    return `${dayjs(+startTime * 1000).format(
      DATE_TIME_FORMAT_EXPORT,
    )} ${this.translateService.get('to')} ${dayjs(+endTime * 1000).format(
      DATE_TIME_FORMAT_EXPORT,
    )}-${
      type === 'workload'
        ? (projectName ||
            this.translateService.get('all') +
              this.translateService.get('project')) + '-'
        : ''
    }${
      (regionName ||
        this.translateService.get('all') +
          this.translateService.get('cluster')) + (type === 'system' ? '' : '-')
    }${
      type === 'kubernetes'
        ? component ||
          this.translateService.get('all') +
            this.translateService.get('component')
        : ''
    }${
      type === 'workload'
        ? (namespace ||
            this.translateService.get('all') +
              this.translateService.get('namespace')) + '-'
        : ''
    }${
      type === 'workload' || type === 'platform'
        ? application ||
          this.translateService.get('all') +
            this.translateService.get('application')
        : ''
    }`;
  };

  delete(resource: ArchiveLog) {
    this.k8sApiService
      .deleteGlobalResource<ArchiveLog>({
        type: RESOURCE_TYPES.ARCHIVELOG,
        resource,
      })
      .subscribe(() => {
        this.messageService.success(
          this.translateService.get('delete_success'),
        );
      });
  }

  download(report: ArchiveLog) {
    combineLatest(
      this.apiGateway.getApiAddress(),
      this.auth.getToken(),
    ).subscribe(([address, token]) => {
      window.open(address + report.status.downloadLink + '?token=' + token);
    });
  }

  reExport(report: ArchiveLog) {
    this.advanceApi.clearReportStatus(report, 'log').subscribe(() => {
      this.messageService.success(
        this.translateService.get('export_processing'),
      );
    });
  }
}
