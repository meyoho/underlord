import { RouterModule, Routes } from '@angular/router';

import { ExportRecordsComponent } from './export-records/component';
import { LogQueryComponent } from './query/component';
import { StrategyManagementComponent } from './strategy-management/component';
const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'log-query',
  },
  {
    path: 'log-query',
    component: LogQueryComponent,
  },
  {
    path: 'export-records',
    component: ExportRecordsComponent,
  },
  {
    path: 'strategy-management',
    component: StrategyManagementComponent,
  },
];

export const LogRoutes = RouterModule.forChild(routes);
