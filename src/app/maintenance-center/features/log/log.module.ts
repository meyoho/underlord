import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { LogSharedModule } from 'submodules/ait-shared/log/shared.module';

import { SharedModule } from 'app/shared/shared.module';

import { LogDashboardComponent } from './dashboard/component';
import { ExportRecordsComponent } from './export-records/component';
import { LogRoutes } from './log.routing';
import { LogQueryComponent } from './query/component';
import { StrategyManagementComponent } from './strategy-management/component';
import { GlobalStrategyDialogComponent } from './strategy-management/global-strategy-dialog/component';
import { GlobalStrategyComponent } from './strategy-management/global-strategy/component';
import { ProjectStrategyDialogComponent } from './strategy-management/project-strategy-dialog/component';
@NgModule({
  imports: [
    CommonModule,
    LogRoutes,
    SharedModule,
    FormsModule,
    LogSharedModule,
  ],
  declarations: [
    LogQueryComponent,
    LogDashboardComponent,
    ExportRecordsComponent,
    StrategyManagementComponent,
    GlobalStrategyDialogComponent,
    GlobalStrategyComponent,
    ProjectStrategyDialogComponent,
  ],
  entryComponents: [
    GlobalStrategyDialogComponent,
    ProjectStrategyDialogComponent,
  ],
})
export class LogModule {}
