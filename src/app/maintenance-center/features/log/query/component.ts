import { Component } from '@angular/core';

@Component({
  template: `
    <ng-container
      *aclFeatureGate="'logpolicy'; then new; else old"
    ></ng-container>
    <ng-template #new>
      <acl-log-dashboard></acl-log-dashboard>
    </ng-template>
    <ng-template #old>
      <alu-log-query></alu-log-query>
    </ng-template>
  `,
})
export class LogQueryComponent {}
