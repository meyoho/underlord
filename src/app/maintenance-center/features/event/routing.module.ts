import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EventDashboardComponent } from './dashboard/event.component';
const eventRoutes: Routes = [
  {
    path: '',
    component: EventDashboardComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(eventRoutes)],
  exports: [RouterModule],
})
export class EventRoutingModule {}
