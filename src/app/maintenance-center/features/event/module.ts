import { NgModule } from '@angular/core';

import { EventSharedModule } from 'app/maintenance-center/features-shared/event/shared.module';
import { SharedModule } from 'app/shared/shared.module';

import { EventDashboardComponent } from './dashboard/event.component';
import { EventRoutingModule } from './routing.module';
@NgModule({
  imports: [SharedModule, EventSharedModule, EventRoutingModule],
  declarations: [EventDashboardComponent],
})
export class EventModule {}
