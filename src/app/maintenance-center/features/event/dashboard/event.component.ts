import { Reason } from '@alauda/common-snippet';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { shareReplay, takeUntil } from 'rxjs/operators';

import { AdvanceApi } from 'app/api/advance/api';
import { MaintenanceCenterComponent } from 'app/maintenance-center/component';
@Component({
  templateUrl: './event.component.html',
})
export class EventDashboardComponent implements OnInit, OnDestroy {
  initialized = false;
  cluster: string;
  reason = Reason;
  isDeployed$ = this.advanceApi.getDiagnoseInfo().pipe(shareReplay());
  onDestroy$ = new Subject<void>();

  ngOnInit() {
    this.layoutComponent
      .getCluster$()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(cluster => {
        this.cluster = cluster.metadata.name;
        this.initialized = true;
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  constructor(
    private readonly advanceApi: AdvanceApi,
    private readonly layoutComponent: MaintenanceCenterComponent,
  ) {}
}
