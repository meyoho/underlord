import { publishRef } from '@alauda/common-snippet';
import { NavItemConfig } from '@alauda/ui';
import { Component, Injector } from '@angular/core';
import { NavigationEnd } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { distinctUntilChanged, filter, takeUntil } from 'rxjs/operators';

import { BaseLayoutComponent } from 'app/base-layout-component';
import { Cluster } from 'app/typings';

/**
 * 白名单内的 url 路径会展示集群选择组件
 * 目前只在 feature 列表页响应集群变化事件
 * feature 的列表页路由需要是 xxx/list 格式
 */
const CLUSTER_FEATURE_URL_WHITELIST = ['/monitor', '/event', '/alarm/list'];
@Component({
  templateUrl: 'component.html',
})
export class MaintenanceCenterComponent extends BaseLayoutComponent {
  navConfig: NavItemConfig[] = [
    {
      label: 'monitor',
      key: '/maintenance-center/monitor',
      icon: 'basic:monitor',
      href: '/maintenance-center/monitor',
    },
    {
      label: 'log',
      key: '/maintenance-center/log-main',
      icon: 'basic:log',
      notFlat: true,
      children: [
        {
          label: 'log_strategy_management',
          key: '/maintenance-center/log/strategy-management',
          href: '/maintenance-center/log/strategy-management',
          gate: 'logpolicy',
        },
        {
          label: 'log_query',
          key: '/maintenance-center/log/log-query',
          href: '/maintenance-center/log/log-query',
        },
        {
          label: 'log_export_records',
          key: '/maintenance-center/log/export-records',
          href: '/maintenance-center/log/export-records',
          gate: 'logpolicy',
        },
      ],
    },
    {
      label: 'event',
      key: '/maintenance-center/event',
      icon: 'basic:event',
      href: '/maintenance-center/event',
    },
    {
      label: 'alarm',
      icon: 'basic:alarm_s',
      key: '/maintenance-center/alarm-main',
      notFlat: true,
      children: [
        {
          label: 'alarm',
          key: '/maintenance-center/alarm',
          href: '/maintenance-center/alarm',
        },
        {
          label: 'alarm_template',
          key: '/maintenance-center/alarm_template',
          href: '/maintenance-center/alarm_template',
        },
      ],
    },
    {
      label: 'notification',
      key: '/maintenance-center/noti',
      icon: 'basic:notice_s',
      notFlat: true,
      children: [
        {
          label: 'notification',
          key: '/maintenance-center/notification',
          href: '/maintenance-center/notification',
        },
        {
          label: 'notification_template',
          key: '/maintenance-center/notification_template',
          href: '/maintenance-center/notification_template',
        },
        {
          label: 'notification_object',
          key: '/maintenance-center/notification_receiver',
          href: '/maintenance-center/notification_receiver',
        },
        {
          label: 'notification_sender',
          key: '/maintenance-center/notification_sender',
          href: '/maintenance-center/notification_sender',
        },
        {
          label: 'notification_server',
          key: '/maintenance-center/notification_server',
          href: '/maintenance-center/notification_server',
        },
      ],
    },
  ];

  shouldShowClusterBadge = true;
  private readonly cluster$$ = new BehaviorSubject<Cluster>(null);
  private readonly cluster$ = this.cluster$$.asObservable().pipe(
    filter(_ => !!_),
    distinctUntilChanged(),
    publishRef(),
  );

  constructor(protected injector: Injector) {
    super(injector);
    this.initRouterObserver();
    // 确保在页面没有其他订阅的情况下，保持第一次获得的值
    this.cluster$.pipe(takeUntil(this.onDestroy$)).subscribe();
  }

  getCluster$() {
    return this.cluster$;
  }

  getCluster() {
    return this.cluster$$.value;
  }

  onClusterChange(cluster: Cluster) {
    this.cluster$$.next(cluster);
  }

  private initRouterObserver() {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(({ urlAfterRedirects }: NavigationEnd) => {
        if (
          CLUSTER_FEATURE_URL_WHITELIST.find(url =>
            urlAfterRedirects.includes(url),
          )
        ) {
          this.shouldShowClusterBadge = true;
        } else {
          this.shouldShowClusterBadge = false;
        }
      });
  }
}
