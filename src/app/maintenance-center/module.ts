import { PageModule, PlatformNavModule } from '@alauda/ui';
import { PortalModule } from '@angular/cdk/portal';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { FullScreenService } from 'app/services/full-screen.service';
import { FeaturesModule } from 'app/shared/features/module';
import { SharedModule } from 'app/shared/shared.module';

import { MaintenanceCenterComponent } from './component';
import { MaintenanceCenterRoutingModule } from './routing.module';
@NgModule({
  imports: [
    PageModule,
    PlatformNavModule,
    PortalModule,
    CommonModule,
    FormsModule,
    MaintenanceCenterRoutingModule,
    SharedModule,
    FeaturesModule,
  ],
  declarations: [MaintenanceCenterComponent],
  providers: [FullScreenService],
})
export class MaintenanceCenterModule {}
