import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MaintenanceCenterComponent } from './component';
const routes: Routes = [
  {
    path: '',
    component: MaintenanceCenterComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'monitor',
      },
      {
        path: 'monitor',
        loadChildren: () =>
          import('./features/monitor/monitor.module').then(
            M => M.MonitorModule,
          ),
      },
      {
        path: 'log',
        loadChildren: () =>
          import('./features/log/log.module').then(M => M.LogModule),
      },
      {
        path: 'notification',
        loadChildren: () =>
          import(
            './features/notification/notification/notification.module'
          ).then(M => M.NotificationModule),
      },
      {
        path: 'notification_sender',
        loadChildren: () =>
          import('./features/notification/email-sender/module').then(
            M => M.EmailSenderModule,
          ),
      },
      {
        path: 'notification_receiver',
        loadChildren: () =>
          import('./features/notification/receiver/module').then(
            M => M.NotificationReceiverModule,
          ),
      },
      {
        path: 'notification_server',
        loadChildren: () =>
          import('./features/notification/server/module').then(
            M => M.NotificationServerModule,
          ),
      },
      {
        path: 'notification_template',
        loadChildren: () =>
          import('./features/notification/template/module').then(
            M => M.NotificationTemplateModule,
          ),
      },
      {
        path: 'event',
        loadChildren: () =>
          import('./features/event/module').then(M => M.EventModule),
      },
      {
        path: 'alarm',
        loadChildren: () =>
          import('./features/alarm/alarm/module').then(M => M.AlarmModule),
      },
      {
        path: 'alarm_template',
        loadChildren: () =>
          import('./features/alarm/alarm-template/module').then(
            M => M.AlarmTemplateModule,
          ),
      },
    ],
  },
  {
    path: '**',
    redirectTo: 'manage-platform',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MaintenanceCenterRoutingModule {}
