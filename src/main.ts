import { recordInitUrl } from '@alauda/common-snippet';
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ajax } from 'rxjs/ajax';
import { retry } from 'rxjs/operators';
import 'zone.js/dist/zone';

import { AppModule } from './app/app.module';
import { envs } from './env';
import { environment } from './environments/environment';
import { hmrBootstrap } from './hmr';

if (environment.production) {
  enableProdMode();
}
recordInitUrl();

const bootstrap = () => platformBrowserDynamic().bootstrapModule(AppModule);

ajax
  .getJSON('api/v1/envs')
  .pipe(retry(3))
  .subscribe(remoteEnvs => {
    Object.assign(envs, remoteEnvs);
    if (environment.hmr) {
      if (module.hot) {
        hmrBootstrap(bootstrap);
      } else {
        console.warn('Are you using the --hmr flag for ng serve?');
      }
    } else {
      bootstrap().catch(console.error);
    }
  });
