import { baseDomain, globalNamespace } from '@alauda/common-snippet';

export const envs = {
  LABEL_BASE_DOMAIN: baseDomain,
  GLOBAL_NAMESPACE: globalNamespace,
  LOG_PROXY_PREFIX: 'log-',
};
